function(add_toolkit_target targetName)
	add_executable(${targetName} ${ARGN})
	set_target_properties(${targetName} PROPERTIES FOLDER toolkit)
	target_link_libraries(${targetName} PRIVATE ${toolkit_libraries})

    # Install executable
    install(TARGETS ${targetName}) 

    # Install PDBs
    if (MSVC)
        install(FILES $<TARGET_PDB_FILE:${targetName}> 
            CONFIGURATIONS "RelWithDebInfo" "Debug" 
            TYPE BIN
            OPTIONAL
        )
    endif(MSVC)

endfunction(add_toolkit_target)

