.TH airmass 1 "February 01, 2021" "version 2.1.0" "C\-Munipack 2.0"
.SH NAME
airmass \- utility for computing air\-mass coefficient
.SH SYNOPSIS
.B airmass [ \fIoptions\fR ] \fIinput\-files\fR ...
.br
.B airmass [ \fIoptions\fR ] \fI\-j\fR \fIjulian\-date\fR
.SH DESCRIPTION
The \fBairmass\fR command computes value of air\-mass coefficient (X) for given Julian date, object\'s coordinates and observer\'s coordinates. It may also append the values to a set of measurements stored in a text file.
.PP
When the \fI\-j\fR option is present on the command line, the value of air\-mass coefficient is printed to the standard output stream.
.PP
If one or more file names are present on the command line, each source file given is processed line by line, the program expects the JD value in the first column, which must be divided at least one of common used dividers (semicolon, comma, space, tab char, ...). The JD value can be in full (2453xxx.x) or short (53xxx.x) form. Decimal places must be separated by point, not comma. The value of air\-mass coefficient is computed and appended to the end of the line. If the line starts with the text \fBJD\fR, it is considered to be a table header and the text \fBAIRMASS\fR is appended to the end of the line. All other lines which do not fit to any of previous rules are copied to the output file without modification.
.SH INPUT FILES
Names of input files can be specified directly on a command\-line as command arguments; it is allowed to use the usual wild\-card notation. In case the input files are placed outside the working directory, you have to specify the proper path relative to the current working directory.
.PP
Alternatively, you can also prepare a list of input file names in a text file, each input file on a separate line. It is not allowed to use the wild\-card notation here. Use the \fI\-i\fR option to instruct the program to read the file.
.SH OUTPUT FILES
By default, output files are stored to the current working directory. Their names are derived from the command name followed by a sequential number starting by 1. Command options allows a caller to modify the default naming of output files:
.PP
The \fI\-o\fR option sets the format string; it may contain a path where the files shall be stored to. Special meaning has a sequence of question marks, it is replaced by the ordinal number of a file	indented by leading zeros to the same number of decimal places as the number of the question marks.
.PP
By means of the \fI\-i\fR option, you can modify the initial value of a counter.
.PP
On request, the program can write a list of output files to a text file, use the \fI\-g\fR option to specify a file name.
.SH OPTIONS
Options are used to provide extra information to customize the execution of a command. They are specified as command arguments.
.PP
Each option has a full form starting with two dashes and an optional short form starting with one dash only. Options are case\-sensitive. It is allowed to merge two or more successive short options together. Some options require a value; in this case a value is taken from a subsequent argument. When a full form is used, an option and its value can also be separated by an equal sign. When a short form is used, its value can immediately follow the option.
.PP
Whenever there is a conflict between a configuration file parameter and an option of the same meaning, the option always take precedence.
.TP
.B \fI\-j\fR, \fI\-\-julian\-date\fR \fIjd\fR
compute and print air\-mass coefficient for given Julian date. Do not combine this option with input file names.
.TP
.B \fI\-a\fR, \fI\-\-right\-ascension\fR \fIhhmmss\fR
right ascension of object in hours, minutes and seconds
.TP
.B \fI\-d\fR, \fI\-\-declination\fR \fI\-ddmmss\fR
declination of object in degrees, minutes and seconds
.TP
.B \fI\-l\fR, \fI\-\-longitude\fR \fIdddmmss\fR
longitude of observer in degrees, minutes and seconds; positive values for a location to the east of zero meridian, negative values for a location to the west of zero meridian.
.TP
.B \fI\-b\fR, \fI\-\-latitude\fR \fI\-ddmmss\fR
latitude of observer in degrees, minutes and seconds; positive values for a location to the north of equator, negative values for a location to the south of equator.
.TP
.B \fI\-\-altitude\fR
compute and print also the altitude of the object in degrees.
.TP
.B \fI\-s\fR, \fI\-\-set\fR \fIname=value\fR
set value of configuration parameter
.TP
.B \fI\-i\fR, \fI\-\-read\-dirfile\fR \fIfilepath\fR
read list of input files from specified file; see the Files section for details.
.TP
.B \fI\-g\fR, \fI\-\-make\-dirfile\fR \fIfilepath\fR
save list of output files to specified file, existing content of the file will be overwritten; see the Files section for details.
.TP
.B \fI\-o\fR, \fI\-\-output\-mask\fR \fImask\fR
set output file mask (default=\fBamass????.dat\fR), see the Files section for details.
.TP
.B \fI\-c\fR, \fI\-\-counter\fR \fIvalue\fR
set initial counter value (default=1), see the Files section for details.
.TP
.B \fI\-p\fR, \fI\-\-configuration\-file\fR \fIfilepath\fR
read parameters from given configuration file. See the Configuration file section for details.
.TP
.B \fI\-h\fR, \fI\-\-help\fR
print list of command\-line parameters
.TP
.B \fI\-q\fR, \fI\-\-quiet\fR
quiet mode; suppress all messages
.TP
.B \fI\-\-version\fR
print software version string
.TP
.B \fI\-\-licence\fR
print software licence
.TP
.B \fI\-\-verbose\fR
verbose mode; print debug messages
.SH CONFIGURATION FILE
Configuration files are used to set the input parameters to the process that is going to be executed by a command. Use the \fI\-p\fR option to instruct the program to read the file before other command\-line options are processed.
.PP
The configuration file consists of a set of parameters stored in a text file. Each parameter is stored on a separate line in the following form: \fIname\fR = \fIvalue\fR, all other lines are silently ignored. Parameter names are case\-sensitive.
.TP
.B \fBright\-ascension\fR = \fIhhmmss\fR
right ascension of object in hours, minutes and seconds
.TP
.B \fBdeclination\fR = \fI\-ddmmss\fR
declination of object in degrees, minutes and seconds
.TP
.B \fBlongitude\fR = \fIdddmmss\fR
longitude of observer in degrees, minutes and seconds; positive values for a location to the east of zero meridian, negative values for a location to the west of zero meridian.
.TP
.B \fBlatitude\fR = \fI\-ddmmss\fR
latitude of observer in degrees, minutes and seconds; positive values for a location to the north of equator, negative values for a location to the south of equator.
.SH EXAMPLES
.TP
.B \fBairmass \-l164000 \-b491300 \-a182932 \-d223424 \-j2452763.5670\fR
The command computes and prints value of air\-mass coefficient to the standard output. The object\'s coordinates are R.A. = 18h 29m 32s, DEC. = +22d 34m 24s, the observer\'s coordinates are LON. = 16d 40m to the east of the zero meridian and LAT. = 49d 13m to the north of the equator. Julian date of observation is 2452763.5670.
.TP
.B \fBairmass \-l164000 \-b491300 \-a182932 \-d223424 \-oamass.dat table.dat\fR
The command adds values of air\-mass coefficient to the table stored in \fBtable.dat\fR file and the resulting table stores in \fBamass.dat\fR file. The object\'s and observer\'s coordinates are the same as in previous example.
.SH EXIT STATUS
The command returns a zero exit status if it succeeds to process all specified files. Otherwise, it will stop immediately when an error occurs and a nonzero error code is returned.
.SH HOME PAGE
http://c\-munipack.sourceforge.net/
.SH BUG REPORTS
David Motl, dmotl@volny.cz
.SH COPYING
This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License version 2 as published by the Free Software Foundation.
.PP
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
.SH SEE ALSO
\fBcmunipack\fR(3), \fBmuniwin\fR(1), \fBmeanbias\fR(1), \fBmeandark\fR(1), \fBautoflat\fR(1), \fBbiasbat\fR(1), \fBdarkbat\fR(1), \fBflatbat\fR(1), \fBtimebat\fR(1), \fBhelcor\fR(1), \fBkombine\fR(1), \fBkonve\fR(1), \fBmuniphot\fR(1), \fBmunimatch\fR(1), \fBmunilist\fR(1), \fBmunifind\fR(1)
