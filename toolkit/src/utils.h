/**************************************************************

comfun.h (C-Munipack project)
Common functions, definitions, etc.
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef UTILS_H
#define UTILS_H

/************** Common constats *******************/

/* Standard name of the configuration file */
#define OPTFILE	"default.cfg"
	
/* Max length of the line in text files */
#define MAXLINE 1024
	
/* Error codes: */
#define CMPACK_ERR_CMDLINE_ERROR		2001		/**< Command-line syntax error */
#define CMPACK_ERR_CANT_MAKE_DIRFILE	2002		/**< Cannot make a dirfile */
#define CMPACK_ERR_CANT_OPEN_DIRFILE	2003		/**< Dirfile not found */
#define CMPACK_ERR_CFG_OPEN_ERROR		2004		/**< Cannot open configuratio file */
#define CMPACK_ERR_CFG_READ_ERROR		2005		/**< Error reading configuration file */
#define CMPACK_ERR_INVALID_ROTATION		2006		/**< Invalid angle of image rotation */
#define CMPACK_ERR_NO_TIME_CORRECTION   2007        /**< Missing value of time correction */
#define CMPACK_ERR_INVALID_FILE_MASK	2008		/**< Invalid syntax of file mask */
#define CMPACK_ERR_INVALID_OUT_FILE		2009		/**< Invalid syntax of file path */

/**************** Common datatypes *****************/

/* Program specification */
struct _AppInfo
{
	struct _AppName *program;		/**< Program information */
	struct _CmdOption *options;		/**< List of command-line options */
	struct _CmdParam *params;		/**< List of command-line input files */
	struct _CfgParam *cfg;			/**< Configuration file parameters */
};
typedef struct _AppInfo AppInfo;

/* Program information */
struct _AppName
{	
	const char *name;				/**< Program name */
	const char *description;		/**< Program description */
};
typedef struct _AppName AppName;

/**************** Common functions *****************/

/* Init program info */
void Init(AppInfo *info);

/* Clean allocated memory in program info */
void Clean(AppInfo *info);

/* Prints short version of GPL license to the stdout */
void PrintLicense(AppInfo *info);

/* Prints package version and short info */
void PrintVersion(AppInfo *info);

/* Print help */
void PrintHelp(AppInfo *info);

/* Prints package version and short info */
void PrintPrologue(AppInfo *info);

/* Print program epilogue */
void PrintEpilogue(AppInfo *info, int count);
void PrintEpilogueEx(AppInfo *info, int in_files, int out_files);

/* Prints human readable form of error code to the specified file */
void PrintError(AppInfo *info, const char *file, int aline, int status);

#ifdef _WIN32

/* Formats the name of the output file */
void MakeInputName(char *buf, int buflen, const char *searchpath, const char *filename);

#endif

/* Formats the name of the output file */
void FormatOutputName(char *buf, int buflen, const char *mask, const char *infile, int index);

/* Check syntax of file path */
int CheckFilePath(char *fname);

/* Check syntax of file mask */
int CheckFileMask(char *fmask);

/* Changes extension of inf file to ext, results stores to outf */
void ChangeFileExt(char *outf, const char *inf, const char *ext);

/* Convert string to array of integers */
int StrToIntV(const char *str, int **intv, int *intc);

/* Convert string to array of integers */
int StrToDoubleV(const char *str, double **intv, int *intc);

/* Check syntax of parameter */
int StrToKeyVal(const char *param, char **key, char **value);

/* Make copy of a string */
char *StrDup(const char *str);

#endif
