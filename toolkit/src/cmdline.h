/**************************************************************

cmdline.h (C-Munipack project)
Parser for command line
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMDLINE_H
#define CMDLINE_H

#include "utils.h"

/* Attribute data type */
enum _CmdParamType
{
	CMD_PTYPE_VOID,			/**< Undefined or unknown type */
	CMD_PTYPE_INT,			/**< Integer value */
	CMD_PTYPE_DOUBLE,		/**< Real value */
	CMD_PTYPE_STRING,		/**< Null terminated string */
	CMD_PTYPE_FILEPATH,		/**< File path and name */
	CMD_PTYPE_FILEMASK,		/**< File name mask (can contain '?') */
	CMD_PTYPE_INT_V,		/**< Array of integer numbers */
	CMD_PTYPE_DOUBLE_V,		/**< Array of real numbers */
	CMD_PTYPE_FILELIST,		/**< Array of file names */
	CMD_PTYPE_PARAMLIST		/**< Array of tuples (key, value) */
};
typedef enum _CmdParamType CmdParamType;

/* Attribute value */
struct _CmdParamData
{
	int count, capacity;			/**< Number of values in array */
	union {
		int int_val;				/**< Integer number */
		double dbl_val;				/**< Real number */
		char *str_val;				/**< Null terminated string */
		int *int_vec;				/**< Array of integer numbers */
		double *dbl_vec;			/**< Array of real numbers */
		char **str_vec;				/**< Array of strings */
	} val;							/** Value(s) */
};
typedef struct _CmdParamData CmdParamData;
	
/* Option / parameter data */
struct _CmdParamInstance
{
	CmdParamType type;				/**< Data type */
	CmdParamData data;				/**< Attributes */
};
typedef struct _CmdParamInstance CmdParamInstance;

/* Command-line options */
struct _CmdOption
{
	const char *name;				/**< Long name */
	const char abbrev;				/**< Abbreviation */
	const char *description;		/**< Option description */
	const char *attribute;			/**< Attribute name */
	CmdParamType type;				/**< Data type */
	CmdParamInstance *inst;			/**< Option data */
};
typedef struct _CmdOption CmdOption;

/* Command-line positional paremeters */
struct _CmdParam
{
	const char *name;				/**< Parameter name */
	const char *description;		/**< Parameter description */
	CmdParamType type;				/**< Data type */
	CmdParamInstance *inst;			/**< Parameter data */
};
typedef struct _CmdParam CmdParam;

/********************   Public functions   ********************************/

/* Parses the command line and fills the information to 'cmd' structure */
void cmdline_init(AppInfo *cmd);
	
/* Free memory allocated in the 'cmd' structure */
void cmdline_clean(AppInfo *cmd);

/* Parses the command line and fills the information to 'cmd' structure */
int cmdline_parse(int argc, char **argv, AppInfo *cmd);
	
/* Dump content of the command line parameters */
void cmdline_dump(AppInfo *cmd);
	
/* Prints help */
void cmdline_help(AppInfo *cmd);

/* Returns nonzero if the parameter is present */
int cmdline_isdef(AppInfo *cmd, const char *option);
	
/* Get option value (integer number) */
int cmdline_geti(AppInfo *cmd, const char *option, int *value);
	
/* Get option value (real number) */
int cmdline_getd(AppInfo *cmd, const char *option, double *value);

/* Get option value (string) */
int cmdline_gets(AppInfo *cmd, const char *option, char **value);
	
/* Get option value (array of integer numbers) */
int cmdline_getiv(AppInfo *cmd, const char *option, int **array, int *count);
	
/* Get option value (array of real numbers) */
int cmdline_getdv(AppInfo *cmd, const char *option, double **array, int *count);

/* Returns nonzero if the configuration parameter is present */
int cmdline_iscfgdef(AppInfo *cmd, const char *keyword);

/* Get configuration parameter value (integer number) */
int cmdline_getcfgi(AppInfo *cmd, const char *keyword, int *value);
	
/* Get configuration parameter value (real number) */
int cmdline_getcfgd(AppInfo *cmd, const char *keyword, double *value);

/* Get configuration parameter value (string) */
int cmdline_getcfgs(AppInfo *cmd, const char *keyword, char **value);
	
/* Get configuration parameter value (array of integer numbers) */
int cmdline_getcfgiv(AppInfo *cmd, const char *keyword, int **array, int *count);
	
/* Get configuration parameter value (array of real numbers) */
int cmdline_getcfgdv(AppInfo *cmd, const char *keyword, double **array, int *count);
	
/* Returns nonzero if the positional parameter is present */
int cmdline_ispardef(AppInfo *cmd, const char *param);

/* Get positional parameter value (integer number) */
int cmdline_getpari(AppInfo *cmd, const char *param, int *value);
	
/* Get positional parameter value (real number) */
int cmdline_getpard(AppInfo *cmd, const char *param, double *value);

/* Get positional parameter value (string) */
int cmdline_getpars(AppInfo *cmd, const char *param, char **value);
	
/* Get positional parameter value (array of integer numbers) */
int cmdline_getparsv(AppInfo *cmd, const char *param, char ***array, int *count);
	
#endif
