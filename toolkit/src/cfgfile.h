/**************************************************************

cfgfile.h (C-Munipack project)
Parser for configuration files
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

****************************************************************/

#ifndef CFGFILE_H
#define CFGFILE_H

#include "cmpack_common.h"
#include "utils.h"

/* Data type */
enum _CfgParamType 
{
	CFG_PTYPE_INT,			/**< Signed integer value */
	CFG_PTYPE_DOUBLE,		/**< Double precision FP number */
	CFG_PTYPE_STRING,		/**< Null terminated string */
	CFG_PTYPE_INT_V,		/**< Vector of integer numbers */
	CFG_PTYPE_DOUBLE_V		/**< Vector of double numbers */
};
typedef enum _CfgParamType CfgParamType;

/* Attribute value */
union _CfgParamValue
{
	int int_val;			/**< Integer number */
	double dbl_val;			/**< Real number */
	char *str_val;			/**< Null terminated string */
	int *int_vec;			/**< Array of integer numbers */
	double *dbl_vec;		/**< Array of real numbers */
};
typedef union _CfgParamValue CfgParamValue;

/* Configuration parameter */
struct _CfgParam
{
	const char *name;			/**< Long name */
	CfgParamType type;			/**< Parameter type */
	const char *description;	/**< Option description */
	int present;				/**< Set to nonzero if the parameter has been found */
	int count;					/**< Number of items in the array */
	CfgParamValue val;			/**< Attribute value */
	char *str;					/**< Parameter string */
};
typedef struct _CfgParam CfgParam;

/********************   Public functions   ********************************/

/* Init configuration paremeters */
void cfgfile_init(AppInfo *info);
	
/* Read and process the configuration file */
int cfgfile_read(AppInfo *info, const char *filename);
	
/* Free allocated memory */
void cfgfile_clean(AppInfo *info);
	
/* Returns nonzero if the parameter is present */
int cfgfile_isdef(AppInfo *info, const char *name);
	
/* Get parameter attribute value (integer number) */
int cfgfile_geti(AppInfo *info, const char *name, int *value);
	
/* Get parameter attribute value (real number) */
int cfgfile_getd(AppInfo *info, const char *name, double *value);
	
/* Get parameter attribute value (string) */
int cfgfile_gets(AppInfo *info, const char *name, char **def);
	
/* Get parameter attribute value (array of integer numbers) */
int cfgfile_getiv(AppInfo *info, const char *name, int **array, int *count);
	
/* Get parameter attribute value (array of real numbers) */
int cfgfile_getdv(AppInfo *info, const char *name, double **array, int *count);
	
/* Dumps content of the photometry file to a stream */
void cfgfile_dump(AppInfo *info);
	
/* Prints help */
void cfgfile_help(AppInfo *info);
               
#endif
