/**************************************************************

darkbat.c (C-Munipack project)
Dark-frame correction utility
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifdef _WIN32
#include <io.h>
#endif

#include "cmunipack.h"
#include "cmdline.h"
#include "cfgfile.h"
#include "utils.h"

/* Program */
AppName prog = 
{ 
	"darkbat", 
	"Utility for dark-frame correction of CCD frames"
};

/* Options */
CmdOption opt[] =
{
	{ "configuration-file", 'p', "read parameters from configuration file", "filepath", CMD_PTYPE_FILEPATH },
	{ "set", 's', "set configuration parameter", "name=value", CMD_PTYPE_PARAMLIST },
	{ "read-dirfile", 'i', "read list of source CCD frames from specified file", "filepath", CMD_PTYPE_FILEPATH },
	{ "make-dirfile", 'g', "save list of output files to specified file", "filepath", CMD_PTYPE_FILEPATH },
	{ "output-mask", 'o', "change output file mask (default='dout???.fts')", "mask", CMD_PTYPE_FILEMASK },
	{ "counter", 'c', "set initial counter value (default=1)", "value", CMD_PTYPE_INT },
	{ "help", 'h', "print list of command-line parameters" },
	{ "quiet", 'q', "quiet mode; suppress all messages" },
	{ "version", 0, "print software version" },
	{ "licence", 0, "print software licence" },
	{ "verbose", 0, "verbose mode; print debug messages" },
	{ NULL }
};

/* Input files */
CmdParam flist[] = 
{
	{ "dark-file", "read dark correction frame from specified file", CMD_PTYPE_FILEPATH },
	{ "files", "names of simple CCD frames to dark subtract", CMD_PTYPE_FILELIST },
	{ NULL }
};

/* Configuration parameters */
CfgParam cfg[] = 
{
	{ "scaling", CFG_PTYPE_INT, "Dark frame scaling (0=disabled, 1=enabled)" },
	{ NULL }
};

/* Command-line definition */
AppInfo cmd = 
{ 
	&prog, opt, flist, cfg
};

static void report(int status, const char *file)
{
	PrintError(&cmd, file, 0, status);
}

static void error(int status, const char *file)
{
	PrintError(&cmd, file, 0, status);
	exit(status);
}

static int dark(CmpackDarkCorr *lc, const char *srcpath, const char *dstpath)
{
	int res;
	CmpackCcdFile *src, *dst;

	printf("%s => %s\n", srcpath, dstpath);

	res = cmpack_ccd_open(&src, srcpath, CMPACK_OPEN_READONLY, 0);
	if (res!=0) {
		report(res, srcpath);
		return res;
	}
	res = cmpack_ccd_open(&dst, dstpath, CMPACK_OPEN_CREATE, 0);
	if (res!=0) {
		report(res, dstpath);
		cmpack_ccd_destroy(src);
		return res;
	}
	res = cmpack_dark_ex(lc, src, dst);
	cmpack_ccd_destroy(src);
	if (res!=0) {
		report(res, srcpath);
		cmpack_ccd_destroy(dst);
		return res;
	}
	res = cmpack_ccd_close(dst);
	if (res!=0) {
		report(res, dstpath);
		cmpack_ccd_destroy(dst);
		return res;
	}
	return 0;
}

int main(int argc, char **argv)
{
	FILE	*din = NULL, *dout = NULL;
	int		i, icit, res, scaling, count, in_files = 0, out_files = 0;
	char 	line[MAXLINE], outf[MAXLINE], buf1[MAXLINE];
	char	*darkfile, *dirfile_in, *dirfile_out, *out_mask, *cfgfile, **files;
	CmpackDarkCorr *lc;
	CmpackConsole *con;
	CmpackCcdFile *file;

	Init(&cmd);

	/* Parse command line */
	if (!cmdline_parse(argc, argv, &cmd))
		return CMPACK_ERR_CMDLINE_ERROR;

	/* Process standard options */
	if (cmdline_isdef(&cmd, "version")) {
		PrintVersion(&cmd);
		return 0;
	}
	if (cmdline_isdef(&cmd, "licence")) {
		PrintLicense(&cmd);
		return 0;
	}
	if (cmdline_isdef(&cmd, "help")) {
		PrintHelp(&cmd);
		return 0;
	}

	/* Print program info */
	PrintPrologue(&cmd);

	/* Create context */
	lc = cmpack_dark_init();
	if (!lc) error(CMPACK_ERR_MEMORY, NULL);
	
	/* Set callback function and output level */
	con = cmpack_con_init();
	if (cmdline_isdef(&cmd, "quiet")) 
		cmpack_con_set_level(con, CMPACK_LEVEL_QUIET);
	if (cmdline_isdef(&cmd, "verbose"))
		cmpack_con_set_level(con, CMPACK_LEVEL_DEBUG);
	cmpack_dark_set_console(lc, con);

	/* Read configuration file */
	if (cmdline_gets(&cmd, "configuration-file", &cfgfile)) {
		res = cfgfile_read(&cmd, cfgfile);
		if (res!=0) error(res, NULL);
	}

	/* Set configuration paremeters */
	if (cfgfile_geti(&cmd, "scaling", &scaling))
		cmpack_dark_set_scaling(lc, scaling);
	
	/* Set dark frame */
	if (!cmdline_getpars(&cmd, "dark-file", &darkfile))
		error(CMPACK_ERR_NO_DARK_FRAME, NULL);
	res = cmpack_ccd_open(&file, darkfile, CMPACK_OPEN_READONLY, 0);
	if (res!=0) error(res, darkfile);
	res = cmpack_dark_rdark(lc, file);
	if (res!=0) error(res, darkfile);
	cmpack_ccd_destroy(file);

	/* prepare format string for generating output file names */
	if (!cmdline_gets(&cmd, "output-mask", &out_mask))
		out_mask = "dout???.fts";
	if (!CheckFileMask(out_mask))
		error(CMPACK_ERR_INVALID_FILE_MASK, NULL);

	/* Initial counter value */
	if (!cmdline_geti(&cmd, "counter", &icit))
		icit = 1;

	/* Create output dirfile */
	if (cmdline_gets(&cmd, "make-dirfile", &dirfile_out)) {
		if (strcmp(dirfile_out, "@")==0) {
			dout = stdout;
		} else {
			dout = fopen(dirfile_out, "w+");
		}
		if (dout==NULL) 
			error(CMPACK_ERR_CANT_MAKE_DIRFILE, dirfile_out);
	}

	if (cmdline_gets(&cmd, "read-dirfile", &dirfile_in)) {
		/* Image names are read from a file */
		if (strcmp(dirfile_in, "@")==0) {
			din = stdin;
		} else {
			din = fopen(dirfile_in, "r");
		}
		if (din==NULL) {
			error(CMPACK_ERR_CANT_OPEN_DIRFILE, dirfile_in);
		} else {
			while (fgets(buf1, MAXLINE, din)) {
				if (sscanf(buf1, "%1023s", line) == 1 && CheckFilePath(line)) {
					in_files++;
					FormatOutputName(outf, MAXLINE, out_mask, line, icit++);
					res = dark(lc, line, outf);
					if (res==0) {
						if (dout) fprintf(dout, "%s\n", outf);
						out_files++;
					}
				}
			}
			if (din!=stdin)
				fclose(din);
		}
	} else {
		/* images names are given through the command line */
		if (cmdline_getparsv(&cmd, "files", &files, &count)) {
			for (i=0; i<count; i++) {
				if (CheckFilePath(files[i])) {
#ifdef _WIN32
					struct _finddata_t fd;
					intptr_t fh = _findfirst(files[i], &fd);
					if (fh!=-1) {
						do {
							if ((fd.attrib & _A_SUBDIR)==0)  {
								in_files++;
								MakeInputName(line, MAXLINE, files[i], fd.name);
								FormatOutputName(outf, MAXLINE, out_mask, fd.name, icit++);
								res = dark(lc, line, outf);
								if (res==0) {
									if (dout) fprintf(dout, "%s\n", outf);
									out_files++;
								}
							}
						} while (_findnext(fh, &fd)==0);
						_findclose(fh);
					}
#else
					in_files++;
					FormatOutputName(outf, MAXLINE, out_mask, files[i], icit++);
					res = dark(lc, files[i], outf);
					if (res==0) {
						if (dout) fprintf(dout, "%s\n", outf);
						out_files++;
					}
#endif
				}
			}
		}
	}

	if (dout && dout!=stdout)
		fclose(dout);

	PrintEpilogueEx(&cmd, in_files, out_files);

	cmpack_dark_destroy(lc);
	cmpack_con_destroy(con);
	Clean(&cmd);

	return 0;
}
