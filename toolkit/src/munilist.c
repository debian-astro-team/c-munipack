/**************************************************************

munilist.c (C-Munipack project)
Making output data from photometry files
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifdef _WIN32
#include <io.h>
#endif

#include "cmunipack.h"
#include "cmdline.h"
#include "cfgfile.h"
#include "utils.h"

/* Program */
AppName prog = 
{ 
	"munilist", 
	"utility for making output data from photometry files"
};

/* Options */
CmdOption opt[] =
{
	{ "light-curve", 0, "make a light curve (default)" },
	{ "chart", 0, "make table of objects to make a chart (single frame only)" },
	{ "obj-plot", 0, "make table of properties of an object (see 'object' parameter)" },
	{ "track-list", 0, "make table of frame offsets" },
	{ "diff-mag", 0, "make table of differential instrumental magnitudes (default)" },
	{ "inst-mag", 0, "make table of absolute instrumental magnitudes" },
	{ "aperture", 'a', "aperture identifier", "index", CMD_PTYPE_INT },
	{ "object", 0, "object identifier (see 'obj-plot' parameter)", "star", CMD_PTYPE_INT },
	{ "variable-stars", 'v', "identifier(s) of the variable star(s)", "star", CMD_PTYPE_INT_V },
	{ "comparison-stars", 'c', "indentifier(s) of the comparison star(s)", "star", CMD_PTYPE_INT_V },
	{ "check-stars", 'e', "indentifier(s) of the check star(s)", "star", CMD_PTYPE_INT_V },
	{ "configuration-file", 'p', "read parameters from configuration file", "filepath", CMD_PTYPE_FILEPATH },
	{ "set", 's', "set configuration parameter", "name=value", CMD_PTYPE_PARAMLIST },
	{ "read-dirfile", 'i', "read list of source photometry files from specified file", "filepath", CMD_PTYPE_FILEPATH },
	{ "help", 'h', "print list of command-line parameters" },
	{ "quiet", 'q', "quiet mode; suppress all messages" },
	{ "version", 0, "print software version" },
	{ "licence", 0, "print software licence" },
	{ "verbose", 0, "verbose mode; print debug messages" },
	{ NULL }
};

/* Configuration parameters */
CfgParam cfg[] = 
{
	{ "aperture", CFG_PTYPE_INT, "aperture identifier" },
	{ "var", CFG_PTYPE_INT_V, "identifier(s) of the variable star(s)" },
	{ "comp", CFG_PTYPE_INT_V, "indentifier(s) of the comparison star(s)" },
	{ "check", CFG_PTYPE_INT_V, "indentifier(s) of the check star(s)" },
	{ "object", CFG_PTYPE_INT, "object identifier (see 'obj-plot' parameter)" },
	{ NULL }
};

/* Input files */
CmdParam flist[] = 
{
	{ "output-file", "output file name", CMD_PTYPE_FILEPATH },
	{ "files", "list of input photometry files", CMD_PTYPE_FILELIST },
	{ NULL }
};

/* Command-line definition */
AppInfo cmd = 
{ 
	&prog, opt, flist, cfg
};

static void error(int status, const char *file)
{
	PrintError(&cmd, file, 0, status);
	exit(status);
}

static int list(CmpackFrameSet *fset, const char *srcpath, int frame_id)
{
	int res;
	CmpackPhtFile *pht;

	printf("%s\n", srcpath);

	res = cmpack_pht_open(&pht, srcpath, CMPACK_OPEN_READONLY, 0);
	if (res!=0) {
		error(res, srcpath);
		return res;
	}
	res = cmpack_fset_append_frame(fset, pht, frame_id, srcpath);
	if (res!=0) {
		error(res, srcpath);
		cmpack_pht_destroy(pht);
		return res;
	}
	cmpack_pht_destroy(pht);
	return 0;
}

static int make_light_curve()
{
	FILE	*din = NULL;
	int		i, res, frame_id = 0, instmag, aperture, *intv, in_files = 0, count;
	int		ap_valid = 0;
	char 	line[MAXLINE], buf1[MAXLINE];
	char	*dirfile_in, *output, *cfgfile, **files;
	CmpackLCurve *lc;
	CmpackFrameSet *fset;
	CmpackConsole *con;
	CmpackTable *tab = NULL;
	CmpackPhtAperture apinfo;
	CmpackCatObject stinfo;

	fset = cmpack_fset_init();
	if (!fset) error(CMPACK_ERR_MEMORY, NULL);
	lc = cmpack_lcurve_init();
	if (!lc) error(CMPACK_ERR_MEMORY, NULL);
	
	/* Set output format */
	instmag = 0;
	if (cmdline_isdef(&cmd, "inst-mag"))
		instmag = 1;

	if (!cmdline_getpars(&cmd, "output-file", &output))
		error(CMPACK_ERR_NO_OUTPUT_FILE, NULL);
	if (!CheckFilePath(output))
		error(CMPACK_ERR_INVALID_OUT_FILE, NULL);

	/* Set callback function and output level */
	con = cmpack_con_init();
	if (cmdline_isdef(&cmd, "quiet")) 
		cmpack_con_set_level(con, CMPACK_LEVEL_QUIET);
	if (cmdline_isdef(&cmd, "verbose"))
		cmpack_con_set_level(con, CMPACK_LEVEL_DEBUG);
	cmpack_lcurve_set_console(lc, con);

	/* Read configuration file */
	if (cmdline_gets(&cmd, "configuration-file", &cfgfile)) {
		res = cfgfile_read(&cmd, cfgfile);
		if (res!=0) error(res, NULL);
	}

	/* Set configuration paremeters */
	if (cfgfile_geti(&cmd, "aperture", &aperture)) {
		cmpack_lcurve_set_aperture(lc, aperture);
		apinfo.id = aperture;
		apinfo.radius = 0;
		cmpack_fset_add_aperture(fset, CMPACK_PA_ID, &apinfo);
		ap_valid = 1;
	}
	if (cfgfile_getiv(&cmd, "variable-stars", &intv, &count)) {
		cmpack_lcurve_set_var(lc, intv, count);
		for (i=0; i<count; i++) {
			stinfo.id = intv[i];
			cmpack_fset_add_object(fset, CMPACK_OM_ID, &stinfo);
		}
	}
	if (cfgfile_getiv(&cmd, "comparison-stars", &intv, &count)) {
		cmpack_lcurve_set_comp(lc, intv, count);
		for (i=0; i<count; i++) {
			stinfo.id = intv[i];
			cmpack_fset_add_object(fset, CMPACK_OM_ID, &stinfo);
		}
	}
	if (cfgfile_getiv(&cmd, "check-stars", &intv, &count)) {
		cmpack_lcurve_set_check(lc, intv, count);
		for (i=0; i<count; i++) {
			stinfo.id = intv[i];
			cmpack_fset_add_object(fset, CMPACK_OM_ID, &stinfo);
		}
	}
	
	/* Aperture selection */
	if (cmdline_geti(&cmd, "aperture", &aperture)) {
		cmpack_lcurve_set_aperture(lc, aperture);
		apinfo.id = aperture;
		apinfo.radius = 0;
		cmpack_fset_add_aperture(fset, CMPACK_PA_ID, &apinfo);
		ap_valid = 1;
	}

	/* Set selection */
	if (cmdline_getiv(&cmd, "variable-stars", &intv, &count)) {
		cmpack_lcurve_set_var(lc, intv, count);
			for (i=0; i<count; i++) {
			stinfo.id = intv[i];
			cmpack_fset_add_object(fset, CMPACK_OM_ID, &stinfo);
		}
	}
	if (cmdline_getiv(&cmd, "comparison-stars",  &intv, &count)) {
		cmpack_lcurve_set_comp(lc, intv, count);
		for (i=0; i<count; i++) {
			stinfo.id = intv[i];
			cmpack_fset_add_object(fset, CMPACK_OM_ID, &stinfo);
		}
	}
	if (cmdline_getiv(&cmd, "check-stars", &intv, &count)) {
		cmpack_lcurve_set_check(lc, intv, count);
		for (i=0; i<count; i++) {
			stinfo.id = intv[i];
			cmpack_fset_add_object(fset, CMPACK_OM_ID, &stinfo);
		}
	}

	if (!ap_valid) {
		fprintf(stderr, "Missing aperture selection.");
		return CMPACK_ERR_AP_NOT_FOUND;
	}
	
	/* Create a frame set */
	if (cmdline_gets(&cmd, "read-dirfile", &dirfile_in)) {
		/* Image names are read from a file */
		if (strcmp(dirfile_in, "@")==0) {
			din = stdin;
		} else {
			din = fopen(dirfile_in, "r");
			if (din==NULL) error(CMPACK_ERR_CANT_OPEN_DIRFILE, dirfile_in);
		}
		while (fgets(buf1, MAXLINE, din)) {
			if (sscanf(buf1, "%1023s", line) == 1 && CheckFilePath(line)) {
				in_files++;
				list(fset, line, frame_id++);
			}
		}
		if (din && din!=stdin)
			fclose(din);
	} else {
		/* images names are given through the command line */
		if (cmdline_getparsv(&cmd, "files", &files, &count)) {
			for (i=0; i<count; i++) {
				if (CheckFilePath(files[i])) {
#ifdef _WIN32
					struct _finddata_t fd;
					intptr_t fh = _findfirst(files[i], &fd);
					if (fh!=-1) {
						do {
							if ((fd.attrib & _A_SUBDIR)==0)  {
								in_files++;
								MakeInputName(line, MAXLINE, files[i], fd.name);
								list(fset, line, frame_id++);
							}
						} while (_findnext(fh, &fd)==0);
						_findclose(fh);
					}
#else
					in_files++;
					list(fset, files[i], frame_id++);
#endif
				}
			}
		}
	}

	if (instmag) 
		res = cmpack_lcurve(lc, fset, &tab, CMPACK_LCURVE_INSTMAG);
	else
		res = cmpack_lcurve(lc, fset, &tab, CMPACK_LCURVE_DEFAULT);
	if (res!=0) error(res, NULL);

	/* Write table to file */
	res = cmpack_tab_save(tab, output, 0, NULL, 0);
	if (res!=0) error(res, output);

	PrintEpilogue(&cmd, in_files);

	cmpack_tab_destroy(tab);
	cmpack_fset_destroy(fset);
	cmpack_lcurve_destroy(lc);
	cmpack_con_destroy(con);

	return 0;
}

static int make_track_curve()
{
	FILE	*din = NULL;
	int		i, res, frame_id = 0, in_files = 0, count;
	char 	line[MAXLINE], buf1[MAXLINE];
	char	*dirfile_in, *output, *cfgfile, **files;
	CmpackFrameSet *fset;
	CmpackConsole *con;
	CmpackTable *tab = NULL;

	fset = cmpack_fset_init();
	if (!fset) error(CMPACK_ERR_MEMORY, NULL);
	
	if (!cmdline_getpars(&cmd, "output-file", &output))
		error(CMPACK_ERR_NO_OUTPUT_FILE, NULL);
	if (!CheckFilePath(output))
		error(CMPACK_ERR_INVALID_OUT_FILE, NULL);

	/* Set callback function and output level */
	con = cmpack_con_init();
	if (cmdline_isdef(&cmd, "quiet")) 
		cmpack_con_set_level(con, CMPACK_LEVEL_QUIET);
	if (cmdline_isdef(&cmd, "verbose"))
		cmpack_con_set_level(con, CMPACK_LEVEL_DEBUG);

	/* Read configuration file */
	if (cmdline_gets(&cmd, "configuration-file", &cfgfile)) {
		res = cfgfile_read(&cmd, cfgfile);
		if (res!=0) error(res, NULL);
	}

	/* Create a frame set */
	if (cmdline_gets(&cmd, "read-dirfile", &dirfile_in)) {
		/* Image names are read from a file */
		if (strcmp(dirfile_in, "@")==0) {
			din = stdin;
		} else {
			din = fopen(dirfile_in, "r");
			if (din==NULL) error(CMPACK_ERR_CANT_OPEN_DIRFILE, dirfile_in);
		}
		while (fgets(buf1, MAXLINE, din)) {
			if (sscanf(buf1, "%1023s", line) == 1 && CheckFilePath(line)) {
				in_files++;
				list(fset, line, frame_id++);
			}
		}
		if (din && din!=stdin)
			fclose(din);
	} else {
		/* images names are given through the command line */
		if (cmdline_getparsv(&cmd, "files", &files, &count)) {
			for (i=0; i<count; i++) {
				if (CheckFilePath(files[i])) {
#ifdef _WIN32
					struct _finddata_t fd;
					intptr_t fh = _findfirst(files[i], &fd);
					if (fh!=-1) {
						do {
							if ((fd.attrib & _A_SUBDIR)==0)  {
								in_files++;
								MakeInputName(line, MAXLINE, files[i], fd.name);
								list(fset, line, frame_id++);
							}
						} while (_findnext(fh, &fd)==0);
						_findclose(fh);
					}
#else
					in_files++;
					list(fset, files[i], frame_id++);
#endif
				}
			}
		}
	}

	res = cmpack_tcurve(fset, &tab, CMPACK_TCURVE_DEFAULT, con);
	if (res!=0) error(res, NULL);

	/* Write table to file */
	res = cmpack_tab_save(tab, output, 0, NULL, 0);
	if (res!=0) error(res, output);

	PrintEpilogue(&cmd, in_files);

	cmpack_tab_destroy(tab);
	cmpack_fset_destroy(fset);
	cmpack_con_destroy(con);

	return 0;
}

static int make_obj_plot()
{
	static const int cols = CMPACK_FC_JULDAT | CMPACK_FC_CENTER | CMPACK_FC_SKY | CMPACK_FC_FWHM | CMPACK_FC_MAG;

	FILE	*din = NULL;
	int		i, res, aperture = 0, ap_valid = 0, frame_id = 0, in_files = 0, object_id = 0, obj_valid = 0, count;
	int		object_index, aperture_index;
	char 	line[MAXLINE], buf1[MAXLINE];
	char	*dirfile_in, *output, *cfgfile, **files;
	CmpackFrameSet *fset;
	CmpackConsole *con;
	CmpackTable *tab = NULL;

	fset = cmpack_fset_init();
	if (!fset) error(CMPACK_ERR_MEMORY, NULL);
	
	if (!cmdline_getpars(&cmd, "output-file", &output))
		error(CMPACK_ERR_NO_OUTPUT_FILE, NULL);
	if (!CheckFilePath(output))
		error(CMPACK_ERR_INVALID_OUT_FILE, NULL);

	/* Set callback function and output level */
	con = cmpack_con_init();
	if (cmdline_isdef(&cmd, "quiet")) 
		cmpack_con_set_level(con, CMPACK_LEVEL_QUIET);
	if (cmdline_isdef(&cmd, "verbose"))
		cmpack_con_set_level(con, CMPACK_LEVEL_DEBUG);

	/* Set configuration paremeters */
	if (cfgfile_geti(&cmd, "aperture", &aperture)) {
		CmpackPhtAperture apinfo;
		apinfo.id = aperture;
		cmpack_fset_add_aperture(fset, CMPACK_PA_ID, &apinfo);
		ap_valid = 1;
	}
	if (cfgfile_geti(&cmd, "object", &object_id)) {
		CmpackCatObject objinfo;
		objinfo.id = object_id;
		cmpack_fset_add_object(fset, CMPACK_OM_ID, &objinfo);
		obj_valid = 1;
	}

	/* Aperture selection */
	if (cmdline_geti(&cmd, "aperture", &aperture)) {
		CmpackPhtAperture apinfo;
		apinfo.id = aperture;
		cmpack_fset_add_aperture(fset, CMPACK_PA_ID, &apinfo);
		ap_valid = 1;
	}
	if (cmdline_geti(&cmd, "object", &object_id)) {
		CmpackCatObject objinfo;
		objinfo.id = object_id;
		cmpack_fset_add_object(fset, CMPACK_OM_ID, &objinfo);
		obj_valid = 1;
	}

	/* Read configuration file */
	if (cmdline_gets(&cmd, "configuration-file", &cfgfile)) {
		res = cfgfile_read(&cmd, cfgfile);
		if (res!=0) error(res, NULL);
	}

	if (!ap_valid) {
		fprintf(stderr, "Missing aperture identification.");
		return CMPACK_ERR_AP_NOT_FOUND;
	}
	if (!obj_valid) {
		fprintf(stderr, "Missing object identification.");
		return CMPACK_ERR_STAR_NOT_FOUND;
	}

	/* Create a frame set */
	if (cmdline_gets(&cmd, "read-dirfile", &dirfile_in)) {
		/* Image names are read from a file */
		if (strcmp(dirfile_in, "@")==0) {
			din = stdin;
		} else {
			din = fopen(dirfile_in, "r");
			if (din==NULL) error(CMPACK_ERR_CANT_OPEN_DIRFILE, dirfile_in);
		}
		while (fgets(buf1, MAXLINE, din)) {
			if (sscanf(buf1, "%1023s", line) == 1 && CheckFilePath(line)) {
				in_files++;
				list(fset, line, frame_id++);
			}
		}
		if (din && din!=stdin)
			fclose(din);
	} else {
		/* images names are given through the command line */
		if (cmdline_getparsv(&cmd, "files", &files, &count)) {
			for (i=0; i<count; i++) {
				if (CheckFilePath(files[i])) {
#ifdef _WIN32
					struct _finddata_t fd;
					intptr_t fh = _findfirst(files[i], &fd);
					if (fh!=-1) {
						do {
							if ((fd.attrib & _A_SUBDIR)==0)  {
								in_files++;
								MakeInputName(line, MAXLINE, files[i], fd.name);
								list(fset, line, frame_id++);
							}
						} while (_findnext(fh, &fd)==0);
						_findclose(fh);
					}
#else
					in_files++;
					list(fset, files[i], frame_id++);
#endif
				}
			}
		}
	}

	/* Find aperture index, object index */
	object_index = cmpack_fset_find_object(fset, object_id);
	aperture_index = cmpack_fset_find_aperture(fset, aperture);

	res = cmpack_fset_plot(fset, &tab, CMPACK_TABLE_OBJ_PROP, 
		(CmpackFSetColumns)cols, object_index, aperture_index, 0, 0, 0, 0, 0, 0, con);
	if (res!=0) error(res, NULL);

	/* Write table to file */
	res = cmpack_tab_save(tab, output, 0, NULL, 0);
	if (res!=0) error(res, output);

	PrintEpilogue(&cmd, in_files);

	cmpack_tab_destroy(tab);
	cmpack_fset_destroy(fset);
	cmpack_con_destroy(con);

	return 0;
}

static int pht_file_dump(const char *srcpath, const char *dstfile, int aperture_id)
{
	int res, aperture_index;
	CmpackPhtFile *pht;
	CmpackTable *tab = NULL;

	printf("%s\n", srcpath);

	/* Open source file */
	res = cmpack_pht_open(&pht, srcpath, CMPACK_OPEN_READONLY, 0);
	if (res!=0) {
		error(res, srcpath);
		return res;
	}

	/* Find aperture */
	aperture_index = cmpack_pht_find_aperture(pht, aperture_id);

	/* Make table */
	res = cmpack_pht_dump(pht, &tab, aperture_index, 0, 0);
	if (res!=0) {
		error(res, srcpath);
		cmpack_pht_destroy(pht);
		return res;
	}
	if (res!=0) error(res, NULL);

	/* Write table to file */
	res = cmpack_tab_save(tab, dstfile, 0, NULL, 0);
	if (res!=0) error(res, dstfile);

	cmpack_pht_destroy(pht);
	cmpack_tab_destroy(tab);
	return 0;
}

static int cat_file_dump(const char *srcpath, const char *dstfile)
{
	int res;
	CmpackCatFile *cat;
	CmpackTable *tab = NULL;

	printf("%s\n", srcpath);

	/* Open source file */
	res = cmpack_cat_open(&cat, srcpath, CMPACK_OPEN_READONLY, 0);
	if (res!=0) {
		error(res, srcpath);
		return res;
	}

	/* Make table */
	res = cmpack_cat_dump(cat, &tab, 0, 0);
	if (res!=0) {
		error(res, srcpath);
		cmpack_cat_destroy(cat);
		return res;
	}
	if (res!=0) error(res, NULL);

	/* Write table to file */
	res = cmpack_tab_save(tab, dstfile, 0, NULL, 0);
	if (res!=0) error(res, dstfile);

	cmpack_cat_destroy(cat);
	cmpack_tab_destroy(tab);
	return 0;
}

static int dump(const char *srcpath, const char *dstfile, int aperture_id)
{
	int bytes, filesize;
	char buffer[2048];

	FILE *f = fopen(srcpath, "rb");
	if (!f) 
		return CMPACK_ERR_FILE_NOT_FOUND;

	fseek(f, 0, SEEK_END);
	filesize = ftell(f);
	fseek(f, 0, SEEK_SET);
	bytes = (int)fread(buffer, 1, 2048, f);
	fclose(f);
	if (cmpack_pht_test_buffer(buffer, bytes, filesize))
		return pht_file_dump(srcpath, dstfile, aperture_id);
	else if (cmpack_cat_test_buffer(buffer, bytes, filesize))
		return cat_file_dump(srcpath, dstfile);
	else
		return CMPACK_ERR_UNKNOWN_FORMAT;
}

static int make_chart()
{
	FILE	*din = NULL;
	int		res, aperture = 0, ap_valid = 0, in_files = 0, count;
	char 	line[MAXLINE], buf1[MAXLINE];
	char	*dirfile_in, *output, *cfgfile, **files;

	if (!cmdline_getpars(&cmd, "output-file", &output))
		error(CMPACK_ERR_NO_OUTPUT_FILE, NULL);
	if (!CheckFilePath(output))
		error(CMPACK_ERR_INVALID_OUT_FILE, NULL);

	/* Set configuration paremeters */
	if (cfgfile_geti(&cmd, "aperture", &aperture)) 
		ap_valid = 1;

	/* Aperture selection */
	if (cmdline_geti(&cmd, "aperture", &aperture)) 
		ap_valid = 1;

	/* Read configuration file */
	if (cmdline_gets(&cmd, "configuration-file", &cfgfile)) {
		res = cfgfile_read(&cmd, cfgfile);
		if (res!=0) error(res, NULL);
	}

	if (!ap_valid) 
		aperture = 0;
		
	/* Create a frame set */
	if (cmdline_gets(&cmd, "read-dirfile", &dirfile_in)) {
		/* Image names are read from a file */
		if (strcmp(dirfile_in, "@")==0) {
			din = stdin;
		} else {
			din = fopen(dirfile_in, "r");
			if (din==NULL) error(CMPACK_ERR_CANT_OPEN_DIRFILE, dirfile_in);
		}
		if (din) {
			while (fgets(buf1, MAXLINE, din)) {
				if (sscanf(buf1, "%1023s", line) == 1 && CheckFilePath(line)) {
					in_files++;
					dump(line, output, aperture);
					break;
				}
			}
		}
		if (din && din!=stdin)
			fclose(din);
	} else {
		/* images names are given through the command line */
		if (cmdline_getparsv(&cmd, "files", &files, &count)) {
			if (count>0) {
				int i = 0;
				if (CheckFilePath(files[i])) {
#ifdef _WIN32
					struct _finddata_t fd;
					intptr_t fh = _findfirst(files[i], &fd);
					if (fh!=-1) {
						do {
							if ((fd.attrib & _A_SUBDIR)==0)  {
								in_files++;
								MakeInputName(line, MAXLINE, files[i], fd.name);
								dump(line, output, aperture);
								break;
							}
						} while (_findnext(fh, &fd)==0);
						_findclose(fh);
					}
#else
					in_files++;
					dump(files[i], output, aperture);
#endif
				}
			}
		}
	}

	PrintEpilogue(&cmd, in_files);

	return 0;
}

int main(int argc, char **argv)
{
	int retval;

	Init(&cmd);

	/* Parse command line */
	if (!cmdline_parse(argc, argv, &cmd))
		return CMPACK_ERR_CMDLINE_ERROR;

	/* Process standard options */
	if (cmdline_isdef(&cmd, "version")) {
		PrintVersion(&cmd);
		return 0;
	}
	if (cmdline_isdef(&cmd, "licence")) {
		PrintLicense(&cmd);
		return 0;
	}
	if (cmdline_isdef(&cmd, "help")) {
		PrintHelp(&cmd);
		return 0;
	}

	/* Print program info */
	PrintPrologue(&cmd);

	/* Output type */
	if (cmdline_isdef(&cmd, "track-list"))
		retval = make_track_curve();
	else if (cmdline_isdef(&cmd, "chart"))
		retval = make_chart();
	else if (cmdline_isdef(&cmd, "obj-plot"))
		retval = make_obj_plot();
	else
		retval = make_light_curve();

	Clean(&cmd);

	return retval;
}
