/*============================================================================
*
* wcsconfig.h is generated from wcsconfig.h.in by 'configure'.  It contains
* C preprocessor macro definitions for compiling WCSLIB 6.3
*
* Author: Mark Calabretta, Australia Telescope National Facility, CSIRO.
* http://www.atnf.csiro.au/people/Mark.Calabretta
* $Id: wcsconfig.h.in,v 6.3 2019/07/12 07:33:40 mcalabre Exp $
*===========================================================================*/ 

/* wcslib_version() is available (as of 5.0). */
#define HAVE_WCSLIB_VERSION

/* WCSLIB library version number. */
#define WCSLIB_VERSION 7.7

/* Define to 1 if sincos() is available. */
#undef HAVE_SINCOS

/* 64-bit integer data type. */
#define WCSLIB_INT64 long long

