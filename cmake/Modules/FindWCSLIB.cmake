if(NOT WCSLIB_FOUND)

  find_path(WCSLIB_INCLUDE_DIR wcslib/wcs.h
    HINTS ${WCSLIB_ROOT_DIR} PATH_SUFFIXES include)
  find_library(WCSLIB_LIBRARY wcs
    HINTS ${WCSLIB_ROOT_DIR} PATH_SUFFIXES lib)

  if(EXISTS ${WCSLIB_INCLUDE_DIR})
    file(STRINGS "${WCSLIB_INCLUDE_DIR}/wcslib/wcsconfig.h" TLINE
           REGEX "^#[\t ]*define[\t ]+WCSLIB_VERSION[\t ]+[0-9.]+$")

    string(REGEX REPLACE "^#[\t ]*define[\t ]+WCSLIB_VERSION[\t ]+([0-9.]+)$" "\\1"
           WCSLIB_VERSION_STRING "${TLINE}")

    if(NOT "${WCSLIB_VERSION_STRING}" STREQUAL "")
      message(STATUS "Found WCSLIB version ${WCSLIB_VERSION_STRING}")
    endif()
  endif()

  include(FindPackageHandleStandardArgs)
  find_package_handle_standard_args(WCSLIB DEFAULT_MSG
    WCSLIB_LIBRARY WCSLIB_INCLUDE_DIR WCSLIB_VERSION_STRING)
endif(NOT WCSLIB_FOUND)

# Copy the results to the output variables and target.
if(WCSLIB_FOUND)
  set(WCSLIB_INCLUDE_DIRS ${WCSLIB_INCLUDE_DIR})
  set(WCSLIB_LIBRARIES ${WCSLIB_LIBRARY})

  if(NOT TARGET WCSLIB::WCSLIB)
    add_library(WCSLIB::WCSLIB UNKNOWN IMPORTED)
    set_target_properties(WCSLIB::WCSLIB PROPERTIES
      IMPORTED_LINK_INTERFACE_LANGUAGES "C"
      IMPORTED_LOCATION "${WCSLIB_LIBRARY}"
      INTERFACE_INCLUDE_DIRECTORIES "${WCSLIB_INCLUDE_DIRS}")
  endif()
endif()

mark_as_advanced(WCSLIB_INCLUDE_DIR WCSLIB_LIBRARY WCSLIB_VERSION_STRING M_LIBRARY)
