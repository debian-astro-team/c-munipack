/**************************************************************

tcorr_test.c (C-Munipack project)
Time correction test
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <cmunipack.h>

extern CmpackConsole *g_console;


/* Input frame */
#define IN_FILE "test_fits.fts"

/* Output file name */
#define OUT_FILE "tcorr_out.fts"
        
/* Testing tool for read-all files */
int tcorr_test(void)
{
	int res;
	double tcor;
	CmpackCcdFile *infile, *outfile;
	CmpackCcdParams params;

	tcor = 3600.0;

	/* Run tests */
	res = cmpack_ccd_open(&infile, IN_FILE, CMPACK_OPEN_READONLY, 0);
	if (res!=0) 
		return res;
	res = cmpack_ccd_open(&outfile, OUT_FILE, CMPACK_OPEN_CREATE, 0);
	if (res!=0) 
		return res;
	res = cmpack_tcorr_ex(infile, outfile, tcor, g_console);
	if (res!=0) 
		return res;
	
	cmpack_ccd_get_params(infile, CMPACK_CM_JD, &params);
	printf("Old julian date: %.6f\n", params.jd);
	cmpack_ccd_get_params(outfile, CMPACK_CM_JD, &params);
	printf("New julian date: %.6f\n", params.jd);

	/* Destroy context */
	cmpack_ccd_destroy(infile);
	cmpack_ccd_destroy(outfile);
	return 0;
}
