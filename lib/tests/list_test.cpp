/**************************************************************

list_test.c (C-Munipack project)
Listing test
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <cmunipack.h>

extern CmpackConsole *g_console;

/* Input file names */
#define IN_FILE				"test_pht.pht"

/* Output data file */
#define DIFF_OUT_FILE		"diff_out.dat"
#define INST_OUT_FILE		"inst_out.dat"
#define TRACK_OUT_FILE		"track_out.dat"
#define STDDEV_OUT_FILE		"stdev_out.dat"
#define VARFIND_OUT_FILE	"vfind_out.dat"

/* Variable star indices */
static const int var[] = { 1 };

/* Comparison star indices */
static const int comp[] = { 2, 3 };

/* Check star indices */
static const int check[] = { 4, 5 };

/* Differential magnitudes */
static int test_diff(const char *outfile)
{
	CmpackLCurve *lc = cmpack_lcurve_init();
	CmpackTable *table = cmpack_tab_init(CMPACK_TABLE_UNSPECIFIED);

	cmpack_lcurve_set_console(lc, g_console);
	cmpack_lcurve_set_aperture(lc, 1);
	cmpack_lcurve_set_var(lc, var, sizeof(var)/sizeof(int));
	cmpack_lcurve_set_comp(lc, comp, sizeof(comp)/sizeof(int));
	cmpack_lcurve_set_check(lc, check, sizeof(check)/sizeof(int));

	/* Destroy context */
	cmpack_lcurve_destroy(lc);
	cmpack_tab_destroy(table);
	return 0;
}

/* Instrumental magnitudes */
static int test_inst(const char *outfile)
{
	CmpackLCurve *lc = cmpack_lcurve_init();
	CmpackTable *table = cmpack_tab_init(CMPACK_TABLE_UNSPECIFIED);

	cmpack_lcurve_set_console(lc, g_console);
	cmpack_lcurve_set_aperture(lc, 1);
	cmpack_lcurve_set_var(lc, var, sizeof(var)/sizeof(int));
	cmpack_lcurve_set_comp(lc, comp, sizeof(comp)/sizeof(int));
	cmpack_lcurve_set_check(lc, check, sizeof(check)/sizeof(int));

	/* Destroy context */
	cmpack_lcurve_destroy(lc);
	cmpack_tab_destroy(table);
	return 0;
}

/* Instrumental magnitudes */
static int test_track(const char *outfile)
{
	return 0;
}

/* Instrumental magnitudes */
static int test_stddev(const char *outfile)
{
	return 0;
}

/* Testing tool for read-all files */
int list_test(void)
{
	/* Listing of differential magnitudes */
	test_diff(DIFF_OUT_FILE);

	/* Listing of instrumental magnitudes */
	test_inst(INST_OUT_FILE);

	/* Make track list */
	test_track(TRACK_OUT_FILE);

	/* Make table of standard deviations */
	test_stddev(STDDEV_OUT_FILE);

	return 0;
}
