/**************************************************************

tabtest.c (C-Munipack project)
Testing module for tables
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <cmunipack.h>

extern CmpackConsole *g_console;

/* Input file name */
#define IN_FILE "test_dat.dat"
		
/* Output file name */
#define OUT_FILE "test_out.dat"

/* Read test table */
static int test_read(const char *filename)
{
	int res;
	CmpackTable *fin;

	/* Open input file */
	res = cmpack_tab_load(&fin, filename, 0);
	if (res!=0) 
		return res;

	/* Dump content */
	//cmpack_dump(fin, fin);

	cmpack_tab_destroy(fin);

	return 0;
}

/* Write test table */
static int test_write(const char *filename)
{
	int i, res;
	char val[32];
	CmpackTable *fout = cmpack_tab_init(CMPACK_TABLE_UNSPECIFIED);

	/* Set header data */
	cmpack_tab_pkyi(fout, "KEY1", 1234);
	cmpack_tab_pkyd(fout, "KEY2", 1.234, 3);
	cmpack_tab_pkys(fout, "KEY3", "String");

	/* Set columns */
	cmpack_tab_add_column_int(fout, "COL1", 0, 999, -1);
	cmpack_tab_add_column_dbl(fout, "COL2", 6, 0, 999.9, -1.0);
	cmpack_tab_add_column_str(fout, "COL3");

	/* Set table data */
	for (i=0; i<3; i++) {
		cmpack_tab_append(fout);
		cmpack_tab_ptdi(fout, 0, i);
		cmpack_tab_ptdd(fout, 1, i);
		sprintf(val, "Str:%d", i);
		cmpack_tab_ptds(fout, 2, val);
	}

	//cmpack_dump(fout, fout);

	/* Save output */
	res = cmpack_tab_save(fout, OUT_FILE, 0, 0, 0);
	if (res!=0)
		return res;

	/* Close file */
	cmpack_tab_destroy(fout);
	return 0;
}

int table_test(void)
{
	int res;

	/* Read testing table */
	res = test_read(IN_FILE);
	if (res!=0)
		return res;

	/* Write testing table */
	res = test_write(OUT_FILE);
	if (res!=0)
		return res;

	return 0;
}
