/**************************************************************

catfile_test.c (C-Munipack project)
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <cmunipack.h>

extern CmpackConsole *g_console;

/* Testing catalogue file */
#define CAT_FILE	"test_cat.xml"
	
/* Testing photometry file */
#define PHT_FILE	"test_pht.pht"
	
/* Output file name */
#define OUT_FILE	"test_out.xml"

/* Selected stars */
const static struct 
{ 
	CmpackSelectionType type;
	int count, stars[2]; 
} seltab[] = {
	{ CMPACK_SELECT_VAR, 1, { 6 } },
	{ CMPACK_SELECT_COMP, 1, { 7 } },
	{ CMPACK_SELECT_CHECK, 2, { 41, 52 } },
	{ 0 }
};

/* Test reading file */
static int test_read(const char *filename)
{
	int res;
	CmpackCatFile *f = NULL;
	
	printf("Input file  : %s\n", filename);
	res = cmpack_cat_open(&f, filename, CMPACK_OPEN_READONLY, 0);
	if (res!=0)
		return CMPACK_ERR_CANT_OPEN_SRC;
	/* Close file */
	cmpack_cat_destroy(f);
	printf("-------------------------------\n");
	return 0;
}

/* Test reading file */
static int test_write(const char *infile, const char *outfile)
{
	int i, j, res;
	CmpackCatFile *in, *out;
	
	/* Open source file */
	printf("Input file  : %s\n", infile);
	res = cmpack_cat_open(&in, infile, CMPACK_OPEN_READONLY, 0);
	if (res!=0)
		return res;

	/* Save output file */
	printf("Output file : %s\n", outfile);
	res = cmpack_cat_open(&out, outfile, CMPACK_OPEN_CREATE, 0);
	if (res!=0)
		return res;

	cmpack_cat_copy(out, in);

	/* Set selection */
	for (i=0; seltab[i].type>0; i++) {
		for (j=0; j<seltab[i].count; j++) 
			cmpack_cat_update_selection(out, seltab[i].stars[j], seltab[i].type); 
	}


	/* Close files */
	cmpack_cat_destroy(in);
	cmpack_cat_destroy(out);
	printf("-------------------------------\n");
	return 0;
}

/* Testing tool for catalogue files */
int catfile_test(void)
{
	int res;

	/* Read testing catalogue file */
	res = test_read(CAT_FILE);
	if (res!=0)
		return res;

	/* Read testing photometry file */
	res = test_read(PHT_FILE);
	if (res!=0)
		return res;

	/* Write testing catalogue file */
	res = test_write(PHT_FILE, OUT_FILE);
	if (res!=0)
		return res;

	return 0;
}
