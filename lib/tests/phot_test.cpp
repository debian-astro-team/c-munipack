/**************************************************************

phot_test.c (C-Munipack project)
Photometry test
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <cmunipack.h>

extern CmpackConsole *g_console;

/* Input frame */
#define IN_FILE "test.fits"

/* Output file name */
#define OUT_FILE "phot_out.pht"
        
/* Apertures */
static const double ap[] = { 2.0, 3.0 };

/* Testing tool for read-all files */
int phot_test(void)
{
	int res, nstars;
	CmpackCcdFile *infile;
	CmpackPhtFile *outfile;

	CmpackPhot *lc = cmpack_phot_init();
	cmpack_phot_set_console(lc, g_console);

	cmpack_phot_set_rnoise(lc, 15.0);
	cmpack_phot_set_adcgain(lc, 2.3);
	cmpack_phot_set_minval(lc, 0.0);
	cmpack_phot_set_maxval(lc, 65535.0);
	cmpack_phot_set_fwhm(lc, 3.0);
	cmpack_phot_set_thresh(lc, 3.0);
	cmpack_phot_set_aper(lc, ap, sizeof(ap)/sizeof(double));

	/* Run tests */
	res = cmpack_ccd_open(&infile, IN_FILE, CMPACK_OPEN_READONLY, 0);
	if (res!=0) 
		return res;
	res = cmpack_pht_open(&outfile, OUT_FILE, CMPACK_OPEN_CREATE, 0);
	if (res!=0) 
		return res;
	res = cmpack_phot(lc, infile, outfile, &nstars);
	if (res!=0) 
		return res;
	printf("Stars found: %d\n", nstars);

	/* Destroy context */
	cmpack_ccd_destroy(infile);
	cmpack_pht_destroy(outfile);
	cmpack_phot_destroy(lc);
	return 0;
}
