/**************************************************************

match_test.c (C-Munipack project)
Matching test
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <cmunipack.h>

extern CmpackConsole *g_console;

/* Input file names */
#define IN_FILE		"test_pht.pht"

/* Reference frame */
#define REF_FILE	"test_pht.pht"
        
/* Output file names */
#define OUT_FILE	"match_out.pht"

/* Testing tool for read-all files */
int match_test(void)
{
	int res, mstars;
	CmpackMatch *lc = cmpack_match_init();
	CmpackPhtFile *reffile, *infile, *outfile;

	cmpack_match_set_console(lc, g_console);

	/* Read reference frame */
	res = cmpack_pht_open(&reffile, REF_FILE, CMPACK_OPEN_READONLY, 0);
	if (res!=0) 
		return res;
	res = cmpack_match_readref_pht(lc, reffile);
	if (res!=0) 
		return res;
	cmpack_pht_destroy(reffile);

	/* Open source file */
	res = cmpack_pht_open(&infile, IN_FILE, CMPACK_OPEN_READONLY, 0);
	if (res!=0) 
		return res;

	/* Open target file */
	res = cmpack_pht_open(&outfile, OUT_FILE, CMPACK_OPEN_CREATE, 0);
	if (res!=0)
		return res;

	cmpack_pht_copy(outfile, infile);
	res = cmpack_match(lc, outfile, &mstars);
	if (res!=0) {
		cmpack_match_destroy(lc);
		return res;
	}
	printf("Stars matched: %d\n", mstars);
	cmpack_pht_destroy(infile);
	cmpack_pht_destroy(outfile);

	/* Destroy context */
	cmpack_match_destroy(lc);
	return 0;
}
