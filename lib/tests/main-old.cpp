/**************************************************************

main.c (C-Munipack project)
Main module for debugging suite
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <cmunipack.h>

CmpackConsole *g_console = NULL;

/* Test functions: */
int allfile_test(void);
int catfile_test(void);
int ccdfile_test(void);
int phtfile_test(void);
int table_test(void);
int common_test(void);
int konv_test(void);
int bias_test(void);
int dark_test(void);
int tcorr_test(void);
int flat_test(void);
int amass_test(void);
int helcor_test(void);
int mbias_test(void);
int mdark_test(void);
int mflat_test(void);
int kombine_test(void);
int phot_test(void);
int match_test(void);
int list_test(void);
int readall_test(void);
int fset_test(void);
int xml_test(void);

/* Main module */
int main (int argc, char *argv[]) 
{
	/* Library initialization */
	cmpack_init();

	/* Make console */
	g_console = cmpack_con_init();
	cmpack_con_set_level(g_console, CMPACK_LEVEL_DEBUG);
	
	/* Run tests */
	//allfile_test();
	//catfile_test();
	//ccdfile_test();
	phtfile_test();
	//table_test();
	//common_test();
	//konv_test();
	//bias_test();
	//dark_test();
	//flat_test();
	//tcorr_test();
	//amass_test();
	//helcor_test();
	//mbias_test();
	//mdark_test();
	//mflat_test();
	//kombine_test();
	//phot_test();
	//match_test();
	//list_test();
	//readall_test();
	//xml_test();
	//fset_test();

	/* Destroy console */
	cmpack_con_destroy(g_console);

	/* Dump memory leaks */
	cmpack_cleanup();

	return 0;
}
