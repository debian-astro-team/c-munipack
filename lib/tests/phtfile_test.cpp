/**************************************************************

phttest.c (C-Munipack project)
Testing module for photometry files
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <cmunipack.h>

#include <Windows.h>

extern CmpackConsole *g_console;

/* Testing photometry file */
#define IN_FILE		"test_pht.srt"
	
/* Output file name */
#define OUT_FILE	"test_out.pht"

/* Output file name */
#define OUT_FILE2	"test_out2.pht"

/* Read test file */
static int test_read(const char *filename)
{
	int res;
	CmpackPhtFile *f = NULL;

	/* Open file */
	res = cmpack_pht_open(&f, filename, CMPACK_OPEN_READONLY, 0);
	if (res!=0) 
		return res;

	/* Close file */
	cmpack_pht_destroy(f);
	return 0;
}

/* Write test file */
static int test_write(const char *filename)
{
	int i, j, res, st_index;
	CmpackPhtFile *f;
	CmpackPhtInfo info;
	CmpackPhtObject obj;
	CmpackPhtAperture aper;
	CmpackPhtData data;

	static const double center[3][2] = {
		{ 312.798, 201.677 },
		{ 490.046, 131.553 },
		{ 273.838, 129.021 }
	};

	res = cmpack_pht_open(&f, filename, CMPACK_OPEN_CREATE, 0);
	if (res!=0)
		return res;

	memset(&obj, 0, sizeof(CmpackPhtObject));
	obj.ref_id = -1;

	/* Set header */
	info.width = 768;
	info.height = 512;
	info.jd = 2453613.39685;
	info.filter = "1:Luminance";
	info.exptime = 10.00;
	info.ccdtemp = -6.94;	
	cmpack_pht_set_info(f, CMPACK_PI_FRAME_PARAMS, &info);

	/* Add aperture */
	for (j=0; j<12; j++) {
		aper.id = j;
		aper.radius = 2.0 + j;
		cmpack_pht_add_aperture(f, CMPACK_PA_ID | CMPACK_PA_RADIUS, &aper);
	}

	/* Add star with magnitude and error 
	obj.id = 1;
	obj.x = center[0][0];
	obj.y = center[0][1];
	st_index = cmpack_pht_add_star(f, &obj, sizeof(obj));
	data.valid = 1;
	data.magnitude = 12.6779;
	data.error = 0.0025;
	cmpack_pht_set_data(f, st_index, ap_index, &data, sizeof(data));

	/* Add star with magnitude and code 
	obj.id = 2;
	obj.x = center[1][0];
	obj.y = center[1][1];
	st_index = cmpack_pht_add_star(f, &obj, sizeof(obj));
	data.valid = 1;
	data.magnitude = 14.1172;
	data.error = 0.0;
	cmpack_pht_set_data(f, st_index, ap_index, &data, sizeof(data));

	/* Add star without measurement 
	obj.id = 3;
	obj.x = center[2][0];
	obj.y = center[2][1];
	cmpack_pht_add_star(f, &obj, sizeof(obj));*/

	for (i=0; i<10000; i++) {
		obj.id = i;
		obj.x = center[0][0];
		obj.y = center[0][1];
		st_index = cmpack_pht_add_object(f, CMPACK_PO_ID | CMPACK_PO_CENTER, &obj);
		data.mag_valid = 1;
		data.magnitude = 12.6779;
		data.mag_error = 0.0025;
		for (j=0; j<12; j++) 
			cmpack_pht_set_data(f, st_index, j, &data);
	}
	
	cmpack_pht_destroy(f);
	return 0;
}

static int test_update(const char *infile, const char *outfile)
{
	int i, count, res;
	CmpackPhtFile *fin, *fout;
	CmpackPhtObject obj;

	/* Open input file */
	res = cmpack_pht_open(&fin, infile, CMPACK_OPEN_READONLY, 0);
	if (res!=0) 
		return res;

	res = cmpack_pht_open(&fout, outfile, CMPACK_OPEN_CREATE, 0);
	if (res!=0)
		return res; 

	/* Update stars */
	cmpack_pht_copy(fout, fin);
	count = cmpack_pht_object_count(fout);
	for (i=0; i<count; i++) {
		obj.ref_id = i+1;
		cmpack_pht_set_object(fout, i, CMPACK_PO_REF_ID, &obj);
	}

	cmpack_pht_destroy(fin);
	cmpack_pht_destroy(fout);
	return 0;
}

int phtfile_test(void)
{
	int i, res;
	DWORD time0, dtime;

	/* Read testing photometry file */
	time0 = GetTickCount();
	for (i=0; i<1000; i++)
		res = test_read(OUT_FILE);
	dtime = GetTickCount() - time0;
	printf("%lu", dtime);
	if (res!=0)
		return res;

	/* Write testing photometry file */
	time0 = GetTickCount();
	for (i=0; i<100; i++)
		res = test_write(OUT_FILE);
	dtime = GetTickCount() - time0;
	printf("%lu", dtime);
	if (res!=0)
		return res;

	/* Update testing catalogue file 
	res = test_update(IN_FILE, OUT_FILE2);
	if (res!=0)
		return res;*/

	return 0;
}
