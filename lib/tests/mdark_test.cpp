/**************************************************************

mdark_test.c (C-Munipack project)
Master-dark test
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <cmunipack.h>

extern CmpackConsole *g_console;


/* Input frame */
#define IN_FILES_FMT	"test_fits%d.fts"
#define IN_FILES_COUNT	2

/* Output file name */
#define OUT_FILE		"mdark_out.fts"
        
/* Testing tool for read-all files */
int mdark_test(void)
{
	int i, res;
	char aux[FILENAME_MAX+1];
	CmpackMasterDark *lc = cmpack_mdark_init();
	CmpackCcdFile *infile, *outfile;

	cmpack_mdark_set_console(lc, g_console);
	cmpack_mdark_set_bitpix(lc, CMPACK_BITPIX_SLONG);
	cmpack_mdark_set_scalable(lc, 1);

	/* Open output file */
	res = cmpack_ccd_open(&outfile, OUT_FILE, CMPACK_OPEN_CREATE, 0);
	if (res!=0) 
		return res;
	res = cmpack_mdark_open(lc, outfile);
	if (res!=0) 
		return res;
	cmpack_ccd_destroy(outfile);

	/* Read input frames */
	for (i=0; i<IN_FILES_COUNT; i++) {
		sprintf(aux, IN_FILES_FMT, i+1);
		res = cmpack_ccd_open(&infile, aux, CMPACK_OPEN_READONLY, 0);
		if (res!=0) 
			return res;
		res = cmpack_mdark_read(lc, infile);
		if (res!=0) 
			return res;
		cmpack_ccd_destroy(infile);
	}

	/* Close output file */
	res = cmpack_mdark_close(lc);
	if (res!=0) {
		cmpack_mdark_destroy(lc);
		return res;
	}

	/* Destroy context */
	cmpack_mdark_destroy(lc);
	return 0;
}
