/**************************************************************

kombine_test.c (C-Munipack project)
Matching test
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <cmunipack.h>

extern CmpackConsole *g_console;

/* Input frame */
#define IN_FILES_FMT1	"test_fits%d.fts"
#define IN_FILES_FMT2	"test_fits%d.pht"
#define IN_FILES_COUNT	2

/* Output file */
#define OUT_FILE		"kombine_out.fts"

/* Testing tool for read-all files */
int kombine_test(void)
{
	int i, res;
	char aux[FILENAME_MAX+1];
	CmpackKombine *lc = cmpack_kombine_init();
	CmpackCcdFile *outfile, *ccdfile;
	CmpackPhtFile *phtfile;

	cmpack_kombine_set_console(lc, g_console);
	cmpack_kombine_set_bitpix(lc, CMPACK_BITPIX_SLONG);

	/* Open output file */
	res = cmpack_ccd_open(&outfile, OUT_FILE, CMPACK_OPEN_CREATE, 0);
	if (res!=0) 
		return res;
	res = cmpack_kombine_open(lc, outfile);
	if (res!=0) 
		return res;
	cmpack_ccd_destroy(outfile);

	/* Read input frames */
	for (i=0; i<IN_FILES_COUNT; i++) {
		sprintf(aux, IN_FILES_FMT1, i+1);
		res = cmpack_ccd_open(&ccdfile, aux, CMPACK_OPEN_READONLY, 0);
		if (res!=0) 
			return res;
		sprintf(aux, IN_FILES_FMT2, i+1);
		res = cmpack_pht_open(&phtfile, aux, CMPACK_OPEN_READONLY, 0);
		if (res!=0) 
			return res;
		res = cmpack_kombine_read(lc, ccdfile, phtfile);
		if (res!=0) 
			return res;
		cmpack_ccd_destroy(ccdfile);
		cmpack_pht_destroy(phtfile);
	}

	/* Close output file */
	res = cmpack_kombine_close(lc);
	if (res!=0) {
		cmpack_kombine_destroy(lc);
		return res;
	}

	/* Destroy context */
	cmpack_kombine_destroy(lc);
	return 0;
}
