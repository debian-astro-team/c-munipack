#include "gtest/gtest.h"

#include <cmunipack.h>

CmpackConsole* g_console = NULL;

int main(int argc, char *argv[])
{
    _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

    testing::InitGoogleTest(&argc, argv);

    cmpack_init();

    /* Make console */
    g_console = cmpack_con_init();
    cmpack_con_set_level(g_console, CMPACK_LEVEL_DEBUG);

    int retval = RUN_ALL_TESTS();

    /* Destroy console */
    cmpack_con_destroy(g_console);

    cmpack_cleanup();
    return retval;
}
