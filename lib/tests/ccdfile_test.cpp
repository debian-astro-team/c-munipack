/**************************************************************

ccdfile_test.c (C-Munipack project)
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <fitsio.h>
#include <cmunipack.h>

extern CmpackConsole *g_console;

/* Testing FITS file */
#define FITS_FILE	"test_fits.fts"
/* Testing SBIG file */
#define SBIG_FILE	"test_sbig.st7"
/* Testing OES file */
#define OES_FILE	"test_oes.ast"
	
/* Input file name */
#define OUT_FILE	"test_out.fts"
/* Width of a sample file */
#define OUT_WIDTH	32
/* Height of a sample file */
#define OUT_HEIGHT	32

/* String buffer size */
#define MAXLINE		1024

/* Create sample FITS file */
static int test_out(const char *filename)
{
	fitsfile *fits;
	long naxes[2];
	int i, stat=0, nx = OUT_WIDTH, ny = OUT_HEIGHT;
	char buf[MAXLINE];
	time_t tsec;
	struct tm *actim;
	double *image;

	/* Create simple FITS file */
	remove(filename);
	ffdkinit(&fits, filename, &stat);
	if (stat!=0) {
		ffclos(fits, &stat);
		return stat;
	}

	/* Set file header */
	naxes[0]=nx; naxes[1]=ny;
	ffphps(fits, 16, 2, naxes, &stat);
	sprintf(buf, "%04d-%02d-%02d", 2005, 12, 31);
	ffukys(fits, "DATE-OBS", buf, "UT DATE OF START", &stat);
	sprintf(buf, "%02d:%02d:%02d.%03d", 12, 34, 56, 0);
	ffukys(fits, "TIME-OBS", buf, "UT TIME OF START", &stat);
	tsec = time(&tsec);
	actim = localtime(&tsec);
	sprintf(buf, "%04d-%02d-%02d", actim->tm_year+1900, actim->tm_mon+1, actim->tm_mday);
	ffpkys(fits, "DATE", buf, "DATE OF CREATION", &stat);
	ffpkys(fits, "ORIGIN", "C-Munipack test suite", "FILE ORIGIN", &stat);
	ffpkys(fits, "OBJECT", "Sample", "SAMPLE FITS FILE", &stat);
	ffpkyg(fits, "EXPTIME", 20.0, 2, "EXPOSURE IN SECONDS", &stat);
	ffpkyg(fits, "TEMPERAT", -10.0, 2, "TEMPERATURE IN DEGREES C", &stat);
	ffpkys(fits, "FILTER", "None", "OPTICAL FILTER NAME", &stat);
	/* Set image data */
	image = (double *)malloc(nx*ny*sizeof(double));
	if (image) {
		for (i=0; i<nx*ny; i++)
			image[i] = i;
		ffp2dd(fits, 1, nx, nx, ny, image, &stat);
		free(image);
	}
	/* Close file */
	ffclos(fits, &stat);
	return stat;
}

static void print_par(CmpackCcdFile *file)
{
	CmpackCcdParams params;
   	
	cmpack_ccd_get_params(file, CMPACK_CM_IMAGE | CMPACK_CM_FORMAT |
		CMPACK_CM_OBJECT | CMPACK_CM_DATETIME | CMPACK_CM_EXPOSURE |
		CMPACK_CM_CCDTEMP | CMPACK_CM_FILTER, &params);
	if (params.format_name)
		printf("File format: %s\n", params.format_name);
	printf("Image size : %d x %d pixels\n", params.image_width, params.image_height);
	if (params.object.designation)
		printf("Object     : %s\n", params.object.designation);
  	printf("Date & time: %2d.%2d.%04d %2d:%02d:%02d.%03d UT\n", 
		params.date_time.date.year, params.date_time.date.month, params.date_time.date.day, 
		params.date_time.time.hour, params.date_time.time.minute, params.date_time.time.second, 
		params.date_time.time.milisecond);
	if (params.exposure > 0)
		printf("Exposure   : %.2f s\n", params.exposure);
	if (params.ccdtemp > -999 && params.ccdtemp < 999)
		printf("Temperature: %.1f deg.C\n", params.ccdtemp);
	if (params.filter) 
		printf("Filter     : %s\n", params.filter);
}
 
/* Test reading FITS file */
static int test_read(const char *filename)
{
	int res;
	CmpackCcdFile *f = NULL;
	
	printf("Input file : %s\n", filename);
	res = cmpack_ccd_open(&f, filename, CMPACK_OPEN_READONLY, 0);
	if (f) {
		/* Print short info */
		print_par(f);
		/* Close files */
		cmpack_ccd_destroy(f);
	}
	printf("-------------------------------\n");
	return res;
}

int ccdfile_test(void)
{
	int res;

	/* Create sample FITS file */
	res = test_out(OUT_FILE);
	if (res!=0)
		return res;

	/* Read testing FITS file */
	res = test_read(FITS_FILE);
	if (res!=0)
		return res;

	/* Read testing SBIG file */
	res = test_read(SBIG_FILE);
	if (res!=0)
		return res;

	/* Read testing OES file */
	res = test_read(OES_FILE);
	if (res!=0)
		return res;

	return 0;
}
