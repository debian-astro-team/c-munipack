/**************************************************************

xml_test.c (C-Munipack project)
XML reading test
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <cmunipack.h>

#include <cmunipack.h>

/* Input file name */
#define IN_FILE		"xml_test.xml"

static void indent(int level)
{
	int i;

	for (i=0; i<level; i++)
		printf("  ");
}

static void dump_comment(CmpackComment *node, int level)
{
	indent(level);
	printf("Comment = '%s'\n", cmpack_xml_comment_text(node));
}

static void dump_cdata(CmpackCData *node, int level)
{
	indent(level);
	printf("CDATA = '%s'\n", cmpack_xml_cdata_text(node));
}

static void dump_element(CmpackElement *node, int level)
{
	indent(level);
	printf("%s:\n", cmpack_xml_element_name(node));
	for (int i = 0; i < cmpack_xml_element_n_attributes(node); i++) {
		indent(level);
		CmpackAttribute* att = cmpack_xml_element_attribute(node, i);
		printf("  - %s = '%s'\n", cmpack_xml_attribute_name(att), cmpack_xml_attribute_value(att));
	}

	CmpackNode* child = cmpack_xml_node_first_child((CmpackNode*)node);
	while (child) {
		CmpackXmlNodeType type = cmpack_xml_node_type((CmpackNode*)node);
		if (type == CMPACK_XML_ELEMENT_NODE)
			dump_element((CmpackElement*)child, level+1);
		else if (type == CMPACK_XML_COMMENT_NODE)
			dump_comment((CmpackComment*)child, level+1);
		else if (type == CMPACK_XML_TEXT_NODE)
			dump_cdata((CmpackCData*)child, level+1);
		child = cmpack_xml_node_next_sibling(child);
	}
}

/* Testing tool for frame sets */
int xml_test(void)
{
	FILE *f = fopen(IN_FILE, "rb");
	if (f) {
		CmpackDocument *fin = cmpack_xml_doc_from_file(f);
		dump_element(cmpack_xml_doc_get_root(fin), 0);
		cmpack_xml_doc_free(fin);
		fclose(f);
	}
	return 0;
}
