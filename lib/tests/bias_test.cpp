/**************************************************************

bias_test.c (C-Munipack project)
Bias correction test
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <cmunipack.h>

extern CmpackConsole *g_console;

/* Input frame */
#define IN_FILE "test_fits.fts"

/* Correction frame */
#define BIAS_FILE "test_bias.fts"

/* Output file name */
#define OUT_FILE "bias_out.fts"
        
/* Testing tool for read-all files */
int bias_test(void)
{
	int res;
	CmpackBiasCorr *lc = cmpack_bias_init();
	CmpackCcdFile *bias, *infile, *outfile;

	cmpack_bias_set_console(lc, g_console);

	/* Read correction frame */
	res = cmpack_ccd_open(&bias, BIAS_FILE, CMPACK_OPEN_READONLY, 0);
	if (res!=0) 
		return res;
	res = cmpack_bias_rbias(lc, bias);
	cmpack_ccd_destroy(bias);
	if (res!=0) 
		return res;

	/* Run tests */
	res = cmpack_ccd_open(&infile, IN_FILE, CMPACK_OPEN_READONLY, 0);
	if (res!=0) 
		return res;
	res = cmpack_ccd_open(&outfile, OUT_FILE, CMPACK_OPEN_CREATE, 0);
	if (res!=0) 
		return res;
	res = cmpack_bias_ex(lc, infile, outfile);
	if (res!=0) 
		return res;
	cmpack_ccd_destroy(infile);
	cmpack_ccd_destroy(outfile);

	cmpack_bias_destroy(lc);
	return 0;
}
