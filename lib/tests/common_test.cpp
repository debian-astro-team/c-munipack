/**************************************************************

common_test.c (C-Munipack project)
Test of conversion and astronomical routines
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <cmunipack.h>

extern CmpackConsole *g_console;


int common_test(void)
{
	const char *str;
	char buf[128];
	CmpackDate date;
	CmpackTime time;
	CmpackDateTime dt;
	int res;
	double jd, ra, dec, lon, lat, amass, alt, hcorr;

	/******************** Date & time conversion functions ********************/
	str = "2007-12-24";
	res = cmpack_strtodate(str, &date);
	if (res==0)
		printf("Date conversion: '%s' --> %.2d.%.2d.%.4d\n", str, date.day, date.month, date.year);
	if (cmpack_datetostr(&date, buf, 128)==0)
		printf("Date conversion: %.2d.%.2d.%.4d --> '%s'\n", date.day, date.month, date.year, buf);

	str = "12:34:56.789";
	res = cmpack_strtotime(str, &time);
	if (res==0)
		printf("Time conversion: '%s' --> %.2d:%.2d:%.2d.%.3d\n", str, time.hour, time.minute, time.second, time.milisecond);
	if (cmpack_timetostr(&time, buf, 128)==0)
		printf("Time conversion: %.2d:%.2d:%.2d.%.3d --> '%s'\n", time.hour, time.minute, time.second, time.milisecond, buf);

	jd = 2454123.853642;
	res = cmpack_decodejd(jd, &dt);
	if (res==0)
		printf("JD conversion: %.7f --> %.2d.%.2d.%.4d %.2d:%.2d:%.2d.%.3d\n", jd, dt.date.day, dt.date.month,
		dt.date.year, dt.time.hour, dt.time.minute, dt.time.second, dt.time.milisecond);
	jd = cmpack_encodejd(&dt);
	if (jd>0)
		printf("JD conversion: %.2d.%.2d.%.4d %.2d:%.2d:%.2d.%.3d --> %.7f\n", dt.date.day, dt.date.month,
		dt.date.year, dt.time.hour, dt.time.minute, dt.time.second, dt.time.milisecond, jd);

	/******************** Conversion of coordinates **********************/

	str = "230452.0";
	res = cmpack_strtora(str, &ra);
	if (res==0)
		printf("R.A. conversion: '%s' --> %.5f\n", str, ra);
	if (cmpack_ratostr(ra, buf, 128)==0)
		printf("R.A. conversion: %.5f --> '%s'\n", ra, buf);

	str = "+592257.0";
	res = cmpack_strtodec(str, &dec);
	if (res==0)
		printf("Dec. conversion: '%s' --> %.5f\n", str, dec);
	if (cmpack_dectostr(dec, buf, 128)==0)
		printf("Dec. conversion: %.5f --> '%s'\n", dec, buf);

	str = "163502";
	res = cmpack_strtolon(str, &lon);
	if (res==0)
		printf("Lon. conversion: '%s' --> %.5f\n", str, lon);
	if (cmpack_lontostr(lon, buf, 128)==0)
		printf("Lon. conversion: %.5f --> '%s'\n", lon, buf);

	str = "491217";
	res = cmpack_strtolat(str, &lat);
	if (res==0)
		printf("Lat. conversion: '%s' --> %.5f\n", str, lat);
	if (cmpack_lattostr(lat, buf, 128)==0)
		printf("Lat. conversion: %.5f --> '%s'\n", lat, buf);

	/************************ Other functions **********************************/

	cmpack_airmass(jd, ra, dec, lon, lat, &amass, &alt);
	printf("Air-mass coef.: %.5f, %.4f, %.4f, %.4f, %.4f --> %.3f, %.3f\n", jd, ra, dec, lon, lat, amass, alt);

	hcorr = cmpack_helcorr(jd, ra, dec);
	printf("Hel. correction: %.5f, %.4f, %.4f --> %.5f\n", jd, ra, dec, hcorr);

	return 0;
}
