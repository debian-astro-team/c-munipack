/**************************************************************

fset_test.c (C-Munipack project)
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <cmunipack.h>

/* Input file name */
#define IN_FILE		"lightcurve-test.xml"

/* Testing photometry file */
#define PHT_FILE	"test_pht.pht"

/* Input file name */
#define OUT_FILE	"fset_out.xml"

/* Testing tool for frame sets */
int fset_test(void)
{
	int res;
	CmpackFrameSet *fin;

	/* Open file */
	res = cmpack_fset_load(&fin, IN_FILE, 0);
	if (res!=0) 
		return res;
	cmpack_fset_destroy(fin);

	/* Create output file 
	fout = cmpack_fset_init();
	if (cmpack_pht_load(&f, PHT_FILE, CMPACK_LOAD_DEFAULT)==0) {
		cmpack_fset_add_frame(fout, f, 1, &index);
		cmpack_fset_chart_loadpht(fout, f);
		cmpack_unref(f);
	}
	res = cmpack_fset_save(fout, OUT_FILE);
	cmpack_unref(fout);*/
	return res;
}
