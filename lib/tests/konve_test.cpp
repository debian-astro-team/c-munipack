/**************************************************************

konve_test.c (C-Munipack project)
File conversion test
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <cmunipack.h>

extern CmpackConsole *g_console;

/* Input file name(s) */
#define FITS_IN_FILE "test_fits.fts"
#define SBIG_IN_FILE "test_sbig.st7"
#define OES_IN_FILE "test_oes.ast"
		
/* Output file name */
#define FITS_OUT_FILE "konve_out1.fts"
#define SBIG_OUT_FILE "konve_out2.fts"
#define OES_OUT_FILE "konve_out3.fts"
        
/* Run test for specified file */
static int test_file(CmpackKonv *lc, const char *in, const char *out)
{
	int res;
	CmpackCcdFile *infile, *outfile;

	res = cmpack_ccd_open(&infile, in, CMPACK_OPEN_READONLY, 0);
	if (res!=0) 
		return res;
	res = cmpack_ccd_open(&outfile, out, CMPACK_OPEN_CREATE, 0);
	if (res!=0) 
		return res;

	/* Convert file */
	res = cmpack_konv(lc, infile, outfile);
	if (res!=0)
		return res;

	cmpack_ccd_destroy(infile);
	cmpack_ccd_destroy(outfile);
	return 0;
}


/* Testing tool for read-all files */
int konv_test(void) 
{
	/* Make process context */
	CmpackKonv *lc = cmpack_konv_init();
	cmpack_konv_set_console(lc, g_console);

	/* Run tests */
	test_file(lc, FITS_IN_FILE, FITS_OUT_FILE);
	test_file(lc, SBIG_IN_FILE, SBIG_OUT_FILE);
	test_file(lc, OES_IN_FILE, OES_OUT_FILE);

	/* Destroy context */
	cmpack_konv_destroy(lc);
	return 0;
}
