/**************************************************************

xml_dom.h (C-Munipack project)
Simplified Document Object Model 
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_XML_DOM_H
#define CMPACK_XML_DOM_H

#include "cmpack_common.h"

/***********************      Datatypes      ******************/

/* Node type */
typedef enum _CmpackNodeType
{
	ELEMENT_NODE,
	TEXT_NODE,
	COMMENT_NODE,
	DOCUMENT_NODE
} CmpackNodeType;

/* Node */
typedef struct _CmpackNode
{
	char *nodeName;
	char *nodeValue;
	CmpackNodeType nodeType;
	struct _CmpackNode *parentNode;
	struct _CmpackNode *firstChild;
	struct _CmpackNode *lastChild;
	struct _CmpackNode *nextSibling;
	void (*destroy)(void*);
} CmpackNode;

/* Document */
typedef struct _CmpackDocument 
{
	CmpackNode	*root;
} CmpackDocument;

typedef struct _CmpackAttribute
{
	char *name;
	char *value;
} CmpackAttribute;

/* Attributes */
typedef struct _CmpackAttributes
{
	int count;
	CmpackAttribute *list;
} CmpackAttributes;

/* Element */
typedef struct _CmpackElement
{
	CmpackNode node;
	CmpackAttributes attr;
} CmpackElement;

/* Comment */
typedef struct _CmpackComment
{
	CmpackNode node;
} CmpackComment;

/* Character data */
typedef struct _CmpackCData
{
	CmpackNode node;
} CmpackCData;

/******************   Public functions    *********************/

/* Load document from a file */
CmpackDocument *xml_doc_from_file(FILE *f);

/* Destroy the XML document */
void xml_doc_free(CmpackDocument *doc);

/* Get document root */
CmpackElement *xml_doc_get_root(CmpackDocument *doc);

/* Get first child element by name */
CmpackElement *xml_first_element(CmpackElement *node, const char *name);

/* Get next child element of the same name */
CmpackElement *xml_next_element(CmpackElement *node);

/* Get number of child nodes of given name */
int xml_get_n_children(CmpackElement *elem, const char *name);

/* Returns nonzero if the element has the attribute */
int xml_has_attribute(CmpackElement *elem, const char *attr);
	
/* Get element's attribute */
const char *xml_attr_s(CmpackElement *elem, const char *attr, const char *defval);
int xml_attr_i(CmpackElement *elem, const char *attr, int defval);
double xml_attr_d(CmpackElement *elem, const char *attr, double defval);

/* Get node's content */
const char *xml_value(CmpackElement *elem, const char *defval);
int xml_value_s(CmpackElement *elem, char *buf, int bufsize);
int xml_value_i(CmpackElement *elem, int defval);
double xml_value_d(CmpackElement *elem, double defval);
int xml_value_datetime(CmpackElement *elem, struct tm *t);

/* Get pointer to the first child node of comment type */
const char *xml_comment(CmpackNode *node);

/* Get child node's value */
const char *xml_child_value(CmpackElement *elem, const char *name, const char *defval);
int xml_child_value_s(CmpackElement *elem, const char *name, char *buf, int bufsize);
int xml_child_value_i(CmpackElement *elem, const char *name, int defval);
double xml_child_value_d(CmpackElement *elem, const char *name, double defval);
int xml_child_value_tm(CmpackElement *elem, const char *name, struct tm *t);

#endif
