/**************************************************************

mflat_test.c (C-Munipack project)
Master-flat test
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <cmunipack.h>

extern CmpackConsole *g_console;


/* Input frame */
#define IN_FILES_FMT	"test_fits%d.fts"
#define IN_FILES_COUNT	2

/* Output file name */
#define OUT_FILE		"mflat_out.fts"
        
/* Testing tool for read-all files */
int mflat_test(void)
{
	int i, res;
	char aux[FILENAME_MAX+1];
	CmpackCcdFile *infile, *outfile;

	CmpackMasterFlat *lc = cmpack_mflat_init();
	cmpack_mflat_set_console(lc, g_console);
	cmpack_mflat_set_bitpix(lc, CMPACK_BITPIX_ULONG);
	cmpack_mflat_set_level(lc, 1000000.0);

	/* Open output file */
	res = cmpack_ccd_open(&outfile, OUT_FILE, CMPACK_OPEN_CREATE, 0);
	if (res!=0) 
		return res;
	res = cmpack_mflat_open(lc, outfile);
	if (res!=0) 
		return res;
	cmpack_ccd_destroy(outfile);

	/* Read input frames */
	for (i=0; i<IN_FILES_COUNT; i++) {
		sprintf(aux, IN_FILES_FMT, i+1);
		res = cmpack_ccd_open(&infile, aux, CMPACK_OPEN_READONLY, 0);
		if (res!=0) 
			return res;
		res = cmpack_mflat_read(lc, infile);
		if (res!=0) 
			return res;
		cmpack_ccd_destroy(infile);
	}

	/* Close output file */
	res = cmpack_mflat_close(lc);
	if (res!=0) {
		cmpack_mflat_destroy(lc);
		return res;
	}

	/* Destroy context */
	cmpack_mflat_destroy(lc);
	return 0;
}
