/**************************************************************

comfun.h (C-Munipack project)
Private common types, definitions, helper functions, ...
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CONSOLE_H
#define CONSOLE_H

#include "cmpack_console.h"

/**************** Common data types *****************/

typedef void CmpackPrint(struct _CmpackConsole *con, const char *message);

/**************** Common functions *****************/

int is_debug(CmpackConsole *ctx);

/**
	\brief Print message using callback function
	\details This internal functions should be used for all print outs. 
	It performs message filtering and pass it to the callback procedure. 
	\param[in] ctx			any context
	\param[in] debug		0=normal message, 1=debug message
	\param[in] message		text
*/
void printout(CmpackConsole *ctx, int debug, const char *message);

/**
	\brief Format error message and print it using callback function
	\param[in] ctx			any context
	\param[in] error		error code
*/
void printerr(CmpackConsole *ctx, int error);

/**
	\brief Print parameter value (integer)
	\param[in] ctx			any context
	\param[in] name			parameter name
	\param[in] value		parameter value
	\param[in] valid		nonzero if the value is defined
*/
void printpari(CmpackConsole *ctx, const char *name, int valid, int value);

/**
	\brief Print parameter value (double)
	\param[in] ctx			any context
	\param[in] name			parameter name
	\param[in] value		parameter value
	\param[in] valid		nonzero if the value is defined
	\param[in] prec			number of decimal places
*/
void printpard(CmpackConsole *ctx, const char *name, int valid, double value, int prec);

/**
	\brief Print parameter value (string)
	\param[in] ctx			any context
	\param[in] name			parameter name
	\param[in] valid		nonzero if the value is defined
	\param[in] value		parameter value (NULL=not defined)
*/
void printpars(CmpackConsole *ctx, const char *name, int valid, const char *value);

/**
	\brief Print parameter value (integer)
	\param[in] ctx			any context
	\param[in] name			parameter name
	\param[in] valid		nonzero if the value is defined
	\param[in] count		number of items in the array
	\param[in] values		array of values
*/
void printparvi(CmpackConsole *ctx, const char *name, int valid, int count, const int *values);

/**
	\brief Print parameter value (double)
	\param[in] ctx			any context
	\param[in] name			parameter name
	\param[in] valid		nonzero if the value is defined
	\param[in] count		number of items in the array
	\param[in] values		array of values
	\param[in] prec			number of decimal places
*/
void printparvd(CmpackConsole *ctx, const char *name, int valid, int count, const double *values, int prec);

#endif
