/**************************************************************

konvsbig.h (C-Munipack project)
SBIG frames manipulating functions
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef KONVSBIG_H
#define KONVSBIG_H

#include "cmpack_common.h"
#include "cmpack_console.h"
#include "ccdfile.h"

/* Returns 1 if the file is of SBIG format */
int sbig_test(const char *data, size_t data_length, size_t filesize);

/* Open a file */
int sbig_open(tHandle *handle, const char *filename, CmpackOpenMode mode, unsigned flags);

/* Close file and free allocated memory */
void sbig_close(tHandle handle);

/* Reads string value from header */
int sbig_gets(tHandle handle, const char *key, char **val);

/* Reads integer value from header */
int sbig_geti(tHandle handle, const char *key, int *val);

/* Reads real value from header */
int sbig_getd(tHandle handle, const char *key, double *val);

/* Random access reading of (key, value, comment) triples from hedader */
int sbig_gkyn(tHandle handle, int index, char **key, char **val, char **com);

/* Get image size in pixels */
int sbig_getsize(tHandle handle, int *width, int *height);

/* Get native image data format */
CmpackBitpix sbig_getbitpix(tHandle handle);

/* Get image data range */
int sbig_getrange(tHandle handle, double *minvalue, double *maxvalue);

/* Get image data to given buffer */
int sbig_getimage(tHandle handle, void *buf, int bufsize, CmpackChannel channel);

/* Get file format designation */
char *sbig_getmagic(tHandle handle);

/* Get date and time of observation */
int sbig_getdatetime(tHandle handle, CmpackDateTime *dt);

/* Get exposure duration */
int sbig_getexptime(tHandle handle, double *val);

/* Get CCD temperature */
int sbig_getccdtemp(tHandle handle, double *val);

/* Get optical filter name */
char *sbig_getfilter(tHandle handle, CmpackChannel channel);

/* Get observer's name */
char *sbig_getobserver(tHandle handle);

/* Add string to history */
int sbig_copyheader(tHandle handle, CmpackImageHeader *out, CmpackChannel channel, CmpackConsole *con);

#endif
