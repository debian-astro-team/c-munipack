/**************************************************************

matfun.h (C-Munipack project)
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_MATCH_H
#define CMPACK_MATCH_H

#include "cmpack_match.h"
#include "console.h"
#include "matstack.h"
#include "fft.h"

/* Matching context */
struct _CmpackMatch
{
	int refcnt;					/**< Reference counter */
	CmpackConsole *con;			/**< Output console */

	/* Configuration parameters: */
	int nstar;                  /**< Number of vertices of polygons */
	int maxstar;                /**< Maximum number of stars used in Solve procedure */
	double clip;                /**< Clipping threshold in sigmas */
	CmpackMatchMethod method;	/**< Matching method */
	double maxoffset;			/**< Max. position offset for Simple method */
	int ignore[2];				/**< Object to ignore (reference, source) */
	int setref[2];				/**< Object to match manually (reference, source) */
	int setpos_ref_id;			/**< Object id with custom position */
	double setpos_xy[2];		/**< Object custom posiiton (x, y) */
	
	/* Reference frame: */
	int width1, height1;		/**< Reference frame dimensions in pixels */
	int q1;						/**< Size of allocated buffers i1, x1 and y1 */
	int c1;						/**< No. of stars in reference file */
	int *i1;					/**< Identification of star */
	double *x1, *y1;			/**< Coordinates of stars */
	CmpackWcs *wcs;				/**< Reference WCS data */

	/* Source frame: */
	int width2, height2;		/**< Source frame dimensions in pixels */
	int q2;						/**< Size of allocated buffers i2, xref, x2 and y2 */
	int c2;						/**< No. of stars in input file */
	int *i2;					/**< Identification of star */
	double *x2, *y2;			/**< Coordinates of stars */
	int *xref;					/**< Cross-reference id (0 = not matched) */

	/* Last frame */
	int matched_stars;			/**< Number of stars matched */
	CmpackMatrix matrix;		/**< Frame offset in pixels */

	/* Allocated memory */
	int fft_imsize;				/**< Size of projected image */
	double **fft_n[2];			/**< Two projected images */
	fft_type fft_fk[2];			/**< Results of FFT */
	double **fft_theccf;		/**< Result of CCF */
};

typedef struct _CmpackMatchObject
{
	int	i;						/**< Object's ordinal number */
	double r;					/**< Relative radius */
} CmpackMatchObject;

typedef struct _CmpackMatchFrame
{
	/* Reference frame: */
	int g_c1;					/**< Selection of the stars used for matching */
	double *g_x1, *g_y1;		/**< Coordinates of stars */

	/* Source frame */
	int g_c2;					/**< Selection of the stars used for matching */
	double *g_x2, *g_y2;		/**< Coordinates of stars */
} CmpackMatchFrame;

/* Clear match output */
void match_frame_reset(CmpackMatch *lc);

#endif
