/**************************************************************
*
* cr3.h (C-Munipack project)
* Copyright (C) 2021 David Motl, dmotl@volny.cz
*
* This code is adaptation of the code include in LibRaw version 0.21
*
* I removed everything that is not needed to decode CR3 files and
* I made the code compatible with ANSI C. The memory allocation uses
* the cmpack* routines in order to track memory leaks.
*
* I release this under the same license as the LibRaw, see below.
*
***************************************************************/

/*
Libraw licence

Copyright (C) 2018-2019 Alexey Danilchenko
Copyright (C) 2019 Alex Tutubalin, LibRaw LLC

LibRaw is free software; you can redistribute it and/or modify
it under the terms of the one of two licenses as you choose:

1. GNU LESSER GENERAL PUBLIC LICENSE version 2.1
   (See file LICENSE.LGPL provided in LibRaw distribution archive for details).

2. COMMON DEVELOPMENT AND DISTRIBUTION LICENSE (CDDL) Version 1.0
   (See file LICENSE.CDDL provided in LibRaw distribution archive for details).

 */
#ifndef CRX_H
#define CRX_H

#include <stdio.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C"
{
#endif

/* contents of tag CMP1 for relevant track in CR3 file */
typedef struct
{
    int32_t version;
    int32_t f_width;
    int32_t f_height;
    int32_t tileWidth;
    int32_t tileHeight;
    int32_t nBits;
    int32_t nPlanes;
    int32_t cfaLayout;
    int32_t encType;
    int32_t imageLevels;
    int32_t hasTileCols;
    int32_t hasTileRows;
    uint32_t mdatHdrSize;
} crx_data_header_t;

/* Process CMP1 tag data and fill in the header
*   params:
*      hdr          [out] header 
*      cmp1TagData  [in] tag data
*   returns:
*       0 on success
*       -1 if data does not contain valid crx header
*/
int32_t crxParseImageHeader(crx_data_header_t* hdr, const unsigned char* cmp1TagData);

/* Load CRX data from a file
*  params:
*       hdr         [in/out] header  
*       file        [in] file handle
*       file_size   [in] size of file in bytes
*       data_offset [in] offset of the data block 
*       data_size   [in] size of the data block in bytes
*       raw_image   [out] image data
*   returns:
*       0 on success
*       -1 if data does not contain valid crx header
*/
int32_t crxLoadRaw(crx_data_header_t* hdr, FILE* file, uint32_t file_size, uint32_t data_offset, uint32_t data_size, int16_t* raw_image);

#ifdef __cplusplus
}
#endif

#endif
