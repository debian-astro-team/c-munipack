/**************************************************************

frameset.h (C-Munipack project)
Frame set private header
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_FRAMESET_H
#define CMPACK_FRAMESET_H

#include "cmpack_fset.h"
#include "header.h"

/************************ Data types *******************************/

typedef struct _FrameObjData
{
	int valid;
	CmpackPhtObject data;
} FrameObjData;

typedef struct _FrameRec
{
	CmpackFrameInfo info;		/**< Frame parameters */
	int nstar;					/**< Number of objects */
	int naper;					/**< Number of apertures */
	FrameObjData *objs;			/**< Object properties */
	CmpackPhtData *data;		/**< List of measurements */
	struct _FrameRec *prev;		/**< Pointer to the previous frame */
	struct _FrameRec *next;		/**< Pointer to the next frame */
} FrameRec;

typedef struct _ApertureList
{
	int count, capacity;		/**< Number of items, allocated space */
	CmpackPhtAperture *items;	/**< List of items */
} ApertureList;

typedef struct _ObjectList
{
	int count, capacity;		/**< Number of items, allocated space */
	CmpackCatObject *items;		/**< List of items */
} ObjectList;

typedef struct _FrameList
{
	FrameRec *first;			/**< Pointer to the first frame */
	FrameRec *last;				/**< Pointer to the last frame */
} FrameList;

struct _CmpackFrameSet
{
	int refcnt;					/**< Reference counter */
	CmpackFrameSetInfo info;	/**< Global parameters */
	ApertureList apertures;		/**< List of apertures */
	ObjectList objects;			/**< List of objects */
	FrameList frames;			/**< List of frames */
	FrameRec *current;			/**< Pointer to the active frame */
};

#endif
