/**************************************************************

imglist.h (C-Munipack project)
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef IMGLIST_H
#define IMGLIST_H

/***********************      Datatypes      ******************/

typedef struct _CmpackList
{
	void *ptr;
	struct _CmpackList *next;
} CmpackList;

/******************   Public functions    *********************/

/* Append an item to the end of the list */
CmpackList *list_append(CmpackList *list, void *ptr);

/* Add an item to the beginning of the list */
CmpackList *list_prepend(CmpackList *list, void *ptr);

/* Reverse order of items */
CmpackList *list_reverse(CmpackList *list);

/* Free the list (it does not free the items) */
void list_free(CmpackList *list);

/* Free the list (calls freefn for each item) */
void list_free_with_items(CmpackList *list, void (*freefn)(void *));

/* Get number of items in the list */
int list_count(CmpackList *list);
   
#endif
