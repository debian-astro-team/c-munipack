/**************************************************************

linsolver.h (C-Munipack project)
Linear solver - solving a system of linear equations
Copyright (C) 2016 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef LINSOLVER_H
#define LINSOLVER_H

/* Linear solver based on QR decomposition */
typedef struct _CmpackLinSolver CmpackLinSolver;

/* Create a linear solver based on QR decomposition */
CmpackLinSolver *cmpack_linsolver_qr_create(int n, const double *a);

/* Solve equation Ax = b */
void cmpack_linsolver_solve(CmpackLinSolver *qr, const double *b, double *x);

/* Free allocated memory */
void cmpack_linsolver_destroy(CmpackLinSolver *qr);

#endif
