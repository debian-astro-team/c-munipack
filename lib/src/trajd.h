/**************************************************************

trajd.h (C-Munipack project)
Private common types, definitions, helper functions, ...
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef TRAJD_H
#define TRAJD_H

#include "cmpack_common.h"

/* Convert coordinates from equatorial system to ecliptical one */
void cmpack_rdtolb(double ra, double de, double *la, double *be);

/* Compute ecliptic longitude of the sun and earth-sun distance */
void cmpack_sun(double jd, double *sun_ls, double *sun_rs);

/* Computes Greenwich mean sidereal time in hours */
double cmpack_siderealtime(double jd);

#endif
