/**************************************************************

matread.h (C-Munipack project)
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef MATREAD_H
#define MATREAD_H

#include "cmpack_catfile.h"
#include "cmpack_phtfile.h"
#include "match.h"

/* Read reference file */
void ReadRef(CmpackMatch *lc, CmpackPhtFile *pht);

/* Read catalog file */
void ReadCat(CmpackMatch *lc, CmpackCatFile *pht);

/* Read source file */
void ReadSrc(CmpackMatch *lc, CmpackPhtFile *pht);

#endif
