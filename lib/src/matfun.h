/**************************************************************

matfun.h (C-Munipack project)
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_MATFUN_H
#define CMPACK_MATFUN_H

/* Finds a center of a set of points */
void Center(int n, double *x, double *y, double t[2]);

/* Compute angle between two vectors */
double Uhel(double a[2], double b[2]);

/* Sort the array in ascending order*/
void Serad(double a[3]);

/* Computes u,v lengths of triangle i, j, k */
void UV(int i, int j, int k, double *x, double *y, double *u, double *v);
       
/* Transform (x1,y1) to (x2,y2) by means of m */
void Trafo(double *m, double x1, double y1, double *x2, double *y2);

#endif
