/**************************************************************

phwrite.c (C-Munipack project)
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "config.h"
#include "comfun.h"
#include "console.h"
#include "robmean.h"
#include "cmpack_common.h"
#include "cmpack_phtfile.h"
#include "phot.h"
#include "phfun.h"
#include "phwrite.h"

int SrtWrite(CmpackPhot *kc, CmpackPhotFrame *frame, CmpackPhtFile *pht)
/* Writes the photometry file */
{
	int		i, j, index, obj_mask;
	char	origin[256];
	time_t	tsec;
	CmpackPhtInfo info;
	CmpackPhtData data;
	CmpackPhtObject obj;
	CmpackPhtAperture aper;

	memset(&obj, 0, sizeof(CmpackPhtObject));
	obj.ref_id = -1;

	/* Frame parameters */
	info.width = cmpack_image_width(frame->image);
	info.height = cmpack_image_height(frame->image);
	info.jd = frame->jd;
	info.filter = frame->filter;
	info.exptime = frame->exptime;
	info.ccdtemp = frame->ccdtemp;
	/* Creation software and date */
	sprintf(origin, "%s %s", CMAKE_PROJECT_NAME, VERSION);
	info.origin = origin;
	tsec = time(&tsec);
	info.crtime = *localtime(&tsec);
	/* Photometry process information */
	info.range[0] = frame->datalo;
	info.range[1] = frame->datahi;
	info.gain = kc->gain;
	info.rnoise = kc->readns;
	info.fwhm_exp = kc->fwhm;
	info.fwhm_mean = frame->fwhm_med;
	info.fwhm_err = frame->fwhm_err;
	info.threshold = kc->thresh;
	info.sharpness[0] = kc->shrplo;
	info.sharpness[1] = kc->shrphi;
	info.roundness[0] = kc->rndlo;
	info.roundness[1] = kc->rndhi;
	info.matched = 0;
	cmpack_pht_set_info(pht, CMPACK_PI_FRAME_PARAMS | CMPACK_PI_ORIGIN_CRDATE | 
		CMPACK_PI_PHOT_PARAMS, &info);

	/* Apertures */
	for (i=0; i<frame->naper; i++) {
		aper.id = frame->apid[i];
		aper.radius = frame->aper[i];
		cmpack_pht_add_aperture(pht, CMPACK_PA_ID | CMPACK_PA_RADIUS, &aper);
	}

	obj_mask = CMPACK_PO_ID | CMPACK_PO_CENTER | CMPACK_PO_SKY | CMPACK_PO_FWHM;

	/* If we use user-defined objects, set also a flag that the frames are matched */
	if (kc->useobjectlist) {
		info.matched = 1;
		info.match_mstars = frame->nstar;
		info.match_clip = 0;
		info.match_istars = 0;
		info.match_rstars = 0;
		cmpack_matrix_identity(&info.trafo);
		info.offset[0] = info.offset[1] = 0;
		cmpack_pht_set_info(pht, CMPACK_PI_MATCH_PARAMS, &info);
		obj_mask |= CMPACK_PO_REF_ID;
	}

	/* Star data */
	for (j=0; j<frame->nstar; j++) {
		CmpackPhotStar *st = frame->list[j];

		obj.id = j+1;
		obj.x = st->xcen;
		obj.y = st->ycen;
		obj.skymed = st->skymod;
		obj.skysig = st->skysig;
		obj.fwhm   = st->fwhm;
		obj.ref_id = obj.id;
		index = cmpack_pht_add_object(pht, obj_mask, &obj);
		if (index>=0) {
			/* Parameters */
			/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			cmpack_pht_pstd(pht, star, "skymod", st->skymod, 3);
			cmpack_pht_pstd(pht, star, "skysig", st->skysig, 3);
			cmpack_pht_pstd(pht, star, "sharpness", st->sharpness, 3);
			cmpack_pht_pstd(pht, star, "roundness", st->roundness, 3);
			cmpack_pht_pstd(pht, star, "fwhm", st->fwhm, 3);
			cmpack_pht_pstd(pht, star, "height", st->height, 3);
			!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
			/* Magnitudes */
			for (i=0; i<frame->naper; i++) {
				data.mag_valid = st->apmag[i] < 99;
				data.magnitude = st->apmag[i];
				data.mag_error = st->magerr[i];
				cmpack_pht_set_data_with_code(pht, index, i, &data, st->code[i]);
			}
		}
	}
	return 0;
}
