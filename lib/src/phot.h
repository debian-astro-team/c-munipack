/**************************************************************

phot.h (C-Munipack project)
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef PHOT_H
#define PHOT_H

#include "console.h"
#include "cmpack_phot.h"

/* Detected star parameters: */
typedef struct _CmpackPhotStar
{
  int    xmax, ymax;					/* Coordinates of max. brightness pixel */
  double xcen, ycen;           			/* Coordinates of centriod */
  double height;               			/* Max. brightness value */
  double signal;						/* Total sum of brightness */
  double roundness, sharpness; 			/* Shape indices */
  double skymod, skysig, skyskw; 		/* Sky parameters */
  double apmag[MAXAP], magerr[MAXAP];	/* Magnitudes and std. deviations */
  CmpackError code[MAXAP];				/* Code indicating a reason why the brightness was not measured */
  double fwhm;							/* FWHM in pixels */
} CmpackPhotStar;

/* Photometry frame context */
typedef struct _CmpackPhotFrame
{
	double 	jd;						/**< Julian date (center of observation) */
	CmpackBorder border;			/**< Border size */
	CmpackImage *image;				/**< Frame image data */
	double exptime;					/**< Exposure duration in seconds */
	char *object;					/**< Object's designation */
	char *filter;					/**< Optical filter name */
	double datalo;					/**< Low good datum in ADU */
	double datahi;					/**< High good datum in ADU */
	int fravg, frsum;				/**< Number of frames averaged, summed */
	double ccdtemp;					/**< CCD temperture in deg. C */
	double noise;					/**< Readout noise */
	double phpadu;					/**< ADC gain (photons per ADU) */
	double relerr;					/**< ??? */
	double skymod, skysig;			/**< Sky mean value, standard deviation */
	double maglim, magsq;			/**< ??? */
	double fwhm_med, fwhm_err;		/**< Mean FWHM of objects, standard deviation */
	double aper[MAXAP];				/**< Selected apertures (radii) */
	int apid[MAXAP];				/**< Selected apertures (indices) */
	int naper;						/**< Number of apertures used */
	CmpackPhotStar **list;			/**< List of detected objects */
	int length;						/**< Number of allocated stars */
	int nstar;						/**< Number of stars accepted (see lc->maxstar) */
	int find_processed;				/**< Set to nonzero in the Find routine */
	int current_pos;				/**< Index of the current object (for enumeration) */

	int nhalf;						/**< Halfwidth of Gaussian filter */
	int left, ncol, top, nrow;		/**< Position and size of valid data on a source frame */
	double hmin;					/**< Minimum height */
	double sigsq;
	double *g, *h;					/**< Image data filtered, filter */
	char *skip;						/**< Filter mask */
} CmpackPhotFrame;

/* Photometry configuration context */
struct _CmpackPhot
{
	int refcnt;						/**< Reference counter */
	CmpackConsole *con;				/**< Console */
	double readns;					/**< Read noise in ADU per frame */
	double gain;					/**< Gain in e-/ADU */
	double datalo;					/**< Low good datum in ADU */
	double datahi;					/**< High good datum in ADU */
	double fwhm;					/**< FWHM od object */
	double thresh;					/**< Threshold in sigmas */
	double shrplo;					/**< Low sharpness cutoff */
	double shrphi;					/**< High sharpness cutoff */
	double rndlo;					/**< Low roundness cutoff */
	double rndhi;					/**< High roundness cutoff */
	CmpackBorder border;			/**< Border size */
	double ap[MAXAP];				/**< Available apertures */
	double is;						/**< Inner sky radius */
	double os;						/**< Outer sky radius */
	int maxstar;					/**< Max. number of objects */
	int useobjectlist;				/**< Use user-defined list of objects */
	CmpackPhotFrame frame;			/**< Frame data */
	double *usr_coords;				/**< User-defined objects, X and Y coordinates */
	int usr_length, usr_alloc;		/**< Number of objects, allocated space */
};

typedef struct _CmpackPhotList
{
	CmpackPhotStar *data;
	struct _CmpackPhotList *next;
} CmpackPhotList;

#define D(x,y)    d[(x)+((y)*rowwidth)]
#define G(x,y)    g[(x)+((y)*nbox)]
#define H(x,y)    h[(x)+((y)*ncol)]
#define SKIP(x,y) skip[(x)+((y)*nbox)]

#endif
