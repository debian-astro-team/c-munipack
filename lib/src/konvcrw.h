/**************************************************************

konvraw.h (C-Munipack project)
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef KONVCRW_H
#define KONVCRW_H

#include "cmpack_common.h"
#include "cmpack_console.h"
#include "ccdfile.h"

/* Returns 1 if the file is of FITS format */
int crw_test(const char *data, size_t data_length, size_t filesize);

/* Open a file */
int crw_open(tHandle *handle, const char *filename, CmpackOpenMode mode, unsigned flags);

/* Close file and free allocated memory */
void crw_close(tHandle handle);

/* Get magic string */
int crw_getsize(tHandle handle, int *width, int *height);

/* Get magic string */
CmpackBitpix crw_getbitpix(tHandle handle);

/* Get magic string */
int crw_getrange(tHandle handle, double *minvalue, double *maxvalue);

/* Get pointer to image data */
int crw_getimage(tHandle handle, void *buf, int bufsize, CmpackChannel channel);

/* Get magic string */
char *crw_getmagic(tHandle handle);

/* Get date and time of observation */
int crw_getdatetime(tHandle handle, CmpackDateTime *dt);

/* Get exposure duration */
int crw_getexptime(tHandle handle, double *val);

/* Get sensor temperature */
int crw_getccdtemp(tHandle handle, double *val);

/* Get optical filter name */
char *crw_getfilter(tHandle handle, CmpackChannel channel);

/* Get number of combined frames */
void crw_getframes(tHandle handle, CmpackChannel channel, int *avg_frames, int *sum_frames);

/* Add string to history */
int crw_copyheader(tHandle handle, CmpackImageHeader *out, CmpackChannel channel, CmpackConsole *con);

#endif
