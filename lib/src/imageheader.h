#ifndef IMAGE_HEADER_H
#define IMAGE_HEADER_H

#include "cmpack_common.h"

typedef struct _CmpackImageHeader
{
	void *fits;
	int status;
	void *buffer;
	size_t bufsize;
} CmpackImageHeader;

void cmpack_image_header_init(CmpackImageHeader *hdr);

void cmpack_image_header_destroy(CmpackImageHeader *hdr);

void cmpack_image_header_copy(const CmpackImageHeader *src, CmpackImageHeader *dst);

#endif
