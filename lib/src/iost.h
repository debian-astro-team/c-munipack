/**************************************************************

iost.h (C-Munipack project)
Reading SBIG files
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef IOST_H
#define IOST_H

/* Header length of the SBIG image file in bytes */
#define STHEADSIZE   2048             

/* Opaque structure used to access data in SBIG file */
typedef struct _stfile stfile;

/* Opens a file and reads the header */
int stopen(stfile **handle, const char *filename);

/* Frees allocated memory in stfile */
void stclos(stfile *handle);
     
/* Return parameter value (string) */
int stgkys(stfile *handle, const char *key, char **value);

/* Return parameter value (integer) */
int stgkyi(stfile *handle, const char *key, int *value);

/* Return parameter value (double) */
int stgkyd(stfile *handle, const char *key, double *value);
     
/* Get image data */
int stgimg(stfile *handle, uint16_t *buf, int bufsize);

/* Parameter enumeration */
int stgkyn(stfile *handle, int nkey, char **key, char **val);
    
#endif
