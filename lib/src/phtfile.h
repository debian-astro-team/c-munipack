/**************************************************************

file.h (C-Munipack project)
Photometry file private header file
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef PHOT_FILE_H
#define PHOT_FILE_H

#include "cmpack_phtfile.h"
#include "cmpack_wcs.h"

/* Table of apertures */
typedef struct _CmpackApertures
{
	int	count;					/**< Number of apertures */
	int	capacity;				/**< Number of allocated records */
	CmpackPhtAperture *list;	/**< List of apertures */
} CmpackApertures;

/* Table of measurements */
typedef struct _CmpackMagData
{
	int valid;					/**< Measurement is defined */
	CmpackError code;			/**< Error indicating a reson why the measurement is not valid */
	double mag;					/**< Instrumental brightness in magnitudes */
	double err;					/**< Error estimation of the instrumental brightness */
} CmpackMagData;

/* Table of measurements */
typedef struct _CmpackMagList
{
	int size;
	CmpackMagData *items;		/**< Table of measurements */
} CmpackMagList;

/* Star info */
typedef struct _CmpackObject
{
	CmpackPhtObject info;		/**< Object's parameters */
	CmpackMagList data;			/**< Allocated space for measurements */
} CmpackObject;

/* Table of stars */
typedef struct _CmpackObjects
{
	int count;					/**< Number of stars */
	int capacity;				/**< Number of allocated records */
	CmpackObject *list;			/**< List of stars */
} CmpackObjects;

/* Format identifiers */
typedef enum _CmpackPhtFormat
{
	CMPACK_PHT_DEFAULT,
	CMPACK_PHT_XML,
	CMPACK_PHT_DAOPHOT
} CmpackPhtFormat;

/* Photometry file context */
struct _CmpackPhtFile 
{
	int				refcnt;		/**< Reference counter */
	FILE			*f;			/**< Associated resource */
	unsigned		flags;		/**< Read/write flags */
	int				readonly;	/**< Nonzero if changes are not allowed */
	int				changed;	/**< Nonzero if changes were made */
	int				revision;	/**< File format revision number */
	int				delayload;	/**< Nonzero = measurements are loaded when they are needed */
	int				ap_loaded;	/**< Number of apertures that were loaded into memory */
	int				st_loaded;	/**< Number of stars that were loaded into memory */
	int				dt_loaded;	/**< Number of objects that were loaded into memory */
	long			ap_offset;	/**< Start position in bytes of aperture data within a file */
	long			st_offset;	/**< Start position in bytes of star data within a file */
	long			dt_offset;	/**< Start position in bytes of measurements within a file */
	long			wcs_offset;	/**< Start position in bytes of WCS data within a file */
	int				wcs_length;	/**< Size of WCS data in bytes */
	CmpackPhtFormat	format;		/**< Format of original file */
	CmpackPhtInfo	hd;			/**< Photometry file metadata */
	CmpackApertures ap;			/**< Table of apertures */
	CmpackObjects	st;			/**< Table of objects */
	CmpackWcs		*wcs;		/**< World Coordinate System data */
};

#endif
