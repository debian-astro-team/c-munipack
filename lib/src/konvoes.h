/**************************************************************

konvoes.h (C-Munipack project)
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef KONVOES_H
#define KONVOES_H

#include "cmpack_common.h"
#include "cmpack_console.h"
#include "ccdfile.h"

/* Returns 1 if the file is of FITS format */
int oes_test(const char *data, size_t data_length, size_t filesize);

/* Open a file */
int oes_open(tHandle *handle, const char *filename, CmpackOpenMode mode, unsigned flags);

/* Close file and free allocated memory */
void oes_close(tHandle handle);

/* Reads string value from header */
int oes_gets(tHandle handle, const char *key, char **val);

/* Reads integer value from header */
int oes_geti(tHandle handle, const char *key, int *val);

/* Reads floating point value from header */
int oes_getd(tHandle handle, const char *key, double *val);

/* Random access reading of (key, value, comment) triples from hedader */
int oes_gkyn(tHandle handle, int index, char **key, char **val, char **com);

/* Get magic string */
int oes_getsize(tHandle handle, int *width, int *height);

/* Get magic string */
CmpackBitpix oes_getbitpix(tHandle handle);

/* Get magic string */
int oes_getrange(tHandle handle, double *minvalue, double *maxvalue);

/* Reads image data to given buffer */
int oes_getimage(tHandle handle, void *buf, int bufsize, CmpackChannel channel);

/* Get magic string */
char *oes_getmagic(tHandle handle);

/* Get date and time of observation */
int oes_getdatetime(tHandle handle, CmpackDateTime *dt);

/* Get exposure duration */
int oes_getexptime(tHandle handle, double *val);

/* Get CCD temperature */
int oes_getccdtemp(tHandle handle, double *val);

/* Add string to history */
int oes_copyheader(tHandle handle, CmpackImageHeader *out, CmpackChannel channel, CmpackConsole *con);

#endif
