/**************************************************************

table.h (C-Munipack project)
Tables -- private header file
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_TABLE_H
#define CMPACK_TABLE_H

#include "header.h"

/* Table cell data */
typedef union _TabValue
{
	char *sValue;
	int iValue;
	double dValue;
} TabValue;

/* Table cell data */
typedef struct _TabCell
{
	int assigned;
	TabValue data;
} TabCell;

/* Table column context */
typedef struct _TabColumn
{
	char *name;				/**< Name of the column */
	CmpackType dtype;			/**< Type of stored values */
	int prec;					/**< Number of decimal places */
	int iNulVal;				/**< Undefined value */
	int iMin;					/**< Lower limit for stored values */
	int iMax;					/**< Upper limit for stored values */
	double dNulVal;				/**< Undefined value */
	double dMin;				/**< Lower limit for stored	values */
	double dMax;				/**< Upper limit for stored values */
	int needs_update;			/**< Limits needs updating */
	int valid_range;			/**< Value limits are valid */
	double min, max;			/**< Limits */
} TabColumn;

/* Table columns */
typedef struct _TabColumns
{
	int				count;		/**< Number of columns */
	int				capacity;	/**< Allocated size */
	TabColumn		*list;		/**< List of columns */
} TabColumns;

typedef struct _TabRecord
{
	int				ndata;		/**< Number of stored values */
	TabCell			*data;		/**< Table data */
	struct _TabRecord *next;	/**< Pointer to the next record */
	struct _TabRecord *prev;	/**< Pointer to the previous record */
} TabRecord;

/* Table */
struct _CmpackTable
{
	int				refcnt;		/**< Reference counter */
	CmpackTableType	type;		/**< Type */
	CmpackHeader	head;		/**< Header */
	TabColumns		cols;		/**< Columns */
	TabRecord		*first;		/**< Pointer to the first record */
	TabRecord		*last;		/**< Pointer to the next record */
	TabRecord		*current;	/**< Pointer to active record */
};

#endif
