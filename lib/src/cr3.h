/**************************************************************
*
* cr3.h (C-Munipack project)
* Copyright (C) 2021 David Motl, dmotl@volny.cz
*
* This code is adaptation of the code include in LibRaw version 0.21
*
* I removed everything that is not needed to decode CR3 files and
* I made the code compatible with ANSI C. The memory allocation uses
* the cmpack* routines in order to track memory leaks.
*
* I release this under the same license as the LibRaw, see below.
*
***************************************************************/

/*
Libraw licence
 
Copyright (C) 2018-2019 Alexey Danilchenko
Copyright (C) 2019 Alex Tutubalin, LibRaw LLC

LibRaw is free software; you can redistribute it and/or modify
it under the terms of the one of two licenses as you choose:

1. GNU LESSER GENERAL PUBLIC LICENSE version 2.1
   (See file LICENSE.LGPL provided in LibRaw distribution archive for details).

2. COMMON DEVELOPMENT AND DISTRIBUTION LICENSE (CDDL) Version 1.0
   (See file LICENSE.CDDL provided in LibRaw distribution archive for details).

 */

#ifndef CR3_H
#define CR3_H

#include <stdio.h>
#include <inttypes.h>

#ifdef __cplusplus
extern "C"
{
#endif

enum cr3_errors
{
    CR3_SUCCESS = 0,
    CR3_IO_ERROR = -1,
    CR3_FILE_UNSUPPORTED = -2,
    CR3_TOO_BIG = -3,
    CR3_FILE_CLOSED = -4,
};

typedef struct
{
    char versionID[4];         /* VersionID */
    unsigned latitude[3][2];     /* Deg,min,sec */
    unsigned longitude[3][2];    /* Deg,min,sec */
    unsigned altitude[2];        /* Meters */
    char  altref; /* 0 = Above sea level, 1 or negative number - below sea level */
    char latref;  /* 'N' = North, 'S' = South or positive number - north, negative number - south */
    char longref; /* 'E' = East, 'W' = West or positive number - east, negative number - west */
    char gpsstatus; /* 'A' = Active, 'V' = Void */
    char  gpsfound; /* 0 = not found, 1 = found */
} gps_info_t;

#define MAKE_SIZE 64
#define MODEL_SIZE 64
#define TIMESTAMP_SIZE 20

typedef struct
{
    unsigned short raw_height, raw_width, height, width, top_margin, left_margin;
    unsigned short iheight, iwidth;

    char make[MAKE_SIZE];
    char model[MODEL_SIZE];
    int colors;
    unsigned filters;

    short SensorWidth;
    short SensorHeight;
    short SensorLeftBorder;
    short SensorTopBorder;
    short SensorRightBorder;
    short SensorBottomBorder;

    unsigned shutter[2];
    char timestamp[TIMESTAMP_SIZE];

    gps_info_t parsed_gps;

    /* really allocated bitmap */
    unsigned short* raw_alloc;

    unsigned short(*image)[4];
} cr3_data_t;

typedef struct cr3_internal_data_t* cr3_internal_data_p;

typedef struct
{
    cr3_data_t imgdata;
    cr3_internal_data_p intdata;
} cr3_t;

void cr3_init(cr3_t* data);
void cr3_free(cr3_t* data);

int cr3_open(cr3_t* data, FILE *);
int cr3_unpack(cr3_t* data);
int cr3_process(cr3_t* data);
int cr3_save_color(cr3_t* data, int color, FILE* file);
int cr3_save_asrgb(cr3_t* data, FILE* file);

#ifdef __cplusplus
}
#endif

#endif
