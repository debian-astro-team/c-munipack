/**************************************************************

matread.c (C-Munipack project)
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "comfun.h"
#include "cmpack_common.h"
#include "cmpack_phtfile.h"
#include "cmpack_catfile.h"
#include "matread.h"

#define ALLOC_UNIT 4096
#define ALLOC_SIZE(n,b) ((((n)+((b)-1))/(b))*(b))

void ReadRef(CmpackMatch *lc, CmpackPhtFile *pht)
{
	int	i, j, count;
	CmpackPhtInfo info;
	CmpackPhtObject obj;
	CmpackWcs *wcs;

	lc->c1 = lc->width1 = lc->height1 = 0;

	if (cmpack_pht_get_info(pht, CMPACK_PI_FRAME_PARAMS, &info)==0) {
		lc->width1 = info.width;
		lc->height1 = info.height;
	}

	count = cmpack_pht_object_count(pht);
	if (count > lc->q1) {
		lc->q1 = ALLOC_SIZE(count, ALLOC_UNIT);
		cmpack_free(lc->i1);
		lc->i1 = (int*) cmpack_malloc(lc->q1*sizeof(int));
		cmpack_free(lc->x1);
		lc->x1 = (double*) cmpack_malloc(lc->q1*sizeof(double));
		cmpack_free(lc->y1);
		lc->y1 = (double*) cmpack_malloc(lc->q1*sizeof(double));
	}
	if (lc->x1 && lc->y1 && lc->i1) {
		for (i=0, j=0; i<count; i++) {
			if (cmpack_pht_get_object(pht, i, CMPACK_PO_ID | CMPACK_PO_CENTER, &obj)==0) {
				lc->i1[j] = obj.id;
				lc->x1[j] = obj.x;
				lc->y1[j] = obj.y;
				j++;
			}
		}
		lc->c1 = j;
	}

	/* Read WCS data */
	if (lc->wcs) {
		cmpack_wcs_destroy(lc->wcs);
		lc->wcs = NULL;
	}
	if (cmpack_pht_get_wcs(pht, &wcs)==0)
		lc->wcs = cmpack_wcs_reference(wcs);
}

void ReadCat(CmpackMatch *lc, CmpackCatFile *cat)
{
	int	i, j, count;
	CmpackCatObject obj;
	CmpackWcs *wcs;

	lc->c1 = 0;

	lc->width1 = cmpack_cat_get_width(cat);
	lc->height1 = cmpack_cat_get_height(cat);

	/* Read objects */
	count = cmpack_cat_nstar(cat);
	if (count > lc->q1) {
		lc->q1 = ALLOC_SIZE(count, ALLOC_UNIT);
		cmpack_free(lc->i1);
		lc->i1 = (int*) cmpack_malloc(lc->q1*sizeof(int));
		cmpack_free(lc->x1);
		lc->x1 = (double*) cmpack_malloc(lc->q1*sizeof(double));
		cmpack_free(lc->y1);
		lc->y1 = (double*) cmpack_malloc(lc->q1*sizeof(double));
	}
	if (lc->x1 && lc->y1 && lc->i1) {
		for (i=0, j=0; i<count; i++) {
			if (cmpack_cat_get_star(cat, i, CMPACK_OM_ID | CMPACK_OM_CENTER, &obj)==0) {
				lc->i1[j] = obj.id;
				lc->x1[j] = obj.center_x;
				lc->y1[j] = obj.center_y;
				j++;
			}
		}
		lc->c1 = j;
	}

	/* Read WCS data */
	if (lc->wcs) {
		cmpack_wcs_destroy(lc->wcs);
		lc->wcs = NULL;
	}
	cmpack_cat_get_wcs(cat, &wcs);
	if (wcs)
		lc->wcs = cmpack_wcs_reference(wcs);
}

void ReadSrc(CmpackMatch *lc, CmpackPhtFile *pht)
{
	int i, j, count;
	CmpackPhtInfo info;
	CmpackPhtObject obj;

	lc->c2 = lc->width2 = lc->height2 = 0;

	if (cmpack_pht_get_info(pht, CMPACK_PI_FRAME_PARAMS, &info)==0) {
		lc->width2 = info.width;
		lc->height2 = info.height;
	}

	count = cmpack_pht_object_count(pht);
	if (count > lc->q2) {
		lc->q2 = ALLOC_SIZE(count, ALLOC_UNIT);
		cmpack_free(lc->i2);
		lc->i2 = (int*) cmpack_malloc(lc->q2*sizeof(int));
		cmpack_free(lc->x2);
		lc->x2 = (double*) cmpack_malloc(lc->q2*sizeof(double));
		cmpack_free(lc->y2);
		lc->y2 = (double*) cmpack_malloc(lc->q2*sizeof(double));
		cmpack_free(lc->xref);
		lc->xref = (int*) cmpack_malloc(lc->q2*sizeof(int));
	}
	if (lc->x2 && lc->y2 && lc->i2 && lc->xref) {
		for (i=0, j=0; i<count; i++) {
			if (cmpack_pht_get_object(pht, i, CMPACK_PO_ID | CMPACK_PO_CENTER, &obj)==0) {
				lc->i2[j] = obj.id;
				lc->x2[j] = obj.x;
				lc->y2[j] = obj.y;
				j++;
			}
		}
		lc->c2 = j;
	}
}
