/*
 * fft_FFTPACK.h
 */
#ifndef CMPACK_FFT_H
#define CMPACK_FFT_H

#include "config.h"

typedef struct { double r, i; } complex;

typedef		complex** fft_type;

#ifdef CMUNIPACK_USE_INTERNAL_FFTPACK

void	alloc_fft(fft_type *fk, int Nx, int Ny);
void	free_fft(fft_type fk, int Nx, int Ny);
void	forward_fft(double **f, int nx, int ny, fft_type fk);
void	inverse_fft(fft_type fk, int nx, int ny, double **f);

void	ccf(fft_type fk1, fft_type fk2, int Nx, int Ny, double **ff12, int x0, int y0);

#else 
#ifdef CMUNIPACK_USE_FFTW



#endif
#endif

#endif
