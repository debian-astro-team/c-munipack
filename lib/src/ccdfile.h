/**************************************************************

konve.c )(C-Munipack project)
Copyright )(C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or )(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CCDFILE_H
#define CCDFILE_H

#include "cmpack_ccdfile.h"
#include "cmpack_console.h"
#include "imageheader.h"

typedef void *tHandle;

/* Set frame size in pixels */
int ccd_prepare(CmpackCcdFile *fc, int width, int height, CmpackBitpix bitpix);

/* Set image data - given buffer must correspond to specified image data format */
int ccd_write_image(CmpackCcdFile *ctx, const CmpackImage *image);

/* Write package name and version to file header */
void ccd_set_origin(CmpackCcdFile *fc);

/* Write date of creation */
void ccd_set_pcdate(CmpackCcdFile *fc);

/* Save image header */
int ccd_save_header(CmpackCcdFile *file, CmpackImageHeader *hdr, CmpackConsole *con);

/* Restore image header */
int ccd_restore_header(CmpackCcdFile *file, CmpackImageHeader *hdr, CmpackConsole *con);

/* Copy header 
	wcs = 0: Copy WCS data (default)
	    = 1: Do not copy WCS data
		= 2: Copy WCS data only
*/
int ccd_copy_header(CmpackCcdFile *dstfile, CmpackCcdFile *srcfile, CmpackConsole *con, int wcs);

/* Append text to the history */
void ccd_update_history(CmpackCcdFile *ctx, const char *text);

/* Set a logical value to a file header */
void ccd_set_bool(CmpackCcdFile *fc, const char *key, int val, const char *comment);

/* Set a logical value to a file header */
void ccd_set_int(CmpackCcdFile *fc, const char *key, int val, const char *comment);

/* Set a logical value to a file header */
void ccd_set_str(CmpackCcdFile *fc, const char *key, const char *val, const char *comment);

/* Set a real value to a file header */
void ccd_set_dbl(CmpackCcdFile *fc, const char *key, double val, int prec, const char *comment);

#endif
