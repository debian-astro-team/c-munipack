/**************************************************************

tcurve.c (C-Munipack project)
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "comfun.h"
#include "console.h"
#include "cmpack_common.h"
#include "cmpack_tcurve.h"

/********************   PUBLIC FUNCTIONS   **************************/

/* Open output table, start reading sequence */
int cmpack_tcurve(CmpackFrameSet *fset, CmpackTable **ptable, CmpackTCurveFlags flags,
	CmpackConsole *con)
{	
	int cols = CMPACK_FC_JULDAT | CMPACK_FC_OFFSET;
	
	if (flags & CMPACK_TCURVE_FRAME_IDS)
		cols |= CMPACK_FC_OFFSET;

	return cmpack_fset_plot(fset, ptable, CMPACK_TABLE_TRACKLIST, (CmpackFSetColumns)cols, 
		0, 0, 0, 0, 0, 0, 0, 0, con);
}
