/**************************************************************

konvfits.h (C-Munipack project)
FITS frames manipulating functions
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef KONVFITS_H
#define KONVFITS_H

#include "cmpack_common.h"
#include "cmpack_console.h"
#include "ccdfile.h"

/* Returns 1 if the file is of FITS format */
int fits_test(const char *data, size_t data_length, size_t filesize);

/* Open a file */
int fits_open(tHandle *handle, const char *filename, CmpackOpenMode mode, unsigned flags);

/* Create a memory file */
tHandle fits_init(void);

/* Close file and free allocated memory */
void fits_close(tHandle handle);

/* Reads string value from header */
int fits_gkys(tHandle handle, const char *key, char **buf);

/* Reads integer value from header */
int fits_gkyi(tHandle handle, const char *key, int *val);

/* Reads logic value from header */
int fits_gkyl(tHandle handle, const char *key, int *val);

/* Reads real value from header */
int fits_gkyd(tHandle handle, const char *key, double *val);

/* Random access reading of (key, value, comment) triples from hedader */
int fits_gkyn(tHandle handle, int index, char **key, char **val, char **com);

/* Get image dimensions in pixels */
int fits_getsize(tHandle handle, int *width, int *height);

/* Get native image data format */
CmpackBitpix fits_getbitpix(tHandle handle);

/* Is this standard FITS format? */
int fits_working_format(tHandle handle);

/* Get special pixel values */
int fits_getrange(tHandle handle, double *minvalue, double *maxvalue);

/* Reads image data to given buffer */
int fits_getimage(tHandle handle, void *data, int buflen, CmpackChannel channel);

/* Get file format name */
char *fits_getmagic(tHandle handle);

/* Get date and time of observation */
int fits_getdatetime(tHandle handle, CmpackDateTime *dt);

/* Get exposure duration */
int fits_getexptime(tHandle handle, double *val);

/* Get CCD temperature */
int fits_getccdtemp(tHandle handle, double *val);

/* Get number of frames averaged or combined */
void fits_getframes(tHandle handle, CmpackChannel channel, int *avg_frames, int *sum_frames);

/* Get optical filter name */
char *fits_getfilter(tHandle handle, CmpackChannel channel);

/* Get observer's name */
char *fits_getobserver(tHandle handle);

/* Get observatory name */
char *fits_getobservatory(tHandle handle);

/* Get telescope designation */
char *fits_gettelescope(tHandle handle);

/* Get instrument (camera) designation */
char *fits_getinstrument(tHandle handle);

/* Get observer's longitude */
char *fits_getobslon(tHandle handle);

/* Get observer's latitude */
char *fits_getobslat(tHandle handle);

/* Get object's designation */
char *fits_getobject(tHandle handle);

/* Get object's right ascension */
char *fits_getobjra(tHandle handle);

/* Get object's declination */
char *fits_getobjdec(tHandle handle);

/* Get WCS data */
int fits_getwcs(tHandle handle, CmpackWcs **wcs);

/* Add string to history */
int fits_copyheader(tHandle handle, CmpackImageHeader *out, CmpackChannel channel, CmpackConsole *con);

/* Add string to history */
int fits_restoreheader(tHandle handle, CmpackImageHeader *out, CmpackConsole *con);

/* Add string to history */
int fits_prepare(tHandle handle, int width, int height, CmpackBitpix bitpix);

/* Add string to history */
int fits_putimage(tHandle handle, const CmpackImage *data);

/* Add string to history */
int fits_puthistory(tHandle handle, const char *text);

/* Set date of observation */
int fits_setdatetime(tHandle handle, const CmpackDateTime *dt);

/* Set exposure duration */
void fits_setexptime(tHandle handle, double exptime);

/* Set CCD temperature */
void fits_setccdtemp(tHandle handle, double ccdtemp);

/* Set filter name */
void fits_setfilter(tHandle handle, const char *filter);

/* Set object's name */
void fits_setobject(tHandle handle, const char *object);

/* Set number of frames averaged */
void fits_setframes(tHandle handle, int avg_frames, int sum_frames);

/* Reads string value from header */
void fits_pkys(tHandle handle, const char *key, const char *value, const char *comment);

/* Reads integer value from header */
void fits_pkyi(tHandle handle, const char *key, int value, const char *comment);

/* Reads floating point value from header */
void fits_pkyl(tHandle handle, const char *key, int value, const char *comment);

/* Reads floating point value from header */
void fits_pkyd(tHandle handle, const char *key, double value, int prec, const char *comment);

/* Convert string to longiture */
int fits_strtolon(tHandle handle, const char* str, double* value);

#endif
