/**************************************************************

phot_head.h (C-Munipack project)
Photometry file header
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_HEADER_H
#define CMPACK_HEADER_H

#include "hash.h"
#include "cmpack_console.h"
#include "xmldom.h"

/***********************      Datatypes      ******************/

/* Single header item */
struct _CmpackHeadItem
{
	char *key;						/**< Parameter name (case sensitive) */
	char *val;						/**< Parameter value */
	char *com;						/**< Comment text */
};
typedef struct _CmpackHeadItem CmpackHeadItem;
	
/* File header */
struct _CmpackHeader
{
	int	count;						/**< Number of sections */
	int	capacity;					/**< Number of allocated records */
	CmpackHeadItem **list;			/**< List of section items */
	CmpackHashTable hash;			/**< Hash table of items */
};
typedef struct _CmpackHeader CmpackHeader;

/******************   Public functions    *********************/

/* Init file header */
void header_init(CmpackHeader *head);
	
/* Clear header content */
void header_clear(CmpackHeader *head);
	
/* Copy header content */
void header_copy(CmpackHeader *dsthead, const CmpackHeader *srchead);
	
/* Dump content of header */
void header_dump(CmpackConsole *ctx, CmpackHeader *head);

/* Load header from XML file */
void header_load_xml(CmpackHeader *head, CmpackElement *parent);

/* Write header into XML file */
void header_write_xml(CmpackHeader *head, FILE *to);

/* Trim all leading and trailing whitespaces in keys, values and comments */
void header_normalize(CmpackHeader *head);

/* Find item by keyword, return pointer to an item */
CmpackHeadItem *header_finditem(CmpackHeader *head, const char *key);

/* Find item by keyword, return index of an item */
int header_find(CmpackHeader *head, const char *key);

/* Get pointer to header item */
int header_add(CmpackHeader *head, const char *key);

/* Delete parameter from a section */
void header_delete(CmpackHeader *head, int index);

/* Set parameter value in file header (string) */
void header_pkys(CmpackHeader *head, const char *key, const char *val, const char *com);

/* Set parameter value in file header (integer) */
void header_pkyi(CmpackHeader *head, const char *key, int val, const char *com);

/* Set parameter value in file header (real number - exponential format "e") */
void header_pkye(CmpackHeader *head, const char *key, double val, int prec, const char *com);

/* Set parameter value in file header (real number - fixed format "f") */
void header_pkyf(CmpackHeader *head, const char *key, double val, int prec, const char *com);

/* Set parameter value in file header (real number - shorter of "e" or "f") */
void header_pkyg(CmpackHeader *head, const char *key, double val, int prec, const char *com);

/* Get value of parameter in file header (string) */
const char *header_gkys(CmpackHeader *head, const char *key);

/* Get value of parameter in file header (integer number) */
int header_gkyi(CmpackHeader *head, const char *key, int *value);

/* Get value of parameter in file header (real number) */
int header_gkyd(CmpackHeader *head, const char *key, double *value);

/* Get comment text of parameter in file header */
const char *header_gkyc(CmpackHeader *head, const char *key);

/* Get the parameter from header section by its index. */
int header_gkyn(CmpackHeader *head, int index, const char **key, const char **val, const char **com);

/** Delete parameter from header section */
void header_dkey(CmpackHeader *head, const char *key);

/** Add a string to the history */
void header_phis(CmpackHeader *head, const char *str);

/* Set parameter value/comment */
void headitem_setval(CmpackHeadItem *item, const char *val);
void headitem_setcom(CmpackHeadItem *item, const char *com);

#endif
