/**************************************************************

cat_file.h (C-Munipack project)
Catalog files - private header file
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_CAT_FILE_H
#define CMPACK_CAT_FILE_H

#include "cmpack_catfile.h"
#include "header.h"

/* Table of stars */
typedef struct _CmpackObjTab
{
	int nx, ny;					/**< Chart size */
	int count;					/**< Number of stars */
	int capacity;				/**< Number of allocated records */
	CmpackCatObject *list;		/**< List of stars */
} CmpackObjTab;

/* Selection record */
typedef struct _CmpackSelectionItem
{
	int star_id;
	CmpackSelectionType type;
	struct _CmpackSelectionItem *next;		
} CmpackSelectionItem;

typedef struct _CmpackSelectionList
{
	CmpackSelectionItem *first, *last;
} CmpackSelectionList;

/* Selection set */
typedef struct _CmpackSelectionSet
{
	char *name;
	CmpackSelectionList list;
	struct _CmpackSelectionSet *next;
} CmpackSelectionSet;

typedef struct CmpackSelection
{
	CmpackSelectionSet *first, *last;
	CmpackSelectionSet *current;
} CmpackSelection;

/* Selection record */
typedef struct _CmpackTag
{
	int star_id;
	char *tag;
	struct _CmpackTag *next;
} CmpackTag;

typedef struct _CmpackTagList
{
	CmpackTag *first, *last;
} CmpackTagList;

/* Catalog file context */
struct _CmpackCatFile 
{
	int				refcnt;		/**< Reference counter */
	FILE			*f;			/**< Associated resource */
	char			*fpath;		/**< File path */
	int				readonly;	/**< Nonzero if changes are not allowed */
	int				changed;	/**< Nonzero if changes were made */
	unsigned		flags;		/**< Read/write flags */
	CmpackHeader	head;		/**< File header */
	CmpackObjTab	stars;		/**< Table of stars */
	CmpackSelection	sel;		/**< Selection lists */
	CmpackTagList	tags;		/**< Table of tags */
	CmpackWcs		*wcs;		/**< WCS data */
};

#endif
