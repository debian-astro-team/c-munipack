/**************************************************************

oesfile.h (C-Munipack project)
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_OESFILE_H_INCLUDED
#define CMPACK_OESFILE_H_INCLUDED

#include "cmpack_common.h"
#include "cmpack_console.h"

/* Magic numbers */
#define H_MAG1_OWN 0x012AEE94L
#define H_MAG2_OWN 0x012F3462L
#define H_MAG1_INV 0x94EE2A01L
#define H_MAG2_INV 0x62342F01L

/* File descriptor */
typedef struct _oesfile oesfile;

/* Opens a file and reads the header but not the image data */
int oesopen(oesfile **oes, const char *filename);
     
/* Frees allocated memory in stfile */
void oesclos(oesfile *oes);
     
/* Returns the size of the image in pixels */
int oesghpr(oesfile *oes, int *nx, int *ny);
     
/* Returns the exposure duration in seconds */
int oesgexp(oesfile *oes, double *exp);
     
/* Returns temperature */
int oesgtem(oesfile *oes, double *temp);
     
/* Reads date and time */
int oesgdat(oesfile *oes, int *yr, int *mon, int *day, int *hr, int *min, int *sec);
	 
/* Returns value from the header as int */
int oesgkyi(oesfile *oes, int nkey, int *value);
    
/* Returns value from the header as double */
int oesgkyd(oesfile *oes, int nkey, double *value);
     
/* Returns value from the header as string */
int oesgkys(oesfile *oes, int nkey, char **value);
     
/* Sequential read of parameters */
int oesgkyn(oesfile *oes, int nkey, char **keyname, char **keyval, char **comm);

/* Copies the image data into the given buffer */
int oesgimg(oesfile *oes, uint16_t *buf, int bufsize);

#endif
