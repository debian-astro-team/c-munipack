/**************************************************************

linsolver.h (C-Munipack project)
Linear solver - solving a system of linear equations
Copyright (C) 2016 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef QRDECOMP_H
#define QRDECOMP_H

/* QR decomposition of a matix */
struct _QRDecomposition
{
	int cols, rows, minmn;
	double *a, *work, *t, *tau;
};
typedef struct _QRDecomposition QRDecomposition;

/* Create memory buffers for a QR decomposition */
void cmpack_qrd_alloc(QRDecomposition *qr, int rows, int cols);

/* Decomposition of matrix A */
void cmpack_qrd_set(QRDecomposition *qr, const double *A, double *q, double *r);

/* Release allocated memory buffers */
void cmpack_qrd_free(QRDecomposition *qr);

#endif
