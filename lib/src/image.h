#ifndef IMAGE_H
#define IMAGE_H

#include "cmpack_image.h"

struct _CmpackImage
{
	int refcnt;
	int width, height;
	int internal_buffer;
	CmpackBitpix format;
	int datalen;
	void *data;
};

#endif
