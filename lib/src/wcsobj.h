/**************************************************************

xml_dom.h (C-Munipack project)
Simplified Document Object Model 
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_WCS_H
#define CMPACK_WCS_H

#include "cmpack_common.h"
#include "cmpack_wcs.h"

#include "imageheader.h"
#include "xmldom.h"

/* Initialize the mutex */
void cmpack_wcs_init(void);

/* Destroy the mutex */
void cmpack_wcs_clean(void);

/* Parse FITS header and create a wcs object */
CmpackWcs *cmpack_wcs_new_from_FITS_header(char *header, int nkeyrec);

/* Load WCS data from a XML node */
CmpackWcs *cmpack_wcs_new_from_XML_node(CmpackElement *node);

/* Create FITS header from a WCS data */
/* Returns zero on success or error code on failure */
int cmpack_wcs_to_FITS_header(CmpackWcs *wcs, char **buf, int *nkeyrec);

/* Save WCS data to a file in XML format */
void cmpack_wcs_write_XML(CmpackWcs *wcs, FILE *to);

/* Updates FITS header 
   action:	1 = remove all WCS keywords
			2 = keep WCS keywords only
*/
void cmpack_wcs_update_header(const CmpackImageHeader *src, CmpackImageHeader *dst, int action);

/* Returns nonzero if the key is a valid WCS keyword */
int is_wcs_keyword(const char *key);

#endif
