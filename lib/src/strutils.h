/**************************************************************

strutils.h (C-Munipack project)
String utilities
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef STRUTILS_H
#define STRUTILS_H

/***********************      Datatypes      ******************/

/* String buffer */
typedef struct _CmpackString CmpackString;

/******************   Public functions    *********************/

/* Create a new empty string */
CmpackString *cmpack_str_create(void);

/* Create a string from nul-terminated string or buffer*/
CmpackString *cmpack_str_from_text(const char *text, int nchar);

/* Create a new string from another string */
CmpackString *cmpack_str_from_string(const CmpackString *str);

/* Destroy string and free allocated memory */
void cmpack_str_free(CmpackString *str);

/* Clear content of a string */
void cmpack_str_clear(CmpackString *str);

/* Append nul-terminated string into existing string */
void cmpack_str_set_text(CmpackString *str, const char *text, int nchar);

/* Append a string from another string */
void cmpack_str_set_string(CmpackString *dst, const CmpackString *src);

/* Append nul-terminated string into existing string */
void cmpack_str_add_text(CmpackString *str, const char *text, int nchar);

/* Append a string from another string */
void cmpack_str_add_string(CmpackString *dst, const CmpackString *src);

/* Tell number of characters */
int cmpack_str_length(const CmpackString *str);

/* Convert a string into nul-terminated string */
const char *cmpack_str_cstr(CmpackString *str);

/* Convert a string into integer number */
int cmpack_str_int(CmpackString *str, int *value);

/* Convert a string into integer number */
int cmpack_str_dbl(CmpackString *str, double *value);

/* Trim all trailing white characters from the string */
void cmpack_str_rtrim(CmpackString *str);

#endif
