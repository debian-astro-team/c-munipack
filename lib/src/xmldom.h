/**************************************************************

xml_dom.h (C-Munipack project)
Simplified Document Object Model 
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_XML_DOM_H
#define CMPACK_XML_DOM_H

#include "cmpack_common.h"
#include "cmpack_xml.h"

/***********************      Datatypes      ******************/

/* Node */
typedef struct _CmpackNode
{
	char *nodeName;
	char *nodeValue;
	CmpackXmlNodeType nodeType;
	struct _CmpackNode *parentNode;
	struct _CmpackNode *firstChild;
	struct _CmpackNode *lastChild;
	struct _CmpackNode *nextSibling;
	void (*destroy)(void*);
} CmpackNode;

/* Document */
typedef struct _CmpackDocument 
{
	CmpackNode	*root;
} CmpackDocument;

typedef struct _CmpackAttribute
{
	char *name;
	char *value;
} CmpackAttribute;

/* Attributes */
typedef struct _CmpackAttributes
{
	int count;
	CmpackAttribute *list;
} CmpackAttributes;

/* Element */
typedef struct _CmpackElement
{
	CmpackNode node;
	CmpackAttributes attr;
} CmpackElement;

/* Comment */
typedef struct _CmpackComment
{
	CmpackNode node;
} CmpackComment;

/* Character data */
typedef struct _CmpackCData
{
	CmpackNode node;
} CmpackCData;

#endif
