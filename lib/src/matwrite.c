/**************************************************************

matwrite.c (C-Munipack project)
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "comfun.h"
#include "cmpack_common.h"
#include "cmpack_match.h" 
#include "cmpack_phtfile.h" 
#include "matwrite.h"

/* Generates output file */
void MatWrite(CmpackMatch *cfg, CmpackPhtFile *pht)
{
	int		i, count; 
	CmpackPhtInfo info;
	CmpackPhtObject obj;

	/* Matching information */
	info.matched = 1;
	info.match_rstars = cfg->maxstar;
	info.match_istars = cfg->nstar;
	info.match_clip = cfg->clip;
	info.match_mstars = cfg->matched_stars;
	info.trafo = cfg->matrix;
	cmpack_match_get_offset(cfg, info.offset, info.offset+1);
	cmpack_pht_set_info(pht, CMPACK_PI_MATCH_PARAMS | CMPACK_PI_TRAFO, &info);

	/* For all stars in the reference file */
	count = cmpack_pht_object_count(pht);
	for (i=0; i<count; i++) {
		if (cmpack_pht_get_object(pht, i, CMPACK_PO_ID, &obj)==0) {
			if (cfg->i2[i] == obj.id) {
				obj.ref_id = cfg->xref[i];
				cmpack_pht_set_object(pht, i, CMPACK_PO_REF_ID, &obj);
			}
		}
	}
}
