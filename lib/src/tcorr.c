/**************************************************************

tcor.c (C-Munipack project)
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "comfun.h"
#include "trajd.h"
#include "console.h"
#include "ccdfile.h"
#include "cmpack_common.h"
#include "cmpack_tcorr.h"

/* Time correction */
int cmpack_tcorr(CmpackCcdFile *file, double tcor, CmpackConsole *con)
{
	CmpackCcdParams params;

	/* Check source file */
	if (!file) {
		printout(con, 0, "Invalid frame context");
		return CMPACK_ERR_INVALID_PAR;
	}

	/* Read date and time */
	if (cmpack_ccd_get_params(file, CMPACK_CM_JD, &params)!=0) {
		printout(con, 0, "Failed to read image parameters from the file");
		return CMPACK_ERR_READ_ERROR;
	}
	if (params.jd<=0.0) {
		printout(con, 0, "Invalid date and time of observation in the source file");
		return CMPACK_ERR_INVALID_DATE;
	}

	/* Time correction */
	printpard(con, "Old JD", 1, params.jd, 6);
	if (params.jd>0)
		params.jd += tcor/86400.0;
	printpard(con, "New JD", 1, params.jd, 6);

	/* Write date and time */
	if (cmpack_ccd_set_params(file, CMPACK_CM_JD, &params)!=0) {
		printout(con, 0, "Failed to write the date and time of observation");
		return CMPACK_ERR_WRITE_ERROR;
	}

	/* Update file header */
	ccd_update_history(file, "Time corrected.");
	return 0;
}

/* Time correction */
int cmpack_tcorr_ex(CmpackCcdFile *src, CmpackCcdFile *dst, double tcor, CmpackConsole *con)
{
	int res = cmpack_ccd_copy(dst, src, con);
	if (res==0)
		res = cmpack_tcorr(dst, tcor, con);
	return res;
}

/* Time correction */
int cmpack_tcorr_pht(CmpackPhtFile *file, double tcor, CmpackConsole *con)
{
	CmpackPhtInfo info;

	/* Check source file */
	if (!file) {
		printout(con, 0, "Invalid frame context");
		return CMPACK_ERR_INVALID_PAR;
	}

	/* Read date and time */
	if (cmpack_pht_get_info(file, CMPACK_PI_JD, &info)!=0) {
		printout(con, 0, "Failed to read image parameters from the file");
		return CMPACK_ERR_READ_ERROR;
	}
	if (info.jd<=0.0) {
		printout(con, 0, "Invalid date and time of observation in the source file");
		return CMPACK_ERR_INVALID_DATE;
	}

	/* Time correction */
	printpard(con, "Old JD", 1, info.jd, 6);
	if (info.jd>0)
		info.jd += tcor/86400.0;
	printpard(con, "New JD", 1, info.jd, 6);

	/* Write date and time */
	if (cmpack_pht_set_info(file, CMPACK_PI_JD, &info)!=0) {
		printout(con, 0, "Failed to write the date and time of observation");
		return CMPACK_ERR_WRITE_ERROR;
	}

	/* Update file header */
	return 0;
}
