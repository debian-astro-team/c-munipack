/**************************************************************

hash.h (C-Munipack project)
HASH table header (photometry file)
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

/* ATTENTION: All records referenced by the table MUST starts with 
   key of char* type!!!! See implementation of hash_search. */

#ifndef HASH_H
#define HASH_H

/*********************   Constants  **********************/

#define HASHSIZE 	32
#define HASHMASK 	0x1F

/*********************   Data types   ********************/

/* Hash table item */
struct _CmpackHashItem
{
	void *ptr;					/**< Pointer to the item */
	struct _CmpackHashItem *next;		/**< Next item */
};
typedef struct _CmpackHashItem CmpackHashItem;

/* Hash table */
struct _CmpackHashTable
{
	CmpackHashItem *tab[HASHSIZE];	/**< Table of lists */
};
typedef struct _CmpackHashTable CmpackHashTable;

/********************   Public functions    ***********************/

/* Init hash table */
void hash_init(CmpackHashTable *tab);

/* Insert record to the table */
void hash_insert(CmpackHashTable *tab, const char *key, void *ptr);

/* Search record in the table */
void *hash_search(CmpackHashTable *tab, const char *key);

/* Delete record from the table */
void hash_delete(CmpackHashTable *tab, const char *key, void *ptr);

/* Clear the table */
void hash_clear(CmpackHashTable *tab);

#endif
