/**
	\file
	\brief Functions for the time correction

	Set of functions defined in this module allows user to 
	apply time correction to CCD frames.

	\author David Motl <dmotl@volny.cz>
	
	\par Copying
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation, version 2.
	
	$Id: cmpack_tcorr.h,v 1.2 2015/07/12 08:11:55 dmotl Exp $
*/

#ifndef _CMPACK_TCORR_H_INCLUDED
#define _CMPACK_TCORR_H_INCLUDED

#include "cmpack_common.h"
#include "cmpack_console.h"
#include "cmpack_ccdfile.h"
#include "cmpack_phtfile.h"

/************************   Public functions   ***********************************/

#ifdef __cplusplus
extern "C" {
#endif

/**
	\brief Execute time correction on a CCD frame
	\details The function reads header data from the CCD frame, performs
	the time correction and stores the output to the same context.
	\param[in,out] file		frame context
	\param[in] seconds		correction in seconds
	\param[in] console		console context for debug information
	\return zero on success or error code on failure
*/
	CMPACK_EXPORT(int, cmpack_tcorr, (CmpackCcdFile* file, double seconds, CmpackConsole* console));

/**
	\brief Execute time correction on a file
	\details The function reads header data from the infile context, performs
	the time correction and stores the output image to the outfile context.
	\param[in] file			input frame context
	\param[in] outfile		output frame context
	\param[in] seconds		correction in seconds
	\param[in] console		console context for debug information
	\return zero on success or error code on failure
*/
	CMPACK_EXPORT(int, cmpack_tcorr_ex, (CmpackCcdFile* file, CmpackCcdFile* outfile, double seconds,
		CmpackConsole* console));

/**
	\brief Execute time correction on a photometry file
	\details The function reads header data from the CCD frame, performs
	the time correction and stores the output to the same context.
	\param[in] file			input frame context
	\param[in] seconds		correction in seconds
	\param[in] console		console context for debug information
	\return zero on success or error code on failure
*/
	CMPACK_EXPORT(int, cmpack_tcorr_pht, (CmpackPhtFile* file, double seconds, CmpackConsole* console));

#ifdef __cplusplus
}
#endif

#endif
