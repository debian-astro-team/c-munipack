/**
	\file
	\brief Astronomy functions

	Astronomy related functions, date & time conversion functions, 
	Julian date conversions, conversion of coordinate systems,
	air-mass coefficient computation, heliocentric correction, etc.
	
	\author David Motl <dmotl@volny.cz>
	
	\par Copying
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation, version 2.
	
	$Id: cmpack_astro.h,v 1.1 2015/07/06 08:33:22 dmotl Exp $
*/
#ifndef _CMPACK_ASTRO_H_INCLUDED
#define _CMPACK_ASTRO_H_INCLUDED

/* Moved to cmpack_common.h */
#include "cmpack_common.h"

#endif
