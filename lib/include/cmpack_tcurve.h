/**
	\file
	\brief Functions for making a track curve

	Set of functions defined in this module allows user to 
	make a track curve
	
	\author David Motl <dmotl@volny.cz>
	
	\par Copying
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation, version 2.
	
	$Id: cmpack_tcurve.h,v 1.1 2015/07/06 08:33:22 dmotl Exp $
*/
#ifndef _CMPACK_TCURVE_H_INCLUDED
#define _CMPACK_TCURVE_H_INCLUDED

#include "cmpack_console.h"
#include "cmpack_fset.h"
#include "cmpack_table.h"

/**********************   Public constants   ***********************************/

/**
	\brief Output modifiers
*/
typedef enum _CmpackTCurveFlags
{
	CMPACK_TCURVE_DEFAULT = 0,			/**< Default behavior */
	CMPACK_TCURVE_FRAME_IDS = (1<<0)	/**< Include frame identifiers */
} CmpackTCurveFlags;

/********************   Public functions   ********************************/

#ifdef __cplusplus
extern "C" {
#endif

/**
	\brief Make a track curve
	\param[in] fset			frame set (source data)
	\param[out] table		new table with the track curve
	\param[in] flags		flags
	\param[in] console		console context for debug information
	\return zero on success or error code on failure
*/
	CMPACK_EXPORT(int, cmpack_tcurve, (CmpackFrameSet* fset, CmpackTable** table, CmpackTCurveFlags flags,
		CmpackConsole* console));

#ifdef __cplusplus
}
#endif

#endif
