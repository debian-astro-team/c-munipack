/**
	\file
	\brief Functions for matching photometry files.

	Set of functions defined in this module allows user to 
	find matching files, i.e. to find crossreferences in two sets
	of stars by their positions.
	
	\author David Motl <dmotl@volny.cz>
	
	\par Copying
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation, version 2.
	
	$Id: cmpack_match.h,v 1.5 2016/06/05 07:19:01 dmotl Exp $
*/
#ifndef _CMPACK_MATCH_H_INCLUDED
#define _CMPACK_MATCH_H_INCLUDED

#include "cmpack_console.h"
#include "cmpack_phtfile.h"
#include "cmpack_catfile.h"

/********************   Data type definitions   ********************************/

/**
	\brief Matching configuration context
	\details This private data structure holds the configuration parameters for 
	CCD-frame conversion.
*/
typedef struct _CmpackMatch CmpackMatch;

/**
	\brief Matching method
*/
typedef enum _CmpackMatchMethod 
{
	CMPACK_MATCH_STANDARD,
	CMPACK_MATCH_AUTO,
	CMPACK_MATCH_SPARSE_FIELDS,
	CMPACK_MATCH_PHILNR
} CmpackMatchMethod;

/************************   Public functions   ********************************/

#ifdef __cplusplus
extern "C" {
#endif

/**
	\brief Make new matching context
	\details The function allocates memory with matching context and returns a 
	new reference to it. The reference counter is set to one. The caller is 
	responsible to call cmpack_match_destroy() when it is no longer needed.
	\return pointer to a new reference
*/
	CMPACK_EXPORT(CmpackMatch*, cmpack_match_init, (void));

/**
	\brief Make a new reference to the matching context
	\details The function makes a new reference to the context and returns a 
	pointer to it. The reference counter is incremented by one. The caller 
	is responsible to call cmpack_match_destroy() when the reference is 
	no longer needed.
	\return pointer to a new reference
*/
	CMPACK_EXPORT(CmpackMatch*, cmpack_match_reference, (CmpackMatch* ctx));

/**
	\brief Release a reference to the matching context
	\details The function releases a reference to the context. 
	The reference counter is decreased by one and when it was the 
	last reference to the context, the context is freed and all memory 
	allocated in the context is reclaimed.
*/
	CMPACK_EXPORT(void, cmpack_match_destroy, (CmpackMatch* ctx));

/**
	\brief Attach console to the matching context
	\details The function connects a matching context with
	a console context. The console is designed to print the information
	during the data processing. The functions makes its own reference
	to the console. Only one console can be attached to a single context, 
	if another console is attached to the single context, the original 
	one is released. Set console to NULL to release a reference to the 
	console that is currently attached to the context.
	\param[in] ctx			matching computation context
	\param[in] con			console context
*/
	CMPACK_EXPORT(void, cmpack_match_set_console, (CmpackMatch* ctx, CmpackConsole* con));

/**
	\brief Set max. number of stars used for matching
	\param[in] ctx			matching context
	\param[in] nstars		number of stars
*/
	CMPACK_EXPORT(void, cmpack_match_set_maxstars, (CmpackMatch* ctx, int nstars));

/**
	\brief Get max. number of stars used for matching
	\param[in] ctx			matching context
	\return number of stars
*/
	CMPACK_EXPORT(int, cmpack_match_get_maxstars, (CmpackMatch* ctx));

/**
	\brief Set number of vertices of polygons
	\param[in] ctx			matching context
	\param[in] vertices		number of vertices
*/
	CMPACK_EXPORT(void, cmpack_match_set_vertices, (CmpackMatch* ctx, int vertices));

/**
	\brief Get number of vertices of polygons
	\param[in] ctx			matching context
	\return number of vertices
*/
	CMPACK_EXPORT(int, cmpack_match_get_vertices, (CmpackMatch* ctx));

/**
	\brief Set clipping threshold
	\param[in] ctx			matching context
	\param[in] threshold	threshold 
*/
	CMPACK_EXPORT(void, cmpack_match_set_threshold, (CmpackMatch* ctx, double threshold));

/**
	\brief Get clipping threshold
	\param[in] ctx			matching context
	\return	current clipping threshold 
*/
	CMPACK_EXPORT(double, cmpack_match_get_threshold, (CmpackMatch* ctx));

/**
	\brief Set matching method
	\param[in] ctx			matching context
	\param[in] method		(0=Standard, 1=Auto, 2=Sparse_fields)
*/
	CMPACK_EXPORT(void, cmpack_match_set_method, (CmpackMatch* ctx, CmpackMatchMethod method));

/**
	\brief Get matching method
	\param[in] ctx			matching context
	\return (0=Standard, 1=Auto, 2=Sparse_fields)
*/
	CMPACK_EXPORT(CmpackMatchMethod, cmpack_match_get_method, (CmpackMatch* ctx));

/**
	\brief Set Max. offset for 'sparse fields' method
	\param[in] ctx			matching context
	\param[in] maxoffset	offset in pixels
*/
	CMPACK_EXPORT(void, cmpack_match_set_maxoffset, (CmpackMatch* ctx, double maxoffset));

/**
	\brief Get Max. offset for 'sparse fields' method
	\param[in] ctx			matching context
	\return offset in pixels
*/
	CMPACK_EXPORT(double, cmpack_match_get_maxoffset, (CmpackMatch* ctx));

/**
	\brief Load reference frame from the photometry file
	\details The function reads photometry file into the context.
	\param[in] ctx			matching context
	\param[in] reffile		reference file context
	\return zero on success or error code on failure
*/
	CMPACK_EXPORT(int, cmpack_match_readref_pht, (CmpackMatch* ctx, CmpackPhtFile* reffile));

/**
	\brief Load reference frame from the catalog file
	\details The function reads catalog file into the context.
	\param[in] ctx			matching context
	\param[in] reffile		reference file context
	\return zero on success or error code on failure
*/
	CMPACK_EXPORT(int, cmpack_match_readref_cat, (CmpackMatch* ctx, CmpackCatFile* reffile));

/**
	\brief Match photometry file to the reference file
	\details The function reads the intput file, finds crossreferences to the
	reference frame and set the reference IDs.
	\param[in] ctx			matching context
	\param[in] file			photometry file context
	\param[out] mstars		number of stars matched
	\return zero on success or error code on failure
*/
	CMPACK_EXPORT(int, cmpack_match, (CmpackMatch* ctx, CmpackPhtFile* file, int* mstars));

/**
	\brief Get offset of the last frame
	\details The function retrieves the offset of the last frame
	\param[in] ctx			matching context
	\param[out] offset_x	offset in X axis
	\param[out] offset_y	offset in Y axis
	\return zero on success or error code on failure
*/
	CMPACK_EXPORT(int, cmpack_match_get_offset, (CmpackMatch* ctx, double* offset_x, double* offset_y));

/**
	\brief Get the transformation matrix
	\details The function retrieves the offset of the last frame
	\param[in] ctx			matching context
	\param[out] matrix		memory buffer for 6 values (the transformation matrix)
	\return zero on success or error code on failure
*/
	CMPACK_EXPORT(int, cmpack_match_get_matrix, (CmpackMatch* ctx, CmpackMatrix* matrix));

/**
	\brief Ignore the specified objects during matching
	\details The function is called to specify an object on the reference frame
	and an object on the source frame that will be ignored in the matching.
	This is used in the minor body photometry on the moving object.
	Set the object id to zero to disable this feature (include all objects)
	\param[in]	ctx			matching context
	\param[in]	src_id		object id on the source frame (0 = include all)
	\param[in]	ref_id		object id on the reference frame (0 = include all)
*/
	CMPACK_EXPORT(void, cmpack_match_set_ignore, (CmpackMatch* ctx, int src_id, int ref_id));

/**
	\brief Set reference id for given object 
	\details The function is called to specify a reference object id for 
	a special object that cannot be matched using standard techniques. 
	This is used in the photometry of minor bodies where a position of the 
	minor body depends on time. It is used together with cmpack_match_set_ignore
	alternatively with cmpack_match_set_objpos.
	\param[in]	ctx			matching context
	\param[in]	src_id		object id on the source frame (0 = include all)
	\param[in]	ref_id		object id on the reference frame (0 = include all)
*/
	CMPACK_EXPORT(void, cmpack_match_set_objref, (CmpackMatch* ctx, int src_id, int ref_id));

/**
	\brief Set coordinates of an object on the reference frame
	\details The function overrides the position of an object
	on the reference frame. This is used in the photometry of minor
	bodies where a position of the minor body depends on the time.
	Set the object id to zero to disable this feature (use position from the photometry / catalogue file)
	\param[in]	ctx			matching context
	\param[in]	ref_id		object id on the reference frame (0 = use original position)
	\param[in]	x			object x coordinate in pixels
	\param[in]	y			object y coordinate in pixels
*/
	CMPACK_EXPORT(void, cmpack_match_set_objpos, (CmpackMatch* ctx, int ref_id, double x, double y));

/**
	\brief Get number of reference stars
	\details The function retrieves the number of objects on the reference frame.
	\param[in] ctx			matching context
	\return zero if the reference file was not read or positive value on success
*/
	CMPACK_EXPORT(int, cmpack_match_refcount, (CmpackMatch* ctx));

#ifdef __cplusplus
}
#endif

#endif
