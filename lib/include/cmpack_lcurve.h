/**
	\file
	\brief Functions for making a light curve

	Set of functions defined in this module allows user to 
	make a light curve
	
	\author David Motl <dmotl@volny.cz>
	
	\par Copying
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation, version 2.
	
	$Id: cmpack_lcurve.h,v 1.2 2016/05/19 20:47:18 dmotl Exp $
*/
#ifndef _CMPACK_LCURVE_H_INCLUDED
#define _CMPACK_LCURVE_H_INCLUDED

#include "cmpack_console.h"
#include "cmpack_fset.h"
#include "cmpack_table.h"

/********************   Private data structures   ********************************/

/**
	\brief Listing process context
	\details This private data structure holds the temporary data, which are
	used during listing process.
*/
typedef struct _CmpackLCurve CmpackLCurve;

/**********************   Public constants   ***********************************/

/**
	\brief Output modifiers
*/
typedef enum _CmpackLCurveFlags
{
	CMPACK_LCURVE_DEFAULT = 0,			/**< Default behavior */
	CMPACK_LCURVE_INSTMAG	= (1<<0),	/**< Instrumental magnitudes */
	CMPACK_LCURVE_FRAME_IDS = (1<<1),	/**< Include frame identifiers */
	CMPACK_LCURVE_ALTITUDE  = (1<<2),	/**< Include altitude in degrees */
	CMPACK_LCURVE_AIRMASS   = (1<<3),	/**< Include airmass coefficients */
	CMPACK_LCURVE_HELCORR	= (1<<4),	/**< Include heliocentric correction */
	CMPACK_LCURVE_HJD		= (1<<5),	/**< Include heliocentric Julian date */
	CMPACK_LCURVE_HJDONLY	= (1<<6),	/**< Exclude geocentric Julian date */
	CMPACK_LCURVE_ALLSTARS	= (1<<7),	/**< Include all stars from the reference frame */
	CMPACK_LCURVE_EXPOSURE	= (1<<8),	/**< Include exposure is seconds */
	CMPACK_LCURVE_FILENAME	= (1<<9)	/**< Include original file name */
} CmpackLCurveFlags;

/********************   Public functions   ********************************/

#ifdef __cplusplus
extern "C" {
#endif

/**
	\brief Make new light-curve context
	\details The function allocates memory with light-curve context and returns a 
	new reference to it. The reference counter is set to one. The caller is 
	responsible to call cmpack_lcurve_destroy() when it is no longer needed.
	\return pointer to a new reference
*/
	CMPACK_EXPORT(CmpackLCurve*, cmpack_lcurve_init, (void));

/**
	\brief Make a new reference to the light-curve context
	\details The function makes a new reference to the context and returns a 
	pointer to it. The reference counter is incremented by one. The caller 
	is responsible to call cmpack_lcurve_destroy() when the reference is 
	no longer needed.
	\return pointer to a new reference
*/
	CMPACK_EXPORT(CmpackLCurve*, cmpack_lcurve_reference, (CmpackLCurve* ctx));

/**
	\brief Release a reference to the light-curve context
	\details The function releases a reference to the context. 
	The reference counter is decreased by one and when it was the 
	last reference to the context, the context is freed and all memory 
	allocated in the context is reclaimed.
*/
	CMPACK_EXPORT(void, cmpack_lcurve_destroy, (CmpackLCurve* ctx));

/**
	\brief Attach console to the light-curve context
	\details The function connects a light-curve context with
	a console context. The console is designed to print the information
	during the data processing. The functions makes its own reference
	to the console. Only one console can be attached to a single context, 
	if another console is attached to the single context, the original 
	one is released. Set console to NULL to release a reference to the 
	console that is currently attached to the context.
	\param[in] ctx			listing computation context
	\param[in] con			console context
*/
	CMPACK_EXPORT(void, cmpack_lcurve_set_console, (CmpackLCurve* ctx, CmpackConsole* con));

/**
	\brief Set the aperture identifier
	\details The function sets the identifier of the aperture which
	is used to read the measurements. The default value is zero.
	\param[in] proc			listing context
	\param[in] aperture		aperture identifier
*/
	CMPACK_EXPORT(void, cmpack_lcurve_set_aperture, (CmpackLCurve* proc, int aperture));

/**
	\brief Get current aperture identifier
	\details The function sets the identifier of the aperture which
	is used to read the measurements. The default value is zero.
	\param[in] proc			listing context
	\return aperture identifier
*/
	CMPACK_EXPORT(int, cmpack_lcurve_get_aperture, (CmpackLCurve* proc));

/**
	\brief Set list of variable stars
	\details The function sets the list of variable stars that are 
	included in the light curve.
	\param[in] proc			listing context
	\param[in] items		array of star identifiers
	\param[in] nitems		number of items
*/
	CMPACK_EXPORT(void, cmpack_lcurve_set_var, (CmpackLCurve* proc, const int* items, int nitems));

/**
	\brief Get list of variable stars
	\details The function makes copy of the data and returns the pointer
	to it. The caller is reponsible to free the memory by means of the
	cmpack_free() method.
	\param[in] proc			listing context
	\param[out] items		array of star identifiers
	\param[out] nitems		number of items
*/
	CMPACK_EXPORT(void, cmpack_lcurve_get_var, (CmpackLCurve* proc, int** items, int* nitems));

/**
	\brief Set list of comparison stars
	\details The function sets the list of comparison stars that are included
	in the light curve.
	\param[in] proc			listing context
	\param[in] items		array of star identifiers
	\param[in] nitems		number of items
*/
	CMPACK_EXPORT(void, cmpack_lcurve_set_comp, (CmpackLCurve* proc, const int* items, int nitems));

/**
	\brief Get list of comparison stars
	\details The function makes copy of the data and returns the pointer
	to it. The caller is reponsible to free the memory by means of the
	cmpack_free() method.
	\param[in] proc			listing context
	\param[out] items		array of star identifiers
	\param[out] nitems		number of items
*/
	CMPACK_EXPORT(void, cmpack_lcurve_get_comp, (CmpackLCurve* proc, int** items, int* nitems));

/**
	\brief Set list of check stars
	\details The function sets the list of check stars that are included 
	in the light curve.
	\param[in] proc			listing context
	\param[in] items		array of star identifiers
	\param[in] nitems		number of items
*/
	CMPACK_EXPORT(void, cmpack_lcurve_set_check, (CmpackLCurve* proc, const int* items, int nitems));

/**
	\brief Get list of check stars
	\details The function makes copy of the data and returns the pointer
	to it. The caller is reponsible to free the memory by means of the
	cmpack_free() method.
	\param[in] proc			listing context
	\param[out] items		array of star identifiers
	\param[out] nitems		number of items
*/
	CMPACK_EXPORT(void, cmpack_lcurve_get_check, (CmpackLCurve* proc, int** items, int* nitems));

/**
	\brief Make a light curve
	\param[in] proc			listing process context
	\param[in] fset			frame set (source data)
	\param[out] table		new table with the light curve
	\param[in] flags		flags
	\return zero on success or error code on failure
*/
	CMPACK_EXPORT(int, cmpack_lcurve, (CmpackLCurve* proc, CmpackFrameSet* fset,
		CmpackTable** table, CmpackLCurveFlags flags));

#ifdef __cplusplus
}
#endif

#endif
