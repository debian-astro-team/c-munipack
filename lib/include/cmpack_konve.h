/**
	\file
	\brief Functions for converting CCD frames from vendor
	specific format to the FITS format.

	Set of functions defined in this module allows user to 
	convert the CCD frames to FITS format.
	
	\author David Motl <dmotl@volny.cz>
	
	\par Copying
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation, version 2.
	
	$Id: cmpack_konve.h,v 1.2 2015/07/12 08:11:55 dmotl Exp $
*/
#ifndef _CMPACK_KONVE_H_INCLUDED
#define _CMPACK_KONVE_H_INCLUDED

#include "cmpack_common.h"

/********************   Private data structures   ********************************/

/** \brief Image transformation bits */
typedef enum _CmpackImageTrafo
{
	CMPACK_FLIP_0	= 0x00,			/**< No flipping */
	CMPACK_FLIP_X	= 0x01,			/**< Flip horizontally */
	CMPACK_FLIP_Y	= 0x02,			/**< Flip vertically */
	CMPACK_FLIP_XY	= 0x03			/**< Rotate image 180 degrees */
} CmpackImageTrafo;

/**
	\brief Conversion context
	\details This private data structure holds the configuration parameters for 
	CCD-frame conversion.
*/
typedef struct _CmpackKonv CmpackKonv;

/********************   Public functions   ********************************/

#ifdef __cplusplus
extern "C" {
#endif

/**
	\brief Make new conversion context
	\details The function allocates memory with conversion context and returns a 
	new reference to it. The reference counter is set to one. The caller is 
	responsible to call cmpack_konv_destroy() when it is no longer needed.
	\return pointer to a new reference
*/
	CMPACK_EXPORT(CmpackKonv*, cmpack_konv_init, (void));

/**
	\brief Make a new reference to the conversion context
	\details The function makes a new reference to the context and returns a 
	pointer to it. The reference counter is incremented by one. The caller 
	is responsible to call cmpack_konv_destroy() when the reference is 
	no longer needed.
	\return pointer to a new reference
*/
	CMPACK_EXPORT(CmpackKonv*, cmpack_konv_reference, (CmpackKonv* ctx));

/**
	\brief Release a reference to the conversion context
	\details The function releases a reference to the context. 
	The reference counter is decreased by one and when it was the 
	last reference to the context, the context is freed and all memory 
	allocated in the context is reclaimed.
*/
	CMPACK_EXPORT(void, cmpack_konv_destroy, (CmpackKonv* ctx));

/**
	\brief Attach console to the context
	\details The function connects a conversion context with
	a console context. The console is designed to print the information
	during the data processing. The functions makes its own reference
	to the console. Only one console can be attached to a single context, 
	if another console is attached to the single context, the original 
	one is released. Set console to NULL to release a reference to the 
	console that is currently attached to the context.
	\param[in] ctx			conversion context
	\param[in] con			console context
*/
	CMPACK_EXPORT(void, cmpack_konv_set_console, (CmpackKonv* ctx, CmpackConsole* con));

/**
	\brief Set output data format
	\details The function sets the format of the output CCD frame. If
	the format is set to CMPACK_BITPIX_AUTO (default), the format of the first
	source frame is used. Otherwise, the image data are converted into
	specified format.
	\param[in] file			conversion context
	\param[in] bitpix		output data format
*/
	CMPACK_EXPORT(void, cmpack_konv_set_bitpix, (CmpackKonv* file, CmpackBitpix bitpix));

/**
	\brief Set image border size
	\details The function sets the size of CCD frame borders. The pixels
	inside the frame borders are set always to zero. You can use this feature 
	to clear an unusable part of a frame.
	\param[in] file			conversion context
	\param[in] border		border size in pixels
*/
	CMPACK_EXPORT(void, cmpack_konv_set_border, (CmpackKonv* file, const CmpackBorder* border));

/**
	\brief Set pixel value thresholds
	\details The function sets the value of two thresholds that determine 
	valid pixel values. Pixels of value <= minvalue are treated as bad pixels, 
	pixels of value >= maxvalue are treated as overexposed pixels.
	\param[in] ctx			conversion context
	\param[in] minvalue		bad pixel threshold
	\param[in] maxvalue		overexposed pixel threshold
*/
	CMPACK_EXPORT(void, cmpack_konv_set_thresholds, (CmpackKonv* ctx, double minvalue, double maxvalue));

/**
	\brief Set pixel transformation of the image
	\details The function sets the pixel transformation that is performed
	to the CCD frame during conversion. The default value of binning is one, 
	which means that no operation is performed.
	\param[in] file			conversion context
	\param[in] hbin			binning factor in horizontal direction (>=1)
	\param[in] vbin			binning factor in vertical direction (>=1)
*/
	CMPACK_EXPORT(void, cmpack_konv_set_binning, (CmpackKonv* file, int hbin, int vbin));

/**
	\brief Transpose (flip) image
	\details The function transposes the source image and stores the 
	output to the same image.
	\param[in] file			conversion context
	\param[in] hflip		nonzero = horizontal flip
	\param[in] vflip		nonzero = vertical flip
*/
	CMPACK_EXPORT(void, cmpack_konv_set_transposition, (CmpackKonv* file, int hflip, int vflip));

/**
	\brief Selects color to grayscale transformation
	\details The function sets the color channel for conversion of
	DSLR images to FITS. The default is CMPACK_CHANNEL_SUM which sums
	all color channels.
	\param[in] file			conversion context
	\param[in] channel		channel identifier (CMPACK_CHANNEL_xxx)
*/
	CMPACK_EXPORT(void, cmpack_konv_set_channel, (CmpackKonv* file, CmpackChannel channel));

/**
	\brief Set time offset between source and target frames
	\details The function sets the time offset that will be applied
	to source frames. The offset is specified in seconds. The offset
	is added to the time of observation, it can be positive or negative.
	Fields other that the time of observations are not affected.
	\param[in] file			conversion context
	\param[in] seconds		time offset in seconds (0 = disable)
*/
	CMPACK_EXPORT(void, cmpack_konv_set_toffset, (CmpackKonv* file, double seconds));

/**
	\brief Convert a specified file to FITS format
	\details The function converts the source CCD frame to the target
	CCD frame. It perform the image data conversion and image transformation.
	The function preserves all information from the image header.
	\param[in] lc			conversion context
	\param[in] infile		source file context
	\param[in] outfile		output file context
	\return zero on success or error code on failure
*/
	CMPACK_EXPORT(int, cmpack_konv, (CmpackKonv* lc, CmpackCcdFile* infile, CmpackCcdFile* outfile));

/**
	\brief Convert a specified file to FITS format, take WCS data from another file
	\details The function converts the source CCD frame to the target
	CCD frame. All WCS keywords from the original file are skipped. Then, the 
	function parses the header in the file indicated by wcsfile parameter, reads
	all WCS keywords and adds them to the target frame. Then, the image data are
	copied. If binning or transposition are in effect, the WCS data are adjusted
	accordingly.
	\param[in] lc			conversion context
	\param[in] infile		source file context
	\param[in] wcsfile		file with external WCS data
	\param[in] outfile		output file context
	\return zero on success or error code on failure
*/
	CMPACK_EXPORT(int, cmpack_konv_with_wcs, (CmpackKonv* lc, CmpackCcdFile* infile, CmpackCcdFile* wcsfile, CmpackCcdFile* outfile));

#ifdef __cplusplus
}
#endif

#endif
