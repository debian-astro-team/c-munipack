/**
	\file
	\brief Functions for reading and writing read-all files.

	The read-all file contains simplified result of photometry for
	a set of frames. Use cmpack-readall object to make a read-all file 
	from a set of photometry files. You can pass it to the cmpack-list 
	object to make an output table.

	This file format was inherited from the varfind	project and because 
	it not extensible without breaking the compatibility it's planed to 
	be replaced in C-Munipack version 2. Please, do not use this functions 
	in newly written code.
	
	\author David Motl <dmotl@volny.cz>
	
	\par Copying
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation, version 2.
	
	$Id: cmpack_allfile.h,v 1.1 2015/07/06 08:33:22 dmotl Exp $
*/

#ifndef _CMPACK_ALLFILE_H_INCLUDED
#define _CMPACK_ALLFILE_H_INCLUDED

#endif
