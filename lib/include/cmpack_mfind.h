/**
	\file
	\brief Functions for the making mag-dev graph

	Set of functions defined in this module allows user to 
	make a mag-dev graph, that is used to find variable stars
	in a viewfield.
	
	\author David Motl <dmotl@volny.cz>
	
	\par Copying
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation, version 2.
	
	$Id: cmpack_mfind.h,v 1.2 2015/07/12 08:11:55 dmotl Exp $
*/
#ifndef _CMPACK_MFIND_H_INCLUDED
#define _CMPACK_MFIND_H_INCLUDED

#include "cmpack_console.h"
#include "cmpack_table.h"
#include "cmpack_fset.h"

/********************   Private data structures   ********************************/

/**
	\brief Munifind context
	\details This private data structure holds information needed to build
	the mag-dev graph.
*/
typedef struct _CmpackMuniFind CmpackMuniFind;

/**********************   Public constants   ***********************************/

#define CMPACK_MFIND_AUTO	(-1)		/**< Automatically select comparison star */

/**
	\brief Output modifiers
*/
typedef enum _CmpackMFindFlags
{
	CMPACK_MFIND_DEFAULT	= 0		/**< Default behavior */
} CmpackMFindFlags;

/********************   Public functions   ********************************/

#ifdef __cplusplus
extern "C" {
#endif

/**
	\brief Make new Munifind context
	\details The function allocates memory with Munifind context and returns a 
	new reference to it. The reference counter is set to one. The caller is 
	responsible to call cmpack_konv_destroy() when it is no longer needed.
	\return pointer to a new reference
*/
	CMPACK_EXPORT(CmpackMuniFind*, cmpack_mfind_init, (void));

/**
	\brief Make a new reference to the Munifind context
	\details The function makes a new reference to the context and returns a 
	pointer to it. The reference counter is incremented by one. The caller 
	is responsible to call cmpack_konv_destroy() when the reference is 
	no longer needed.
	\return pointer to a new reference
*/
	CMPACK_EXPORT(CmpackMuniFind*, cmpack_mfind_reference, (CmpackMuniFind* ctx));

/**
	\brief Release a reference to the Munifind context
	\details The function releases a reference to the context. 
	The reference counter is decreased by one and when it was the 
	last reference to the context, the context is freed and all memory 
	allocated in the context is reclaimed.
*/
	CMPACK_EXPORT(void, cmpack_mfind_destroy, (CmpackMuniFind* ctx));

/**
	\brief Attach console to the context
	\details The function connects a Munifind context with
	a console context. The console is designed to print the information
	during the data processing. The functions makes its own reference
	to the console. Only one console can be attached to a single context, 
	if another console is attached to the single context, the original 
	one is released. Set console to NULL to release a reference to the 
	console that is currently attached to the context.
	\param[in] ctx			munifind context
	\param[in] con			console context
*/
	CMPACK_EXPORT(void, cmpack_mfind_set_console, (CmpackMuniFind* ctx, CmpackConsole* con));

/**
	\brief Set the aperture identifier
	\details The function sets the identifier of the aperture which
	is used to read the measurements. The default value is zero.
	\param[in] ctx			munifind context
	\param[in] aperture		aperture aperture
*/
	CMPACK_EXPORT(void, cmpack_mfind_set_aperture, (CmpackMuniFind* ctx, int aperture));

/**
	\brief Set the comparison star 
	\details The function sets the comparison star which is used to compute
	the brightness of the objects. If this parameter is set to CMPACK_MFIND_AUTO, 
	the program will choose a suitable comparison star (default). When the cmpack_mfind 
	function is finished, you can tell the used comparison star by means of the 
	cmpack_mfind_get_comparison() function. If you set this parameter to positive 
	value, it must be a valid star identifier and this star will be used as a comparison 
	star. This is equivalent to calling cmpack_mfind_clear_comparison and cmpack_mfind_add_comparison.
	\param[in] ctx			munifind context
	\param[in] star_id		comparison star identifier or CMPACK_MFIND_AUTO
*/
	CMPACK_EXPORT(void, cmpack_mfind_set_comparison, (CmpackMuniFind* ctx, int star_id));

/**
	\brief Clear selection of comparison stars
	\details The function clears the list of selected comparison stars. If no comparison
	star is selected, the program will choose a suitable comparison star (this is the 
	default behavior). 
	\param[in] ctx			munifind context
*/
	CMPACK_EXPORT(void, cmpack_mfind_clear_comparison, (CmpackMuniFind* ctx));

/**
	\brief Select an object as a comparison star
	\details The function appends an object to the list of comparison stars. See
	cmpack_mfind_set_comparison and cmpack_mfind_clear_comparison for more info.
	\param[in] ctx			munifind context
	\param[in] star_id		comparison star identifier
*/
	CMPACK_EXPORT(void, cmpack_mfind_add_comparison, (CmpackMuniFind* ctx, int star_id));

/**
	\brief Get comparison star used 
	\details If the autodetection mode is in effect (see the cmpack_mfind_set_comparison()
	function), this function returns the identifier or the comparison star which was 
	actually used during computation.
	\param[in] ctx			munifind context
	\return identifier of the comparison star or negative value on error
*/
	CMPACK_EXPORT(int, cmpack_mfind_get_comparison, (CmpackMuniFind* ctx));

/**
	\brief Set clipping threshold
	\details The function sets the clipping threshold for the
	Munifind tool.
	\param[in] ctx			munifind context
	\param[in] threshold	threshold in %
*/
	CMPACK_EXPORT(void, cmpack_mfind_set_threshold, (CmpackMuniFind* ctx, double threshold));

/**
	\brief Make mag-dev graph from frame set
	\details The result is stored to a given table
	\param[in] ctx			munifind context
	\param[in] fset			frameset context
	\param[out] table		table with results
	\param[in] flags		flags
	\return zero on success or error code on failure.
*/
	CMPACK_EXPORT(int, cmpack_mfind, (CmpackMuniFind* ctx, CmpackFrameSet* fset,
		CmpackTable** table, CmpackMFindFlags flags));

/**
	\brief Find a suitable comparison star
	\details The function tries to find a star, that be the best
	choice to use as a comparison star.
	\param[in] ctx			munifind context
	\param[in] fset			frameset context
	\param[out] star_id		comparison star identifier
	\param[in] flags		flags
	\return zero on success or error code on failure.
*/
	CMPACK_EXPORT(int, cmpack_mfind_autocomp, (CmpackMuniFind* ctx, CmpackFrameSet* fset,
		int* star_id, CmpackMFindFlags flags));

/**
	\brief Determine date and time range
	\details The function finds valid measurements for the selected
	comparison star and finds minimum and maximum date of observation. 
	This value can be used for presentation as a magnitude scale range. 
	\param[in] ctx			munifind context
	\param[in] fset			frameset context
	\param[out] jdmin		minimum JD of the valid measurements
	\param[out] jdmax		maximum JD of the valid measurements
	\param[in] flags		flags
	\return zero on success or error code on failure.
*/
	CMPACK_EXPORT(int, cmpack_mfind_jdrange, (CmpackMuniFind* ctx, CmpackFrameSet* fset,
		double* jdmin, double* jdmax, CmpackMFindFlags flags));

/**
	\brief Determine magnitude range
	\details The function makes pairs using the selected comparison
	star and all other stars, creates a light curve for each pair
	and determines the mag range as difference between minimum and
	maximum magnitude in each light curve. The function returns 
	maximum from the mag ranges. This value can be used for presentation
	as a magnitude scale range. 
	\param[in] ctx			munifind context
	\param[in] fset			frameset context
	\param[out] magrange	maximum magnitude range
	\param[in] flags		flags
	\return zero on success or error code on failure.
*/
	CMPACK_EXPORT(int, cmpack_mfind_magrange, (CmpackMuniFind* ctx, CmpackFrameSet* fset,
		double* magrange, CmpackMFindFlags flags));

#ifdef __cplusplus
}
#endif

#endif
