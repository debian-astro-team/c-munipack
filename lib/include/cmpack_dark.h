/**
	\file
	\brief Functions for the dark-frame correction

	Set of functions defined in this module allows user to 
	apply dark-frame correction to CCD frames.
	
	\author David Motl <dmotl@volny.cz>
	
	\par Copying
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation, version 2.
	
	$Id: cmpack_dark.h,v 1.1 2015/07/06 08:33:22 dmotl Exp $
*/

#ifndef _CMPACK_DARK_H_INCLUDED
#define _CMPACK_DARK_H_INCLUDED

#include "cmpack_common.h"
#include "cmpack_console.h"
#include "cmpack_ccdfile.h"

/********************   Private data structures   ********************************/

/**
	\brief Dark-frame correction context
	\details This private data structure holds the parameter for dark-frame
	correction as well as the dark frame itself.
*/
typedef struct _CmpackDarkCorr CmpackDarkCorr;

/********************   Public functions   *************************************/

#ifdef __cplusplus
extern "C" {
#endif

/**
	\brief Make new dark-frame correction context
	\details The function allocated memory with dark-frame correction context
	and returns a new reference to it. The reference counter is set to one. 
	The caller is responsible to call cmpack_dark_destroy() when it is no 
	longer needed.
	\return pointer to a new reference
*/
	CMPACK_EXPORT(CmpackDarkCorr*, cmpack_dark_init, (void));

/**
	\brief Make a new reference to the dark-frame correction context
	\details The function makes a new reference to the file and returns a 
	pointer to it. The reference counter is incremented by one. The caller 
	is responsible to call cmpack_dark_destroy() when the reference is 
	no longer needed.
	\return pointer to a new reference
*/
	CMPACK_EXPORT(CmpackDarkCorr*, cmpack_dark_reference, (CmpackDarkCorr* ctx));

/**
	\brief Release a reference to the context
	\details The function releases a reference to the context. The reference 
	counter is decreased by one and when it was the last reference to the 
	file, the content of the disk file is updated and it is closed. The data
	are freed from the memory.
*/
	CMPACK_EXPORT(void, cmpack_dark_destroy, (CmpackDarkCorr* ctx));

/**
	\brief Attach console to the context
	\details The function connects a dark-frame correction context with
	a console context. The console is designed to print the information
	during the data processing. The functions makes its own reference
	to the console. Only one console can be attached to a single context, 
	if another console is attached to the single context, the original 
	one is released. Set console to NULL to release a reference to the 
	console that is currently attached to the context.
	\param[in] ctx			dark-frame correction context
	\param[in] con			console context
*/
	CMPACK_EXPORT(void, cmpack_dark_set_console, (CmpackDarkCorr* ctx, CmpackConsole* con));

/**
	\brief Enable and disable dark-frame scaling
	\details The function enables and disable the dark-frame scaling.
	If the scaling is enabled, the exposure duration of the input
	CCD frame and the exposure duration of the correction frame are 
	compared and the correction frame is scaled to match the duration
	of the corrected frame. The dark-frame must be scalable.
	\param[in] ctx			dark-frame correction context
	\param[in] scaling		0 = disable, 1 = enable feature
*/
	CMPACK_EXPORT(void, cmpack_dark_set_scaling, (CmpackDarkCorr* ctx, int scaling));

/**
	\brief Get status of dark-frame scaling
	\details The function returns current status of the "scaling" flag
	\param[in] ctx			dark-frame correction context
	\return nonzero if scaling is enable, zero if it is disabled
*/
	CMPACK_EXPORT(int, cmpack_dark_get_scaling, (CmpackDarkCorr* ctx));

/**
	\brief Set image border size
	\details The function sets the size of the CCD frame border. If you set the 
	border to nonzero size, the bias will set pixels that are inside the
	border area to zero. You can use this feature to skip an unusable part 
	of a frame.
	\param[in] file			dark correction context
	\param[in] border		border size in pixels
*/
	CMPACK_EXPORT(void, cmpack_dark_set_border, (CmpackDarkCorr* file, const CmpackBorder* border));

/**
	\brief Get image border size
	\details The function returns current border size
	\param[in] file			dark correction context
	\param[out] border		border size in pixels
*/
	CMPACK_EXPORT(void, cmpack_dark_get_border, (CmpackDarkCorr* file, CmpackBorder* border));

/**
	\brief Set pixel value thresholds
	\details The function sets the value of two thresholds that determine 
	valid pixel values. Pixels of value <= minvalue are treated as bad pixels, 
	pixels of value >= maxvalue are treated as overexposed pixels.
	\param[in] ctx			dark correction context
	\param[in] minvalue		bad pixel threshold
	\param[in] maxvalue		overexposed pixel threshold
*/
	CMPACK_EXPORT(void, cmpack_dark_set_thresholds, (CmpackDarkCorr* ctx, double minvalue, double maxvalue));

/**
	\brief Set threshold for "invalid" pixel value
	\details The function sets the value of threshold that determines 
	valid pixel values. Pixels of value <= minvalue are treated as bad pixels.
	\param[in] ctx			dark correction context
	\param[in] minvalue		bad pixel threshold
*/
	CMPACK_EXPORT(void, cmpack_dark_set_minvalue, (CmpackDarkCorr* ctx, double minvalue));

/**
	\brief Get threshold for "invalid" pixel values
	\param[in] ctx			dark correction context
	\return pixel value
*/
	CMPACK_EXPORT(double, cmpack_dark_get_minvalue, (CmpackDarkCorr* ctx));

/**
	\brief Set threshold for "overexposed" pixels
	\details The function sets the value of threshold that determines 
	valid pixel values. Pixels of value >= maxvalue are treated as overexposed pixels.
	\param[in] ctx			dark correction context
	\param[in] maxvalue		overexposed pixel threshold
*/
	CMPACK_EXPORT(void, cmpack_dark_set_maxvalue, (CmpackDarkCorr* ctx, double maxvalue));

/**
	\brief Get threshold for "overexposed" pixels
	\param[in] ctx			dark correction context
	\return pixel value
*/
	CMPACK_EXPORT(double, cmpack_dark_get_maxvalue, (CmpackDarkCorr* ctx));

/**
	\brief Load dark-frame from a file
	\details The function reads the correction frame from the given frame 
	context. The internal copy of the image data is made, no reference to 
	the frame is held, so you can free it when this function has returned.
	\param[in] ctx			dark-frame correction context
	\param[in] darkfile		dark frame context
	\return zero on success or error code on failure
*/
	CMPACK_EXPORT(int, cmpack_dark_rdark, (CmpackDarkCorr* ctx, CmpackCcdFile* darkfile));

/**
	\brief Execute dark-frame correction on a file
	\details The function reads image data from the context, performs
	the dark correction and stores the output image to the same context.
	\param[in] ctx			dark-frame correction context
	\param[in,out] file		frame context
	\return zero on success or error code on failure
*/
	CMPACK_EXPORT(int, cmpack_dark, (CmpackDarkCorr* ctx, CmpackCcdFile* file));

/**
	\brief Execute dark-frame correction on a file
	\details The function reads image data from the infile context, performs
	the dark correction and stores the output image to the outfile context.
	\param[in] ctx			dark-frame correction context
	\param[in] infile		input frame context
	\param[in] outfile		output frame context
	\return zero on success or error code on failure
*/
	CMPACK_EXPORT(int, cmpack_dark_ex, (CmpackDarkCorr* ctx, CmpackCcdFile* infile, CmpackCcdFile* outfile));

#ifdef __cplusplus
}
#endif

#endif
