/**
	\file
	\brief Functions for reading and writing output data files (tables).

	Output data files contain data in form of table. It is used for
	storing light curves, track lists, etc.

	Use cmpack-list object to process a set of photometry files and 
	make output data. This format was inherited from original Munipack,
	but it's been extended to support saving a set of parameters 
	(file header).
	
	\author David Motl <dmotl@volny.cz>
	
	\par Copying
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation, version 2.
	
	$Id: cmpack_table.h,v 1.2 2016/05/27 13:30:00 dmotl Exp $
*/

#ifndef _CMPACK_TABLE_H_INCLUDED
#define _CMPACK_TABLE_H_INCLUDED

#include "cmpack_common.h"

/*****************    Private data structures    ***************************/

/**
	\brief Table context
	\details This private structure holds the table header and data
*/
typedef struct _CmpackTable CmpackTable;

/** \brief Table type identifiers */
typedef enum _CmpackTableType
{
	CMPACK_TABLE_UNSPECIFIED,	/**< Unspecified type */
	CMPACK_TABLE_LCURVE_DIFF,	/**< Differential light curve (V-C, ...) */
	CMPACK_TABLE_LCURVE_INST,	/**< Instrumental absolute magnitudes (V, C, ...) */
	CMPACK_TABLE_MAGDEV,		/**< Mag-dev graph, result of the Munifind tool */
	CMPACK_TABLE_TRACKLIST,		/**< Track-list, frame offsets */
	CMPACK_TABLE_APERTURES,		/**< Apertures and standard deviations */
	CMPACK_TABLE_AIRMASS,		/**< Air-mass coefficients (JD, ALTITUDE, AIRMASS) */
	CMPACK_TABLE_CCD_TEMP,		/**< CCD temperatures (JD, CCDTEMP) */
	CMPACK_TABLE_OBJ_PROP		/**< Object properties (JD, X, Y, SKY, FWHM, MAG) */
} CmpackTableType;

/** \brief Parameter flags */
typedef enum _CmpackTabColumnMask
{
	CMPACK_TM_NAME			= (1<<0),	/**< Colum name */
	CMPACK_TM_TYPE_PREC		= (1<<1),	/**< Data type and precision */
	CMPACK_TM_NULVAL		= (1<<2),	/**< Null value */
	CMPACK_TM_LIMITS		= (1<<3),	/**< Limits */
	CMPACK_TM_RANGE			= (1<<4)	/**< Actual data range */
} CmpackTabColumnMask;

/** \brief Object properties */
typedef struct _CmpackTabColumn
{
	char *name;					/**< Name of the column */
	CmpackType dtype;			/**< Type of stored values */
	int prec;					/**< Precision, number of decimal places */
	double limit_min;			/**< Minimum valid value */
	double limit_max;			/**< Maximum valid value */
	double nul_value;			/**< Undefined value */
	int range_valid;			/**< Range is valid */
	double range_min;			/**< Minimum value */
	double range_max;			/**< Maximum value */
} CmpackTabColumn;	

/********************   Public functions   *********************************/

#ifdef __cplusplus
extern "C" {
#endif

/**
	\brief Create a new table
	\details The function allocated memory for a new table and returns 
	a reference to it. The reference counter is set to one. The caller is 
	responsible to call cmpack_tab_destroy() when it is no longer needed.
	\param[in] type			table type
	\return pointer to a new reference
*/
	CMPACK_EXPORT(CmpackTable*, cmpack_tab_init, (CmpackTableType type));

/**
	\brief Make a new reference to the existing table
	\details The function makes a new reference to the table and returns 
	a pointer to it. The reference counter is incremented by one. The caller 
	is responsible to call cmpack_tab_destroy() when the reference is 
	no longer needed.
	\param[in] tab			table context
	\return pointer a new reference
*/
	CMPACK_EXPORT(CmpackTable*, cmpack_tab_reference, (CmpackTable* tab));

/**
	\brief Release a reference to the table
	\details The function releases a reference to the table. The reference 
	counter is decreased by one and when it was the last reference to the 
	file, the frame set is released from the memory.
	\param[in] tab			table context
*/
	CMPACK_EXPORT(void, cmpack_tab_destroy, (CmpackTable* tab));

/**
	\brief Load a table from a file
	\details The function parses the specified file and loads
	a table from it into memory. 
	\param[out] tab			table context
	\param[in] filename		path + file name
	\param[in] flags 		flags (see CMPACK_LOAD_xxx constants)
	\return zero on success, error code on failure
*/
	CMPACK_EXPORT(int, cmpack_tab_load, (CmpackTable** tab, const char* filename, int flags));

/**
	\brief Save a table to a file
	\details The function saves the table to the specified file.
	\param[in] tab			table context
	\param[in] filename		path + file name
	\param[in] flags 		flags (see CMPACK_SAVE_xxx constants)
	\param[in] columns		list of column indices that are exported to the file
	\param[in] ncolumns		number of columns
	\return zero on success, error code on failure
*/
	CMPACK_EXPORT(int, cmpack_tab_save, (CmpackTable* tab, const char* filename, int flags,
		const int* columns, int ncolumns));

/**
	\brief Clear table definition, header and data
	\details The function clears the content of the table. All rows,
	columns and header fields are deleted. The type of the table is 
	set to CMPACK_TABLE_UNKNOWN.
	\param[in] tab			table context
*/
	CMPACK_EXPORT(void, cmpack_tab_clear, (CmpackTable* tab));

/**
	\brief Copy a table
	\details Makes a copy of a source table to a destination table.
	All content of the destination table will be deleted first.
	\param[in] dsttab		destination table context
	\param[in] srctab		source table context
	\return zero on success, error code on failure
*/
	CMPACK_EXPORT(int, cmpack_tab_copy, (CmpackTable* dsttab, const CmpackTable* srctab));

/**
	\brief Set table type
	\details The function changes the type of the table
	\param[in] tab			table context
	\param[in] type			table type identifier
*/
	CMPACK_EXPORT(void, cmpack_tab_set_type, (CmpackTable* tab, CmpackTableType type));

/**
	\brief Get table type
	\details The function returns the type of the table
	\return the integer value that represents the type of the table
*/
	CMPACK_EXPORT(CmpackTableType, cmpack_tab_get_type, (CmpackTable* tab));

/**
	\brief Test if given file seems to be of one of supported table formats
	\details The function reads the beginning of the file and
	examines them to check if the file is a valid table or not.
	This function is used for file format autodetection.
	\param[in] 	filename		path + file name
	\return nonzero if the file is a valid table, zero otherwise
*/
	CMPACK_EXPORT(int, cmpack_tab_test, (const char* filename));

/** 
	\brief Test memory buffer if it seems to be a valid table
	\details The function examines the given memory buffer to check
	if the buffer contains a beginning of a valid table or not.
	This function is used for file format autodetection.
	\param[in] 	buffer			data to be examined
	\param[in]	buflen			number of bytes in the buffer
	\param[in]	filesize		real size of the complete file in bytes
	\return nonzero if the file is a valid table, zero otherwise
*/
	CMPACK_EXPORT(int, cmpack_tab_test_buffer, (const char* buffer, int buflen, int filesize));

/**********************   Table header   *****************************/

/**
	\brief Set parameter value in file header (string)
	\details The function updates a field in the table header or
	creates a new field. The function makes an internal copy of
	the given value. Keys are case sensitive.
	\param[in] tab			table context
	\param[in] key			parameter name
	\param[in] val			parameter value
*/
	CMPACK_EXPORT(void, cmpack_tab_pkys, (CmpackTable* tab, const char* key, const char* val));

/**
	\brief Set parameter value in file header (integer)
	\details The function updates a field in the table header or
	creates a new field. Keys are case sensitive.
	\param[in] tab			table context
	\param[in] key			parameter name
	\param[in] val			parameter value
*/
	CMPACK_EXPORT(void, cmpack_tab_pkyi, (CmpackTable* tab, const char* key, int val));

/**
	\brief Set parameter value in file header (integer)
	\details The function updates a field in the table header or
	creates a new field. Keys are case sensitive.
	\param[in] tab			table context
	\param[in] key			parameter name
	\param[in] val			parameter value
	\param[in] prec			number of decimal places
*/
	CMPACK_EXPORT(void, cmpack_tab_pkyd, (CmpackTable* tab, const char* key, double val, int prec));

/**
	\brief Get value of parameter in file header (string)
	\details The function returns a pointer to the parameter value
	stored in the table header or NULL if the field is undefined. 
	The caller must not modify or free it.
	\param[in] tab			table context
	\param[in] key			parameter name
	\return pointer to internal buffer or NULL on failure
*/
	CMPACK_EXPORT(const char*, cmpack_tab_gkys, (CmpackTable* tab, const char* key));

/**
	\brief Get value of parameter in file header (integer)
	\details The function finds a record in the file header, and retrieves
	its value as an integer number through the 'value' parameter.
	If the record doesn't exist or its value cannot be converted
	into an integer number, an error code is returned.
	Keys are case sensitive.
	\param[in] tab			table context
	\param[in] key			parameter name
	\param[out] value		parameter value
	\return zero on success, error code on failure
*/
	CMPACK_EXPORT(int, cmpack_tab_gkyi, (CmpackTable* tab, const char* key, int* value));

/**
	\brief Get value of parameter in file header (double)
	\details The function finds a record in the file header, and retrieves
	its value as a boolean value through the 'value' parameter.
	If the record doesn't exist or its value cannot be converted
	into a boolean value, an error code is returned.
	Keys are case sensitive.
	\param[in] tab			table context
	\param[in] key			parameter name
	\param[out] value		parameter value
	\return zero on success, error code on failure
*/
	CMPACK_EXPORT(int, cmpack_tab_gkyd, (CmpackTable* tab, const char* key, double* value));

/**
	\brief Get number of parameters from the file header
	\details The function returns the number of records that
	are stored in the table header.
	\param[in] tab			table context
	\return number of parameter or zero on failure
*/
	CMPACK_EXPORT(int, cmpack_tab_nkey, (CmpackTable* tab));

/**
	\brief Get the parameter from file header by its index
	\details Index of the first parameter is zero. The output
	parameters receive the pointers to internal buffers, the 
	caller must not modify or free it. You can set the value 
	of output parameter to NULL if you do not need to read it.
	\param[in] tab			table context
	\param[in] index		parameter index (starting by zero)
	\param[out] key			parameter name (can be NULL)
	\param[out] val			parameter value (can be NULL)
	\return zero on success, error code on failure
*/
	CMPACK_EXPORT(int, cmpack_tab_gkyn, (CmpackTable* tab, int index, const char** key, const char** val));

/**
	\brief Delete parameter from table header
	\details The function deletes a parameter from the table header.
	Keys are case sensitive.
	\param[in] tab			table context
	\param[in] key			parameter name
*/
	CMPACK_EXPORT(void, cmpack_tab_dkey, (CmpackTable* tab, const char* key));

/*******************   Table definition   ***************************/

/**
	\brief Get number of columns
	\details The function returns the number of columns defined
	in the table.
	\param[in] tab			table context
	\return number of columns or zero on failure
*/
	CMPACK_EXPORT(int, cmpack_tab_ncolumns, (CmpackTable* tab));

/**
	\brief Append new column to the table
	\details The function creates a new column in the table.
	The new column is append to the end of the table. This function
	takes a structure with column parameters.
	\param[in] tab			table context
	\param[in] mask			which fields are valid in the 'info'
	\param[in] info			column parameters
	\return index of new column or negative value on failure
*/
	CMPACK_EXPORT(int, cmpack_tab_add_column, (CmpackTable* tab, unsigned mask, CmpackTabColumn* info));

/**
	\brief Append new column that stores integer numbers to the table
	\details The function creates a new column in the table.
	This is convenient function to create a column that contains
	integer numbers.
	\param[in] tab			table context
	\param[in] name			column name
	\param[in] min			minimum value
	\param[in] max			maximum value
	\param[in] nulval		value that represents an undefined value
	\return index of new column or negative value on failure
*/
	CMPACK_EXPORT(int, cmpack_tab_add_column_int, (CmpackTable* tab, const char* name,
		int min, int max, int nulval));

/**
	\brief Append new column that stores real numbers to the table
	\details The function creates a new column in the table.
	This is convenient function to create a column that contains
	real numbers.
	\param[in] tab			table context
	\param[in] name			column name
	\param[in] prec			number of decimal places
	\param[in] min			minimum value
	\param[in] max			maximum value
	\param[in] nulval		value that represents an undefined value
	\return index of new column or negative value on failure
*/
	CMPACK_EXPORT(int, cmpack_tab_add_column_dbl, (CmpackTable* tab, const char* name,
		int prec, double min, double max, double nulval));

/**
	\brief Append new column that stores strings to the table
	\details The function creates a new column in the table.
	This is convenient function to create a column that contains
	strings.
	\param[in] tab			table context
	\param[in] name			column name
	\return index of new column or negative value on failure
*/
	CMPACK_EXPORT(int, cmpack_tab_add_column_str, (CmpackTable* tab, const char* name));

/**
	\brief Get column parameters
	\details The function retrieves column parameter.
	The column is indicated by its index.
	\param[in] tab			table context
	\param[in] index		column index
	\param[in] mask			which fields shall be retrieved?
	\param[out] info		column parameters
	\return zero on success, error code on failure
*/
	CMPACK_EXPORT(int, cmpack_tab_get_column, (CmpackTable* tab, int index, unsigned mask,
		CmpackTabColumn* info));

/**
	\brief Set column parameters
	\details The function modifies column parameters.
	The column is indicated by its index.
	\param[in] tab			table context
	\param[in] index		column index
	\param[in] mask			which fields shall be modified?
	\param[in] info			column parameters
*/
	CMPACK_EXPORT(void, cmpack_tab_set_column, (CmpackTable* tab, int index, unsigned mask,
		const CmpackTabColumn* info));

/**
	\brief Find column by its name
	\details The function searches for a column in the table
	which has specified name. The function returns an index
	to the table of columns or -1 if the column does not exist.
	\param[in] tab			table context
	\param[in] name			column name
	\return index of table column or negative value on failure
*/
	CMPACK_EXPORT(int, cmpack_tab_find_column, (CmpackTable* tab, const char* name));

/**************************   Table data   ***************************/

/**
	\brief Get number of rows in the table
	\details The function returns the number of rows stored
	in the table.
	\param[in] tab			table context
	\return number of rows or zero on failure.
*/
	CMPACK_EXPORT(int, cmpack_tab_nrows, (CmpackTable* tab));

/**
	\brief Set actual frame to the first row in the table
	\details The function sets the active row in the frame set
	to the first row. If the table is empty it returns 
	CMPACK_ERR_OUT_OF_RANGE value, on success it returns zero.
	\param[in] tab			table context
	\return zero on success, error code on failure
*/
	CMPACK_EXPORT(int, cmpack_tab_rewind, (CmpackTable* tab));

/**
	\brief Move to the next row in the table
	\details The function sets the active row in the table
	to the next row. If the last row was active, the function
	returns CMPACK_ERR_OUT_OF_RANGE value, on success it returns zero.
	\param[in] tab			table context
	\return zero on success, error code on failure
*/
	CMPACK_EXPORT(int, cmpack_tab_next, (CmpackTable* tab));

/**
	\brief Move frame pointer to a n-th row in the table
	\details The function sets the active row to a row indicated
	by its index, starting from zero. If such frame does not exist,
	the function returns CMPACK_ERR_OUT_OF_RANGE value, on success 
	it returns zero.
	\param[in] tab			table context
	\param[in] row			row index
	\return zero on success, error code on failure
*/
	CMPACK_EXPORT(int, cmpack_tab_setpos, (CmpackTable* tab, int row));

/**
	\brief Is the active row valid?
	\details The function returns nonzero value if the active 
	row in the table refers to a valid row. The function is used 
	in conjuction with cmpack_tab_first() and cmpack_tab_next() 
	to iterate over frames in the set.
	\param[in] tab			table context
	\return nonzero if the active row is a valid row, zero otherwise
*/
	CMPACK_EXPORT(int, cmpack_tab_eof, (CmpackTable* tab));

/**
	\brief Add a new row to the table
	\details The function inserts a new row to the table. All fields
	of the new row will be undefined. The new frame becomes an active row.
	\param[in] tab			table context
*/
	CMPACK_EXPORT(void, cmpack_tab_append, (CmpackTable* tab));

/**
	\brief Delete active row
	\details The function deletes the active row from the table.
	The next frame becomes an active row, if possible. When the last 
	frame is deleted from the table, no row will be active.
	\param[in] tab			table context
	\return zero on success, error code on failure
*/
	CMPACK_EXPORT(int, cmpack_tab_delete, (CmpackTable* tab));
	
/**
	\brief Set table data (integer number)
	\details The function modifies table data in active row.
	\param[in] tab			table context
	\param[in] col			column index
	\param[in] val			new value
*/
	CMPACK_EXPORT(void, cmpack_tab_ptdi, (CmpackTable* tab, int col, int val));

/**
	\brief Set table data (real number)
	\details The function modifies table data in active row.
	\param[in] tab			table context
	\param[in] col			column index
	\param[in] val			new value
*/
	CMPACK_EXPORT(void, cmpack_tab_ptdd, (CmpackTable* tab, int col, double val));

/**
	\brief Set table data (string)
	\details The function modifies table data in active row.
	\param[in] tab			table context
	\param[in] col			column index
	\param[in] val			new value
*/
	CMPACK_EXPORT(void, cmpack_tab_ptds, (CmpackTable* tab, int col, const char* val));

/**
	\brief Get table data (integer number)
	\details The function retrieves data from active row.
	\param[in] tab			table context
	\param[in] col			column index
	\param[out] val			value
	\return nonzero on success, zero on failure
*/
	CMPACK_EXPORT(int, cmpack_tab_gtdi, (CmpackTable* tab, int col, int* val));

/**
	\brief Get table data (real number)
	\details The function retrieves data from active row.
	\param[in] tab			table context
	\param[in] col			column index
	\param[out] val			value
	\return nonzero on success, zero on failure
*/
	CMPACK_EXPORT(int, cmpack_tab_gtdd, (CmpackTable* tab, int col, double* val));

/**
	\brief Get table data (string)
	\details The function retrieves data from active row. The function
	can be used only to string-type columns
	\param[in] tab			table context
	\param[in] col			column index
	\param[out] val			value (nul-terminated, newly allocated)
	\return nonzero on success, zero on failure
*/
	CMPACK_EXPORT(int, cmpack_tab_gtds, (CmpackTable* tab, int col, char** val));

/** \brief Find row by value
	\details The function compares a stored value in an indicated
	column to a value specified in the last parameter and when it
	matches, returns nonzero value. Otherwise, continues to the 
	next row etc. If there are no other rows, returns zero. 
	When the function returns with nonzero result, the active row
	contains the specified value in the column. When it returns
	with zero result, the value was not found and no row is active.
	\param[in] tab			table context
	\param[in] col			column index
	\param[in] value		value
	\returns nonzero on success, zero on failure
*/
	CMPACK_EXPORT(int, cmpack_tab_ftdi, (CmpackTable* tab, int col, int value));

#ifdef __cplusplus
}
#endif

#endif
