/**
	\file
	\brief Console related functions

	C-Munipack functions use a console as an output device for
	output and debug messages. By default, all messages are thrown away.
	If you want to print them to standard error stream or process them
	in a user defined callback, make a console and attach it to an object
	using cmpack_*_set_console function.

	\author David Motl <dmotl@volny.cz>
	
	\par Copying
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation, version 2.
	
	$Id: cmpack_console.h,v 1.1 2015/07/06 08:33:22 dmotl Exp $
*/

#ifndef _CMPACK_CONSOLE_H_INCLUDED
#define _CMPACK_CONSOLE_H_INCLUDED

#include "cmpack_config.h"

/********************   Public constants   ********************************/

/** \brief Verbosity level */
typedef enum _CmpackOutputLevel
{
	CMPACK_LEVEL_QUIET,			/**< Be quiet, suppress all messages */
	CMPACK_LEVEL_NORMAL,		/**< Print messages, suppress debug output (default) */
	CMPACK_LEVEL_DEBUG			/**< Be verbose, print all messages including debug ones */
} CmpackOutputLevel;

/********************   Public data types   ********************************/

/**
	\brief Callback procedure
	\details Prototype for custom callback procedures. The procedure
	receives the pointer to message string. It points to internal memory
	buffer, the callee must not free or modify it. The second parameter 
	contains callback registration data.
	\param[in] text			text to be printed to output device
	\param[in] user_data	callback registration data
*/
typedef void CmpackCallbackType(const char *text, void *user_data);

/**	
	\brief Console context 
	\details This private structure holds the console parameters
*/
typedef struct _CmpackConsole CmpackConsole;

/******************   Public functions   ***************************/

#ifdef __cplusplus
extern "C" {
#endif

/**
	\brief Create a new console (for 'stderr' stream)
	\details The function makes a new console that prints the
	information to the standard error stream. This is intended 
	to be used in the console applications.
	\return pointer to context of zero if failed
*/
	CMPACK_EXPORT(CmpackConsole*, cmpack_con_init, (void));

/**
	\brief Create a new console (user-defined processing)
	\details The function makes a new console that sends the output
	to a user-defined callback function. The second parameter is passed
	to the callback function without any processing. The caller can use
	this to pass a context to the callback.
	\return pointer to context of zero if failed
*/
	CMPACK_EXPORT(CmpackConsole*, cmpack_con_init_cb, (CmpackCallbackType* cbproc, void* cbdata));

/**
	\brief Make a new reference to the console
	\details The function makes a new reference to the file and returns a 
	pointer to it. The reference counter is incremented by one. The caller 
	is responsible to call cmpack_con_destroy() when the reference is 
	no longer needed.
	\return pointer to a new reference
*/
	CMPACK_EXPORT(CmpackConsole*, cmpack_con_reference, (CmpackConsole* con));

/**
	\brief Release a reference to the console
	\details The function releases a reference to the console. The reference 
	counter is decreased by one and when it was the last reference to the 
	file, the content of the disk file is updated and it is closed. The data
	are freed from the memory.
*/
	CMPACK_EXPORT(void, cmpack_con_destroy, (CmpackConsole* con));

/**
	\brief Set the debug level
	\details The function sets the debug level to the console. Each message
	has an integer number that indicates the level of the message. The console
	filters out the messages with level lower that the debug level.
	\param[in] ctx			any context
	\param[in] level		verbosity level (see CMPACK_LEVEL_xxx constants)
*/
	CMPACK_EXPORT(void, cmpack_con_set_level, (CmpackConsole* ctx, CmpackOutputLevel level));

#ifdef __cplusplus
}
#endif

#endif
