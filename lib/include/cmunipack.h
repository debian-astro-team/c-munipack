/**
	\file
	\brief Includes all other header files.

	This header don't define anythink particular. It includes all
	other header files from the C-Munipack library and its provided
	only for user's convenience.

	\author David Motl <dmotl@volny.cz>
	
	\par Copying
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation, version 2.
	
	$Id: cmunipack.h,v 1.1 2015/07/06 08:33:22 dmotl Exp $
*/
#ifndef _CMUNIPACK_H_INCLUDED
#define _CMUNIPACK_H_INCLUDED

#include <cmpack_adcurve.h>
#include <cmpack_amass.h>
#include <cmpack_bias.h>
#include <cmpack_catfile.h>
#include <cmpack_ccdfile.h>
#include <cmpack_common.h>
#include <cmpack_console.h>
#include <cmpack_dark.h>
#include <cmpack_flat.h>
#include <cmpack_fset.h>
#include <cmpack_hcorr.h>
#include <cmpack_image.h>
#include <cmpack_kombine.h>
#include <cmpack_konve.h>
#include <cmpack_lcurve.h>
#include <cmpack_match.h>
#include <cmpack_mbias.h>
#include <cmpack_mdark.h>
#include <cmpack_mfind.h>
#include <cmpack_mflat.h>
#include <cmpack_phot.h>
#include <cmpack_phtfile.h>
#include <cmpack_table.h>
#include <cmpack_tcorr.h>
#include <cmpack_tcurve.h>
#include <cmpack_version.h>
#include <cmpack_wcs.h>
#include <cmpack_xml.h>

#endif
