/**
	\file
	\brief Functions for reading and writing catalog files

	Catalog files contain information relative position
	of stars and position of variable, comparison and check
	stars. It is designed to simplify the final phase of the 
	reduction process. The application loads the selection
	from a catalog file and user only checks and confirms it.

	The catalog file is a XML document, see the Reference
	manual for its document type definition.

	\author David Motl <dmotl@volny.cz>
	
	\par Copying
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation, version 2.
	
	$Id: cmpack_catfile.h,v 1.3 2016/05/19 20:47:18 dmotl Exp $
*/

#ifndef _CMPACK_CATFILE_H_INCLUDED
#define _CMPACK_CATFILE_H_INCLUDED

#include "cmpack_phtfile.h"
#include "cmpack_wcs.h"

/*****************    Data types    ***************************/

/**	
	\brief Catalog file context 
	\details This private structure holds the content of the catalog file.
*/
typedef struct _CmpackCatFile CmpackCatFile;

/** \brief Object properties */
typedef struct _CmpackCatObject
{
	int		id;					/**< Object identifier */
	double	center_x;			/**< Centroid x-coordinate in pixels */
	double	center_y;			/**< Centroid y-cooedinate in pixels */
	int		refmag_valid;		/**< Reference magnitude is valid */
	double	refmagnitude;		/**< Brightness on a reference frame */
} CmpackCatObject;

/** \brief Object parameter flags */
typedef enum _CmpackCatParamMask
{
	CMPACK_OM_ID			= (1<<0),	/**< Fields: id (read only) */
	CMPACK_OM_MAGNITUDE		= (1<<1),	/**< Fields: refmag_valid, refmagnitude */
	CMPACK_OM_CENTER		= (1<<2)	/**< Fields: center_x, center_y */
} CmpackCatParamMask;

/********************   Functions   **************************/

#ifdef __cplusplus
extern "C" {
#endif

/**
	\brief Create a new memory-only catalog file
	\details The function creates a new catalog file and returns a new 
	reference to it. This file is designed to store data temporarily
	in the memory. To open a file from the disk or create a permanent
	file, use cmpack_cat_open() function instead. The reference counter 
	is set to one. The caller is responsible to call cmpack_cat_close() 
	or cmpack_cat_destroy() when it is no longer needed.
	\return pointer to a new reference
*/
	CMPACK_EXPORT(CmpackCatFile*, cmpack_cat_new, (void));

/**
	\brief Make a new reference to the catalog file
	\details The function makes a new reference to the file and returns a 
	pointer to it. The reference counter is incremented by one. The caller 
	is responsible to call cmpack_cat_destroy() when the reference is 
	no longer needed.
	\param[in]	file		file context
	\return pointer to a new reference
*/
	CMPACK_EXPORT(CmpackCatFile*, cmpack_cat_reference, (CmpackCatFile* file));

/**
	\brief Release a reference to the catalog file.
	\details The function releases a reference to the file. The reference 
	counter is decreased by one and when it was the last reference to the 
	file, the content of the disk file is updated and it is closed. The data
	are freed from the memory.
	\param[in]	file		file context
*/
	CMPACK_EXPORT(void, cmpack_cat_destroy, (CmpackCatFile* file));

/**
	\brief Open or create a catalog file stored on the disk.
	\details The function opens an existing catalog file stored on the
	disk or creates a new file. 
	\param[out]	file			new file context
	\param[in] 	filename		path + file name
	\param[in]	mode			opening mode (see CMPACK_OPEN_xxx constants)
	\param[in] 	flags 			not used, should be 0
	\return zero on success, error code on failure
*/
	CMPACK_EXPORT(int, cmpack_cat_open, (CmpackCatFile** file, const char* filename,
		CmpackOpenMode mode, unsigned flags));

/**
	\brief Close catalog file and release the reference
	\details The function updates the content of the disk file and 
	closes the file. The context is then switched to the read-only
	mode, so it is possible in some cases to retrieve the information, 
	but the file cannot be modified. The function also decrements the 
	reference counter and when it was the last reference, the memory 
	is freed. In that case, the context is no longer valid and it is 
	not possible to retrieve the information from it.
	\param[in]	file			file context
	\return zero on success, error code on failure
*/
	CMPACK_EXPORT(int, cmpack_cat_close, (CmpackCatFile* file));

/**
	\brief Copy content of the catalog file 
	\details The function makes copy of the source catalog file
	and to the target catalog file. If the target file contains
	any information, it is overwritten.
	\param[in] dstfile			target file
	\param[in] srcfile			source file
	\return zero on success, error code on failure
*/
	CMPACK_EXPORT(int, cmpack_cat_copy, (CmpackCatFile* dstfile, const CmpackCatFile* srcfile));

/**
	\brief Clear the catalog file
	\details The function deletes all defined objects and sets all header
	fields to default values.
	\param[in]	file			catalog file 
*/
	CMPACK_EXPORT(void, cmpack_cat_clear, (CmpackCatFile* file));

/** 
	\brief Make catalog file from a photometry file
	\details The function reads the list of objects from the
	source photometry file and fills the table of object in
	the catalogue file. The header fields are updated as well
	when appropriate. The caller must specify an index of an
	aperture that is used to retrieve the brightness of a object.
	If the target file contains any objects, they are deleted
	first.
	\param[in] file				target catalogue file
	\param[in] srcfile			source photometry file
	\param[in] aperture			aperture index
	\return zero on success, error code on failure
*/
	CMPACK_EXPORT(int, cmpack_cat_make, (CmpackCatFile* file, CmpackPhtFile* srcfile, int aperture));

/** 
	\brief Test if given file seems to be a valid catalog file
	\details The function reads the beginning of the file and
	examines them to check if the file is a valid catalogue file or not.
	This function is used for file format autodetection.
	\param[in] 	filename		path + file name
	\return nonzero if the file is a valid catalog file, zero otherwise
*/
	CMPACK_EXPORT(int, cmpack_cat_test, (const char* filename));

/** 
	\brief Test memory buffer if it seems to be a valid catalog file
	\details The function examines the given memory buffer to check
	if the buffer contains a beginning of a valid catalogue file or not.
	This function is used for file format autodetection.
	\param[in] 	buffer			data to be examined
	\param[in]	buflen			number of bytes in the buffer
	\param[in]	filesize		real size of the complete file in bytes
	\return nonzero if the file is a valid catalog file, zero otherwise
*/
	CMPACK_EXPORT(int, cmpack_cat_test_buffer, (const char* buffer, int buflen, int filesize));

/**
	\brief Dump content of the catalogue file to a table
	\details The function makes a table of objects and their positions.
	\param[in] file				file context
	\param[out] table			new table with the light curve
	\param[in] con				where debug messages are printed to
	\param[in] flags			reserved for future use, set to zero
	\return zero on success or error code on failure
*/
	CMPACK_EXPORT(int, cmpack_cat_dump, (CmpackCatFile* file, CmpackTable** table, CmpackConsole* con, unsigned flags));

/************************** File header *******************************************/

/**
	\brief Set frame size in pixels
	\details The function updates the frame size in pixels. This information
	is used to draw a chart.
	\param[in] file 			file context
	\param[out] width			frame width in pixels (0 = not available)
	\param[out] height			frame height in pixels (0 = not available)
*/
	CMPACK_EXPORT(void, cmpack_cat_set_size, (CmpackCatFile* file, int width, int height));

/**
	\brief Get frame width in pixels
	\details The function returns width of the frame.
	\param[in] file 			file context
	\return width in pixels or zero on failure
*/
	CMPACK_EXPORT(int, cmpack_cat_get_width, (CmpackCatFile* file));

/**
	\brief Get frame height in pixels
	\details The function returns width of the frame.
	\param[in] file 			file context
	\return width in pixels or zero on failure
*/
	CMPACK_EXPORT(int, cmpack_cat_get_height, (CmpackCatFile* file));

/**
	\brief Set parameter value in file header (string)
	\details The function updates a field in the file header. 
	If the field does not exist, a new record in the file header 
	is created. Keys are case sensitive.
	\param[in] file			file context
	\param[in] key			parameter name
	\param[in] val			parameter value
	\param[in] com			comment (parameter description)
*/
	CMPACK_EXPORT(void, cmpack_cat_pkys, (CmpackCatFile* file, const char* key,
		const char* val, const char* com));

/**
	\brief Set parameter value in file header (integer)
	\details The function updates a field in the file header. 
	If the field does not exist, a new record in the file header 
	is created. Keys are case sensitive.
	\param[in] file			file context
	\param[in] key			parameter name
	\param[in] val			parameter value
	\param[in] com			comment (parameter description)
*/
	CMPACK_EXPORT(void, cmpack_cat_pkyi, (CmpackCatFile* file, const char* key,
		int val, const char* com));

/**
	\brief Set parameter value in file header (double)
	\details The function updates a field in the file header. 
	If the field does not exist, a new record in the file header 
	is created. Keys are case sensitive.
	\param[in] file			file context
	\param[in] key			parameter name
	\param[in] val			parameter value
	\param[in] prec			number of decimal places
	\param[in] com			comment (parameter description)
*/
	CMPACK_EXPORT(void, cmpack_cat_pkyd, (CmpackCatFile* file, const char* key,
		double val, int prec, const char* com));
	
/**
	\brief Retrieve value of parameter in file header (string)
	\details The function finds a record in the file header
	and returns its value as a string. The function returns 
	pointer to internal buffer, the caller must not modify or
	free it. If the record doesn't exist, it returns NULL.
	Keys are case sensitive.
	\param[in] file			file context
	\param[in] key			parameter name
	\return pointer to internal buffer or NULL
*/
	CMPACK_EXPORT(const char*, cmpack_cat_gkys, (CmpackCatFile* file, const char* key));

/**
	\brief Get value of parameter in file header (integer number)
	\details The function finds a record in the file header, and retrieves
	its value as an integer number through the 'value' parameter.
	If the record doesn't exist or its value cannot be converted
	into an integer number, an error code is returned.
	Keys are case sensitive.
	\param[in] file			file context
	\param[in] key			parameter name
	\param[out] value		current parameter value
	\return zero on success, nonzero on failure
*/
	CMPACK_EXPORT(int, cmpack_cat_gkyi, (CmpackCatFile* file, const char* key, int* value));

/**
	\brief Get value of parameter in file header (real number)
	\details The function finds a record in the file header, and retrieves
	its value as a real number through the 'value' parameter.
	If the record doesn't exist or its value cannot be converted
	into a real number, an error code is returned.
	Keys are case sensitive.
	\param[in] file			file context
	\param[in] key			parameter name
	\param[out] value		current parameter value
	\return zero on success, nonzero on failure
*/
	CMPACK_EXPORT(int, cmpack_cat_gkyd, (CmpackCatFile* file, const char* key, double* value));

/**
	\brief Get number of records stored in the file header
	\param[in] file			file context
	\return number of parameter or zero on failure.
*/
	CMPACK_EXPORT(int, cmpack_cat_nkey, (CmpackCatFile* file));

/**
	\brief Get parameter from the file header by index
	\details The function returns pointer to keyword, value and comment
	string that are stored in the file header on position that is indicated
	by the index parameter. Index of the first parameter is zero. The output
	parameters receive the pointers to internal buffers, the caller must
	not modify or free it. If the caller tries to read beyond the end of
	the file, the output variable are set to NULL and an error code is 
	returned.
	\param[in] file			file context
	\param[in] index		parameter index (starting by zero)
	\param[out] key			parameter name (can be NULL)
	\param[out] val			parameter value (can be NULL)
	\param[out] com			comment (can be NULL)
	\return zero on success, error code on failure
*/
	CMPACK_EXPORT(int, cmpack_cat_gkyn, (CmpackCatFile* file, int index, const char** key,
		const char** val, const char** com));

/**
	\brief Delete parameter from file header
	\details The function removes a record from the file header. The
	subsequent fields are moved by one position backwards, so the index
	of each record changes. Keys are case sensitive.
	\param[in] file			file context
	\param[in] key			parameter name
*/
	CMPACK_EXPORT(void, cmpack_cat_dkey, (CmpackCatFile* file, const char* key));

/*************************** Table of objects **********************************/

/**
	\brief Gets number of objects defined in the catalog file
	\details The function returns number of objects stored in
	the catalog file.
	\param[in] file			file context
	\return number of objects or zero on failure
*/
	CMPACK_EXPORT(int, cmpack_cat_nstar, (CmpackCatFile* file));

/**
	\brief Add a new object to the catalog file
	\details The function creates a new record in the table 
	of objects and assigns its properties. The new record is
	appended to the end of the table.
	\param[in] file			file context
	\param[in] mask			fields to be initialized
	\param[in] obj			object properties
	\return index of a new record or negative value on failure.
*/
	CMPACK_EXPORT(int, cmpack_cat_add_star, (CmpackCatFile* file, unsigned mask,
		const CmpackCatObject* obj));

/**
	\brief Find object by identifier
	\details The function finds an object record with specified
	object identifier and returns its index. When such record does
	not exist, the function returns -1.
	\param[in] file			file context
	\param[in] star_id		star identifier
	\return index of record or negative value on failure.
*/	
	CMPACK_EXPORT(int, cmpack_cat_find_star, (CmpackCatFile* file, int star_id));

/**
	\brief Get object properties
	\details The function retrieves object properties from 
	the record that is indicated by its index. When such record 
	does not exist, an error code is returned.
	\param[in] file			file context
	\param[in] index		record index
	\param[in] mask			fields to be retrieved
	\param[out] obj			object properties
	\return zero on success or error code on failure
*/
	CMPACK_EXPORT(int, cmpack_cat_get_star, (CmpackCatFile* file, int index, unsigned mask,
		CmpackCatObject* obj));

/**
	\brief Update object properties
	\details The function updates object properties, the record is 
	indicated by its index. When such record does not exist, an error 
	code is returned.
	\param[in] file			file context
	\param[in] index		record index
	\param[in] mask			fields to be retrieved
	\param[in] obj			object properties
	\return zero on success or error code on failure
*/
	CMPACK_EXPORT(int, cmpack_cat_set_star, (CmpackCatFile* file, int index, unsigned mask,
		const CmpackCatObject* obj));

/************************   Selection of stars   ****************************/

/**
	\brief Get number of selection lists
	\details The function retrieves the number of selection lists that 
	are defined in the catalog file. When a new catalog file is created,
	it contains one selection list which is made active.
	\return number of selection lists present in the catalog file
*/
	CMPACK_EXPORT(int, cmpack_cat_selection_set_count, (CmpackCatFile* file));

/**
	\brief Set active selection list
	\details The function sets the active selection list. All selection
	manipulating functions work with the active list. 
	\return zero on success or error code on failure
*/
	CMPACK_EXPORT(int, cmpack_cat_set_current_selection_set, (CmpackCatFile* file, int index));

/**
	\brief Remove all selection lists
	\details The function removes all selection lists from the file
	and create one empty selection list that is made active.
	\param[in] file			file context
	\return zero on success or error code on failure
*/
	CMPACK_EXPORT(int, cmpack_cat_clear_all_selections, (CmpackCatFile* file));

/**
	\brief Append a new selection
	\details The function create a new selection and appends it 
	to the end of the list. The new selection is made active.
	\param[in] file			file context
	\param[in] name			name of the selection (can be NULL)
	\param[in] position		index of position where to insert the new selection. If it is a  
							negative or it is equal to number of selections, the new selection 
							is appended to the end of the table of selection sets.
	\return zero on success or error code on failure
*/
	CMPACK_EXPORT(int, cmpack_cat_selection_set_new, (CmpackCatFile* file, const char* name, int position));

/**
	\brief Remove a selection
	\details The function removes the selection specified by its index.
	If you delete an active selection, no selection will be active.
	\param[in] file			file context
	\param[in] index		index of selection to be removed
	\return zero on success or error code on failure
*/
	CMPACK_EXPORT(int, cmpack_cat_selection_set_remove, (CmpackCatFile* file, int index));

/**
	\brief Set name of the active selection list
	\details The function sets the name of the active selection list.
	\param[in] file			file context
	\param[in] name			name of the selection (can be NULL)
	\return zero on success or error code on failure
*/
	CMPACK_EXPORT(int, cmpack_cat_set_selection_set_name, (CmpackCatFile* file, const char* name));

/**
	\brief Get name of the active selection list
	\details The function sets the name of the active selection list.
	\return zero on success or error code on failure
*/
	CMPACK_EXPORT(const char*, cmpack_cat_get_selection_set_name, (CmpackCatFile* file));

/**
	\brief Clear the list of selected objects
	\details The function clears the list of selected objects. Effectively,
	the selection type of all stars are set to CMPACK_SELECT_NONE.
	\param[in] file			file context
	\return zero on success or error code on failure
*/
	CMPACK_EXPORT(int, cmpack_cat_clear_selection, (CmpackCatFile* file));

/**
	\brief Set selection type of an object
	\details The function updates the table of selected objects. If the 
	record indicated by object identifier is already in the table and
	has the same type as indicated by 'type' parameter, it does nothing.
	If the object is already in the table but of different selection type,
	the selection record is moved to the end of the table and the selection
	type is updated. If the object is not present in the table a new record
	is created and appended to the end of the table.
	\param[in] file			file context
	\param[in] star_id		object identifier
	\param[in] type			new selection type
	\return zero on success or error code on failure
*/
	CMPACK_EXPORT(int, cmpack_cat_update_selection, (CmpackCatFile* file, int star_id,
		CmpackSelectionType type));

/**
	\brief Get number of selection record
	\details The function returns number of records in the selection
	table, this is the number of selected objects.
	\param[in] file			file context
	\return number of records or zero on failure.
*/
	CMPACK_EXPORT(int, cmpack_cat_get_selection_count, (CmpackCatFile* file));

/**
	\brief Get selection record by index
	\details The function retrieves the information from the record 
	indicated by the index. The information is stored to given output
	variables. When such record does not exist, an error code is 
	returned.
	\param[in] file			file context
	\param[in] rec_index	record index
	\param[out] star_id		object identifier
	\param[out] type		selection type
	\return zero on success, error code on failure
*/
	CMPACK_EXPORT(int, cmpack_cat_get_selection, (CmpackCatFile* file, int rec_index,
		int* star_id, CmpackSelectionType* type));

/**
	\brief Find selection record by object identifier
	\details The function finds a record in the table of selected
	objects by object identifier and returns index of the record.
	\param[in] file			file context
	\param[in] star_id		object identifier
	\return record index on success, negative value on failure.
*/
	CMPACK_EXPORT(int, cmpack_cat_find_selection, (CmpackCatFile* file, int star_id));

/************************   Object tags   ****************************/

/**
	\brief Clear all tags
	\details The function clears the table of tags.
	\param[in] file			file context
*/
	CMPACK_EXPORT(void, cmpack_cat_clear_tags, (CmpackCatFile* file));

/**
	\brief Add or update a tag
	\details The function updates the table of tags. If the record indicated 
	by object identifier is already in the table, it is updated. If the object 
	is not present in the table a new record is created and appended to the 
	table.
	It is also possible to remove a tag by setting its value to NULL.
	\param[in] file			file context
	\param[in] star_id		object identifier
	\param[in] tag			new tag value (NULL = remove tag)
	\return zero on success or error code on failure
*/
	CMPACK_EXPORT(int, cmpack_cat_update_tag, (CmpackCatFile* file, int star_id, const char* tag));

/**
	\brief Remove a tag
	\details The function removes a tag from an object. If the object 
	is not present in the table or if it does not have any tag, it does nothing. 
	\param[in] file			file context
	\param[in] star_id		object identifier
	\return zero on success or error code on failure
*/
	CMPACK_EXPORT(int, cmpack_cat_remove_tag, (CmpackCatFile* file, int star_id));

/**
	\brief Get number of tags defined
	\details The function returns number of records in the table of tags.
	\param[in] file			file context
	\return number of records or zero on failure.
*/
	CMPACK_EXPORT(int, cmpack_cat_get_tag_count, (CmpackCatFile* file));

/**
	\brief Get tag record by index
	\details The function retrieves the information from the record 
	indicated by the index. The information is stored to given output
	variables. When such record does not exist, an error code is 
	returned. The tag value is returned as a pointer to internal memory
	buffer. The caller should not free or modify it.
	\param[in] file			file context
	\param[in] rec_index	record index
	\param[out] star_id		object identifier
	\param[out] tag			tag value (nul-terminated string)
	\return zero on success, error code on failure
*/
	CMPACK_EXPORT(int, cmpack_cat_get_tag, (CmpackCatFile* file, int rec_index,
		int* star_id, const char** tag));

/**
	\brief Find tag record by object identifier
	\details The function finds a record in the table of tags 
	by object identifier and returns index of the record.
	\param[in] file			file context
	\param[in] star_id		object identifier
	\return record index on success, negative value on failure.
*/
	CMPACK_EXPORT(int, cmpack_cat_find_tag, (CmpackCatFile* file, int star_id));

/************************   WCS data   ****************************/

/**
	\brief Get World Coordinate System (WCS) data
	\details The function retrieves WCS data stored in the file. The data
	are returned as a new CmpackWcs object. If the file contains several 
	WCS data sets, all of them are retrieved. The returned object is a deep
	copy of internal data, use cmpack_cat_set_wcs() function to update
	the wcs data in the catalogue file.
	\returns zero on success, error code on failure
*/
	CMPACK_EXPORT(int, cmpack_cat_get_wcs, (CmpackCatFile* fc, CmpackWcs** wcs));

/**
	\brief Set World Coordinate System (WCS) data
	\details The function replaces any existing WCS data stored in the file.
	If the object contains several WCS data sets, all of them are preserved.
	An deep copy of the given object is made into the catalogue file, the 
	subsequent changes to the CmpackWcsData does not affect the catalogue file.
	\returns zero on success, error code on failure
*/
	CMPACK_EXPORT(int, cmpack_cat_set_wcs, (CmpackCatFile* fc, const CmpackWcs* wcs));

/**
	\brief Clear World Coordinate System (WCS) data
	\details The function removes any existing WCS data stored in the file.
*/
	CMPACK_EXPORT(void, cmpack_cat_clear_wcs, (CmpackCatFile* fc));

#ifdef __cplusplus
}
#endif

#endif
