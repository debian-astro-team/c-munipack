/**
	\file
	\brief Functions for the merging (combining) CCD frames

	Set of functions defined in this module allows user to 
	merge (combine) a set of CCD frames.
	
	\author David Motl <dmotl@volny.cz>
	
	\par Copying
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation, version 2.
	
	$Id: cmpack_kombine.h,v 1.1 2015/07/06 08:33:22 dmotl Exp $
*/
#ifndef _CMPACK_KOMBINE_H_INCLUDED
#define _CMPACK_KOMBINE_H_INCLUDED

#include "cmpack_common.h"
#include "cmpack_console.h"
#include "cmpack_ccdfile.h"
#include "cmpack_phtfile.h"

/********************   Private data structures   ********************************/

/**
	\brief Output frame context for kombine tool 
	\details This private data structure holds the accumulated
	data for single output frame.
*/
typedef struct _CmpackKombine CmpackKombine;

/**********************   Public constants   ***********************************/

/**
	\brief Output modifiers
*/
typedef enum _CmpackKombineFlags
{
	CMPACK_KOMBINE_DEFAULT = 0,			/**< Default behavior */
	CMPACK_KOMBINE_NOALIGN = (1 << 0)	/**< Do not transform input frames */
} CmpackKombineFlags;

/********************   Public functions   *************************************/

#ifdef __cplusplus
extern "C" {
#endif

/**
	\brief Make new 'kombine' context
	\details The function allocates memory with kombine context and returns a 
	new reference to it. The reference counter is set to one. The caller is 
	responsible to call cmpack_kombine_destroy() when it is no longer needed.
	\return pointer to a new reference
*/
	CMPACK_EXPORT(CmpackKombine*, cmpack_kombine_init, (void));

/**
	\brief Make a new reference to the 'kombine' context
	\details The function makes a new reference to the context and returns a 
	pointer to it. The reference counter is incremented by one. The caller 
	is responsible to call cmpack_kombine_destroy() when the reference is 
	no longer needed.
	\return pointer to a new reference
*/
	CMPACK_EXPORT(CmpackKombine*, cmpack_kombine_reference, (CmpackKombine* ctx));

/**
	\brief Release a reference to the 'kombine' context
	\details The function releases a reference to a 'kombine' context. 
	The reference counter is decreased by one and when it was the 
	last reference to the context, the context is freed and all memory 
	allocated in the context is reclaimed.
*/
	CMPACK_EXPORT(void, cmpack_kombine_destroy, (CmpackKombine* ctx));

/**
	\brief Attach console to the context
	\details The function connects a 'kombine' context with
	a console context. The console is designed to print the information
	during the data processing. The functions makes its own reference
	to the console. Only one console can be attached to a single context, 
	if another console is attached to the single context, the original 
	one is released. Set console to NULL to release a reference to the 
	console that is currently attached to the context.
	\param[in] ctx			kombine context
	\param[in] con			console context
*/
	CMPACK_EXPORT(void, cmpack_kombine_set_console, (CmpackKombine* ctx, CmpackConsole* con));

/**
	\brief Set output data format
	\details The function sets the format of the output CCD frame. If
	the format is set to CMPACK_BITPIX_AUTO (default), the format of the first
	source frame is used. Otherwise, the image data are converted into
	specified format.
	\param[in] ctx			output file context
	\param[in] bitpix		output data format
*/
	CMPACK_EXPORT(void, cmpack_kombine_set_bitpix, (CmpackKombine* ctx, CmpackBitpix bitpix));

/**
	\brief Get output data format
	\param[in] ctx			master-flat context
	\return current image data format
*/
	CMPACK_EXPORT(CmpackBitpix, cmpack_kombine_get_bitpix, (CmpackKombine* ctx));

/**
	\brief Set image border size
	\details The function sets the size of CCD frame borders. The pixels
	inside the frame borders are set always to zero. You can use this feature 
	to clear an unusable part of a frame.
	\param[in] ctx			kombine context
	\param[in] border		border size in pixels
*/
	CMPACK_EXPORT(void, cmpack_kombine_set_border, (CmpackKombine* ctx, const CmpackBorder* border));

/**
	\brief Get image border size
	\details The function returns current border size
	\param[in] ctx			master-bias context
	\param[out] border		border size in pixels
*/
	CMPACK_EXPORT(void, cmpack_kombine_get_border, (CmpackKombine* ctx, CmpackBorder* border));

/**
	\brief Set pixel value thresholds
	\details The function sets the value of two thresholds that determine 
	valid pixel values. Pixels of value <= minvalue are treated as bad pixels, 
	pixels of value >= maxvalue are treated as overexposed pixels.
	\param[in] ctx			kombine context
	\param[in] minvalue		bad pixel threshold
	\param[in] maxvalue		overexposed pixel threshold
*/
	CMPACK_EXPORT(void, cmpack_kombine_set_thresholds, (CmpackKombine* ctx, double minvalue, double maxvalue));

/**
	\brief Set threshold for "invalid" pixel value
	\details The function sets the value of threshold that determines 
	valid pixel values. Pixels of value <= minvalue are treated as bad pixels.
	\param[in] ctx			master-bias context
	\param[in] minvalue		bad pixel threshold
*/
	CMPACK_EXPORT(void, cmpack_kombine_set_minvalue, (CmpackKombine* ctx, double minvalue));

/**
	\brief Get threshold for "invalid" pixel values
	\param[in] ctx			master-bias context
	\return pixel value
*/
	CMPACK_EXPORT(double, cmpack_kombine_get_minvalue, (CmpackKombine* ctx));

/**
	\brief Set threshold for "overexposed" pixels
	\details The function sets the value of threshold that determines 
	valid pixel values. Pixels of value >= maxvalue are treated as overexposed pixels.
	\param[in] ctx			master-bias context
	\param[in] maxvalue		overexposed pixel threshold
*/
	CMPACK_EXPORT(void, cmpack_kombine_set_maxvalue, (CmpackKombine* ctx, double maxvalue));

/**
	\brief Get threshold for "overexposed" pixels
	\param[in] ctx			kombine context
	\return pixel value
*/
	CMPACK_EXPORT(double, cmpack_kombine_get_maxvalue, (CmpackKombine* ctx));

/**
	\brief Set processing flags
	\details The function modifies the flags listed in the 'flags' parameter by setting or clearing them,
	depending on the value of 'state' parameter. The current status of flags, after applying changes, is
	returned. The function can be also used to get the current status of flags by setting the 'flags' parameter 
	to zero.
	\param[in] ctx			kombine context
	\param[in] flags		packed bitmap of flags to be modified
	\param[in] state		nonzero = set the flags, zero = clear the flags
	\returns packed bitmap of flags that are currently set (after applying changes)
*/
	CMPACK_EXPORT(CmpackKombineFlags, cmpack_kombine_set_flags, (CmpackKombine* ctx, CmpackKombineFlags flags, int state));

/**
	\brief Open output file
	\details The function clears the image buffers in the context and initializes
	the output file. After calling this function, you can process the source 
	frames by calling the cmpack_kombine_read() function. It is necessary to
	finish the operation by calling the cmpack_kombine_close() function.
	\param[in] ctx			kombine context
	\param[in] outfile		output file context
	\return zero on success or error code on failure.
*/
	CMPACK_EXPORT(int, cmpack_kombine_open, (CmpackKombine* ctx, CmpackCcdFile* outfile));

/**
	\brief Add a frame to the accumulation buffer
	\details The function reads a CCD frame and its offset from a photometry file.
	Then shifts the frame by the offset and adds it to the accumulation buffer.
	\param[in] ctx			kombine context
	\param[in] ccdfile		CCD frame file
	\param[in] phtfile		photometry file
	\return zero on success or error code on failure.
*/
	CMPACK_EXPORT(int, cmpack_kombine_read, (CmpackKombine* ctx, CmpackCcdFile* ccdfile, CmpackPhtFile* phtfile));

/**
	\brief Make combined CCD frame and writes it to the file
	\details The function computes the output CCD data, writes it to the 
	output file.
	\param[in] ctx			kombine context
	\return zero on success or error code on failure.
*/
	CMPACK_EXPORT(int, cmpack_kombine_close, (CmpackKombine* ctx));
	
#ifdef __cplusplus
}
#endif

#endif
