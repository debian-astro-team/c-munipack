/**
	\file
	\brief Functions for the heliocentric correction

	Set of functions defined in this module allows user to 
	apply heliocentric correction to light-curve data files.
	
	\author David Motl <dmotl@volny.cz>
	
	\par Copying
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation, version 2.
	
	$Id: cmpack_hcorr.h,v 1.1 2015/07/06 08:33:22 dmotl Exp $
*/
#ifndef _CMPACK_HCORR_H_INCLUDED
#define _CMPACK_HCORR_H_INCLUDED

#include "cmpack_console.h"
#include "cmpack_table.h"
#include "cmpack_fset.h"

/**********************   Public constants   ***********************************/

/**
	\brief Correction flags
*/
typedef enum _CmpackHCorrFlags
{
	CMPACK_HCORR_DEFAULT	= 0,			/**< Default behavior */
	CMPACK_HCORR_REVERSE	= (1<<0),		/**< Compute geocentric JD from heliocentric JD */
	CMPACK_HCORR_NOHELCOR	= (1<<1),		/**< Do not add a new column with heliocentric correction */
	CMPACK_HCORR_NOJULDAT	= (1<<2),		/**< Do not add a new column with heliocentric JD */
	CMPACK_HCORR_MODIFYJD	= (1<<3),		/**< Modify JD -> JDHEL, do not add new columns */
	CMPACK_HCORR_FRAME_IDS	= (1<<4)		/**< Include frame identifiers */
} CmpackHCorrFlags;

/********************   Public functions   *************************************/

#ifdef __cplusplus
extern "C" {
#endif

/**
	\brief Computes heliocentric correction
	\details The function computes value of heliocentric correction
	for given Julian date and object coordinates.
	\param[in] jd			Julian date (UT)
	\param[in] ra			right ascension of object in hours
	\param[in] declination	declination of object in degrees
	\return heliocentric correction in days
*/
	CMPACK_EXPORT(double, cmpack_helcorr, (double jd, double ra, double declination));

/**
	\brief Make heliocentric correction of data stored in a table
	\details The function computes heliocentric correction for all
	rows in the table. The table must have a column named 'JD', its
	values must be geocentric Julian date of observation. The value
	of correction in days and heliocentric Julian date are stored
	to the column 'HELCOR' and 'JDHEL' respectively. If such
	columns do not exist, the function appends them after the last
	column. The function also updates the table's header to set
	the object designation and coordinates.
	\param[in] table		table
	\param[in] objname		object designation (can be NULL)
	\param[in] ra			right ascension of object in hours
	\param[in] declination	declination of object in degrees
	\param[in] console		used to print debug outputs (can be NULL)
	\param[in] flags		see CMPACK_HCORR_xxx constants
	\return zero on success or error code on failure.
*/
	CMPACK_EXPORT(int, cmpack_helcorr_table, (CmpackTable* table, const char* objname, double ra,
		double declination, CmpackConsole* console, CmpackHCorrFlags flags));

/**
	\brief Make heliocentric correction curve 
	\details The function create a new table that consists of JD,
	HELCOR and JDHEL columns. Optionally, it can consits FRAME column, too.
	\param[in] fset			frame set
	\param[out] table		new table with the air mass curve
	\param[in] objname		object designation (can be NULL)
	\param[in] ra			right ascension of object in hours
	\param[in] declination	declination of object in degrees
	\param[in] console		used to print debug outputs (can be NULL)
	\param[in] flags		see CMPACK_HCORR_xxx constants
	\return zero on success or error code on failure.
*/
	CMPACK_EXPORT(int, cmpack_helcorr_curve, (CmpackFrameSet* fset, CmpackTable** table, const char* objname,
		double ra, double declination, CmpackHCorrFlags flags, CmpackConsole* console));

/**
	\brief Make heliocentric correction of data stored in a frame set
	\details The function updates the heliocentric correction and heliocentric
	Julin dates for each frame. The object parameters are updated as well.
	\param[in] fset			frame set
	\param[in] objname		object designation (can be NULL)
	\param[in] ra			right ascension of object in hours
	\param[in] declination	declination of object in degrees
	\param[in] console		used to print debug outputs (can be NULL)
	\return zero on success or error code on failure.
*/
	CMPACK_EXPORT(int, cmpack_helcorr_fset, (CmpackFrameSet* fset, const char* objname, double ra,
		double declination, CmpackConsole* console));

#ifdef __cplusplus
}
#endif

#endif
