/**************************************************************

xml_dom.h (C-Munipack project)
Simplified Document Object Model 
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef _CMPACK_XML_H_INCLUDED
#define _CMPACK_XML_H_INCLUDED

#include <stdio.h>

#include "cmpack_common.h"

/***********************      Datatypes      ******************/

/* Node type */
typedef enum _CmpackXmlNodeType
{
	CMPACK_XML_INVALID_NODE,
	CMPACK_XML_ELEMENT_NODE,
	CMPACK_XML_TEXT_NODE,
	CMPACK_XML_COMMENT_NODE,
	CMPACK_XML_DOCUMENT_NODE,
	CMPACK_XML_CDATA_NODE
} CmpackXmlNodeType;

/* Node */
typedef struct _CmpackNode CmpackNode;

/* Document */
typedef struct _CmpackDocument CmpackDocument;

/* Attribute */
typedef struct _CmpackAttribute CmpackAttribute;

/* Element */
typedef struct _CmpackElement CmpackElement;

/* Comment */
typedef struct _CmpackComment CmpackComment;

/* Character data */
typedef struct _CmpackCData CmpackCData;

/********************   Public functions   ********************************/

#ifdef __cplusplus
extern "C" {
#endif

/* Load document from a file */
	CMPACK_EXPORT(CmpackDocument*, cmpack_xml_doc_from_file, (FILE* f));

/* Destroy the XML document */
	CMPACK_EXPORT(void, cmpack_xml_doc_free, (CmpackDocument* doc));

/* Get document root */
	CMPACK_EXPORT(CmpackElement*, cmpack_xml_doc_get_root, (CmpackDocument* doc));

/* Get first child element by name */
	CMPACK_EXPORT(CmpackNode*, cmpack_xml_node_first_child, (CmpackNode* node));

/* Get next sibling node */
	CMPACK_EXPORT(CmpackNode*, cmpack_xml_node_next_sibling, (CmpackNode* node));

/* Node type */
	CMPACK_EXPORT(CmpackXmlNodeType, cmpack_xml_node_type, (CmpackNode* node));

/* Get first child element by name */
	CMPACK_EXPORT(CmpackElement*, cmpack_xml_element_first_element, (CmpackElement* node, const char* name));

/* Get next child element of the same name */
	CMPACK_EXPORT(CmpackElement*, cmpack_xml_element_next_element, (CmpackElement* node));

/* Get number of child nodes of given name */
	CMPACK_EXPORT(int, cmpack_xml_element_n_children, (CmpackElement* elem, const char* name));

/* Returns nonzero if the element has the attribute */
	CMPACK_EXPORT(int, cmpack_xml_element_has_attribute, (CmpackElement* elem, const char* attr));

/* Returns nonzero if the element has the attribute */
	CMPACK_EXPORT(int, cmpack_xml_element_n_attributes, (CmpackElement* elem));

/* Returns nonzero if the element has the attribute */
	CMPACK_EXPORT(CmpackAttribute*, cmpack_xml_element_attribute, (CmpackElement* elem, int index));

/* Get element's attribute */
	CMPACK_EXPORT(const char*, cmpack_xml_attr_s, (CmpackElement* elem, const char* attr, const char* defval));
	CMPACK_EXPORT(int, cmpack_xml_attr_i, (CmpackElement* elem, const char* attr, int defval));
	CMPACK_EXPORT(double, cmpack_xml_attr_d, (CmpackElement* elem, const char* attr, double defval));

/* Get node's content */
	CMPACK_EXPORT(const char*, cmpack_xml_value, (CmpackElement* elem, const char* defval));
	CMPACK_EXPORT(int, cmpack_xml_value_s, (CmpackElement* elem, char* buf, int bufsize));
	CMPACK_EXPORT(int, cmpack_xml_value_i, (CmpackElement* elem, int defval));
	CMPACK_EXPORT(double, cmpack_xml_value_d, (CmpackElement* elem, double defval));
	CMPACK_EXPORT(int, cmpack_xml_value_datetime, (CmpackElement* elem, struct tm* t));

/* Get pointer to the first child node of comment type */
	CMPACK_EXPORT(CmpackComment*, cmpack_xml_comment, (CmpackNode* node));

/* Get child node's value */
	CMPACK_EXPORT(const char*, cmpack_xml_child_value, (CmpackElement* elem, const char* name, const char* defval));
	CMPACK_EXPORT(int, cmpack_xml_child_value_s, (CmpackElement* elem, const char* name, char* buf, int bufsize));
	CMPACK_EXPORT(int, cmpack_xml_child_value_i, (CmpackElement* elem, const char* name, int defval));
	CMPACK_EXPORT(double, cmpack_xml_child_value_d, (CmpackElement* elem, const char* name, double defval));
	CMPACK_EXPORT(int, cmpack_xml_child_value_tm, (CmpackElement* elem, const char* name, struct tm* t));

/* Get text of the comment */
	CMPACK_EXPORT(const char*, cmpack_xml_comment_text, (CmpackComment* node));

/* Get text of the CDATA section */
	CMPACK_EXPORT(const char*, cmpack_xml_cdata_text, (CmpackCData* node));

/* Get text of the CDATA section */
	CMPACK_EXPORT(const char*, cmpack_xml_element_name, (CmpackElement* node));

/* Get attribute name */
	CMPACK_EXPORT(const char*, cmpack_xml_attribute_name, (CmpackAttribute* attr));

/* Get attribute value */
	CMPACK_EXPORT(const char*, cmpack_xml_attribute_value, (CmpackAttribute* attr));

#ifdef __cplusplus
}
#endif

#endif
