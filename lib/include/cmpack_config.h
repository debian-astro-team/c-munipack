/**
	\file
	\brief Functions for making a track curve

	Set of functions defined in this module allows user to 
	make a track curve
	
	\author David Motl <dmotl@volny.cz>
	
	\par Copying
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation, version 2.
	
	$Id: cmpack_tcurve.h,v 1.1 2015/07/06 08:33:22 dmotl Exp $
*/
#ifndef _CMPACK_CONFIG_H_INCLUDED
#define _CMPACK_CONFIG_H_INCLUDED

#ifndef CMPACK_EXPORT
#  define CMPACK_EXPORT(type, name, args) CMPACK_FUNCTION3(CMPACK_EXPORT_TYPE(type), (name), args)
#endif

#ifndef CMPACK_FUNCTION3
#  define CMPACK_FUNCTION3(type, name, args) type name args
#endif

#ifndef CMPACK_FUNCTION4
#  define CMPACK_FUNCTION4(type, name, args, attributes) attributes type name args
#endif

#ifndef CMPACK_EXPORT_TYPE
#  define CMPACK_EXPORT_TYPE(type) CMPACK_IMPEXP type
#endif

#if defined CMUNIPACK_BUILD_SHARED_LIB
  #if defined (libcmpack_EXPORTS)  /* Compiling the shared library */
    #if defined (_MSC_VER)  /* MSVC Compiler Case */
      #define  CMPACK_IMPEXP __declspec(dllexport)
    #elif (__GNUC__ >= 4)  /* GCC 4.x has support for visibility options */
      #define CMPACK_IMPEXP __attribute__ ((visibility("default")))
    #endif
  #else  /* Importing the DLL into another project */
    #if defined (_MSC_VER)  /* MSVC Compiler Case */
      #define  CMPACK_IMPEXP __declspec(dllimport)
    #elif (__GNUC__ >= 4)  /* GCC 4.x has support for visibility options */
      #define CMPACK_IMPEXP __attribute__ ((visibility("default")))
    #endif
  #endif
#endif

/* If CMPACK_IMPEXP was never defined, define it here */
#ifndef CMPACK_IMPEXP
  #define CMPACK_IMPEXP
#endif

#endif
