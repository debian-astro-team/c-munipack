/**
	\file
	\brief Functions for the air mass coefficient computation

	Set of functions defined in this module allows user to include
	the air mass coefficients to a table stored in the file.
	
	\author David Motl <dmotl@volny.cz>
	
	\par Copying
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation, version 2.
	
	$Id: cmpack_amass.h,v 1.1 2015/07/06 08:33:22 dmotl Exp $
*/

#ifndef _CMPACK_AMASS_H_INCLUDED
#define _CMPACK_AMASS_H_INCLUDED

#include "cmpack_console.h"
#include "cmpack_table.h"
#include "cmpack_fset.h"

/**********************   Public constants   ***********************************/

/**
	\brief Air-mass coefficient control flags
*/
typedef enum _CmpackAMassFlags
{
	CMPACK_AMASS_DEFAULT	= 0,		/**< Default behavior */
	CMPACK_AMASS_NOALTITUDE	= (1<<0),	/**< Do not add altitude in degrees */
	CMPACK_AMASS_NOAIRMASS	= (1<<1),  	/**< Do not add air mass coefficients */
	CMPACK_AMASS_FRAME_IDS	= (1<<2)	/**< Include frame identifiers */
} CmpackAMassFlags;

/********************   Public functions   *************************************/

#ifdef __cplusplus
extern "C" {
#endif

/**
	\brief Computes air mass coefficient
	\details The function computes the air mass coefficient for
	given Julian date, object coordinates and observer coordinates.
	It also computes the altitude of the object in degrees. The function
	returns value of the air mass coefficient or negative value if 
	the object is below the horizon.
	\param[in] jd			Julian date (UT)
	\param[in] ra			right ascension of object in hours
	\param[in] declination	declination of object in degrees
	\param[in] longitude	longitude of observer in degrees
	\param[in] latitude		latitude of observer in degrees
	\param[out] airmass		air mass coeficient
	\param[out] altitude	altitude in degrees (optional)
	\return zero on success, error code on failure
*/
	CMPACK_EXPORT(int, cmpack_airmass, (double jd, double ra, double declination, double longitude,
		double latitude, double* airmass, double* altitude));

/**
	\brief Include air mass coefficients to a table
	\details The function computes air mass coefficients for all
	rows in the table. The table must have a column named 'JD', its
	values must be geocentric Julian date of obsrvation. The value
	of air mass coefficient and altitude of the object are stored
	to the column 'AIRMASS' and 'ALTITUDE' respectively. If such
	columns do not exist, the function appends them after the last
	column. The function also updates the table's header to set
	the object designation and coordinates and location name and
	observer coordinates.
	\param[in] table		table
	\param[in] objname		object designation (can be NULL)
	\param[in] ra			right ascension of object in hours
	\param[in] declination	declination of object in degrees
	\param[in] location		location name (can be NULL)
	\param[in] longitude	longitude of observer in degrees
	\param[in] latitude		latitude of observer in degrees
	\param[in] console		used to print debug outputs (can be NULL)
	\param[in] flags		see CMPACK_AMASS_xxx constants
	\return zero on success or error code on failure.
*/
	CMPACK_EXPORT(int, cmpack_airmass_table, (CmpackTable* table, const char* objname, double ra,
		double declination, const char* location, double longitude, double latitude,
		CmpackConsole* console, unsigned flags));

/**
	\brief Update air mass coefficients in a frame set
	\details The function updates the air mass coefficients and
	the altitudes based on the Julian date of each frame. The object
	parameters and observer parameters are updated as well.
	\param[in] fset			frame set
	\param[in] objname		object designation (can be NULL)
	\param[in] ra			right ascension of object in hours
	\param[in] declination	declination of object in degrees
	\param[in] location		location name (can be NULL)
	\param[in] longitude	longitude of observer in degrees
	\param[in] latitude		latitude of observer in degrees
	\param[in] console		used to print debug outputs (can be NULL)
	\return zero on success or error code on failure.
*/
	CMPACK_EXPORT(int, cmpack_airmass_fset, (CmpackFrameSet* fset, const char* objname, double ra,
		double declination, const char* location, double longitude, double latitude,
		CmpackConsole* console));

/**
	\brief Make a air mass curve from a frame set
	\details The function create a new table consisting of JD, AIRMASS and
	ALTITUDE columns. Optionally, it can consits FRAME column, too.
	\param[in] fset			frame set
	\param[out] table		new table with the air mass curve
	\param[in] objname		object designation (can be NULL)
	\param[in] ra			right ascension of object in hours
	\param[in] declination	declination of object in degrees
	\param[in] location		location name (can be NULL)
	\param[in] longitude	longitude of observer in degrees
	\param[in] latitude		latitude of observer in degrees
	\param[in] flags		flags
	\param[in] console		used to print debug outputs (can be NULL)
	\return zero on success or error code on failure.
*/
	CMPACK_EXPORT(int, cmpack_airmass_curve, (CmpackFrameSet* fset, CmpackTable** table, const char* objname, double ra,
		double declination, const char* location, double longitude, double latitude,
		CmpackAMassFlags flags, CmpackConsole* console));

#ifdef __cplusplus
}
#endif

#endif
