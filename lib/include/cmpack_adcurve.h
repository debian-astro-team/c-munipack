/**
	\file
	\brief Functions for making an AD curve

	Set of functions defined in this module allows user to 
	make a track curve
	
	\author David Motl <dmotl@volny.cz>
	
	\par Copying
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation, version 2.
	
	$Id: cmpack_adcurve.h,v 1.1 2015/07/06 08:33:22 dmotl Exp $
*/
#ifndef _CMPACK_ADCURVE_H_INCLUDED
#define _CMPACK_ADCURVE_H_INCLUDED

#include "cmpack_console.h"
#include "cmpack_fset.h"
#include "cmpack_table.h"

/********************   Private data structures   ********************************/

/**
	\brief Listing process context
	\details This private data structure holds the temporary data, which are
	used during listing process.
*/
typedef struct _CmpackADCurve CmpackADCurve;

/**********************   Public constants   ***********************************/

/**
	\brief Output modifiers
*/
typedef enum _CmpackADCurveFlags
{
	CMPACK_ADCURVE_DEFAULT = 0			/**< Default behavior */
} CmpackADCurveFlags;

/********************   Public functions   ********************************/

#ifdef __cplusplus
extern "C" {
#endif

/**
	\brief Make new AD-curve context
	\details The function allocates memory with AD-curve context and returns a 
	new reference to it. The reference counter is set to one. The caller is 
	responsible to call cmpack_adcurve_destroy() when it is no longer needed.
	\return pointer to a new reference
*/
	CMPACK_EXPORT(CmpackADCurve*, cmpack_adcurve_init, (void));

/**
	\brief Make a new reference to the AD-curve context
	\details The function makes a new reference to the context and returns a 
	pointer to it. The reference counter is incremented by one. The caller 
	is responsible to call cmpack_adcurve_destroy() when the reference is 
	no longer needed.
	\return pointer to a new reference
*/
	CMPACK_EXPORT(CmpackADCurve*, cmpack_adcurve_reference, (CmpackADCurve* ctx));

/**
	\brief Release a reference to the AD-curve context
	\details The function releases a reference to the context. 
	The reference counter is decreased by one and when it was the 
	last reference to the context, the context is freed and all memory 
	allocated in the context is reclaimed.
*/
	CMPACK_EXPORT(void, cmpack_adcurve_destroy, (CmpackADCurve* ctx));

/**
	\brief Attach console to the AD-curve context
	\details The function connects a AD-curve context with
	a console context. The console is designed to print the information
	during the data processing. The functions makes its own reference
	to the console. Only one console can be attached to a single context, 
	if another console is attached to the single context, the original 
	one is released. Set console to NULL to release a reference to the 
	console that is currently attached to the context.
	\param[in] ctx			air mass computation context
	\param[in] con			console context
*/
	CMPACK_EXPORT(void, cmpack_adcurve_set_console, (CmpackADCurve* ctx, CmpackConsole* con));

/**
	\brief Set list of comparison stars
	\details The function sets the list of comparison stars that are included
	in the AD curve.
	\param[in] proc			listing context
	\param[in] items		array of star identifiers
	\param[in] nitems		number of items
*/
	CMPACK_EXPORT(void, cmpack_adcurve_set_comp, (CmpackADCurve* proc, const int* items, int nitems));

/**
	\brief Get list of comparison stars
	\details The function makes copy of the data and returns the pointer
	to it. The caller is reponsible to free the memory by means of the
	cmpack_free() method.
	\param[in] proc			listing context
	\param[out] items		array of star identifiers
	\param[out] nitems		number of items
*/
	CMPACK_EXPORT(void, cmpack_adcurve_get_comp, (CmpackADCurve* proc, int** items, int* nitems));

/**
	\brief Set list of check stars
	\details The function sets the list of check stars that are included 
	in the AD curve.
	\param[in] proc			listing context
	\param[in] items		array of star identifiers
	\param[in] nitems		number of items
*/
	CMPACK_EXPORT(void, cmpack_adcurve_set_check, (CmpackADCurve* proc, const int* items, int nitems));

/**
	\brief Get list of check stars
	\details The function makes copy of the data and returns the pointer
	to it. The caller is reponsible to free the memory by means of the
	cmpack_free() method.
	\param[in] proc			listing context
	\param[out] items		array of star identifiers
	\param[out] nitems		number of items
*/
	CMPACK_EXPORT(void, cmpack_adcurve_get_check, (CmpackADCurve* proc, int** items, int* nitems));

/**
	\brief Make a AD curve
	\param[in] proc			listing process context
	\param[in] fset			frame set (source data)
	\param[out] table		new table with the AD curve
	\param[in] flags		flags
	\return zero on success or error code on failure
*/
	CMPACK_EXPORT(int, cmpack_adcurve, (CmpackADCurve* proc, CmpackFrameSet* fset,
		CmpackTable** table, CmpackADCurveFlags flags));

#ifdef __cplusplus
}
#endif

#endif
