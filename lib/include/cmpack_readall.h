/**
	\file
	\brief Functions for making read-all files.

	Set of functions defined in this module allows user to 
	make read-all file from a set of photometry files.

	\author David Motl <dmotl@volny.cz>
	
	\par Copying
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation, version 2.
	
	$Id: cmpack_readall.h,v 1.1 2015/07/06 08:33:22 dmotl Exp $
*/
#ifndef _CMPACK_READALL_H_INCLUDED
#define _CMPACK_READALL_H_INCLUDED

#endif
