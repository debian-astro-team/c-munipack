/**
	\file
	\brief Functions for the making master-dark correction frame

	Set of functions defined in this module allows user to 
	make a master-dark frame from a set of CCD frames.
	
	\author David Motl <dmotl@volny.cz>
	
	\par Copying
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation, version 2.
	
	$Id: cmpack_mdark.h,v 1.1 2015/07/06 08:33:22 dmotl Exp $
*/
#ifndef _CMPACK_MDARK_H_INCLUDED
#define _CMPACK_MDARK_H_INCLUDED

#include "cmpack_common.h"
#include "cmpack_console.h"
#include "cmpack_ccdfile.h"

/********************   Private data structures   ********************************/

/**
	\brief Configuration context for the Master-dark tool
	\details This private data structure holds the configuration parameters
	for the master-dark tool
*/
typedef struct _CmpackMasterDark CmpackMasterDark;

/********************   Public functions   ********************************/

#ifdef __cplusplus
extern "C" {
#endif

/**
	\brief Make new master-dark computation context
	\details The function allocates memory with master-dark computation context 
	and returns a new reference to it. The reference counter is set to one. 
	The caller is responsible to call cmpack_mbias_destroy() when it is no longer 
	needed.
	\return pointer to a new reference
*/
	CMPACK_EXPORT(CmpackMasterDark*, cmpack_mdark_init, (void));

/**
	\brief Make a new reference to the master-dark computation context
	\details The function makes a new reference to the context and returns a 
	pointer to it. The reference counter is incremented by one. The caller 
	is responsible to call cmpack_mbias_destroy() when the reference is 
	no longer needed.
	\return pointer to a new reference
*/
	CMPACK_EXPORT(CmpackMasterDark*, cmpack_mdark_reference, (CmpackMasterDark* ctx));

/**
	\brief Release a reference to the master-dark computation context
	\details The function releases a reference to the context. 
	The reference counter is decreased by one and when it was the 
	last reference to the context, the context is freed and all memory 
	allocated in the context is reclaimed.
*/
	CMPACK_EXPORT(void, cmpack_mdark_destroy, (CmpackMasterDark* ctx));

/**
	\brief Attach console to the master-dark computation context
	\details The function connects a master-bias computation context with
	a console context. The console is designed to print the information
	during the data processing. The functions makes its own reference
	to the console. Only one console can be attached to a single context, 
	if another console is attached to the single context, the original 
	one is released. Set console to NULL to release a reference to the 
	console that is currently attached to the context.
	\param[in] ctx			master-dark context
	\param[in] con			console context
*/
	CMPACK_EXPORT(void, cmpack_mdark_set_console, (CmpackMasterDark* ctx, CmpackConsole* con));

/**
	\brief Set output data format
	\details The function sets the format of the output CCD frame. If
	the format is set to CMPACK_BITPIX_AUTO (default), the format of the first
	source frame is used. Otherwise, the image data are converted into
	specified format.
	\param[in] ctx			master-dark context
	\param[in] bitpix		output data format
*/
	CMPACK_EXPORT(void, cmpack_mdark_set_bitpix, (CmpackMasterDark* ctx, CmpackBitpix bitpix));

/**
	\brief Get output data format
	\param[in] ctx			master-bias context
	\return current image data format
*/
	CMPACK_EXPORT(CmpackBitpix, cmpack_mdark_get_bitpix, (CmpackMasterDark* ctx));

/**
	\brief Enable and disable making of a scalable dark frame
	\details The function enables and disables making of a scalable
	dark frame. The scalable dark frame is used in the advanced 
	calibration scheme. The feature is disabled by default.
	\param[in] ctx			master-dark context
	\param[in] scalable		1 = enable, 0 = disable feature
*/
	CMPACK_EXPORT(void, cmpack_mdark_set_scalable, (CmpackMasterDark* ctx, int scalable));

/**
	\brief Make scalable dark frame?
	\param[in] ctx			master-dark context
	\return 1 = yes, 0 = no
*/
	CMPACK_EXPORT(int, cmpack_mdark_get_scalable, (CmpackMasterDark* ctx));

/**
	\brief Set image border size
	\details The function sets the size of CCD frame borders. The pixels
	inside the frame borders are set always to zero. You can use this feature 
	to clear an unusable part of a frame.
	\param[in] ctx			master-dark context
	\param[in] border		border size in pixels
*/
	CMPACK_EXPORT(void, cmpack_mdark_set_border, (CmpackMasterDark* ctx, const CmpackBorder* border));

/**
	\brief Get image border size
	\details The function returns current border size
	\param[in] ctx			master-bias context
	\param[out] border		border size in pixels
*/
	CMPACK_EXPORT(void, cmpack_mdark_get_border, (CmpackMasterDark* ctx, CmpackBorder* border));

/**
	\brief Set pixel value thresholds
	\details The function sets the value of two thresholds that determine 
	valid pixel values. Pixels of value <= minvalue are treated as bad pixels, 
	pixels of value >= maxvalue are treated as overexposed pixels.
	\param[in] ctx			bias correction context
	\param[in] minvalue		bad pixel threshold
	\param[in] maxvalue		overexposed pixel threshold
*/
	CMPACK_EXPORT(void, cmpack_mdark_set_thresholds, (CmpackMasterDark* ctx, double minvalue, double maxvalue));

/**
	\brief Set threshold for "invalid" pixel value
	\details The function sets the value of threshold that determines 
	valid pixel values. Pixels of value <= minvalue are treated as bad pixels.
	\param[in] ctx			master-bias context
	\param[in] minvalue		bad pixel threshold
*/
	CMPACK_EXPORT(void, cmpack_mdark_set_minvalue, (CmpackMasterDark* ctx, double minvalue));

/**
	\brief Get threshold for "invalid" pixel values
	\param[in] ctx			master-bias context
	\return pixel value
*/
	CMPACK_EXPORT(double, cmpack_mdark_get_minvalue, (CmpackMasterDark* ctx));

/**
	\brief Set threshold for "overexposed" pixels
	\details The function sets the value of threshold that determines 
	valid pixel values. Pixels of value >= maxvalue are treated as overexposed pixels.
	\param[in] ctx			master-bias context
	\param[in] maxvalue		overexposed pixel threshold
*/
	CMPACK_EXPORT(void, cmpack_mdark_set_maxvalue, (CmpackMasterDark* ctx, double maxvalue));

/**
	\brief Get threshold for "overexposed" pixels
	\param[in] ctx			master-bias context
	\return pixel value
*/
	CMPACK_EXPORT(double, cmpack_mdark_get_maxvalue, (CmpackMasterDark* ctx));

/**
	\brief Open output file
	\details The function clears the image buffers in the context and initializes
	the output file. After calling this function, you can process the source 
	frames by calling the cmpack_mdark_read() function. It is necessary to
	finish the operation by calling the cmpack_mdark_close() function.
	\param[in] ctx			master-dark context
	\param[in] outfile		output file context
	\return zero on success or error code on failure.
*/
	CMPACK_EXPORT(int, cmpack_mdark_open, (CmpackMasterDark* ctx, CmpackCcdFile* outfile));

/**
	\brief Add a frame to the accumulation buffer
	\details The function reads the frame from specified CCD-frame 
	file into memory.
	\param[in] ctx			output file context
	\param[in] infile		input file context
	\return zero on success or error code on failure.
*/
	CMPACK_EXPORT(int, cmpack_mdark_read, (CmpackMasterDark* ctx, CmpackCcdFile* infile));

/**
	\brief Make master-dark frame and save it to the file
	\details The function computes the output CCD data, writes it to the 
	output file.
	\param[in] ctx			output file context
	\return zero on success or error code on failure.
*/
	CMPACK_EXPORT(int, cmpack_mdark_close, (CmpackMasterDark* ctx));

#ifdef __cplusplus
}
#endif

#endif
