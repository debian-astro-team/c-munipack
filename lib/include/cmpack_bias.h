/**
	\file
	\brief Functions for the bias-frame correction

	Set of functions defined in this module allows user to 
	apply bias-frame correction to CCD frames.
	
	\author David Motl <dmotl@volny.cz>
	
	\par Copying
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation, version 2.
	
	$Id: cmpack_bias.h,v 1.1 2015/07/06 08:33:22 dmotl Exp $
*/

#ifndef _CMPACK_BIAS_H_INCLUDED
#define _CMPACK_BIAS_H_INCLUDED

#include "cmpack_common.h"
#include "cmpack_console.h"
#include "cmpack_ccdfile.h"

/********************   Private data structures   ********************************/

/**
	\brief Bias-frame correction context
	\details This private data structure holds the parameter for bias-frame
	correction as well as the bias frame itself.
*/
typedef struct _CmpackBiasCorr CmpackBiasCorr;

/********************   Public functions   *************************************/

#ifdef __cplusplus
extern "C" {
#endif

/**
	\brief Make new bias-frame correction context
	\details The function allocated memory with bias correction context
	and returns a new reference to it. The reference counter is set to one. 
	The caller is responsible to call cmpack_bias_destroy() when it is no 
	longer needed.
	\return pointer to a new reference
*/
	CMPACK_EXPORT(CmpackBiasCorr*, cmpack_bias_init, (void));

/**
	\brief Make a new reference to the bias-frame correction context
	\details The function makes a new reference to the context and returns a 
	pointer to it. The reference counter is incremented by one. The caller 
	is responsible to call cmpack_bias_destroy() when the reference is 
	no longer needed.
	\return pointer to a new reference
*/
	CMPACK_EXPORT(CmpackBiasCorr*, cmpack_bias_reference, (CmpackBiasCorr* ctx));

/**
	\brief Release a reference to the bias-frame correction context
	\details The function releases a reference to a bias-frame correction
	context. The reference counter is decreased by one and when it was the 
	last reference to the context, the context is freed and all memory 
	allocated in the context is reclaimed.
*/
	CMPACK_EXPORT(void, cmpack_bias_destroy, (CmpackBiasCorr* ctx));

/**
	\brief Attach console to the context
	\details The function connects a bias-frame correction context with
	a console context. The console is designed to print the information
	during the data processing. The functions makes its own reference
	to the console. Only one console can be attached to a single context, 
	if another console is attached to the single context, the original 
	one is released. Set console to NULL to release a reference to the 
	console that is currently attached to the context.
	\param[in] ctx			bias correction context
	\param[in] con			console context
*/
	CMPACK_EXPORT(void, cmpack_bias_set_console, (CmpackBiasCorr* ctx, CmpackConsole* con));

/**
	\brief Set image border size
	\details The function sets the size of the CCD frame border. If you set the 
	border to nonzero size, the bias will set pixels that are inside the
	border area to zero. You can use this feature to skip an unusable part 
	of a frame.
	\param[in] ctx			bias correction context
	\param[in] border		border size in pixels
*/
	CMPACK_EXPORT(void, cmpack_bias_set_border, (CmpackBiasCorr* ctx, const CmpackBorder* border));

/**
	\brief Get image border
	\details The function reads the size of the CCD frame border. 
	\param[in] ctx			bias correction context
	\param[out] border		border size in pixels
*/
	CMPACK_EXPORT(void, cmpack_bias_get_border, (CmpackBiasCorr* ctx, CmpackBorder* border));

/**
	\brief Set pixel value thresholds
	\details The function sets the value of two thresholds that determine 
	valid pixel values. Pixels of value <= minvalue are treated as bad pixels, 
	pixels of value >= maxvalue are treated as overexposed pixels.
	\param[in] ctx			bias correction context
	\param[in] minvalue		bad pixel threshold
	\param[in] maxvalue		overexposed pixel threshold
*/
	CMPACK_EXPORT(void, cmpack_bias_set_thresholds, (CmpackBiasCorr* ctx, double minvalue, double maxvalue));

/**
	\brief Set threshold for "invalid" pixel value
	\details The function sets the value of threshold that determines 
	valid pixel values. Pixels of value <= minvalue are treated as bad pixels.
	\param[in] ctx			bias correction context
	\param[in] minvalue		bad pixel threshold
*/
	CMPACK_EXPORT(void, cmpack_bias_set_minvalue, (CmpackBiasCorr* ctx, double minvalue));

/**
	\brief Get threshold for "invalid" pixel values
	\param[in] ctx			bias correction context
	\return pixel value
*/
	CMPACK_EXPORT(double, cmpack_bias_get_minvalue, (CmpackBiasCorr* ctx));

/**
	\brief Set threshold for "overexposed" pixels
	\details The function sets the value of threshold that determines 
	valid pixel values. Pixels of value >= maxvalue are treated as overexposed pixels.
	\param[in] ctx			bias correction context
	\param[in] maxvalue		overexposed pixel threshold
*/
	CMPACK_EXPORT(void, cmpack_bias_set_maxvalue, (CmpackBiasCorr* ctx, double maxvalue));

/**
	\brief Get threshold for "overexposed" pixels
	\param[in] ctx			bias correction context
	\return pixel value
*/
	CMPACK_EXPORT(double, cmpack_bias_get_maxvalue, (CmpackBiasCorr* ctx));

/**
	\brief Set correction frame
	\details The function reads image data from the given CCD frame. The 
	internal copy of the image data is made, no reference to the frame is 
	held, so you can free it when this function has returned.
	\param[in] ctx			bias correction context
	\param[in] biasfile		bias frame context
	\return zero on success or error code on failure
*/
	CMPACK_EXPORT(int, cmpack_bias_rbias, (CmpackBiasCorr* ctx, CmpackCcdFile* biasfile));

/**
	\brief Execute bias-frame correction 
	\details The function reads image data from the file context, performs
	the bias correction and stores the output to the same context.
	\param[in] ctx			bias correction context
	\param[in,out] file		CCD frame context
	\return zero on success or error code on failure
*/
	CMPACK_EXPORT(int, cmpack_bias, (CmpackBiasCorr* ctx, CmpackCcdFile* file));

/**
	\brief Execute bias-frame correction 
	\details The function reads image data from the infile context, performs
	the bias correction and stores the output image to the outfile context.
	\param[in] ctx			bias-frame correction context
	\param[in] infile		input frame context
	\param[in] outfile		output frame context
	\return zero on success or error code on failure
*/
	CMPACK_EXPORT(int, cmpack_bias_ex, (CmpackBiasCorr* ctx, CmpackCcdFile* infile, CmpackCcdFile* outfile));

#ifdef __cplusplus
}
#endif

#endif
