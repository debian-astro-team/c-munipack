@echo off
REM --------------------------------------------------------------------------------------------------
REM	This script finds the Visual Studio and runs the `VsDevCmd.bat` batch to set up 
REM build environment for building the 64-bit code.
REM --------------------------------------------------------------------------------------------------

rem
rem Uncomment setting VSCMD_DEBUG to enable debugging to output
rem
rem set VSCMD_DEBUG=3

rem
rem   Determine path to VsDevCmd.bat
rem
for /f "usebackq delims=#" %%a in (`"%programfiles(x86)%\Microsoft Visual Studio\Installer\vswhere" -latest -products * -requires Microsoft.VisualStudio.Component.VC.Tools.x86.x64 -property installationPath`) do set VsDevCmd_Path=%%a\Common7\Tools\VsDevCmd.bat

"%VsDevCmd_Path%" -arch=amd64

rem set VSCMD_DEBUG=
set VsDevCmd_Path=
