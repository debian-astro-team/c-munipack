rem @echo off

set OUTPUT_DIR=out
set CMAKE_BUILD_TYPE=RelWithDebInfo

set CMAKE_SOURCE_DIR=.
set CMAKE_BUILD_DIR=%OUTPUT_DIR%\build-dist
set CMAKE_INSTALL_PREFIX=%OUTPUT_DIR%\install

REM Set environment for Visual Studio
if defined VCINSTALLDIR goto vcready
call "%CMAKE_SOURCE_DIR%\utils\vsenv64.bat"
:vcready

rem Make clean build directory
rmdir /S /Q "%CMAKE_BUILD_DIR%"
mkdir "%CMAKE_BUILD_DIR%"
mkdir "%CMAKE_INSTALL_PREFIX%"

set GTKMM_BASEPATH=%GTK2_X64_ROOT%

rem CMake - Generate a Project Buildsystem
cmake -G "Ninja" ^
	-D CMAKE_BUILD_TYPE:STRING="%CMAKE_BUILD_TYPE%" ^
	-D CMAKE_INSTALL_PREFIX:PATH="%CMAKE_INSTALL_PREFIX%" ^
	-S "%CMAKE_SOURCE_DIR%"^
	-B "%CMAKE_BUILD_DIR%"

rem CMake - Build a Project
cmake --build "%CMAKE_BUILD_DIR%"

rem CMake - Install a Project
cmake --install "%CMAKE_BUILD_DIR%"

rem CMake - Make a binary package
cmake --build "%CMAKE_BUILD_DIR%" --target package package_source

:ok
rem OK
EXIT /B 0

:Error
pause
