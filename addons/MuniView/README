**********************************
* CCD viewer for Total Commander *
**********************************

Installation
------------

The automatic plugin installation is supported in TC >= 6.50
  1. Start TC and navigate to C-Munipack installation directory.
  2. Enter the 'addons' -> 'MuniView' subdirectories.
  3. Open the archive 'MuniView.zip' a confirmation dialog should appear.
   
If the automatic installation is not possible, try the following steps:
  1. Start TC and navigate to C-Munipack installation directory.
  2. Enter the 'addons' -> 'MuniView' subdirectory.
  3. In the other pane, go to the TC's installation directory.
  4. Enter the 'plugins' -> 'wlx' subdirectory.
  5. Create a fresh 'MuniView' directory.
  6. Open the archive 'MuniView.zip' into a the 'MuniView' directory.
  7. Open the configuration dialog and go to page 'plugins'.
  8. In the 'Lister plugins' click on the 'Configure' button, a new dialog appears.
  9. Click on the 'Add' button and in the open dialog navigate to the 'plugins' ->
  		'wlx' -> 'MuniView' directory and select file 'MuniView.wlx'. Confirm.


Uninstallation
--------------
  1. Open the configuration dialog and go to page 'plugins'.
  2. In the 'Lister plugins' click on the 'Configure' button, a new dialog appears.
  3. Select the 'MuniView.wlx' plugin and click the 'Remove' button.
  4. Close and save the configuration dialog.
  5. Close TC and start it again to release the DLL from memory.
  6. Go to the TC's installation directory.
  7. Enter the 'plugins' -> 'wlx' subdirectory.
  8. Delete 'MuniView' folder and its content. 


Minimal requirements
--------------------
* Total Commander 5.51


Supported formats
-----------------
* FITS image files
* SBIG's image files
* OES Astro files
* Munipack's photometry files
* Munipack's catalogue files
* Munipack's output files (light curves, etc.)


Home Page
---------
http://c-munipack.sourceforge.net/


Author
------
Ing. David Motl, 2012
dmotl@volny.cz


Bug reports
-----------
If you have discovered any bug in the software or the documentation, 
please send the reports to the project manager. The email address is:

dmotl@volny.cz

Please, include Muniwin, Munipack, MuniView or C-Munipack text in 
the subject of your email. It ensures that the e-mail won't be thrown 
away by a spam filter.


Known problems
--------------
If the plugin does not start after pressing F3, try press number 4.
If the file is longer than 16 MB, the text mode is preset when lister 
starts. You can change the size limit in wincmd.ini configuration file.


References
----------
Based on C-Munipack, CFitsio and Expat libraries.


Copyrights
----------
Muniview - lister plugin for Christian Ghisler's Total Commander
Copyright 2012 David Motl 
    
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

********************************************************************************

The Muniwin plugin in binary form includes the C-Munipack library which is freely distributable 
under the conditions of GNU General Public License.

********************************************************************************

The Muniview plugin in binary form also includes the CFitsio library which is freely distributable 
under the following terms:

Copyright (Unpublished-all rights reserved under the copyright laws of the United States), 
U.S. Government as represented by the Administrator of the National Aeronautics and Space 
Administration. No copyright is claimed in the United States under Title 17, U.S. Code. 

Permission to freely use, copy, modify, and distribute this software and its documentation 
without fee is hereby granted, provided that this copyright notice and disclaimer of warranty 
appears in all copies. 

DISCLAIMER: 

THE SOFTWARE IS PROVIDED 'AS IS' WITHOUT ANY WARRANTY OF ANY KIND, EITHER EXPRESSED, IMPLIED, 
OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, ANY WARRANTY THAT THE SOFTWARE WILL CONFORM TO 
SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, 
AND FREEDOM FROM INFRINGEMENT, AND ANY WARRANTY THAT THE DOCUMENTATION WILL CONFORM TO THE 
SOFTWARE, OR ANY WARRANTY THAT THE SOFTWARE WILL BE ERROR FREE. IN NO EVENT SHALL NASA BE 
LIABLE FOR ANY DAMAGES, INCLUDING, BUT NOT LIMITED TO, DIRECT, INDIRECT, SPECIAL OR CONSEQUENTIAL 
DAMAGES, ARISING OUT OF, RESULTING FROM, OR IN ANY WAY CONNECTED WITH THIS SOFTWARE, WHETHER OR 
NOT BASED UPON WARRANTY, CONTRACT, TORT , OR OTHERWISE, WHETHER OR NOT INJURY WAS SUSTAINED BY 
PERSONS OR PROPERTY OR OTHERWISE, AND WHETHER OR NOT LOSS WAS SUSTAINED FROM, OR AROSE OUT OF 
THE RESULTS OF, OR USE OF, THE SOFTWARE OR SERVICES PROVIDED HEREUNDER." 

The file compress.c contains (slightly modified) source code that originally came from gzip-1.2.4 
which is freely distributed under the GNU General Public License. A copy of the GNU license is 
included at the beginning of that file.

********************************************************************************

The Muniwiew plugin also includes the Expat library which is freely distributable under 
the following terms:

Copyright (c) 1998, 1999, 2000 Thai Open Source Software Center Ltd
                               and Clark Cooper
Copyright (c) 2001, 2002 Expat maintainers.

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
