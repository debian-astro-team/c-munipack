/**************************************************************

properties_dlg.h (C-Munipack project)
The "Properties" dialogs
Copyright (C) 2012 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef PROPERTIES_DLG_H
#define PROPERTIES_DLG_H

#include <cmunipack.h>

#include "dialogs.h"

//
// Base class for "Properties" dialogs
//
class CFileProperties:public CDialog
{
public:
	// Constructor
	// params:
	//	filepath			- [in] full path to the file
	CFileProperties(const class TCCD_Interface *parent);

	// Destructor
	virtual ~CFileProperties();

protected:
	HFONT	m_hTitleFont;
	char	*m_FileName, *m_DirPath;
	const TCCD_Interface *m_pParent;

	// Initialize the dialog
	virtual BOOL OnInitDialog(void);

	// Command handler
	virtual BOOL OnCommand(int iCtrlId, int iCode);
};

//
// Image properties
//
class CImageProperties:public CFileProperties
{
public:
	// Constructor
	CImageProperties(const class TCCD_Image *parent);

protected:
	// Resource id 
	virtual int ResourceId(void);

	// Initialize the dialog
	virtual BOOL OnInitDialog(void);
};

//
// Chart properties
//
class CChartProperties:public CFileProperties
{
public:
	// Constructor
	CChartProperties(const class TCCD_Chart *parent);

protected:
	// Resource id 
	virtual int ResourceId(void);

	// Initialize the dialog
	virtual BOOL OnInitDialog(void);
};

//
// CTable properties
//
class CTableProperties:public CFileProperties
{
public:
	// Constructor
	CTableProperties(const class TCCD_Graph *parent);

protected:
	CmpackTableType m_Type;

	// Resource id 
	virtual int ResourceId(void);

	// Initialize the dialog
	virtual BOOL OnInitDialog(void);
};

#endif
