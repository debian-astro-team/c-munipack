/**************************************************************

mv_apertures.h (C-Munipack project)
List of apertures
Copyright (C) 2012 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CCD_CHART_APERTURES_H
#define CCD_CHART_APERTURES_H

//
// Aperture parameters
//
class CAperture 
{
public:
	// Constructors
	CAperture():m_Id(0), m_Radius(0) {}
	CAperture(int id, double radius):m_Id(id), m_Radius(radius) {}

	// Destructor
	virtual ~CAperture(void) {}

	// Get identifier
	int Id(void) const
	{ return m_Id; }

	// Get radius in pixels
	double Radius(void) const
	{ return m_Radius; }

	// Is the aperture valid?
	bool Valid(void) const
	{ return m_Radius>0; }

private:
	int		m_Id;				// Aperture identifier
	double	m_Radius;			// Radius in pixels
};

//
// Table of apertures
//
class CApertures
{
public:
	// Constructor(s)
	CApertures();
	CApertures(const CApertures &orig);
	
	// Destructor
	virtual ~CApertures();

	// assignment operator
	CApertures &operator=(const CApertures &orig);

	// Get number of apertures
	int Count(void) const
	{ return m_Count; }

	// Returns true if the table is empty
	bool Empty(void) const
	{ return m_Count==0; }

	// Clear the table
	void Clear(void);

	// Add an aperture, if the aperture with the same id is 
	// in the table, it changes its parameters
	void Add(const CAperture &item);

	// Find aperture by id 
	int Find(int id) const;

	// Return index of smallest aperture
	int FindSmallest(void) const;

	// Get aperture by index
	const CAperture *Get(int index) const;

	// Get aperture by index
	int GetId(int index) const;

	// Get aperture by index
	double GetRadius(int index) const;

protected:
	int			m_Count, m_Capacity;
	CAperture	**m_List;
};

#endif
