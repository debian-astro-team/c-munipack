/**************************************************************

mv_utils.h (C-Munipack project)
Common functions, definitions, etc.
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CCD_UTILS_H
#define CCD_UTILS_H

#include <Windows.h>

/* ************* Common constats *******************/

// Max length of the line in text files
#define MAXLINE 1024

/* **************** Common macros ******************/

#define fsig(x)   ((x)>=0?(+1):(-1))
#define fabs(x)   ((x)>=0?(x):-(x))
#define fmax(x,y) (((x)>(y))?(x):(y))
#define fmin(x,y) (((x)<(y))?(x):(y))

/**************** Common functions *****************/

// Finds a string needle in array haystack (case sensitive)
const char *memstr(const char *haystack, size_t haystacklen, const char *needle);

// Finds a string needle in array haystack (case insensitive)
const char *memcasestr(const char *haystack, size_t haystacklen, const char *needle);

// Round to nearest integer
int RoundToInt(double x);

// Limit value
int LimitValue(int value, int min, int max);
double LimitValue(double x, double min, double max);

// Make copy of a string
char *StrDup(const char *str);

enum UsrColor {
	CMPACK_COLOR_DEFAULT,
	CMPACK_COLOR_GRAY,
	CMPACK_COLOR_RED,
	CMPACK_COLOR_GREEN,
	CMPACK_COLOR_BLUE,
	CMPACK_COLOR_OUTLINE,
	CMPACK_COLOR_YELLOW,
	CMPACK_N_COLORS
};

COLORREF GetScreenColor(UsrColor index);
COLORREF GetPrintColor(UsrColor index);

#endif
