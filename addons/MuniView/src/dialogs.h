/**************************************************************

dialogs.h (C-Munipack project)
A base class for dialogs
Copyright (C) 2012 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef DIALOGS_H
#define DIALOGS_H

#include <Windows.h>

//
// Base class for modal dialogs
//
class CDialog
{
public:
	// Constructor
	CDialog();

	// Destructor
	virtual ~CDialog(void) {}

	// Show modal dialog, blocks until the dialog is closed
	// params:
	//	hParent				- [in] parent window
	void ShowModal(HWND hParent);

protected:
	HWND	m_hWnd;

	// Resource id 
	virtual int ResourceId(void) = 0;

	// Initialize the dialog (WM_INITDIALOG handler)
	virtual BOOL OnInitDialog(void) { return FALSE; }

	// Command handler (WM_COMMAND handler)
	virtual BOOL OnCommand(int iCtrlId, int iCode) { return FALSE; }

private:
	// Dialog procedure
	static INT_PTR CALLBACK DlgProc(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);
};

#endif
