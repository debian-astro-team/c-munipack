/**************************************************************

about_dlg.h (C-Munipack project)
The "About plug-in" dialog
Copyright (C) 2012 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef ABOUT_DLG_H
#define ABOUT_DLG_H

#include "dialogs.h"

//
// "About plugin" dialog
//
class CAboutDlg:public CDialog
{
public:
	// Constructor
	CAboutDlg();

	// Destructor
	virtual ~CAboutDlg(void);

protected:
	HFONT	m_hTitleFont, m_hUrlFont;

	// Resource id 
	virtual int ResourceId(void);

	// Initialize the dialog
	virtual BOOL OnInitDialog(void);

	// Command handler
	virtual BOOL OnCommand(int iCtrlId, int iCode);
};

#endif
