/**************************************************************

about_dlg.cpp (C-Munipack project)
The "About plug-in" dialog
Copyright (C) 2012 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#include <stdio.h>
#include <windows.h>

#include "about_dlg.h"
#include "main.h"
#include "config.h"
#include "resource.h"

// Constructor
CAboutDlg::CAboutDlg(void)
{
	HDC hdc = CreateCompatibleDC(0);
	m_hTitleFont = CreateFont(-MulDiv(22, GetDeviceCaps(hdc, LOGPIXELSY), 72), 
		0, 0, 0, 0, 0, 0, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH, "Arial");
	m_hUrlFont = CreateFont(-MulDiv(8, GetDeviceCaps(hdc, LOGPIXELSY), 72), 
		0, 0, 0, 0, 0, TRUE, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH, "Lucida Console");
	DeleteDC(hdc);
}

// Destructor
CAboutDlg::~CAboutDlg(void)
{
	DeleteObject(m_hTitleFont);
	DeleteObject(m_hUrlFont);
}

// Resource id 
int CAboutDlg::ResourceId(void)
{
	return IDD_ABOUT;
}

// Dialog initialization
BOOL CAboutDlg::OnInitDialog(void)
{
	SendDlgItemMessage(m_hWnd, IDC_TITLE, WM_SETFONT, (WPARAM)m_hTitleFont, 0); 
	SetDlgItemText(m_hWnd, IDC_VERSION, "Version " VERSION);	
	SendDlgItemMessage(m_hWnd, IDC_URL, WM_SETFONT, (WPARAM)m_hUrlFont, 0); 
	return TRUE;
}

// Command handler
BOOL CAboutDlg::OnCommand(int iCtrlId, int iCode)
{
	char buf[256];

	switch (iCtrlId)
	{
	case IDOK:
		EndDialog(m_hWnd, IDOK);
		return TRUE;

	case IDC_URL:
		if (iCode==BN_CLICKED) {
			GetDlgItemText(m_hWnd, IDC_URL, buf, 256);
			ShellExecute(m_hWnd, "open", buf, 0, 0, SW_SHOWMAXIMIZED);
		}
		return TRUE;
	}

	return FALSE;
}
