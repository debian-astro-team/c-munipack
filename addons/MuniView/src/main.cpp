/**************************************************************

Main.cpp (C-Munipack project)
Lister plugin main module
Copyright (C) 2012 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#include <stdio.h>
#include <windows.h>

#include "mv_image.h"
#include "mv_chart.h"
#include "mv_graph.h"

#define BUFF_SIZE 8192

#define IDC_PAINT 1001
#define IDC_STATUS 1002

static const char *detectstring = 
	"(FORCE|(SIZE<16777216))&(FINDI(\"ST-\")|FINDI(\"SBIG\")|FINDI(\"PixCel\")|FINDI(\"SIMPLE  =\")|"
	"FINDI(\"<?xml\")|FINDI(\"C-Munipack\")|FINDI(\"NL   NX   NY\")|"
	"FINDI(\"JD V-C s1\")|FINDI(\"JD V1 s1\")|FINDI(\"JD MAG0 ERR0\")|"
	"FINDI(\"JDHEL V-C s1\")|FINDI(\"JDHEL V1 s1\")|FINDI(\"JDHEL MAG0 ERR0\")|" 
	"FINDI(\"JD OFFSETX OFFSETY\")|FINDI(\"JD AIRMASS ALTITUDE\")|FINDI(\"JD X Y\")|FINDI(\"JD CCDTEMP\")|"
	"FINDI(\"INDEX MEAN_MAG STDEV\")|FINDI(\"AP# D(C-C1)\"))";

HINSTANCE g_hInst = 0;

extern "C" {

static bool g_initialized = false;

static void Initialize(void)
{
	if (!g_initialized) {
		g_initialized = true;
		cmpack_init();
		TCCD_Interface::RegisterClass(g_hInst);
	}
}

static TCCD_Interface *MakeInterface(const char *buf, int bytes)
{
	if (TCCD_Image::Test(buf, bytes))
		return new TCCD_Image();
	if (TCCD_Chart::Test(buf, bytes))
		return new TCCD_Chart();
	if (TCCD_Graph::Test(buf, bytes))
		return new TCCD_Graph();
	return NULL;
}

BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
    switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		g_hInst=(HINSTANCE)hModule;
		break;
	case DLL_PROCESS_DETACH:
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
    }
    return TRUE;
}

HWND __stdcall ListLoad(HWND ParentWin,char* FileToLoad,int ShowFlags)
{	
	RECT r;
	char buf[BUFF_SIZE];
	FILE *f = NULL;

	Initialize();

	if (!fopen_s(&f, FileToLoad, "rb")) {
		memset(buf, 0, 8192);
		size_t bytes = fread(buf, 1, BUFF_SIZE, f);
		fclose(f);
		if (bytes>0 && bytes<0x7FFFFFFF) {
			TCCD_Interface *iface = MakeInterface(buf, (int)bytes);
			if (iface && iface->Load(FileToLoad)) {
				iface->SetShowFlags(ShowFlags);
				GetClientRect(ParentWin, &r);
				return CreateWindow(PAINTWNDCLASS, NULL, WS_CHILD | WS_VISIBLE, r.left, r.top, 
					r.right-r.left, r.bottom-r.top, ParentWin, NULL, g_hInst, iface);	
			}
			delete iface;
		}
	}
	return NULL;
}

void __stdcall ListCloseWindow(HWND ListWin)
{
	DestroyWindow(ListWin);
	return;
}

int __stdcall ListSendCommand(HWND ListWin, int Command, int Parameter)
{
	switch(Command) 
	{
	case lc_newparams:
		TCCD_Interface *data = (TCCD_Interface*)GetWindowLongPtr(ListWin, GWLP_USERDATA);
		if (data)
			data->SetShowFlags(Parameter);
		return LISTPLUGIN_OK;
	}
	return LISTPLUGIN_ERROR;
}

void __stdcall ListGetDetectString(char* DetectString,int maxlen)
{
	int len = (int)strlen(detectstring);
	if (len>=maxlen) 
		DetectString[0] = 0;
	else
		strcpy_s(DetectString, maxlen, detectstring);
}

HBITMAP __stdcall ListGetPreviewBitmap(char* FileToLoad, int width, int height, 
	char* contentbuf,int contentbuflen)
{
	HBITMAP hBmp = 0;

	Initialize();

	TCCD_Interface *iface = MakeInterface(contentbuf, contentbuflen); 
	if (iface && iface->Load(FileToLoad)) 
		hBmp = iface->MakePreview(width, height);
	delete iface;
	return hBmp;
}

}