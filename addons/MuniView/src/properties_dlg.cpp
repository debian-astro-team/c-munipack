/**************************************************************

properties_dlg.cpp (C-Munipack project)
The "Properties" dialogs
Copyright (C) 2012 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#include <stdio.h>
#include <windows.h>

#include "properties_dlg.h"
#include "mv_image.h"
#include "mv_chart.h"
#include "mv_graph.h"
#include "main.h"
#include "resource.h"

// Constructor
CFileProperties::CFileProperties(const TCCD_Interface *parent):m_FileName(NULL), 
	m_DirPath(NULL), m_pParent(parent)
{
	HDC hdc = CreateCompatibleDC(0);
	m_hTitleFont = CreateFont(-MulDiv(9, GetDeviceCaps(hdc, LOGPIXELSY), 72), 
		0, 0, 0, FW_BOLD, 0, TRUE, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH, "Microsoft Sans Serif");
	DeleteDC(hdc);

	if (parent && parent->FilePath()) {
		const char *filepath = parent->FilePath();
		const char *aux = strrchr(filepath, '\\');
		if (aux) {
			size_t len = strlen(aux+1);
			m_FileName = (char*)malloc(len+1);
			if (m_FileName)
				strcpy_s(m_FileName, len+1, aux+1);
			len = (aux-filepath);
			m_DirPath = (char*)malloc(len+1);
			if (m_DirPath) {
				strncpy_s(m_DirPath, len+1, filepath, len);
				m_DirPath[len] = '\0';
			}
		}
	}
}

// Destructor
CFileProperties::~CFileProperties()
{
	DeleteObject(m_hTitleFont);
	free(m_FileName);
	free(m_DirPath);
}

// Initialize the dialog
BOOL CFileProperties::OnInitDialog(void)
{
	SendDlgItemMessage(m_hWnd, IDC_FILE_SECTION, WM_SETFONT, (WPARAM)m_hTitleFont, 0); 
	SetDlgItemText(m_hWnd, IDC_FILE, m_FileName);
	SetDlgItemText(m_hWnd, IDC_PATH, m_DirPath);
	return TRUE;
}

// Command handler
BOOL CFileProperties::OnCommand(int iCtrlId, int iCode)
{
	if (iCtrlId == IDOK) {
		EndDialog(m_hWnd, IDOK);
		return TRUE;
	}
	return FALSE;
}

// Constructor
CImageProperties::CImageProperties(const TCCD_Image *parent):CFileProperties(parent)
{
}

// Resource id 
int CImageProperties::ResourceId(void)
{
	return IDD_IMAGE_PROPERTIES;
}

// Dialog initialization
BOOL CImageProperties::OnInitDialog(void)
{
	char buf[256];

	CFileProperties::OnInitDialog();
	SendDlgItemMessage(m_hWnd, IDC_FRAME_SECTION, WM_SETFONT, (WPARAM)m_hTitleFont, 0); 
	if (m_pParent) {
		const TCCD_Image *parent = (TCCD_Image*)m_pParent;
		SetDlgItemText(m_hWnd, IDC_FORMAT, parent->FormatName());
		if (parent->JulianDate()>0) {
			const CmpackDateTime *dt = parent->DateTime();
			sprintf_s(buf, "%04d-%02d-%02d %d:%02d:%02d.%03d", dt->date.year, dt->date.month, dt->date.day,
				dt->time.hour, dt->time.minute, dt->time.second, dt->time.milisecond);
			SetDlgItemText(m_hWnd, IDC_DATE, buf);
			sprintf_s(buf, "%.7lf", parent->JulianDate());
			SetDlgItemText(m_hWnd, IDC_JD, buf);
		} else {
			SetDlgItemText(m_hWnd, IDC_DATE, "Not available");
			SetDlgItemText(m_hWnd, IDC_JD, "Not available");
		}
		if (parent->Exposure()>0) {
			sprintf_s(buf, "%.3lf seconds", parent->Exposure());
			SetDlgItemText(m_hWnd, IDC_EXPOSURE, buf);
		} else
			SetDlgItemText(m_hWnd, IDC_EXPOSURE, "Not available");
		if (parent->CCDTemperature()>-999 && parent->CCDTemperature()<999) {
			sprintf_s(buf, "%.2lf \xB0""C", parent->CCDTemperature());
			SetDlgItemText(m_hWnd, IDC_CCDTEMP, buf);
		} else
			SetDlgItemText(m_hWnd, IDC_CCDTEMP, "Not available");
		if (parent->Filter())
			SetDlgItemText(m_hWnd, IDC_FILTER, parent->Filter());
		else
			SetDlgItemText(m_hWnd, IDC_FILTER, "Not available");
		if (parent->Object())
			SetDlgItemText(m_hWnd, IDC_OBJECT, parent->Object());
		else
			SetDlgItemText(m_hWnd, IDC_OBJECT, "Not available");
		if (parent->Observer())
			SetDlgItemText(m_hWnd, IDC_OBSERVER, parent->Observer());
		else
			SetDlgItemText(m_hWnd, IDC_OBSERVER, "Not available");
		if (parent->AvgFrames()>0) {
			sprintf_s(buf, "%d", parent->AvgFrames());
			SetDlgItemText(m_hWnd, IDC_FRAMES_AVG, buf);
		} else
			SetDlgItemText(m_hWnd, IDC_FRAMES_AVG, "Not available");
		if (parent->SumFrames()>0) {
			sprintf_s(buf, "%d", parent->SumFrames());
			SetDlgItemText(m_hWnd, IDC_FRAMES_SUM, buf);
		} else
			SetDlgItemText(m_hWnd, IDC_FRAMES_SUM, "Not available");
		if (parent->ImageWidth()>0 && parent->ImageHeigt()>0) {
			sprintf_s(buf, "%d x %d pixels", parent->ImageWidth(), parent->ImageHeigt());
			SetDlgItemText(m_hWnd, IDC_IMG_SIZE, buf);
		} else
			SetDlgItemText(m_hWnd, IDC_IMG_SIZE, "Not available");
	}
	return TRUE;
}

// Constructor
CChartProperties::CChartProperties(const TCCD_Chart *parent):CFileProperties(parent)
{
}

// Resource id 
int CChartProperties::ResourceId(void)
{
	return IDD_CHART_PROPERTIES;
}

// Dialog initialization
BOOL CChartProperties::OnInitDialog(void)
{
	char buf[256];

	CFileProperties::OnInitDialog();
	SendDlgItemMessage(m_hWnd, IDC_FRAME_SECTION, WM_SETFONT, (WPARAM)m_hTitleFont, 0); 
	if (m_pParent) {
		const TCCD_Chart *parent = (TCCD_Chart*)m_pParent;
		SetDlgItemText(m_hWnd, IDC_FORMAT, parent->FormatName());
		if (parent->JulianDate()>0) {
			const CmpackDateTime *dt = parent->DateTime();
			sprintf_s(buf, "%04d-%02d-%02d %d:%02d:%02d.%03d", dt->date.year, dt->date.month, dt->date.day,
				dt->time.hour, dt->time.minute, dt->time.second, dt->time.milisecond);
			SetDlgItemText(m_hWnd, IDC_DATE, buf);
			sprintf_s(buf, "%.7lf", parent->JulianDate());
			SetDlgItemText(m_hWnd, IDC_JD, buf);
		} else {
			SetDlgItemText(m_hWnd, IDC_DATE, "Not available");
			SetDlgItemText(m_hWnd, IDC_JD, "Not available");
		}
		if (parent->Exposure()>0) {
			sprintf_s(buf, "%.3lf seconds", parent->Exposure());
			SetDlgItemText(m_hWnd, IDC_EXPOSURE, buf);
		} else
			SetDlgItemText(m_hWnd, IDC_EXPOSURE, "Not available");
		if (parent->Filter())
			SetDlgItemText(m_hWnd, IDC_FILTER, parent->Filter());
		else
			SetDlgItemText(m_hWnd, IDC_FILTER, "Not available");
		if (parent->Object())
			SetDlgItemText(m_hWnd, IDC_OBJECT, parent->Object());
		else
			SetDlgItemText(m_hWnd, IDC_OBJECT, "Not available");
		if (parent->Observer())
			SetDlgItemText(m_hWnd, IDC_OBSERVER, parent->Observer());
		else
			SetDlgItemText(m_hWnd, IDC_OBSERVER, "Not available");
		sprintf_s(buf, "%d", parent->Stars());
		SetDlgItemText(m_hWnd, IDC_STARS, buf);
	}
	return TRUE;
}

// Constructor
CTableProperties::CTableProperties(const TCCD_Graph *parent):CFileProperties(parent)
{
	m_Type = parent->TableType();
}

// Resource id 
int CTableProperties::ResourceId(void)
{
	if (m_Type == CMPACK_TABLE_LCURVE_DIFF || m_Type == CMPACK_TABLE_LCURVE_INST)
		return IDD_LCURVE_PROPERTIES;
	else
		return IDD_TABLE_PROPERTIES;
}

// Dialog initialization
BOOL CTableProperties::OnInitDialog(void)
{
	//char buf[256];

	CFileProperties::OnInitDialog();
	SendDlgItemMessage(m_hWnd, IDC_LCURVE_SECTION, WM_SETFONT, (WPARAM)m_hTitleFont, 0); 
	if (m_pParent) {
		const TCCD_Graph *parent = (TCCD_Graph*)m_pParent;
		SetDlgItemText(m_hWnd, IDC_FORMAT, parent->FormatName());
		if (parent->Filter())
			SetDlgItemText(m_hWnd, IDC_FILTER, parent->Filter());
		else
			SetDlgItemText(m_hWnd, IDC_FILTER, "Not available");
	}
	return TRUE;
}
