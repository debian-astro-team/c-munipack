/**************************************************************

mv_class.h (C-Munipack project)
Base class for viewers
Copyright (C) 2012 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CCDCLASS_H
#define CCDCLASS_H

#define MAX_GRAPH_COLS 256
#define PAINTWNDCLASS "MuniViewPaintWnd"

#include "mv_utils.h"
#include "listplug.h"

/****************************************************************/
/* Abstract interface                                           */
/* This is base class for all display classes                   */
/****************************************************************/
class TCCD_Interface 
{
public:
	// Register PaintWnd window class
	static void RegisterClass(HINSTANCE hInst);
		
public:
	// Constructor
	TCCD_Interface(void);
		
	// Destructor
	virtual ~TCCD_Interface(void);
		
	// Load a file
	// params:
	//	filename		- [in] path to the file to be loaded
	// Returns true on success and false on failure
	virtual bool Load(const char *filename) = 0;
	    
	// Enables and disables fitting image to window
	// params:
	//	ShowFlags		- [in] parameter of ListLoad, ListSendCommand
	virtual void SetShowFlags(int ShowFlags) {}
		
	// Makes preview and returns the HBITMAP
	// params:
	//	max_width, max_height - [in] max. size of the image
	// Returns handle to a new bitmap
	virtual HBITMAP MakePreview(int max_width, int max_height) { return NULL; }
		
	// Get image format name
	const char *FilePath(void) const { return m_FilePath; }

protected:
	HWND	m_hWnd;						// Window handle
	char	*m_FilePath;				// Path to the loaded file
	
	// Set file path
	void SetFilePath(const char *filepath);

	// Ask for redraw, invalidate the window
	void Invalidate(void);

	// Update HSB parameters
	void UpdateHSB(int nMin, int nMax, int nPage, int nPos);

	// Update VSB parameters
	void UpdateVSB(int nMin, int nMax, int nPage, int nPos);

	// Draw a bitmap, image, chart, ...
	virtual void OnRender(HDC hdc) {}
		
	// Window size changed
	virtual void OnSizeChanged(void) {}
		
	// HSB changed
	virtual void OnHSBChanged(int nPos) {}
		
	// VSB changed
	virtual void OnVSBChanged(int nPos) {}

	// Keyboard event handler
	virtual void OnKeyDown(int vKey);

	// Instance window procedure
	virtual LRESULT InstanceWndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
		
	// Create menu 
	virtual HMENU CreateCtxMenu(void);

	// Update menu items
	virtual void UpdateMenu(HMENU hMenu) {};

	// Context menu commands
	virtual void OnMenuCommand(int iCmdId);
	
	// Get topic id (help page opened)
	virtual int GetTopicId(void) { return 0; }

private:
	HBITMAP	m_hBitmap;					// Backcreen buffer
	int		m_BmpWidth, m_BmpHeight;	// Backscreen buffer size
	bool	m_NeedUpdate;				// Update backscreen buffer before rendering

	// PaintWnd window procedure
	static LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

	// Initialize control
	void OnCreate(void);
		
	// Free allocated resources
	void OnDestroy(void);
		
	// Paints backscreen buffer to display context
	void OnPaint(HDC hDC, RECT *pRect);
		
	// Window enabled/disabled
	void OnEnable(bool enabled);
		
	// Resize visible area
	void OnSize(void);

	// HSB changed
	void OnHScroll(int nCode);
		
	// VSB changed
	void OnVScroll(int nCode);

	// Open context menu
	void OnContextMenu(POINT *pt);

	// Show "About plugin" dialog
	void ShowAboutDlg(void);

	// Show help 
	void ShowHelp(int TopicId);
};

#endif

