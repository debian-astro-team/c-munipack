/**************************************************************

mv_channels.h (C-Munipack project)
List of channels
Copyright (C) 2012 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CCD_GRAPH_CHANNELS_H
#define CCD_GRAPH_CHANNELS_H

//
// Channel info
//
enum tChannelInfo {
	DATA_UNDEFINED,				// Nothing
	DATA_JD,					// Julian date
	DATA_MAGNITUDE,				// Magnitude
	DATA_HELCOR,				// Heliocentric correction
	DATA_AIRMASS,				// Airmass coefficient
	DATA_OFFSET,				// Pixel offset
	DATA_DEVIATION,				// Deviation
	DATA_ALTITUDE,				// Altitude
	DATA_ANY					// General purpose FP numbers
};

//
// Channel descriptor
//
class CChannel
{
public:
	// Constructors
	CChannel(class TCCD_Graph *tab, int y_col, int u_col, tChannelInfo info);

	// Destructor
	virtual ~CChannel(void);

	// Get identifier
	int Column(void) const
	{ return m_Column; }

	// Get identifier
	int ColumnU(void) const
	{ return m_ColumnU; }

	// Get radius in pixels
	const char *Name(void);

	// Is the aperture valid?
	bool Valid(void) const
	{ return m_Column>=0; }
	
	// Channel information
	tChannelInfo Info(void) const
	{ return m_Info; }

	// Minimum value
	double Min(void) const;

	// Maximum value
	double Max(void) const;

	// Maximum error value
	double MaxU(void) const;

	// Enable/disable exporting of this column
	void SetExported(bool enable);

	// Is this column exported?
	bool Exported(void) const
	{ return m_Exported; }
		
private:
	TCCD_Graph		*m_Table;
	int				m_Column, m_ColumnU;
	char			*m_Name;
	bool			m_Exported;
	tChannelInfo	m_Info;

	// Assignment operator
	CChannel &operator=(const CChannel&);

	CChannel(const CChannel&);
};

//
// Table of channels
//
class CChannels
{
public:
	// Constructor(s)
	CChannels(void);
	
	// Destructor
	virtual ~CChannels();

	// Get number of apertures
	int Count(void) const
	{ return m_Count; }

	// Returns true if the table is empty
	bool Empty(void) const
	{ return m_Count==0; }

	// Clear the table
	void Clear(void);

	// Add a channel, if the channel with the column index is 
	// already in the table, it changes its parameters
	void Add(CChannel *item);

	// Find first channel by name
	int FindColumn(int column) const;

	// Find first channel by name
	int FindFirst(const char *name) const;

	// Find first channel by given info
	int FindFirst(tChannelInfo info) const;

	// Get aperture by index
	const CChannel *Get(int index) const;
	CChannel *Get(int index);

	// Get column by index
	int GetColumn(int index) const;

	// Get column by index
	int GetColumnU(int index) const;

	// Get name by index
	const char *GetName(int index);

	// Get channel info
	tChannelInfo GetInfo(int index) const;

	// Minimum value
	double GetMin(int index) const;

	// Maximum value
	double GetMax(int index) const;

	// Maximum error value
	double GetMaxU(int index) const;

	// Get "exported" flag
	bool GetExported(int index) const;

protected:
	int			m_Count, m_Capacity;
	CChannel	**m_List;

private:
	// assignment operator
	CChannels &operator=(const CChannels&);

	// Copy constructor
	CChannels(const CChannels&);
};

#endif
