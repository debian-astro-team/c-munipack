/**************************************************************

dialogs.cpp (C-Munipack project)
Base class for modal dialogs
Copyright (C) 2012 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#include <stdio.h>
#include <windows.h>

#include "dialogs.h"
#include "main.h"

// Constructor
CDialog::CDialog(void):m_hWnd(NULL)
{
}

// Show dialog
void CDialog::ShowModal(HWND hParent)
{
	DialogBoxParam(g_hInst, MAKEINTRESOURCE(ResourceId()), hParent, DlgProc, (LPARAM)this);
}

// Dialog procedure
INT_PTR CALLBACK CDialog::DlgProc(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	CDialog *pMe = (CDialog*)GetWindowLongPtr(hwndDlg, GWLP_USERDATA);

	switch (uMsg)
	{
	case WM_INITDIALOG:
		if (lParam) {
			((CDialog*)lParam)->m_hWnd = hwndDlg; 
			SetWindowLongPtr(hwndDlg, GWLP_USERDATA, (LONG_PTR)lParam);
			return ((CDialog*)lParam)->OnInitDialog();
		}
		return TRUE;

	case WM_COMMAND:
		if (pMe) 
			return pMe->OnCommand(LOWORD(wParam), HIWORD(wParam));
		return FALSE;

	case WM_CLOSE:
		EndDialog(hwndDlg, IDCANCEL);
		return TRUE;
	}
	return FALSE;
}
