/**************************************************************

mv_utils.cpp (C-Munipack project)
Common functions, macros, etc.
Copyright (C) 2012 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "mv_utils.h"

const char *memstr(const char *haystack, size_t haystacklen, const char *needle)
// Finds a string needle in array haystack (uses memmem)
{
	size_t i, j;
	const char *aux, *s1, *s2;

	for (i=0, aux=haystack; i<haystacklen; i++, aux++) {
		if (*aux == *needle) {
			s1 = aux; s2 = needle; j = i;
			do {
				s1++; s2++; j++;
				if (*s2=='\0') return aux;
				if (j>=haystacklen) return NULL;
			} while (*s1==*s2);
		}
	}
	return NULL;
}

const char *memcasestr(const char *haystack, size_t haystacklen, const char *needle)
// Finds a string needle in array haystack (uses memmem)
{
	size_t i, j;
	const char *aux, *s1, *s2;

	for (i=0, aux=haystack; i<haystacklen; i++, aux++) {
		if (toupper(*aux) == toupper(*needle)) {
			s1 = aux; s2 = needle; j = i;
			do {
				s1++; s2++; j++;
				if (*s2=='\0') return aux;
				if (j>=haystacklen) return NULL;
			} while (toupper(*s1)==toupper(*s2));
		}
	}
	return NULL;
}

// Limit value
int LimitValue(int value, int min, int max)
{
	if (value < min)
		return min;
	if (value > max)
		return max;
	return value;
}
double LimitValue(double x, double min, double max)
{
	if (x < min)
		return min;
	else if (x > max)
		return max;
	else
		return x;
}

int RoundToInt(double x)
{
	if (x > 2147483647)
		return 2147483647;
	else if (x < -2147483647 - 1)
		return - 2147483647 - 1;
	else if (x >= 0.0)
		return (int)(x + 0.5);
	else
		return (int)(x - 0.5);
}

char *StrDup(const char *str)
{
	if (str) {
		size_t len = strlen(str);
		char *buf = (char*)malloc((len+1)*sizeof(char));
		if (buf) {
			memcpy(buf, str, len);
			buf[len] = '\0';
		}
		return buf;
	}
	return NULL;
}

COLORREF GetScreenColor(UsrColor index)
{
	static const COLORREF ScreenColor[CMPACK_N_COLORS] = 
	{
		RGB(255,255,255),
		RGB(128,128,128),
		RGB(178,0,0),
		RGB(0,153,0),
		RGB(0,102,255),
		RGB(0,0,0),
		RGB(178,178,0)
	};

	if (index>=0 && index<CMPACK_N_COLORS)
		return ScreenColor[index];
	return 0;
}

COLORREF GetPrintColor(UsrColor index)
{
	static const COLORREF PrintColor[CMPACK_N_COLORS] = 
	{
		RGB(0,0,0),
		RGB(128,128,128),
		RGB(178,0,0),
		RGB(0,153,0),
		RGB(0,102,255),
		RGB(255,255,255),
		RGB(178,178,0)
	};

	if (index>=0 && index<CMPACK_N_COLORS)
		return PrintColor[index];
	return 0;
}
