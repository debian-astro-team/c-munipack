/**************************************************************

mv_chart.h (C-Munipack project)
Display image class
Copyright (C) 2012 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CCD_CHART_H
#define CCD_CHART_H

#include <cmunipack.h>

#include "mv_class.h"
#include "mv_apertures.h"

class TCCD_Chart:public TCCD_Interface 
{
public:
	// Check if the buffer contains beginning of an image
	static bool Test(const char *buf, int bytes);

public:
	// Constructor
	TCCD_Chart(void);
		
	// Destructor
	virtual ~TCCD_Chart(void);
		
	// Load a file
	virtual bool Load(const char *filename);
	    
	// Enables and disables fitting image to window
	virtual void SetShowFlags(int ShowFlags);
		
	// Makes preview and returns the HBITMAP
	virtual HBITMAP MakePreview(int max_width, int max_height);

	// Get image format name
	const char *FormatName(void) const;

	// Get Julian date of observation
	double JulianDate(void) const { return m_JulianDate; }

	// Get date and time of observation
	const CmpackDateTime *DateTime(void) const { return &m_DateTime; }

	// Get optical filter name
	const char *Filter(void) const { return m_Filter; }

	// Get object's designation
	const char *Object(void) const { return m_Object; }

	// Get observer's name
	const char *Observer(void) const { return m_Observer; }

	// Get exposure duration in seconds
	double Exposure(void) const { return m_Exposure; }

	// Get number of frames summed
	int Stars(void) const { return m_Stars; }

protected:
	// Adjust scroll bars
	virtual void OnSizeChanged(void);

	// Draw a bitmap, image, chart, ...
	virtual void OnRender(HDC hdc);

	// Keyboard event handler
	virtual void OnKeyDown(int vKey);

	// Create menu 
	virtual HMENU CreateCtxMenu(void);

	// Context menu commands
	virtual void OnMenuCommand(int iCmd);

	// Get topic id (help page opened)
	virtual int GetTopicId(void);

private:
	struct tChartData
	{
		int			id;
		bool		filled, topmost;
		double		xproj, yproj;
		double		size;
		char		*tag;
		UsrColor	color;
	};

	CmpackPhtFile	*m_Pht;						// Photometry file
	CmpackCatFile	*m_Cat;						// Catalogue file
	double			m_JulianDate;				// Julian date of observation
	double			m_Exposure;					// Exposure duration in seconds
	char			*m_Filter;					// Optical filter name
	char			*m_Object;					// Object designation
	char			*m_Observer;				// Oberver's name
	CmpackDateTime	m_DateTime;					// Date and time of observation
	int				m_Stars;					// Number of stars

	tChartData		*m_Data;					// Object descriptors
	int				m_Count, m_Capacity;		// Number of objects
	int				m_Width, m_Height;			// Chart size in pixels
	CApertures		m_Apertures;				// List of apertures
	int				m_ApertureIndex;

	bool			m_CenterImage;				// Center image to window
	HFONT			m_hFont, m_hBold;			// Fonts
	RECT			m_CanvasRc;					// Canvas rectangle
	char			*m_Title;					// Title text
	int				m_SizeBoost;				// Circle boost factor

	// Update file properties
	void UpdateParams();

	// Update list of apertures
	void UpdateApertures();

	// Update title text
	void UpdateTitle();

	// Update chart data
	void UpdateData();

	// Clear data
	void Clear();

	// Set title
	void SetTitle(const char *caption);

	// Update reference magnitudes
	double GetRefMag(int index);

	// Set size boost factor
	void SetSizeBoost(int factor);

	// Change aperture
	void SelectAperture(int index);

	// Render chart
	void RenderChart(HDC hDC, int left, int top, int width, int height, 
		bool for_print, bool for_preview);

	// Paint object
	void DrawObject(HDC hDC, HBRUSH *brushes, HPEN *pens, tChartData *item, 
		double kx, double ky, int dx, int dy, double zoom, bool preview);

	// Paint label
	void DrawLabel(HDC hDC, COLORREF *colors, tChartData *item, double kx, double ky, 
		int dx, int dy, double zoom);

	// Compute text bounding rectangle
	void text_extents(HDC hDC, const char *buf, SIZE *size);

	// Compute text height
	int	text_height(HDC hDC, const char *buf);

	// Show image properties
	void ShowProperties(void);

	// Copy to clipboard
	void CopyToClipboard(void);
};

#endif
