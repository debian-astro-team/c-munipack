#ifndef IDC_STATIC
#define IDC_STATIC (-1)
#endif

#define IDD_ABOUT                               101
#define IDD_IMAGE_PROPERTIES                    104
#define IDD_CHART_PROPERTIES                    106
#define IDD_TABLE_PROPERTIES                    108
#define IDD_LCURVE_PROPERTIES                   110
#define IDC_FILE                                1001
#define IDC_VERSION                             1001
#define IDC_PATH                                1002
#define IDC_URL                                 1002
#define IDC_FORMAT                              1003
#define IDC_TITLE                               1003
#define IDC_DESCRIPTION                         1004
#define IDC_DATE                                1005
#define IDC_JD                                  1006
#define IDC_FILTER                              1007
#define IDC_EXPOSURE                            1008
#define IDC_CCDTEMP                             1009
#define IDC_FRAMES_AVG                          1010
#define IDC_FRAMES_SUM                          1011
#define IDC_OBJECT                              1012
#define IDC_OBSERVER                            1013
#define IDC_FILE_SECTION                        1014
#define IDC_FRAME_SECTION                       1015
#define IDC_STARS                               1017
#define IDC_LCURVE_SECTION                      1018
#define IDC_IMG_SIZE							1019
