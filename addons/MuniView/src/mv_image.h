/**************************************************************

mv_image.h (C-Munipack project)
Display image class
Copyright (C) 2012 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CCD_IMAGE_H
#define CCD_IMAGE_H

#include <cmunipack.h>

#include "mv_class.h"

class TCCD_Image:public TCCD_Interface 
{
public:
	// Check if the buffer contains beginning of an image
	static bool Test(const char *buf, int bytes);
		
public:
	// Constructor
	TCCD_Image(void);
		
	// Destructor
	virtual ~TCCD_Image(void);
		
	// Load a file
	virtual bool Load(const char *filename);
	    
	// Enables and disables fitting image to window
	virtual void SetShowFlags(int ShowFlags);
		
	// Makes preview and returns the HBITMAP
	virtual HBITMAP MakePreview(int max_width, int max_height);

	// Get image format name
	const char *FormatName(void) const { return m_FormatName; }

	// Get Julian date of observation
	double JulianDate(void) const { return m_JulianDate; }

	// Get date and time of observation
	const CmpackDateTime *DateTime(void) const { return &m_DateTime; }

	// Get optical filter name
	const char *Filter(void) const { return m_Filter; }

	// Get object's designation
	const char *Object(void) const { return m_Object; }

	// Get observer's name
	const char *Observer(void) const { return m_Observer; }

	// Get exposure duration in seconds
	double Exposure(void) const { return m_Exposure; }

	// Get CCD temperature in deg. C
	double CCDTemperature(void) const { return m_CCDTemperature; }

	// Get number of frames averaged
	int AvgFrames(void) const { return m_AvgFrames; }

	// Get number of frames summed
	int SumFrames(void) const { return m_SumFrames; }

	// Get image width
	int ImageWidth(void) const { return m_ImageWidth; }

	// Get image height
	int ImageHeigt(void) const { return m_ImageHeight; }

protected:
	// Adjust scroll bars
	virtual void OnSizeChanged(void);
		
	// Draw a bitmap, image, chart, ...
	virtual void OnRender(HDC hdc);

	// HSB changed
	virtual void OnHSBChanged(int nPos);
		
	// VSB changed
	virtual void OnVSBChanged(int nPos);

	// Keyboard event handler
	virtual void OnKeyDown(int vKey);

	// Create menu 
	virtual HMENU CreateCtxMenu(void);

	// Context menu commands
	virtual void OnMenuCommand(int iCmd);

	// Get topic id (help page opened)
	virtual int GetTopicId(void);

private:
	BITMAPINFO		*m_pDIB;				// DIB
	size_t			m_DIBSize;				// DIB size in bytes
	RECT			m_CanvasRc;				// Area where an image is rendered to
	char			*m_Title;				// Caption
	int				m_ScrollX, m_ScrollY;	// Scroll positions
	bool			m_Invert;				// Invert image
	bool			m_CenterImage;			// Center image to window
	bool			m_FitToWindow;			// Fit image to window
	bool			m_FitLargerOnly;		// Fit only larger images
	double			m_ZoomBase;				// Magnification log base
	double			m_ZoomMin, m_ZoomMax;	// Magnification factor limits
	double			m_ZoomPos;				// Custom magnification factor
	HFONT			m_hFont;				// Font used for rendering texts
	double			m_JulianDate;			// Julian date of observation
	CmpackDateTime	m_DateTime;				// Date and time of observation
	char			*m_FormatName;			// File format name;
	int				m_ImageWidth, m_ImageHeight;	// Image size
	char			*m_Filter;				// Optical filter name
	char			*m_Object;				// Object designation
	char			*m_Observer;			// Oberver's name
	double			m_Exposure;				// Exposure duration in seconds
	double			m_CCDTemperature;		// CCD temperature in deg. C
	int				m_AvgFrames, m_SumFrames;		// Number of frames averaged / summed

	// Set title
	void SetTitle(const char *caption);

	// Update title
	void UpdateTitle(void);

	// Update frame properties
	void UpdateParams(const CmpackCcdParams &params);

	// Update DIB image
	void UpdateDIB(CmpackImage *img);

	// Zoom in
	void ZoomIn(void);

	// Zoom out
	void ZoomOut(void);

	// Fit image to window
	void FitToWindow(void);

	// Show image properties
	void ShowProperties(void);

	// Copy to clipboard
	void CopyToClipboard(void);
	
	// Compute text bounding rectangle
	void text_extents(HDC hDC, const char *buf, SIZE *size);

	// Compute text height
	int	text_height(HDC hDC, const char *buf);
};

#endif
