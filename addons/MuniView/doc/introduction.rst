.. MuniView - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: introduction.rst,v 1.1 2015/07/06 08:24:15 dmotl Exp $

.. _introduction:
   
Introduction
============

MuniView is a lister plugin for Total Commander.


Compatibility
-------------

The plugin is compatible with Total Commander version >= 5.51


Features and capabilities
-------------------------

The plugin adds a support of several file formats:

- The FITS format with various date and time formats

- The SBIG's ST-x compressed and uncompressed formats

- The OES Astro format

- C-Munipack photometry files

- C-Munipack catalog files

- C-Munipack output files (light curves, etc.)

The following features are enhanced:

- Lister dialog

- Thumbnail view mode

- Quick View Panel

.. index::
   single: authors

Authors
-------

The project manager David Motl is also the author of the most part of the source codes. 
Some small pieces of the sources originate from Munipack package, coded by Filip Hroch. 
Algorithms for aperture photometry originate from Daophot software by P. B. Stetson. 
Munifind algorithm originates from Varfind tool coded by Lukas Kral. The code of the lossless 
JPEG decoder originates used in the RAW reader originates from Dave Coffin's dcraw utility.

The package uses the following third party software:

- FITSIO library, written by Dr. William Pence, NASA

- EXPAT library, written by James Clark's

- GTK+ toolkit, developed by the The GTK+ Team

- IJG JPEG library by Independent JPEG Group

- Sphinx documentation generator

- MikTex implementation of Tex typesetting system


.. index::
   single: licence

License and copying
-------------------

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the Free 
Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the LICENSE file for more details.


Where can I get the latest version?
-----------------------------------

The project is hosted by the SourceForge. The latest version of the binaries, source codes 
and documentation are available on the following address:

http://c-munipack.sourceforge.net/
