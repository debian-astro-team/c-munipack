.. MuniView - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: installation.rst,v 1.1 2015/07/06 08:24:15 dmotl Exp $

.. _installation:
   
Installation and uninstallation
===============================

Compiled binary code is included in the installation package of the C-Munipack 1.2
software in the "addons" directory. The source code can be obtained from the C-Munipack 
project's home page: http://c-munipack.sourceforge.net/


Automatic installation
----------------------

The automatic plugin installation is supported in TC >= 6.50.

#) Start TC and navigate to C-Munipack installation directory.

#) Enter the :file:`addons` --> :file:`MuniView` subdirectories.

#) Open the archive :file:`MuniView.zip` a confirmation dialog should appear.


Manual installation
-------------------

If the automatic installation is not possible, try the following steps:

#) Start TC and navigate to C-Munipack installation directory.

#) Enter the :file:`addons` --> :file:`MuniView` subdirectory.

#) In the other pane, go to the TC's installation directory.

#) Enter the :file:`plugins` --> :file:`wlx` subdirectory.

#) Create a fresh :file:`MuniView` directory.

#) Open the archive :file:`MuniView.zip` into a the :file:`MuniView` directory.

#) Open the configuration dialog and go to page :guilabel:`plugins`.

#) In the :guilabel:`Lister plugins` click on the :guilabel:`Configure` button, a new dialog appears.

#) Click on the :guilabel:`Add` button and in the open dialog navigate to the :file:`plugins` -->
   :file:`wlx` --> :file:`MuniView` directory and select file :file:`MuniView.wlx`. Confirm.
   
#) Close and save the configuration dialog.


Uninstallation
--------------

To uninstall the plugin:

#) Close TC and start it again to release the DLL from memory.

#) Open the configuration dialog and go to page :guilabel:`plugins`.

#) In the :guilabel:`Lister plugins` click on the :guilabel:`Configure` button, a new dialog appears.

#) Select the :file:`MuniView.wlx` plugin and click the :guilabel:`Remove` button.

#) Close and save the configuration dialog.
