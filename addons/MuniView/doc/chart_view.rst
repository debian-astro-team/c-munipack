.. MuniView - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: chart_view.rst,v 1.1 2015/07/06 08:24:15 dmotl Exp $

.. _chart-view:

Photometry files and catalog files
==================================

When you open the Lister window (F3 key) on a C-Munipack photometry file
or C-Munipack catalog file, the window is opened in the "Image or multimedia" 
display mode and a chart is presented in the window. If the file is very big (>16 MB), 
you have to switch the display mode manually ('4' key).

.. figure:: images/chart_view.png
   :align: center
   :alt: Lister window with a catalog file

   TC's Lister window in the "Image or multimedia" display mode 
   presenting a C-Munipack catalog file.

The scale of the chart is always chosen in such a way that the chart fits the window.

The descriptive information about the CCD image is presented at the
top of the window. The following data are shown: name of the file format (1),
date and time of observation or object designation (2) and optical filter name 
(3). The ordinal number of the aperture that is being used for drawing is also 
displayed (4).


Changing the aperture
---------------------

When a file is opened, the first aperture is automatically selected.
You can change the aperture:

- Press '+' key on the numeric pad to select the next aperture.

- Press '-' key on the numeric pad to select the previous aperture.


Changing the symbol size
------------------------

When a file is opened, the default symbol size is selected. You can change the size of the symbols:

- Press '*' key on the numeric pad to increase the size of symbols.

- Press '-' key on the numeric pad to decrease the size of symbols.


File properties
---------------

Basic properties are displayed at the top of the window. To show more information,
click on the image using the right mouse button to open the context menu. In the
menu, select the :menuselection:`Properties` item, a new dialog appears. In this dialog, 
information about the file are presented.


Copying image to clipboard
--------------------------

Click on the chart using the right mouse button to open the context menu. 
In the menu, select the :menuselection:`Copy to clipboard` item. 
