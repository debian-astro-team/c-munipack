.. MuniView - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: lister.rst,v 1.1 2015/07/06 08:24:15 dmotl Exp $

.. _lister:
   
Using Lister dialog
===================

.. toctree::
   :maxdepth: 1

   image_view
   chart_view
   graph_view
