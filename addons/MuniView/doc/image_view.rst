.. MuniView - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: image_view.rst,v 1.1 2015/07/06 08:24:15 dmotl Exp $

.. _image-view:

CCD images
==========

When you open the Lister window (F3 key) on a CCD image file, the window
is opened in the "Image or multimedia" display mode and an image is presented
in the window. If the file is very big (>16 MB), you have to switch the display
mode manually ('4' key).

.. figure:: images/image_view.png
   :align: center
   :alt: Lister window with a CCD image

   TC's Lister window in the "Image or multimedia" display mode 
   presenting a CCD image.
   
The descriptive information about the CCD image is presented at the
top of the window. The following data are shown: name of the file format (1),
image width and height in pixels (2), date and time of observation (3) and optical
filter name (4).


Changing zoom of the preview
----------------------------

You can change the zoom of the preview:

- Press 'F' key to turn on and off fitting of the image to the window.

- Press 'L' key to turn on and off fitting of larger images only.

- Press 'C' key to change alignment style of the image.

- Press '+' key on the numeric pad to increase the zoom.

- Press '-' key on the numeric pad to increase the zoom.


File properties
---------------

Basic properties are displayed at the top of the window. To show more 
information, click on the image using the right mouse button to open the context 
menu. In the menu, select the :menuselection:`Properties` item, a new dialog appears. In this 
dialog, information about the file and CCD frame are presented.


Copying image to clipboard
--------------------------

Click on the image using the right mouse button to open the context menu. 
In the menu, select the :menuselection:`Copy to clipboard` item. 
