/**************************************************************

ctxhelp.h (C-Munipack project)
Context help identifiers
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef MUNIVIEW_CTXHELP_H
#define MUNIVIEW_CTXHELP_H

/* Context help identifiers */
#define IDH_IMAGE_VIEW			1000	/* image-view */
#define IDH_CHART_VIEW			1001	/* chart-view */
#define IDH_GRAPH_VIEW			1002	/* graph-view */

#endif
