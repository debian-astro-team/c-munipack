.. MuniView - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: index.rst,v 1.1 2015/07/06 08:24:15 dmotl Exp $

   
Muniview 2.0 - User's manual
============================

| Release: |release|
| Updated: |today|

Copyright 2003 - 2012 David Motl 

Permission is granted to copy, distribute and/or modify this document under the terms of the 
GNU Free Documentation License, Version 1.2 or any later version published by the Free Software 
Foundation; with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.


Contents
--------

.. toctree::
   :maxdepth: 1

   introduction
   installation
   lister
