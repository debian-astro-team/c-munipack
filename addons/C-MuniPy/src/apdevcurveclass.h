#ifndef CMPACK_APDEVCURVE_CLASS
#define CMPACK_APDEVCURVE_CLASS

#include "tableclass.h"

typedef struct {
    Table t;
} ApDevCurve;

extern PyTypeObject ApDevCurveType;

#define ApDevCurve_Check(op) \
(op!=NULL && PyObject_TypeCheck(op, &ApDevCurveType))
#define ApDevCurve_CheckExact(op) (Py_TYPE(op) == &ApDevCurveType)

/* Creates a new object, steals the reference */
PyObject *ApDevCurve_FromHandle(CmpackTable *f);

#endif
