#ifndef CMPACK_CCDFILE_CLASS
#define CMPACK_CCDFILE_CLASS

typedef struct {
    PyObject_HEAD
	CmpackCcdFile *f;
	int p_valid;
	CmpackCcdParams p;
} CCDFile;

extern PyTypeObject CCDFileType;

#define CCDFile_Check(op) \
                 (op!=NULL && PyObject_TypeCheck(op, &CCDFileType))
#define CCDFile_CheckExact(op) (Py_TYPE(op) == &CCDFileType)

/* Creates a new object, steals the reference */
PyObject *CCDFile_FromHandle(CmpackCcdFile *f);

#endif
