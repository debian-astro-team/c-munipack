#ifndef CMPACK_TRACKCURVE_CLASS
#define CMPACK_TRACKCURVE_CLASS

#include "tableclass.h"

typedef struct {
Table t;
} TrackCurve;

extern PyTypeObject TrackCurveType;

#define TrackCurve_Check(op) \
(op!=NULL && PyObject_TypeCheck(op, &TrackCurveType))
#define TrackCurve_CheckExact(op) (Py_TYPE(op) == &TrackCurveType)

/* Creates a new object, steals the reference */
PyObject *TrackCurve_FromHandle(CmpackTable *f);

#endif
