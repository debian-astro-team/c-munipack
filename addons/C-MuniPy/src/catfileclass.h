#ifndef CMPACK_CATFILE_CLASS
#define CMPACK_CATFILE_CLASS

typedef struct {
    PyObject_HEAD
	CmpackCatFile *f;
} CatFile;

extern PyTypeObject CatFileType;

#define CatFile_Check(op) \
                 (op!=NULL && PyObject_TypeCheck(op, &CatFileType))
#define CatFile_CheckExact(op) (Py_TYPE(op) == &CatFileType)

/* Creates a new object, steals the reference */
PyObject *CatFile_FromHandle(CmpackCatFile *f);

#endif
