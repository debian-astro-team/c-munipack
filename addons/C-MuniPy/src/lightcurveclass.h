#ifndef CMPACK_LIGHTCURVE_CLASS
#define CMPACK_LIGHTCURVE_CLASS

#include "tableclass.h"

typedef struct {
    Table t;
} LightCurve;

extern PyTypeObject LightCurveType;

#define LightCurve_Check(op) \
                 (op!=NULL && PyObject_TypeCheck(op, &LightCurveType))
#define LightCurve_CheckExact(op) (Py_TYPE(op) == &LightCurveType)

/* Creates a new object, steals the reference */
PyObject *LightCurve_FromHandle(CmpackTable *f);

#endif
