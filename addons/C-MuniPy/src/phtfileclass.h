#ifndef CMPACK_PHTFILE_CLASS
#define CMPACK_PHTFILE_CLASS

typedef struct {
    PyObject_HEAD
	CmpackPhtFile *f;
	int p_valid;
	CmpackPhtInfo p;
	CmpackDateTime date_time;
} PhtFile;

extern PyTypeObject PhtFileType;

#define PhtFile_Check(op) \
                 (op!=NULL && PyObject_TypeCheck(op, &PhtFileType))
#define PhtFile_CheckExact(op) (Py_TYPE(op) == &PhtFileType)

/* Creates a new object, steals the reference */
PyObject *PhtFile_FromHandle(CmpackPhtFile *f);

#endif
