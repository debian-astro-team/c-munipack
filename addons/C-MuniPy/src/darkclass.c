#define PY_SSIZE_T_CLEAN
#include <Python.h>

#include "cmunipack.h"
#include "ccdfileclass.h"

PyObject *cmpack_error(int code);

typedef struct {
    PyObject_HEAD
	CmpackDarkCorr *f;
} Dark;

static PyObject *
Dark_new(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
    Dark *self = (Dark*)type->tp_alloc(type, 0);
	if (self != NULL)
		self->f = NULL;
    return (PyObject *)self;
}

static void
Dark_dealloc(Dark* self)
{
	if (self->f)
		cmpack_dark_destroy(self->f);
    self->ob_base.ob_type->tp_free((PyObject*)self);
}

static int
Dark_init(Dark *self, PyObject *args, PyObject *kwds)
{
	int res;
	double minvalue = 0.0, maxvalue = 65535.0;
	CmpackBorder b;
	PyObject *from, *border = NULL, *scaling = Py_False;

	static char *kwlist[] = {"from", "border", "minvalue", "maxvalue", "scaling", NULL};
    if (!PyArg_ParseTupleAndKeywords(args, kwds, "O!|O!ddO!", kwlist, &CCDFileType, &from, 
		&PyTuple_Type, &border, &minvalue, &maxvalue, &PyBool_Type, &scaling))
        return -1;

	if (!self->f) {
		self->f = cmpack_dark_init();
		if (!self->f) {
			PyErr_SetString(PyExc_MemoryError, "Memory allocation error");
			return -1;
		}
	}
	if (border) {
		if (!PyArg_ParseTuple(border, "iiii", &b.left, &b.top, &b.right, &b.bottom)) 
			return -1;
		cmpack_dark_set_border(self->f, &b);
	}
	cmpack_dark_set_minvalue(self->f, minvalue);
	cmpack_dark_set_maxvalue(self->f, maxvalue);
	cmpack_dark_set_scaling(self->f, scaling == Py_True);
	res = cmpack_dark_rdark(self->f, ((CCDFile*)from)->f);
	if (res!=0) {
		cmpack_error(res);
		return -1;
	}
	return 0;
}

static PyObject *
Dark_border(Dark *self, void *data)
{
	CmpackBorder b;
	cmpack_dark_get_border(self->f, &b);
	return Py_BuildValue("iiii", b.left, b.top, b.right, b.bottom);
}

static PyObject *
Dark_minvalue(Dark *self, void *data)
{
	return PyFloat_FromDouble(cmpack_dark_get_minvalue(self->f));
}

static PyObject *
Dark_maxvalue(Dark *self, void *data)
{
	return PyFloat_FromDouble(cmpack_dark_get_maxvalue(self->f));
}

static PyObject *
Dark_scaling(Dark *self, void *data)
{
	return PyBool_FromLong(cmpack_dark_get_scaling(self->f));
}

static PyObject *
Dark_apply(Dark *self, PyObject *args)
{
	int res;
	PyObject *frame;
	CmpackCcdFile *dst;

	if (!PyArg_ParseTuple(args, "O!", &CCDFileType, &frame)) 
		return NULL;

	dst = cmpack_ccd_new();
	res = cmpack_dark_ex(self->f, ((CCDFile*)frame)->f, dst);
	if (res!=0) {
		cmpack_error(res);
		cmpack_ccd_destroy(dst);
		return NULL;
	}
	return CCDFile_FromHandle(dst);
}

static PyObject *
Dark_modify(Dark *self, PyObject *args)
{
	int res;
	PyObject *frame;

	if (!PyArg_ParseTuple(args, "O!", &CCDFileType, &frame)) 
		return NULL;

	res = cmpack_dark(self->f, ((CCDFile*)frame)->f);
	if (res!=0) 
		return cmpack_error(res);
	Py_RETURN_TRUE;
}

static PyGetSetDef Dark_getset[] = {
	{"border", (getter)Dark_border, NULL, 
	"Image border size in pixels"},
	{"minvalue", (getter)Dark_minvalue, NULL, 
	"Threshold for \"invalid\" pixel values"},
	{"maxvalue", (getter)Dark_maxvalue, NULL, 
	"Threshold for \"overexposed\" pixels"},
	{"scaling", (getter)Dark_scaling, NULL,
	"Set to True in advanced calibration scheme"},
	{NULL}
};

static PyMethodDef Dark_methods[] = {
	{"apply", (PyCFunction)Dark_apply, METH_VARARGS,
	"Execute dark-frame correction, creates a new frame"},
	{"modify", (PyCFunction)Dark_modify, METH_VARARGS,
	"Execute dark-frame correction, modifies content of given frame"},
    {NULL}  /* Sentinel */
};

PyTypeObject DarkType = {
    PyObject_HEAD_INIT(NULL)
    .tp_name = "cmpack.Dark",          /*tp_name*/
    .tp_basicsize = sizeof(Dark),           /*tp_basicsize*/
    .tp_dealloc = (destructor)Dark_dealloc,           /*tp_dealloc*/
    .tp_flags = Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,        /*tp_flags*/
    .tp_doc = "Dark correction",        /* tp_doc */
    .tp_methods = Dark_methods,           /* tp_methods */
    .tp_getset = Dark_getset,            /* tp_getset */
    .tp_init = (initproc)Dark_init,      /* tp_init */
    .tp_new = Dark_new                 /* tp_new */
};
