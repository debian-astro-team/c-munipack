#ifndef CMPACK_MAGDEVCURVE_CLASS
#define CMPACK_MAGDEVCURVE_CLASS

#include "tableclass.h"

typedef struct {
	Table t;
	int comp_star;
} MagDevCurve;

extern PyTypeObject MagDevCurveType;

#define MagDevCurve_Check(op) \
(op!=NULL && PyObject_TypeCheck(op, &MagDevCurveType))
#define MagDevCurve_CheckExact(op) (Py_TYPE(op) == &MagDevCurveType)

/* Creates a new object, steals the reference */
PyObject *MagDevCurve_FromHandle(CmpackTable *f);

#endif
