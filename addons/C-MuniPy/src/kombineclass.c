#define PY_SSIZE_T_CLEAN
#include <Python.h>

#include "cmunipack.h"
#include "ccdfileclass.h"
#include "phtfileclass.h"

PyObject *cmpack_error(int code);
PyObject *err_closed(void);

typedef struct {
    PyObject_HEAD
	CmpackKombine *f;
} Kombine;

static PyObject *
Kombine_new(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
    Kombine *self = (Kombine*)type->tp_alloc(type, 0);
	if (self != NULL)
		self->f = NULL;
    return (PyObject *)self;
}

static void
Kombine_dealloc(Kombine* self)
{
	if (self->f)
		cmpack_kombine_destroy(self->f);
    self->ob_base.ob_type->tp_free((PyObject*)self);
}

static int
Kombine_init(Kombine *self, PyObject *args, PyObject *kwds)
{
	int res, format = CMPACK_BITPIX_AUTO;
	double minvalue = 0.0, maxvalue = 65535.0;
	CmpackBorder b;
	PyObject *output, *border = NULL;

	static char *kwlist[] = {"output", "format", "border", "minvalue", "maxvalue", NULL};
    if (!PyArg_ParseTupleAndKeywords(args, kwds, "O!|iO!dd", kwlist, &CCDFileType, &output, 
		&format, &PyTuple_Type, &border, &minvalue, &maxvalue))
        return -1;

	if (!self->f) {
		self->f = cmpack_kombine_init();
		if (!self->f) {
			PyErr_SetString(PyExc_MemoryError, "Memory allocation error");
			return -1;
		}
	}
	res = cmpack_kombine_open(self->f, ((CCDFile*)output)->f);
	if (res!=0) {
		cmpack_error(res);
		return -1;
	}
	cmpack_kombine_set_bitpix(self->f, format);
	if (border) {
		if (!PyArg_ParseTuple(border, "iiii", &b.left, &b.top, &b.right, &b.bottom)) 
			return -1;
		cmpack_kombine_set_border(self->f, &b);
	}
	cmpack_kombine_set_minvalue(self->f, minvalue);
	cmpack_kombine_set_maxvalue(self->f, maxvalue);
	return 0;
}

static PyObject *
Kombine_flush(Kombine *self, PyObject *args)
{
	if (self->f) {
		int res = cmpack_kombine_close(self->f);
		self->f = NULL;
		if (res!=0)
			return cmpack_error(res);
	}
	Py_RETURN_NONE;
}

static PyObject *
Kombine_border(Kombine *self, void *data)
{
	CmpackBorder b;
	cmpack_kombine_get_border(self->f, &b);
	return Py_BuildValue("iiii", b.left, b.top, b.right, b.bottom);
}

static PyObject *
Kombine_format(Kombine *self, void *data)
{
	return PyLong_FromLong(cmpack_kombine_get_bitpix(self->f));
}

static PyObject *
Kombine_minvalue(Kombine *self, void *data)
{
	return PyFloat_FromDouble(cmpack_kombine_get_minvalue(self->f));
}

static PyObject *
Kombine_maxvalue(Kombine *self, void *data)
{
	return PyFloat_FromDouble(cmpack_kombine_get_maxvalue(self->f));
}

static PyObject *
Kombine_add(Kombine *self, PyObject *param)
{
	int res;
	CCDFile *ccd;
	PhtFile *pht;

	if (!PyArg_ParseTuple(param, "O!O!", &CCDFileType, &ccd, &PhtFileType, &pht))
		return NULL;

	res = cmpack_kombine_read(self->f, ccd->f, pht->f);
	if (res!=0) {
		cmpack_error(res);
		return NULL;
	}
	Py_RETURN_NONE;
}

static PyGetSetDef Kombine_getset[] = {
	{"format", (getter)Kombine_format, NULL, 
	"Image border size in pixels"},
	{"border", (getter)Kombine_border,  NULL, 
	"Image border size in pixels"},
	{"minvalue", (getter)Kombine_minvalue,  NULL, 
	"Threshold for \"invalid\" pixel values"},
	{"maxvalue", (getter)Kombine_maxvalue,  NULL, 
	"Threshold for \"overexposed\" pixels"},
	{NULL}
};

static PyMethodDef Kombine_methods[] = {
	{"flush", (PyCFunction)Kombine_flush, METH_NOARGS,
	"Close file"},
	{"add", (PyCFunction)Kombine_add, METH_VARARGS,
	"Add a frame"},
    {NULL}  /* Sentinel */
};

PyTypeObject KombineType = {
    PyObject_HEAD_INIT(NULL)
    .tp_name = "cmpack.Kombine",          /*tp_name*/
    .tp_basicsize = sizeof(Kombine),           /*tp_basicsize*/
    .tp_dealloc = (destructor)Kombine_dealloc,           /*tp_dealloc*/
    .tp_flags = Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,        /*tp_flags*/
    .tp_doc = "Kombining frames",        /* tp_doc */
    .tp_methods = Kombine_methods,           /* tp_methods */
    .tp_getset = Kombine_getset,            /* tp_getset */
    .tp_init = (initproc)Kombine_init,      /* tp_init */
    .tp_new = Kombine_new                 /* tp_new */
};
