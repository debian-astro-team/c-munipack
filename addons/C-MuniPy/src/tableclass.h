#ifndef CMPACK_TABLE_CLASS
#define CMPACK_TABLE_CLASS

typedef struct {
    PyObject_HEAD
	CmpackTable *f;
} Table;

extern PyTypeObject TableType;

#define Table_Check(op) \
                 (op!=NULL && PyObject_TypeCheck(op, &TableType))
#define Table_CheckExact(op) (Py_TYPE(op) == &TableType)

/* Creates a new object, steals the reference */
PyObject *Table_FromHandle(CmpackTable *f);

#endif
