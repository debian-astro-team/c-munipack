#define PY_SSIZE_T_CLEAN
#include <Python.h>

#include "cmunipack.h"
#include "ccdfileclass.h"

PyObject *cmpack_error(int code);

typedef struct {
    PyObject_HEAD
	CmpackFlatCorr *f;
} Flat;

static PyObject *
Flat_new(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
    Flat *self = (Flat*)type->tp_alloc(type, 0);
	if (self != NULL)
		self->f = NULL;
    return (PyObject *)self;
}

static void
Flat_dealloc(Flat* self)
{
	if (self->f)
		cmpack_flat_destroy(self->f);
    self->ob_base.ob_type->tp_free((PyObject*)self);
}

static int
Flat_init(Flat *self, PyObject *args, PyObject *kwds)
{
	int res;
	double minvalue = 0.0, maxvalue = 65535.0;
	CmpackBorder b;
	PyObject *from, *border = NULL;

	static char *kwlist[] = {"from", "border", "minvalue", "maxvalue", NULL};
    if (!PyArg_ParseTupleAndKeywords(args, kwds, "O!|O!dd", kwlist, &CCDFileType, &from, 
		&PyTuple_Type, &border, &minvalue, &maxvalue))
        return -1;

	if (!self->f) {
		self->f = cmpack_flat_init();
		if (!self->f) {
			PyErr_SetString(PyExc_MemoryError, "Memory allocation error");
			return -1;
		}
	}
	if (border) {
		if (!PyArg_ParseTuple(border, "iiii", &b.left, &b.top, &b.right, &b.bottom)) 
			return -1;
		cmpack_flat_set_border(self->f, &b);
	}
	cmpack_flat_set_minvalue(self->f, minvalue);
	cmpack_flat_set_maxvalue(self->f, maxvalue);
	res = cmpack_flat_rflat(self->f, ((CCDFile*)from)->f);
	if (res!=0) {
		cmpack_error(res);
		return -1;
	}
	return 0;
}

static PyObject *
Flat_border(Flat *self, void *data)
{
	CmpackBorder b;
	cmpack_flat_get_border(self->f, &b);
	return Py_BuildValue("iiii", b.left, b.top, b.right, b.bottom);
}

static PyObject *
Flat_minvalue(Flat *self, void *data)
{
	return PyFloat_FromDouble(cmpack_flat_get_minvalue(self->f));
}

static PyObject *
Flat_maxvalue(Flat *self, void *data)
{
	return PyFloat_FromDouble(cmpack_flat_get_maxvalue(self->f));
}

static PyObject *
Flat_apply(Flat *self, PyObject *args)
{
	int res;
	PyObject *frame;
	CmpackCcdFile *dst;

	if (!PyArg_ParseTuple(args, "O!", &CCDFileType, &frame)) 
		return NULL;

	dst = cmpack_ccd_new();
	res = cmpack_flat_ex(self->f, ((CCDFile*)frame)->f, dst);
	if (res!=0) {
		cmpack_error(res);
		cmpack_ccd_destroy(dst);
		return NULL;
	}
	return CCDFile_FromHandle(dst);
}

static PyObject *
Flat_modify(Flat *self, PyObject *args)
{
	int res;
	PyObject *frame;

	if (!PyArg_ParseTuple(args, "O!", &CCDFileType, &frame)) 
		return NULL;

	res = cmpack_flat(self->f, ((CCDFile*)frame)->f);
	if (res!=0) 
		return cmpack_error(res);
	Py_RETURN_TRUE;
}

static PyGetSetDef Flat_getset[] = {
	{"border", (getter)Flat_border, NULL, 
	"Image border size in pixels"},
	{"minvalue", (getter)Flat_minvalue, NULL, 
	"Threshold for \"invalid\" pixel values"},
	{"maxvalue", (getter)Flat_maxvalue, NULL, 
	"Threshold for \"overexposed\" pixels"},
	{NULL}
};

static PyMethodDef Flat_methods[] = {
	{"apply", (PyCFunction)Flat_apply, METH_VARARGS,
	"Execute flat-frame correction, creates a new frame"},
	{"modify", (PyCFunction)Flat_modify, METH_VARARGS,
	"Execute flat-frame correction, modifies content of given frame"},
    {NULL}  /* Sentinel */
};

PyTypeObject FlatType = {
    PyObject_HEAD_INIT(NULL)
    .tp_name = "cmpack.Flat",          /*tp_name*/
    .tp_basicsize = sizeof(Flat),           /*tp_basicsize*/
    .tp_dealloc = (destructor)Flat_dealloc,           /*tp_dealloc*/
    .tp_flags = Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,        /*tp_flags*/
    .tp_doc = "Flat correction",        /* tp_doc */
    .tp_methods = Flat_methods,           /* tp_methods */
    .tp_getset = Flat_getset,            /* tp_getset */
    .tp_init = (initproc)Flat_init,      /* tp_init */
    .tp_new = Flat_new                 /* tp_new */
};
