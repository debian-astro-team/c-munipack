#ifndef CMPACK_AIRMASSCURVE_CLASS
#define CMPACK_AIRMASSCURVE_CLASS

#include "tableclass.h"

typedef struct {
    Table t;
} AirMassCurve;

extern PyTypeObject AirMassCurveType;

#define AirMassCurve_Check(op) \
(op!=NULL && PyObject_TypeCheck(op, &AirMassCurveType))
#define AirMassCurve_CheckExact(op) (Py_TYPE(op) == &AirMassCurveType)

/* Creates a new object, steals the reference */
PyObject *AirMassCurve_FromHandle(CmpackTable *f);

#endif
