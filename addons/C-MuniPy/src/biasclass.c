#define PY_SSIZE_T_CLEAN
#include <Python.h>

#include "cmunipack.h"
#include "ccdfileclass.h"

PyObject *cmpack_error(int code);

typedef struct {
    PyObject_HEAD
	CmpackBiasCorr *f;
} Bias;

static PyObject *
Bias_new(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
    Bias *self = (Bias*)type->tp_alloc(type, 0);
	if (self != NULL)
		self->f = NULL;
    return (PyObject *)self;
}

static void
Bias_dealloc(Bias* self)
{
	if (self->f)
		cmpack_bias_destroy(self->f);
    self->ob_base.ob_type->tp_free((PyObject*)self);
}

static int
Bias_init(Bias *self, PyObject *args, PyObject *kwds)
{
	int res;
	double minvalue = 0.0, maxvalue = 65535.0;
	CmpackBorder b;
	PyObject *from, *border = NULL;

	static char *kwlist[] = {"from", "border", "minvalue", "maxvalue", NULL};
    if (!PyArg_ParseTupleAndKeywords(args, kwds, "O!|O!dd", kwlist, &CCDFileType, &from, 
		&PyTuple_Type, &border, &minvalue, &maxvalue))
        return -1;

	if (!self->f) {
		self->f = cmpack_bias_init();
		if (!self->f) {
			PyErr_SetString(PyExc_MemoryError, "Memory allocation error");
			return -1;
		}
	}
	if (border) {
		if (!PyArg_ParseTuple(border, "iiii", &b.left, &b.top, &b.right, &b.bottom)) 
			return -1;
		cmpack_bias_set_border(self->f, &b);
	}
	cmpack_bias_set_minvalue(self->f, minvalue);
	cmpack_bias_set_maxvalue(self->f, maxvalue);
	res = cmpack_bias_rbias(self->f, ((CCDFile*)from)->f);
	if (res!=0) {
		cmpack_error(res);
		return -1;
	}
	return 0;
}

static PyObject *
Bias_border(Bias *self, void *data)
{
	CmpackBorder b;
	cmpack_bias_get_border(self->f, &b);
	return Py_BuildValue("iiii", b.left, b.top, b.right, b.bottom);
}

static PyObject *
Bias_minvalue(Bias *self, void *data)
{
	return PyFloat_FromDouble(cmpack_bias_get_minvalue(self->f));
}

static PyObject *
Bias_maxvalue(Bias *self, void *data)
{
	return PyFloat_FromDouble(cmpack_bias_get_maxvalue(self->f));
}

static PyObject *
Bias_apply(Bias *self, PyObject *args)
{
	int res;
	PyObject *frame;
	CmpackCcdFile *dst;

	if (!PyArg_ParseTuple(args, "O!", &CCDFileType, &frame)) 
		return NULL;

	dst = cmpack_ccd_new();
	res = cmpack_bias_ex(self->f, ((CCDFile*)frame)->f, dst);
	if (res!=0) {
		cmpack_error(res);
		cmpack_ccd_destroy(dst);
		return NULL;
	}
	return CCDFile_FromHandle(dst);
}

static PyObject *
Bias_modify(Bias *self, PyObject *args)
{
	int res;
	PyObject *frame;

	if (!PyArg_ParseTuple(args, "O!", &CCDFileType, &frame)) 
		return NULL;

	res = cmpack_bias(self->f, ((CCDFile*)frame)->f);
	if (res!=0) 
		return cmpack_error(res);
	Py_RETURN_TRUE;
}

static PyGetSetDef Bias_getset[] = {
	{"border", (getter)Bias_border, NULL, 
	"Image border size in pixels"},
	{"minvalue", (getter)Bias_minvalue, NULL, 
	"Threshold for \"invalid\" pixel values"},
	{"maxvalue", (getter)Bias_maxvalue, NULL, 
	"Threshold for \"overexposed\" pixels"},
	{NULL}
};

static PyMethodDef Bias_methods[] = {
	{"apply", (PyCFunction)Bias_apply, METH_VARARGS,
	"Execute bias-frame correction, creates a new frame"},
	{"modify", (PyCFunction)Bias_modify, METH_VARARGS,
	"Execute bias-frame correction, modifies content of given frame"},
    {NULL}  /* Sentinel */
};

PyTypeObject BiasType = {
    PyObject_HEAD_INIT(NULL)
    .tp_name = "cmpack.Bias",          /*tp_name*/
    .tp_basicsize = sizeof(Bias),           /*tp_basicsize*/
    .tp_dealloc = (destructor)Bias_dealloc,           /*tp_dealloc*/
    .tp_flags = Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,        /*tp_flags*/
    .tp_doc = "Bias correction",        /* tp_doc */
    .tp_methods = Bias_methods,           /* tp_methods */
    .tp_getset = Bias_getset,            /* tp_getset */
    .tp_init = (initproc)Bias_init,      /* tp_init */
    .tp_new = Bias_new                 /* tp_new */
};
