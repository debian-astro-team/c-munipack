#ifndef CMPACK_FRAMESET_CLASS
#define CMPACK_FRAMESET_CLASS

typedef struct {
    PyObject_HEAD
	CmpackFrameSet *f;
	int p_valid;
	CmpackFrameSetInfo p;
} FrameSet;

extern PyTypeObject FrameSetType;

#define FrameSet_Check(op) \
                 (op!=NULL && PyObject_TypeCheck(op, &FrameSetType))
#define FrameSet_CheckExact(op) (Py_TYPE(op) == &FrameSetType)

/* Creates a new object, steals the reference */
PyObject *FrameSet_FromHandle(CmpackFrameSet *f);

#endif
