#!/usr/bin/env python
#
# Setup script for python interface to the C-Munipack 2.0 library
#
# Usage: python setup.py install
#
NAME = "C-MuniPy"
VERSION = '2.1.32'
DESCRIPTION = "Python interface to C-Munipack 2.0"
AUTHOR = "David Motl"
EMAIL = "dmotl@volny.cz"
HOMEPAGE = "http://c-munipack.sourceforge.net/"
LICENCE = "GNU GPL"
PLATFORMS = "Python 1.5.2 and later."
LONG_DESCRIPTION =  """The software package C-Munipack presents the complete system for reduction of
images carried out by CCD camera, oriented at the observation of variable stars.
This extension module provides a Python interface to the library that allows to
incorporate the library in highly automated tools, like automatic sky surveys."""

SOURCES = [ 
	"src/airmassclass.c", 
	"src/airmasscurveclass.c",
 	"src/apdevcurveclass.c",
 	"src/biasclass.c",
 	"src/catfileclass.c",
 	"src/ccdfileclass.c",
 	"src/cmpackmodule.c", 
 	"src/darkclass.c",
 	"src/flatclass.c",
 	"src/framesetclass.c",
 	"src/helcorrclass.c",
 	"src/kombineclass.c",
 	"src/lightcurveclass.c",
 	"src/magdevcurveclass.c",
 	"src/masterbiasclass.c", 
 	"src/masterdarkclass.c",
 	"src/masterflatclass.c",
 	"src/matchclass.c",
 	"src/photometryclass.c",
 	"src/phtfileclass.c", 
 	"src/tableclass.c", 
 	"src/trackcurveclass.c",
 	]

HEADERS = [ 
	"src/airmasscurveclass.h",
	"src/apdevcurveclass.h",
	"src/catfileclass.h",
	"src/ccdfileclass.h",
	"src/framesetclass.h",
	"src/lightcurveclass.h",
	"src/magdevcurveclass.h",
	"src/phtfileclass.h",
	"src/tableclass.h",
	"src/trackcurveclass.h"
	]
 	
from distutils.core import setup
from distutils.extension import Extension
import commands

def pkgconfig(*packages, **kw):
   flag_map = { '-I': 'include_dirs', '-L': 'library_dirs', '-l': 'libraries' }
   for token in commands.getoutput("pkg-config --libs --cflags %s" % ' '.join(packages)).split():
      if flag_map.has_key(token[:2]):
         kw.setdefault(flag_map.get(token[:2]), []).append(token[2:])
      else:
         kw.setdefault('extra_link_args', []).append(token)
   for k,v in kw.iteritems():
      kw[k] = list(set(v))
   return kw

setup(
    author=AUTHOR, author_email=EMAIL, description=DESCRIPTION, license=LICENCE,
    long_description=LONG_DESCRIPTION, name=NAME, platforms=PLATFORMS, url=HOMEPAGE,
    packages=[""], version=VERSION, ext_modules = [Extension(name="cmpack", sources=SOURCES,
	depends=HEADERS, **pkgconfig('cmunipack-2.0'))])
