#!/usr/bin/env python
import os
import datetime 

def test_read():
	base	= "."
	file	= "CR Cas.xml" 

	with cmpack.CatFile.open(os.path.join(base, file), cmpack.READONLY) as f:
		print("Image size  : " + str(f.image_size))
		print("Date & time : " + str(f.date_time))
		print("Julian date : " + str(f.julian_date))
		print("Exposure    : " + str(f.exposure))
		print("Filter      : " + str(f.filter))
		print("Observer    : %s" % f.observer)
		print("Object      : '%s', %f, %f" % f.object)
		print("Location    : '%s', %f, %f" % f.location)
		print("")
		#
		# Print list of selected stars
		#
		print("Selected stars:")
		print("---------------")
		sel = f.selection
		print("Variable stars:")
		for id in sel[cmpack.VARIABLE_STAR]:
			print("Object #%d: %s" % (id, f.get_star(id)))
		print("")
		print("Comparison stars:")
		for id in sel[cmpack.COMPARISON_STAR]:
			print("Object #%d: %s" % (id, f.get_star(id)))
		print("")
		print("Check stars:")
		for id in sel[cmpack.CHECK_STAR]:
			print("Object #%d: %s" % (id, f.get_star(id)))
		print("")
		#
		# Print tags
		#
		print("")
		print("Tags:")
		print("-----")
		tags = f.tags
		for id in f.tags.keys():
			print("Object #%d:" % id)
			print(tags[id])
			print("")

try:
	import cmpack

	test_read()

except Exception as E:
	print E
 
raw_input()