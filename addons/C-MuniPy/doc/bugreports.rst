.. C-Munipack Python module documentation 

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: bugreports.rst,v 1.1 2015/07/06 08:27:20 dmotl Exp $
   
   
Getting help
============

Which version of the extension module do I use?
-----------------------------------------------

The version() function retrieves the version information from the
module. The following script prints the version string to the standard output::

	import cmpack
	print cmpack.version()[3]

	
Bug reports, new feature requests
---------------------------------

If you find a bug in the software or if you miss some feature, please don't hesitate to 
send me an email. Please include at least the following information to your report:

#) The version of the extension module you use.
#) The version of the Python software.
#) Your operating system.
#) A short script that reproduces the error, if possible.
#) If an unexpected error message is shown, an exact copy of the message.
#) Does this bug occur always, randomly but often or seldom?

Thanks!

David Motl, dmotl@volny.cz
