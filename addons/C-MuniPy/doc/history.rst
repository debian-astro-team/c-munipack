.. C-Munipack Python module documentation 

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: history.rst,v 1.1 2015/07/06 08:27:20 dmotl Exp $
   
   
History
=======

.. glossary::

	Version 1.2.29
		* First public release of the :mod:`cmpack` module
