.. C-Munipack Python module documentation 

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: matchclass.rst,v 1.1 2015/07/06 08:27:20 dmotl Exp $

.. py:currentmodule:: cmpack

:class:`Match` --- Photometry file matching
-------------------------------------------

The :class:`Match` class finds cross-references between photometry files. It is called
as a part of the CCD data reduction process.

.. class:: Match(file, [readstars, [identstars, [clipping, [method, [maxoffset]]]]])   
	
   A constructor that makes a new matching context. It reads a reference set of objects 
   from a :class:`PhtFile` object or :class:`CatFile` object specified as the *file*
   argument. The *readstars* argument specify max. . 
   The default is 10.   
   The *identstars* argument specify the number of stars used for making a 
   polygon. The default is 5.   
   The *clipping* argument can be used to change the . 
   The default is 2.3.   
   The *method* argument can be used to set the preferred matching method, 
   use one of cmpack.MATCH_xxx constants. The default is cmpack.MATCH_STANDARD.   
   The *maxoffset* argument can be used to adjust the maximum allowed offset 
   for "sparse fields" method. The default is 5.

   A :class:`Match` object has the following read-only attributes:   
	
   .. attribute:: readstars
   
      Max. number of stars used for matching
   
   .. attribute:: identstars
   
      Number of stars used for making a polygon
   
   .. attribute:: clipping
   
      Clipping threshold
   
   .. attribute:: method 
   
      Preferred matching method, one of cmpack.MATCH_xxx constants
   
   .. attribute:: maxoffset   
   
      Maximum allowed object position offset for "sparse fields" method

   A :class:`Match` object has the following method:   
	
   .. method:: run(frame)   
   
      Finds cross-references between a reference frame given in the	constructor 
      and the source frame given as the *frame* argument. When the transformation between
      the two frame was found, each object in the source frame is assigned a reference
      identifier which is the same as object's identifier in the reference file. 
      The given photometry file must be writable. The function returns 'None' if
      the transformation was not found, or tuple consisting of the number of objects
      that were successfully matched and offsets in X and Y axis, respectively.   
