.. C-Munipack Python module documentation 

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: airmassclass.rst,v 1.1 2015/07/06 08:27:20 dmotl Exp $

.. py:currentmodule:: cmpack
   
:class:`AirMass` --- Air-mass coefficient computation
-----------------------------------------------------

The :class:`AirMass` class computes a value of air-mass coefficient or apparent altitude
of an object. Object's equatorial coordinates (right ascension and declination) and observer's
geographical coordinates (longitude and latitude) are specified using the constructor.

.. class:: AirMass(object, location)

   Make a new AirMass object.
   The *object* argument should be a 3-tuple. The first
   item is not used and can be a string or None. The second item is object's right ascension 
   in hours. The third item is object's declination in degrees.   
   The *location* argument should be a 3-tuple. The first item is not used and can be a string 
   or None. The second item is observer's longitude in degrees and the third argument is 
   observer's latitude in degrees.

   An :class:`AirMass` object has the following public methods:   
   
   .. method:: airmass(jd)
   
      Compute value of air-mass coefficient for a Julian date specified as *jd* argument. 
      It returns a air-mass coefficient value if the object is above horizon, or None if 
      the object is below horizon.

   .. method:: altitude(jd)
   
      Compute value of apparent altitude for a Julian date specified as *jd* argument.
      It returns altitude in degrees, a negative angle indicates that the object is below horizon.   
