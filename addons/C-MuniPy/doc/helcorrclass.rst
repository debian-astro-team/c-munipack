.. C-Munipack Python module documentation 

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: helcorrclass.rst,v 1.1 2015/07/06 08:27:20 dmotl Exp $

.. py:currentmodule:: cmpack

:class:`HelCorr` --- Air-mass coefficient computation
-----------------------------------------------------

The :class:`HelCorr` class computes a value of heliocentric correction. Object's equatorial 
coordinates (right ascension and declination) are specified using the constructor.

.. class:: HelCorr(object)

   Constructor for the HelCorr class, which creates a new instance that stores object 
   coordinates. The *object* argument should be a 3-tuple. The first item is not used 
   and can be a string or None. The second item is object's right ascension in hours. The third 
   item is object's declination in degrees.

   A :class:`HelCorr` object has the following methods:   
    
   .. method:: hc(jd)   
   
   Compute value of heliocentric correction in days for given Julian date.   

   .. method:: hjd(jd)   
   
   Compute value of heliocentric Julian date for given geocentric Julian date.   
