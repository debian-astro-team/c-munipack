.. C-Munipack Python module documentation 

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: darkclass.rst,v 1.1 2015/07/06 08:27:20 dmotl Exp $

.. py:currentmodule:: cmpack
   
:class:`Dark` --- Dark correction
---------------------------------

The :class:`Dark` class applies a dark correction to a CCD frame. The constructor is
used to create a context that keeps a dark frame and configuration parameters. The
correction is applied by calling :meth:`apply` or :meth:`modify` methods.

.. class:: Dark(from, [border, [minvalue, [maxvalue, [scaling]]]])

   Constructor for the :class:`Dark` class, which stores a dark frame and parameters 
   required to apply the dark correction to a CCD frame.	
   The *from* argument must be a :class:`CCDFile` object and it is a dark frame.
   The *border* argument can be used to exclude a certain number of pixels
   along sides of the frame. Pixels in border area will be set to *minvalue* in the
   output file. The value should be a tuple of four integer numbers which specify
   the size of the left, top, right and bottom border in that order. If not used,
   the border is not applied.
   The *minvalue* argument can be used to specify a value that
   indicates "bad" pixels. The default is 0.
   The *maxvalue* argument can be used to specify a value that
   indicates "overexposed" pixels. The default is 65535.
   The *scaling* argument can be set to True to enable the dark-frame scaling.
   If it is enabled, the exposure duration of the input CCD frame and the exposure 
   duration of the correction frame are compared and the correction frame is scaled 
   to match the duration of the corrected frame. The dark-frame must be scalable.
	
   An :class:`Dark` object has the following read-only attributes:
   
   .. attribute: border
      
      Tuple of four integer numbers which specify the size of the left, 
      top, right and bottom border in that order
   
   .. attribute:: minvalue
   
      Pixel value that indicates "bad" pixels.
      
   .. attribute:: maxvalue
   
      Pixel value that indicates "overexposed" pixels.
      
   .. attribute:: scaling
   
      True if dark-frame scaling is enabled.

   An :class:`Dark` object has the following methods:
	
   .. method:: apply(source)
   
      Apply dark correction to a :class:`CCDFile` object given in the 'source' argument.
      The output frame is returned as a new :class:`CCDFile` object.
	
   .. method:: modify(frame)
   
      Apply dark correction to a :class:`CCDFile` object given in the 'frame' argument.
      Unlike the apply method, the output data are written to the given object. The 
      :class:`CCDFile` object must be writable. The function returns True on success.
