.. C-Munipack Python module documentation 

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: framesetclass.rst,v 1.1 2015/07/06 08:27:20 dmotl Exp $

.. py:currentmodule:: cmpack
   
:class:`FrameSet` --- A set of frames
-------------------------------------

The :class:`FrameSet` class is designed to load and store measurement data from a set of photometry 
files. A frame set is given to methods that produce various outputs, i.e. light curve.

.. class:: FrameSet(stars, apertures)

   A constructor which creates a new empty frame set. 
   The *stars* argument is a list of (reference) identifiers of stars which shall
   be loaded from photometry files. The *apertures* argument is a list of aperture identifiers for 
   which the data shall be loaded from photometry files. Both these parameters are used to speed up 
   the processing and reduce memory consumption, because in many cases it is not neccesary to hold 
   data for all objects and apertures in the memory.   
   
   A :class:`FrameSet` object has the following attributes related to the whole frame set:

   .. attribute:: object
   
      A tuple consisting of object's designation, right ascension in hours
      and declination in degrees.   

   .. attribute:: location   
   
      A tuple consisting of designation of observer's location, observer's
      longitude in degrees and latitude in degrees.   

   .. attribute:: frames   
   
      A list of all frame identifiers. [read-only]   
                             
   .. attribute:: stars   
   
      A list of all object identifiers. [read-only]   

   .. attribute:: apertures   
   
      A list of all aperture identifiers. [read-only]   

   The following attributes related to the active frame. They return None if no frame is active.   

   .. attribute:: frame_id   
   
      Identifier of the current frame.   

   .. attribute:: date_time   
   
      Date and time of observation. The DateTime instance.   

   .. attribute:: julian_date   
   
      Julian date of observation.   

   .. attribute:: filter   
   
      Color filter designation   

   .. attribute:: offset   
   
      A tuple consisting of frame offset in x and y coordinates respectively
      with respect to the reference frame.   

      
   A :class:`FrameSet` object has the following methods related to the whole frame set:
   
   .. staticmethod:: import(filename)   
   
      Load a read-all file specified by its path as *filename* argument. It returns a new 
      :class:`FrameSet` object.      

   .. staticmethod:: test(filename)   
   
      The method returns True if the file, specified as its argument, is a read-all file.
      If the specified file does not exist or it was not recognized as a photometry file,
      the False value is returned.   
	
   .. method:: export(filename)   
   
      The method writes a frame set to a read-all file. Please note, that the
      read-all file cannot keep exact copy of the frame set, so if you save the frame
      set to a file and load it back, you won't get the same information.   

   .. method:: clear()   
   
      The method delete all frames from the frame set.   

   .. method:: rewind()   
   
      The method makes a first frame in the set active. It returns True on
      success or False if the frame set is empty.   

   .. method:: next()   
   
      The method makes active a frame after the active frame. It returns True
      on success or False if the active frame was the last one.   

   .. method:: go_to(id)   
   
      The method finds a frame by its identifier, specified as an argument.
      On success, it makes the frame active and returns True. Otherwise, no frame
      will be active and the method returns False.   

   .. method:: append(id, file)   
   
      The method appends a new frame into the frame set. The *id* argument 
      can be a non-negative integer number which will be used to identify the frame,
      the *file* argument is a PhtFile object which the data are read from. On success,
      the method returns True and the frame is made active.   

   .. method:: light_curve(variable, comparison, aperture)   
   
      Create a light curve. The method returns a list of tuples, each tuple corresponds
      to one frame. Tuples consist of Julian date of observation, differential magnitude 
      of the variable star with respect to the comparison star and error estimation of the 
      magnitude.   
      The *variable* argument specify an indentification number of the variable star.   
      The *comparison* argument specify an indentification number of the comparison star.   
      The *aperture* argument specify identification number of the aperture   

   .. method:: light_curve2(variable, comparison, aperture)   
   
      The same as :meth:`light_curve`, but returns a dictionary where the keys are
      frame identifiers and values are tuples described above.   

   .. method:: track_list()   
   
      Create a list of frame offsets. The methods returns a list of tuples, each tuple corresponds
      to one frame. Tuples consist of Julian date of observation and offset in X and Y axis.   

   .. method:: track_list2()   
   
      The same as :meth:`track_list`, but returns a dictionary where the keys are
      frame identifiers and values are tuples described above.    

   .. method:: magdev_curve(aperture, [clipping[, comparison]])   
   
      The method returns a tuple. The first item is a identifier of the comparison
      star. The second item is a list of tuples, each item corresponds to an object. 
      Tuples consist of object identifier, its mean brightness and sample deviation.   

   The following methods related to the active frame:   

   .. method:: delete()   
   
      The method removes the current frame from the frame set. A frame which follows
      the removed frame (if any) is made active.     

   .. method:: get_mag(star, aperture)   
   
      The method takes a star identifier and an aperture identifier as its 
      arguments and it returns a tuple that consists of magnitude and error estimation
      from the current frame. 
      If magnitude or error is undefined, the corresponding item in the tuple is None.
      If the object or the aperture is undefined or no frame is active, the function returns None. If the
      object and aperture exists, but either the magnitude or the error estimation
      is not known, the function return a tuple, but corresponding item is None.   

   .. method:: get_all(aperture)   
   
      The method takes an aperture identifier as its arguments and it returns 
      a dictionary. The keys are star identifiers, the values are tuples that consists
      of magnitudes and error estimations. If the aperture does not exist or no frame 
      is active, it returns None. If either the magnitude or the error estimation	is not 
      known, the corresponding item in the tuple is None.   
