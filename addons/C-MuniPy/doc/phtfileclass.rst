.. C-Munipack Python module documentation 

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: phtfileclass.rst,v 1.1 2015/07/06 08:27:20 dmotl Exp $

.. py:currentmodule:: cmpack

:class:`PhtFile` --- Photometry file
------------------------------------
   
The :class:`PhtFile` class provides an interface for making and reading photometry
files. A photometry file stores a result of the aperture photometry and the matching 
process. It stores a set of stars, their positions and measurement data.

.. note::

   The :class:`PhtFile` class is compatible with the 'with' statement.   

.. class:: PhtFile()   

   A constructor for a new empty photometry file stored in the memory.
       	
   A new :class:`PhtFile` object is often created by the following static method:
	
   .. staticmethod:: open(filename, mode)   
    
      Create a new :class:`PhtFile` object attached to a file. The *filename* argument is a 
      path of the file. The *mode* argument specify an access mode, it must be one of the
      constants: cmpack.READONLY, cmpack.READWRITE, cmpack.CREATE.

   .. staticmethod:: test(filename)
   
   Check if a file is a photometry file. The function inspects a content of a file specified
   by its path as the *filename* argument and returns True if the file looks like a photometry 
   file.
   
   A :class:`PhtFile` object has the following attributes:
   
   .. attribute:: image_size   
   
      A tuple consisting of width and height of the image in pixels   

   .. attribute:: date_time   
   
      Date and time of observation. The DateTime instance.   

   .. attribute:: julian_date   
   
      Julian date of observation.   

   .. attribute:: exposure   
   
      Exposure duration in seconds   
	
   .. attribute:: filter   
   
      Color filter designation   

   .. attribute:: object   
   
      A tuple consisting of object's designation, right ascension in hours
      and declination in degrees.   

   .. attribute:: location   
   
      A tuple consisting of designation of observer's location, observer's
      longitude in degrees and latitude in degrees.   

   .. attribute:: offset   
   
      A tuple consisting of frame offset in x and y coordinates respectively
      with respect to the reference frame.   

   .. attribute:: apertures   
   
      A list of all aperture identifiers. [read-only]   

   .. attribute:: stars   
   
      A list of all star identifiers. [read-only]   

   .. attribute:: ref_stars   
   
      A list of reference identifiers of matched stars. [read-only]   

   .. attribute:: ref_table   
   
      A dictionary where keys are reference identifiers and values are
      object identifiers. [read-only]   

   A :class:`PhtFile` object has the following methods:   
	
   .. method:: close()   
   
      The method updates the file (if neccessary) and clears the memory.
      After this call, the data are not accessible any more.   

   .. method:: save(filename)   
   
      The method writes a copy of the catalogue file and writes its content
      to the specified file.   

   .. method:: get_aperture(id)   
   
      The method takes a aperture identifier as its argument and it returns 
      the radius of the aperture.   

   .. method:: get_star(id)   
   
      The method takes a star identifier as its argument and it returns a 
      tuple. The first two items are object's X and Y coordinates, the third item
      is either a reference identifier of the object on None, if the object was 
      not matched.   

   .. method:: get_mag(star, aperture)   
   
      The method takes a star identifier and an aperture identifier as its 
      arguments and it returns a tuple that consists of magnitude and error estimation.
      If magnitude or error is undefined, the corresponding item in the tuple is None.
      If the object or the aperture is undefined, the function returns None. If the
      object and aperture exists, but either the magnitude or the error estimation
      is not known, the function return a tuple, but corresponding item is None.   

   .. method:: get_all(aperture)   
   
      The method takes an aperture identifier as its arguments and it returns 
      a dictionary. The keys are star identifiers, the values are tuples that consists
      of magnitudes and error estimations. If either the magnitude or the error estimation
      is not known, the corresponding item in the tuple is None.   
