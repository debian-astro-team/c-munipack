.. C-Munipack Python module documentation 

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.
   
   $Id: examples.rst,v 1.1 2015/07/06 08:27:20 dmotl Exp $
   

Examples
========

This chapter contains a pieces of Python code that shows how to use the :mod:`cmpack` module. 

Example 1 --- Basic reduction and making light curve
----------------------------------------------------

The following sample code takes a set of CCD images and applies flat-correction, photometry and matching to them.
Resulting photometry files are put into a frame set and a light curve in printed to the standard output. ::

	#! /usr/bin/env python
	import os
	import cmpack

	base		= "D:/Samples"					# Sample data base dir
	datadir		= "data"						# Source files, relative to the base dir
	darkfile    = "dark/dark.fts"				# Dark frame, relative to the base dir
	flatfile	= "flat/flatc.st7"				# Flat frame, relative to the base dir
	aperture	= 1								# Aperture identifier
	var			= 7								# Variable star, Object identifier
	comp		= 5								# Comparison star, Object identifier

	#
	# List of source files
	#	
	srcdir = os.path.join(base, datadir)
	flist = os.listdir(srcdir)
	flist.sort()
	
	#
	# Prologue
	#
	frames = []
	d = cmpack.Dark(cmpack.CCDFile.open(os.path.join(base, darkfile), cmpack.READONLY))
	f = cmpack.Flat(cmpack.CCDFile.open(os.path.join(base, flatfile), cmpack.READONLY))
	p = cmpack.Photometry()
	m = None
	s = cmpack.FrameSet([var, comp], [aperture])
	frame_id = 1
	tmpdir = os.path.join(base, "tmp")
	
	#
	# Calibration, photometry and matching
	#
	for path in flist:
		print path
		with cmpack.CCDFile.open(os.path.join(srcdir, path), cmpack.READONLY) as src:
			dst = f.apply(d.apply(src))
			dst.save(os.path.join(tmpdir, "frame%d.fts" % frame_id))
			pht = p.run(dst)
			print("%d stars found." % len(pht.stars))
			pht.save(os.path.join(tmpdir, "frame%d.pht" % frame_id))
			if m == None:
				m = cmpack.Match(pht)
			res = m.run(pht)
			if res:
				print ("%d stars matched." % res[0])
				s.append(frame_id, pht)
			else:
				print "Not matched.";
		frame_id = frame_id + 1

	#
	# Make and print a light curve
	#
	print s.light_curve(1, var, comp, check)

Example 2 --- Making a master-dark frame
----------------------------------------

The following sample code takes a set of raw dark frames and creates a master dark frame. ::

	#! /usr/bin/env python
	import os
	import cmpack

	base		= "D:/Samples"
	darkdir	    = "dark/raw"

	srcdir = os.path.join(base, flatdir)
	outdir = base

	flist = os.listdir(srcdir)
	flist.sort()
	with cmpack.CCDFile.open(os.path.join(outdir, "masterdark.fts"), cmpack.CREATE) as out:
		mf = cmpack.MasterDark(out)
		for path in flist:
			print path
			with cmpack.CCDFile.open(os.path.join(srcdir, path), cmpack.READONLY) as src:
				mf.add(src)
		mf.flush()
	
Example 3 --- Making a master-flat frame
----------------------------------------
	
The following sample code takes a set of raw flat frames and creates a master flat frame. ::

	#! /usr/bin/env python
	import os
	import cmpack

	base		= "D:/Samples"
	flatdir	    = "flat/raw"

	srcdir = os.path.join(base, flatdir)
	outdir = base

	flist = os.listdir(srcdir)
	flist.sort()
	with cmpack.CCDFile.open(os.path.join(outdir, "masterflat.fts"), cmpack.CREATE) as out:
		mf = cmpack.MasterFlat(out)
		for path in flist:
			print path
			with cmpack.CCDFile.open(os.path.join(srcdir, path), cmpack.READONLY) as src:
				mf.add(src)
		mf.flush()
	
	
Example 4 --- A light curve with heliocentric JD
------------------------------------------------

The following sample code takes a set of CCD images and applies flat-correction, photometry and matching to them.
Resulting photometry files are put into a frame set and a light curve is printed to the standard output. ::

	#! /usr/bin/env python
	import os
	import cmpack

	base		= "D:/Samples"					# Sample data base dir
	datadir		= "data"						# Source files, relative to the base dir
	flatfile	= "flat/flatc.st7"				# Temporary files, Relative to the base dir
	aperture	= 1								# Aperture identifier
	var			= [7]							# Variable star, object identifier
	comp		= [5]							# Comparison star, Object identifier
	check		= [11, 12]						# Check stars, object identifiers
	object      = ("CR Cas", 23.0811, 59.5658)	# Object's designation, R.A. and declination

	#
	# List of stars
	#	
	stars = []
	stars.extend(var)
	stars.extend(comp)
	stars.extend(check)
	
	#
	# List of source files
	#	
	srcdir = os.path.join(base, datadir)
	flist = os.listdir(srcdir)
	flist.sort()
	frames = []
	p = cmpack.Photometry()
	m = None
	s = cmpack.FrameSet(stars, range(1, 13))

	#
	# Calibration, photometry and matching
	#
	frame_id = 1
	tmpdir = os.path.join(base, "tmp")
	with cmpack.CCDFile.open(os.path.join(base, flatfile), cmpack.READONLY) as ff:
		f = cmpack.Flat(ff)
		for path in flist:
			print path
			with cmpack.CCDFile.open(os.path.join(srcdir, path), cmpack.READONLY) as src:
				dst = f.apply(src)
				dst.save(os.path.join(tmpdir, "frame%d.fts" % frame_id))
				pht = p.run(dst)
				print("%d stars found." % len(pht.stars))
				pht.save(os.path.join(tmpdir, "frame%d.pht" % frame_id))
				if m == None:
					m = cmpack.Match(pht)
				res = m.run(pht)
				if res:
					print ("%d stars matched." % res[0])
					s.append(frame_id, pht)
				else:
					print "Not matched.";
			frame_id = frame_id + 1

	#
	# Make and print a light curve
	#
	c = cmpack.LightCurve(s, aperture, var, comp, check, jdtype=cmpack.JD_HELIOCENTRIC, object=object)
	print c.get_all(["JDHEL", "V-C"])
	
Example 5 --- A light curve with air-mass coefficients
------------------------------------------------------

The following sample code takes a set of CCD images and applies flat-correction, photometry and matching to them.
Resulting photometry files are put into a frame set and a light curve is printed to the standard output. ::

	#! /usr/bin/env python
	import os
	import cmpack

	base		= "D:/Samples"					# Sample data base dir
	datadir		= "data"						# Source files, relative to the base dir
	flatfile	= "flat/flatc.st7"				# Temporary files, Relative to the base dir
	aperture	= 1								# Aperture identifier
	var			= [7]							# Variable star, object identifier
	comp		= [5]							# Comparison star, Object identifier
	check		= [11, 12]						# Check stars, object identifiers
	object      = ("CR Cas", 23.0811, 59.5658)	# Object's designation, R.A. and declination
	location    = ("Vyskov", 17.0225, 49.2836)	# Observer's location, longitude and latitude

	#
	# List of stars
	#	
	stars = []
	stars.extend(var)
	stars.extend(comp)
	stars.extend(check)
	
	#
	# List of source files
	#	
	srcdir = os.path.join(base, datadir)
	flist = os.listdir(srcdir)
	flist.sort()
	frames = []
	p = cmpack.Photometry()
	m = None
	s = cmpack.FrameSet(stars, range(1, 13))

	#
	# Calibration, photometry and matching
	#
	frame_id = 1
	tmpdir = os.path.join(base, "tmp")
	with cmpack.CCDFile.open(os.path.join(base, flatfile), cmpack.READONLY) as ff:
		f = cmpack.Flat(ff)
		for path in flist:
			print path
			with cmpack.CCDFile.open(os.path.join(srcdir, path), cmpack.READONLY) as src:
				dst = f.apply(src)
				dst.save(os.path.join(tmpdir, "frame%d.fts" % frame_id))
				pht = p.run(dst)
				print("%d stars found." % len(pht.stars))
				pht.save(os.path.join(tmpdir, "frame%d.pht" % frame_id))
				if m == None:
					m = cmpack.Match(pht)
				res = m.run(pht)
				if res:
					print ("%d stars matched." % res[0])
					s.append(frame_id, pht)
				else:
					print "Not matched.";
			frame_id = frame_id + 1

	#
	# Make and print a light curve
	#
	c = cmpack.LightCurve(s, aperture, var, comp, check, airmass=True, object=object, observer=location)
	print c.get_all(["JD", "V-C", "AIRMASS"])

Example 6 --- Make a track curve
--------------------------------

The following sample code takes a set of CCD images and applies flat-correction, photometry and matching to them.
Resulting photometry files are put into a frame set and a table of frame offsets is printed to the standard output. ::

	#! /usr/bin/env python
	import os
	import cmpack

	base		= "D:/Samples"					# Sample data base dir
	datadir		= "data"						# Source files, relative to the base dir
	flatfile	= "flat/flatc.st7"				# Temporary files, Relative to the base dir

	#
	# List of source files
	#	
	srcdir = os.path.join(base, datadir)
	flist = os.listdir(srcdir)
	flist.sort()
	frames = []
	p = cmpack.Photometry()
	m = None
	s = cmpack.FrameSet(stars, range(1, 13))

	#
	# Calibration, photometry and matching
	#
	frame_id = 1
	tmpdir = os.path.join(base, "tmp")
	with cmpack.CCDFile.open(os.path.join(base, flatfile), cmpack.READONLY) as ff:
		f = cmpack.Flat(ff)
		for path in flist:
			print path
			with cmpack.CCDFile.open(os.path.join(srcdir, path), cmpack.READONLY) as src:
				dst = f.apply(src)
				dst.save(os.path.join(tmpdir, "frame%d.fts" % frame_id))
				pht = p.run(dst)
				print("%d stars found." % len(pht.stars))
				pht.save(os.path.join(tmpdir, "frame%d.pht" % frame_id))
				if m == None:
					m = cmpack.Match(pht)
				res = m.run(pht)
				if res:
					print ("%d stars matched." % res[0])
					s.append(frame_id, pht)
				else:
					print "Not matched.";
			frame_id = frame_id + 1

	#
	# Make and print a track curve
	#
	c = cmpack.TrackCurve(s)
	print c.get_all(c.columns)
