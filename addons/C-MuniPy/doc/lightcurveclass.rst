.. C-Munipack Python module documentation 

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: lightcurveclass.rst,v 1.1 2015/07/06 08:27:20 dmotl Exp $

.. py:currentmodule:: cmpack

:class:`LightCurve` --- Light curve
-----------------------------------

The :class:`LightCurve` class creates a light curve from a frame set. 

This class derives all methods of :class:`Table`.

.. class:: LightCurve(frameset, aperture, var_list, comp_list, check_list, [magnitudes, [altitude, [airmass, [helcorr, [jdtype, [frameids, [object, [location]]]]]]]])
   	
   A constructor that makes a light curve from a frame set.
   The 'framset' argument is a frame set that contains measurement data for 
   specified apertures and objects.   
   The *aperture* argument is an aperture identifier.   
   The *var_list* argument is a list of object identifiers, list of variable stars.   
   The *comp_lis* argument is a list of object identifiers, list of comparison stars.   
   The *check_list* argument is a list of object identifiers, list of check stars.   
   The *magnitudes* argument specify what kind of magnitudes the curve will contain, see
   cmpack.LCURVE_xxx constants for possible values, the default is cmpack.LCURVE_DIFFERENTIAL.   
   The *altitude* argument can be set to True to include object's aparent altitude in
   degrees. The default is False. If it's True, the object coordinates and location must be 
   specified.    
   The *airmass* argument can be set to True to include object's air-mass coefficient. 
   The default is False. If it's True, the object coordinates and location must be specified.    
   The *helcorr* argument can be set to True to include value of heliocentric correction. 
   The default is False. If it's True, the object coordinates must be specified.    
   The *jdtype* argument specify what kind of julian date shall be included in the
   table, see cmpack.JD_xxx constants for possible values, the default is cmpack.JD_GEOCENTRIC.
   If it's nonzero, the object coordinates must be specified.    
   The *frameids* argument can be set to True to include frame identifiers. The default
   value is False.    
   The *object* argument is a tuple consisting of object's designation (can be None),
   right ascension in hours and declination in degrees.    
   The *location* argument is a tuple consisting of location's designation (can be None),
   longitude and latitude, both in degrees.    

   A :class:`LightCurve` object has the following attributes, in addition to :class:`Table`'s attributes:

   .. attribute:: frame_id   
     
      Frame identifier of current frame, returns None if the table does not include
      frame identifiers.   

   .. attribute:: aperture   
   
      Aperture identifier.   

   .. attribute:: filter   
   
      Color filter designation   

   A :class:`LightCurve` object has the following methods, in addition to :class:`Table`'s methods:   
	
   .. method:: copy()   
   
      Make a deep copy of the table and returns a new LightCurve object.   
	
   .. method:: go_to(frame)   
   
      The method finds a frame by its identifier, specified as an argument.
      On success, it makes the frame active and returns True. Otherwise, no frame
      will be active and the method returns False.   

   .. method:: get_mag(name)   
   
      The method takes a name of a column, the column contain a magnitude
      and returns a tuple consisting of magnitude and error estimation from an
      active frame. If no frame is active of the column does not exists, it returns None.    
