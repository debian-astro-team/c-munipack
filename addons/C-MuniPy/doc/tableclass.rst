.. C-Munipack Python module documentation 

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: tableclass.rst,v 1.1 2015/07/06 08:27:20 dmotl Exp $

.. py:currentmodule:: cmpack

:class:`Table` --- A tabular data
---------------------------------
   
The :class:`Table` object is a storage for tabular data. There are also specialized 
classes derived from the :class:`Table` class:

* :class:`LightCurve`
* :class:`TrackCurve`
* :class:`AirMassCurve`
* :class:`MagDevCurve`
* :class:`ApDevCurve`

.. class:: Table()

   A constructor that makes a new table. It creates a new empty table stored in the memory.

   A :class:`Table` object can be also created from a file:
    
   .. staticmethod load(filename)   
   
      Load a table from a file. The program tries to recognize the type of the table by inspecting 
      the name of columns and if it fits into one of the specialized classes from the list above, 
      it returns an object of that class. For unrecognized types, a :class:`Table` object is returned.
    
   .. staticmethod test(filename)   
   
      Check if a file is a table. The function inspects a content of a file specified
      by its path as the *filename* argument and returns True if the file looks like a table      
      
   A :class:`Table` object has the following attributes:

   .. attribute:: row_count   
   
      Number of rows   

   .. attribute:: col_count   
   
      Number of columns.   

   .. attribute:: columns   
   
      List of column names   

   A :class:`Table` object has the following methods:

   .. method:: save(filename)   
   
      Write a copy of the table to the specified file. The *filename* argument is a 
      path of the file.

   .. method:: rewind()   
   
      Make a first row in the table active. It returns True on success or False if the 
      table is empty.   

   .. method:: next()   
   
      Make active a row after the active row. It returns True on success or False if 
      the active row was the last one.   

   .. method:: get_col(name)   
   
      Take a name of a column from the *name* argument and return a dictionary of column 
      atributes. If the column does not exist, None is returned.

   .. method:: get_all(columns)   
   
      Take a list of column names from the *columns* argument and return a list of tuples.
      Each list item corresponds to a row, and the tuples have values from the columns 
      listed in the parameter.   

   .. method:: Table.get_val(name)
   
      Take a column name from the *name* argument and return a value stored in the table
      on active row for specified column. On failure, None is returned

   .. method:: delete()   
   
      The method removes the current row from the table. A row which follows the removed row 
      (if any) is made active.     
