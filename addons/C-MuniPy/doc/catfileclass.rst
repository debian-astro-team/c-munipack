.. C-Munipack Python module documentation 

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: catfileclass.rst,v 1.1 2015/07/06 08:27:20 dmotl Exp $

.. py:currentmodule:: cmpack
   
:class:`CatFile` --- Catalogue file
-----------------------------------

The :class:`CatFile` class provides an interface for making and reading catalogue
files. A catalog file is used in the Matching process. It stores a set of stars, a star
can be assigned one of roles: variable star, comparison star or check stars.

.. note::
   
   The :class:`CatFile` class is compatible with the 'with' statement.

.. class:: CatFile()

   A constructor for the :class:`CatFile` class. It creates a new empty catalogue file 
   stored in the memory.
     	
   A new :class:`CatFile` instance is often created by the following static method:
	
   .. staticmethod:: open(filename, mode)
   
      Create a new :class:`CatFile` object attached to a file. The *filename* argument is a 
      path of the file. The *mode* argument specify an access mode, it must be one of the
      constants: cmpack.READONLY, cmpack.READWRITE, cmpack.CREATE.
	
   .. staticmethod:: test(filename)
      
      Check if a file is a catalogue file. The function inspects a content of a file specified
      by its path as the *filename* argument and returns True if the file looks like a catalogue file.
   
   A :class:`CatFile` object has the following attributes:

   .. attribute:: image_size
   
      A tuple consisting of width and height of the image in pixels

   .. attribute:: date_time
   
      Date and time of observation. The :class:`DateTime` instance.

   .. attribute:: julian_date
   
      Julian date of observation.

   .. attribute:: exposure
   
      Exposure duration in seconds
	
   .. attribute:: filter
   
      Color filter designation

   .. attribute:: observer
   
      Observer's name

   .. attribute:: object
   
      A tuple consisting of object's designation, right ascension in hours
      and declination in degrees.

   .. attribute:: location
   
      A tuple consisting of designation of observer's location, observer's
      longitude in degrees and latitude in degrees.

   .. attribute:: stars
   
      A list of all star identifiers. [read-only]

   .. attribute:: selection
   
      A dictionary of selected stars. Keys are type identifiers (see constants cmpack.VARIABLE_STAR, 
      cmpack.COMPARISON_STAR, cmpack.CHECK_STAR) and values are lists of object identifiers. [read-only]
      
   .. attribute:: tags
   
      A dictionary of object tags. Keys are object identifiers, values are strings. [read-only]
      
   A :class:`CatFile` object has the following methods:
	
   .. method:: close()
   
     The method updates the file (if neccessary) and clears the memory.
     After this call, the data are not accessible any more.

   .. method:: save(filename)
     
      The method writes a copy of the catalogue file and writes its content
      to the specified file.

   .. method:: get_star(id)
   
      The method takes a star identifier as *id* argument and it returns a 
      tuple. The first two items are object's X and Y coordinates, the third item
      is either object's magnitude or None to indicate unknown or invalid magnitude.

   .. method:: set_star(id, x, y[, mag=None])
   
      The method updates a record for a star specified by its identifier
      given in the *id* argument. The next two arguments are mandatory and they
      specify object's X and Y coordinates. The fourth argument is either object's
      magnitude or None to indicate unknown or invalid magnitude.   
    
   .. method:: get_selection(type)
   
      The method returns a list of identifiers of object, that are
      of specified type. The *type* argument should be one of constants:
      cmpack.VARIABLE_STAR, cmpack.COMPARISON_STAR, cmpack.CHECK_STAR

   .. method:: clear_selection()
   
      The method clear type indicator for all objects. After this call,
      no star will be selected as a variable, comparison or check star.

   .. method:: select(id, type)
   
      The method updates type of a star specified by its identifier given
      in the *id* argument. The *type* argument specify a new type, it should be
      one of the constants: cmpack.VARIABLE_STAR, cmpack.COMPARISON_STAR, 
      cmpack.CHECK_STAR

   .. method:: unselect(id)
   
      The method removes a star specified by its identifier given in the *id*
      argument from lists of selected star.

   .. method:: clear_tags()
   
      The method removes all defined tags.

   .. method:: update_tag(id, tag)
   
      The method adds or updates a tag for an object. If the object, specified 
      by its identifier given in the *id* argument, does not have any tag, a new 
      tag with value specified as the *tag* argument is created. If the object
      has got a tag, its value is updated.

   .. method:: remove_tag(id)
   
      The method removes a tag from an object specified by its identifier 
      given in the *id* argument. If the object does not exist or if it does not
      have any tag, the method does nothing.
