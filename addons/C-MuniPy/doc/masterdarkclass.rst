.. C-Munipack Python module documentation 

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: masterdarkclass.rst,v 1.1 2015/07/06 08:27:20 dmotl Exp $

.. py:currentmodule:: cmpack

:class:`MasterDark` --- Making master-dark frame
------------------------------------------------

The :class:`MasterDark` merges a set of raw dark frames into one frame, called a master dark 
frame. 

.. class:: MasterDark(output, [format, [border, [minvalue, [maxvalue, [scalable]]]]])

   A constructor for a new master-dark frame.   
   The *output* argument is a CCDFile object that shall receive an output
   data, it must be opened for writing.
   The *format* argument can be used to explicitly specify which image
   data format shall be used to represent the output data. By default, the
   image data format in which the source frames are stored is used.
   The *border* argument can be used to exclude a certain number of pixels
   along sides of the frame. Pixels in border area will be set to 'minvalue' in the
   output file. The value should be a tuple of four integer numbers which specify
   the size of the left, top, right and bottom border in that order. If not used,
   the border is not applied.
   The *minvalue* argument can be used to specify a value that
   indicates "bad" pixels. The default is 0.
   The *maxvalue* argument can be used to specify a value that
   indicates "overexposed" pixels. The default is 65535.
   When the *scalable* argument is set to True, a scalable dark frame
   will be created. The scalable dark frame is used in the advanced calibration 
   scheme. The default is False.
	
   A :class:`MasterDark` object has the following read-only attributes:
	
   .. attribute:: format
   
      Image data format shall be used to represent the output data
   
   .. attribute: border
      
      Tuple of four integer numbers which specify the size of the left, 
      top, right and bottom border in that order
   
   .. attribute:: minvalue
   
      Pixel value that indicates "bad" pixels.
      
   .. attribute:: maxvalue
   
      Pixel value that indicates "overexposed" pixels.

   .. attribute:: scalable
    
      True if scalable dark frame is created
	
   A :class:`MasterDark` object has the following methods:
	
   .. method:: add(frame)
   
      Read image data from given CCD frame. The function is used to add
      source frames that shall be used to compute a master dark frame. The argument
      is a :class:`CCDFile` object. It is required that all source frames have the same
      image size and image data format.

   .. method:: flush()
   
      Finish making an output frame. The function computes the master dark
      frame and writes it to the :class:`CCDFile` object specified in the constructor.
      The data kept in the memory are cleared as well.
