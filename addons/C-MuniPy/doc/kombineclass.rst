.. C-Munipack Python module documentation 

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: kombineclass.rst,v 1.1 2015/07/06 08:27:20 dmotl Exp $

.. py:currentmodule:: cmpack

:class:`Kombine` --- Merging CCD frames
---------------------------------------

The :class:`Kombine` merges a set of CCD frames into one combined frame. 

.. class:: Kombine(output, [format, [border, [minvalue, [maxvalue]]]])
	
   A constructor that creates and initializes a new context for creating a 
   combined frame.
   The *output* argument is a :class:`CCDFile` object that shall receive an output
   data, it must be opened for writing.   
   The *format* argument can be used to explicitly specify which image
   data format shall be used to represent the output data. By default, the
   image data format in which the source frames are stored is used.   
   The *border* argument can be used to exclude a certain number of pixels
   along sides of the frame. Pixels in border area will be set to *minvalue* in the
   output file. The value should be a tuple of four integer numbers which specify
   the size of the left, top, right and bottom border in that order. If not used,
   the border is not applied.   
   The *minvalue* argument can be used to specify a value that
   indicates "bad" pixels. The default is 0.   
   The *maxvalue* argument can be used to specify a value that
   indicates "overexposed" pixels. The default is 65535.   
	
   A :class:`Kombine` object has the following read-only attributes:   
	
   .. attribute:: format
   
      Image data format shall be used to represent the output data
   
   .. attribute: border
      
      Tuple of four integer numbers which specify the size of the left, 
      top, right and bottom border in that order
   
   .. attribute:: minvalue
   
      Pixel value that indicates "bad" pixels.
      
   .. attribute:: maxvalue
   
      Pixel value that indicates "overexposed" pixels.
      
   A :class:`Kombine` object instance has the following methods:   
	
   .. method:: add(ccd_frame, pht_file)   
   
      Read image data from given :class:`CCDFile` object and frame offset from the :class:`PhtFile`
      file. The first argument is a :class:`CCDFile` object, the second argument is a photometry
      file which must be matched. All frames that makes a single kombined frames must
      be matched against the same reference frame, but the reference file itself don't
      need to be a part of the source set. Moreover, it is required that all source frames 
      have the same image size and image data format.   

   .. method:: flush()   
      
      Finish making an output frame. The function computes the combined frame and writes it to 
      the :class:`CCDFile` object specified in the constructor. The data kept in the memory 
      are cleared as well.   
