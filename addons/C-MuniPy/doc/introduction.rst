.. C-Munipack Python module documentation 

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: introduction.rst,v 1.1 2015/07/06 08:27:20 dmotl Exp $
   
   
Introduction
============

This document is a reference manual for an extension module to a Python
programming language that provides an application building interface to the
C-Munipack library. Its main purpose is to allow to build specialized applications, 
like fully automated CCD data reduction for sky surveys, in quick and efficient 
way.


Compatibility
-------------

The module is compatible with Python 2.6 and newer.


Features and capabilities
-------------------------

The module exports several functions and classes that allow to:

* Convert CCD frames from various formats (SBIG, RAW, OES) to FITS
* Merge a set of bias, dark and flat frames into master-bias, master-dark and master-flat frames.
* Apply bias, dark and flat corrections.
* Change observation time (time correction).
* Run the aperture photometry
* Find cross-references between two frames (matching)
* Make light curves
* Make track curves (frame offsets vs. observation time)
* Make a mag-dev curve for finding new variable stars
* Make a ap-dev curve for choosing an optimal aperture
* Compute heliocentric correction and air-mass coefficient
* Combine multiple CCD frames into a single frame.

	
Authors
-------

The project manager David Motl is also the author of the source codes.


License and copying
-------------------

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the Free 
Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the LICENSE file for more details.


Where can I get the latest version?
-----------------------------------

The project is hosted by the SourceForge. The latest version of the binaries, source codes
and documentation are available on the following address:

http://c-munipack.sourceforge.net/


Installation and uninstallation
-------------------------------

.. todo::

   How to install and uninstall the module
