.. C-Munipack Python module documentation 

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: cmpackmodule.rst,v 1.1 2015/07/06 08:27:20 dmotl Exp $

:mod:`cmpack` --- Astrophotometry tools
=======================================
   
.. module:: cmpack
   :synopsis: Astrophotometry tools

The :mod:`cmpack` module provides a complete set of tools for reduction of images carried out by CCD or DSLR cameras,
oriented at the observation of variable stars. Its main purpose is to allow to build specialized applications, 
like fully automated CCD data reduction for sky surveys, in quick and efficient way.
   
Classes
-------

The :mod:`cmpack` module exports the following classes:

.. toctree::

   airmassclass
   airmasscurveclass
   apdevcurveclass
   biasclass
   catfileclass
   ccdfileclass
   darkclass
   flatclass
   framesetclass
   helcorrclass
   kombineclass
   lightcurveclass
   magdevcurveclass
   masterbiasclass
   masterdarkclass
   masterflatclass
   matchclass
   photometryclass
   phtfileclass
   tableclass
   trackcurveclass
      
Functions
---------      
      
The :mod:`cmpack` module exports the following functions:

Conversion functions
^^^^^^^^^^^^^^^^^^^^

.. function:: encodejd(datetime)
   Convert DateTime object to Julian date.
   
.. function:: decodejd(julian_date)
   Convert Julian date to DateTime object.
   
.. function:: strtora(string)
   Return right ascension in hours from a string.
   
.. function:: ratostr(value)
   Print right ascension in hours to a string.
   
.. function:: strtodec(string)
   Return declination in degrees from a string.
   
.. function:: dectostr(value)
   Print declination in degrees to a string.
   
.. function:: strtolat(string)
   Return latitude in degrees from a string.

.. function:: lattostr(value)
   Print latitude in degrees to a string.

.. function:: strtolon(string)
   Return longitude in degrees from a string.

.. function:: lontostr(value)
   Print longitude in degrees to a string.

   
Math function
^^^^^^^^^^^^^
   
.. function:: robustmean(list)
   Take a list of float numbers and compute robust mean. The function returns a tuple consisting of 
   mean value and standard deviation.

   
Version information
^^^^^^^^^^^^^^^^^^^
   
.. function:: version()
   Return tuple consisting of major version, minor version, revision number and version string.

.. function:: check_version(major, minor, revision)
   Compare specified version and actual version of the module. The function returns True if the 
   module is compatible with specified version, otherwise the False value is returned.


   
Constants
---------

The following constants are defined in the :mod:`cmpack` module:

Access mode
^^^^^^^^^^^

The following symbols represent file access modes:
		
.. data:: READONLY
   Open file for reading only. If the file does not exist, raise an exception.

.. data:: READWRITE
   Open file for reading and writing. If the file does not exist, raise an exception.

.. data:: CREATE
   Open file for reading and writing. if the file exists already, its content is
   discarded. If the file does not exist, it is created.

CCD file formats
^^^^^^^^^^^^^^^^
   
The following symbols represent the file format of CCD frames:  

.. data:: FORMAT_UNKNOWN
   The value indicates that the file format was not recognized.

.. data:: FORMAT_FITS
   Flexible Image Tranport System (FITS)

.. data:: FORMAT_SBIG
   Proprietary format produced by SBIG cameras

.. data:: FORMAT_OES
   Proprietary format produced by OES Astro cameras

.. data:: FORMAT_CRW
   Raw image format produced by Canon DSLR cameras
 
.. data:: FORMAT_NEF
   Raw image format produced by Nikon DSLR cameras

DSLR channels
^^^^^^^^^^^^^
   
The following symbols represent channels in the color DSLR images:

.. data:: CHANNEL_DEFAULT
   This is the same as cmpack.CHANNEL_SUM. Use this value when a channel 
   selection is not applicable, as is the case of FITS, SBIG and OES images.

.. data:: CHANNEL_SUM     
   Sum all subpixels of the Bayer mask together. Please not, that the sum
   is NOT divided by a number of subpixels. No interpolation is applied, the resulting
   image will be of 1/4 resolution than the resolution of the original Bayer mask. 
		
.. data:: CHANNEL_RED
   Red channel. No interpolation is applied, the resulting image will be of 1/4 
   resolution than the resolution of the original Bayer mask.

.. data:: CHANNEL_GREEN 
   Green channel. Computes average from the two diagonal green pixels of the Bayer 
   mask.No interpolation is applied, the resulting image will be of 1/4 
   resolution than the resolution of the original Bayer mask. 
		  
.. data:: CHANNEL_BLUE
   Red channel. No interpolation is applied, the resulting image will be of 1/4 
   resolution than the resolution of the original Bayer mask.

Image data formats
^^^^^^^^^^^^^^^^^^
   
The following symbols represent image data formats:

.. data:: BITPIX_UNKNOWN
   The image data format was not recognized.
		
.. data:: BITPIX_INT16
   Signed 2-byte integer numbers in the range of -32,768 to 32,767.
		
.. data:: BITPIX_UINT16
   Unsigned 2-byte integer numbers in the range of 0 to 65,535.  

.. data:: BITPIX_INT32
   Signed 4-byte integer numbers in the range of -2,147,483,648 to 2,147,483,647.  

.. data:: BITPIX_UINT32
   Unsigned 4-byte integer numbers in the range of 0 to 4,294,967,295.

.. data:: BITPIX_FLOAT32
   4-byte long floating point numbers in the range of 3.4E +/- 38 with 7 significat digits.

.. data:: BITPIX_FLOAT64
   4-byte long floating point numbers in the range of 1.7E +/- 308 (15 digits) with 15 significat digits.   

Matching modes
^^^^^^^^^^^^^^
   
The following symbols represent modes for 'match' function:

.. data:: MATCH_STANDARD
   Always use the standard matching algorithm.

.. data:: MATCH_SPARSE_FIELDS
   Always use the matching algorithm for sparse fields.

.. data:: MATCH_AUTO
   Use standard matching algorithm if possible, use algorithm for sparse 
   fields if neccessary.

Table types
^^^^^^^^^^^

The following symbols represent table types:

.. data:: TABLE_UNSPECIFIED
   The type was not recognized or specified.

.. data:: TABLE_LIGHT_CURVE
   Table contains a light curve.

.. data:: TABLE_MUNIFIND
   Table contains a mag-dev curve.

.. data:: TABLE_TRACK_LIST
   Table contains a frame spatial offsets (track-list.
		
.. data:: TABLE_APERTURE
   Table contains apertures and standard deviations.

.. data:: TABLE_AIR_MASS   
   Table contains air-mass coefficients and/or altitudes.

Light curve types
^^^^^^^^^^^^^^^^^
   
The following symbols are used to specify what kind of magnitudes shall be included in the output:

.. data:: LCURVE_DIFFERENTIAL
   The light curve with differential instrumental magnitudes.

.. data:: LCURVE_INSTRUMENTAL
   The light curve with absolute instrumental magnitudes.

Julian date formats
^^^^^^^^^^^^^^^^^^^

The following symbols are used to specify what kind of Julian dates shall be included in the output:

.. data:: JD_GEOCENTRIC
   Print geocentric Julian dates only.
		
.. data:: JD_HELIOCENTRIC
   Print heliocentric Julian dates only.  

.. data:: JD_BOTH
   Print both geocentric and heliocentric Julian dates.  

Object roles
^^^^^^^^^^^^

The following symbols are used to specify a type (role) of an object:

.. data:: VARIABLE_STAR
   The object is a variable star.
		
.. data:: COMPARISON_STAR
   The object is a comparison star.

.. data:: CHECK_STAR
   The object is a check star.
