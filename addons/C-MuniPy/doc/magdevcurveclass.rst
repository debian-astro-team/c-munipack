.. C-Munipack Python module documentation 

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: magdevcurveclass.rst,v 1.1 2015/07/06 08:27:20 dmotl Exp $

.. py:currentmodule:: cmpack

:class:`MagDevCurve` --- Magnitude-deviations curve
---------------------------------------------------

The :class:`MagDevCurve` class creates a table of table of objects, their mean magnitudes
and sample deviations. Such table is suitable for finding a new variable stars.

This class derives all methods of :class:`Table`.

.. class:: MagDevCurve(frameset, aperture, [comp_star])

   A constructor that makes a mag-dev curve from a frame set. The *framset* argument is 
   a frame set that contains measurement data for specified apertures and objects.
   The *aperture* argument is an aperture identifier. The *comp_list* argument is an object 
   identifier of a comparison star, the default value is -1 which means that the program 
   shall automatically find a suitable comparison star.

   A :class:`MagDevCurve` object has the following attributes, in addition to :class:`Table`'s attributes:

   .. attribute:: object_id   
   
      Object identifier of current object, returns None if the table does not include
      object identifiers.   

   A :class:`MagDevCurve` object has the following methods, in addition to :class:`Table`'s methods:   
	
   .. method:: MagDevCurve.copy()   
   
      Make a deep copy of the table and returns a new MagDevCurve object.   
	
   .. method:: go_to(object)   
   
      The method finds an object by its identifier, specified as an argument.
      On success, it makes the object active and returns True. Otherwise, no object
      will be active and the method returns False.   
