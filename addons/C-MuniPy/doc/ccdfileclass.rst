.. C-Munipack Python module documentation 

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: ccdfileclass.rst,v 1.1 2015/07/06 08:27:20 dmotl Exp $

.. py:currentmodule:: cmpack
   
:class:`CCDFile` --- CCD frame file
-----------------------------------

The :class:`CCDFile` class provides an interface for reading and writing CCD frames 
and DSLR images.

.. note::
   
   The :class:`CCDFile` class is compatible with the 'with' statement.

.. class:: CCDFile()

   A constructor which create a new empty CCD image stored in the memory only.
   
   A new :class:`CCDFile` instance is often created by the following static method:    
	
   .. staticmethod:: open(filename, mode)   
   
   Create a new :class:`CCDFile` object attached to a file. The *filename* argument is 
   a path of the file. The *mode* argument specify an access mode, it must be one of the
   constants: cmpack.READONLY, cmpack.READWRITE, cmpack.CREATE.
   
   .. staticmethod:: test(filename)   
   
   Check if a file is a CCD frame file. The function inspects a content of a file specified
   by its path as the *filename* argument and returns True if the file looks like a CCD frame
   or DSLR image.
      
   A :class:`CCDFile` instance has the following attributes:

   .. attribute:: format_id   
   
      A number that identifies a file format (one of cmpack.FORMAT_xxx constants)   

   .. attribute:: format_name 
     
      A string that identifies a file format.   

   .. attribute:: image_size   
   
      A tuple consisting of width and height of the image in pixels   

   .. attribute:: image_format   
   
      A number that identifies a image data format (one of cmpack.BITPIX constants)   

   .. attribute:: date_time   
   
      Date and time of observation. The DateTime instance.   

   .. attribute:: julian_date   
   
      Julian date of observation.   

   .. attribute:: exposure   
   
      Exposure duration in seconds   

   .. attribute:: ccd_temperature   
   
      CCD chip temperature in degress Centigrade   

   .. attribute:: filter   
   
      Color filter designation   

   .. attribute:: observer   
   
      Observer's name   

   .. attribute:: object   
   
      A tuple consisting of object's designation, right ascension in hours
      and declination in degrees.   

   .. attribute:: location   
   
      A tuple consisting of designation of observer's location, observer's
      longitude in degrees and latitude in degrees.   

   A :class:`CCDFile` instance has the following methods:   
	
   .. method:: close()   
    
      The method updates the file (if neccessary) and clears the memory.
      After this call, the data are not accessible any more.   

   .. method:: save(filename)   
   
      The method writes a copy of the catalogue file and writes its content
      to the specified file. The *filename* argument is a path of a file to 
      be created.
