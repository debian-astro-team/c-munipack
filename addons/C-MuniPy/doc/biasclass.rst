.. C-Munipack Python module documentation 

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: biasclass.rst,v 1.1 2015/07/06 08:27:20 dmotl Exp $

.. py:currentmodule:: cmpack
   
:class:`Bias` --- Bias correction
---------------------------------

The :class:`Bias` class applies a bias correction to a CCD frame. The constructor is
used to create a context that keeps a bias frame and configuration parameters. The
correction is applied by calling :meth:`apply` or :meth:`modify` methods.

.. class:: Bias(from, [border, [minvalue, [maxvalue]]])
	
   Constructor that makes a bias correction context. The constructor reads image data
   from a :class:`CCDFile` object specified as *from* argument, this should be a bias frame. 
   The *border* argument can be used to exclude a certain number of pixels
   along sides of the frame. Pixels in border area will be set to 'minvalue' in the
   output file. The value should be a tuple of four integer numbers which specify
   the size of the left, top, right and bottom border in that order. If not used,
   the border is not applied. 
   The *minvalue* argument can be used to specify a value that
   indicates "bad" pixels. The default is 0.
   The *maxvalue* argument can be used to specify a value that
   indicates "overexposed" pixels. The default is 65535.
	
   An :class:`Bias` instance has the following read-only attributes:
	
   .. attribute:: border
   
      Tuple of four integer numbers which specify the size of the left, 
      top, right and bottom border in that order
   
   .. attribute:: minvalue
   
      Pixel value that indicates "bad" pixels.
      
   .. attribute:: maxvalue
   
      Pixel value that indicates "overexposed" pixels.
      
   A :class:`Bias` instance has the following methods:
   
   .. method:: apply(source)
   
      Apply bias correction to a :class:`CCDFile` object given in the *source* argument.
      The output frame is returned as a new :class:`CCDFile` object.
	
   .. method:: modify(frame)
   
      Apply bias correction to a :class:`CCDFile` object given in the *frame* argument.
      Unlike the :meth:`apply` method, the output data are written to the given object. The 
      :class:`CCDFile` object must be writable. The function returns True on success.
