.. C-Munipack Python module documentation 

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: flatclass.rst,v 1.1 2015/07/06 08:27:20 dmotl Exp $

.. py:currentmodule:: cmpack
   
:class:`Flat` --- Flat correction
---------------------------------

The :class:`Flat` class applies a flat correction to a CCD frame. The constructor is
used to create a context that keeps a flat frame and configuration parameters. The
correction is applied by calling :meth:`apply` or :meth:`modify` methods.

.. class:: Flat(from, [border, [minvalue, [maxvalue]]])
	
   Constructor for the Flat class, which stores a flat frame and
   parameters required to apply the flat correction to a CCD frame.
   The *from* argument must be a :class:`CCDFile` object and it is a flat
   frame.
   The *border* argument can be used to exclude a certain number of pixels
   along sides of the frame. Pixels in border area will be set to *minvalue* in the
   output file. The value should be a tuple of four integer numbers which specify
   the size of the left, top, right and bottom border in that order. If not used,
   the border is not applied.
   The *minvalue* argument can be used to specify a value that
   indicates "bad" pixels. The default is 0.
   The *maxvalue* argument can be used to specify a value that
   indicates "overexposed" pixels. The default is 65535.
	
   A :class:`Flat` object has the following read-only attributes:
	
   .. attribute: border
      
      Tuple of four integer numbers which specify the size of the left, 
      top, right and bottom border in that order
   
   .. attribute:: minvalue
   
      Pixel value that indicates "bad" pixels.
      
   .. attribute:: maxvalue
   
      Pixel value that indicates "overexposed" pixels.
      
   An :class:`Flat` instance has the following methods:
	
   .. method:: apply(source)
   
      Apply flat correction to a :class:`CCDFile` object given in the 'source' argument.
      The output frame is returned as a new :class:`CCDFile` object.
	
   .. method:: modify(frame)
   
      Apply flat correction to a :class:`CCDFile` object given in the 'frame' argument.
      Unlike the apply method, the output data are written to the given object. The 
      :class:`CCDFile` object must be writable. The function returns True on success.
