.. C-Munipack Python module documentation 

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: trackcurveclass.rst,v 1.1 2015/07/06 08:27:20 dmotl Exp $

.. py:currentmodule:: cmpack
   
:class:`TrackCurve` --- Frame offset curve
------------------------------------------

The :class:`TrackCurve` class creates a table of frame offsets vs. observation time. 
Such table is suitable for finding problems or checking performance of telescope's clock drive.

This class derives all methods of :class:`Table`.

.. class:: TrackCurve(frameset, [frameids])

   A constructor that makes a track curve from a frame set.
   The *framset* argument specify a frame set that was created from photometry files 
   after matching process. The optional *frameids* argument can be set to True to include 
   frame identifiers to the table. The default value is False.    

   The :class:`TrackCurve` object has the following attributes, in addition to :class:`Table` class attributes:

   .. attribute:: frame_id   
   
      Frame identifier of current frame, returns None if the table does not include frame identifiers.   

   .. attribute:: offset   
   
      A tuple consisting of offset in x and y directions from the active frame.   

   A :class:`TrackCurve` object has the following methods, in addition to :class:`Table` class methods:   
	
   .. method:: copy()   
   
      Make a deep copy of the table and returns a new TrackCurve object.   
	
   .. method:: go_to(frame)   
   
      Find a frame by its identifier and make it an active row. The *frame* argument is a frame
      identifier. The table must have been created with frame identifiers (see the constructor).
      On success, returns True. Otherwise, no row will be made active and the method returns False.
