.. C-Munipack Python module documentation 

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: airmasscurveclass.rst,v 1.1 2015/07/06 08:27:20 dmotl Exp $

.. py:currentmodule:: cmpack
   
:class:`AirMassCurve` --- Table of air-mass coefficients
--------------------------------------------------------

The :class:`AirMassCurve` class creates a table of air-mass coefficient and apparent
altitude for a set of frames. Object's equatorial coordinates (right ascension and declination) 
and observer's geographical coordinates (longitude and latitude) are specified using the 
constructor.

This class derives all methods of :class:`Table`.

.. class:: AirMassCurve(frameset, object, location, [frameids])

   A constructor that makes a air-mass curve from a frame set. 
   The *framset* argument is a frame set that contains measurement data for 
   specified apertures and objects.   
   The *object* argument is a tuple consisting of object's designation (can be None),
   right ascension in hours and declination in degrees.    
   The *location* argument is a tuple consisting of location's designation (can be None),
   longitude and latitude, both in degrees.    
   The *frameids* argument can be set to True to include frame identifiers. The default
   value is False.    

   An :class:`AirMassCurve` object has the following attributes, in addition to :class:`Table` class attributes:

   .. attribute:: frame_id   
   
      Frame identifier of current frame, returns None if the table does not include
      frame identifiers.   

   An :class:`AirMassCurve` object has the following methods, in addition to :class:`Table` class methods:   

   .. method:: copy()   
   
      Make a deep copy of the table and returns a new AirMassCurve object.   
	
   .. method:: go_to(frame)   
   
      The method finds a frame by its identifier, specified as *frame* argument.
      On success, it makes the frame active and returns True. Otherwise, no frame
      will be active and the method returns False.   
