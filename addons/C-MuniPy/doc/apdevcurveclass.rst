.. C-Munipack Python module documentation 

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: apdevcurveclass.rst,v 1.1 2015/07/06 08:27:20 dmotl Exp $

.. py:currentmodule:: cmpack
   
:class:`ApDevCurve` --- Aperture-Deviations curve
-------------------------------------------------

The :class:`ApDevCurve` class creates a table of deviations for a set of apertures. 
Such table is suitable for finding an optimal aperture. For optimal aperture, its deviation
is minimum. A set of stars, which are constant (non-variable stars), shall be specified
in the constructor. For compatibility with :class:`LightCurve` class interface, the
stars are divided into *comparison stars* and *check stars*.

This class derives all methods of :class:`Table`.

.. class:: ApDevCurve(frameset, comp_list, check_list)

   A constructor that makes a aperture-deviation curve from a frame set.   
   The *framset* argument is a frame set that contains measurement data for 
   specified apertures and objects.
   The *comp_list* argument is a list of object identifiers, list of comparison stars.
   The *check_list* argument is a list of object identifiers, list of check stars.

   The :class:`ApDevCurve` object has the following attributes, in addition to :class:`Table`'s attributes:

   .. attribute:: aperture
   
      Aperture identifier of current record, returns None if no record is active.

   .. attribute:: filter
   
      Color filter designation
      
   A :class:`ApDevCurve` object has the following methods, in addition to :class:`Table`'s methods:

   .. method:: copy()
   
      Make a deep copy of the table and returns a new ApDevCurve object.
	
   .. method:: go_to(aperture)
   
      The method finds an aperture by its identifier specified as *aperture* argument
      On success, it makes the record active and returns True. Otherwise, no record
      will be active and the method returns False.
