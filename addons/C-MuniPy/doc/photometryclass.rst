.. C-Munipack Python module documentation 

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: photometryclass.rst,v 1.1 2015/07/06 08:27:20 dmotl Exp $

.. py:currentmodule:: cmpack

:class:`Photometry` --- Aperture photometry
-------------------------------------------

The :class:`Photometry` class detects objects on a CCD frame and measures their
brightness using the aperture photometry algorithm. It makes a new photometry file
that contains a list of detected object and measurement data.

.. class:: Photometry()
	
   A constructor that makes a new photometry context. The constructor sets all parameters 
   to default values. The configuration parameters can be changes using object's attributes.

   A :class:`Photometry` object has the following attributes:

   .. attribute:: border   
   
      The border can be used to exclude a certain number of pixels along sides 
      of the frame. Pixels in border area will be regarded as bad pixels. By default,
      no border is used.   

   .. attribute:: minvalue   
   
      The value that indicates "bad" pixels. All pixels that have value equal to 
      minvalue or lesser are treated as bad pixels. The default is 0.   

   .. attribute:: maxvalue   
   
      The value that indicates "overexposed" pixels. All pixels that have value equal 
      to maxvalue or greater are treated as overexposed pixels. The default is 65535.   

   .. attribute:: rnoise   
   
      The readout noise level. The default is 15.0   

   .. attribute:: adcgain   
   
      The gain of the A/D converter. The default is 2.3   

   .. attribute:: fwhm   
   
      Expected value of FWHM of objects   

   .. attribute:: threshold    
   
      Detection threshold   

   .. attribute:: minsharpness   
   
      Min. sharpness threshold   

   .. attribute:: maxsharpness   
   
      Max. sharpness threshold   

   .. attribute:: minroundness   
   
      Min. roundness threshold   

   .. attribute:: maxroundness   
   
      Max. roundness threshold   

   .. attribute:: skyannulus   
   
      The value is a tuple consisting of inner and outer radius in pixels 
      of the annulus that is used to estimate local background (sky) level.   

   .. attribute:: apertures   
   
      The value is a list of apertures that are used to measure object's 
      brightness. The list consists of float numbers, each item specify radius
      in pixels of single aperture. Max. number of apertures is 12.   

   A :class:`Photometry` object has the following methods:   
	
   .. method:: run(frame)   
   
      Detect objects on a CCD frame specified as :class:`CCDFile` object in
      the argument and measure brightness of objects using aperture photometry. 
      It returns a new :class:`PhtFile` object that contains list of objects 
      and their measurement data.
