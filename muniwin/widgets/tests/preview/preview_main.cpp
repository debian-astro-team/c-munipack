/**************************************************************

main.cpp (C-Munipack project)
Main module
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#include <stdlib.h>
#include <locale.h>
#include <gtk/gtk.h>
#include <glib.h>

#include "preview_main.h"
#include "cmunipack.h"

tGlobal Global = { NULL };
GStaticRecMutex gs_lock = G_STATIC_REC_MUTEX_INIT;

static void enter_lock(void)
{
	g_static_rec_mutex_lock(&gs_lock);
}

static void leave_lock(void)
{
	g_static_rec_mutex_unlock(&gs_lock);
}

int main (int argc, char *argv[])
{
	setlocale(LC_ALL, "C");
	gtk_disable_setlocale();

	cmpack_init();

	// Init GTK
	g_thread_init(NULL);
	gdk_threads_set_lock_functions(enter_lock, leave_lock);
	gdk_threads_init();
	gdk_threads_enter();
	gtk_init(&argc, &argv);

	// Make global data
	Global.pMainWnd = new CMainWindow();

	// Main event loop
	gtk_main();

	// Clean up
	delete Global.pMainWnd;

	cmpack_cleanup();

	gdk_threads_leave();
	return 0;
}
