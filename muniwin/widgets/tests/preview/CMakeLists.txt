set(test_preview_headers
preview_dlg.h
preview_main.h
)

set(test_preview_sources
${test_preview_headers}
preview_dlg.cpp
preview_main.cpp
)

set(test_preview_libs
libcmpack
GTK2::gtk 
test_widgets_common
)

add_executable(test_preview ${test_preview_sources})
target_link_options(test_preview PRIVATE -mwindows)
set_target_properties(test_preview PROPERTIES FOLDER tests/widgets)
target_link_libraries(test_preview PRIVATE ${test_preview_libs})
