/**************************************************************

main_dlg.h (C-Munipack project)
Main dialog
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_MAIN_DLG_H
#define CMPACK_MAIN_DLG_H

#include <gtk/gtk.h>

#include "cmpack_widgets.h"

class CMainWindow
{
public:
	CMainWindow();
	~CMainWindow();

private:
	GtkWidget	*m_pDlg, *m_pWidget, *m_pPreview;
	GObject		*m_Data;
	gchar		*m_Text;

	// Selected file changed, update preview
	void OnUpdatePreview(GtkFileChooser *chooser);

	// Load image file
	void LoadImage(const gchar *fpath);

	// Load photometry file
	void LoadChart(const gchar *fpath);

	// Load catalog file
	void LoadCatalog(const gchar *fpath);

	// Load catalog file
	void LoadTable(const gchar *fpath);

	// Load readall file
	void LoadReadall(const gchar *fpath);

	// Signal handlers
	static void update_preview(GtkFileChooser *chooser, CMainWindow *pMe);
	static gint exit_event(GtkWidget *widget, GdkEvent *event, CMainWindow *pMe);
};

#endif
