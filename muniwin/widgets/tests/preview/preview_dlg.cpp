/**************************************************************

main_dlg.cpp (C-Munipack project)
Main dialog
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#include <stdlib.h>
#include <glib.h>
#include <math.h>
#include <string.h>

#include "preview_main.h"
#include "preview_dlg.h"
#include "image_class.h"
#include "phot_class.h"
#include "catalog_class.h"
#include "table_class.h"
#include "utils.h"

//-------------------------   PREVIEW TEST DIALOG   --------------------------------

CMainWindow::CMainWindow():m_Data(NULL), m_Text(NULL)
{
	// Main window
	m_pDlg = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW (m_pDlg), "Preview test");
	gtk_window_set_position(GTK_WINDOW(m_pDlg), GTK_WIN_POS_CENTER);
	gtk_window_set_default_size(GTK_WINDOW(m_pDlg), 800, 600);
	gtk_container_set_border_width(GTK_CONTAINER(m_pDlg), 8);
	g_signal_connect(G_OBJECT(m_pDlg), "delete_event", G_CALLBACK (exit_event), this);
	gtk_widget_realize(GTK_WIDGET(m_pDlg));

	// Window layout
	m_pWidget = gtk_file_chooser_widget_new(GTK_FILE_CHOOSER_ACTION_OPEN);
	g_signal_connect(G_OBJECT(m_pWidget), "update-preview", G_CALLBACK(update_preview), this);
	gtk_container_add(GTK_CONTAINER(m_pDlg), m_pWidget);
	gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(m_pWidget), "D:\\munipack\\data\\various\\verze 1.1");

	// Preview widget
	m_pPreview = cmpack_preview_new();
	gtk_widget_set_size_request(m_pPreview, 160, -1);
	gtk_file_chooser_set_preview_widget(GTK_FILE_CHOOSER(m_pWidget), m_pPreview);

	// Show window
	gtk_widget_show(m_pWidget);
	gtk_window_present(GTK_WINDOW(m_pDlg));
}

CMainWindow::~CMainWindow()
{
	if (m_Data)
		g_object_unref(m_Data);
	if (m_Text)
		g_free(m_Text);
}

gint CMainWindow::exit_event(GtkWidget *widget, GdkEvent *event, CMainWindow *pMe)
{
	gtk_main_quit();
	return FALSE;
}

void CMainWindow::update_preview(GtkFileChooser *widget, CMainWindow *pMe)
{
	pMe->OnUpdatePreview(widget);
}

void CMainWindow::OnUpdatePreview(GtkFileChooser *widget)
{
	gchar *fpath;

	cmpack_preview_set_model(CMPACK_PREVIEW(m_pPreview), NULL);
	cmpack_preview_set_text(CMPACK_PREVIEW(m_pPreview), NULL);
	if (m_Data) {
		g_object_unref(m_Data);
		m_Data = NULL;
	}
	if (m_Text) {
		g_free(m_Text);
		m_Text = NULL;
	}
	fpath = gtk_file_chooser_get_preview_filename(widget);
	if (fpath) {
		switch (FileType(fpath))
		{
		case TYPE_IMAGE:
			LoadImage(fpath);
			break;
		case TYPE_PHOT:
			LoadChart(fpath);
			break;
		case TYPE_CAT:
			LoadCatalog(fpath);
			break;
		case TYPE_TABLE:
			LoadTable(fpath);
			break;
		case TYPE_FRAMESET:
			LoadReadall(fpath);
			break;
		default:
			break;
		}
		g_free(fpath);
		if (m_Data)
			cmpack_preview_set_model(CMPACK_PREVIEW(m_pPreview), m_Data);
		if (m_Text)
			cmpack_preview_set_text(CMPACK_PREVIEW(m_pPreview), m_Text);
	}
}

//
// Load image file
//
void CMainWindow::LoadImage(const char *filename)
{
	CImage img;

	if (img.Load(filename)) {
		CmpackImageData *data = cmpack_image_data_new();
		img.Render(data, false, false, false);
		m_Data = G_OBJECT(data);
	}
}

//
// Load photometry file
//
void CMainWindow::LoadChart(const char *filename)
{
	CPhot pht;

	if (pht.Open(filename, CMPACK_OPEN_READONLY)) {
		CmpackChartData *data = cmpack_chart_data_new();
		pht.Render(data, 0, true, false);
		m_Data = G_OBJECT(data);
	}
}

//
// Load catalog file
//
void CMainWindow::LoadCatalog(const char *filename)
{
	CCatalog cat;

	// Catalog file
	if (cat.Open(filename, CMPACK_OPEN_READONLY)) {
		CmpackChartData *data = cmpack_chart_data_new();
		cat.Render(data);
		m_Data = G_OBJECT(data);
	}
}

//
// Load table
//
void CMainWindow::LoadTable(const char *filename)
{
	CTable tab;

	if (tab.Open(filename, CMPACK_OPEN_READONLY)) {
		CmpackGraphData *data = cmpack_graph_data_new();
		tab.Render(data, 0, 0, false);
		m_Data = G_OBJECT(data);
	}
}

//
// Load table
//
void CMainWindow::LoadReadall(const char *filename)
{
	m_Text = g_strdup("Munifind data file\n(no preview available)");
}
