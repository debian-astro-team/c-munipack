/**************************************************************

main_dlg.h (C-Munipack project)
Main dialog
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_MAIN_DLG_H
#define CMPACK_MAIN_DLG_H

#include <gtk/gtk.h>

#include "popup.h"
#include "menubar.h"
#include "cmpack_widgets.h"
#include "image_class.h"
#include "phot_class.h"
#include "utils.h"

class CMainWindow
{
public:
	CMainWindow();
	~CMainWindow();

private:
	bool			m_Updating, m_UpdatePos;
	int				m_ActAperture, m_SelectedCount, m_SelectedItem;
	gint			m_TimerId, m_StatusCtx, m_StatusMsg;
	GtkWidget		*m_pDlg, *m_pChart, *m_pStatus;
	CImage			*m_Image;
	CPhot			m_Data;
	CmpackImageData	*m_ImageData;
	CmpackChartData	*m_ChartData;
	CMenuBar		m_Menu;
	CPopupMenu		m_Popup;

	void UpdateStatus(void);

	void OnItemActivated(int row);
	void OnSelectionChanged(void);
	void OnCommand(int command);
	void OnContextMenu(GtkWidget *pWidget, GdkEventButton *event);

	static gint exit_event(GtkWidget *widget, GdkEvent *event, CMainWindow *pMe);
	static void MenuCallback(CCBObject *sender, int message, int wparam, void* lparam, void* cb_data);
	static void PopupCallback(CCBObject *sender, int message, int wparam, void* lparam, void* cb_data);
	static void mouse_moved(GtkWidget *graph, CMainWindow *pMe);
	static void item_activated(GtkWidget *graph, gint row, CMainWindow *pMe);
	static void selection_changed(GtkWidget *graph, CMainWindow *pMe);
	static gboolean button_press_event(GtkWidget *widget, GdkEventButton *event, CMainWindow *pMe);
	static gboolean update_sbar(CMainWindow *pMe);
};

#endif
