/**************************************************************

makelightcurve_dlg.h (C-Munipack project)
'Make light curve' dialog
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_LIGHT_CURVE_OPTIONS_DLG_H
#define CMPACK_LIGHT_CURVE_OPTIONS_DLG_H

#include <gtk/gtk.h>

#include "graph_dlg.h"

//
// Make catalog file 
//
class CLightCurveOptionsDlg
{
public:
	// Constructor
	CLightCurveOptionsDlg(CMainWindow *pDlg);

	// Destructor
	~CLightCurveOptionsDlg();

	// Execute the dialog
	bool Execute(void);

private:
	CMainWindow		*m_pLCD;
	GtkWidget		*m_pDlg, *m_OptBox, *m_HelCor, *m_AirMass, *m_Altitude;
	GtkWidget		*m_AdvBox, *m_InstMag;
	GtkWidget		*m_ObjBox, *m_Object, *m_ObjBtn, *m_RA, *m_Dec;
	GtkWidget		*m_LocBox, *m_Location, *m_LocBtn, *m_Lon, *m_Lat;

	void SetData(void);
	void GetData(void);
	void EditObjectCoords(void);
	void EditLocation(void);

	bool OnResponseDialog(gint response_id);
	void OnButtonClicked(GtkWidget *pBtn);
	bool OnCloseQuery(void);

	static void response_dialog(GtkDialog *pDlg, gint response_id, CLightCurveOptionsDlg *pMe);
	static void button_clicked(GtkWidget *button, CLightCurveOptionsDlg *pDlg);
};

#endif
