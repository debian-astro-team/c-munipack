/**************************************************************

makelightcurve_dlg.cpp (C-Munipack project)
'Make light curve files' dialog
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#include <stdlib.h>
#include <string.h>

#include "lcoptions_dlg.h"
#include "helper_classes.h"
#include "graph_main.h"
#include "utils.h"

CLightCurveOptionsDlg::CLightCurveOptionsDlg(CMainWindow *pDlg):m_pLCD(pDlg)
{
	GtkWidget *tbox, *label;

	// Dialog with buttons
	m_pDlg = gtk_dialog_new_with_buttons("Light curve options", GTK_WINDOW(m_pLCD->GetHandle()), 
		(GtkDialogFlags)(GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_NO_SEPARATOR),
		GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL, GTK_STOCK_APPLY, GTK_RESPONSE_ACCEPT, 
		GTK_STOCK_HELP, GTK_RESPONSE_HELP, NULL);
	g_signal_connect(G_OBJECT(m_pDlg), "response", G_CALLBACK(response_dialog), this);

	// Dialog icon
	gchar *icon = get_icon_file("lightcurve");
	gtk_window_set_icon(GTK_WINDOW(m_pDlg), gdk_pixbuf_new_from_file(icon, NULL));
	g_free(icon);

	// Dialog layout
	tbox = gtk_table_new(12, 4, FALSE);
	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(m_pDlg)->vbox), tbox);
	gtk_container_set_border_width(GTK_CONTAINER(tbox), 4);
	gtk_table_set_row_spacings(GTK_TABLE(tbox), 4);
	gtk_table_set_row_spacing(GTK_TABLE(tbox), 2, 12);
	gtk_table_set_row_spacing(GTK_TABLE(tbox), 3, 12);
	gtk_table_set_row_spacing(GTK_TABLE(tbox), 6, 12);
	gtk_table_set_col_spacings(GTK_TABLE(tbox), 4);

	// Light curve options
	m_HelCor = gtk_check_button_new_with_label("Compute heliocentric correction");
	gtk_table_attach(GTK_TABLE(tbox), m_HelCor, 0, 3, 0, 1, GTK_FILL, GTK_FILL, 0, 0);
	m_AirMass = gtk_check_button_new_with_label("Compute air-mass coefficient");
	gtk_table_attach(GTK_TABLE(tbox), m_AirMass, 0, 3, 1, 2, GTK_FILL, GTK_FILL, 0, 0);
	m_Altitude = gtk_check_button_new_with_label("Compute altitude");
	gtk_table_attach(GTK_TABLE(tbox), m_Altitude, 0, 3, 2, 3, GTK_FILL, GTK_FILL, 0, 0);

	// Advanced options
	m_InstMag = gtk_check_button_new_with_label("Show raw instrumental magnitudes");
	gtk_table_attach(GTK_TABLE(tbox), m_InstMag, 0, 3, 3, 4, GTK_FILL, GTK_FILL, 0, 0);

	// Object coordinates
	label = gtk_label_new("Object - designation");
	gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
	gtk_table_attach_defaults(GTK_TABLE(tbox), label, 0, 1, 4, 5);
	m_Object = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(m_Object), MAX_OBJNAME_LEN);
	gtk_table_attach_defaults(GTK_TABLE(tbox), m_Object, 1, 2, 4, 5);
	m_ObjBtn = gtk_button_new_with_label("...");
	g_signal_connect(G_OBJECT(m_ObjBtn), "clicked", G_CALLBACK(button_clicked), this);
	gtk_table_attach_defaults(GTK_TABLE(tbox), m_ObjBtn, 2, 3, 4, 5);
	label = gtk_label_new("- right ascension");
	gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
	gtk_table_attach_defaults(GTK_TABLE(tbox), label, 0, 1, 5, 6);
	m_RA = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(m_RA), 64);
	gtk_table_attach_defaults(GTK_TABLE(tbox), m_RA, 1, 2, 5, 6);
	label = gtk_label_new("[h m s]");
	gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
	gtk_table_attach_defaults(GTK_TABLE(tbox), label, 2, 3, 5, 6);
	label = gtk_label_new("- declination");
	gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
	gtk_table_attach_defaults(GTK_TABLE(tbox), label, 0, 1, 6, 7);
	m_Dec = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(m_Dec), 64);
	gtk_table_attach_defaults(GTK_TABLE(tbox), m_Dec, 1, 2, 6, 7);
	label = gtk_label_new("[\xC2\xB1""d m s]");
	gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
	gtk_table_attach_defaults(GTK_TABLE(tbox), label, 2, 3, 6, 7);

	// Location
	label = gtk_label_new("Location - name");
	gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
	gtk_table_attach_defaults(GTK_TABLE(tbox), label, 0, 1, 7, 8);
	m_Location = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(m_Location), MAX_LOCATION_LEN);
	gtk_table_attach_defaults(GTK_TABLE(tbox), m_Location, 1, 2, 7, 8);
	m_LocBtn = gtk_button_new_with_label("...");
	g_signal_connect(G_OBJECT(m_LocBtn), "clicked", G_CALLBACK(button_clicked), this);
	gtk_table_attach_defaults(GTK_TABLE(tbox), m_LocBtn, 2, 3, 7, 8);
	label = gtk_label_new("- longitude");
	gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
	gtk_table_attach_defaults(GTK_TABLE(tbox), label, 0, 1, 8, 9);
	m_Lon = gtk_entry_new_with_max_length(32);
	gtk_table_attach_defaults(GTK_TABLE(tbox), m_Lon, 1, 2, 8, 9);
	label = gtk_label_new("[E/W d m s]");
	gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
	gtk_table_attach_defaults(GTK_TABLE(tbox), label, 2, 3, 8, 9);
	label = gtk_label_new("- latitude");
	gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
	gtk_table_attach_defaults(GTK_TABLE(tbox), label, 0, 1, 9, 10);
	m_Lat = gtk_entry_new_with_max_length(32);
	gtk_table_attach_defaults(GTK_TABLE(tbox), m_Lat, 1, 2, 9, 10);
	label = gtk_label_new("[N/S d m s]");
	gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
	gtk_table_attach_defaults(GTK_TABLE(tbox), label, 2, 3, 9, 10);

	gtk_widget_show_all(GTK_DIALOG(m_pDlg)->vbox);
}

CLightCurveOptionsDlg::~CLightCurveOptionsDlg()
{
	gtk_widget_destroy(m_pDlg);
}

void CLightCurveOptionsDlg::response_dialog(GtkDialog *pDlg, gint response_id, CLightCurveOptionsDlg *pMe)
{
	if (!pMe->OnResponseDialog(response_id))
		g_signal_stop_emission_by_name(pDlg, "response");
}

bool CLightCurveOptionsDlg::OnResponseDialog(gint response_id)
{
	switch (response_id)
	{
	case GTK_RESPONSE_ACCEPT:
		// Commit changes
		return OnCloseQuery();

	case GTK_RESPONSE_HELP:
		// Show context help
		Global.pMainWnd->ShowHelp(IDH_PLOT_LIGHT_CURVE);
		return false;
	}
	return true;
}

bool CLightCurveOptionsDlg::Execute(void)
{
	if (m_pLCD) {
		SetData();
		if (gtk_dialog_run(GTK_DIALOG(m_pDlg)) == GTK_RESPONSE_ACCEPT) {
			GetData();
			return true;
		}
	}
	return false;
}

//
// Set parameters
//
void CLightCurveOptionsDlg::SetData(void)
{
	CMainWindow::tParams params;

	m_pLCD->GetParams(&params);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(m_HelCor), params.helcor);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(m_AirMass), params.airmass);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(m_Altitude), params.altitude);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(m_InstMag), params.instmag);

	const CObjectCoords *obj = m_pLCD->ObjectCoords();
	if (obj->Name())
		gtk_entry_set_text(GTK_ENTRY(m_Object), obj->Name());
	else
		gtk_entry_set_text(GTK_ENTRY(m_Object), "");
	if (obj->RA())
		gtk_entry_set_text(GTK_ENTRY(m_RA), obj->RA());
	else
		gtk_entry_set_text(GTK_ENTRY(m_RA), "");
	if (obj->Dec())
		gtk_entry_set_text(GTK_ENTRY(m_Dec), obj->Dec());
	else
		gtk_entry_set_text(GTK_ENTRY(m_Dec), "");

	const CLocation *obs = m_pLCD->Location();
	if (obs->Name())
		gtk_entry_set_text(GTK_ENTRY(m_Location), obs->Name());
	else
		gtk_entry_set_text(GTK_ENTRY(m_Location), "");
	if (obs->Lon())
		gtk_entry_set_text(GTK_ENTRY(m_Lon), obs->Lon());
	else
		gtk_entry_set_text(GTK_ENTRY(m_Lon), "");
	if (obs->Lat())
		gtk_entry_set_text(GTK_ENTRY(m_Lat), obs->Lat());
	else
		gtk_entry_set_text(GTK_ENTRY(m_Lat), "");
}

//
// Get parameters
//
void CLightCurveOptionsDlg::GetData(void)
{
	CMainWindow::tParams params;

	m_pLCD->GetParams(&params);
	params.helcor   = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(m_HelCor))!=0;
	params.airmass  = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(m_AirMass))!=0;
	params.altitude = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(m_Altitude))!=0;
	params.instmag  = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(m_InstMag))!=0;
	m_pLCD->SetParams(&params);

	if (params.airmass || params.altitude) {
		CLocation obs;
		obs.SetName(gtk_entry_get_text(GTK_ENTRY(m_Location)));
		obs.SetLon(gtk_entry_get_text(GTK_ENTRY(m_Lon)));
		obs.SetLat(gtk_entry_get_text(GTK_ENTRY(m_Lat)));
		m_pLCD->SetLocation(obs);
	}

	if (params.helcor || params.airmass || params.altitude) {
		CObjectCoords obj;
		obj.SetName(gtk_entry_get_text(GTK_ENTRY(m_Object)));
		obj.SetRA(gtk_entry_get_text(GTK_ENTRY(m_RA)));
		obj.SetDec(gtk_entry_get_text(GTK_ENTRY(m_Dec)));
		m_pLCD->SetObjectCoords(obj);
	}
}

bool CLightCurveOptionsDlg::OnCloseQuery()
{
	gboolean helcor, airmass, altitude;
	const gchar *ra, *dec, *lon, *lat;
	gchar buf[64];
	gdouble x, y;

	helcor   = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(m_HelCor));
	airmass  = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(m_AirMass));
	altitude = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(m_Altitude));

	if (airmass || altitude) {
		ra = gtk_entry_get_text(GTK_ENTRY(m_RA));
		dec = gtk_entry_get_text(GTK_ENTRY(m_Dec));
		if (*ra=='\0' || *dec=='\0') {
			ShowError(GTK_WINDOW(m_pDlg), "Please, enter object's coordinates.");
			return false;
		}
		if (cmpack_strtora(ra, &x)!=0) {
			ShowError(GTK_WINDOW(m_pDlg), "Invalid value of the right ascension.");
			return false;
		} else {
			cmpack_ratostr(x, buf, 64);
			gtk_entry_set_text(GTK_ENTRY(m_RA), buf);
		}
		if (cmpack_strtodec(dec, &y)!=0) {
			ShowError(GTK_WINDOW(m_pDlg), "Invalid value of the declination.");
			return false;
		} else {
			cmpack_dectostr(y, buf, 64);
			gtk_entry_set_text(GTK_ENTRY(m_Dec), buf);
		}
	}

	if (helcor || airmass || altitude) {
		lon = gtk_entry_get_text(GTK_ENTRY(m_Lon));
		lat = gtk_entry_get_text(GTK_ENTRY(m_Lat));
		if (*lon=='\0' || *lat=='\0') {
			ShowError(GTK_WINDOW(m_pDlg), "Please, enter the geographic coordinates.");
			return false;
		}
		if (cmpack_strtolon(lon, &x)!=0) {
			ShowError(GTK_WINDOW(m_pDlg), "Invalid value of the longitude.");
			return false;
		} else {
			cmpack_lontostr(x, buf, 64);
			gtk_entry_set_text(GTK_ENTRY(m_Lon), buf);
		}
		if (cmpack_strtolat(lat, &y)!=0) {
			ShowError(GTK_WINDOW(m_pDlg), "Invalid value of the latitude.");
			return false;
		} else {
			cmpack_lattostr(y, buf, 64);
			gtk_entry_set_text(GTK_ENTRY(m_Lat), buf);
		}
	}
	return true;
}

void CLightCurveOptionsDlg::button_clicked(GtkWidget *pButton, CLightCurveOptionsDlg *pMe)
{
	pMe->OnButtonClicked(pButton);
}

void CLightCurveOptionsDlg::OnButtonClicked(GtkWidget *pButton)
{
	if (pButton == m_ObjBtn) 
		EditObjectCoords();
	else if (pButton == m_LocBtn) 
		EditLocation();
}

void CLightCurveOptionsDlg::EditObjectCoords(void)
{
	/*CObjectCoords obj;
	CObjectDlg dlg(GTK_WINDOW(m_pDlg));

	obj.SetName(gtk_entry_get_text(GTK_ENTRY(m_Object)));
	obj.SetRA(gtk_entry_get_text(GTK_ENTRY(m_RA)));
	obj.SetDec(gtk_entry_get_text(GTK_ENTRY(m_Dec)));
	if (dlg.EditCoords(&obj)) {
		gtk_entry_set_text(GTK_ENTRY(m_Object), obj.Name());
		gtk_entry_set_text(GTK_ENTRY(m_RA), obj.RA());
		gtk_entry_set_text(GTK_ENTRY(m_Dec), obj.Dec());
	}*/
}

void CLightCurveOptionsDlg::EditLocation(void)
{
	/*CLocation obs;
	CLocationDlg dlg(GTK_WINDOW(m_pDlg));

	obs.SetName(gtk_entry_get_text(GTK_ENTRY(m_Location)));
	obs.SetLon(gtk_entry_get_text(GTK_ENTRY(m_Lon)));
	obs.SetLat(gtk_entry_get_text(GTK_ENTRY(m_Lat)));
	if (dlg.EditCoords(&obs)) {
		gtk_entry_set_text(GTK_ENTRY(m_Location), obs.Name());
		gtk_entry_set_text(GTK_ENTRY(m_Lon), obs.Lon());
		gtk_entry_set_text(GTK_ENTRY(m_Lat), obs.Lat());
	}*/
}
