/**************************************************************

cmpack_graph_view.h (C-Munipack project)
Widget which can draw a XY graph (derived from GtkWidget)
Copyright (C) 2011 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

$Id: cmpack_image_data.h,v 1.2 2016/04/24 12:24:07 dmotl Exp $

**************************************************************/

#ifndef CMPACK_IMAGE_DATA_H
#define CMPACK_IMAGE_DATA_H

#include <gtk/gtk.h>

#include "cmpack_widgdefs.h"

G_BEGIN_DECLS

typedef struct {
	GObject			parent;

	/* Private data */ 
	cairo_surface_t	*data;
} CmpackImageData;

typedef struct {
	GObjectClass parent_class;

	/* Signals */
	void (*data_changed)(CmpackImageData *model);
	void (*size_changed)(CmpackImageData *model);
} CmpackImageDataClass;

#define CMPACK_TYPE_IMAGE_DATA            (cmpack_image_data_get_type ())
#define CMPACK_IMAGE_DATA(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CMPACK_TYPE_IMAGE_DATA, CmpackImageData))
#define CMPACK_IMAGE_DATA_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), CMPACK_TYPE_IMAGE_DATA, CmpackImageDataClass))
#define CMPACK_IS_IMAGE_DATA(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CMPACK_TYPE_IMAGE_DATA))
#define CMPACK_IS_IMAGE_DATA_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), CMPACK_TYPE_IMAGE_DATA))
#define CMPACK_IMAGE_DATA_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), CMPACK_TYPE_IMAGE_DATA, CmpackImageDataClass)) 

GType cmpack_image_data_get_type(void) G_GNUC_CONST;

/* Create new empty image data */
CmpackImageData *cmpack_image_data_new(cairo_format_t format, gint width, gint height);

/* Call this function when you have finished updating the data*/
void cmpack_image_data_changed(CmpackImageData *data);

/* Get image width in pixels */
gint cmpack_image_data_width(CmpackImageData *data);

/* Get image height in pixels */
gint cmpack_image_data_height(CmpackImageData *data);

/* Get pointer to cairo surface */
cairo_surface_t *cmpack_image_data_get_surface(CmpackImageData *data);

/* Export image into a file */
gboolean cmpack_image_data_write_to_file(CmpackImageData *data,
	const gchar *filepath, const gchar *format, gint width, gint height, gint jpeg_quality);

G_END_DECLS

#endif
