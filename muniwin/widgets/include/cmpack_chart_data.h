/**************************************************************

cmpack_chart_data.h (C-Munipack project)
Object which holds data for a chart (derived from GtkObject)
Copyright (C) 2011 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

$Id: cmpack_chart_data.h,v 1.2 2016/04/24 12:24:07 dmotl Exp $

**************************************************************/
#ifndef CMPACK_CHART_DATA_H
#define CMPACK_CHART_DATA_H

#include <stdint.h>
#include <gtk/gtk.h>

#include "cmpack_widgdefs.h"
#include "cmpack_image_data.h"

G_BEGIN_DECLS

typedef struct {
	gdouble		x, y;			// Position of center in pixels
	gdouble		d;				// Circle diameter
	gchar		*tag;			// Object's tag
	CmpackColor	color;			// Color of the circle and tag
	gboolean	hidden : 1;		// Display this item
	gboolean	outline : 1;	// Do not fill the circle
	gboolean	disabled : 1;	// Cannot be selected or activated
	gboolean    topmost : 1;	// Displayed on top of other items
	intptr_t	param;			// User data
} CmpackChartItem;

typedef struct {
	GObject			parent;

	/* Private data */ 
	gint			width, height;		/* Dimensions in pixels */
	gint			count, capacity;	/* Number of objects, allocated space */
	CmpackChartItem **items;			/* List of objects */
} CmpackChartData;

typedef struct {
	GObjectClass parent_class;

	/* Signals */
	void (*object_inserted)(CmpackChartData *model, gint row);
	void (*object_updated)(CmpackChartData *model, gint row);
	void (*object_deleted)(CmpackChartData *model, gint row);
	void (*dim_changed)(CmpackChartData *model);
	void (*data_cleared)(CmpackChartData *model);
} CmpackChartDataClass;

#define CMPACK_TYPE_CHART_DATA            (cmpack_chart_data_get_type ())
#define CMPACK_CHART_DATA(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CMPACK_TYPE_CHART_DATA, CmpackChartData))
#define CMPACK_CHART_DATA_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), CMPACK_TYPE_CHART_DATA, CmpackChartDataClass))
#define CMPACK_IS_CHART_DATA(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CMPACK_TYPE_CHART_DATA))
#define CMPACK_IS_CHART_DATA_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), CMPACK_TYPE_CHART_DATA))
#define CMPACK_CHART_DATA_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), CMPACK_TYPE_CHART_DATA, CmpackChartDataClass)) 

GType cmpack_chart_data_get_type(void) G_GNUC_CONST;

/* Create new chart data */
CmpackChartData *cmpack_chart_data_new(void);

/* Create new chart data with allocated capacity */
CmpackChartData *cmpack_chart_data_new_with_alloc(gint capacity);

/* Set allocated space for objects */
void cmpack_chart_data_alloc(CmpackChartData *data, gint capacity);

/* Set allocated space for objects */
void cmpack_chart_data_set_dimensions(CmpackChartData *data, gint width, gint height);

/* Get chart width in pixels */
gint cmpack_chart_data_width(CmpackChartData *data);

/* Get chart height in pixels */
gint cmpack_chart_data_height(CmpackChartData *data);

/* Get number of objects */
gint cmpack_chart_data_count(CmpackChartData *data);

/* Clear all data, set dimensions to zero */
void cmpack_chart_data_clear(CmpackChartData *data);

/* Add object to the list */
gint cmpack_chart_data_add(CmpackChartData *data, const CmpackChartItem *item, gsize item_size);

/* Find object by user data */
gint cmpack_chart_data_find_item(CmpackChartData *data, intptr_t param);

/* Change object's visibility */
void cmpack_chart_data_show_item(CmpackChartData *data, gint row, gboolean visible);

/* Change object's position */
void cmpack_chart_data_set_center(CmpackChartData *data, gint row, gdouble center_x, gdouble center_y);

/* Change object's size */
void cmpack_chart_data_set_diameter(CmpackChartData *data, gint row, gdouble diameter);

/* Change object's color */
void cmpack_chart_data_set_color(CmpackChartData *data, gint row, CmpackColor color);

/* Change object's tag */
void cmpack_chart_data_set_tag(CmpackChartData *data, gint row, const char *tag);

/* Change object's user data */
void cmpack_chart_data_set_param(CmpackChartData *data, gint row, intptr_t param);

/* Change object's sensitivity */
void cmpack_chart_data_set_sensitivity(CmpackChartData *data, gint row, gboolean sensitive);

/* Change object's sensitivity */
void cmpack_chart_data_set_outline(CmpackChartData *data, gint row, gboolean outline);

/* Change object's priority */
void cmpack_chart_data_set_topmost(CmpackChartData *data, gint row, gboolean topmost);

/* Delete object */
void cmpack_chart_data_delete(CmpackChartData *data, gint row);

/* Get object's identifier */
intptr_t cmpack_chart_data_get_param(CmpackChartData *data, gint row);

/* Get object's position */
gboolean cmpack_chart_data_get_center(CmpackChartData *data, gint row, gdouble *x, double *y);

/* Get object's tag */
const gchar *cmpack_chart_data_get_tag(CmpackChartData *data, gint row);

/* Get pointer to object */
CmpackChartItem *cmpack_chart_data_get_item(CmpackChartData *data, gint row);

/* Clear all tags */
void cmpack_chart_data_clear_tags(CmpackChartData *data);

/* Export chart to a file */
gboolean cmpack_chart_data_write_to_file(CmpackChartData *data, CmpackImageData *image,
	const gchar *filepath, const gchar *format, gint width, gint height, gboolean negative,
	CmpackOrientation orientation, gint jpeg_quality);

G_END_DECLS

#endif
