/**************************************************************

cmpack_widgdefs.h (C-Munipack project)
Definitions and constants common for all widgets
Copyright (C) 2011 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

$Id: cmpack_widgdefs.h,v 1.1 2015/07/12 08:32:30 dmotl Exp $

**************************************************************/
#ifndef CMPACK_WIDGDEFS_H
#define CMPACK_WIDGDEFS_H

#include <gtk/gtk.h>

G_BEGIN_DECLS

/* User-defined colors */
typedef enum _CmpackColor {
	CMPACK_COLOR_DEFAULT,
	CMPACK_COLOR_GRAY,
	CMPACK_COLOR_RED,
	CMPACK_COLOR_GREEN,
	CMPACK_COLOR_BLUE,
	CMPACK_COLOR_YELLOW,
	CMPACK_COLOR_CUSTOM,
	CMPACK_N_COLORS
} CmpackColor;

/* Activation style */
typedef enum _CmpackActivationMode {
	CMPACK_ACTIVATION_DISABLED,
	CMPACK_ACTIVATION_CLICK,
	CMPACK_ACTIVATION_DBLCLICK
} CmpackActivationMode;

/* Graph axis */
typedef enum {
	CMPACK_AXIS_X,			// X axis
	CMPACK_AXIS_Y			// Y axis
} CmpackGraphAxis;

/* Chart orientation */
typedef enum {
	CMPACK_ROWS_DOWNWARDS = 0,
	CMPACK_ROWS_UPWARDS   = (1<<0)
} CmpackOrientation;

G_END_DECLS

#endif
