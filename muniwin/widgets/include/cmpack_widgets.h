/**************************************************************

cmpack_widgets.h (C-Munipack project)
This header includes all other widget headers
Copyright (C) 2011 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

$Id: cmpack_widgets.h,v 1.2 2016/02/21 10:32:08 dmotl Exp $

**************************************************************/
#ifndef CMPACK_WIDGETS_H
#define CMPACK_WIDGETS_H

#include <gtk/gtk.h>

#include "cmpack_widgdefs.h"
#include "cmpack_curve_plot.h"
#include "cmpack_graph_data.h"
#include "cmpack_graph_view.h"
#include "cmpack_chart_data.h"
#include "cmpack_chart_view.h"
#include "cmpack_image_data.h"
#include "cmpack_scale.h"
#include "cmpack_preview.h"
#include "cmpack_time_scale_data.h"
#include "cmpack_time_scale_view.h"

#endif
