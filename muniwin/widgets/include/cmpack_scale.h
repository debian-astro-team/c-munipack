/**************************************************************

cmpack_scale.h (C-Munipack project)
Widget which can draw a gray scale or pseudocolor scale (derived from GtkWidget)
Copyright (C) 2011 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

$Id: cmpack_scale.h,v 1.1 2015/07/12 08:32:30 dmotl Exp $

**************************************************************/

#ifndef CMPACK_SCALE_H
#define CMPACK_SCALE_H

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define CMPACK_TYPE_SCALE            (cmpack_scale_get_type ())
#define CMPACK_SCALE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CMPACK_TYPE_SCALE, CmpackScale))
#define CMPACK_SCALE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), CMPACK_TYPE_SCALE, CmpackScaleClass))
#define CMPACK_IS_SCALE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CMPACK_TYPE_SCALE))
#define CMPACK_IS_SCALE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), CMPACK_TYPE_SCALE))
#define CMPACK_SCALE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), CMPACK_TYPE_SCALE, CmpackScaleClass))

typedef struct _CmpackScale           CmpackScale;
typedef struct _CmpackScaleClass      CmpackScaleClass;

/* Graph view */
struct _CmpackScale
{
	GtkWidget parent;

	/* Private data */
	gint width, height;
	gboolean needs_repaint, needs_rebuild;
	GdkPixmap *offscreen_pixmap;
	GtkOrientation orientation;
	gboolean pseudocolor, reverse;
	gdouble min, max;
	GdkRectangle scale_rc;
	cairo_surface_t *scale_image;		// Scale image
};

/* Graph view class */
struct _CmpackScaleClass
{
  GtkWidgetClass parent_class;
};

GType cmpack_scale_get_type(void) G_GNUC_CONST;

/* -------------------------   PUBLIC FUNCTIONS   --------------------------------- */

/* Create a new scale 
 * Returns floating reference to the scale
 */
GtkWidget *cmpack_scale_new(GtkOrientation orientation);

/* Change the orientation of the displayed scale
 * params:
 *	scale					- [in] scale
 *	type					- [in] new scale type
 */
void cmpack_scale_set_orientation(CmpackScale *scale, GtkOrientation orientation);

/* Get current orientation of displayed scale
 * params:
 *	scale					- [in] scale
 * Returns current scale type
 */
GtkOrientation cmpack_scale_get_orientation(CmpackScale *scale);

/* Change the type of the displayed scale
 * params:
 *	scale					- [in] scale
 *	type					- [in] new scale type
 */
void cmpack_scale_set_pseudocolor(CmpackScale *scale, gboolean pseudocolor);

/* Get current type of displayed scale
 * params:
 *	scale					- [in] scale
 * Returns current scale type
 */
gboolean cmpack_scale_get_pseudocolor(CmpackScale *scale);

/* Change the inversion flag of the scale
 * params:
 *	scale					- [in] scale
 *	type					- [in] new scale type
 */
void cmpack_scale_set_invert(CmpackScale *scale, gboolean invert);

/* Get the state of the inversion flag
 * params:
 *	scale					- [in] scale
 * Returns inversion flag
 */
gboolean cmpack_scale_get_invert(CmpackScale *scale);

/* Change the range of values
 * params:
 *	scale					- [in] scale
 *	min, max				- [in] minimum and maximum value
 */
void cmpack_scale_set_range(CmpackScale *scale, gdouble min, gdouble max);

G_END_DECLS

#endif
