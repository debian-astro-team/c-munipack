/**************************************************************

cmpack_preview.h (C-Munipack project)
Widget which can draw a text, graph, chart or image
Copyright (C) 2011 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

$Id: cmpack_preview.h,v 1.1 2015/07/12 08:32:30 dmotl Exp $

**************************************************************/

#ifndef CMPACK_PREVIEW_H
#define CMPACK_PREVIEW_H

#include <gtk/gtk.h>

#include "cmpack_widgdefs.h"

G_BEGIN_DECLS

#define CMPACK_TYPE_PREVIEW            (cmpack_preview_get_type ())
#define CMPACK_PREVIEW(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CMPACK_TYPE_PREVIEW, CmpackPreview))
#define CMPACK_PREVIEW_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), CMPACK_TYPE_PREVIEW, CmpackPreviewClass))
#define CMPACK_IS_PREVIEW(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CMPACK_TYPE_PREVIEW))
#define CMPACK_IS_PREVIEW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), CMPACK_TYPE_PREVIEW))
#define CMPACK_PREVIEW_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), CMPACK_TYPE_PREVIEW, CmpackPreviewClass))

typedef struct _CmpackPreview           CmpackPreview;
typedef struct _CmpackPreviewClass      CmpackPreviewClass;

/* Mapping parameters */
typedef struct _CmpackPreviewAxis
{
	gdouble		Min, Max;			// Scroll limits in chart units
	gdouble		ProjMin, ProjMax;	// Scroll limits in projection units
	gdouble		ProjEps;			// Minimum viewfield in projection units
	gboolean	Log, Reverse;		// Logarithmic scale, flip axis direction
} CmpackPreviewAxis;

typedef struct _CmpackPreviewItem
{
	gboolean    valid;				// Is this item valid?
	gboolean	filled;				// Filled marker
	gboolean	topmost;			// Topmost item
	gdouble		xproj, yproj;		// Object's center in projection units
	gdouble		size;				// Position and size of the marker
	CmpackColor	color;
} CmpackPreviewItem; 

/* Chart view */
struct _CmpackPreview
{
	GtkWidget parent;

	/* Private data */
	gboolean		dirty;
	GdkPixmap		*offscreen_pixmap;

	GObject			*model;
	gint			width, height;
	gchar			*text;
	CmpackOrientation image_orientation;

	CmpackPreviewItem *items;
	gint			item_count, item_capacity;

	CmpackPreviewAxis x, y;					// Axis parameters
	gboolean		negative;				// Negative chart

	GdkColor		*int_colors;
};

/* Chart view class */
struct _CmpackPreviewClass
{
  GtkWidgetClass parent_class;
};

GType cmpack_preview_get_type(void) G_GNUC_CONST;

/* -------------------------   PUBLIC FUNCTIONS   --------------------------------- */

/* Create a new chart view (makes a new data model) 
 * Returns floating reference to the chart view
 */
GtkWidget *cmpack_preview_new(void);

/* Create a new chart view and link it to given data model
 * params:
 *	chart_data			- [in] data model
 * Returns floating reference to the chart view
 */
GtkWidget *cmpack_preview_new_with_model(GObject *model);

/* Change the data model which the view is connected to
 * params:
 *	preview				- [in] chart view
 *	chart_data			- [in] new data model
 */
void cmpack_preview_set_model(CmpackPreview *preview, GObject *model);

/* Get the data model with the view is connected to
 * params:
 *	preview				- [in] chart view
 * Returns borrowed reference to the current data model
 */
GObject* cmpack_preview_get_model(CmpackPreview *preview);

/* Change the image model which the view is connected to
 * params:
 *	preview				- [in] chart view
 *	chart_data				- [in] new data model
 */
void cmpack_preview_set_text(CmpackPreview *preview, const char *text);

/* Set mapping direction
 * params:
 *	chart_view				- [in] chart view
 *	flip_x					- [in] direction for x axis
 *	flip_y					- [in] direction for y axis
 */
void cmpack_preview_set_image_orientation(CmpackPreview *view, CmpackOrientation orientation);

/* Set mapping direction
 * params:
 *	preview					- [in] preview
 *	flip_x					- [in] direction for x axis
 *	flip_y					- [in] direction for y axis
 */
void cmpack_preview_set_x_axis(CmpackPreview *view, 
	gboolean log_scale, gboolean reverse, gdouble min, gdouble max, gdouble eps);
void cmpack_preview_set_y_axis(CmpackPreview *view, 
	gboolean log_scale, gboolean reverse, gdouble min, gdouble max, gdouble eps);

/* Set drawing style (valid for chart only)
 * params:
 *	preview					- [in] preview
 *	mode					- [in] new drawing style
*/
void cmpack_preview_set_negative(CmpackPreview *view, gboolean negative);

/* Get drawing style (valid for chart only)
 * params:
 *	preview					- [in] preview
 * Returns TRUE of FALSE
*/
gboolean cmpack_preview_get_negative(CmpackPreview *view);

G_END_DECLS

#endif
