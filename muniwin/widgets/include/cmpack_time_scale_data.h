/**************************************************************

cmpack_timescale_data.h (C-Munipack project)
Object which holds data for a time scale (derived from GtkObject)
Copyright (C) 2011 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

$Id: cmpack_time_scale_data.h,v 1.2 2016/04/24 12:24:07 dmotl Exp $

**************************************************************/
#ifndef CMPACK_TIME_SCALE_DATA_H
#define CMPACK_TIME_SCALE_DATA_H

#include <stdint.h>
#include <gtk/gtk.h>

#include "cmpack_widgdefs.h"

G_BEGIN_DECLS

/* Row data */
typedef struct _CmpackTimeScaleItem {
	gdouble		x;				// Date and time of observation
	CmpackColor	color;			// Color of a bar
	gchar		*tag;			// A text associated with the frame
	gboolean	hidden : 1;		// Display this item
	gboolean	disabled : 1;	// Cannot be selected or activated
	gboolean    topmost : 1;	// Displayed on top of other items
	intptr_t	param;			// Custom value
} CmpackTimeScaleItem;

/* Graph data */
typedef struct _CmpackTimeScaleData {
	GObject		parent;

	/* Private data */ 
	gint		count, capacity;	/* Number of items, allocated space */
	CmpackTimeScaleItem	**items;	/* List of items */
} CmpackTimeScaleData;

typedef struct {
	GObjectClass parent_class;

	/* Signals */
	void (*row_inserted)(CmpackTimeScaleData *model, gint row);
	void (*row_updated)(CmpackTimeScaleData *model, gint row);
	void (*row_deleted)(CmpackTimeScaleData *model, gint row);
	void (*data_cleared)(CmpackTimeScaleData *model);
} CmpackTimeScaleDataClass;

#define CMPACK_TYPE_TIME_SCALE_DATA            (cmpack_time_scale_data_get_type ())
#define CMPACK_TIME_SCALE_DATA(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CMPACK_TYPE_TIME_SCALE_DATA, CmpackTimeScaleData))
#define CMPACK_TIME_SCALE_DATA_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), CMPACK_TYPE_TIME_SCALE_DATA, CmpackTimeScaleDataClass))
#define CMPACK_IS_TIME_SCALE_DATA(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CMPACK_TYPE_TIME_SCALE_DATA))
#define CMPACK_IS_TIME_SCALE_DATA_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), CMPACK_TYPE_TIME_SCALE_DATA))
#define CMPACK_TIME_SCALE_DATA_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), CMPACK_TYPE_TIME_SCALE_DATA, CmpackTimeScaleDataClass)) 

GType cmpack_time_scale_data_get_type(void) G_GNUC_CONST;

/* Create new time scale data */
CmpackTimeScaleData *cmpack_time_scale_data_new(void);

/* Create new time scale data */
CmpackTimeScaleData *cmpack_time_scale_data_new_with_alloc(gint capacity);

/* Clear data and set number of channels and initial capacity */
void cmpack_time_scale_data_alloc(CmpackTimeScaleData *data, gint capacity);

/* Get number of frames */
gint cmpack_time_scale_data_nrows(CmpackTimeScaleData *data);

/* Clear all data */
void cmpack_time_scale_data_clear(CmpackTimeScaleData *data);

/* Append frames to the time scale */
gint cmpack_time_scale_data_add(CmpackTimeScaleData *data, const CmpackTimeScaleItem *item, gsize item_size);

/* Clear all tags */
void cmpack_time_scale_data_clear_tags(CmpackTimeScaleData *data);

/* Set row tag */
void cmpack_time_scale_data_set_tag(CmpackTimeScaleData *data, gint row, const gchar *tag);

/* Set topmost flag */
void cmpack_time_scale_data_set_topmost(CmpackTimeScaleData *data, gint row, gboolean topmost);

/* Set color to given cell */
void cmpack_time_scale_data_set_color(CmpackTimeScaleData *data, gint row, CmpackColor color);

/* Set custom value (integer or pointer) to given cell */
void cmpack_time_scale_data_set_param(CmpackTimeScaleData *data, gint row, intptr_t param);

/* Get frame parameters */
const CmpackTimeScaleItem *cmpack_time_scale_data_get_item(CmpackTimeScaleData *data, gint row);

/* Get a tag associated with a frame */
const gchar *cmpack_time_scale_data_get_tag(CmpackTimeScaleData *data, gint row);

/* Get color of a frame */
CmpackColor cmpack_time_scale_data_get_color(CmpackTimeScaleData *data, gint row);

/* Get custom value associated with a frame */
intptr_t cmpack_time_scale_data_get_param(CmpackTimeScaleData *data, gint row);

/* Get first row with given custom value */
gint cmpack_time_scale_data_find_item(CmpackTimeScaleData *data, intptr_t param);

G_END_DECLS

#endif
