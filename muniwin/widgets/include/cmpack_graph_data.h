/**************************************************************

cmpack_graph_data.h (C-Munipack project)
Object which holds data for a graph (derived from GtkObject)
Copyright (C) 2011 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

$Id: cmpack_graph_data.h,v 1.4 2016/06/02 11:34:30 dmotl Exp $

**************************************************************/
#ifndef CMPACK_GRAPH_DATA_H
#define CMPACK_GRAPH_DATA_H

#include <stdint.h>
#include <gtk/gtk.h>

#include "cmpack_widgdefs.h"

G_BEGIN_DECLS

/* Row data */
typedef struct _CmpackGraphItem {
	gdouble		x, y;			// Position 
	gdouble		error;			// Length of an error bar
	CmpackColor	color;			// Mark color
	gchar		*tag;			// Caption
	gboolean	hidden : 1;		// Display this item
	gboolean	outline : 1;	// Do not fill the circle
	gboolean	disabled : 1;	// Cannot be selected or activated
	gboolean    topmost : 1;	// Displayed on top of other items
	intptr_t	param;			// User data associated with an item
} CmpackGraphItem;

/* Column data */
typedef struct _CmpackGraphColumn {
	CmpackColor	color;			// Mark color
	gboolean	hidden : 1;		// Display this dataset
	gboolean	outline : 1;	// Do not fill the circle
	gboolean	disabled : 1;	// Cannot be selected or activated
	gboolean    topmost : 1;	// Displayed on top of other items
	intptr_t	param;			// User data associated with the dataset
} CmpackGraphColumn;

/* Graph data */
typedef struct _CmpackGraphData {
	GObject		parent;

	/* Private data */ 
	gint		ncol, nrow;			/* Number of columns and rows (allocated space) */
	CmpackGraphItem	**items;		/* List of items */
	CmpackGraphColumn **cols;		/* Columns */
} CmpackGraphData;

typedef struct {
	GObjectClass parent_class;

	/* Signals */
	void (*data_updated)(CmpackGraphData *model, gint first_col, gint last_col, gint first_row, gint last_row);

} CmpackGraphDataClass;

#define CMPACK_TYPE_GRAPH_DATA            (cmpack_graph_data_get_type ())
#define CMPACK_GRAPH_DATA(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CMPACK_TYPE_GRAPH_DATA, CmpackGraphData))
#define CMPACK_GRAPH_DATA_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), CMPACK_TYPE_GRAPH_DATA, CmpackGraphDataClass))
#define CMPACK_IS_GRAPH_DATA(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CMPACK_TYPE_GRAPH_DATA))
#define CMPACK_IS_GRAPH_DATA_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), CMPACK_TYPE_GRAPH_DATA))
#define CMPACK_GRAPH_DATA_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), CMPACK_TYPE_GRAPH_DATA, CmpackGraphDataClass)) 

GType cmpack_graph_data_get_type(void) G_GNUC_CONST;

/* Create new graph data */
CmpackGraphData *cmpack_graph_data_new(gint ncol, gint nrow);

/* Get number of samples per measurement (datasets) */
gint cmpack_graph_data_ncol(CmpackGraphData *data);

/* Get number of data points */
gint cmpack_graph_data_nrow(CmpackGraphData *data);

/* Set all items to invalid state, does not change the allocated space */
void cmpack_graph_data_clear(CmpackGraphData *data);

/* Append row to the graph */
void cmpack_graph_data_set(CmpackGraphData *data, gint col, gint row, const CmpackGraphItem *item, gsize item_size);

/* Clear all tags */
void cmpack_graph_data_clear_tags(CmpackGraphData *data);

/* Set row tag */
void cmpack_graph_data_set_tag(CmpackGraphData *data, gint col, gint row, const gchar *tag);

/* Set topmost flag */
void cmpack_graph_data_set_topmost(CmpackGraphData *data, gint col, gint row, gboolean topmost);

/* Set color to given cell */
void cmpack_graph_data_set_color(CmpackGraphData *data, gint col, gint row, CmpackColor color);

/* Set user data to given cell */
void cmpack_graph_data_set_param(CmpackGraphData *data, gint col, gint row, intptr_t param);

/* Get single value */
gboolean cmpack_graph_data_get_item(CmpackGraphData *data, gint col, gint row, CmpackGraphItem *item, gsize item_size);

/* Get user data */
intptr_t cmpack_graph_data_get_param(CmpackGraphData *data, gint col, gint row);

/* Find item by user data */
gboolean cmpack_graph_data_find_item(CmpackGraphData *data, intptr_t param, gint *col, gint *row);

/* Set default topmost flag for entire dataset */
void cmpack_graph_data_set_dataset_topmost(CmpackGraphData *data, gint col, gboolean topmost);

/* Set default color for entire dataset */
void cmpack_graph_data_set_dataset_color(CmpackGraphData *data, gint col,  CmpackColor color);

/* Set dataset user data */
void cmpack_graph_data_set_dataset_param(CmpackGraphData *data, gint col, intptr_t param);

G_END_DECLS

#endif
