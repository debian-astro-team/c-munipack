/**************************************************************

popup.h (C-Munipack project)
Popup menu wrapper
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef _POPUP_MENU_H
#define _POPUP_MENU_H

#include <gtk/gtk.h>

#include "callback.h"

class CPopupMenu:public CCBObject
{
public:
	enum tPopupMenuItemType
	{
		MB_ITEM = 0,				// Normal item
		MB_SEPARATOR,				// Separator
		MB_END = -1					// Last item
	};

	struct tPopupMenuItem
	{
		tPopupMenuItemType type;	// Menu item type
		int cmd_id;					// Command identifier
		const char *text;			// Displayed text
		const char *icon;			// Icon name
	};

	enum tMessageId
	{
		CB_ACTIVATE = 300,			// Menu item activated (wParam = command id)
		CB_SELECTION_DONE			// Selection done or cancelled
	};

public:
	// Constructor
	CPopupMenu(const tPopupMenuItem *items = NULL);

	// Destructor
	virtual ~CPopupMenu();

	// Create a menu
	void Create(const tPopupMenuItem *items);

	// Enable/disable commands
	void Enable(int cmd, bool enable);

	// Execute the menu
	int Execute(const GdkEventButton *event);

private:
	GMainLoop			*m_Loop;
	struct tPopupMenuItemData	*m_Data;
	GtkWidget			*m_hMenu;
	int					m_Count, m_Command;

	static void activate(GtkMenuItem *menuitem, tPopupMenuItemData *data);
	static void selection_done(GtkWidget *widget, CPopupMenu *pMe);

	void OnActivate(int cmd_id);
	void OnSelectionDone(void);

private:
	// Disable popup menu
	CPopupMenu(const CPopupMenu&);
};

#endif
