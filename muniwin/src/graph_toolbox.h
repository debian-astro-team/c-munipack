/**************************************************************

graph_toolbox.h (C-Munipack project)
A widget that shows a toolbox for a graph
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_GRAPH_TOOLBOX_H
#define CMPACK_GRAPH_TOOLBOX_H

#include <gtk/gtk.h>

#include "infobox.h"

class CGraphToolBox:public CInfoBox
{
public:
	// Constructor
	CGraphToolBox();

	// Destructor
	virtual ~CGraphToolBox(void);
	
	// Initialization start
	void BeginReset();

	// Add channel
	void AddChannel(gint channel, const gchar *caption, const GdkColor *color);

	// Initialization done
	void EndReset();

	// Set channel check state
	void SetState(gint channel, gboolean checked);

private:
	GtkWidget *m_Table;
	GtkListStore *m_Model;
	GtkTreeViewColumn *m_Cols[2];

	void OnRowActivated(GtkTreeView *tree_view, GtkTreePath *path);
	void OnCellToggled(GtkCellRendererToggle *cell, char *path);

	static void row_activated(GtkTreeView *treeview, GtkTreePath *path, GtkTreeViewColumn *column, CGraphToolBox *pMe);
	static void cell_toggled(GtkCellRendererToggle *cell, char *path, CGraphToolBox *pMe);
};

#endif
