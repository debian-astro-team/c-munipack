/**************************************************************

preview_dlg.h (C-Munipack project)
The preview dialog
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_PREVIEW_DLG_H
#define CMPACK_PREVIEW_DLG_H

#include <gtk/gtk.h>

#include "cmpack_widgets.h"
#include "phot_class.h"
#include "image_class.h"
#include "progress_dlg.h"
#include "menubar.h"
#include "popup.h"
#include "infobox.h"
#include "qphot.h"

class CFrameDlg
{
public:
	// Constructor
	CFrameDlg(void);

	// Destructor
	virtual ~CFrameDlg();

	// Display non-modal dialog
	bool LoadFrame(GtkTreePath *path);

	// Show the dialog
	void Show(void);

	// Close the window
	void Close(void);

	// Get row reference to a frame
	GtkTreeRowReference *Frame(void) const
	{ return m_pFile; }

	// Environment changed, reload settings
	void EnvironmentChanged(void);

private:
	enum tDispMode {
		DISP_EMPTY,
		DISP_IMAGE,
		DISP_CHART,
		DISP_FULL,
		DISP_TABLE,
		DISP_MODE_COUNT
	};

	enum tInfoMode {
		INFO_NONE,
		INFO_OBJECT,
		INFO_PHOTOMETRY,
		INFO_GRAYSCALE,
		INFO_PROFILE,
		INFO_HISTOGRAM
	};

	GtkWidget			*m_Status, *m_pDlg;
	GtkWidget			*m_ChartScrWnd, *m_ChartView, *m_TableScrWnd, *m_TableView;
	GtkWidget			*m_FrameEntry, *m_AperLabel, *m_AperCombo;
	GtkToolItem			*m_AperSeparator;
	GtkToolItem			*m_NextBtn, *m_PrevBtn, *m_ZoomFit, *m_ZoomIn, *m_ZoomOut;
	GtkListStore		*m_Apertures;
	CMenuBar			m_Menu;
	CPhot				m_Phot;
	CImage				*m_Image;
	CWcs				*m_Wcs;
	CApertures			m_Aper;
	CmpackChartData		*m_ChartData;
	CmpackImageData		*m_ImageData;
	GtkTreeModel		*m_TableData;
	GtkTreeRowReference *m_pFile;
	CTextBox			m_InfoBox;
	CScaleBox			m_Scale;
	CProfileBox			m_Profile;
	CHistogramBox		m_Histogram;
	CQuickPhotBox		m_QPhot;
	tDispMode			m_DispMode;
	tInfoMode			m_InfoMode;
	int					m_SortColumnId;
	GtkSortType			m_SortType;
	GtkTreeViewColumn	*m_SortCol;
	int					m_ApertureIndex, m_SelectedRow, m_SelectedObjId, m_SelectedRefId;
	int					m_MovingTarget;
	bool				m_Rulers, m_ShowOrig, m_DontClose;
	bool				m_Updating, m_UpdatePos, m_MouseOnProfile, m_MouseOnHistogram;
	bool				m_UpdateZoom, m_Pseudocolors, m_Negative, m_RowsUpward, m_ShowTrace;
	gdouble				m_LastPosX, m_LastPosY, m_SelectedRefX, m_SelectedRefY;
	gint				m_LastFocus, m_FrameState;
	guint				m_TimerId;
	gint				m_StatusCtx, m_StatusMsg;
	gint				m_ApLayerId, m_ApObjectId[4];
	gint				m_TdLayerId, m_TdObjectId[2];
	GtkTreePath			*m_SelectedPath;
	CTrackingData		m_TrackData;
	CPopupMenu			m_ObjectMenu, m_ChartMenu;

	void UpdateTableHeader(void);
	void UpdateImage(void);
	void UpdateChart(void);
	void UpdateStatus(void);
	void UpdateZoom(void);
	void UpdateApertures(void);
	void UpdateInfoBox(void);
	void SetDisplayMode(tDispMode mode);
	void SetInfoMode(tInfoMode mode);
	void SetSortMode(int column, GtkSortType type);
	void ReloadImage(void);
	void ReloadChart(void);
	void UpdateControls(void);
	void ShowProperties(void);
	void GoToFirstFrame(void);
	void GoToLastFrame(void);
	void GoToPreviousFrame(void);
	void GoToNextFrame(void);
	void RemoveFromProject(void);
	void SetStatus(const char *text);
	void ShowAperture(double x, double y, double aperture, double fwhm, double sky_in, double sky_out);
	void HideAperture(void);
	void ShowTrack(const CTrackingData &data);
	void HideTrack(void);
	void Export(void);
	void CopyWcsCoordinatesFromChart(int row);
	void CopyWcsCoordinatesFromTable(GtkTreePath *path);
	void CopyWcsCoordinates(double lng, double lat);

	void OnCommand(int cmd_id);
	void OnButtonClicked(GtkWidget *pBtn);
	void OnRowDeleted(GtkTreeModel *tree_model, GtkTreePath  *path);
	void OnChartItemActivated(gint row);
	void OnSelectionChanged(void);
	void OnProfileChanged(void);
	void OnInfoBoxClosed(CInfoBox *pBox);
	void OnLButtonClick(guint32 timestamp);
	void OnTableColumnClicked(GtkTreeViewColumn *pCol);
	void OnTableRowActivated(GtkTreeView *treeview, GtkTreePath *path);
	void OnObjectMenu(GdkEventButton *event, gint item);
	void OnObjectMenu(GdkEventButton *event, GtkTreePath *path);
	void OnChartMenu(GdkEventButton *event);

	static void destroy(GtkObject *pWnd, CFrameDlg *pDlg);
	static void button_clicked(GtkWidget *pButton, CFrameDlg *pDlg);
	static gboolean button_press_event(GtkWidget *widget, GdkEventButton *event, CFrameDlg *pMe);
	static void row_deleted(GtkTreeModel *tree_model, GtkTreePath *path, CFrameDlg *pDlg);
	static void mouse_moved(GtkWidget *pChart, CFrameDlg *pMe);
	static void mouse_left(GtkWidget *pChart, CFrameDlg *pMe);
	static void zoom_changed(GtkWidget *pChart, CFrameDlg *pMe);
	static void chart_item_activated(GtkWidget *pChart, gint row, CFrameDlg *pMe);
	static void selection_changed(GtkWidget *pChart, CFrameDlg *pMe);
	static void profile_changed(GtkWidget *pChart, CFrameDlg *pMe);
	static gboolean timer_cb(CFrameDlg *pMe);
	static void MenuCallback(CCBObject *sender, int message, int wparam, void* lparam, void* cb_data);
	static void InfoBoxCallback(CCBObject *sender, int message, int wparam, void* lparam, void* cb_data);
	static void table_column_clicked(GtkTreeViewColumn *pCol, CFrameDlg *pMe);
	static void table_row_activated(GtkTreeView *treeview, GtkTreePath *path, GtkTreeViewColumn *column, CFrameDlg *pMe);
};

#endif
