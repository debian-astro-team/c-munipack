/**************************************************************

config.h (C-Munipack project)
Configuration file interface
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef MUNIWIN_CONFIGURATION_H
#define MUNIWIN_CONFIGURATION_H

#include <float.h>

#include "helper_classes.h"
#include "profile.h"

#define RECENT_GROUP_PROJECT		"C-Munipack project"

#define FILE_EXTENSION_PROJECT		"cmpack"
#define FILE_EXTENSION_PROFILE		"cfg"
#define FILE_EXTENSION_CATALOG		"xml"
#define FILE_EXTENSION_FITS			"fts"
#define FILE_EXTENSION_PHOTOMETRY	"pht"

class CConfig
{
public:
	// Parameter identifiers
	enum tParameter
	{
		NEGATIVE_CHARTS,
		DISABLE_PREVIEWS,
		EVENT_SOUNDS,
		DEBUG_OUTPUTS,
		FILE_LIST_COLUMNS,
		SEARCH_GCVS,
		GCVS_PATH,
		SEARCH_NSV,
		NSV_PATH,
		SEARCH_NSVS,
		NSVS_PATH,
		USER_PROFILES,
		ROWS_UPWARD,
		NO_MAXIMIZE,
		SEARCH_VSX,
		VSX_PATH,
		POSITIVE_WEST,
		EndOfParameters
	};

	static void Init(void);
	static void Free(void);
	static void Flush(void);
	static void Lock(void);
	static void Unlock(void);

	static void SetStr(tParameter param, const gchar *val);
	static void SetInt(tParameter param, const int val);
	static void SetBool(tParameter param, bool val);

	static gchar *GetStr(tParameter param);
	static int GetInt(tParameter param);
	static bool GetBool(tParameter param);

	static bool GetDefaultBool(tParameter param);
	static int GetDefaultInt(tParameter param);
	static const gchar *GetDefaultStr(tParameter param);

	static void SetStr(const char *group, const char *key, const char *val);
	static void SetDbl(const char *group, const char *key, const double val);
	static void SetInt(const char *group, const char *key, const int val);
	static void SetBool(const char *group, const char *key, bool val);

	static bool TryGetStr(const char *group, const char *key, char **value);
	static bool TryGetDbl(const char *group, const char *key, double *value);
	static bool TryGetInt(const char *group, const char *key, int *value);
	static bool TryGetBool(const char *group, const char *key, bool *value);

	static char *GetStr(const char *group, const char *key, const char *defval = "");
	static double GetDbl(const char *group, const char *key, double defval = 0.0,
		double minval = -DBL_MAX, double maxval = DBL_MAX);
	static int GetInt(const char *group, const char *key, int defval = 0,
		int minval = INT32_MIN, int maxval = INT32_MAX);
	static bool GetBool(const char *group, const char *key, bool defval = false);

	static void AddProjectToRecentList(tProjectType type, const gchar *fpath);
	static void RenameProjectInRecentList(tProjectType type, const gchar *oldpath, const gchar *newpath);
	static void AddFileToRecentList(tFileType type, const gchar *fpath);
	
private:
	static void Load(void);
	static void Save(void);

	static GKeyFile	*ms_KeyFile;
	static bool ms_Modified;
};

#endif
