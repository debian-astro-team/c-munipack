/**************************************************************

main_dlg.h (C-Munipack project)
Main dialog
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_COLOR_CHOOSER_DLG_H
#define CMPACK_COLOR_CHOOSER_DLG_H

#include <gtk/gtk.h>

#include "popup.h"
#include "menubar.h"
#include "cmpack_widgets.h"
#include "utils.h"

/* Number of elements in the custom palatte */
#define COLOR_CHOOSER_PALETTE_WIDTH  8
#define COLOR_CHOOSER_PALETTE_HEIGHT 6

class CColorChooserDlg
{
public:
	CColorChooserDlg(GtkWindow *pParent, const gchar *title);
	virtual ~CColorChooserDlg();

	bool Execute(const GdkColor &old_color, GdkColor &new_color);

private:
	GtkWidget	*m_pDlg;
	GtkWidget	*custom_palette [COLOR_CHOOSER_PALETTE_WIDTH][COLOR_CHOOSER_PALETTE_HEIGHT]; 
	GtkWidget	*cur_sample;

	// Create a palette entry
	GtkWidget* CreateEntry(void);

	// Set focus on an entry
	void SetSelectedEntry(int x, int y);
	void OnDrawEntry (GtkWidget *drawing_area, const GdkRectangle &area);
	void update_palette(void);
	
	static void set_focus_line_attributes (GtkWidget *drawing_area, cairo_t *cr, gint *focus_width);
	static void get_palette_color(GtkWidget *drawing_area, GdkColor &color);
	static void palette_get_color(GtkWidget *drawing_area, gdouble *color);
	static void palette_set_color(GtkWidget *drawing_area, gdouble *color);
	static void set_palette_color(GtkWidget *drawing_area, const GdkColor &color);

	static gboolean palette_expose(GtkWidget *drawing_area, GdkEventExpose *event, CColorChooserDlg *pMe);
	static gboolean palette_enter(GtkWidget *drawing_area, GdkEventCrossing *event, CColorChooserDlg *pMe);
	static gboolean palette_leave(GtkWidget *drawing_area, GdkEventCrossing *event, CColorChooserDlg *pMe);
	static gboolean palette_press(GtkWidget *drawing_area, GdkEventButton *event, CColorChooserDlg *pMe);
	static gboolean palette_release(GtkWidget *drawing_area, GdkEventButton *event, CColorChooserDlg *pMe);
};

#endif
