/**************************************************************

matching_mt.h (C-Munipack project)
The 'Match stars' tab for tracking moving targets
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_MATCHING_MT_H
#define CMPACK_MATCHING_MT_H

#include <gtk/gtk.h>

#include "cmpack_widgets.h"
#include "catfile_class.h"
#include "phot_class.h"
#include "ccdfile_class.h"
#include "image_class.h"
#include "preview.h"
#include "matching_dlg.h"
#include "editselections_dlg.h"

class CMatchingMTTab:public CMatchingTab
{
public:
	CMatchingMTTab(CMatchingDlg *pParent);
	~CMatchingMTTab();

	virtual bool OnInitDialog(tInit init, int *frame_id, char **filepath, GError **error);
	virtual bool OnResponseDialog(gint response_id, GError **error);
	virtual int ProcessFiles(GList *files, int &in_files, int &out_files, CProgressDlg *sender);
	virtual void OnTabShow(void);
	virtual void OnTabHide(void);

private:
	// Display mode
	enum tDisplayMode {
		DISPLAY_IMAGE,
		DISPLAY_CHART
	};

	// Frame icons
	enum tIcon {
		ICO_KEY_FIRST, 
		ICO_KEY_OTHER,
		ICON_COUNT
	};
	
	GtkWidget		*m_FrameView, *m_FrameBox, *m_Preview;
	GtkWidget		*m_Label1, *m_ResetBtn, *m_AddBtn, *m_RemoveBtn, *m_SetRefBtn, *m_ObjectBtn;
	GtkWidget		*m_FirstKeyIcon, *m_FirstKeyLabel, *m_OtherKeyIcon, *m_OtherKeyLabel;
	GtkWidget		*m_LegendLabel, *m_TargetLabel, *m_TargetIcon;
	GtkToolItem		*m_ShowImage, *m_ShowChart;
	int				m_SelectedFrame, m_ObjectID, m_ReferenceFrame, m_KeyCount;
	tDisplayMode	m_DisplayMode;
	GSList			*m_FrameCols;
	GtkListStore	*m_Frames;
	CmpackChartData	*m_ChartData;
	CmpackImageData	*m_ImageData;
	gchar			*m_SelectionName;
	bool			m_Updating, m_Negative, m_RowsUpward;
	GdkPixbuf		*m_Icons[ICON_COUNT];

	void UpdateImage(CImage *img);
	void UpdateChart(CPhot &pht);
	void SelectionChanged(GtkTreeSelection *selection);
	void ReadFrames(void);
	void UpdatePreview(bool force_update = false);
	void UpdateControls(void);

	void ResetAll(void);
	void SetKeyFrame(void);
	void ResetKeyFrame(void);
	void SetRefFrame(void);
	void ChangeTarget(void);

	void OnButtonClicked(GtkWidget *pBtn);

	static void selection_changed(GtkTreeSelection *pWidget, CMatchingMTTab *pMe);
	static void button_clicked(GtkWidget *pButton, CMatchingMTTab *pDlg);

	static void GetIcon(GtkTreeViewColumn *tree_column, GtkCellRenderer *cell,
		GtkTreeModel *tree_model, GtkTreeIter *iter, gpointer data);
};

class CChooseTargetDlg
{
public:
	CChooseTargetDlg(GtkWindow *pParent);
	~CChooseTargetDlg();

	bool Execute(GtkTreePath *path, int &objectID, GError **error);

private:
	// Display modes
	enum tDisplayMode {
		DISPLAY_IMAGE,
		DISPLAY_CHART,
		DISPLAY_FULL
	};

	GtkWidget		*m_pDlg, *m_Chart, *m_Status;
	GtkToolItem		*m_ShowImage, *m_ShowChart, *m_ShowMixed;
	GtkToolItem		*m_ZoomIn, *m_ZoomOut, *m_ZoomFit;

	tDisplayMode	m_DisplayMode;
	int				m_ObjectID;
	bool			m_Negative, m_RowsUpward, m_UpdatePos;
	gint			m_StatusCtx, m_StatusMsg, m_LastFocus;
	gdouble			m_LastPosX, m_LastPosY;
	guint			m_TimerId;
	CPhot			m_Phot;
	CCatalog		m_Catalog;
	CImage			*m_Image;
	CWcs			*m_Wcs;
	CmpackImageData	*m_ImageData;
	CmpackChartData	*m_ChartData;

	// Set status text
	void SetStatus(const char *text);
	void UpdateStatus();
	void UpdateImage(void);
	void UpdateChart(void);
	void UpdateControls(void);
	void UpdateObject(int row, int star_id);
	void UpdateAll(void);

	void OnChartItemActivated(gint item);
	bool OnResponseDialog(gint response_id);
	void OnButtonClicked(GtkWidget *pButton);

	static void response_dialog(GtkDialog *pDlg, gint response_id, CChooseTargetDlg *pMe);
	static void button_clicked(GtkWidget *pButton, CChooseTargetDlg *pDlg);
	static void chart_item_activated(GtkWidget *pChart, gint item, CChooseTargetDlg *pMe);
	static void chart_mouse_moved(GtkWidget *button, CChooseTargetDlg *pDlg);
	static void chart_mouse_left(GtkWidget *button, CChooseTargetDlg *pDlg);
	static gboolean timer_cb(CChooseTargetDlg *pMe);
};

#endif
