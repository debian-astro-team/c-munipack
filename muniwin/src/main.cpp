/**************************************************************

main.cpp (C-Munipack project)
Main module
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifdef _MSC_VER
#include <windows.h>
#endif

#include <stdlib.h>
#include <locale.h>
#include <gtk/gtk.h>
#include <cmunipack.h>
#include <fitsio.h>

#include "main_dlg.h"
#include "messages_dlg.h"
#include "configuration.h"
#include "configuration.h"
#include "sound.h"
#include "main.h"
#include "config.h"

CMainWindow *g_MainWnd = NULL;
CProject *g_Project = NULL;
const gchar *g_AppTitle = "Muniwin";
GQuark g_AppError = 0;

static GStaticRecMutex gs_lock = G_STATIC_REC_MUTEX_INIT;

static void enter_lock(void)
{
	g_static_rec_mutex_lock(&gs_lock);
}

static void leave_lock(void)
{
	g_static_rec_mutex_unlock(&gs_lock);
}

int main (int argc, char *argv[])
{
	const char *fpath = NULL;

	setlocale(LC_ALL, "C");

	// Init GTK
	g_thread_init(NULL);
	gdk_threads_set_lock_functions(enter_lock, leave_lock);
	gdk_threads_init();
	gdk_threads_enter();
	gtk_disable_setlocale();
	gtk_init(&argc, &argv);

	// Init FITSIO library
	fits_init_cfitsio();

	// Init C-Munipack
	cmpack_init();

	// Make global data
	g_AppError = g_quark_from_static_string ("muniwin-error-quark"); 
	CConfig::Init();
	CMessagesDlg::InitBuffer();
	ProfilesInitGlobals();
	g_Project = new CProject();
	if (argc>=2)
		fpath = argv[1];
	g_MainWnd = new CMainWindow(fpath);
	SoundInit(&argc, &argv);

	// Main event loop
	gtk_main();

	// Clean up
	SoundCleanup();
	delete g_MainWnd;
	delete g_Project;
	CMessagesDlg::FreeBuffer();
	CConfig::Free();
	ProfilesFreeGlobals();
	cmpack_cleanup();
	gdk_threads_leave();
	return 0;
}

#ifdef _MSC_VER

int __stdcall WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, char*, int nShowCmd)
{
	return main(0, NULL);
}
#endif
