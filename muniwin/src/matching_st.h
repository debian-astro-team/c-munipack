/**************************************************************

matching_st.h (C-Munipack project)
The 'Match stars' tab for stationary targets
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_MATCHING_ST_H
#define CMPACK_MATCHING_ST_H

#include <gtk/gtk.h>

#include "cmpack_widgets.h"
#include "catfile_class.h"
#include "phot_class.h"
#include "ccdfile_class.h"
#include "image_class.h"
#include "preview.h"
#include "matching_dlg.h"

class CMatchingSTTab:public CMatchingTab
{
public:
	CMatchingSTTab(CMatchingDlg *pParent);
	~CMatchingSTTab();

	void SelectFrame(int *frame_id);
	void SelectFile(char **filepath);

	virtual void OnTabShow(void);
	virtual void OnTabHide(void);
	virtual bool OnInitDialog(tInit init, int *frame_id, char **filepath, GError **error);
	virtual bool OnResponseDialog(gint response_id, GError **error);
	virtual int ProcessFiles(GList *files, int &in_files, int &out_files, CProgressDlg *sender);
	
private:
	// Selection modes
	enum tSelectMode {
		REFERENCE_FRAME,
		CATALOG_FILE
	};

	// Display mode
	enum tDisplayMode {
		DISPLAY_IMAGE,
		DISPLAY_CHART
	};

	GtkWidget		*m_FrameBtn, *m_CatalogBtn, *m_FrameBox, *m_CatalogBox;
	GtkWidget		*m_FrameView, *m_UseFrame, *m_CatalogView, *m_SelLabel;
	GtkWidget		*m_PathEntry, *m_PathLabel, *m_PathBtn, *m_Preview;
	GtkToolItem		*m_ShowImage, *m_ShowChart;
	int				m_FrameID;
	tSelectMode		m_SelectMode;
	tDisplayMode	m_DMFrame, m_DMCatalog;
	int				m_FrameSort;
	GSList			*m_FrameCols, *m_CatalogCols;
	GtkListStore	*m_Frames, *m_Catalogs;
	CmpackChartData	*m_ChartData;
	CmpackImageData	*m_ImageData;
	gchar			*m_CatFile, *m_SelectionName;
	bool			m_Updating, m_Negative, m_RowsUpward;

	void UpdateImage(CImage *img);
	void UpdateChart(CPhot &pht);
	void UpdateChart(CCatalog &cat);
	void ChangeCatalogPath(void);
	void SetSelectMode(tSelectMode mode);
	void SetDisplayMode(tDisplayMode mode);
	tDisplayMode GetDisplayMode(void) const;
	void SetSortMode(int column);
	void SelectionChanged(GtkTreeSelection *selection);
	void ReadFrames(bool all_frames);
	void ReadCatalogs();
	void UpdatePreview(bool force_update = false);
	void UpdateControls(void);

	void OnButtonClicked(GtkWidget *pBtn);
	void OnFrameColumnClicked(GtkTreeViewColumn *pCol);

	static void selection_changed(GtkTreeSelection *pWidget, CMatchingSTTab *pMe);
	static void button_clicked(GtkWidget *pButton, CMatchingSTTab *pDlg);
	static void frame_column_clicked(GtkTreeViewColumn *pCol, CMatchingSTTab *pDlg);
};

#endif
