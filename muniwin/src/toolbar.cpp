/**************************************************************

toolbar.cpp (C-Munipack project)
Tool bar wrapper
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#include <stdlib.h>

#include "utils.h"
#include "toolbar.h"

struct tToolBtnData
{
	bool separator;		// Button type
	CToolBar *tbar;
	GtkToolItem *btn;
	int cmd_id;
	bool visible;
};

// Default constructor
CToolBar::CToolBar():m_Count(0), m_hBar(NULL), m_Data(NULL)
{
}

// Destructor
CToolBar::~CToolBar()
{
	if (m_Data)
		g_free(m_Data);
}

// Create and initialize the m_hBar
void CToolBar::Create(const tToolBtn *btns)
{
	m_hBar = gtk_toolbar_new();
	gtk_toolbar_set_style (GTK_TOOLBAR (m_hBar), GTK_TOOLBAR_ICONS);
	gtk_toolbar_set_orientation (GTK_TOOLBAR (m_hBar), GTK_ORIENTATION_HORIZONTAL);

	if (btns) {
		int count = 0;
		for (int i=0; btns[i].type!=TB_END; i++)
			count++;
		if (count>0) 
			m_Data = (tToolBtnData*) g_realloc(m_Data, count*sizeof(tToolBtnData));
		int j = 0;
		for (int i=0; btns[i].type!=TB_END; i++) {
			tToolBtnData *data = m_Data+j;
			switch (btns[i].type)
			{
			case TB_PUSHBUTTON:
				{
					char *icon = get_icon_file(btns[i].icon);
					data->btn = gtk_tool_button_new(gtk_image_new_from_file(icon), btns[i].text);
					gtk_widget_set_tooltip_text(GTK_WIDGET(data->btn), btns[i].ttip);
					g_signal_connect(G_OBJECT(data->btn), "clicked", G_CALLBACK(push_event), &m_Data[i]);
					g_free(icon);
				}
				break;
			case TB_SEPARATOR:
				data->btn = gtk_separator_tool_item_new();
				break;
			default:
				break;
			}
			if (data->btn) {
				gtk_toolbar_insert(GTK_TOOLBAR(m_hBar), data->btn, -1);
				data->separator = btns[i].type == TB_SEPARATOR;
				data->tbar = this;
				data->cmd_id = btns[i].cmd_id;
				data->visible = true;
				j++;
			}
		}
		m_Count = j;
	}
	gtk_toolbar_set_tooltips (GTK_TOOLBAR (m_hBar), true);
	gtk_widget_show (m_hBar);
}

void CToolBar::push_event(GtkWidget *widget, gpointer data)
{
	((tToolBtnData*)data)->tbar->OnPushEvent(((tToolBtnData*)data)->cmd_id);
}

void CToolBar::OnPushEvent(int cmd_id)
{
	Callback(CB_ACTIVATE, cmd_id);
}

void CToolBar::Show(int cmd_id, bool show)
{
	bool changed = false;

	for (int i=0; i<m_Count; i++) {
		tToolBtnData *data = m_Data+i;
		if (!data->separator && data->cmd_id == cmd_id) {
			if (show) {
				if (!data->visible) {
					gtk_widget_show(GTK_WIDGET(data->btn));
					data->visible = true;
					changed = true;
				}
			} else {
				if (data->visible) {
					gtk_widget_hide(GTK_WIDGET(data->btn));
					data->visible = false;
					changed = true;
				}
			}
		}
	}

	if (changed) {
		tToolBtnData *last_item = NULL, *last_sep = NULL;
		for (int i=0; i<m_Count; i++) {
			tToolBtnData *data = m_Data+i;
			if (!data->separator) {
				if (data->visible) {
					if (last_item && last_sep) {
						gtk_widget_show(GTK_WIDGET(last_sep->btn));
						last_sep->visible = true;
						last_sep = NULL;
					}
					last_item = data;
				}
			} else {
				if (data->visible) {
					gtk_widget_hide(GTK_WIDGET(data->btn));
					data->visible = false;
				}
				last_sep = data;
			}
		}
	}
}

void CToolBar::Enable(int cmd_id, bool enable)
{
	int i;

	for (i=0; i<m_Count; i++) {
		if (m_Data[i].cmd_id == cmd_id) {
			gtk_widget_set_sensitive(GTK_WIDGET(m_Data[i].btn), enable);
			return;
		}
	}
}

void CToolBar::SetStyle(GtkToolbarStyle style)
{
	gtk_toolbar_set_style(GTK_TOOLBAR(m_hBar), style);
}
