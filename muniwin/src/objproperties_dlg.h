/**************************************************************

objproperties_dlg.h (C-Munipack project)
The 'Plot object properties' dialog
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_OBJPROPERTIES_DLG_H
#define CMPACK_OBJPROPERTIES_DLG_H

#include <gtk/gtk.h>

#include "helper_classes.h"
#include "frameset_class.h"
#include "output_dlg.h"
#include "menubar.h"
#include "popup.h"
#include "table_class.h"
#include "infobox.h"
#include "measurement.h"
#include "graph_toolbox.h"

// 
// Initial dialog
//
class CMakeObjPropertiesDlg
{
public:
	CMakeObjPropertiesDlg(GtkWindow *pParent);
	~CMakeObjPropertiesDlg();

	void Execute(void);

private:
	GtkWindow		*m_pParent;
	GtkWidget		*m_pDlg, *m_AllBtn, *m_SelBtn;

	bool OnResponseDialog(gint response_id);
	bool OnCloseQuery(void);

	static void response_dialog(GtkDialog *pDlg, gint response_id, CMakeObjPropertiesDlg *pMe);
};


//
// Object properties
//
class CObjPropertiesDlg:public COutputCurveDlg
{
public:
	// Constructor
	CObjPropertiesDlg(void);

	// Destructor
	virtual ~CObjPropertiesDlg();

	// Initialize the dialog
	bool Make(GtkWindow *parent, bool selected_files);

protected:
	// Get icon name
	virtual const char *GetIconName(void) { return "objproperties"; }

	// Frame set was updated
	virtual void OnFrameSetChanged(void);

	// Get list of GtkTreePaths of all selected frames
	virtual GList *GetSelectedFrames(void);

	// Get path to the first selected frame
	virtual GtkTreePath *GetSelectedFrame(void);

private:
	enum tDisplayMode {
		DISPLAY_GRAPH,
		DISPLAY_TABLE
	};

	enum tInfoMode {
		INFO_NONE,
		INFO_STATISTICS,
		INFO_MEASUREMENT
	};

	CMenuBar		m_Menu;
	GtkWidget		*m_ZoomLabel, *m_XLabel, *m_DCombo, *m_YLabel, *m_YCombo;
	GtkWidget		*m_ALabel, *m_ACombo, *m_OLabel, *m_OEntry;
	GtkToolItem		*m_SelectObject;
	GtkWidget		*m_GraphScrWnd, *m_GraphView, *m_TableScrWnd, *m_TableView;
	GtkListStore	*m_DateFormats, *m_YChannels, *m_Apertures;
	GtkToolItem		*m_ZoomFit, *m_ZoomIn, *m_ZoomOut; 
	bool			m_UpdatePos, m_LastPosValid, m_ShowErrors, m_ShowGrid;
	double			m_LastPosX, m_LastPosY;
	tIndex			m_LastFocus;
	int				m_InFiles, m_OutFiles;
	tDisplayMode	m_DispMode;
	tDateFormat		m_DateFormat;
	int				m_ApertureIndex, m_ObjectId, m_ChannelX, m_ChannelY, m_MovingTarget;
	CTable			*m_Table;
	gint			m_TimerId;
	CPopupMenu		m_GraphMenu;
	CTextBox		m_InfoBox;
	tInfoMode		m_InfoMode;
	CMeasurementBox	m_MeasBox;
	CmpackGraphData	*m_GraphData;
	GtkTreeModel	*m_TableData;
	CApertures		m_AperList;
	bool			m_ShowToolBox;
	CGraphToolBox	m_ToolBox;

	// Rebuild frame set
	bool RebuildData(GtkWindow *parent);

	// Update the table of object properties
	bool UpdateCurve(GtkWindow *parent);

	void UpdateApertures(void);
	void UpdateObject(void);
	void UpdateChannels(void);
	void UpdateGraphTable(gboolean autozoom_x, gboolean autozoom_y);
	void UpdateStatus(void);
	void UpdateControls(void);
	void UpdateTools(void);
	void SelectObject(void);
	void SetData(int column);
	void SaveData(void);
	void Export(void);
	void SetDisplayMode(tDisplayMode mode);
	void SetInfoMode(tInfoMode mode);
	void ShowToolBoxMode(bool show);
	void PrintValue(char *buf, double val, const CChannel *channel);
	void PrintKeyValue(char *buf, double val, const CChannel *channel);

	void OnCommand(int cmd_id);
	void OnButtonClicked(GtkWidget *pBtn);
	void OnEntryChanged(GtkWidget *pButton);
	void OnContextMenu(GtkWidget *widget, GdkEventButton *event);
	void OnSelectionChanged(void);
	void OnInfoBoxClosed(void);
	void OnToolBoxClosed(void);

	static void button_clicked(GtkWidget *pButton, CObjPropertiesDlg *pDlg);
	static void entry_changed(GtkWidget *pButton, CObjPropertiesDlg *pMe);
	static void mouse_moved(GtkWidget *pGraph, CObjPropertiesDlg *pMe);
	static void mouse_left(GtkWidget *pGraph, CObjPropertiesDlg *pMe);
	static gint button_press_event(GtkWidget *widget, GdkEventButton *event, CObjPropertiesDlg *pMe);
	static gboolean timer_cb(CObjPropertiesDlg *pMe);
	static void selection_changed(GtkWidget *pChart, CObjPropertiesDlg *pMe);
	static void MenuCallback(CCBObject *sender, int message, int wparam, void* lparam, void* cb_data);
	static void InfoBoxCallback(CCBObject *sender, int message, int wparam, void* lparam, void* cb_data);
	static void ToolBoxCallback(CCBObject *sender, int message, int wparam, void* lparam, void* cb_data);
};

//
// Save light curve
//
class CSaveObjPropertiesDlg
{
public:
	// Constructor
	CSaveObjPropertiesDlg(GtkWindow *pParent);

	// Destructor
	virtual ~CSaveObjPropertiesDlg();

	// Execute the dialog
	bool Execute(const CTable &table, int channel, int objectId);

private:
	enum tFileType {
		TYPE_MUNIPACK,
		TYPE_TEXT,
		TYPE_CSV,
		TYPE_N_ITEMS			// Number of file formats
	};

	struct tOptions {
		bool frame_id, all_values;
		bool skip_invalid, header;
	};
	
	GtkWindow		*m_pParent;
	GtkWidget		*m_pDlg, *m_TypeCombo, *m_VCCombo;
	GtkWidget		*m_Header, *m_SkipInvalid, *m_FrameIds, *m_AllValues;
	GtkListStore	*m_FileTypes, *m_Channels;
	CTable			m_Table;
	bool			m_Updating;
	tFileType		m_FileType;
	int				m_SelectedY;
	tOptions		m_Options[TYPE_N_ITEMS];
	
	void UpdateControls(void);

	bool OnResponseDialog(gint response_id);
	void OnSelectionChanged(GtkComboBox *pWidget);
	void OnTypeChanged(void);
	void OnButtonToggled(GtkToggleButton *widget);
	
	static tFileType StrToFileType(const gchar *str);
	static const gchar *FileTypeToStr(tFileType type);

	static void response_dialog(GtkWidget *widget, gint response_id, CSaveObjPropertiesDlg *pMe);
	static void selection_changed(GtkComboBox *pWidget, CSaveObjPropertiesDlg *pMe);
	static void button_toggled(GtkToggleButton *widget, CSaveObjPropertiesDlg *user_data);
};

//
// Save light curve
//
class CSelectObjectDlg
{
public:
	// Constructor
	CSelectObjectDlg(GtkWindow *pParent);

	// Destructor
	virtual ~CSelectObjectDlg();

	// Execute the dialog. Returns id of the selected object
	// or zero if the dialog was cancelled.
	int Execute(int defaultId);

private:
	// Display modes
	enum tDisplayMode {
		DISPLAY_IMAGE,
		DISPLAY_CHART,
		DISPLAY_FULL
	};

	GtkWindow		*m_pParent;
	GtkWidget		*m_pDlg, *m_Chart, *m_Status;
	GtkToolItem		*m_ShowImage, *m_ShowChart, *m_ShowMixed;
	GtkToolItem		*m_ZoomIn, *m_ZoomOut, *m_ZoomFit;
	tDisplayMode	m_DisplayMode;
	CTags			*m_Tags;
	CPhot			m_Phot;
	CCatalog		m_Catalog;
	CImage			*m_Image;
	CWcs			*m_Wcs;
	CmpackImageData	*m_ImageData;
	CmpackChartData	*m_ChartData;
	CPopupMenu		m_SelectMenu, m_ContextMenu;
	int				m_ObjectId, m_MovingTarget;
	gint			m_StatusCtx, m_StatusMsg, m_LastFocus;
	gdouble			m_LastPosX, m_LastPosY;
	bool			m_Negative, m_RowsUpward;
	bool			m_UpdatePos, m_Updating;
	guint			m_TimerId;

	void UpdateImage(void);
	void UpdateChart(void);
	void UpdateControls(void);
	void UpdateStatus(void);
	void Select(int star_id);
	void EditTag(int row, int star_id);
	void SetTag(int row, int star_id, const char *tag);
	void RemoveTag(int row, int star_id);
	void ClearTags(void);

	// Set string in status bar
	void SetStatus(const char *text);

	void UpdateObject(int row, int star_id);
	void UpdateAll(void);

	bool OnCloseQuery(void);
	void OnButtonClicked(GtkWidget *pButton);
	void OnSelectMenu(GdkEventButton *event, gint item);
	void OnContextMenu(GdkEventButton *event);
	bool OnResponseDialog(gint response_id);
	bool OnTagValidator(const gchar *value, GtkWindow *parent);

	static void response_dialog(GtkDialog *pDlg, gint response_id, CSelectObjectDlg *pMe);
	static void button_clicked(GtkWidget *pButton, CSelectObjectDlg *pDlg);
	static void chart_item_activated(GtkWidget *pChart, gint item, CSelectObjectDlg *pMe);
	static void chart_mouse_moved(GtkWidget *button, CSelectObjectDlg *pDlg);
	static void chart_mouse_left(GtkWidget *button, CSelectObjectDlg *pDlg);
	static gint button_press_event(GtkWidget *widget, GdkEventButton *event, CSelectObjectDlg *pMe);
	static gboolean timer_cb(CSelectObjectDlg *pMe);
	static bool tag_validator(const gchar *value, GtkWindow *parent, CSelectObjectDlg *pMe);
};

#endif
