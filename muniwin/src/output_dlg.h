/**************************************************************

output_dlg.h (C-Munipack project)
The base class for file preview dialogs
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_OUTPUT_DLG_H
#define CMPACK_OUTPUT_DLG_H

#include <gtk/gtk.h>

#include "frameset_class.h"
#include "progress_dlg.h"

//
// Base class for windows that show outputs
//
class COutputDlg
{
public:
	// Constructor
	COutputDlg(void);

	// Destructor
	virtual ~COutputDlg();

	// Show non-modal dialog
	void Show(void);

	// Close the window
	void Close();

	// Unsaved data ?
	virtual bool DataSaved(void) const = 0;

	// Environment changed, reload settings
	virtual void EnvironmentChanged(void) {}

protected:
	GtkWidget	*m_pDlg, *m_MainBox, *m_Status;
	gint		m_StatusCtx, m_StatusMsg;

	// Get name of icon that shall be displayed in the top of the window
	// The default implementation returns NULL which means the application icon
	virtual const char *GetIconName(void) { return NULL; }

	// Dialog initialization
	virtual void OnInitDialog(void);

	// Set string in status bar
	void SetStatus(const char *text);

private:
	static void destroyed(GtkObject *pWnd, COutputDlg *pDlg);
	static void realized(GtkWidget *widget, COutputDlg *pMe);
};

//
// Base class for non-modal file preview windows
//
class COutputCurveDlg:public COutputDlg
{
public:
	// Constructor
	COutputCurveDlg(void);

	// Destructor
	virtual ~COutputCurveDlg();

	// Unsaved data ?
	virtual bool DataSaved(void) const
	{ return m_DataSaved; }

	CFrameSet *FrameSet(void)
	{ return &m_FrameSet; }

protected:
	CFrameSet	m_FrameSet;
	bool		m_RawFiles, m_DataSaved;
	bool		m_Updating;

	// Set icon and start the timer
	virtual void OnInitDialog(void);

	// Updates file list and populates frameset with frames
	bool ProcessFiles(GtkWindow *parent, GError **error);

	// Initialize the file list
	bool InitFileList(GtkWindow *parent, bool selected_files);

	// Show preview for selected frame
	void ShowFramePreview(void);

	// Show info about selected frame
	void ShowFrameInfo(void);

	// Remove selected frames from the data set
	void RemoveFromDataSet(void);

	// Delete selected frames from the project
	void DeleteFromProject(void);

	// Frame set changed, update curve
	virtual void OnFrameSetChanged(void) {}

	// Get list of GtkTreePaths of all selected frames
	virtual GList *GetSelectedFrames(void) { return NULL; }

	// Get path to the first selected frame
	virtual GtkTreePath *GetSelectedFrame(void) { return NULL; }

private:
	GSList		*m_FileList, *m_NewFiles;
	bool		m_AllFiles;
	bool		m_DeletedFiles;
	gint		m_Timer;

	// Update list of files
	void UpdateFileList(void);

	void OnFrameChanged(GtkTreeModel *tree_model, GtkTreePath *path, GtkTreeIter *iter);
	void OnFrameDeleted(GtkTreeModel *tree_model, GtkTreePath *path);
	void OnUpdateCurve(void);
	bool OnProcessFiles(CProgressDlg *sender);

	static void frame_changed(GtkTreeModel *tree_model, GtkTreePath *path, GtkTreeIter *iter, COutputCurveDlg *pMe);
	static void frame_deleted(GtkTreeModel *tree_model, GtkTreePath *path, COutputCurveDlg *pMe);
	static gboolean update_timer(COutputCurveDlg *pMe);
	static int ExecuteProc(class CProgressDlg *sender, void *user_data);
};

#endif
