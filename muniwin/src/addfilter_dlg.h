/**************************************************************

checkfiles_dlg.h (C-Munipack project)
Filter for files that are being appended to the project
Copyright (C) 2010 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_CHECKFILES_DLG_H
#define CMPACK_CHECKFILES_DLG_H

#include <gtk/gtk.h>

class CAddFilterDlg
{
public:
	// Constructor
	CAddFilterDlg(GtkWindow *pParent, int context_id);

	// Destructor
	virtual ~CAddFilterDlg();

	// The function executes the dialog, that allows an user to select 
	// a subset of input frames based on the filter, exposure duration 
	// or observation date. The selected files are added to the project.
	// params:
	//	filelist			- [in] list of input files
	void Execute(const GSList *files);

	// The function executes the dialog, that allows an user to select 
	// a subset of source files based on the filter, exposure duration 
	// or observation date. The selected files are added to the project.
	// params:
	//	dirpath				- [in] full path to the base dir
	//	recursive 			- [in] true = include subfolders
	void Execute(const char *dirpath, bool recursive);

private:
	GtkWidget		*m_pDlg, *m_pTabs, *m_pLast;
	GtkWidget		*m_pClose, *m_pBack, *m_pNext, *m_pCancel;
	GSList			*m_Pages;
	GList			*m_Frames;
	const GSList	*m_SrcFiles;
	const gchar		*m_DirPath;
	bool			m_Recursive;
	GtkListStore	*m_FilList, *m_ExpList, *m_ObjList;
	int				m_ContextId, m_FirstPage;

	void UpdateModels(void);
	void UpdateFilterPage(void);
	void UpdateExposurePage(void);
	void UpdateObjectPage(void);
	void ProcessFiles(void);
	void GoToFirstPage(void);
	void GoToPage(int page);
	void GoForward(void);
	void GoBackward(void);
	bool IsFilterSelected(const char *filter);
	bool IsExposureSelected(double exptime);
	bool IsObjectSelected(const char *object);
	int FilterItemCount(void);
	int ExposureItemCount(void);
	int ObjectItemCount(void);
	bool OnResponseDialog(int response_id);

	// Recursive function which makes a list of files
	void PrepareFiles(const GSList *files);
	void PrepareFolder(const char *dirpath, bool recursive);
	void FreeFiles(void);

	// Signal handling
	static void response_dialog(GtkDialog *pDlg, gint response_id, CAddFilterDlg *pMe);
	static void cell_toggled(GtkCellRendererToggle *cell, char *path, GtkTreeModel *pModel);

	static int PrepareFilesProc(class CProgressDlg *sender, void *user_data);
	static int PrepareFolderProc(class CProgressDlg *sender, void *user_data);
	static GList *ScanDir(class CProgressDlg *sender, GList *list, const gchar *dirpath,
		const gchar *relpath, bool recursive, int *result);
};

#endif
