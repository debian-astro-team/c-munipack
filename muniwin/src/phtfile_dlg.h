/**************************************************************

file_dlg.h (C-Munipack project)
The preview dialog
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_PHT_FILE_DLG_H
#define CMPACK_PHT_FILE_DLG_H

#include "file_dlg.h"
#include "info_dlg.h"
#include "progress_dlg.h"
#include "menubar.h"
#include "popup.h"
#include "infobox.h"
#include "phot_class.h"

//
// Non-modal file viewer
//
class CPhtFileDlg:public CFileDlg
{
public:
	// Constructor
	CPhtFileDlg(void);

	// Destructor
	virtual ~CPhtFileDlg();

	// Environment changed, reload settings
	virtual void EnvironmentChanged(void);

protected:
	// Get name of icon
	virtual const char *GetIconName(void) { return "chart"; }

	// Load a file
	virtual bool LoadFile(GtkWindow *pParent, const char *fpath, GError **error);

	// Save a file
	virtual bool SaveFile(const char *fpath, GError **error);

	// Update dialog controls
	virtual void UpdateControls(void);

private:
	enum tDispMode {
		DISP_EMPTY,
		DISP_CHART,
		DISP_TABLE,
		DISP_MODE_COUNT
	};

	enum tInfoMode {
		INFO_NONE,
		INFO_OBJECT
	};

	CPhot				*m_File;
	CWcs				*m_Wcs;
	GtkWidget			*m_ChartScrWnd, *m_ChartView, *m_TableScrWnd, *m_TableView;
	GtkWidget			*m_AperLabel, *m_AperCombo;
	GtkToolItem			*m_ZoomFit, *m_ZoomIn, *m_ZoomOut;
	GtkListStore		*m_Apertures;
	CMenuBar			m_Menu;
	CmpackChartData		*m_ChartData;
	GtkTreeModel		*m_TableData;
	CApertures			m_Aper;
	CTextBox			m_InfoBox;
	bool				m_Updating, m_UpdatePos, m_UpdateZoom;
	bool				m_Negative, m_RowsUpward, m_Rulers;
	int					m_ApertureIndex, m_SelectedObjId, m_SelectedRow;
	int					m_LastPosX, m_LastPosY, m_LastFocus;
	tDispMode			m_DispMode;
	tInfoMode			m_InfoMode;
	int					m_SortColumnId;
	GtkSortType			m_SortType;
	GtkTreeViewColumn	*m_SortCol;
	guint				m_TimerId;
	GtkTreePath			*m_SelectedPath;
	CPopupMenu			m_ContextMenu;

	void UpdateChart(void);
	void UpdateApertures(void);
	void UpdateTableHeader(void);
	void Export(void);
	void SetDisplayMode(tDispMode mode);
	void SetInfoMode(tInfoMode mode);
	void SetSortMode(int column, GtkSortType type);
	void ShowProperties(void);
	void UpdateInfoBox(void);
	void UpdateStatus(void);
	void UpdateZoom(void);
	void CopyWcsCoordinatesFromChart(int row);
	void CopyWcsCoordinatesFromTable(GtkTreePath *path);
	void CopyWcsCoordinates(double lng, double lat);

	void OnCommand(int cmd_id);
	void OnButtonClicked(GtkWidget *pBtn);
	void OnChartItemActivated(gint row);
	void OnSelectionChanged(void);
	void OnTableColumnClicked(GtkTreeViewColumn *pCol);
	void OnTableRowActivated(GtkTreeView *treeview, GtkTreePath *path);
	void OnInfoBoxClosed(void);
	void OnObjectMenu(GdkEventButton *event, gint item);
	void OnObjectMenu(GdkEventButton *event, GtkTreePath *path);

	static void button_clicked(GtkWidget *pButton, CPhtFileDlg *pDlg);
	static void mouse_moved(GtkWidget *pChart, CPhtFileDlg *pMe);
	static void mouse_left(GtkWidget *pChart, CPhtFileDlg *pMe);
	static void zoom_changed(GtkWidget *pChart, CPhtFileDlg *pMe);
	static void chart_item_activated(GtkWidget *pChart, gint row, CPhtFileDlg *pMe);
	static void selection_changed(GtkWidget *pChart, CPhtFileDlg *pMe);
	static gboolean timer_cb(CPhtFileDlg *pMe);
	static void MenuCallback(CCBObject *sender, int message, int wparam, void* lparam, void* cb_data);
	static void InfoBoxCallback(CCBObject *sender, int message, int wparam, void* lparam, void* cb_data);
	static void table_column_clicked(GtkTreeViewColumn *pCol, CPhtFileDlg *pMe);
	static void table_row_activated(GtkTreeView *treeview, GtkTreePath *path, GtkTreeViewColumn *column, CPhtFileDlg *pMe);
	static gboolean button_press_event(GtkWidget *widget, GdkEventButton *event, CPhtFileDlg *pMe);
};


//
// File properties dialog
//
class CPhtFileInfoDlg:public CInfoDlg
{
public:
	// Constructor
	CPhtFileInfoDlg(GtkWindow *pParent);

	// Display dialog
	void ShowModal(CPhot *info, CWcs *wcs, const gchar *name, const gchar *path);

private:
	CPhot		*m_File;
	CWcs		*m_Wcs;
	GtkWidget	*m_HdrBtn, *m_WcsBtn;
	const gchar	*m_Name;

	void OnButtonClicked(GtkWidget *pBtn);
	void ShowHeader(void);
	void ShowWcsData(void);

	static void button_clicked(GtkWidget *pButton, CPhtFileInfoDlg *pDlg);
};

//
// Save photometry file dialog
//
class CExportPhtFileDlg
{
public:
	// Constructor
	CExportPhtFileDlg(GtkWindow *pParent);

	// Destructor
	virtual ~CExportPhtFileDlg();

	// Execute the dialog
	bool Execute(const CPhot &file, const gchar *current_path, int sort_column_id, GtkSortType sort_order);

private:
	enum tFileType {
		TYPE_CSV,
		TYPE_N_ITEMS			// Number of file formats
	};

	struct tOptions {
		bool skip_unmatched, skip_invalid, header;
	};
	
	GtkWindow		*m_pParent;
	GtkWidget		*m_pDlg, *m_TypeCombo;
	GtkWidget		*m_Header, *m_SkipInvalid, *m_SkipUnmatched;
	GtkListStore	*m_FileTypes;
	CPhot			m_Phot;
	tFileType		m_FileType;
	tOptions		m_Options[TYPE_N_ITEMS];
	bool			m_Matched, m_Updating;
	
	void UpdateControls(void);

	bool OnResponseDialog(gint response_id);
	void OnSelectionChanged(GtkComboBox *pWidget);
	void OnTypeChanged(void);
	void OnButtonToggled(GtkToggleButton *widget);

	static void response_dialog(GtkWidget *widget, gint response_id, CExportPhtFileDlg *pMe);
	static void selection_changed(GtkComboBox *pWidget, CExportPhtFileDlg *pMe);
	static void button_toggled(GtkToggleButton *widget, CExportPhtFileDlg *user_data);
};

#endif
