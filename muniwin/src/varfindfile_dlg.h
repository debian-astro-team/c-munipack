/**************************************************************

varfindfile_dlg.h (C-Munipack project)
The 'Find variables' dialog
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_VARFIND_FILE_DLG_H
#define CMPACK_VARFIND_FILE_DLG_H

#include "varfind.h"
#include "file_dlg.h"
#include "info_dlg.h"
#include "output_dlg.h"
#include "progress_dlg.h"
#include "frameset_class.h"
#include "menubar.h"
#include "project.h"
#include "ccdfile_class.h"

//
// Frameset from an external file
//
class CVarFindFileDlg:public CFileDlg
{
public:
	// Constructor
	CVarFindFileDlg(void);

	// Destructor
	virtual ~CVarFindFileDlg();

protected:
	// Get name of icon
	virtual const char *GetIconName(void) { return "varfind"; }

	// Load a file
	virtual bool LoadFile(GtkWindow *parent, const char *fpath, GError **error);

	// Load a file
	virtual bool SaveFile(const char *fpath, GError **error);

private:
	CVarFind	m_VarFind;
	CMenuBar	m_Menu;
	CFrameSet	m_File;
	bool		m_PhotometryFile;
	CCCDFile	m_Frame;
	CImage		*m_Image;
	CPhot		m_Phot;
	CCatalog	m_Catalog;
	CSelectionList m_SelectionList;

	void ReloadFrameSet(void);
	void UpdateControls(void);
	bool UpdateMagDev(GError **error);
	bool UpdateLightCurve(GError **error);
	void ShowProperties(void);
	void RemoveFramesFromDataSet(void);
	void RemoveObjectsFromDataSet(void);

	bool OnEnableCtrlQuery(CVarFind::tControlId ctrl);
	void OnCommand(int cmd_id);

	static void MenuCallback(CCBObject *sender, int message, int wparam, void* lparam, void* cb_data);
	static void VarFindCallback(CCBObject *sender, int message, int wparam, void* lparam, void* cb_data);
};

//
// File properties dialog
//
class CVarFindInfoDlg:public CInfoDlg
{
public:
	// Constructor
	CVarFindInfoDlg(GtkWindow *pParent);

	// Display dialog
	void ShowModal(const CFrameSet &file, const gchar *name, const gchar *path);
};

#endif
