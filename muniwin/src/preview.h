/**************************************************************

preview.h (C-Munipack project)
Preview widget
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef _PREVIEW_H
#define _PREVIEW_H

#include <gtk/gtk.h>

#include "helper_classes.h"

class CPreview
{
public:
	// Constructor
	CPreview(void);

	// Destructor
	virtual ~CPreview();

	// Get widget
	GtkWidget *Handle(void)
	{ return m_Widget; }

	// Clear preview
	void Clear(void);

	// Show file
	void Update(const char *filepath);

protected:
	char			*m_Path;		// File path
	GtkWidget		*m_Widget;		// Whole widget
	GtkTextBuffer	*m_Buffer;		// Text buffer
	GtkWidget		*m_View;		// Text view
	GtkWidget		*m_Preview;		// Preview widget
	GObject			*m_Data;		// Data model
	tFileType		m_FileType;		// Type of displayed file
	gdouble			m_MinX, m_MaxX, m_EpsX;
	gdouble			m_MinY, m_MaxY, m_EpsY;
	gboolean		m_ReverseY;

	// Get top level window
	GtkWindow *GetTopLevel(void);

	// Double click open a big preview
	void OnButtonPress(GtkWidget *widget, GdkEventButton *event);

	// Set file path
	void SetPath(const char *path);

	// Load image file
	void LoadImage(const gchar *fpath);

	// Load photometry file
	void LoadChart(const gchar *fpath);

	// Load catalog file
	void LoadCatalog(const gchar *fpath);

	// Load catalog file
	void LoadTable(const gchar *fpath);

	// Load varfind file
	void LoadVarFind(const gchar *fpath);

	// Load project file
	void LoadProject(const gchar *fpath);

	// Load profile file
	void LoadProfile(const gchar *fpath);

	// On show file
	void ShowFileModal(void);

	// Signal handlers
	static gint button_press_event(GtkWidget *widget, GdkEventButton *event, CPreview *pMe);
};

#endif
