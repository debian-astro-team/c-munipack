/**************************************************************

chartexportdlg.h (C-Munipack project)
Export chart dialog
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_EXPORT_AAVSO_H
#define CMPACK_EXPORT_AAVSO_H

#include <gtk/gtk.h>

// Max. number of comparison stars
#define CNAME_MAX 10

struct tAavsoExportParams
{
	gchar *star_id, *obs_code, *obs_type, *chart, *filter, *notes;
	gchar *cname[CNAME_MAX], *cmag[CNAME_MAX], *kname;

	tAavsoExportParams();
	~tAavsoExportParams();
};

class CAavsoExportDlg
{
public:
	// Constructor
	CAavsoExportDlg(GtkWindow *pParent);

	// Destructor
	virtual ~CAavsoExportDlg();

	// Execute the dialog.
	// params:
	//	table			- [in] light curve
	bool Execute(const gchar *filename, const CTable &table, const CSelection &selection, 
		const CTags &tags, bool heliocentric, GError **error);

	// Get export parameters
	tAavsoExportParams *Params() { return &m_params; }

	// Number of comparison stars
	int NComp(void) const { return m_ncomp; }

	// Selection of stars
	CSelection *Selection() { return &m_selection; }

private:
	friend class CAavsoExportTab;

	GtkWidget		*m_pDlg, *m_pTabs;
	GtkWidget		*m_pSaveBtn, *m_pBackBtn, *m_pNextBtn, *m_pCancelBtn;
	GSList			*m_Pages;
	bool			m_heliocentric, m_haveCheckStar;
	int				m_ncomp;
	tAavsoExportParams m_params;
	CSelection		m_selection;
	CTags			m_tags;

	void LoadParams(void);
	void SaveParams(void);

	bool SaveFile(const gchar *filename, const CTable &table, GError **error);

	void GoToFirstPage(void);
	void GoToPage(int page);
	void GoForward(void);
	void GoBackward(void);

	void UpdateTab(int page);
	bool LeavePageQuery(int response_id);

	void ExportTable(CCSVWriter &f, CTable &table);

	bool OnResponseDialog(int response_id);

	// Signal handling
	static void response_dialog(GtkDialog *pDlg, gint response_id, CAavsoExportDlg *pMe);
};

class CAavsoExportTab
{
public:
	CAavsoExportTab(class CAavsoExportDlg *parent, const gchar *caption);
	virtual ~CAavsoExportTab() {}

	virtual void Update() {}
	virtual bool Check() { return true; }
	virtual void Apply() {}

	GtkWidget *Widget(void) const { return m_tab; }
	GtkWindow *Window(void) const { return GTK_WINDOW(m_pParent->m_pDlg); }

protected:
	CAavsoExportDlg *m_pParent;

private:
	GtkWidget		*m_tab, *m_title;
};

class CAavsoExportInitTab:public CAavsoExportTab
{
public:
	CAavsoExportInitTab(class CAavsoExportDlg *parent);

	virtual void Update();
	virtual bool Check();
	virtual void Apply();

private:
	GtkWidget *m_StarId, *m_ObsCode, *m_ObsType, *m_Filter, *m_Chart, *m_Notes;
	GtkWidget *m_SelectFilterBtn;

	void OnButtonClicked(GtkWidget *pBtn);

	static void button_clicked(GtkWidget *pButton, CAavsoExportInitTab *pMe);
};

class CAavsoExportCompTab:public CAavsoExportTab
{
public:
	CAavsoExportCompTab(class CAavsoExportDlg *parent);

	virtual void Update();
	virtual bool Check();
	virtual void Apply();

private:
	GtkWidget *m_label1, *m_label2, *m_StarId, *m_Mag;
};

class CAavsoExportEnsembleTab:public CAavsoExportTab
{
public:
	CAavsoExportEnsembleTab(class CAavsoExportDlg *parent);

	virtual void Update();
	virtual bool Check();
	virtual void Apply();

private:
	GtkWidget *m_Header[2];
	GtkWidget *m_RowLabel[CNAME_MAX], *m_StarId[CNAME_MAX], *m_Mag[CNAME_MAX];
};

class CAavsoExportCheckTab:public CAavsoExportTab
{
public:
	CAavsoExportCheckTab(class CAavsoExportDlg *parent);

	virtual void Update();
	virtual bool Check();
	virtual void Apply();

private:
	GtkWidget *m_StarId;
};

class AavsoExportSelectFilterDlg
{
public:
	// Constructor
	AavsoExportSelectFilterDlg(GtkWindow *pParent, bool ccd, bool dslr);

	// Destructor
	virtual ~AavsoExportSelectFilterDlg();

	// Execute the dialog
	gchar *Execute(const gchar *defaultValue);

private:
	GtkWidget *m_pDlg, *m_ListView;
	GtkListStore *m_FilterList;
	GtkTreePath *m_SelPath;

	void Select(const gchar *name);
	void UpdateControls();
	
	void OnSelectionChanged(GtkTreeSelection *widget);
	bool OnResponseDialog(gint response_id);

	static void response_dialog(GtkDialog *pDlg, gint response_id, AavsoExportSelectFilterDlg *pMe);
	static void selection_changed(GtkTreeSelection *widget, AavsoExportSelectFilterDlg *pMe);
};

#endif
