/**************************************************************

preview_dlg.h (C-Munipack project)
The preview dialog
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_SHOWHEADER_DLG_H
#define CMPACK_SHOWHEADER_DLG_H

#include <gtk/gtk.h>

#include "helper_classes.h"
#include "ccdfile_class.h"
#include "phot_class.h"
#include "table_class.h"
#include "catfile_class.h"
#include "frameset_class.h"

class CShowHeaderDlg
{
public:
	// Constructor
	CShowHeaderDlg(GtkWindow *pParent);

	// Destructor
	virtual ~CShowHeaderDlg();

	// Display dialog
	void Execute(const CCatalog *file, const gchar *name);
	void Execute(const CCCDFile *file, const gchar *name);
	void Execute(const CPhot *file, const gchar *name);
	void Execute(const CTable *file, const gchar *name);

private:
	enum tFileType {
		UNDEFINED, CCDFILE, CATALOG, PHOT, TABLE
	};
	
	GtkWidget		*m_pDlg, *m_View, *m_ScrWnd, *m_SaveBtn;
	GtkTreeStore	*m_Data;
	tFileType		m_FileType;
	gchar			*m_Name;
	const CCCDFile	*m_CCDFile;
	const CCatalog	*m_CatFile;
	const CPhot		*m_PhtFile;
	const CTable	*m_Table;

	// Set caption
	void SetCaption(const char *name);

	// Begin/end updating
	void BeginUpdate(void);
	void EndUpdate(void);
	
	// Add item
	void Add(const gchar *keyword, const gchar *value, const gchar *comment);
	void Add(const gchar *keyword, int value, const gchar *comment);
	void Add(const gchar *keyword, double value, int prec, const gchar *comment);

	void OnButtonClicked(GtkWidget *pBtn);
	void SaveAs(void);

	static void button_clicked(GtkWidget *pButton, CShowHeaderDlg *pDlg);
};

//
// Save photometry file dialog
//
class CExportHeaderDlg
{
public:
	// Constructor
	CExportHeaderDlg(GtkWindow *pParent);

	// Destructor
	virtual ~CExportHeaderDlg();

	// Display dialog
	void Execute(const CCatalog *file, const gchar *name);
	void Execute(const CCCDFile *file, const gchar *name);
	void Execute(const CPhot *file, const gchar *name);
	void Execute(const CTable *file, const gchar *name);

private:
	enum tFileType {
		TYPE_CSV,
		TYPE_N_ITEMS			// Number of file formats
	};

	struct tOptions {
		bool header;
	};
	
	GtkWindow		*m_pParent;
	GtkWidget		*m_pDlg, *m_TypeCombo;
	GtkWidget		*m_Header;
	GtkListStore	*m_FileTypes;
	tFileType		m_FileType;
	tOptions		m_Options[TYPE_N_ITEMS];
	bool			m_Matched, m_Updating;
	
	void UpdateControls(void);

	gchar *Execute(const gchar *name);
	const gchar *MimeType(void);
	unsigned Flags(void);

	bool OnResponseDialog(gint response_id);
	void OnSelectionChanged(GtkComboBox *pWidget);
	void OnTypeChanged(void);
	void OnButtonToggled(GtkToggleButton *widget);

	static void response_dialog(GtkWidget *widget, gint response_id, CExportHeaderDlg *pMe);
	static void selection_changed(GtkComboBox *pWidget, CExportHeaderDlg *pMe);
	static void button_toggled(GtkToggleButton *widget, CExportHeaderDlg *user_data);
};

#endif
