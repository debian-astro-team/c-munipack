/**************************************************************

convert_dlg.h (C-Munipack project)
The 'Convert files' dialog
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_CONVERT_DLG_H
#define CMPACK_CONVERT_DLG_H

#include <gtk/gtk.h>

class CConvertDlg
{
public:
	CConvertDlg(GtkWindow *pParent);
	~CConvertDlg();

	void Execute();

private:
	GtkWindow		*m_pParent;
	GtkWidget		*m_pDlg, *m_AllBtn, *m_SelBtn, *m_ChCombo, *m_OptionsBtn;
	GtkListStore	*m_Channels;
	int				m_InFiles, m_OutFiles;
	GList			*m_FileList;

	int ConvertFiles(class CProgressDlg *sender);
	void EditPreferences(void);
	void SelectChannel(CmpackChannel channel);
	CmpackChannel SelectedChannel(void);

	bool OnResponseDialog(gint response_id);
	void OnButtonClicked(GtkButton *button);

	static void foreach_sel_files(GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer userdata);
	static gboolean foreach_all_files(GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer data);
	static int ExecuteProc(class CProgressDlg *sender, void *user_data);
	static void response_dialog(GtkDialog *pDlg, gint response_id, CConvertDlg *pMe);
	static void button_clicked(GtkButton *button, CConvertDlg *pDlg);
};

#endif
