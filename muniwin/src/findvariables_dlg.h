/**************************************************************

varfind_dlg.h (C-Munipack project)
The 'Find variables' dialog
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_FIND_VARIABLES_DLG_H
#define CMPACK_FIND_VARIABLES_DLG_H

#include "varfind.h"
#include "file_dlg.h"
#include "info_dlg.h"
#include "output_dlg.h"
#include "progress_dlg.h"
#include "frameset_class.h"
#include "menubar.h"
#include "project.h"
#include "ccdfile_class.h"

// 
// Initial dialog (Process frames or import from an external file)
//
class CMakeVarFindDlg
{
public:
	CMakeVarFindDlg(GtkWindow *pParent);
	~CMakeVarFindDlg();

	void Execute(void);

private:
	GtkWindow	*m_pParent;
	GtkWidget	*m_pDlg, *m_AllBtn, *m_SelBtn, *m_ImportBtn;
	GtkWidget	*m_PathBox, *m_PathLabel, *m_Path, *m_PathBtn, *m_OptionsBtn;
	bool		m_Reference, m_Import;

	void UpdateControls(void);
	void ChangeFilePath(void);
	void EditPreferences(void);
	bool OnResponseDialog(gint response_id);
	void OnToggled(GtkWidget *widget);
	void OnButtonClicked(GtkWidget *widget);

	static void response_dialog(GtkDialog *pDlg, gint response_id, CMakeVarFindDlg *pMe);
	static void toggled(GtkWidget *widget, CMakeVarFindDlg *pMe);
	static void button_clicked(GtkWidget *widget, CMakeVarFindDlg *pMe);
};


//
// Frameset from project frames
//
class CVarFindDlg:public COutputDlg
{
public:
	// Constructor
	CVarFindDlg(void);

	// Destructor
	virtual ~CVarFindDlg();

	// Open a new dialog
	bool Open(GtkWindow *pParent, bool selected_files, CSelectionList *list);

	// Do not ask for user's confirmation when closing this window
	virtual bool DataSaved(void) const { return true; }

protected:
	// Dialog initialization
	virtual void OnInitDialog(void);

private:
	GSList			*m_FileList;
	CVarFind		m_VarFind;
	CMenuBar		m_Menu;
	CFrameSet		m_FrameSet;
	CPhot			m_Phot;
	CCatalog		m_Catalog;
	CCCDFile		m_Frame;
	CImage			*m_Image;
	bool			m_ValidDataSet;

	void RebuildFrameSet(void);
	void UpdateFileList(void);
	void UpdateMagDev(void);
	void UpdateLightCurve();
	void UpdateControls(void);
	void ExportData(void);
	void RemoveFramesFromDataSet(void);
	void DeleteFramesFromProject(void);
	void RemoveObjectsFromDataSet(void);
	void ShowFramePreview(void);
	void ShowFrameInfo(void);
	void SaveSelection(void);

	void OnCommand(int cmd_id);
	bool OnEnableCtrlQuery(CVarFind::tControlId ctrl);
	int OnProcessFiles(CProgressDlg *sender);
	
	static int ExecuteProc(CProgressDlg *sender, void *userdata);
	static void MenuCallback(CCBObject *sender, int message, int wparam, void* lparam, void* cb_data);
	static void VarFindCallback(CCBObject *sender, int message, int wparam, void* lparam, void* cb_data);
};

#endif
