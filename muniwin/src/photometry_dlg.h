/**************************************************************

photometry_dlg.h (C-Munipack project)
The 'Photometry' dialog
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_PHOTOMETRY_DLG_H
#define CMPACK_PHOTOMETRY_DLG_H

#include <gtk/gtk.h>

#include "profile_editor.h"

class CPhotometryDlg
{
public:
	CPhotometryDlg(GtkWindow *pParent);
	~CPhotometryDlg();

	void Execute();

private:
	GtkWindow	*m_pParent;
	GtkWidget	*m_pDlg, *m_AllBtn, *m_SelBtn;
	GtkWidget	*m_Options1Btn, *m_Options2Btn;
	int			m_InFiles, m_OutFiles;
	GList		*m_FileList;

	int ProcessFiles(class CProgressDlg *sender);
	void EditPreferences(tProfilePageId page);

	bool OnResponseDialog(gint response_id);
	void OnButtonClicked(GtkButton *button);
	
	static void response_dialog(GtkDialog *pDlg, gint response_id, CPhotometryDlg *pMe);
	static void button_clicked(GtkButton *button, CPhotometryDlg *pDlg);
	static int ExecuteProc(class CProgressDlg *sender, void *user_data);
};

#endif
