/**************************************************************

locations.h (C-Munipack project)
Table of user defined locations
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef MUNIWIN_LOCATIONS_H
#define MUNIWIN_LOCATIONS_H

#include <gtk/gtk.h>

#include "helper_classes.h"
#include "file_classes.h"

// Location list - model columns
enum tLocationColumns {
	LOCATION_NAME,		// Name
	LOCATION_LON,		// Longitude
	LOCATION_LAT,		// Latitude
	LOCATION_REMARKS,	// Remarks
	LOCATION_NCOLS
};

//
// Project interface
//
class CLocations
{
public:
	// Constructor
	CLocations();

	// Destructor
	virtual ~CLocations(void);

	// Clear the locations
	void Clear(void);

	// Load locations from file
	void Load(void);

	// Save locations to file
	void Save(void);

	// Load locations from file
	int Import(const gchar *filepath);

	// Save locations to file
	void Export(const gchar *filepath);

	// Get list of frames
	GtkTreeModel *List(void)
	{ return GTK_TREE_MODEL(m_List); }

	// Get number of files
	int GetCount(void);

	// Add new object to the list
	GtkTreePath *Add(const CLocation *data);

	// Update coordinates
	void Update(GtkTreePath *pPath, const CLocation *data);

	// Get coordinates
	bool Get(GtkTreePath *pPath, CLocation *data);

	// Delete object from the list
	void Remove(GtkTreePath *pPath);

private:
	// List of locations
	GtkListStore	*m_List;
	int				m_State, m_Level;
	GString			*m_Str;
	CLocation		m_Tmp;

	// Get file name
	gchar *GetPath(void);

	// XML start element
	static void StartElement(GMarkupParseContext *context,
		const gchar *element_name, const gchar **attribute_names,
		const gchar **attribute_values, gpointer user_data, GError **error);

	// XML end element
	static void EndElement(GMarkupParseContext *context,
		const gchar *element_name, gpointer user_data, GError **error);

	// Character data handler
	static void CharacterData(GMarkupParseContext *context,
		const gchar *text, gsize text_len, gpointer user_data, GError **error);
};

#endif
