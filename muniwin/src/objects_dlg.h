/**************************************************************

chart_dlg.h (C-Munipack project)
The 'Plot chart' dialog
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/
#ifndef CMPACK_OBJECTS_DLG_H
#define CMPACK_OBJECTS_DLG_H

#include <gtk/gtk.h>

#include "cmpack_widgets.h"
#include "lightcurve_dlg.h"
#include "phot_class.h"
#include "ccdfile_class.h"
#include "catfile_class.h"
#include "image_class.h"
#include "popup.h"

class CObjectsDlg
{
public:
	// Constructor
	CObjectsDlg(GtkWindow *pParent);

	// Destructor
	virtual ~CObjectsDlg();

	// Execute the dialog
	void Execute(const CSelection &sel, int aperIndex, const CLightCurveDlg::tParamsRec &params, const gchar *default_file_name);

public:
	// Table columns (also sorting columns)
	enum tColumnId {
		COL_NAME, COL_ID, COL_POS_X, COL_POS_Y, COL_WCS_VALID, COL_WCS_LNG, COL_WCS_LAT, 
		COL_MAG_VALID, COL_MAG, COL_SEL_TYPIDX, COL_TAGS, COL_FG_COLOR, COL_COUNT
	};

private:
	GtkWindow			*m_pParent;
	GtkWidget			*m_pDlg, *m_TableView, *m_TableScrWnd;
	GtkToolItem			*m_SaveBtn;
	CPhot				m_Phot;
	CCatalog			m_Catalog;
	CWcs				*m_Wcs;
	CSelection			m_Selection;
	CTags				m_Tags;
	GtkTreeModel		*m_TableData;
	int					m_SortColumnId, m_ApertureIndex;
	GtkSortType			m_SortType;
	GtkTreeViewColumn	*m_SortCol;
	GtkTreePath			*m_SelectedPath;
	CLightCurveDlg::tParamsRec m_Params;
	const gchar			*m_DefaultFileName;
	CPopupMenu			m_ContextMenu;

	void SetView(void);
	void UpdateTable(void);
	void SaveTable(void);
	void CopyWcsCoordinatesFromTable(GtkTreePath *path);
	void CopyWcsCoordinates(CWcs *wcs, double lng, double lat);

	bool OnResponseDialog(gint response_id);
	void OnButtonClicked(GtkWidget *pButton);
	void OnTableColumnClicked(GtkTreeViewColumn *pCol);
	void OnSelectionChanged(void);
	void OnContextMenu(GdkEventButton *event, GtkTreePath *path);

	static void response_dialog(GtkWidget *widget, gint response_id, CObjectsDlg *pMe);
	static void button_clicked(GtkWidget *pButton, CObjectsDlg *pDlg);
	static void table_column_clicked(GtkTreeViewColumn *pCol, CObjectsDlg *pMe);
	static void selection_changed(GtkWidget *pChart, CObjectsDlg *pMe);
	static gint button_press_event(GtkWidget *widget, GdkEventButton *event, CObjectsDlg *pMe);
};

#endif
