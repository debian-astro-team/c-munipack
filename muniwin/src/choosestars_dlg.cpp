/**************************************************************

stars_dlg.cpp (C-Munipack project)
The 'Choose stars' dialog
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#include <stdlib.h>
#include <string.h>

#include "project.h"
#include "choosestars_dlg.h"
#include "main.h"
#include "utils.h"
#include "configuration.h"
#include "configuration.h"
#include "profile.h"
#include "ctxhelp.h"

//-------------------------   CHOOSE STARS DIALOG   --------------------------------

//
// Constructor
//
CChooseStarsDlg::CChooseStarsDlg(GtkWindow *pParent):CEditSelectionsDlg(pParent)
{
	m_SelectionList = g_Project->SelectionList();
	m_Tags = g_Project->Tags();
}


//
// Execute the dialog
//
bool CChooseStarsDlg::Execute(CSelection &sel, CLightCurveDlg::tParamsRec params)
{
	m_Ensemble = params.ensemble;
	m_AllStars = params.allstars;
	m_InstMag = params.instmag;

	m_MovingTarget =  0;
	if (g_Project->GetTargetType() == MOVING_TARGET)
		m_MovingTarget = g_Project->GetReferenceTarget();

	UpdateSelection(sel);
	m_SelectionIndex = g_Project->SelectionList()->IndexOf(sel);
	SetCurrentSelection(sel);
	SetNewSelection(sel);
	m_ShowNewSelection = (m_SelectionIndex < 0);

	bool retval = CEditSelectionsDlg::Execute();
	if (retval)
		sel = m_Current;
	return retval;
}

bool CChooseStarsDlg::OnCloseQuery()
{
	if (!m_InstMag) {
		if (m_Current.CountStars(CMPACK_SELECT_VAR)==0 && !m_AllStars) {
			ShowError(GTK_WINDOW(m_pDlg), "Please, select a variable star.");
			return false;
		}
		if (m_Current.CountStars(CMPACK_SELECT_VAR)>1 && !m_AllStars) {
			ShowError(GTK_WINDOW(m_pDlg), "It is not allowed to select more than one variable star.");
			return false;
		}
		if (m_Current.CountStars(CMPACK_SELECT_COMP)==0) {
			ShowError(GTK_WINDOW(m_pDlg), "Please, select a comparison star.");
			return false;
		}
		if (m_Current.CountStars(CMPACK_SELECT_COMP)>1 && !m_Ensemble) {
			ShowError(GTK_WINDOW(m_pDlg), "It is not allowed to select more than one comparison star.");
			return false;
		}
	}
	return CEditSelectionsDlg::OnCloseQuery();
}
