/**************************************************************

project_dlg.h (C-Munipack project)
Project settions dialog
Copyright (C) 2012 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_PROJECT_DLG_H
#define CMPACK_PROJECT_DLG_H

#include <gtk/gtk.h>

#include "utils.h"
#include "profile_editor.h"
#include "preview.h"

//
// New project dialog
//
class CNewProjectDlg
{
public:
	CNewProjectDlg(GtkWindow *pParent);
	virtual ~CNewProjectDlg();

	// Execute the dialog
	bool Execute(void);
	const gchar *FilePath(void) const { return m_FilePath; }
	const CProfile &Profile(void) const { return m_Profile; }

private:
	enum tTypeColumdId
	{
		TCOL_PIXBUF,
		TCOL_CAPTION,
		TCOL_FILEPATH,
		TCOL_USERDEFINED,
		TNCOLS
	};

	GtkWidget		*m_pDlg, *m_TreeView;
	GtkWidget		*m_Name, *m_Path, *m_PathBtn, *m_OptionsBtn;
	GtkTreeStore	*m_Profiles;
	CProfile		m_Profile;
	gchar			*m_ParentDir, *m_FilePath, *m_ProfilePath;

	void ChangeDirPath(void);
	void UpdateProfiles(void);
	GtkTreePath *FindNode(const gchar *fpath, GtkTreeModel *model, GtkTreeIter *parent);
	void UpdateControls(void);

	bool OnCloseQuery(void);
	bool OnResponseDialog(gint response_id);
	void OnButtonClicked(GtkWidget *widget);
	void OnTreeViewSelectionChanged(GtkTreeSelection *pWidget);
	
	static void response_dialog(GtkDialog *pDlg, gint response_id, CNewProjectDlg *pMe);
	static void button_clicked(GtkWidget *widget, CNewProjectDlg *pMe);
	static void tv_selection_changed(GtkTreeSelection *pWidget, CNewProjectDlg *pMe);
	static void name_changed(GtkWidget *pEntry, CNewProjectDlg *pMe) { pMe->UpdateControls(); }
	static void path_changed(GtkWidget *pEntry, CNewProjectDlg *pMe) { pMe->UpdateControls(); }
};


//
// Save project dialog
//
class CExportProjectDlg
{
public:
	CExportProjectDlg(GtkWindow *pParent);
	virtual ~CExportProjectDlg();

	bool Execute(void);

	const gchar *Path() const
	{ return m_Path; }

private:
	GtkWidget	*m_pDlg;
	gchar		*m_Path;

	bool OnCloseQuery();
	bool OnResponseDialog(gint response_id);

	static void response_dialog(GtkDialog *pDlg, gint response_id, CExportProjectDlg *pMe);
};


//
// Import project dialog (path to existing project)
//
class CImportProjectDlg
{
public:
	CImportProjectDlg(GtkWindow *pParent);
	virtual ~CImportProjectDlg();

	bool Execute(void);

	const gchar *Path(void) const { return m_Path; }

private:
	GtkWidget	*m_pDlg;
	CPreview	m_Preview;
	gchar		*m_Path;

	bool OnResponseDialog(gint response_id);
	void UpdateControls(void);

	static void response_dialog(GtkDialog *pDlg, gint response_id, CImportProjectDlg *pMe);
};

//
// New project dialog (name, path & type)
//
class CImportProject2Dlg
{
public:
	CImportProject2Dlg(GtkWindow *pParent);
	virtual ~CImportProject2Dlg();

	// Execute the dialog
	bool Execute(void);
	const gchar *FilePath(void) const { return m_FilePath; }
	tProjectType Type(void) const { return m_Type; }

private:
	enum tTypeColundId
	{
		TCOL_ID,
		TCOL_CAPTION,
		TCOL_PIXBUF,
		TNCOLS
	};

	GtkWidget		*m_pDlg, *m_TypeView;
	GtkWidget		*m_Name, *m_Path, *m_PathBtn;
	GtkListStore	*m_Types;
	tProjectType	m_Type;
	gchar			*m_ParentDir, *m_FilePath;

	bool OnCloseQuery(void);
	bool OnResponseDialog(gint response_id);
	void OnButtonClicked(GtkWidget *widget);
	void ChangeDirPath(void);
	
	static void response_dialog(GtkDialog *pDlg, gint response_id, CImportProject2Dlg *pMe);
	static void button_clicked(GtkWidget *widget, CImportProject2Dlg *pMe);
};

class CEditProject:public CEditProfileBase
{
public:
	CEditProject(class CEditProjectDlg *pParent);

	void Init(void);
	bool OnCloseQuery(void);

protected:
	virtual GtkWidget *DialogWidget(void);
	virtual void CreatePages(GSList **stack);
	virtual void SetData(void);
	virtual bool IsPageVisible(tProfilePageId pageId);
	virtual void OnButtonClicked(GtkWidget *pWidget);
	virtual void OnComboBoxChanged(GtkWidget *pBox);
	virtual void OnPageChanged(tProfilePageId currentPage, tProfilePageId previousPage);
	virtual tProjectType projectType(void) const { return m_ProjectType; }

private:
	GtkWidget		*m_LoadFromProfile, *m_SaveAsProfile, *m_LoadFromProject;
	GtkWidget		*m_Type, *m_Name, *m_Path, *m_Rename, *m_TypeBtn;
	GtkWidget		*m_Bias, *m_Dark, *m_Flat, *m_Ref, *m_Cat;

	CEditProjectDlg *m_pParent;

	tProjectType	m_ProjectType;

	void LoadFromProfile(void);
	void LoadFromProject(void);
	void SaveAsProfile(void);
};

class CEditProjectDlg
{
public:
	CEditProjectDlg(GtkWindow *pParent);
	~CEditProjectDlg();

	bool Execute(tProfilePageId initPage = EndOfProfilePages);

private:
	friend class CEditProject;

	CEditProject	*m_Widget;
	GtkWidget		*m_pDlg;

	bool OnResponseDialog(gint response_id);

	static void response_dialog(GtkWidget *widget, gint response_id, CEditProjectDlg *pMe);

private:
	// Disable copy constructor and assignment operator
	CEditProjectDlg(const CEditProjectDlg&);
	CEditProjectDlg& operator=(const CEditProjectDlg&);
};

#endif
