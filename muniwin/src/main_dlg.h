/**************************************************************

main_dlg.h (C-Munipack project)
Main dialog
Copyright (C) 2003 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_MAIN_DLG_H
#define CMPACK_MAIN_DLG_H

#include <gtk/gtk.h>

#include "toolbar.h"
#include "menubar.h"
#include "frame_dlg.h"
#include "file_dlg.h"
#include "output_dlg.h"
#include "newfiles_dlg.h"
#include "popup.h"

class CMainWindow
{
public:
	CMainWindow(const char *fpath);
	~CMainWindow();

	GtkTreeSelection *GetSelection(void)
	{ return gtk_tree_view_get_selection(GTK_TREE_VIEW(m_TreeView)); }

	void BeginUpdate(void);
	void EndUpdate(void);

	void ShowFramePreview(GtkTreePath *path);

	void RegisterFrameDlg(CFrameDlg *pDlg);
	void FrameDlgClosed(CFrameDlg *pDlg);

	void RegisterFileDlg(CFileDlg *pDlg);
	void FileDlgClosed(CFileDlg *pDlg);
	CFileDlg *FindFile(const char *fpath);

	void RegisterOutputDlg(COutputDlg *pDlg);
	void OutputDlgClosed(COutputDlg *pDlg);

	void CloseOutputDlgs();

	void ShowNewFiles();
	void CloseNewFiles();
	void OnNewFilesClosed();

	void ShowHelp(GtkWindow *pParent, int context_id);

	void RemoveFilesWithUi(GtkWindow *pParent, GList *pPathList);

private:
	CToolBar			m_TBar;
	CMenuBar			m_Menu;
	CPopupMenu			m_Popup;
	GtkWindow			*m_pDlg;
	GtkWidget			*m_TreeView, *m_Box, *m_TreeScrWnd, *m_Status;
	GdkPixbuf			**m_Icons;
	GSList				*m_Previews, *m_Files, *m_Outputs;
	CNewFilesBox		*m_pNewFiles;
	int					m_LockCounter;
	gint				m_StatusCtx, m_StatusMsg;
	int					m_SortColumnId;
	GtkSortType			m_SortType;
	bool				m_ColumnSortEnabled;

	// Disable copy constructor
	CMainWindow(const CMainWindow&);

	void UpdateTitle();
	bool UnsavedData();
	void UpdateControls();
	void RebuildTable();
	void CloseFrameDlgs(void);
	void CloseFileDlgs(void);

	GdkPixbuf *GetIconRef(int icon_id);
	CFrameDlg *FindFrame(GtkTreePath *pPath);

	static void GetIcon(GtkTreeViewColumn *tree_column, GtkCellRenderer *cell,
		GtkTreeModel *tree_model, GtkTreeIter *iter, gpointer data);

	void OpenFileOrProject(const gchar *path);

	void DisableColumnSort(void);
	void EnableColumnSort(void);
	
	void OnCommand(int cmd_id);
	void OnRowActivated(GtkTreeView *tree_view, GtkTreePath *path);
	void OnContextMenu(GtkWidget *widget, GdkEventButton *event);
	void ShowFrameProperties(GtkTreePath *path);
	bool QueryCloseOutputDlgs(void);
	bool QueryCloseNewFiles(void);
	void OnTableColumnClicked(GtkTreeViewColumn *pCol);

	// Set string in status bar
	void SetStatus(const char *text);

	static gint exit_event(GtkWidget *widget, GdkEvent *event, CMainWindow *pMe);
	static void row_activated(GtkTreeView *treeview, GtkTreePath *path, GtkTreeViewColumn *column, CMainWindow *pMe);
	static void MenuCallback(CCBObject *sender, int message, int wparam, void* lparam, void* cb_data);
	static void ToolbarCallback(CCBObject *sender, int message, int wparam, void* lparam, void* cb_data);
	static void NewFilesCallback(CCBObject *sender, int message, int wparam, void* lparam, void* cb_data);
	static void selection_changed(GtkTreeSelection *widget, CMainWindow *pMe);
	static gint button_press_event(GtkWidget *widget, GdkEventButton *event, CMainWindow *pMe);
	static void table_column_clicked(GtkTreeViewColumn *treeviewcolumn, CMainWindow *pDlg);

	void ExitApplication(void);
	void AddIndividualFrames(void);
	void AddFramesFromFolder(void);
	void NewProject(void);
	void OpenProject(void);
	void ExportProject(void);
	void EditProject(void);
	void CloseProject(void);
	void ClearProject(void);
	void RemoveFiles(void);
	void ConvertFiles(void);
	void OpenFile(void);

	void TimeCorrection(void);
	void BiasCorrection(void);
	void DarkCorrection(void);
	void FlatCorrection(void);

	void Photometry(void);

	void Matching(void);

	void MakeLightCurve(void);

	void FindVariables(void);
	void MakeCatalogFile(void);
	void MasterBias(void);
	void MasterDark(void);
	void MasterFlat(void);
	void MergeFrames(void);
	void PlotTrackCurve(void);
	void PlotAirMassCurve(void);
	void PlotCCDTemperature(void);
	void PlotObjProperties(void);
	void ExpressReduction(void);
	void NewFiles(void);
	void JDConverter(void);
	void HelCorrection(void);
	void AirMass(void);
	void ImportData(void);
	void EditProfiles(void);
	void EditEnvironment(void);

	void ShowMessages(void);
	void ShowThumbnails(void);

	void AboutApplication(void);

	void SelectAll(void);
};

#endif
