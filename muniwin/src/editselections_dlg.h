/**************************************************************

stars_dlg.h (C-Munipack project)
The 'Choose stars' dialog
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_EDIT_SELECTIONS_DLG_H
#define CMPACK_EDIT_SELECTIONS_DLG_H

#include <gtk/gtk.h>

#include "cmpack_widgets.h"
#include "popup.h"
#include "project.h"
#include "lightcurve_dlg.h"
#include "image_class.h"
#include "phot_class.h"
#include "catfile_class.h"

class CEditSelectionsDlg
{
public:
	// Constructor
	CEditSelectionsDlg(GtkWindow *pParent);

	// Destructor
	virtual ~CEditSelectionsDlg();

	// Execute the dialog
	bool Execute();

protected:
	// Display modes
	enum tDisplayMode {
		DISPLAY_IMAGE,
		DISPLAY_CHART,
		DISPLAY_FULL
	};

	struct tCustomColor {
		double red;
		double green;
		double blue;
	};
	
	struct tCatalogSet {
		GtkWidget		*chkButton;
		GtkToolItem		*setButton;
		GtkWidget		*icon;
		bool			enabled, show;
		int				layerId;
		tCustomColor	color;
		GSList			*obj_list;
	};

	GtkWindow		*m_pParent;
	GtkWidget		*m_pDlg, *m_Chart, *m_SelectCbx, *m_Status;
	GtkToolItem		*m_ShowImage, *m_ShowChart, *m_ShowMixed, *m_ShowTags;
	GtkToolItem		*m_ClearBtn, *m_SaveBtn, *m_RemoveBtn;
	GtkToolItem		*m_ZoomIn, *m_ZoomOut, *m_ZoomFit;
	GtkListStore	*m_Selections;
	tDisplayMode	m_DisplayMode;
	CSelectionList	*m_SelectionList;
	CTags			*m_Tags;
	CSelection		m_Current, m_NewSelection;
	CPhot			m_Phot;
	CCatalog		m_Catalog;
	CImage			*m_Image;
	CWcs			*m_Wcs;
	CmpackImageData	*m_ImageData;
	CmpackChartData	*m_ChartData;
	CPopupMenu		*m_SelectMenu, *m_ContextMenu;
	int				m_SelectionIndex, m_MovingTarget;
	gint			m_StatusCtx, m_StatusMsg, m_LastFocus;
	gdouble			m_LastPosX, m_LastPosY;
	bool			m_Negative, m_RowsUpward, m_Ensemble, m_AllStars, m_InstMag, m_VSTags;
	bool			m_UpdatePos, m_ShowNewSelection, m_Updating;
	guint			m_TimerId;
	tCatalogSet     *m_CatalogBtns;
	int				m_CurrentCatalog, m_ImageWidth, m_ImageHeight;
	
	virtual bool OnCloseQuery(void) { return true; }

	void UpdateSelectionList(void);
	void SetCurrentSelection(const CSelection &sel);
	void SetNewSelection(const CSelection &sel);
	void UpdateSelection(CSelection &sel);
	void UpdateImage(void);
	void UpdateChart(void);
	void UpdateControls(void);
	void UpdateStatus(void);
	void Select(int star_id, CmpackSelectionType type);
	void Unselect(int star_id);
	void UnselectType(CmpackSelectionType type);
	void DettachSelection(void);
	void NewSelection(void);
	void SaveSelection(void);
	void RemoveSelection(void);
	void ShowSelection(int index);
	void EditTag(int row, int star_id);
	void SetTag(int row, int star_id, const gchar *tag);
	void RemoveTag(int row, int star_id);
	void ClearTags(void);

	// Set string in status bar
	void SetStatus(const char *text);

	void CopyWcsCoordinatesFromChart(int star_id);
	void CopyWcsCoordinates(CWcs *wcs, double lng, double lat);

	void UpdateObject(int row, int star_id);
	void UpdateAll(void);

	void UpdateCatalogs(GtkWindow *parent);
	void ShowCatalog(GtkWindow *parent, int index, bool show);
	void UpdateCatalog(GtkWindow *parent, int index);
	void EditCatalog(int index);

	void OnButtonClicked(GtkWidget *pButton);
	void OnSelectMenu(GdkEventButton *event, gint item);
	void OnContextMenu(GdkEventButton *event);
	void OnComboChanged(GtkComboBox *widget);
	bool OnResponseDialog(gint response_id);
	bool OnNameValidator(const gchar *value, GtkWindow *parent);
	bool OnTagValidator(const gchar *value, GtkWindow *parent);

	static void response_dialog(GtkDialog *pDlg, gint response_id, CEditSelectionsDlg *pMe);
	static void button_clicked(GtkWidget *pButton, CEditSelectionsDlg *pDlg);
	static void chart_item_activated(GtkWidget *pChart, gint item, CEditSelectionsDlg *pMe);
	static void combo_changed(GtkComboBox *widget, CEditSelectionsDlg *pDlg);
	static gint button_press_event(GtkWidget *widget, GdkEventButton *event, CEditSelectionsDlg *pMe);
	static bool name_validator(const gchar *value, GtkWindow *parent, CEditSelectionsDlg *pMe);
	static bool tag_validator(const gchar *value, GtkWindow *parent, CEditSelectionsDlg *pMe);
	static gboolean timer_cb(CEditSelectionsDlg *pMe);
	static void chart_mouse_moved(GtkWidget *button, CEditSelectionsDlg *pDlg);
	static void chart_mouse_left(GtkWidget *button, CEditSelectionsDlg *pDlg);
	static void AddToLayer(const char *objname, double ra, double dec, const char *catalog, const char *comment, void *data);

private:
	// Disable copy constructor and assignment operator
	CEditSelectionsDlg(const CEditSelectionsDlg&);
	CEditSelectionsDlg &operator=(const CEditSelectionsDlg&);
};

class CTextQueryDlg
{
public:
	// Validator function prototype
	typedef bool tValidator(const gchar *value, GtkWindow *parent, gpointer data);

public:
	// Constructor
	CTextQueryDlg(GtkWindow *pParent, const gchar *caption);

	// Destructor
	virtual ~CTextQueryDlg();

	// Execute the dialog
	gchar *Execute(const gchar *query, int maxsize, const gchar *defval,
		tValidator *validator, gpointer cb_data);

private:
	GtkWidget	*m_pDlg, *m_Query, *m_Entry;
	tValidator	*m_ValidatorProc;
	gpointer	m_ValidatorData;


	bool OnResponseDialog(gint response_id);
	bool OnCloseQuery(void);

	static void response_dialog(GtkDialog *pDlg, gint response_id, CTextQueryDlg *pMe);
};

#endif
