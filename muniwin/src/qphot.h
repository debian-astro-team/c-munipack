/**************************************************************

qphot.h (C-Munipack project)
Quick photometry
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef MUNIWIN_QPHOT_H
#define MUNIWIN_QPHOT_H

#include "image_class.h"
#include "ccdfile_class.h"
#include "infobox.h"

class CQuickPhotBox:public CInfoBox
{
public:
	// Constructor
	CQuickPhotBox(void);

	// Destructor
	virtual ~CQuickPhotBox(void);
	
	// Set reference to a chart view
	void SetChart(CmpackChartView *pView);

	// Set CCD frame parameters
	void SetParams(CCCDFile &pFile);

	// Clear photometry results
	void Clear(void);

	// Show photometry results
	void Update(const CImage *img, double x, double y);

protected:
	// Initialization before the tool is shown
	virtual void OnShow(void);

	// Clean up after the tool is hidden
	virtual void OnHide(void);

private:
	enum tResult {
		RES_OK,
		RES_ERR_POSITION,
		RES_ERR_APERTURE,
		RES_ERR_SKY_FAILED,
		RES_ERR_SKY_EMPTY,
		RES_ERR_SIGNAL_FAILED
	};

	GtkWidget		*m_Info, *m_Aperture, *m_InSky, *m_OutSky, *m_AlignCheckBox;
	GtkTextBuffer	*m_Buffer;
	CmpackChartView	*m_pChart;
	const CImage	*m_Image;
	CWcs			*m_Wcs;
	tResult			m_Result;
	int				m_CenterX, m_CenterY;
	double			m_MaxValue;
	double			m_ADCGain, m_AvgFrames, m_LoData, m_HiData;
	double			m_ApRadius, m_InnerSky, m_OuterSky;
	double			m_SkyMean, m_SkyDev;
	double			m_FwhmX, m_FwhmY, m_Fwhm;
	double			m_Signal, m_Noise;
	int				m_NSky, m_LayerId, m_ObjectId[4];
	int             m_alignMode;
	
	// Compute background level and signal
	bool sky(void);

	// Compute FWHM in horiontal axis
	bool full_width_x(double half);

	// Compute FWHM in vertical axis
	bool full_width_y(double half);

	// Compute background corrected integral height
	bool signal(void);

	// Find maximum pixel in neighbourhood
	bool findmax(int x, int y);

	// Recompute values
	void UpdateValues(void);

	// Update text area
	void UpdateText(void);

	// Update overlays
	void UpdateOverlays(void);

	// Aperture changed
	void OnValueChanged(GtkSpinButton *pBtn);

	// A check box toggled
	void OnButtonToggled(GtkToggleButton* pBtn);

	// Signal handlers
	static void value_changed(GtkSpinButton *spinbutton, CQuickPhotBox *pMe);
	static void button_toggled(GtkToggleButton* button, CQuickPhotBox* pMe);
};

#endif
