/**************************************************************

senddumpfiles.h (C-Munipack project)
Function that packs crash dump files
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_SEND_DUMP_FILES_H
#define CMPACK_SEND_DUMP_FILES_H

#ifdef _MSC_VER

// Returns true if there are any crash dumps available in
// the user's local application directory.
bool haveDumpFiles(void);

// Creates an archive with crash dumps from the user's local 
// application directory. The first parameter specify the path 
// of the target file. Ovewrites previous content of the files.
bool zipDumpFiles(const gchar *fpath, GError **error);

#endif

#endif
