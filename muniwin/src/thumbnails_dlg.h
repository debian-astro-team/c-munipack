/**************************************************************

thumbnails_dlg.h (C-Munipack project)
The 'Show thumbnails' dialog
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_THUMBNAILS_DLG_H
#define CMPACK_THUMBNAILS_DLG_H

#include <gtk/gtk.h>

#include "popup.h"

class CThumbnailsDlg
{
public:
	CThumbnailsDlg(GtkWindow *pParent);
	virtual ~CThumbnailsDlg();

	void ShowModal();

private:
	GtkWindow		*m_pParent;
	GtkWidget		*m_pDlg, *m_Scroller, *m_IconView, *m_SizeCombo;
	GtkToolItem		*m_RemoveBtn, *m_PropertiesBtn;
	GtkListStore	*m_IconSizes;
	GdkPixbuf		*m_pBadIcon;
	int				m_SizeIndex;
	bool			m_Negative, m_RowsUpward;
	CPopupMenu		m_Popup;

	bool OnResponseDialog(gint response_id);
	void OnSelectionChanged(GtkWidget *pWidget);
	void OnContextMenu(GtkWidget *widget, GdkEventButton *event);
	void OnButtonClicked(GtkToolItem *pWidget);

	bool RebuildData(void);
	int ProcessFiles(class CProgressDlg *sender);
	void ShowPreview();
	void ShowProperties();
	void RemoveFromProject();
	void UpdateControls();

	GdkPixbuf *MakeIcon(const gchar *filename);
	int SelectedCount(void);

	static void response_dialog(GtkDialog *pDlg, gint response_id, CThumbnailsDlg *pMe);
	static void selection_changed(GtkWidget *pWidget, CThumbnailsDlg *pMe);
	static void button_clicked(GtkToolItem *pWidget, CThumbnailsDlg *pMe);
	static gboolean button_press_event(GtkWidget *widget, GdkEventButton *event, CThumbnailsDlg *pMe);
	static int ExecuteProc(CProgressDlg *sender, void *userdata);
};

#endif
