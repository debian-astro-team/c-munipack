/**************************************************************

profile.h (C-Munipack project)
"Load Profile" dialog
Copyright (C) 2012 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef MUNIWIN_HELPER_DIALOGS_H
#define MUNIWIN_HELPER_DIALOGS_H

#include <gtk/gtk.h>

#include "profile.h"
#include "preview.h"

//
// Open project/file dialog
//
class COpenDlg
{
public:
	COpenDlg(GtkWindow *pParent, bool open_file);
	virtual ~COpenDlg();

	bool Execute(void);

	const gchar *Path(void) const
	{ return m_Path; }

private:
	bool		m_OpenFile;
	GtkWidget	*m_pDlg;
	CPreview	m_Preview;
	gchar		*m_Path;

	bool OnResponseDialog(gint response_id);
	void OnSelectionChanged(GtkFileChooser *chooser);
	void UpdatePreview(GtkFileChooser *chooser);
	void UpdateControls(void);

	static void update_preview(GtkFileChooser *chooser, COpenDlg *pMe);
	static void selection_changed(GtkFileChooser *chooser, COpenDlg *pMe);
	static void response_dialog(GtkDialog *pDlg, gint response_id, COpenDlg *pMe);
};

//
// Load project settings from a profile
//
class CLoadProfileDlg
{
public:
	CLoadProfileDlg(GtkWindow *pParent);
	virtual ~CLoadProfileDlg();

	bool Execute(CProfile &profile);

private:
	CProfile		m_Profile;
	GtkWidget		*m_pDlg, *m_TreeView, *m_OptionsBtn;
	GtkTreeStore	*m_Profiles;
	gchar			*m_Path;

	void UpdateProfiles(void);
	GtkTreePath *FindNode(const gchar *fpath, GtkTreeModel *model, GtkTreeIter *parent);
	void UpdateControls(void);

	bool OnResponseDialog(gint response_id);
	bool OnCloseQuery(void);
	void OnTreeViewSelectionChanged(GtkTreeSelection *pWidget);
	void OnButtonClicked(GtkButton *button);

	static void response_dialog(GtkWidget *widget, gint response_id, CLoadProfileDlg *pMe);
	static void tv_selection_changed(GtkTreeSelection *pWidget, CLoadProfileDlg *pMe);
	static void button_clicked(GtkButton *button, CLoadProfileDlg *pDlg);
};

//
// Import project settings from a project
//
class CImportProfileDlg
{
public:
	CImportProfileDlg(GtkWindow *pParent, tProjectType type);
	virtual ~CImportProfileDlg();

	bool Execute(CProfile &profile);

private:
	CProfile		m_Profile;
	tProjectType	m_Type;
	GtkWidget		*m_pDlg, *m_SrcEntry, *m_BrowseBtn, *m_RecentList;
	gchar			*m_Path;

	void BrowseForFile(void);

	bool OnResponseDialog(gint response_id);
	bool OnCloseQuery(void);
	void OnButtonClicked(GtkWidget *pBtn);
	void OnSelectionChanged(GtkRecentChooser *widget);
	gboolean OnRecentFilter(const GtkRecentFilterInfo *filter_info);

	static void response_dialog(GtkWidget *widget, gint response_id, CImportProfileDlg *pMe);
	static void button_clicked(GtkWidget *pButton, CImportProfileDlg *pDlg);
	static void rc_selection_changed(GtkRecentChooser *pWidget, CImportProfileDlg *pMe);
	static gboolean recent_filter(const GtkRecentFilterInfo *filter_info, CImportProfileDlg *pMe);
};

//
// Save project settings as a user-defined profile
//
class CSaveProfileDlg
{
public:
	enum tAction {
		SAVE_PROJECT_AS_PROFILE,
		SAVE_PROFILE_AS,
		RENAME_PROFILE
	};

	CSaveProfileDlg(GtkWindow *pParent, tAction action);
	virtual ~CSaveProfileDlg();

	gchar *Execute(const gchar *defaultName);

private:
	tAction			m_Action;
	GtkWidget		*m_pDlg, *m_TreeView, *m_SrcEntry, *m_OptionsBtn;
	GtkListStore	*m_Profiles;
	gchar			*m_Path;
	const gchar		*m_OldName;

	void UpdateProfiles(void);
	void UpdateControls(void);

	bool OnResponseDialog(gint response_id);
	bool OnCloseQuery(void);
	void OnSelectionChanged(GtkTreeSelection *widget);
	void OnButtonClicked(GtkButton *button);

	static void name_changed(GtkWidget *pEntry, CSaveProfileDlg *pMe) { pMe->UpdateControls(); }
	static void response_dialog(GtkWidget *widget, gint response_id, CSaveProfileDlg *pMe);
	static void tv_selection_changed(GtkTreeSelection *pWidget, CSaveProfileDlg *pMe);
	static void button_clicked(GtkButton *button, CSaveProfileDlg *pDlg);
};

#endif
