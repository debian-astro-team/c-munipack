/**************************************************************

masterflat_dlg.h (C-Munipack project)
The 'Master flat-frame' dialog
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_MASTER_FLAT_DLG_H
#define CMPACK_MASTER_FLAT_DLG_H

#include <gtk/gtk.h>

class CMasterFlatDlg
{
public:
	CMasterFlatDlg(GtkWindow *pParent);
	~CMasterFlatDlg();

	void Execute();

private:
	GtkWindow	*m_pParent;
	GtkWidget	*m_pDlg, *m_ProcFrame, *m_AllBtn, *m_SelBtn;
	GtkWidget	*m_OptionsBtn;
	int			m_InFiles;
	GList		*m_FileList;
	char		*m_FilePath;

	void EditPreferences(void);
	bool OnResponseDialog(gint response_id);
	void OnButtonClicked(GtkWidget *pBtn);
	int ProcessFiles(class CProgressDlg *sender);

	static void response_dialog(GtkDialog *pDlg, gint response_id, CMasterFlatDlg *pMe);
	static void button_clicked(GtkWidget *pButton, CMasterFlatDlg *pDlg);
	static int ExecuteProc(class CProgressDlg *sender, void *user_data);
};

#endif
