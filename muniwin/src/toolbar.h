/**************************************************************

toolbar.h (C-Munipack project)
Tool bar wrapper
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef _TOOLBAR_H
#define _TOOLBAR_H

#include <gtk/gtk.h>

#include "callback.h"

class CToolBar:public CCBObject
{
public:
	enum tToolBtnType
	{
		TB_PUSHBUTTON = 0,		// Push button
		TB_SEPARATOR,			// Separator
		TB_END = -1				// Last item
	};

	struct tToolBtn
	{
		tToolBtnType type;		// Button type
		int cmd_id;				// Command identifier
		const char *text;		// Caption text
		const char *icon;		// Icon name
		const char *ttip;		// Tooltip text
	};

	enum tMessageId
	{
		CB_ACTIVATE = 100		// Toolbar button clicked (wParam = command id)
	};

public:
	CToolBar();

	virtual ~CToolBar();

	void Create(const tToolBtn *btns);

	GtkWidget *Handle(void)
	{ return m_hBar; }

	void Show(int cmd_id, bool show);
	void Enable(int cmd_id, bool enable);

	void SetStyle(GtkToolbarStyle style);

private:
	int				m_Count;
	GtkWidget		*m_hBar;
	struct tToolBtnData	*m_Data;

	void OnPushEvent(int cmd_id);

	static void push_event(GtkWidget *widget, gpointer data);
};

#endif
