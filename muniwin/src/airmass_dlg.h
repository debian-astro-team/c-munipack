/**************************************************************

airmass_dlg.h (C-Munipack project)
Air mass coefficient dialog
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_AIRMASS_DLG_H
#define CMPACK_AIRMASS_DLG_H

#include <gtk/gtk.h>

class CAirMassDlg
{
public:
	CAirMassDlg(GtkWindow *pParent);
	~CAirMassDlg();

	void Execute();
private:
	bool			m_Updating;
	gdouble			m_JDValue;
	GtkWidget		*m_pDlg, *m_ObjBtn, *m_LocBtn, *m_AMass, *m_Alt;
	GtkWidget		*m_RA, *m_Dec, *m_Lon, *m_Lat, *m_JD, *m_UTC;
	bool			m_NotFilled, m_InvalidUTC;

	void Refresh(bool refresh_all);
	void EditObjectCoords(void);
	void EditLocation(void);

	bool OnResponseDialog(gint response_id);
	void OnButtonClicked(GtkWidget *widget);
	void OnEntryChanged(GtkWidget *pEntry);

	static void response_dialog(GtkDialog *pDlg, gint response_id, CAirMassDlg *pMe);
	static void button_clicked(GtkWidget *widget, CAirMassDlg *user_data);
	static void entry_changed(GtkWidget *pEntry, CAirMassDlg *pMe);
};

#endif
