/**************************************************************

project_dlg.h (C-Munipack project)
Project settions dialog
Copyright (C) 2012 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_PROFILES_DLG_H
#define CMPACK_PROFILES_DLG_H

#include "profile_editor.h"

//
// Profile editor widget
//
class CProfileEditor:public CEditProfileBase
{
public:
	CProfileEditor(class CProfilesDlg *pParent);
	virtual ~CProfileEditor();
	
	bool Set(const gchar *filePath, bool userDefined, GError **error);
	bool SaveChanges(GError **error);
	bool SaveAs(const gchar *filePath, GError **error);

protected:
	virtual GtkWidget *DialogWidget(void);
	virtual void OnPageChanged(tProfilePageId currentPage, tProfilePageId previousPage);
	virtual void CreatePages(GSList **stack);
	virtual void SetData(void);
	virtual bool IsPageVisible(tProfilePageId pageId);
	virtual void OnComboBoxChanged(GtkWidget *pWidget);
	virtual tProjectType projectType(void) const { return m_currentType; }

private:
	CProfilesDlg	*m_pParent;
	gchar			*m_Path, *m_Name;
	GtkWidget		*m_TypeCombo, *m_TypeBtn;
	tProjectType	m_currentType, m_origType;
	bool			m_UserDefined;
};


//
// Edit profiles dialog
//
class CProfilesDlg
{
public:
	CProfilesDlg(GtkWindow *pParent);
	~CProfilesDlg();

	void Execute(void);

private:
	friend class CProfileEditor;

	GtkWidget		*m_pDlg, *m_TreeView;
	GtkWidget		*m_SaveAsBtn, *m_RenameBtn, *m_RemoveBtn;
	GtkTreeStore	*m_Profiles;
	CProfileEditor	*m_pWidget;
	bool			m_Updating;
	gchar			*m_currentProfilePath;
	bool			m_currentProfileUserDefined;

	void UpdateProfiles(void);
	GtkTreePath *FindNode(const gchar *fpath, GtkTreeModel *model, GtkTreeIter *parent);
	GtkTreePath *FindFirst(GtkTreeModel *model, GtkTreeIter *parent);
	void UpdateControls(void);
	void SaveProfileAs(void);
	void RenameProfile(void);
	void RemoveProfile(void);

	bool OnResponseDialog(gint response_id);
	bool OnCloseQuery(void);
	void OnSelectionChanged(GtkTreeSelection *pWidget);
	void OnTypeChanged(tProjectType type);
	void OnButtonClicked(GtkWidget *widget);

	static void response_dialog(GtkWidget *widget, gint response_id, CProfilesDlg *pMe);
	static void selection_changed(GtkTreeSelection *pWidget, CProfilesDlg *pMe);
	static void button_clicked(GtkWidget *pButton, CProfilesDlg *pDlg);

private:
	// Disable copy constructor and assignment operator
	CProfilesDlg(const CProfilesDlg&);
	CProfilesDlg& operator=(const CProfilesDlg&);
};

#endif
