/**************************************************************

callback.h (C-Munipack project)
Base class for objects which register callbacks
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef MUNIWIN_CALLBACK_H
#define MUNIWIN_CALLBACK_H

class CCBObject
{
public:
	// Callback procedure type
	// params:
	//	sender			- [in] object who sends the message
	//	message			- [in] message code
	//	wparam, lparam	- [in] two message specific parameters
	//	cb_data			- [in] user callback data
	typedef void CBProc(CCBObject *sender, int message, int wparam, void* lparam, void* cb_data);

public:
	// Constructor
	CCBObject(void);

	// Destructor
	virtual ~CCBObject(void);

	// Register callback procedure
	void RegisterCallback(CBProc *cb_proc, void* cb_data);

	// Unregister callback procedure
	void UnregisterCallback(CBProc *cb_proc);

protected:
	// Broadcast message (synchronous)
	void Callback(int message, int wparam = 0, void* lparam = 0);

private:
	struct tCBData *m_List;	// List of registered callbacks
};

#endif
