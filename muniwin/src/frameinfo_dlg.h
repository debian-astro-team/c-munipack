/**************************************************************

preview_dlg.h (C-Munipack project)
The preview dialog
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_FRAMEINFO_DLG_H
#define CMPACK_FRAMEINFO_DLG_H

#include "info_dlg.h"
#include "ccdfile_class.h"
#include "phot_class.h"

class CFrameInfoDlg:public CInfoDlg
{
public:
	// Constructor
	CFrameInfoDlg(GtkWindow *pParent);

	// Destructor
	virtual ~CFrameInfoDlg();

	// Display dialog
	void Show(GtkTreePath *pPath);

private:
	GtkTreeRowReference *m_pFile;
	GtkWidget *m_OrigHdrBtn, *m_TempHdrBtn, *m_PhotHdrBtn;
	GtkWidget *m_OrigWcsBtn, *m_TempWcsBtn, *m_PhotWcsBtn;
	GtkWidget *m_OrigFileLbl, *m_TempFileLbl, *m_PhotFileLbl;

	void OnButtonClicked(GtkWidget *pBtn);
	void ShowHeader_Orig(void);
	void ShowHeader_Temp(void);
	void ShowHeader_Phot(void);
	void ShowWcsData_Orig(void);
	void ShowWcsData_Temp(void);
	void ShowWcsData_Phot(void);

	CCCDFile *LoadOrig(GError **error);
	CCCDFile *LoadTemp(GError **error);
	CPhot *LoadPhot(GError **error);

	static void button_clicked(GtkWidget *pButton, CFrameInfoDlg *pDlg);
};

#endif
