/**************************************************************

ccdfile_class.h (C-Munipack project)
CCD image class interface
Copyright (C) 2011 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef MUNIWIN_IMAGE_CLASS_H
#define MUNIWIN_IMAGE_CLASS_H

#include <gdk/gdkpixbuf.h>

#include "helper_classes.h"

//
// CCD image class
//
class CImage
{
public:
	// Constructor
	CImage(CmpackImage *handle = NULL):m_Handle(handle), m_AutoRangeValid(false), m_Range(0), m_BlackLevel(0) {}

	// Set image parameters
	CImage(int width, int height, CmpackBitpix depth);

	// Destructor
	virtual ~CImage();

	// Apply source file transformations
	void TransformOrigFrame(void);

	// Transpose image
	void Transpose(bool hflip, bool vflip);

	// Is image valid?
	bool Valid(void) const
	{ return m_Handle!=NULL; }

	// Image width in pixels
	int Width(void) const;
	
	// Image height in pixels
	int Height(void) const;

	// Get image depth identifier
	CmpackBitpix Depth(void) const;

	// Automatic brightness contrast result
	double Range(void);
	double BlackLevel(void);

	// Assign to image data object
	CmpackImageData *ToImageData(bool invert, bool pseudocolors, bool badpixels, bool reverse_y);

	// Draw image into pixbuf
	GdkPixbuf *ToPixBuf(bool invert, bool pseudocolors, bool badpixels, bool reverse_y);

	// Get pixel value
	double getPixel(int x, int y) const;

	// Compute pixel value range
	bool MinMax(double *minvalue, double *maxvalue) const;

	// Compute mean and standard deviation
	bool MeanDev(double *mean, double *stddev) const;

	// Get profile
	CmpackGraphData *Profile(int x0, int y0, int x1, int y1, 
		double *ymin, double *ymax, double *mean, double *stddev) const;

	// Get pixel value range
	CmpackGraphData *Histogram(int length, double channel_width,
		double zero_offset, double *ymax, bool logScale) const;

	// Load image from a CCD file
	static CImage *fromFile(const gchar *filepath, const CProfile* profile, CmpackBitpix bitpix = CMPACK_BITPIX_AUTO, GError **error = 0);

protected:
	friend class CCCDFile;

	CmpackImage		*m_Handle;
	bool			m_AutoRangeValid;
	double			m_Range, m_BlackLevel;

	// Draw image into cairo_surface
	void Paint(cairo_surface_t *target, bool invert, bool pseudocolors, bool badpixels, bool reverse_y);

	// Draw image into cairo_surface
	void Paint(GdkPixbuf *target, bool invert, bool pseudocolors, bool badpixels, bool reverse_y);

	// Updates range and blacklevel
	void AutoRange(void);

private:
	// Disable copy constructor and assigment operator
	CImage(const CImage&);
	CImage &operator=(const CImage &);
};

#endif
