/**************************************************************

menubar.h (C-Munipack project)
Menu wrapper
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef _MENUBAR_H
#define _MENUBAR_H

#include <gtk/gtk.h>

#include "callback.h"

class CMenuBar:public CCBObject
{
public:
	enum tMenuItemType
	{
		MB_ITEM,					// Normal item
		MB_CHECKBTN,				// Check button
		MB_RADIOBTN,				// Radio button
		MB_SEPARATOR,				// Separator
		MB_SUBMENU,					// Sub menu
		MB_RECENTMENU,				// Recent chooser menu
		MB_END						// Last item
	};

	enum tMenuItemFlags
	{
		MBF_RIGHT = (1 << 0)		// Right justify
	};

	struct tMenuItem
	{
		tMenuItemType type;			// Menu item type
		int cmd_id;					// Command identifier
		const char *text;			// Displayed text
		const char *icon;			// Displayed icon
		const tMenuItem *submenu;	// Submenu items
		const char *rc_group;		// Recent menu filter - group
		int flags;					// Bitmask of flags
	};

	struct tMenu
	{
		const char *text;			// Menu caption
		int menu_id;				// Menu identifier
		const tMenuItem *items;		// Menu items
		int flags;					// Bitmask of flags
	};

	enum tMessageId
	{
		CB_ACTIVATE = 200,			// Menu item clicked (wParam = command id)
		CB_RECENT_ACTIVATE = 201,	// Recent menu item activated (wParam = command id, lParam = path)
	};

public:
	CMenuBar();

	virtual ~CMenuBar();

	void Create(const tMenu *menus, bool icons = true);

	GtkWidget *Handle(void)
	{ return m_hBar; }

	void ShowMenu(int menu_id, bool enable);
	void Show(int cmd_id, bool show);
	void EnableMenu(int menu_id, bool enable);
	void Enable(int cmd_id, bool enable);
	void Check(int cmd_id, bool check);
	bool IsChecked(int cmd_id);

private:
	bool		m_Updating;
	GtkWidget	*m_hBar;
	GSList		*m_Data;

	void OnActivate(struct tMenuData *data);
	void OnRecentActivated(GtkRecentChooser *widget, struct tMenuData *data);

	struct tMenuData *CreateMenu(const tMenu *menu, bool icons, tMenuData *parent);
	struct tMenuData *CreateItem(const tMenuItem *item, bool icons, tMenuData *parent);
	bool ShowItem(tMenuData *data, int cmd_id, bool show);
	bool EnableItem(tMenuData *data, int cmd_id, bool enable);
	bool CheckItem(tMenuData *data, int cmd_id, bool check, GList *ptr);
	bool IsItemChecked(tMenuData *data, int cmd_id, bool *check);
	static void FreeItem(struct tMenuData *menu, void *user_data);

	static void item_activate(GtkWidget *widget, tMenuData *data);
	static void recent_item_activated(GtkRecentChooser *chooser, tMenuData *data);
};

#endif
