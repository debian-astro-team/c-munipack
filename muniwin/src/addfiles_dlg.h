/**************************************************************

addfiles_dlg.h (C-Munipack project)
The 'Add files' dialog
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_ADDFILES_DLG_H
#define CMPACK_ADDFILES_DLG_H

#include <gtk/gtk.h>

#include "preview.h"

class CAddFilesDlg
{
public:
	// Constructor
	CAddFilesDlg(GtkWindow *pParent);

	// Destructor
	virtual ~CAddFilesDlg();

	// Execute the dialog
	void Execute();

private:
	GtkWidget	*m_pDlg;
	CPreview	m_Preview;

	void OnUpdatePreview(GtkFileChooser *pChooser);
	bool OnResponseDialog(gint response_id);

	static void update_preview(GtkFileChooser *pChooser, CAddFilesDlg *pMe);
	static void response_dialog(GtkDialog *pDlg, gint response_id, CAddFilesDlg *pMe);
};

#endif
