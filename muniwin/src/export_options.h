/**************************************************************

ccd_image_size.h (C-Munipack project)
Widget that allows an user to define a size of exported image
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_EXPORT_OPTIONS_H
#define CMPACK_EXPORT_OPTIONS_H

#include <gtk/gtk.h>

#include "callback.h"
#include "helper_classes.h"

// Default image type
#define DEFAULT_IMAGE_TYPE "image/png"

// Caption (human readable format description) for given image mime type
const gchar *ImageFileCaption(const gchar *type);

// File extension for given image mime type
const gchar *ImageFileExtension(const gchar *type);

// Determine image mime type by file extension
const gchar *ImageTypeByExtension(const gchar *fpath);

// File filter for given image mime type
GtkFileFilter *ImageFileFilter(const gchar *type);

//
// Export chart options
//
class CExportChartOptions:public CCBObject
{
public:
	enum tMessageId
	{
		CB_TYPE_CHANGED
	};

	enum tSizeMode
	{
		SM_ABSOLUTE, SM_RELATIVE
	};

	CExportChartOptions();

	virtual ~CExportChartOptions();

	GtkWidget *Handle(void)
	{ return m_Frame; }

	void Init(int img_width, int img_height, tSizeMode mode, 
		const gchar *type, double zoom, int dst_width, int jpeg_quality);

	int SourceWidth(void) const
	{ return m_ChartWidth; }

	int SourceHeight(void) const
	{ return m_ChartHeight; }

	tSizeMode Mode(void) const
	{ return m_Mode; }

	const gchar *Type(void) const { return FileTypeToMimeType(m_FileType); }

	double Zoom(void);

	int OutputWidth(void);

	int OutputHeight(void);

	int JpegQuality(void) const;

private:
	enum tFileType
	{
		TYPE_PNG,
		TYPE_JPEG,
		TYPE_N_ITEMS			// Number of file formats
	};

	GtkWidget		*m_Relative, *m_Absolute, *m_Frame, *m_TypeCombo;
	GtkWidget		*m_ZoomAdj, *m_AbsHeight, *m_AbsWidth, *m_Percents;
	GtkWidget		*m_HeightLabel, *m_Pixels, *m_JpegQLabel, *m_JpegQuality, *m_JpegQUnit;
	GtkListStore	*m_FileTypes;
	tFileType		m_FileType;
	tSizeMode		m_Mode;
	int				m_ChartWidth, m_ChartHeight;
	int				m_ImageWidth, m_ImageHeight;
	bool			m_Changed, m_Updating;
	double			m_Zoom, m_ZoomMin, m_ZoomMax;

	void OnValueChanged(GtkSpinButton *pButton);
	void OnSelectionChanged(GtkComboBox *pWidget);
	void OnToggled(GtkToggleButton *pButton);
	void UpdateData(void);
	void UpdateControls(void);

	static const gchar *FileTypeCaption(tFileType type) { return ImageFileCaption(FileTypeToMimeType(type)); }
	static const gchar *FileTypeToMimeType(tFileType type);
	static tFileType MimeTypeToFileType(const gchar *type);

	static void value_changed(GtkSpinButton *spinbutton, CExportChartOptions *pMe);
	static void selection_changed(GtkComboBox *pWidget, CExportChartOptions *pMe);
	static void toggled(GtkToggleButton *togglebutton, CExportChartOptions *pMe);
};

#endif
