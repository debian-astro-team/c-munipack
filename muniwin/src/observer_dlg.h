/**************************************************************

observer_dlg.h (C-Munipack project)
The 'Enter observer coordinates' dialog
Copyright (C) 2009 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_OBSERVER_DLG_H
#define CMPACK_OBSERVER_DLG_H

#include <gtk/gtk.h>

#include "popup.h"
#include "locations.h"
#include "callback.h"

class CLocationDlg
{
public:
	CLocationDlg(GtkWindow *pParent);
	virtual ~CLocationDlg();

	bool Execute(CLocation *pCoords, bool no_default = false);

private:
	// Selection modes
	enum tSelectMode {
		DEFAULT_LOCATION,
		MANUAL_ENTRY,
		REFERENCE_FRAME
	};

	CLocation		*m_pCoords;
	bool			m_Updating, m_UserFileChanged, m_UserEntryChanged;
	bool			m_DefEntryModified;
	GtkTreePath		*m_SelPath;
	GtkWidget		*m_pDlg, *m_Select, *m_RefBtn, *m_DefBtn, *m_ManBtn;
	GtkWidget		*m_EntryBox, *m_EntryName, *m_EntryLon, *m_EntryLat, *m_EntryRem;
	GtkWidget		*m_DefBox, *m_DefName, *m_DefLon, *m_DefLat;
	GtkWidget		*m_RefBox, *m_RefName, *m_RefLon, *m_RefLat;
	GtkWidget		*m_UserView, *m_AddBtn, *m_SaveBtn, *m_DelBtn;
	tSelectMode		m_SelectMode;
	CLocations		m_UserList;
	CPopupMenu		m_UserMenu;

	void SetUserData(const CLocation *data);
	void SetRefData(const CLocation *data);
	bool GetData(CLocation *data, bool name_required);
	void SetDefData(const CLocation &data);
	void SetSelectMode(tSelectMode mode);
	void UpdateControls(void);
	void ImportFromFile(void);
	void ExportToFile(void);
	void AddToTable(void);
	void SaveToTable(void);
	void RemoveFromTable(void);
	void SelectAll(void);

	bool OnResponseDialog(gint response_id);
	void OnButtonClicked(GtkWidget *pBtn);
	void OnUsrEntryChanged(GtkWidget *pEntry);
	void OnDefEntryChanged(GtkWidget *pEntry);
	void OnContextMenu(GtkWidget *widget, GdkEventButton *event);
	void OnRowActivated(GtkTreeView *tree_view, GtkTreePath *path);
	void OnSelectionChanged(GtkTreeSelection *widget);

	static void response_dialog(GtkDialog *pDlg, gint response_id, CLocationDlg *pMe);
	static void button_clicked(GtkWidget *pButton, CLocationDlg *pMe);
	static void usr_entry_changed(GtkWidget *pEntry, CLocationDlg *pMe);
	static void def_entry_changed(GtkWidget *pEntry, CLocationDlg *pMe);
	static void row_activated(GtkTreeView *treeview, GtkTreePath *path, GtkTreeViewColumn *column, CLocationDlg *pMe);
	static void selection_changed(GtkTreeSelection *widget, CLocationDlg *pMe);
	static gint button_press_event(GtkWidget *widget, GdkEventButton *event, CLocationDlg *pMe);
};

#endif
