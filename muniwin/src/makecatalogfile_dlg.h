/**************************************************************

makecatalog_dlg.h (C-Munipack project)
The 'Make catalog file' dialog
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_MAKECAT_DLG_H
#define CMPACK_MAKECAT_DLG_H

#include <gtk/gtk.h>

#include "cmpack_widgets.h"
#include "ccdfile_class.h"
#include "phot_class.h"
#include "catfile_class.h"

//
// Make catalog file 
//
class CMakeCatFileDlg
{
public:
	// Constructor
	CMakeCatFileDlg(GtkWindow *pParent);

	// Destructor
	~CMakeCatFileDlg();

	// Execute the dialog
	void Execute();

private:
	GtkWindow		*m_pParent;
	GtkWidget		*m_pDlg, *m_ObjBtn, *m_LocBtn, *m_Chart, *m_SelectCbx;
	GtkWidget		*m_ObjName, *m_RA, *m_Dec, *m_Observer, *m_LocName;
	GtkWidget		*m_Lon, *m_Lat, *m_Telescope, *m_Camera, *m_Filter;
	GtkWidget		*m_FOV, *m_Orientation, *m_Notes;
	GtkWidget		*m_FileName, *m_Path, *m_PathBtn;
	GtkListStore	*m_Selections;
	GtkToolItem		*m_EditSel;
	CObjectCoords	m_Object;
	CLocation		m_Location;
	CCatalog		m_OutFile;
	CSelectionList	m_SelectionList;
	CTags			m_Tags;
	CmpackChartData	*m_ChartData;
	int				m_ApertureIndex, m_SelectionIndex;
	bool			m_Negative, m_RowsUpward;
	bool			m_Updating;

	void UpdateChart(void);
	void UpdateObjectCoords(void);
	void EditObjectCoords(void);
	void UpdateLocation(void);
	void EditLocation(void);
	void ChangeCatalogPath(void);
	void EditSelectionList(void);
	void UpdateSelectionList(void);

	bool OnCloseQuery(void);
	bool OnResponseDialog(gint response_id);
	void OnButtonClicked(GtkWidget *widget);
	void OnComboChanged(GtkComboBox *widget);
	void OnEntryChanged(GtkEntry *widget);

	bool MakeFile(const gchar *exportPath, GError **error);

	static void response_dialog(GtkDialog *pDlg, gint response_id, CMakeCatFileDlg *pMe);
	static void button_clicked(GtkWidget *widget, CMakeCatFileDlg *pMe);
	static void combo_changed(GtkComboBox *widget, CMakeCatFileDlg *pMe);
	static void entry_changed(GtkEntry *widget, CMakeCatFileDlg *pMe);
};

#endif
