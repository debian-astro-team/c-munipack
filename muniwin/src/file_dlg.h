/**************************************************************

filedlg_base.h (C-Munipack project)
The base class for file preview dialogs
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_FILE_DLG_H
#define CMPACK_FILE_DLG_H

#include <gtk/gtk.h>

//
// Base class for non-modal file preview windows
//
class CFileDlg
{
public:
	// Open specified file. If the file is already open, it shows the dialog.
	// Otherwise, it creates a new file dialog and shows it.
	static CFileDlg *Open(GtkWindow *pParent, const gchar *fpath);

	// Load the file
	bool Load(GtkWindow *pParent, const char *path, GError **error);

	// Close the dialog
	void Close(void);

	// Show non-modal dialog
	void Show();

	// Get path to the file
	const gchar *FilePath(void) const { return m_Path; }

	// Unsaved data ?
	bool DataSaved(void) const { return !m_NotSaved; }

	// Environment changed, reload settings
	virtual void EnvironmentChanged(void) {}

protected:
	GtkWidget	*m_pDlg, *m_MainBox, *m_Status;
	gint		m_StatusCtx, m_StatusMsg;
	gchar		*m_Path, *m_Name;
	bool		m_NotSaved;

	// Constructor
	CFileDlg(void);

	// Destructor
	virtual ~CFileDlg();

	// Get name of icon that shall be displayed in the top of the window
	// The default implementation returns NULL which means the application icon
	virtual const char *GetIconName(void) { return NULL; }

	// Load the file
	virtual bool LoadFile(GtkWindow *pParent, const gchar *path, GError **error) = 0;

	// Save a file
	virtual bool SaveFile(const gchar *path, GError **error) = 0;

	// Dialog initialization
	virtual void OnInitDialog(void);

	// Can close the dialog?
	virtual bool OnCloseQuery(void);

	// Update dialog controls
	virtual void UpdateControls(void) {}
		
	// Save to another file
	void SaveAs(const gchar *filterName, const gchar *filterPattern);

	// Update window title
	void UpdateTitle(void);

	// Change path
	void SetPath(const char *path);

	// Set string in status bar
	void SetStatus(const char *text);

	// Open another file
	void Open(void);

private:
	static void destroyed(GtkObject *pWnd, CFileDlg *pDlg);
	static void realized(GtkWidget *widget, CFileDlg *pDlg);
	static gboolean delete_event(GtkWidget *widget, GdkEvent *event, CFileDlg *pDlg);
};

#endif
