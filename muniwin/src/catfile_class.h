/**************************************************************

catfile_class.h (C-Munipack project)
Catalog file class interface
Copyright (C) 2011 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef MUNIWIN_CATFILE_CLASS_H
#define MUNIWIN_CATFILE_CLASS_H

#include "helper_classes.h"
#include "phot_class.h"

//
// Catalogue file class
//
class CCatalog
{
public:
	// Export flags
	enum tExportFlags {
		EXPORT_SKIP_INVALID		= (1<<0),
		EXPORT_NO_HEADER		= (1<<1)
	};

	// Table columns (also sorting columns)
	enum tColumnId {
		COL_ID, COL_POS_X, COL_POS_Y, COL_WCS_VALID, COL_WCS_LNG, COL_WCS_LAT, 
		COL_MAG_VALID, COL_MAG, COL_SEL_CAPTION, COL_SEL_TYPIDX, COL_TAGS, 
		COL_FG_COLOR, COL_COUNT
	};

	// Constructor(s)
	CCatalog():m_Handle(NULL), m_CacheFlags(0), m_RefMag(0), m_CurrentSelection(-1), 
		m_Wcs(NULL) {}

	// Constructor with intialization (makes its own reference)
	CCatalog(CmpackCatFile *handle);

	// Destructor
	virtual ~CCatalog();

	// Create file stored in memory
	void Create(void);

	// Load data from a file.
	// The function opens a file, makes internal copy and closes it.
	bool Load(const gchar *fpath, GError **error = NULL);

	// Create a new catalog file from a photometry file
	bool Create(const CPhot &pht, GError **error = NULL);

	// Save catalog file to a file
	bool SaveAs(const gchar *fpath, GError **error = NULL) const;

	// Make deep copy of another catalog file
	bool MakeCopy(const CCatalog &cat, GError **error = NULL);

	// Release all data keps the memory
	void Clear(void);

	// Is the object valid?
	bool Valid(void) const
	{ return m_Handle!=NULL; }

	// Get header field by index
	bool GetParam(int index, const char **key, const char **val, const char **com) const;
	
	// Chart width in pixels
	int Width(void) const;

	// Chart height in pixels
	int Height(void) const;

	// Date of observation
	bool DateTime(CmpackDateTime *dt) const;
	double JulianDate(void) const;

	// Exposure duration
	double ExposureDuration(void) const;

	// Frame properties
	const gchar *Filter(void) const;
	const gchar *Observer(void) const;
	const gchar *Telescope(void) const;
	const gchar *Instrument(void) const;
	const gchar *FieldOfView(void) const;
	const gchar *Orientation(void) const;
	const gchar *Notes(void) const;

	void SetFilter(const gchar *filter);
	void SetObserver(const gchar *observer);
	void SetTelescope(const gchar *telescope);
	void SetInstrument(const gchar *instrument);
	void SetFieldOfView(const gchar *fov);
	void SetOrientation(const gchar *orientation);
	void SetNotes(const gchar *notes);

	// Number of stars
	int ObjectCount(void) const;

	// Find object
	int FindObject(int id);

	// Get star identifier
	int GetObjectID(int star_index) const;

	// Get star parameters
	bool GetObjectParams(int star_index, unsigned mask, CmpackCatObject *obj) const;

	// Remove all selections
	void RemoveAllSelections(void);

	// Add selection
	void AddSelection(const gchar *name, const CSelection &selection);

	// Get selection map
	const CSelectionList *Selections(void);

	// Set current selection index
	void SelectSelection(int selectionIndex);

	// Get index of current selection
	int CurrentSelectionIndex(void) const { return m_CurrentSelection; }

	// Change tags
	void SetTags(const CTags &tags);

	// Get tags
	const CTags *Tags(void);

	// Get WCS data
	CWcs *Wcs(void);

	// Change object coordinates
	void SetObject(const CObjectCoords &obj);

	// Get object coordinates
	const CObjectCoords *Object(void);

	// Change object coordinates
	void SetLocation(const CLocation &loc);

	// Get location
	const CLocation *Location(void);

	// Make chart data
	CmpackChartData *ToChartData(bool show_selection, bool show_tags, bool transparent);

	// Make table model
	GtkTreeModel *ToTreeModel(bool show_selection, bool show_tags);

	// Set view parameters
	void SetView(GtkTreeView *view);

	// Make table model
	bool ExportTable(const gchar *filepath, const gchar *format, unsigned flags, int sort_column_id,
		GtkSortType sort_order, GError **error);

	// Export file header
	bool ExportHeader(const gchar *filepath, const gchar *format, unsigned flags, GError **error) const;

protected:
	enum tCacheFlags {
		CF_SELECTIONS	= (1<<0),
		CF_REF_MAG		= (1<<1),
		CF_OBJECT		= (1<<2), 
		CF_LOCATION		= (1<<3),
		CF_TAGS			= (1<<4),
		CF_WCS			= (1<<5)
	};

	CmpackCatFile	*m_Handle;
	unsigned		m_CacheFlags;
	double			m_RefMag;
	CSelectionList	m_Selections;
	int				m_CurrentSelection;
	CTags			m_Tags;
	CObjectCoords	m_Object;
	CLocation		m_Location;
	CWcs			*m_Wcs;

	// Refresh cached data
	void InvalidateCache(void);

	// Compute reference magnitude for given aperture
	double GetRefMag(void);

	// Load selection
	void LoadSelection(CSelection &selection, CmpackSelectionType type);

	// Save selection
	void SaveSelection(const CSelection &selection, CmpackSelectionType type) const;

	// Export table in CSV format
	void ExportTable(CCSVWriter &writer, unsigned flags, int sort_column_id, GtkSortType sort_order);

	// Export table in CSV format
	void ExportHeader(CCSVWriter &writer, unsigned flags) const;

private:
	// Disable copy constructor and assigment operator
	CCatalog(const CCatalog &cat);
	CCatalog &operator=(const CCatalog &orig);
};

#endif
