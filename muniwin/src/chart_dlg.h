/**************************************************************

chart_dlg.h (C-Munipack project)
The 'Plot chart' dialog
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/
#ifndef CMPACK_CHART_DLG_H
#define CMPACK_CHART_DLG_H

#include <gtk/gtk.h>

#include "cmpack_widgets.h"
#include "lightcurve_dlg.h"
#include "phot_class.h"
#include "ccdfile_class.h"
#include "catfile_class.h"
#include "image_class.h"

class CChartDlg
{
public:
	// Constructor
	CChartDlg(GtkWindow *pParent);

	// Destructor
	virtual ~CChartDlg();

	// Execute the dialog
	void Execute(const CSelection &sel, int aperIndex, const gchar *default_file_name);

private:
	// Display modes
	enum tDisplayMode {
		DISPLAY_IMAGE,
		DISPLAY_CHART,
		DISPLAY_FULL
	};

	GtkWindow		*m_pParent;
	GtkWidget		*m_pDlg, *m_Chart;
	GtkToolItem		*m_ShowImage, *m_ShowChart, *m_ShowMixed;
	GtkToolItem		*m_ZoomIn, *m_ZoomOut, *m_ZoomFit, *m_SaveBtn;
	tDisplayMode	m_DisplayMode;
	bool			m_Negative, m_RowsUpward;
	CPhot			m_Phot;
	CCatalog		m_Catalog;
	CImage			*m_Image;
	CSelection		m_Selection;
	CTags			m_Tags;
	CmpackChartData	*m_ChartData;
	CmpackImageData	*m_ImageData;
	const gchar		*m_DefaultFileName;

	void UpdateChart(void);
	void UpdateImage(void);
	void SaveChart(void);

	bool OnResponseDialog(gint response_id);
	void OnButtonClicked(GtkWidget *pButton);

	static void response_dialog(GtkWidget *widget, gint response_id, CChartDlg *pMe);
	static void button_clicked(GtkWidget *pButton, CChartDlg *pDlg);
};

#endif
