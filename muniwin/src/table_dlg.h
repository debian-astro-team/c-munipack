/**************************************************************

table_dlg.h (C-Munipack project)
The preview dialog for tables
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_TABLE_DLG_H
#define CMPACK_TABLE_DLG_H

#include "file_dlg.h"
#include "info_dlg.h"
#include "cmpack_widgets.h"
#include "menubar.h"
#include "infobox.h"
#include "table_class.h"
#include "measurement.h"
#include "graph_toolbox.h"

//
// Non-modal file viewer
//
class CTableDlg:public CFileDlg
{
public:
	// Constructor
	CTableDlg(void);

	// Destructor
	virtual ~CTableDlg();

protected:
	// Get name of icon
	virtual const char *GetIconName(void) { return "muniwin"; }

	// Load a file
	virtual bool LoadFile(GtkWindow *pParent, const char *fpath, GError **error);

	// Save a file
	virtual bool SaveFile(const char *fpath, GError **error) { return false; }

	// Update dialog controls
	virtual void UpdateControls(void);

private:
	enum tDisplayMode {
		DISPLAY_GRAPH,
		DISPLAY_TABLE
	};

	enum tInfoMode {
		INFO_NONE,
		INFO_STATISTICS,
		INFO_MEASUREMENT
	};

	GtkWidget		*m_XLabel, *m_XCombo, *m_YLabel, *m_YCombo, *m_ZoomLabel;
	GtkWidget		*m_GraphScrWnd, *m_GraphView, *m_TableScrWnd, *m_TableView;
	GtkToolItem		*m_ZoomFit, *m_ZoomIn, *m_ZoomOut; 
	CMenuBar		m_Menu;
	CTable			m_File;
	GtkListStore	*m_XChannels, *m_YChannels;
	bool			m_Updating, m_UpdatePos, m_ShowErrors, m_ShowGrid;
	bool			m_LastPosValid;
	int				m_ChannelX, m_ChannelY;
	double			m_LastPosX, m_LastPosY;
	tIndex			m_LastFocus;
	int				m_TimerId;
	CTextBox		m_InfoBox;
	tDisplayMode	m_DispMode;
	tInfoMode		m_InfoMode;
	CMeasurementBox	m_MeasBox;
	CmpackGraphData	*m_GraphData;
	GtkTreeModel	*m_TableData;
	bool			m_ShowToolBox;
	CGraphToolBox	m_ToolBox;

	void UpdateChannels(void);
	void UpdateGraphTable(gboolean autozoom_x, gboolean autozoom_y);
	void UpdateStatus(void);
	void UpdateTools(void);
	void Export(void);
	void ShowProperties(void);
	void SetDisplayMode(tDisplayMode mode);
	void SetInfoMode(tInfoMode mode);
	void ShowToolBoxMode(bool show);
	void PrintValue(char *buf, double val, const CChannel *channel);
	void PrintKeyValue(char *buf, double val, const CChannel *channel);
	void PrintKeyValueU(char *buf, double val, double sdev, const CChannel *channel);

	void OnCommand(int cmd_id);
	void OnMouseMoved(void);
	void OnButtonClicked(GtkWidget *pButton);
	void OnComboChanged(GtkComboBox *pWidget);
	void OnSelectionChanged(void);
	void OnInfoBoxClosed(void);
	void OnToolBoxClosed(void);

	static void button_clicked(GtkWidget *pButton, CTableDlg *pDlg);
	static void mouse_moved(GtkWidget *pGraph, CTableDlg *pMe);
	static void mouse_left(GtkWidget *pGraph, CTableDlg *pMe);
	static void combo_changed(GtkComboBox *pButton, CTableDlg *pMe);
	static gboolean timer_cb(CTableDlg *pMe);
	static void selection_changed(GtkWidget *pChart, CTableDlg *pMe);
	static void MenuCallback(CCBObject *sender, int message, int wparam, void* lparam, void* cb_data);
	static void InfoBoxCallback(CCBObject *sender, int message, int wparam, void* lparam, void* cb_data);
	static void ToolBoxCallback(CCBObject *sender, int message, int wparam, void* lparam, void* cb_data);
};


//
// File properties dialog
//
class CTableInfoDlg:public CInfoDlg
{
public:
	// Constructor
	CTableInfoDlg(GtkWindow *pParent);

	// Display dialog
	void ShowModal(const CTable *data, const gchar *name, const gchar *path);

private:
	const CTable *m_File;
	GtkWidget	*m_HdrBtn;
	const gchar	*m_Name;

	void OnButtonClicked(GtkWidget *pBtn);
	void ShowHeader(void);

	static void button_clicked(GtkWidget *pButton, CTableInfoDlg *pDlg);
};

//
// Save light curve
//
class CExportTableFileDlg
{
public:
	// Constructor
	CExportTableFileDlg(GtkWindow *pParent, CmpackTableType tableType);

	// Destructor
	virtual ~CExportTableFileDlg();

	// Execute the dialog
	bool Execute(const CTable &table, const gchar *current_path, int channelX, int channelY);

private:
	enum tFileType {
		TYPE_AVE,
		TYPE_MCV,
		TYPE_CSV,
		TYPE_TEXT,
		TYPE_N_ITEMS,			// Number of file formats
		TYPE_INVALID = -1
	};

	enum tDateType {
		DATE_JD_GEOCENTRIC,
		DATE_JD_HELIOCENTRIC,
		DATE_N_ITEMS,			// Number of JD formats
		DATE_INVALID = -1
	};

	struct tOptions {
		bool helcor, airmass, altitude, all_values;
		bool skip_invalid, zero_invalid, header, errors;
	};
	
	GtkWindow		*m_pParent;
	GtkWidget		*m_pDlg, *m_TypeCombo, *m_JDCombo, *m_VCCombo;
	GtkWidget		*m_Errors, *m_Header, *m_SkipInvalid, *m_ZeroInvalid;
	GtkWidget		*m_AllValues, *m_HelCor, *m_AirMass, *m_Altitude;
	GtkListStore	*m_FileTypes, *m_JDTypes, *m_Channels;
	CmpackTableType	m_Type;
	CTable			m_Table;
	bool			m_Updating;
	tFileType		m_FileType;
	tDateType		m_JDType;
	int				m_SelectedY;
	tOptions		m_Options[TYPE_N_ITEMS];
	bool			m_HaveHelCor, m_HaveAirMass, m_HaveAltitude;
	
	void UpdateControls(void);

	bool OnResponseDialog(gint response_id);
	void OnSelectionChanged(GtkComboBox *pWidget);
	void OnTypeChanged(void);
	void OnButtonToggled(GtkToggleButton *widget);
	
	static tFileType StrToFileType(const gchar *str);
	static const gchar *FileTypeToStr(tFileType type);

	static void response_dialog(GtkWidget *widget, gint response_id, CExportTableFileDlg *pMe);
	static void selection_changed(GtkComboBox *pWidget, CExportTableFileDlg *pMe);
	static void button_toggled(GtkToggleButton *widget, CExportTableFileDlg *user_data);
};

#endif
