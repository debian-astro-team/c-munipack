/**************************************************************

merge_dlg.h (C-Munipack project)
The 'Merge frames' dialog
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_MERGE_DLG_H
#define CMPACK_MERGE_DLG_H

#include <gtk/gtk.h>

class CMergeDlg
{
public:
	CMergeDlg(GtkWindow *pParent);
	~CMergeDlg();

	void Execute();

private:
	GtkWindow	*m_pParent;
	GtkWidget	*m_pDlg, *m_AllBtn, *m_SelBtn, *m_SingleBtn, *m_MultiBtn;
	GtkWidget	*m_OptionsBtn, *m_NoAlignmentBtn;
	GtkWidget	*m_MaxFramesEdit, *m_MinFramesEdit, *m_MaxTimeEdit;
	GtkWidget	*m_MaxFramesLabel, *m_MinFramesLabel, *m_MaxTimeLabel;
	GtkWidget	*m_MaxFramesUnit, *m_MinFramesUnit, *m_MaxTimeUnit;
	GtkWidget	*m_OutPath, *m_PathBtn, *m_OutName, *m_OutStart, *m_OutStep, *m_OutDigits;
	bool		m_Single;
	int			m_MaxFrames, m_MinFrames, m_MaxTime, m_Start, m_Step, m_Digits;
	int			m_AllFiles, m_InFiles, m_OutFiles;
	GList		*m_FileList;
	gchar		*m_FileName;

	void ChangePath(void);
	void UpdateControls(void);
	void EditPreferences(void);
	bool OnResponseDialog(gint response_id);
	void OnButtonClicked(GtkWidget *pBtn);
	bool OnCloseQuery(void);
	int ProcessFiles(class CProgressDlg *sender);
	int ProcessFiles_Align(class CProgressDlg *sender);
	int ProcessFiles_NoAlign(class CProgressDlg *sender);

	static void response_dialog(GtkDialog *pDlg, gint response_id, CMergeDlg *pMe);
	static void button_clicked(GtkWidget *pButton, CMergeDlg *pDlg);
	static int ExecuteProc(class CProgressDlg *sender, void *user_data);
	static int CompareFiles(GtkTreeRowReference *ref1, GtkTreeRowReference *ref2);
};

#endif
