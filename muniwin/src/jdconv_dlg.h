/**************************************************************

jdconv_dlg.h (C-Munipack project)
JD converter dialog
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_JDCONV_DLG_H
#define CMPACK_JDCONV_DLG_H

#include <gtk/gtk.h>

class CJDConvDlg
{
public:
	CJDConvDlg(GtkWindow *pParent);
	~CJDConvDlg();

	void Execute();
private:
	enum tMode {
		MODE_JD_TO_DATETIME,
		MODE_DATETIME_TO_JD
	};

	bool			m_Updating;
	double			m_JD;
	tMode			m_ActMode;
	GtkWidget		*m_pDlg, *m_Mode;
	GtkWidget		*m_Label1, *m_Edit1, *m_Unit1;
	GtkWidget		*m_Label2, *m_Edit2, *m_Unit2;

	void SetMode(tMode mode);
	void GetData(void);
	void SetData(void);

	bool OnResponseDialog(gint response_id);
	void OnSelectChanged(GtkComboBox *pComboBox);
	void OnEntryChanged(GtkEntry *pEntry);

	static void response_dialog(GtkDialog *pDlg, gint response_id, CJDConvDlg *pMe);
	static void select_changed(GtkComboBox *widget, CJDConvDlg *user_data);
	static void entry_changed(GtkEntry *widget, CJDConvDlg *pMe);
};

#endif
