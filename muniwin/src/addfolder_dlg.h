/**************************************************************

addfolder_dlg.h (C-Munipack project)
The 'Add folder' dialog
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_ADDFOLDER_DLG_H
#define CMPACK_ADDFOLDER_DLG_H

#include <gtk/gtk.h>

#include "preview.h"

class CAddFolderDlg
{
public:
	CAddFolderDlg(GtkWindow *pParent);
	~CAddFolderDlg();

	void Execute();
private:
	GtkWidget	*m_pDlg, *m_SubDirs;

	bool OnResponseDialog(gint response_id);

	static void response_dialog(GtkDialog *pDlg, gint response_id, CAddFolderDlg *pMe);
};

#endif
