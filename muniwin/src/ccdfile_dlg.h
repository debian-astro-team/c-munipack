/**************************************************************

file_dlg.h (C-Munipack project)
The preview dialog
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_CCD_FILE_DLG_H
#define CMPACK_CCD_FILE_DLG_H

#include "file_dlg.h"
#include "info_dlg.h"
#include "progress_dlg.h"
#include "menubar.h"
#include "popup.h"
#include "infobox.h"
#include "qphot.h"

//
// Non-modal file viewer
//
class CCCDFileDlg:public CFileDlg
{
public:
	// Constructor
	CCCDFileDlg(void);

	// Destructor
	virtual ~CCCDFileDlg();

	// Environment changed, reload settings
	virtual void EnvironmentChanged(void);

protected:
	// Get name of icon
	virtual const char *GetIconName(void) { return "chart"; }

	// Load a file
	virtual bool LoadFile(GtkWindow *pParent, const char *fpath, GError **error);

	// Save a file
	virtual bool SaveFile(const char *fpath, GError **error);

	// Update dialog controls
	virtual void UpdateControls(void);

private:
	enum tInfoMode {
		INFO_NONE,
		INFO_PHOTOMETRY,
		INFO_GRAYSCALE,
		INFO_PROFILE,
		INFO_HISTOGRAM
	};

	CCCDFile		*m_File;
	CImage			*m_Image;
	CWcs			*m_Wcs;
	CApertures		m_Aper;
	GtkWidget		*m_Chart;
	GtkToolItem		*m_ZoomFit, *m_ZoomIn, *m_ZoomOut; 
	CMenuBar		m_Menu;
	CmpackImageData	*m_ImageData;
	CQuickPhotBox	m_QPhot;
	CScaleBox		m_Scale;
	CProfileBox		m_Profile;
	CHistogramBox	m_Histogram;
	bool			m_Negative, m_RowsUpward, m_Pseudocolors, m_Rulers, m_PositiveWest;
	bool			m_Updating, m_UpdatePos, m_UpdateZoom;
	bool			m_MouseOnProfile, m_MouseOnHistogram;
	gdouble			m_LastPosX, m_LastPosY;
	tInfoMode		m_InfoMode;
	guint			m_TimerId;
	CPopupMenu		m_ContextMenu;

	void UpdateImage(void);
	void ExportImage(void);
	void SetInfoMode(tInfoMode mode);
	void ShowProperties(void);
	void UpdateStatus(void);
	void UpdateZoom(void);
	void HorizontalFlip(void);
	void VerticalFlip(void);
	void CopyWcsCoordinates(double lng, double lat);

	void OnCommand(int cmd_id);
	void OnButtonClicked(GtkWidget *pBtn);
	void OnInfoBoxClosed(CInfoBox *pBox);
	void OnLButtonClick(void);
	void OnProfileChanged(void);
	void OnContextMenu(GdkEventButton *event);

	static void button_clicked(GtkWidget *pButton, CCCDFileDlg *pDlg);
	static void mouse_moved(GtkWidget *graph, CCCDFileDlg *pMe);
	static void mouse_left(GtkWidget *graph, CCCDFileDlg *pMe);
	static void zoom_changed(GtkWidget *pChart, CCCDFileDlg *pMe);
	static void profile_changed(GtkWidget *pChart, CCCDFileDlg *pMe);
	static gboolean button_press_event(GtkWidget *widget, GdkEventButton *event, CCCDFileDlg *pMe);
	static gboolean timer_cb(CCCDFileDlg *pMe);

	static void MenuCallback(CCBObject *sender, int message, int wparam, void* lparam, void* cb_data);
	static void InfoBoxCallback(CCBObject *sender, int message, int wparam, void* lparam, void* cb_data);
};

//
// File properties dialog
//
class CCCDFileInfoDlg:public CInfoDlg
{
public:
	// Constructor
	CCCDFileInfoDlg(GtkWindow *pParent);

	// Display dialog
	void ShowModal(CCCDFile *image, CWcs *wcs, const gchar *name, const gchar *path);

private:
	CCCDFile	*m_Image;
	CWcs		*m_Wcs;
	GtkWidget	*m_HdrBtn, *m_WcsBtn;
	const gchar	*m_Name;

	void OnButtonClicked(GtkWidget *pBtn);
	void ShowHeader(void);
	void ShowWcsData(void);

	static void button_clicked(GtkWidget *pButton, CCCDFileInfoDlg *pDlg);
};

#endif
