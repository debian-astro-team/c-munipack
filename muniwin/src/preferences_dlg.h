/**************************************************************

preferences_dlg.h (C-Munipack project)
Environment options - dialog
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_ENVIRONMENT_DLG_H
#define CMPACK_ENVIRONMENT_DLG_H

#include <gtk/gtk.h>

class CPreferencesDlg
{
public:
	CPreferencesDlg(GtkWindow *pParent);
	~CPreferencesDlg();

	void Execute();
private:
	GtkWidget		*m_pDlg, *m_PageFrame, *m_TitleFrame, *m_PageTitle;
	GtkWidget		*m_CurrentPage, *m_PageView, *m_PageScroller;
	GtkWidget		*m_NegativeCharts, *m_NoPreviews, *m_SoundFinished, *m_RowsUpward, *m_LongitudeDir;
	GtkWidget		*m_DebugOutputs, *m_NoMaximize;
	GSList			*m_Columns, *m_Catalogs, *m_Dirs;
	GtkListStore	*m_PageList;
	bool			m_Updating;
	int				m_PageId, m_DlgWidth, m_DlgHeight;
	

	void LoadConfig(void);
	void SaveConfig(void);
	void UpdateControls(void);
	GtkWidget *CreatePage(int id, const gchar *caption, const gchar *icon, bool def_btn);
	void ShowPage(int id);
	
	bool OnResponseDialog(gint response_id);
	bool OnCloseQuery(void);
	void OnSelectionChanged(void);
	void OnSetDefaultsClicked(GtkButton *pButton);
	void OnFileButtonClicked(GtkButton *pButton);
	void OnCheckBoxToggled(GtkToggleButton *pButton);

	static gboolean SetMaxDialogSize(GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer data);
	static gint selection_changed(GtkTreeSelection *widget, CPreferencesDlg *pMe);
	static void setdefaults_clicked(GtkButton *widget, CPreferencesDlg *pMe);
	static void filebutton_clicked(GtkButton *widget, CPreferencesDlg *pMe);
	static void response_dialog(GtkWidget *widget, gint response_id, CPreferencesDlg *pMe);
	static void check_box_toggled(GtkToggleButton *togglebutton, CPreferencesDlg *pMe);
};

#endif
