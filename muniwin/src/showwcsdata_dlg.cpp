/**************************************************************

preview_dlg.cpp (C-Munipack project)
The preview dialog
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#include <stdlib.h>
#include <string.h>

#include "showwcsdata_dlg.h"
#include "utils.h"
#include "main.h"
#include "ctxhelp.h"

//-------------------------   CHOOSE STARS DIALOG   --------------------------------

CShowWcsDataDlg::CShowWcsDataDlg(GtkWindow *pParent):m_Buffer(NULL), m_Name(NULL), 
	m_Wcs(NULL)
{
	// Dialog with buttons
	m_pDlg = gtk_dialog_new_with_buttons("", pParent, 
		(GtkDialogFlags)(GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_NO_SEPARATOR),
		GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE, NULL);
	gtk_dialog_widget_standard_tooltips(GTK_DIALOG(m_pDlg));

	GtkWidget *vbox = gtk_vbox_new(false, 8);
	gtk_container_set_border_width(GTK_CONTAINER(vbox), 8);
	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(m_pDlg)->vbox), vbox);

	// Text view
	m_View = gtk_text_view_new();
	gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(m_View), GTK_WRAP_WORD_CHAR);
	gtk_text_view_set_editable(GTK_TEXT_VIEW(m_View), false);
	gtk_text_view_set_cursor_visible(GTK_TEXT_VIEW(m_View), false);

	// Scrolled window
	m_ScrWnd = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(m_ScrWnd),
		GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(m_ScrWnd), 
		GTK_SHADOW_ETCHED_IN);
	gtk_container_add(GTK_CONTAINER(m_ScrWnd), m_View);
	gtk_widget_set_size_request(m_ScrWnd, 480, 320);
	gtk_box_pack_start(GTK_BOX(vbox), m_ScrWnd, TRUE, TRUE, 0);

	gtk_widget_show_all(GTK_DIALOG(m_pDlg)->vbox);
}

CShowWcsDataDlg::~CShowWcsDataDlg()
{
	if (m_Buffer) 
		g_object_unref(m_Buffer);
	gtk_widget_destroy(m_pDlg);
	g_free(m_Name);
}

void CShowWcsDataDlg::Execute(const CWcs &wcs, const gchar *name)
{
	if (!wcs.Valid())
		return;

	m_Wcs = wcs;
	SetCaption(name);
	UpdateText();

	gtk_dialog_run(GTK_DIALOG(m_pDlg));
	gtk_widget_hide(m_pDlg);
}

//
// Set window caption
//
void CShowWcsDataDlg::SetCaption(const char *name)
{
	char buf[512];
	sprintf(buf, "%s - WCS data - %s", name, g_AppTitle);
	gtk_window_set_title(GTK_WINDOW(m_pDlg), buf);
	g_free(m_Name);
	m_Name = g_strdup(name);
}

void CShowWcsDataDlg::UpdateText()
{
	char *buf;
	int len;

	gtk_text_view_set_buffer(GTK_TEXT_VIEW(m_View), NULL);
	if (m_Buffer) {
		g_object_unref(m_Buffer);
		m_Buffer = NULL;
	}
	m_Buffer = gtk_text_buffer_new(NULL);
	m_Wcs.Print(&buf, &len);
	gtk_text_buffer_set_text(GTK_TEXT_BUFFER(m_Buffer), buf, len);
	g_free(buf);
	gtk_text_view_set_buffer(GTK_TEXT_VIEW(m_View), GTK_TEXT_BUFFER(m_Buffer));
}
