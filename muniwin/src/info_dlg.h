/**************************************************************

preview_dlg.h (C-Munipack project)
The preview dialog
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_INFO_DLG_H
#define CMPACK_INFO_DLG_H

#include <gtk/gtk.h>

class CInfoDlg
{
public:
	// Constructor
	CInfoDlg(GtkWindow *pParent, int help_id);

	// Destructor
	virtual ~CInfoDlg();

protected:
	int				m_HelpID;
	GtkWidget		*m_pDlg, *m_Box, *m_Tab;
	GList			*m_Labels;

	// Display dialog
	void ShowModal(void);
	bool OnResponseDialog(gint response_id);

	void AddField(int id, int col, int row, const char *caption, int ellipsize = 0);
	void AddHeading(int id, int col, int row, const char *caption);
	void AddSeparator(int col, int row);
	void SetHeading(int id, const char *caption, const char *color = NULL);
	void SetField(int id, int value, const char *unit = NULL);
	void SetField(int id, double value, int prec = 2, const char *unit = NULL);
	void SetField(int id, const char *value);

private:
	static void response_dialog(GtkDialog *pDlg, gint response_id, CInfoDlg *pMe);
};

#endif
