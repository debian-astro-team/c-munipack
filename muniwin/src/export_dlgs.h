/**************************************************************

chartexportdlg.h (C-Munipack project)
Export chart dialog
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_EXPORT_DLGS_H
#define CMPACK_EXPORT_DLGS_H

#include <gtk/gtk.h>

class CChartExportDlg
{
public:
	// Constructor
	CChartExportDlg(GtkWindow *pParent);

	// Destructor
	virtual ~CChartExportDlg();

	// Execute the dialog
	bool Execute(CmpackChartData *chart, CmpackImageData *image, const gchar *fileName,
		bool invertImage, bool rowsUpwards);

private:
	GtkWindow			*m_Parent;
	GtkWidget			*m_pDlg;
	class CExportChartOptions *m_Widget;

	bool OnResponseDialog(gint response_id);
	void OnTypeChanged(void);
	bool CheckFormat(void);

	static void response_dialog(GtkWidget *widget, gint response_id, CChartExportDlg *pMe);
	static void WidgetCallback(CCBObject *sender, int message, int wparam, void* lparam, void* cb_data);

private:
	// Disable copy constructor and assignment operator
	CChartExportDlg(const CChartExportDlg&);
	CChartExportDlg &operator=(const CChartExportDlg&);
};

class CGraphExportDlg
{
public:
	// Constructor
	CGraphExportDlg(GtkWindow *pParent);

	// Destructor
	virtual ~CGraphExportDlg();

	// Execute the dialog
	bool Execute(CmpackGraphView *widget, const gchar *graphName);

private:
	GtkWidget	*m_pDlg;
	GtkWindow	*m_Parent;
	
	bool OnResponseDialog(gint response_id);

	static void response_dialog(GtkWidget *widget, gint response_id, CGraphExportDlg *pMe);
};

class CFrameExportDlg
{
public:
	// Constructor
	CFrameExportDlg(GtkWindow *pParent);

	// Destructor
	virtual ~CFrameExportDlg();

	// Execute the dialog
	bool Execute(const CPhot &file, bool matched, const gchar *name, 
		int sort_column_id, GtkSortType sort_order);

private:
	enum tFileType {
		TYPE_CSV,
		TYPE_N_ITEMS			// Number of file formats
	};

	struct tOptions {
		bool skip_unmatched, skip_invalid, header;
	};

	GtkWindow		*m_Parent;
	GtkWidget		*m_pDlg, *m_TypeCombo;
	GtkWidget		*m_Header, *m_SkipInvalid, *m_SkipUnmatched;
	GtkListStore	*m_FileTypes;
	CPhot			m_File;
	tFileType		m_FileType;
	tOptions		m_Options[TYPE_N_ITEMS];
	bool			m_Matched, m_Updating;

	void UpdateControls(void);

	void OnSelectionChanged(GtkComboBox *pWidget);
	void OnTypeChanged(void);
	void OnButtonToggled(GtkToggleButton *widget);
	
	static void selection_changed(GtkComboBox *pWidget, CFrameExportDlg *pMe);
	static void button_toggled(GtkToggleButton *widget, CFrameExportDlg *pMe);
};

class CObjectsExportDlg
{
public:
	enum tSortColumn { SORT_NAME, SORT_ID, SORT_POS_X, SORT_POS_Y, SORT_WCS_LNG, SORT_WCS_LAT, SORT_MAG, SORT_TAG };
	
	// Constructor
	CObjectsExportDlg(GtkWindow *pParent);

	// Destructor
	virtual ~CObjectsExportDlg();

protected:
	enum tFileType {
		TYPE_CSV,
		TYPE_N_ITEMS			// Number of file formats
	};

	struct tOptions {
		bool header;
	};

	GtkWindow		*m_Parent;
	GtkWidget		*m_pDlg, *m_TypeCombo;
	GtkWidget		*m_Header;
	GtkListStore	*m_FileTypes;
	CSelection		m_Selection;
	CTags			m_Tags;
	tFileType		m_FileType;
	tOptions		m_Options[TYPE_N_ITEMS];
	bool			m_Updating;

	// Execute the dialog
	bool Execute(const CSelection &sel, const CTags &tags, const gchar *name, 
		tSortColumn sort_column_id, GtkSortType sort_type);

	void UpdateControls(void);

	void OnSelectionChanged(GtkComboBox *pWidget);
	void OnTypeChanged(void);
	void OnButtonToggled(GtkToggleButton *widget);

	bool ExportTable(const gchar *filepath, const gchar *format, bool noheader,
		tSortColumn sort_column_id, GtkSortType sort_order, GError **error);
	virtual void ExportTable(CCSVWriter &file, tSortColumn sort_column_id, GtkSortType sort_order) = 0;
		
	static void selection_changed(GtkComboBox *pWidget, CObjectsExportDlg *pMe);
	static void button_toggled(GtkToggleButton *widget, CObjectsExportDlg *pMe);
};

class CPhotObjectsExportDlg:public CObjectsExportDlg
{
public:
	// Constructor
	CPhotObjectsExportDlg(GtkWindow *pParent):CObjectsExportDlg(pParent) {}

	// Execute the dialog
	bool Execute(const CPhot &file, const CSelection &sel, const CTags &tags, int apertureIndex, 
		const gchar *name, tSortColumn sort_column_id, GtkSortType sort_type);

private:
	CPhot			m_File;

	virtual void ExportTable(CCSVWriter &file, tSortColumn sort_column_id, GtkSortType sort_order);
};

class CCatalogObjectsExportDlg:public CObjectsExportDlg
{
public:
	// Constructor
	CCatalogObjectsExportDlg(GtkWindow *pParent):CObjectsExportDlg(pParent) {}

	// Execute the dialog
	bool Execute(const CCatalog &file, const CSelection &sel, const CTags &tags, 
		const gchar *name, tSortColumn sort_column_id, GtkSortType sort_type);

private:
	CCatalog		m_File;

	virtual void ExportTable(CCSVWriter &file, tSortColumn sort_column_id, GtkSortType sort_order);
};

#endif
