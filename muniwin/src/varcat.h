/**************************************************************

varcat.h (C-Munipack project)
Catalogs of variable stars
Copyright (C) 2010 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef MUNIWIN_VARCAT_H
#define MUNIWIN_VARCAT_H

#include <gtk/gtk.h>

class VarCat
{
public:
	typedef void tVarCatProc(const char *objname, double ra, double dec, const char *catalog, const char *comment, void *data);
	typedef bool tCancelledCb(void *data);
	typedef void tSetFileNameCb(const gchar *name, void *data);
	typedef void tSetSizeCb(long size, void *data);
	typedef void tSetPositionCb(long pos, void *data);

	enum tCatalogFilter
	{
		GCVS = (1<<0),
		NSV = (1<<1),
		NSVS = (1<<2),
		VSX = (1<<3),

		AllCatalogs = (GCVS | NSV | NSVS | VSX)
	};

	struct tCallbacks
	{
		tVarCatProc		*cb_proc;
		void			*cb_proc_data;

		tCancelledCb	*cb_cancelled;
		tSetFileNameCb	*cb_setFileName;
		tSetSizeCb		*cb_setSize;
		tSetPositionCb	*cb_setPosition;
		void			*cb_struct_data;
	};

	struct tPosFilter
	{
		double ra, dec;		// Center of area in degrees
		double radius;		// Maximum distance to center in degrees
	};

	static void Search(int catalogFilter, const char *searchstr, const tPosFilter *posfilter, tCallbacks *cb_struct);

	static void SearchEx(GtkWindow *parent, int catalogFilter, const char *searchstr, const tPosFilter *posfilter, tVarCatProc *cb_proc, void *cb_proc_data);

	static bool Test(void);

private:
	static void Search_GCVS(const char *searchstr, const tPosFilter *posfilter, tCallbacks *cb_struct);
	static void Search_NSV(const char *searchstr, const tPosFilter *posfilter, tCallbacks *cb_struct);
	static void Search_NSVS(const char *searchstr, const tPosFilter *posfilter, tCallbacks *cb_struct);
	static void Search_VSX(const char *searchstr, const tPosFilter *posfilter, tCallbacks *cb_struct);
};

#endif
