/**************************************************************

object_dlg.h (C-Munipack project)
The 'Enter object coordinates' dialog
Copyright (C) 2009 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_OBJECT_DLG_H
#define CMPACK_OBJECT_DLG_H

#include <gtk/gtk.h>

#include "popup.h"
#include "objects.h"

class CObjectDlg
{
public:
	CObjectDlg(GtkWindow *pParent);
	virtual ~CObjectDlg();

	bool Execute(CObjectCoords *pObj);

private:
	// Selection modes
	enum tSelectMode {
		MANUAL_ENTRY,
		CATALOG_FILE,
		REFERENCE_FRAME
	};

	CObjectCoords	*m_pCoords;
	bool			m_Updating, m_UserFileChanged, m_UserEntryChanged;
	GtkWidget		*m_pDlg, *m_RefBtn, *m_ManBtn, *m_CatBtn;
	GtkWidget		*m_AutoName, *m_AutoRA, *m_AutoDec, *m_AutoBox;
	GtkWidget		*m_EntryName, *m_EntryRA, *m_EntryDec, *m_EntryRem, *m_EntrySrc, *m_EntryBox;
	GtkWidget		*m_SearchBox, *m_FindText, *m_FindBtn, *m_SearchView;
	GtkWidget		*m_UserView, *m_AddBtn, *m_SaveBtn, *m_DelBtn;
	tSelectMode		m_SelectMode;
	GtkListStore	*m_SearchList;
	GtkTreePath		*m_SelPath;
	CObjects		m_UserList;
	CPopupMenu		m_SearchMenu, m_UserMenu;

	void SetUserData(const CObjectCoords *data);
	void SetRefData(const CObjectCoords *data);
	bool GetData(CObjectCoords *data, bool name_required);
	bool OnResponseDialog(gint response_id);
	void SetSelectMode(tSelectMode mode);
	void UpdateControls(void);
	void ImportFromFile(void);
	void ExportToFile(void);
	void FindObjects(void);
	void AddToTable(void);
	void SaveToTable(void);
	void RemoveFromTable(void);
	void SelectAll(void);

	void OnButtonClicked(GtkWidget *pBtn);
	void OnEntryChanged(GtkWidget *pEntry);
	void OnContextMenu(GtkWidget *widget, GdkEventButton *event);
	void OnRowActivated(GtkTreeView *tree_view, GtkTreePath *path);
	void OnSelectionChanged(GtkTreeSelection *widget);

	static void button_clicked(GtkWidget *pButton, CObjectDlg *pMe);
	static void entry_changed(GtkWidget *pEntry, CObjectDlg *pMe);
	static void row_activated(GtkTreeView *treeview, GtkTreePath *path, GtkTreeViewColumn *column, CObjectDlg *pMe);
	static void selection_changed(GtkTreeSelection *widget, CObjectDlg *pMe);
	static gint button_press_event(GtkWidget *widget, GdkEventButton *event, CObjectDlg *pMe);
	static void AddToList(const char *objname, double ra, double dec, const char *catalog, const char *comment, void *data);
	static void response_dialog(GtkDialog *pDlg, gint response_id, CObjectDlg *pMe);
};

#endif
