/**************************************************************

lightcurve_dlg.h (C-Munipack project)
The 'Plot light curve' dialog
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_LIGHTCURVE_DLG_H
#define CMPACK_LIGHTCURVE_DLG_H

#include <gtk/gtk.h>

#include "cmpack_widgets.h"
#include "output_dlg.h"
#include "frameset_class.h"
#include "ccdfile_class.h"
#include "table_class.h"
#include "infobox.h"
#include "menubar.h"
#include "output_dlg.h"
#include "progress_dlg.h"
#include "popup.h"
#include "measurement.h"
#include "project.h"
#include "graph_toolbox.h"

//
// Make catalog file 
//
class CMakeLightCurveDlg
{
public:
	// Constructor
	CMakeLightCurveDlg(GtkWindow *pParent);

	// Destructor
	~CMakeLightCurveDlg();

	// Execute the dialog
	void Execute(void);

private:
	GtkWindow		*m_pParent;
	GtkWidget		*m_pDlg, *m_AllBtn, *m_SelBtn;
	GtkWidget		*m_OptBox, *m_HelCor, *m_AirMass, *m_InstMag, *m_AllStars, *m_Ensemble;
	GtkWidget		*m_ObjName, *m_ObjBtn, *m_RA, *m_Dec;
	GtkWidget		*m_ObjLabel, *m_RALabel, *m_RAUnit, *m_DecLabel, *m_DecUnit;
	GtkWidget		*m_LocName, *m_LocBtn, *m_Lon, *m_Lat;
	GtkWidget		*m_LocLabel, *m_LonLabel, *m_LonUnit, *m_LatLabel, *m_LatUnit;
	bool			m_ComputeHC, m_ComputeAM, m_ShowInstMag, m_MakeAll, m_MultiComp;
	CObjectCoords	m_ObjCoords;
	CLocation		m_Location;

	void UpdateControls(void);
	void UpdateObjectCoords(void);
	void UpdateLocation(void);
	void EditObjectCoords(void);
	void EditLocation(void);

	bool OnResponseDialog(gint response_id);
	void OnButtonClicked(GtkWidget *pBtn);
	void OnButtonToggled(GtkWidget *pBtn);
	bool OnCloseQuery(void);

	static void response_dialog(GtkDialog *widget, gint response_id, CMakeLightCurveDlg *pDlg);
	static void button_clicked(GtkWidget *button, CMakeLightCurveDlg *pDlg);
	static void button_toggled(GtkWidget *widget, CMakeLightCurveDlg *pDlg);
};

class CLightCurveDlg:public COutputCurveDlg
{
public:
	struct tParamsRec {
		bool helcor;
		bool amass;
		bool instmag;
		bool allstars;
		bool ensemble;
	};

	// Constructor
	CLightCurveDlg();

	// Destructor
	virtual ~CLightCurveDlg();

	// Make a light curve dialog
	bool Make(GtkWindow *parent, bool selected_files, const tParamsRec &rec, 
		const CObjectCoords &obj, const CLocation &loc);

	const tParamsRec *Params(void) const
	{ return &m_Params; }
	
protected:
	// Get icon name
	virtual const char *GetIconName(void) { return "lightcurve"; }

	// Frame set changed, update curve
	virtual void OnFrameSetChanged(void);

	// Get list of GtkTreePaths of all selected frames
	virtual GList *GetSelectedFrames(void);

	// Get path to the first selected frame
	virtual GtkTreePath *GetSelectedFrame(void);

private:
	enum tDisplayMode {
		DISPLAY_GRAPH,
		DISPLAY_TABLE
	};

	enum tInfoMode {
		INFO_NONE,
		INFO_STATISTICS,
		INFO_MEASUREMENT
	};

	GtkWidget		*m_XLabel, *m_DCombo, *m_XCombo, *m_YLabel, *m_YCombo, *m_ALabel, *m_ACombo;
	GtkWidget		*m_GraphScrWnd, *m_GraphView, *m_TableScrWnd, *m_TableView;
	GtkWidget		*m_ZoomLabel, *m_SLabel, *m_SName;
	GtkToolItem		*m_ChangeStars, *m_ChangeAperture, *m_ZoomFit, *m_ZoomIn, *m_ZoomOut; 
	bool			m_UpdatePos, m_LastPosValid, m_ShowErrors, m_ShowGrid;
	tDisplayMode	m_DispMode;
	tDateFormat		m_DateFormat;
	int				m_ApertureIndex, m_ChannelX, m_ChannelY;
	double			m_LastPosX, m_LastPosY;
	tIndex			m_LastFocus;
	tParamsRec		m_Params;
	CMenuBar		m_Menu;
	CObjectCoords	m_ObjCoords;
	CLocation		m_Location;
	CTable			*m_Table;
	GtkListStore	*m_DateFormats, *m_XChannels, *m_YChannels, *m_Apertures;
	gint			m_TimerId;
	CPopupMenu		m_GraphMenu;
	CTextBox		m_InfoBox;
	tInfoMode		m_InfoMode;
	CMeasurementBox	m_MeasBox;
	CmpackGraphData	*m_GraphData;
	GtkTreeModel	*m_TableData;
	CApertures		m_AperList;
	CSelection		m_Selection;
	CCCDFile		m_Frame;
	CImage			m_Image;
	bool			m_ShowToolBox;
	CGraphToolBox	m_ToolBox;
	
	void UpdateSelection(CSelection &sel);
	void EditSelection(void);
	void ChangeAperture(void);
	bool RebuildData(GtkWindow *parent);
	bool UpdateLightCurve(GtkWindow *parent);
	void UpdateChannels(void);
	void UpdateApertures(void);
	void UpdateGraphTable(gboolean autozoom_x, gboolean autozoom_y);
	void UpdateStatus(void);
	void UpdateControls(void);
	void UpdateTools(void);
	void SaveData(void);
	void EditProperties(void);
	void ShowChart(void);
	void ShowObjects(void);
	void SetDisplayMode(tDisplayMode mode);
	void SetInfoMode(tInfoMode mode);
	void ShowToolBoxMode(bool show);
	void Export(void);
	void PrintValue(char *buf, double val, const CChannel *channel);
	void PrintKeyValue(char *buf, double val, const CChannel *channel);
	void PrintKeyValueU(char *buf, double val, double sdev, const CChannel *channel);

	void OnCommand(int cmd_id);
	void OnMouseMoved(void);
	void OnButtonClicked(GtkWidget *pBtn);
	void OnContextMenu(GtkWidget *widget, GdkEventButton *event);
	void OnEntryChanged(GtkWidget *pButton);
	void OnUpdateCurve(void);
	void OnSelectionChanged(void);
	void OnInfoBoxClosed(void);
	void OnToolBoxClosed(void);

	static void button_clicked(GtkWidget *pButton, CLightCurveDlg *pDlg);
	static void mouse_moved(GtkWidget *pGraph, CLightCurveDlg *pMe);
	static void mouse_left(GtkWidget *pGraph, CLightCurveDlg *pMe);
	static gint button_press_event(GtkWidget *widget, GdkEventButton *event, CLightCurveDlg *pMe);
	static void entry_changed(GtkWidget *pButton, CLightCurveDlg *pMe);
	static gboolean update_status_timer(CLightCurveDlg *pMe);
	static void selection_changed(GtkWidget *pChart, CLightCurveDlg *pMe);
	static void MenuCallback(CCBObject *sender, int message, int wparam, void* lparam, void* cb_data);
	static void InfoBoxCallback(CCBObject *sender, int message, int wparam, void* lparam, void* cb_data);
	static void ToolBoxCallback(CCBObject *sender, int message, int wparam, void* lparam, void* cb_data);
};


//
// Light curve options
//
class CLightCurveOptionsDlg
{
public:
	// Constructor
	CLightCurveOptionsDlg(GtkWindow *pParent);

	// Destructor
	virtual ~CLightCurveOptionsDlg();

	// Execute the dialog
	bool Execute(CLightCurveDlg::tParamsRec &params, CObjectCoords &obj,
		CLocation &loc);

private:
	GtkWidget		*m_pDlg, *m_OptBox, *m_HelCor, *m_AirMass;
	GtkWidget		*m_ObjBox, *m_ObjName, *m_ObjBtn, *m_RA, *m_Dec;
	GtkWidget		*m_ObjLabel, *m_RALabel, *m_RAUnit, *m_DecLabel, *m_DecUnit;
	GtkWidget		*m_LocBox, *m_LocName, *m_LocBtn, *m_Lon, *m_Lat;
	GtkWidget		*m_LocLabel, *m_LonLabel, *m_LonUnit, *m_LatLabel, *m_LatUnit;
	bool			m_ComputeHC, m_ComputeAM;
	CObjectCoords	m_ObjCoords;
	CLocation		m_Location;

	void EditObjectCoords(void);
	void EditLocation(void);
	void UpdateObjectCoords(void);
	void UpdateLocation(void);
	void UpdateControls(void);

	bool OnResponseDialog(gint response_id);
	void OnButtonClicked(GtkWidget *pBtn);
	void OnButtonToggled(GtkWidget *pBtn);
	bool OnCloseQuery(void);

	static void response_dialog(GtkDialog *widget, gint response_id, CLightCurveOptionsDlg *pDlg);
	static void button_clicked(GtkWidget *button, CLightCurveOptionsDlg *pDlg);
	static void button_toggled(GtkWidget *widget, CLightCurveOptionsDlg *pDlg);
};

//
// Save light curve
//
class CSaveLightCurveDlg
{
public:
	// Constructor
	CSaveLightCurveDlg(GtkWindow *pParent);

	// Destructor
	virtual ~CSaveLightCurveDlg();

	// Execute the dialog
	bool Execute(const CTable &table, int xchannel, int ychannel, const CSelection &stars, bool all_stars,
		const gchar *default_file_name);

private:
	enum tFileType {
		TYPE_MUNIPACK,
		TYPE_AVE,
		TYPE_MCV,
		TYPE_TEXT,
		TYPE_CSV,
		TYPE_AAVSO,
		TYPE_BAAVSS,
		TYPE_ETD,
		TYPE_N_ITEMS,			// Number of file formats
		TYPE_INVALID = -1
	};

	enum tDateType {
		DATE_JD_GEOCENTRIC,
		DATE_JD_HELIOCENTRIC,
		DATE_N_ITEMS,			// Number of JD formats
		DATE_INVALID = -1
	};

	struct tOptions {
		bool frame_id, helcor, airmass, altitude, all_values, vconly;
		bool skip_invalid, zero_invalid, header, errors;
	};
	
	GtkWindow		*m_pParent;
	GtkWidget		*m_pDlg, *m_TypeCombo, *m_JDCombo, *m_VCCombo;
	GtkWidget		*m_Errors, *m_Header, *m_SkipInvalid, *m_ZeroInvalid;
	GtkWidget		*m_FrameIds, *m_AllValues, *m_HelCor, *m_AirMass, *m_Altitude;
	GtkListStore	*m_FileTypes, *m_JDTypes, *m_Channels;
	CTable			m_Table;
	bool			m_Updating;
	tFileType		m_FileType;
	tDateType		m_JDType;
	int				m_SelectedY;
	tOptions		m_Options[TYPE_N_ITEMS];
	bool			m_InstMag, m_HaveHelCor, m_HaveAirMass, m_HaveAltitude;
	
	void UpdateControls(void);

	bool CheckFormat(tFileType fileType, bool all_stars, const CSelection &stars);

	bool OnResponseDialog(gint response_id);
	void OnSelectionChanged(GtkComboBox *pWidget);
	void OnTypeChanged(void);
	void OnButtonToggled(GtkToggleButton *widget);
	
	static tFileType StrToFileType(const gchar *str);
	static const gchar *FileTypeToStr(tFileType type);

	static void response_dialog(GtkWidget *widget, gint response_id, CSaveLightCurveDlg *pMe);
	static void selection_changed(GtkComboBox *pWidget, CSaveLightCurveDlg *pMe);
	static void button_toggled(GtkToggleButton *widget, CSaveLightCurveDlg *user_data);
};

#endif
