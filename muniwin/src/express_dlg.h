/**************************************************************

newfiles_dlg.h (C-Munipack project)
The 'Process new files' dialog
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_EXPRESS_DLG_H
#define CMPACK_EXPRESS_DLG_H

#include <gtk/gtk.h>

class CExpressDlg
{
public:
	CExpressDlg(GtkWindow *pParent);
	~CExpressDlg();

	void Execute();

private:
	struct tProcParams
	{
		GList	*FileList;
		bool	Convert, TimeCorr, BiasCorr, DarkCorr, FlatCorr;
		bool	Photometry, Matching, UseRef;
		double	Seconds;
		const char *BiasFrame, *DarkFrame, *FlatFrame, *CatFile;
		int		RefFrame;
		int		InFiles, OutFiles;
	};

	GtkWindow		*m_pParent;
	GtkWidget		*m_pDlg, *m_AllBtn, *m_SelBtn;
	GtkWidget		*m_ConvertBtn, *m_ChannelCombo;
	GtkWidget		*m_TimeCorrBtn, *m_SecondsEdit, *m_SecondsBtn;
	GtkWidget		*m_BiasCorrBtn, *m_BiasFrameEdit, *m_BiasFrameBtn;
	GtkWidget		*m_DarkCorrBtn, *m_DarkFrameEdit, *m_DarkFrameBtn;
	GtkWidget		*m_FlatCorrBtn, *m_FlatFrameEdit, *m_FlatFrameBtn;
	GtkWidget		*m_PhotometryBtn, *m_MatchingBtn;
	GtkWidget		*m_RefBtn, *m_RefFrameCombo, *m_RefFrameBtn;
	GtkWidget		*m_CatBtn, *m_CatFrameEdit, *m_CatFrameBtn;
	GtkListStore	*m_Frames, *m_Channels;
	double			m_TimeCorr;
	bool			m_Updating, m_Selected, m_Matching;
	tProcParams		m_Params;

	static gboolean foreach_all_files(GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer data);
	static void foreach_sel_files(GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer userdata);
	static void button_toggled(GtkToggleButton *widget, CExpressDlg *user_data);
	static void button_clicked(GtkButton *widget, CExpressDlg *user_data);
	static void response_dialog(GtkDialog *pDlg, gint response_id, CExpressDlg *pMe);

	bool OnResponseDialog(gint response_id);
	void OnButtonToggled(GtkToggleButton *widget);
	void OnButtonClicked(GtkButton *widget);

	static int ExecuteProc(class CProgressDlg *sender, void *user_data);

	void ResetParams(void);
	void UpdateControls(void);
	void EditTimeCorrection(void);
	void ChooseBiasFrame(void);
	void ChooseDarkFrame(void);
	void ChooseFlatFrame(void);
	void ChooseReferenceFrame(void);
	void ChooseCatalogFile(void);
	void UpdateTimeCorrection(void);

	void SelectChannel(CmpackChannel channel);
	CmpackChannel SelectedChannel(void);

	void SelectRefFrame(int frame);
	int SelectedRefFrame(void);

	int ProcessFiles(class CProgressDlg *sender);
};

#endif
