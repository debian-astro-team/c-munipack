/**************************************************************

flatcorr_dlg.h (C-Munipack project)
The 'Flat correction' dialog
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_FLATCORR_DLG_H
#define CMPACK_FLATCORR_DLG_H

#include <gtk/gtk.h>

#include "preview.h"

class CFlatCorrDlg
{
public:
	CFlatCorrDlg(GtkWindow *pParent);
	~CFlatCorrDlg();

	void Execute();
	bool SelectFrame(char **filepath);

private:
	GtkWindow	*m_pParent;
	GtkWidget	*m_pDlg, *m_ProcFrame, *m_AllBtn, *m_SelBtn;
	CPreview	m_Preview;
	int			m_InFiles, m_OutFiles;
	GList		*m_FileList;
	char		*m_FlatFile;

	static void foreach_sel_files(GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, GList **pList);
	static gboolean foreach_all_files(GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, GList **pList);
	static void response_dialog(GtkDialog *pDlg, gint response_id, CFlatCorrDlg *pMe);
	static void update_preview(GtkFileChooser *chooser, CFlatCorrDlg *pMe);
	static void selection_changed(GtkFileChooser *chooser, CFlatCorrDlg *pMe);

	void OnUpdatePreview(GtkFileChooser *chooser);
	void OnSelectionChanged(GtkFileChooser *chooser);
	bool OnResponseDialog(gint response_id);
	void UpdateControls(void);

	static int ExecuteProc(class CProgressDlg *sender, void *user_data);

	int ProcessFiles(class CProgressDlg *sender);
};

#endif
