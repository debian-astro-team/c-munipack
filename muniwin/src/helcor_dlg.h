/**************************************************************

helcor_dlg.h (C-Munipack project)
Heliocentric correction dialog
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_HELCOR_DLG_H
#define CMPACK_HELCOR_DLG_H

#include <gtk/gtk.h>

class CHelCorDlg
{
public:
	CHelCorDlg(GtkWindow *pParent);
	~CHelCorDlg();

	void Execute();
private:
	enum tMode {
		MODE_HCORR,
		MODE_HJD,
		MODE_GJD
	};

	bool			m_Updating;
	tMode			m_ActMode;
	GtkWidget		*m_pDlg, *m_Mode;
	GtkWidget		*m_Label1, *m_Label3, *m_RA, *m_Dec, *m_JD, *m_UTC;
	GtkWidget		*m_Label2, *m_Edit2, *m_ObjBtn;
	double			m_JDValue;
	bool			m_InvalidUTC, m_NotFilled;

	void SetMode(tMode mode);
	void Refresh(bool refresh_all);
	void EditObjectCoords(void);

	bool OnResponseDialog(gint response_id);
	void OnSelectChanged(GtkComboBox *widget);
	void OnEntryChanged(GtkWidget *pEntry);
	void OnButtonClicked(GtkWidget *widget);

	static void response_dialog(GtkDialog *pDlg, gint response_id, CHelCorDlg *pMe);
	static void select_changed(GtkComboBox *widget, CHelCorDlg *pMe);
	static void entry_changed(GtkWidget *pEntry, CHelCorDlg *pMe);
	static void button_clicked(GtkWidget *widget, CHelCorDlg *pMe);
};

#endif
