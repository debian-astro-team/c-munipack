/**************************************************************

measurement.h (C-Munipack project)
Measurement tool
Copyright (C) 2012 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef MUNIWIN_MEASUREMENT_H
#define MUNIWIN_MEASUREMENT_H

#include "image_class.h"
#include "infobox.h"
#include "table_class.h"

class CMeasurementBox:public CInfoBox
{
public:
	// Constructor
	CMeasurementBox(void);

	// Destructor
	virtual ~CMeasurementBox(void);
	
	// Set reference to a chart view
	void SetGraphView(CmpackGraphView *pView);

	// Set reference to a chart view
	void SetTable(CTable *pTable);

	// Set active axis
	void SetAxis(CmpackGraphAxis axis);

	// Set channel
	void SetChannel(CmpackGraphAxis axis, int channel);

	// Set date format
	void SetDateFormat(tDateFormat datef);

protected:
	// Initialization before the tool is shown
	virtual void OnShow(void);

	// Clean up after the tool is hidden
	virtual void OnHide(void);

private:
	struct tPosition
	{
		gchar *name;
		bool user_defined[2];
		double pos[2];
	};

	GtkWidget		*m_Info, *m_XAxisBtn, *m_YAxisBtn;
	GtkWidget		*m_Cursor[2], *m_Distance;
	GtkTextBuffer	*m_Buffer;
	CmpackGraphView	*m_pGraph;
	CTable			*m_pTable;
	bool			m_Updating, m_UpdateStats;
	CmpackGraphAxis	m_Axis;
	int				m_ChannelX, m_ChannelY;
	tDateFormat		m_DateFormat;
	tPosition		**m_PosX, **m_PosY;
	int				m_ChannelsX, m_ChannelsY;
	int				m_PrecisionX, m_PrecisionY;
	guint			m_Timer;
	const gchar		*m_NameX, *m_NameY, *m_UnitX, *m_UnitY;
	GSList			*m_PosList;

	// Find record in PosList
	tPosition *findPos(const gchar *name) const;

	// Add new record to PosList
	tPosition *addPos(const gchar *name, double min, double max);

	// Update axis labels
	void UpdateLabels();

	// Update cursors in the graph view
	void UpdateGraph();

	// Update displayed values
	void UpdateValues();

	// Update statistics
	void UpdateStatistics();

	// Axis changed
	void OnToggled(GtkToggleButton *pButton);

	// Cursor moved, update displayed values
	void OnCursorMoved(CmpackGraphView *pView, CmpackGraphAxis axis, int cursor);

	// Update statistics on timer
	void OnTimer(void);

	// Print distance
	void PrintValue(gchar *buf, double value, int prec, const gchar *name, const gchar *unit,
		bool distance = false);

	static void toggled(GtkToggleButton *pBtn, CMeasurementBox *pMe);
	static void cursor_moved(CmpackGraphView *pView, CmpackGraphAxis axis, int cursor, CMeasurementBox *pMe);
	static gboolean timer_cb(CMeasurementBox *pMe);
};

#endif
