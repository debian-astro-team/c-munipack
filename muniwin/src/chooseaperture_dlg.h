/**************************************************************

aperture_dlg.h (C-Munipack project)
The 'Choose aperture' dialog
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_CHOOSE_APERTURE_DLG_H
#define CMPACK_CHOOSE_APERTURE_DLG_H

#include <gtk/gtk.h>

#include "cmpack_widgets.h"

class CChooseApertureDlg
{
public:
	// Constructor
	CChooseApertureDlg(GtkWindow *pParent);

	// Destructor
	virtual ~CChooseApertureDlg();

	// Execute the dialog
	int Execute(GtkWindow *pParent, CFrameSet &fset, const CApertures &aper, const CSelection &sel, int default_index);
	
private:
	GtkWidget		*m_pDlg, *m_AperView, *m_DataView, *m_GraphView;
	GtkListStore	*m_Apertures, *m_Channels;
	CApertures		m_Aper;
	CFrameSet		*m_FrameSet;
	CSelection		m_Selection;
	CTable			*m_Table;
	CmpackGraphData	*m_GraphData;
	int				m_ApertureIndex, m_Row, m_Column;

	bool UpdateApertures(GtkWindow *parent);
	void UpdateGraph(gboolean autozoom_x, gboolean autozoom_y);
	bool OnResponseDialog(gint response_id);
	void OnSelectionChanged(GtkTreeSelection *selection);
	bool OnCloseQuery(void);
	void OnItemActivated(GtkWidget *pGraph, gint item);

	static void response_dialog(GtkDialog *pDlg, gint response_id, CChooseApertureDlg *pMe);
	static void selection_changed(GtkTreeSelection *widget, CChooseApertureDlg *pMe);
	static void graph_item_activated(GtkWidget *pGraph, gint col, gint row, CChooseApertureDlg *pMe);
};

class CChooseApertureSimpleDlg
{
public:
	// Constructor
	CChooseApertureSimpleDlg(GtkWindow *pParent);

	// Destructor
	virtual ~CChooseApertureSimpleDlg();

	// Execute the dialog
	int Execute(CFrameSet &fset, const CApertures &aper, int default_index);
	
private:
	GtkWidget		*m_pDlg, *m_AperView;
	GtkListStore	*m_Apertures;
	CFrameSet		*m_FrameSet;
	CApertures		m_Aper;
	int				m_ApertureIndex;

	void UpdateApertures(void);
	bool OnResponseDialog(gint response_id);
	void OnSelectionChanged(GtkTreeSelection *selection);
	bool OnCloseQuery(void);

	static void response_dialog(GtkDialog *pDlg, gint response_id, CChooseApertureSimpleDlg *pMe);
	static void selection_changed(GtkTreeSelection *widget, CChooseApertureSimpleDlg *pMe);
};

#endif
