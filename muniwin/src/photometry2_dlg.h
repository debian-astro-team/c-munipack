/**************************************************************

photometry2_dlg.h (C-Munipack project)
The 'Photometry' dialog without object detection
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_PHOTOMETRY2_DLG_H
#define CMPACK_PHOTOMETRY2_DLG_H

#include <gtk/gtk.h>

#include "profile_editor.h"
#include "image_class.h"
#include "phot_class.h"
#include "proc_classes.h"
#include "catfile_class.h"
#include "helper_classes.h"
#include "popup.h"

class CPhotometry2Dlg
{
public:
	CPhotometry2Dlg(GtkWindow *pParent);
	~CPhotometry2Dlg();

	void Execute();

private:
	struct tCustomColor
	{
		double red;
		double green;
		double blue;
	};

	struct tCatalogSet
	{
		GtkWidget		*chkButton;
		GtkWidget		*setButton;
		GtkWidget		*icon;
		bool			enabled, show;
		int				layerId;
		tCustomColor	color;
		GSList			*obj_list;
	};

	GtkWindow			*m_pParent;
	GtkWidget			*m_pDlg, *m_OptionsBtn, *m_ShowTags;
	GtkWidget			*m_FrameView, *m_Preview, *m_Status, *m_CatalogsLabel;

	gint				m_FrameID, m_StatusCtx, m_StatusMsg, m_LastFocus;
	gdouble				m_LastPosX, m_LastPosY;
	guint				m_TimerId;
	GSList				*m_FrameCols;
	GtkListStore		*m_Frames;
	CmpackImageData		*m_ImageData;
	CPhotometry2Proc	m_Phot;
	CWcs				*m_Wcs;
	bool				m_Updating, m_Negative, m_RowsUpward, m_UpdatePos, m_VSTags;

	CObjectList			m_Objects;
	CSelection			m_Selection;
	CTags				m_Tags;
	CmpackChartData		*m_ChartData;

	CPopupMenu			m_SelectMenu, m_ContextMenu;

	int					m_InFiles, m_OutFiles;
	GList				*m_FileList;

	tCatalogSet			*m_CatalogBtns;
	int					m_ImageWidth, m_ImageHeight, m_CurrentCatalog;

	int ProcessFiles(class CProgressDlg *sender);
	void UpdatePreview(GtkWindow *parent, bool force_update = false);
	void UpdateControls(void);
	void ReadObjects(void);
	void ReadFrames(bool all_frames);
	void UpdateImage(CImage *img);
	void SetStatus(const char *text);
	void UpdateStatus(void);

	void ClearObjects();
	void UpdateChart();
	CmpackChartData *MakeChartData();

	void Create(double x, double y, CmpackSelectionType type);
	void Update(int star_id, CmpackSelectionType type);
	void Update(const CObjectList::tObject &obj, CmpackSelectionType type);
	void Remove(int star_id);
	void RemoveAll(void);
	void EditTag(int row, int star_id);
	void RemoveTag(int row, int star_id);
	void AutoDetect();

	void CopyWcsCoordinatesFromChart(double x, double y);
	void CopyWcsCoordinatesFromChart(int star_id);
	void CopyWcsCoordinates(CWcs *wcs, double lng, double lat);

	void SetTag(int row, int star_id, const gchar *tag);
	void UpdateObject(int row, int star_id);

	bool OnInitDialog(GtkWindow *parent, GError **error);
	bool OnResponseDialog(gint response_id);
	bool OnResponseDialog(gint response_id, GError **error);
	void OnButtonClicked(GtkWidget *button);
	void OnSelectMenu(GdkEventButton *event, gint item);
	void OnContextMenu(GdkEventButton *event);
	bool OnTagValidator(const gchar *value, GtkWindow *parent);

	void CreateCatalogsWidget(GtkBox *parent);
	void UpdateCatalogs(GtkWindow *parent);
	void ShowCatalog(GtkWindow *parent, int index, bool show);
	void UpdateCatalog(GtkWindow *parent, int index);
	void EditCatalog(int index);
	void ResetCatalogs(void);

	static void response_dialog(GtkDialog *pDlg, gint response_id, CPhotometry2Dlg *pMe);
	static void button_clicked(GtkWidget *button, CPhotometry2Dlg *pDlg);
	static void chart_item_activated(GtkWidget *pChart, gint item, CPhotometry2Dlg *pMe);
	static void chart_mouse_moved(GtkWidget *button, CPhotometry2Dlg *pDlg);
	static void chart_mouse_left(GtkWidget *button, CPhotometry2Dlg *pDlg);
	static gint button_press_event(GtkWidget *widget, GdkEventButton *event, CPhotometry2Dlg *pMe);
	static bool tag_validator(const gchar *value, GtkWindow *parent, CPhotometry2Dlg *pMe);
	static void selection_changed(GtkTreeSelection *pWidget, CPhotometry2Dlg *pMe);
	static int ExecuteProc(class CProgressDlg *sender, void *user_data);
	static gboolean timer_cb(CPhotometry2Dlg *pMe);
	static void AddToLayer(const char *objname, double ra, double dec, const char *catalog, const char *comment, void *data);

private:
	// Disable copy constructor and assignment operator
	CPhotometry2Dlg(const CPhotometry2Dlg&);
	CPhotometry2Dlg &operator=(const CPhotometry2Dlg&);
};

#endif

