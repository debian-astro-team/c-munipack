/**************************************************************

matching_dlg.h (C-Munipack project)
The 'Match stars' dialog
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_MATCHING_DLG_H
#define CMPACK_MATCHING_DLG_H

#include <gtk/gtk.h>

#include "progress_dlg.h"

class CMatchingDlg;

class CMatchingTab
{
public:
	enum tInit { INIT_MATCH, INIT_SELECT_FILE, INIT_SELECT_FRAME };

	CMatchingTab(CMatchingDlg *parent);

	GtkWidget *widget() const { return m_Box; }

	GtkWindow *parentWindow() const;

	GtkBox *optionsBox() const;

	virtual bool OnInitDialog(tInit init, int *frame_id, char **filepath, GError **error) = 0;

	virtual bool OnResponseDialog(gint response_id, GError **error) = 0;

	virtual int ProcessFiles(GList *files, int &in_files, int &out_files, CProgressDlg *sender) = 0;

	virtual void OnTabShow(void) {}
	virtual void OnTabHide(void) {}

protected:
	CMatchingDlg	*m_pParent;
	GtkWidget		*m_Box;
};

class CMatchingDlg
{
public:
	CMatchingDlg(GtkWindow *pParent);
	~CMatchingDlg();

	void Execute();
	bool SelectFrame(int *frame_id);
	bool SelectFile(char **filepath);

private:
	enum tTab { TAB_MATCH_ST, TAB_MATCH_MT, TAB_COUNT };

	friend class CMatchingTab;

	GtkWindow		*m_pParent;
	GtkWidget		*m_pDlg, *m_OptionsBtn, *m_OptionsBox;
	GtkWidget		*m_TargetFrame, *m_MTMode, *m_STMode;
	GList			*m_FileList;
	int				m_InFiles, m_OutFiles;
	CMatchingTab	*m_Tabs[TAB_COUNT];
	int				m_currentTab;

	void SelectMode(tTargetType mode);
	void SelectTab(int tabIndex);
	int ProcessFiles(class CProgressDlg *sender);
	void EditPreferences(void);

	bool OnResponseDialog(gint response_id);
	void OnButtonClicked(GtkWidget *pBtn);

	static void response_dialog(GtkDialog *pDlg, gint response_id, CMatchingDlg *pMe);
	static void button_clicked(GtkWidget *pButton, CMatchingDlg *pDlg);
	static int ExecuteProc(class CProgressDlg *sender, void *user_data);
};

#endif
