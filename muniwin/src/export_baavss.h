/**************************************************************

chartexportdlg.h (C-Munipack project)
Export chart dialog
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_EXPORT_BAAVSS_H
#define CMPACK_EXPORT_BAAVSS_H

#include <gtk/gtk.h>

// Max. number of comparison stars
#define CNAME_MAX 10

struct tBaavssExportParams
{
	gchar *star_id, *obs_code, *obs_type, *chart, *filter;
	gchar *scope, *camera, *lon, *lat, *notes;
	gchar *cname[CNAME_MAX], *cmag[CNAME_MAX];

	tBaavssExportParams();
	~tBaavssExportParams();
};

class CBaavssExportDlg
{
public:
	// Constructor
	CBaavssExportDlg(GtkWindow *pParent);

	// Destructor
	virtual ~CBaavssExportDlg();

	// Execute the dialog.
	// params:
	//	table			- [in] light curve
	bool Execute(const gchar *filename, const CTable &table, const CSelection &selection, 
		const CTags &tags, bool heliocentric, GError **error);

	// Get export parameters
	tBaavssExportParams *Params() { return &m_params; }

	// Number of comparison stars
	int NComp(void) const { return m_ncomp; }

	// Selection of stars
	CSelection *Selection() { return &m_selection; }

private:
	friend class CBaavssExportTab;

	GtkWidget		*m_pDlg, *m_pTabs;
	GtkWidget		*m_pSaveBtn, *m_pBackBtn, *m_pNextBtn, *m_pCancelBtn;
	GSList			*m_Pages;
	int				m_ncomp;
	tBaavssExportParams m_params;
	CSelection		m_selection;
	CTags			m_tags;

	void LoadParams(void);
	void SaveParams(void);

	bool SaveFile(const gchar *filename, const CTable &table, GError **error);

	void GoToFirstPage(void);
	void GoToPage(int page);
	void GoForward(void);
	void GoBackward(void);

	void UpdateTab(int page);
	bool LeavePageQuery(int response_id);

	void ExportTable(CCSVWriter &f, CTable &table);

	bool OnResponseDialog(int response_id);

	// Signal handling
	static void response_dialog(GtkDialog *pDlg, gint response_id, CBaavssExportDlg *pMe);
};

class CBaavssExportTab
{
public:
	CBaavssExportTab(class CBaavssExportDlg *parent, const gchar *caption);
	virtual ~CBaavssExportTab() {}

	virtual void Update() {}
	virtual bool Check() { return true; }
	virtual void Apply() {}

	GtkWidget *Widget(void) const { return m_tab; }
	GtkWindow *Window(void) const { return GTK_WINDOW(m_pParent->m_pDlg); }

protected:
	CBaavssExportDlg *m_pParent;

private:
	GtkWidget		*m_tab, *m_title;
};

class CBaavssExportInitTab:public CBaavssExportTab
{
public:
	CBaavssExportInitTab(class CBaavssExportDlg *parent);

	virtual void Update();
	virtual bool Check();
	virtual void Apply();

private:
	GtkWidget *m_StarId, *m_ObsCode, *m_ObsType, *m_Filter, *m_Chart;
	GtkWidget *m_SelectFilterBtn;

	void OnButtonClicked(GtkWidget *pBtn);

	static void button_clicked(GtkWidget *pButton, CBaavssExportInitTab *pMe);
};

class CBaavssExportNextTab:public CBaavssExportTab
{
public:
	CBaavssExportNextTab(class CBaavssExportDlg *parent);

	virtual void Update();
	virtual bool Check();
	virtual void Apply();

private:
	GtkWidget *m_Scope, *m_Camera, *m_Notes, *m_Lon, *m_Lat;
};

class CBaavssExportCompTab:public CBaavssExportTab
{
public:
	CBaavssExportCompTab(class CBaavssExportDlg *parent);

	virtual void Update();
	virtual bool Check();
	virtual void Apply();

private:
	GtkWidget *m_label1, *m_label2, *m_StarId, *m_Mag;

	void OnEntryChanged(GtkWidget *pEntry);

	static void entry_changed(GtkWidget *widget, CBaavssExportCompTab *pDlg);
};

class CBaavssExportEnsembleTab:public CBaavssExportTab
{
public:
	CBaavssExportEnsembleTab(class CBaavssExportDlg *parent);

	virtual void Update();
	virtual bool Check();
	virtual void Apply();

private:
	GtkWidget *m_Header[2];
	GtkWidget *m_RowLabel[CNAME_MAX], *m_StarId[CNAME_MAX], *m_Mag[CNAME_MAX];

	void OnEntryChanged(GtkWidget *pEntry);

	static void entry_changed(GtkWidget *widget, CBaavssExportEnsembleTab *pDlg);
};

class BaavssExportSelectFilterDlg
{
public:
	// Constructor
	BaavssExportSelectFilterDlg(GtkWindow *pParent, bool ccd, bool dslr);

	// Destructor
	virtual ~BaavssExportSelectFilterDlg();

	// Execute the dialog
	gchar *Execute(const gchar *defaultValue);

private:
	GtkWidget *m_pDlg, *m_ListView;
	GtkListStore *m_FilterList;
	GtkTreePath *m_SelPath;

	void Select(const gchar *name);
	void UpdateControls();
	
	void OnSelectionChanged(GtkTreeSelection *widget);
	bool OnResponseDialog(gint response_id);

	static void response_dialog(GtkDialog *pDlg, gint response_id, BaavssExportSelectFilterDlg *pMe);
	static void selection_changed(GtkTreeSelection *widget, BaavssExportSelectFilterDlg *pMe);
};

#endif
