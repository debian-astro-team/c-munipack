/**************************************************************

timecorr_dlg.h (C-Munipack project)
The 'Time correction' dialog
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_TIMECORR_DLG_H
#define CMPACK_TIMECORR_DLG_H

#include <gtk/gtk.h>

class CTimeCorrDlg
{
public:
	CTimeCorrDlg(GtkWindow *pParent);
	~CTimeCorrDlg();

	void Execute();
	bool EditParams(double *seconds);

private:
	enum tMode { SHIFT, RESET };

	GtkWindow	*m_pParent;
	GtkWidget	*m_pDlg, *m_ProcLabel, *m_AllBtn, *m_SelBtn, *m_ProcSep;
	GtkWidget	*m_BySeconds, *m_Seconds, *m_SecsUnit;
	GtkWidget	*m_ByHours, *m_Hours, *m_HoursUnit;
	GtkWidget	*m_ByDays, *m_Days, *m_DaysUnit;
	GtkWidget	*m_ByDiff, *m_ToFuture, *m_ToPast, *m_FromLabel, *m_ToLabel;
	GtkWidget	*m_YearLabel, *m_MonLabel, *m_DayLabel, *m_HourLabel, *m_MinLabel, *m_SecLabel;
	GtkWidget	*m_FromYear, *m_FromMonth, *m_FromDay, *m_FromHour, *m_FromMinute, *m_FromSecond;
	GtkWidget	*m_ToYear, *m_ToMonth, *m_ToDay, *m_ToHour, *m_ToMinute, *m_ToSecond;
	GtkWidget	*m_OptSep, *m_OptLabel, *m_ResetBtn;
	int			m_InFiles, m_OutFiles;
	GList		*m_FileList;
	tMode		m_TMode;
	double		m_TCorr;

	void Init(void);
	void GetParams(void);
	void UpdateControls(void);
	bool OnResponseDialog(gint response_id);
	void OnButtonClicked(GtkWidget *pBtn);
	int ProcessFiles(class CProgressDlg *sender);

	static void foreach_sel_files(GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer userdata);
	static gboolean foreach_all_files(GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer data);
	static void button_clicked(GtkWidget *pButton, CTimeCorrDlg *pDlg);
	static void response_dialog(GtkDialog *pDlg, gint response_id, CTimeCorrDlg *pMe);
	static int ExecuteProc(class CProgressDlg *sender, void *user_data);
};

#endif
