/**************************************************************

ccdfile_class.h (C-Munipack project)
CCD image class interface
Copyright (C) 2011 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef MUNIWIN_FRAMESET_CLASS_H
#define MUNIWIN_FRAMESET_CLASS_H

#include "helper_classes.h"
#include "phot_class.h"
#include "catfile_class.h"

//
// Frameset class interface
//
class CFrameSet
{
public:
	// Constructors
	CFrameSet(void);

	// Destructor
	virtual ~CFrameSet();

	// Is the object valid?
	bool Valid(void) const
	{ return m_Handle!=NULL; }

	// Clear frame set
	void Clear(void);

	// Create a new frameset
	void Init(const CApertures &apertures, const CSelection &sel);
	void Init(const CApertures &apertures, CPhot &phot);
	void Init(const CApertures &apertures, CCatalog &cat);

	// Load data from file
	bool Load(const gchar *filename, GError **error = NULL);

	// Export data to a file in varfind format
	bool Save(const gchar *filename, int aperture, GError **error = NULL);

	// Frameset's handle
	CmpackFrameSet *Handle(void) const
	{ return m_Handle; }

	// Go to the first frame
	bool Rewind(void);

	// Get to the next frame
	bool Next(void);

	// Get number of frames
	int Size(void);

	// Get frame information
	bool GetFrameInfo(unsigned mask, CmpackFrameInfo &info);

	// Append frame
	bool AppendFrame(const gchar *fpath, int frame_id, GError **error = NULL);
	bool AppendFrame(double juldat, int frame_id, GError **error = NULL);

	// Delete frame
	void DeleteFrame(int frame_id);

	// Delete object
	void DeleteObject(int object_id);

	// Number of apertures
	int ApertureCount(void) const;

	// Find aperture by id, returns its index
	int FindAperture(int id) const;

	// Get table of apertures
	const CApertures *Apertures(void);

	// Find object by id, returns its index
	int FindObject(int id) const;

protected:
	enum tCacheFlags {
		CF_APERTURES	= (1<<0)
	};

	CmpackFrameSet	*m_Handle;
	int				m_CacheFlags;
	CApertures		m_Apertures;

	// Refresh cached data
	void InvalidateCache(void);

private:
	// Disable copy constructor and assigment operator
	CFrameSet(const CFrameSet &orig);
	CFrameSet &operator=(const CFrameSet &orig);
};

#endif
