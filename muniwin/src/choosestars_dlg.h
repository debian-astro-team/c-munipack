/**************************************************************

stars_dlg.h (C-Munipack project)
The 'Choose stars' dialog
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_CHOOSE_STARS_DLG_H
#define CMPACK_CHOOSE_STARS_DLG_H

#include <gtk/gtk.h>

#include "editselections_dlg.h"
#include "lightcurve_dlg.h"

class CChooseStarsDlg:public CEditSelectionsDlg
{
public:
	// Constructor
	CChooseStarsDlg(GtkWindow *pParent);

	// Execute the dialog
	bool Execute(CSelection &sel, CLightCurveDlg::tParamsRec inst_mag);

protected:
	bool OnCloseQuery();
};

#endif
