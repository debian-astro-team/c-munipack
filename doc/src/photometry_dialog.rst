.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: photometry_dialog.rst,v 1.1 2015/07/06 08:19:49 dmotl Exp $

.. index::
   pair: Photometry; dialog
   
.. _photometry-dialog:

Photometry dialog (dialog)
==========================

The "Photometry" dialog is used to start the photometry process. The photometry takes the calibrated
working copy of the source frames, detects the objects and measures their brightness and other parameters.
Results are stored to separate files, which are called photometry files.


Activating the dialog
---------------------

The dialog can be activated:

.. |photometry_icon| image:: icons/photometry.png

#. from the main menu: :menuselection:`Reduce --> Photometry`.

#. from the main toolbar: |photometry_icon|


Running photometry
------------------

Any frame calibration steps (bias, dark, flat and time correction) must be applied before the photometry.

.. figure:: images/photometry_couts.png
   :align: center
   :alt: Photometry dialog
   
   The dialog for starting the photometry

\(1) It is possible to apply the operation either on all source files in the project or
on the files that are currently selected in the table of input files. This option is useful
when you are finding the ideal values of the photometry settings. It is strongly recommended
to use a single set of settings for all frames in a set.

Click the "Execute" button (2) to start the operation.

- All calibrations (bias, dark, flat and time correction) must be applied before the photometry.

- The photometry can be applied several times, the old photometry files are overwritten.


Photometry on aligned frames
----------------------------

If the source frames have been already aligned, for example when they were creating by merging frames,
you can alternatively use a :ref:`special photometry and matching mode <photometry2-dialog>`.

.. seealso:: :ref:`match-stars-dialog`, :ref:`express-reduction-dialog`, :ref:`photometry2-dialog`
