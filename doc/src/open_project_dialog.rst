.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: open_project_dialog.rst,v 1.2 2016/01/01 09:39:22 dmotl Exp $

.. index::
   pair: Open project; dialog
   
.. _open-project-dialog:

Open project (dialog)
=====================

The "Open project" dialog is used to load an existing project into the `Muniwin` program.


Activating the dialog
---------------------

The dialog can be activated:

.. |openproject_icon| image:: icons/openproject.png

#. from the main menu: :menuselection:`Project --> Open --> Project`.

#. from the main toolbar: |openproject_icon|


File browsing
-------------

.. figure:: images/openproject_couts.png
   :align: center
   :alt: Open project dialog
   
   The dialog for loading an existing project

The button (1) shows and hides the `Location` text box. The keyboard shortcut
Ctrl+L key combination does the same action.

In the "Location" text box (2) you can type a path to a file. If you don't type any path, the name
of the selected file will be displayed. You can also type the first letters of the name: it will be
auto-completed and a list of file names beginning with these letters will be displayed.

The path to the current folder is displayed at the top of the dialog (3). You can navigate along
this path by clicking on an element.

\(4) Here, you can access to your main folders and to your store devices.

\(5) Here, you can add bookmarks to folders, by using the "Add" or the "Add to Bookmarks" option you
get by right-clicking a folder in the central panel, and also remove them.

The contents of the selected folder is displayed here (6). Change your current folder by double left
clicking on a folder in this panel. Select a file with a single left click. You can use Shift and Ctrl
modifiers to select multiple files. Right-clicking a folder name opens a context menu.

If the selected file is a file recognized by the C-Munipack, the preview and short info is displayed
in the right part of the dialog (7). Double click on the preview to show a larger preview in a separate
dialog.

\(8) By clicking the Add button, you add the selected folder to bookmarks. By clicking the Remove,
you remove the selected bookmark from the list.


.. seealso:: :ref:`projects`, :ref:`new-project-dialog`, :ref:`open-file-dialog`, :ref:`export-project-dialog`
