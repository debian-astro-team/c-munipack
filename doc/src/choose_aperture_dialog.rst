.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: choose_aperture_dialog.rst,v 1.1 2015/07/06 08:19:48 dmotl Exp $

.. index::
   pair: Choose aperture; dialog
   
.. _choose-aperture-dialog:

Choose aperture (dialog)
========================

The "Choose aperture" dialog is used to select an aperture. During the photometry, each object is
measured using a set of predefined apertures of different sizes. The best aperture size depends on
width of objects and background noise level, these parameters vary for each run.

.. index::
   pair: selecting; aperture

Selecting the aperture
----------------------

.. figure:: images/chooseaperture_couts.png
   :align: center
   :alt: Choose aperture dialog
   
   The dialog for selecting an aperture

\(1) The table of defined apertures is displayed here.

\(2) The list of available data sets are available is shown here. Separate data sets are made
for each couple of a comparison star (C) and a check star (K1, K2, etc.).

The graph of curve of deviations as a function of aperture index (3) is shown in the
right part of the dialog. The point that corresponds to the currently selected aperture is
highlighted.

Left click on a point in the graph selects the aperture.

Click the "OK" button (4) to save the selection.

- Usually the best aperture corresponds to the minimum of the curve.

- The matching must be applied before the matching.

- At least one comparison and one check star must be selected before the dialog is activated.


.. seealso:: :ref:`choose-stars-dialog`, :ref:`light-curve-dialog`
