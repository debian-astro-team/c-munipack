.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: track_curve.rst,v 1.3 2016/02/27 09:14:01 dmotl Exp $

.. index::
   pair: track curve; making

Checking the telescope mount
============================

A track curve is a graph of spatial offset of CCD frames, as a function
of time. Such curve is used to monitor the stability and precision of telescope
mount and its clock drive.

You can use the C-Munipack software to make a track curve from a set of
CCD frames of a star field. As a result of the matching step during the reduction of
input frames, an offset relative to a reference file in axes X and Y are stored
to photometry files. The track curve tools uses this information to make a graph of
offset in X and Y axis respectively, as a function of time.


How to make a track curve
-------------------------

#. Process source CCD frames by normal way of reduction till star matching.
#. In the :guilabel:`Plot` menu, select the :guilabel:`Track curve`
   item. A new dialog window with a graph appears. The Julian date is on the horizontal
   axis of the graph and the offsets in pixels are on its vertical axis.

The graph consists of two data columns:

- :guilabel:`OFFSETX` - offset in horizontal direction, positive values mean
  the shift to the right, negative to the left.
  
- :guilabel:`OFFSETY` - offset in vertical direction, positive values mean
  the shift to the bottom, negative to the top.

.. figure:: images/tracklist.png
   :align: center
   :alt: Track curve
   
   The track curve

The track curve shows the periodic error of the telescope
mount. The periodic error is caused by eccentricity of a tooth wheel and
demonstrates itself as a sinusoidal signal in the track curve. Note that
several superimposed sinusoidal signals are present on the picture, each with
different period and amplitude. By finding the period of the signal, you can
tell which tooth well is eccentric.
   
.. seealso:: :ref:`make-track-curve-dialog`


Trimming outliers from the curve
--------------------------------

Is is possible to manually trim outlying observations from the curve.
You can select an individual point by a right click, you can also
select more than one point by pressing a Shift key and left mouse button
and drawing a rectangle in the graph. Then, click the right mouse button
on a point in the selection to open the context menu. There are two options:
When you select the option :guilabel:`delete from data set`,
the selected measurements are removed from the current curve, the
data will be shown when you make another track curve or :guilabel:`Rebuild`
the actual curve. The other option :guilabel:`remove from project`
means that the frames corresponding to the selected measurements are removed
permanently from the list of input files (see: Main application window) and
such measurements won't be included in any other output. It is not allowed
to remove a reference frame.

.. seealso:: :ref:`track-curve-dialog`
