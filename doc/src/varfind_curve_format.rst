.. C-Munipack - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: varfind_curve_format.rst,v 1.1 2015/07/12 07:44:57 dmotl Exp $

.. index::
   pair: Varfind curve; format
      
.. _varfind-curve-format:

Varfind curve format
====================

The Varfind (mag-dev) curve file consists of table of indexes, mean magnitudes and standard deviations 
for all detected stars. Such data is intended for detecting variable stars on a set 
of CCD frames. Magnitudes are always in differential form, they are relative to 
the comparison star.

Output files are stored in the ASCII format; the end of the line is represented by 
``CR+LF`` in the DOS/MS Windows environment and by ``LF`` in the Unix/Linux 
environment. First line contains a list of column names separated by single space
character. Second line consists of additional information and has no special 
formatting, it must not start by number, though. 

On the following lines, the table values are stored. The values are separated by tab 
character or single space, rows are separated by the end-of-line character (see above). 
Parsers must ignore all additional white characters. See table :ref:`1 <table:fnd_head>` 
for short description of columns.

.. _table:fnd_head:

Table 1

========== ===========
Keyword    Description
========== ===========
INDEX      Ordinal number of a star in the reference file 
MEAN_MAG   Mean relative magnitude of a star 
STDEV      Standard deviation of previous value 
GOODPOINTS Number of measurements used for computation 
========== ===========
