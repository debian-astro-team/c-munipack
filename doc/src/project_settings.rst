.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: project_settings.rst,v 1.2 2016/06/18 06:10:08 dmotl Exp $

.. index::
   single: profile
   
.. _project-settings:

Project settings
================

This chapeter provides a complete list of project settings and the description
of each parameter.

.. rubric:: Camera

Camera parameters

- Read noise - readout noise in electrons, you should enter the proper value stated
  in the camera's documentation. Default: 15.0
  
- Gain - number of electrons per ADU, you should enter the proper value stated in
  the camera's documentation. Default: 2.3
  

.. rubric:: Source frames

Source frame conversion and processing parameters

- Min. pixel value - lowest value of pixel in ADU. All pixels, which are equal to
  or less than specified value are considered to be invalid, by default: 0
  
- Max. pixel value - highest value of pixel in ADU. All pixels, which are equal to
  or greater than specified value are considered to be overxposed, by default: 65535.
  
- Image data format - pixel data format in which the working files are
  stored. If the "Autodetection" is selected, the program keeps the format of
  original source files. Otherwise, the data are transformed during the Fetch/Convert
  operation. Default: Autodetection
  
- Binning - reduction of frame size. When this feature is enabled, the
  program will sum the pixel values from groups of neighboring pixels making
  one pixel for each group in the working frame. This transformation is performed
  in the Fetch/Convert operation. This is useful for processing images from the
  hi-resolution DSLR and CCD cameras. Please note, that the sum of pixel values
  are computed (not average), so you when you enable binning, you will probably
  need to increase the "Max. pixel value". Default: No binning
  
- Border - skip invalid parts of the source frames. Set up frame borders
  to nonzero if you need to remove parts of the frames from the processing. Default: 0
  

.. rubric:: Calibration

Parameters for bias, dark and flat corrections.

- Standard calibration - in the standard calibration scheme, only
  two correction frames are used - a dark frame, which also includes the bias,
  and a flat frame. The standard calibration scheme is simpler to follow,
  but it requires that the dark frame must be of the same exposure duration
  as source frames.
  
- Advanced calibration - in the advanced calibration scheme, all three
  correction frames are applied separately - a bias frame, a dark frame
  and a flat frame, in that order. The dark frame must not contain a bias,
  such dark frame is called scalable dark frame, because it need not to be
  of the same exposure duration as source frames.
  

.. rubric:: Star detection

Parameters for detection of objects on a frame. This step is done
during the Photometry operation.

- Filter width (FWHM) - full width at half maximum of the Gaussian filter.
  The low-pass filter is applied to the source image before searching for
  local maxima. If profile of objects on frames roughly corresponds to the
  Gaussian function, the good value for the filter width is the FWHM of
  objects. You can use the Quick Photometry tool in the Preview window
  to measure the FWHM of objects. In case the profile of objects is not
  a Gaussian function and the program splits stars into two or more objects,
  you need to raise this value. Default: 3.0
  
- Detection threshold - the level which a brightness enhancement must have above
  the background level to be considered as a real object. The value is given
  in standard deviations. The best starting point is the default value 4.0.
  Process a set of frames and check the results. If you need to detect fainter
  object, reduce it. If many background artefacts are considered as stars,
  raise it. Run the photometry again and repeat the steps until the results
  are satisfactory. Please note, that the filter width also affects the
  detection capabilities. Default: 4.0
  
- Minimum and maximum sharpness - minimum and maximum value of sharpness
  which a brightness enhancement must have to be considered a real object. The main
  purpose is to eliminate bad pixels, it may also help to eliminate blurred objects
  such as nebulaes and galaxies. Defaults: 0.2 and 1.0
  
- Minimum and maximum roundness - minimum and maximum value of roundness
  which a brightness enhancement must have to be considered a real object. This test
  is intended to eliminate bad rows and columns, it may also help to eliminate
  galaxies or cosmic particle traces. Defaults: -1.0 and 1.0

- Max. stars - maximum number of objects. This parameter is used to limit the
  photometry file to a reasonable number of objects. The stars are sorted by 
  their magnitudes and specified number of stars from the top of the list 
  is preserved, the rest is discarded. Default: 10000


.. rubric:: Photometry

Parameters for aperture photometry.

- Aperture - the program computes brightness of stars in all defined
  apertures. It is possible to define at most 12 apertures. An user can
  select one of them when making output data, a light curve for example.
  The aperture is defined by its radius in pixels.
  
- Inner and outer radius - local background level is measured in
  an annulus that is centered on the object. These parameters define
  its inner and outer radius in pixels. Defaults: 20.0 and 30.0.

    
.. rubric:: Matching

Parameters for matching stars.

- Standard algorithm - identifies similar polygons of stars and
  find the best match. It works well if a source frame is scaled, rotated
  or even mirrored with respect to the reference frame. The limitation is
  that it requires at least three stars on a frame.
  
- Algorithm for sparse fields - this algorithm can match frames which
  contain at least one star. Scale, tilt and position of a source frame
  with respect to the reference frame should be close. Otherwise, it
  leads to a false match.
  
- Algorithm for dense fields - this algorithm works well on very dense
  fields containing multitude of objects of similar magnitude, like 
  globular clusters. It is slower that the standard algorithm.
  
.. rubric:: Standard algorithm parameters

- Read stars - if a source or reference file contain more than
  given number of stars, the program reads only that number of
  brightest stars. This limit speeds up the computation. Default: 10
  
- Identification stars - a number of stars that are identified
  in each iteration. More information is given in the theory of operation.
  Default: 5
  
- Clipping factor - tolerance used during the iterative evaluation
  of the transformation coefficients. Default: 2.5
  
.. rubric:: Parameters of sparse-fields algorithm

- Max. offset - maximum offset between a source frame and a reference
  frame in pixels. It is intended to eliminate false matches. This parameter
  should as high as the highest offset of an object of frames, but no higher.
  Default: 2.0

.. rubric:: Parameters of dense-fields algorithm

- Clipping factor - tolerance used during the iterative evaluation
  of the transformation coefficients. Default: 2.5
  

.. rubric:: Master bias

Parameters for making a master bias frame.

- Output data format - pixel data format in which the master bias frame is
  stored. If the "Autodetection" is selected, the program keeps the format of
  working files. Otherwise, the data are transformed into specified format.
  Default: Autodetection
  

.. rubric:: Master dark

Parameters for making a master dark frame.

- Make scalable dark frame - make a master dark frame that can be
  scaled to match the exposure duration of a source frame and a dark
  correction frame (see the advanced calibration scheme for further
  information). When you check this option, you have to subtract
  a bias frame from your dark frames before you make a master dark frame.
  
- Output data format - pixel data format in which the master dark frame is
  stored. If the "Autodetection" is selected, the program keeps the format of
  working files. Otherwise, the data are transformed into specified format.
  Default: Autodetection
  

.. rubric:: Master flat

Parameters for making a master flat frame.

- Output data format - pixel data format in which the master dark frame is
  stored. If the "Autodetection" is selected, the program keeps the format of
  working files. Otherwise, the data are transformed into specified format.
  Default: Autodetection
  
- Output mean level - the program normalizes the master flat frame so
  its mean level is equal to a given value. If it is set too high, the output
  frame has many overexposed pixels. On the other hand, too low value raises
  the noise when a integer pixel format is used, because pixel values in a frame
  are rounded to the nearest integer values. Default: 10000
  

.. rubric:: Merge frames

Parameters for frame merging.

- Output data format - pixel data format in which the merged frame is
  stored. If the "Autodetection" is selected, the program keeps the format of
  working files. Otherwise, the data are transformed into specified format.
  Default: Autodetection
  

.. rubric:: Find variables

Parameters for finding new variables.

- Clipping threshold - minimum share of valid brightness measurements
  that a star must have to be included in the mag-dev graph. This test
  is intended to eliminate stars that have few valid measurements only
  and their position in the mag-dev graph more or less random. Default: 60
  

.. rubric:: Observer

Observer's geographic coordinates.

- Name - any text identifying the observing location, i.e. name of the town

- Longitude - observer's longitude in degrees. Use the hexagesimal format, separate
  the fields by a space character. Enter 'E' character at the first position in a string to indicate
  that the location is on the eastern hemisphere or 'W' character for locations on a western hemisphere.
  
- Latitude - observer's latitude in degrees. Use the hexagesimal format, separate
  the fields by a space character. Enter 'N' character at the first position in s string to indicate
  that the location is on the north hemisphere or 'S' character for locations on a southern hemisphere.

  
.. seealso:: :ref:`projects`, :ref:`profiles`, :ref:`environment-options-dialog`,
   :ref:`project-settings-dialog`, :ref:`edit-profiles-dialog`
