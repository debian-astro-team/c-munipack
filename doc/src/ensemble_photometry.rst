.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: ensemble_photometry.rst,v 1.2 2016/01/02 09:53:13 dmotl Exp $

.. index::
   pair: Ensemble; photometry
   
.. _ensemble-photometry:

Ensemble photometry
===================

The ensemble photometry is a technique of a light curve construction which utilitizes measurements
of multiple comparison (constant) stars in order to reduce the random errors. The method implemented 
in the C-Munipack software can be called "strict" ensemble photometry, because it requires that *all*
stars from the comparison star set are present on every frame, if this is not satistifed, the frame
is discarded from the light curve. Fluxes (not magnitudes) of the comparison stars are summed up to 
get instrumental magnitude of an "artificial" comparison star which is than used to find out 
differential brightness of a variable star.

.. seealso:: :ref:`light-curve`
