.. C-Munipack - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: theory.rst,v 1.3 2016/02/27 09:14:01 dmotl Exp $

.. _theory:   
   
Theory of operation
===================

This section of the user manual give a reader a detailed explanation how the data are processed 
in the C-Munipack software, how the output data are computed and how the temporary and the output 
data are formatted. The text is divided into the two sections; the description of the algorithms
incorporated in the software is provided in the first part of the section, the second part comprises
the specifications of the file formats.

.. toctree::
   :maxdepth: 1

   algorithms
   files
