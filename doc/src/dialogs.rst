.. C-Munipack - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: dialogs.rst,v 1.2 2016/01/01 09:39:22 dmotl Exp $

.. _dialogs:   
   
Dialogs and tools
=================

.. toctree::
   :maxdepth: 1

   about_muniwin_dialog
   add_frames_from_folder_dialog
   add_individual_frames_dialog
   air_mass_coefficient_dialog
   air_mass_curve_dialog
   bias_correction_dialog
   catalog_file_window
   ccd_temperature_dialog
   chart_dialog
   choose_aperture_dialog
   choose_stars_dialog
   dark_correction_dialog
   edit_profiles_dialog
   environment_options_dialog
   export_project_dialog
   express_reduction_dialog
   fetch_convert_files_dialog
   find_variables_dialog
   flat_correction_dialog
   frame_preview_window
   graph_window
   heliocentric_correction_dialog
   image_file_window
   import_profile_dialog
   import_project_dialog
   jd_converter_dialog
   light_curve_dialog
   load_profile_dialog
   main_window   
   make_catalog_file_dialog
   make_find_variables_dialog
   master_bias_frame_dialog
   master_dark_frame_dialog
   master_flat_frame_dialog
   match_stars_dialog
   merge_frames_dialog
   message_log_dialog
   new_project_dialog
   object_coordinates_dialog
   object_properties_dialog
   observer_coordinates_dialog
   open_file_dialog
   open_project_dialog
   photometry_dialog
   photometry2_dialog
   photometry_file_window
   plot_air_mass_dialog
   plot_ccd_temperature_dialog
   plot_light_curve_dialog
   plot_object_properties_dialog
   plot_track_curve_dialog
   process_new_frames_dialog
   project_settings_dialog
   save_chart_dialog
   save_graph_dialog
   save_profile_dialog
   save_table_dialog
   thumbnails_dialog
   time_correction_dialog
   track_curve_dialog

   