.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: flat_correction_dialog.rst,v 1.1 2015/07/06 08:19:48 dmotl Exp $

.. index::
   pair: Flat correction; dialog

.. _flat-correction-dialog:

Flat correction (dialog)
========================

The "Flat correction" dialog is used to select a flat frame during the calibration of the
source files. The flat-frame calibration is used in the standard and the advanced calibration
scheme.


Activating the dialog
---------------------

The dialog can be activated:

.. |flatcorr_icon| image:: icons/flatcorr.png

#. from the main menu: :menuselection:`Reduce --> Flat correction`.

#. from the main toolbar: |flatcorr_icon|


File browsing
-------------

.. figure:: images/flatcorr_couts.png
   :align: center
   :alt: Flat correction dialog
   
   The dialog for selecting a flat frame

The button "Type a file name" (1) shows and hides the "Location" text box. The keyboard shortcut
Ctrl+L key combination does the same action.

In the "Location" text box (2) you can type a path to a file. If you don't type any path, the name
of the selected file will be displayed. You can also type the first letters of the name: it will be
auto-completed and a list of file names beginning with these letters will be displayed.

\(3) The path to the current folder is displayed. You can navigate along this path by clicking on
an element.

\(4) Here, you can access to your main folders and to your store devices.

\(5) Here, you can add bookmarks to folders, by using the "Add" or the "Add to Bookmarks" option you
get by right-clicking a folder in the central panel, and also remove them.

The contents of the selected folder is displayed here (6). Change your current folder by double left
clicking on a folder in this panel. Select a file with a single left click. You can start the operation
by clicking on the Execute button or by a double left click on a file. Right-clicking a folder name
opens a context menu.

If the selected file is a file recognized by the C-Munipack, the preview and short info is displayed
in the right part of the dialog (7). Double click on the preview to show a larger preview in a separate
dialog.

\(8) By clicking the Add button, you add the selected folder to bookmarks.

By clicking the Remove, you remove the selected bookmark from the list.

\(9) It is possible to apply the operation on all source files in the project or
on the files that are currently selected in the table of input files.


.. index::
   pair: flat; correction

Applying the flat correction
----------------------------

Click the "Execute" button (10) to start the operation. The flat correction is applied to a working
copy of source files. The working copy is replaced by corrected frames.

- A working copy of source frame must be made before the flat correction.

- The bias and the dark correction must be applied before the flat correction, if necessary.

- The flat correction can be applied to each frame only once.

- The flat correction must be applied before the photometry.

If you make a mistake, you can revoke the step by making a fresh copy of source frames
by means of the "Fetch/convert files" function.


.. seealso:: :ref:`main-window`, :ref:`master-flat-frame-dialog`
