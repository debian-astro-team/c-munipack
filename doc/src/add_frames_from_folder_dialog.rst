.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: add_frames_from_folder_dialog.rst,v 1.1 2015/07/06 08:19:48 dmotl Exp $

.. index:: 
   pair: Add frames from folder; dialog

.. _add-frames-from-folder-dialog:

Add frames from folder (dialog)
===============================

The "Add frames from folder" dialog is used to add source frames into the current project. Unlike the
closely related dialog "Add individual frames", it takes all files from a selected folder and optionally its
subfolders.


Activating the dialog
---------------------

The dialog can be activated:

.. |addfolder_icon| image:: icons/addfolder.png

#. from the main menu: :menuselection:`Frames --> Add frames from folder`.

#. from the main toolbar: |addfolder_icon|


File browsing
-------------

.. figure:: images/addfolder_couts.png
   :align: center
   :alt: Add folder dialog
   
   The dialog for adding multiple files to the project.

The button "Type a file name" (1) shows and hides the "Location" text box. The keyboard shortcut
Ctrl+L key combination does the same action.

In the "Location" text box (2) you can type a path to a folder. If you don't type any path, the name
of the selected folder will be displayed. You can also type the first letters of the name: it will be
auto-completed and a list of folders names beginning with these letters will be displayed.

The path to the current folder is displayed at the top of the dialog (3). You can navigate along this
path by clicking on an element.

\(4) Here, you can access to your main folders and to your store devices.

\(5) Here, you can add bookmarks to folders, by using the "Add" or the "Add to Bookmarks" option you
get by right-clicking a folder in the central panel, and also remove them.

The contents of the selected folder is displayed here (6). Change your current folder by double left
clicking on a folder in this panel. Right-clicking a folder name opens a context menu.

\(7) By clicking the Add button, you add the selected folder to bookmarks. By clicking the Remove,
you remove the selected bookmark from the list.

Check the option "Include subdirectories" (8) to instruct the program that it shall also
search for CCD frames in the subfolders of the current folder.


Including files the project
---------------------------

To add the files from the current folder, click the "Add" button (9). The program searches it for
available files. Several situations may occur:

- If the file is not a valid CCD frame, it is ignored.

- If the file is already included in the project, it is ignored.

- If there are multiple files left and they have different color filter name, the program
  shows a new dialog and asks an user to select which filters shall be added to the project.
  You can use the "All files" option or select one or more items in the box, other files will be
  ignored.
  
- If there are multiple files left and they have different exposure duration, the program
  shows another dialog and asks an user to select which exposures shall be added to the project.
  You can use the "All files" option or select one or more items in the box, other files will be
  ignored.
  
- If there are multiple files left and they have different object designation, the program
  shows another dialog and asks an user to select which objects shall be added to the project.
  You can use the "All files" option or select one or more items in the box, other files will be
  ignored.
  
When the action is completed, the dialog is not closed; you can go on to add files from another
folder or close the dialog by the "Close" button (10).

.. seealso:: :ref:`add-individual-frames-dialog`, :ref:`main-window`
