.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: make_find_variables_dialog.rst,v 1.1 2015/07/06 08:19:49 dmotl Exp $

.. index::
   pair: Make find variables; dialog
   
.. _make-find-variables-dialog:

Make find variables dialog
==========================

The "Make find variables" dialog is used to select the data source for the :ref:`find-variables-dialog`.
Basically, there are two options - the data can be construed from the current project or
loaded from a varfind file.


Activating the dialog
---------------------

The dialog can be activated:

.. |varfind_icon| image:: icons/varfind.png

#. from the main menu: :menuselection:`Tools --> Find variables`.

#. from the main toolbar: |varfind_icon|


The dialog controls
-------------------

.. figure:: images/makevarfind_couts.png
   :align: center
   :alt: Make find variables dialog
   
   "Make find variables" dialog

\(1) It is possible to include all source files in the project or the files that are
currently selected in the table of input files.

\(2) To load the data from an external varfind file, check this option. The path to the
file must be specified into the edit field \(3). Click the :guilabel:`Browse` button to
open a standard file selection dialog. 

Click the button (4) to proceed.


.. seealso:: :ref:`finding-variables`, :ref:`find-variables-dialog`
