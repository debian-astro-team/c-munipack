.. C-Munipack - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: tracking_moving_target.rst,v 1.2 2016/02/28 15:55:01 dmotl Exp $

.. index::
   pair: tracking; moving target

.. _tracking-moving-target: 

Tracking a moving target
========================

The process of matching frames, based on features that are preserved under certain transformations, 
as it was described in the :ref:`previous chapter <frame-matching>`, works well for stellar objects; 
detected stars form the same pattern on each frame. Mismatch between any two frames is change 
in spatial offset, scale and rotation, in some cases also a mirror operation applies. The task of
matching a set of frames is about finding a set of equations (matrix) that transforms coordinates 
of any vector (point) on one frame to a vector in a reference coordinate system.

In case of moving targets, such as minor Solar System bodies, in additional to the mismatch 
desribed above, another source of mismatch is introduced - an object of interest, having its
own radial movement, changes its position with respect to surrounding stars between the 
frames (in time). Therefore, if a pattern used for the frame matching would include such an object, 
it would not match. The challange in reduction of an observation of minor bodies is
in founding an image of the object of interest on each source frame. Fortunately, it can
be reasonably expected, that such an object does not move randomly, but follows a simple 
trajectory that can be fitted using a simple polynomial function or spline.

The C-Munipack software approaches this task with help of a user. He picks up a small set
of frames, called key frames, and on each frame he indicates an object that corresponds
to the target. As in the frame matching for stellar objects, a user also chooses one frame
to be a reference frame. The rest of the process does not require any further interaction.

The key frames are processed first. The object indicated by the user as the target
is left out from the reference frame and the key frame. Then, the same frame matching 
process as in case of a stellar object is applied to those two frames. This is repeated for each 
key frame and the equations that transforms frame coordinates into reference coordinates
are preserved.

The software keeps an observation time and position of the target object from each key 
frame and transforms those positions into the reference coordinate system. Then, using 
linear least squares method, the transformed positions are interpolated using two 
second order polynomial function of time t:

.. math::
   :label: f_x
   
   x(t) = A_x + B_x . t + C_x . t^2
   
and

.. math::
   :label: f_y
      
   y(t) = A_y + B_y . t + C_y . t^2
   
Using the functions :math:`x(t)` and :math:`y(t)` a position of the target in the reference
coordinate system can be determined an any given time :math:`t`.

Subsequently, the matching process continues with processing all other frames. The software reads an 
observation time :math:`t` of a frame and using the functions :math:`x(t)` and :math:`y(t)`,
determines the position of the target in reference coordinate system in time :math:`t`. Then,
the target object's position on the reference frame is changed to values :math:`x(t)` 
and :math:`y(t)` and a standard frame matching process is applied. If the fit is correct,
an object close to the expected position of the target is found, the pattern of objects
on the reference frame and the pattern on source frame match and an identification of 
the target object is found as a result of the standard frame matching process.
