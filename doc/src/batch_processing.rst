.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: batch_processing.rst,v 1.1 2015/07/06 08:19:48 dmotl Exp $

.. index::
   pair: batch; processing

.. batch-processing:

Batch processing
================
        
Repetitive challenge with reduction of CCD observation is in treating
a large number of files. To do this job effectively, it is necessary to
execute each reduction step for all frames in subsequent manner. This issue
is inconsiderable namely for calibration of CCD frames - the calibration data
should be fetched into the memory only once and then applied to all frames.
Thus one need to execute an user command for a large set of input files.

One straightforward way in processing large batch of frames is writing 
the names of input files as separate command-line arguments. More shrewd users
would use the wild-card notation, but it doesn't work in more complicated situation. 
There is yet another way - using directory files.

The directory file is a simple text file. Each line consists of a file
path or name. Such file can be obtained using simple command.
		
On GNU/Linux::

    ls ??? &gt; dirfile.txt 

On Windows::

    dir /b *.st7 &gt; dirfile.txt
		
If the files are placed not in the current working directory, you have to 
specify the proper path in full or shortened form relative to the current working 
directory. It is not allowed to use the wild-card notation here. Use the :samp:`-i` 
option to instruct the program to read the file.

When a program produces a number of files, it is also necessary to assign 
a distinct name for each output file. By default, the output files are stored to 
the current working directory. Their names are derived from the command name followed 
by a sequential number starting by 1. By means of the command-line options, it is
possible to override this behavior.

The :samp:`-o` option sets the format string; it may contain a path where 
the files shall be stored to. Special meaning has a sequence of question marks, it
is replaced by the ordinal number of a file	indented by leading zeros to at least the 
same number of decimal places as the number of the question marks. By means of the 
:samp:`-i` option, you can modify the ordinal number of the first frame.

The following command writes its output to files :file:`out010.fts`,
:file:`out011.fts` and :file:`out012.fts`::
		
    darkbat -o out???.fts -i 10 dark.fts in001.fts in002.fts in003.fts
		
If you are going to use the files produced by one command as the input files
of the subsequent operation, you can ask the former to make a directory file for you
by including the :samp:`-g` option followed by a file name.
