.. C-Munipack - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: light_curve.rst,v 1.4 2016/02/28 10:27:21 dmotl Exp $

.. index::
   pair: light; curve
      
.. _light-curve:

Light curve
===========

The light curve is a graph of intensity of an object (a variable star
in most cases), as a function of time. The light curve of the observered object
is usually presented in a form of a graph that has time on the independent axis
and the intensity in magnitudes on the other axis. Each point in the graph
represents single measurement, which in simple cases corresponds to a single
input CCD frame.

To make a light curve of an observed object, we need a set of photometry
files with cross-reference information (matched photometry files). An user have
to the program which stars are variable stars and comparison stars. In case of
aperture photometry, he also have to choose an aperture.

The Muniwin program allows an user to apply further corrections and
include additional information to a light curve, all of which will be discussed
later in the text.

This chapter explains how to make a light curve of stationary objects, such as 
variable stars and exoplanets. For a guide on making a light curve of moving 
objects (minor Solar System bodies) follow :ref:`this link <moving-targets>`.



Before you start
----------------

Before you start the reduction of your own CCD frames, you may
need to perform several pre-processing steps. Though it isn't necessary,
combining the several correction frames into so-called "master" ones
is advisory, because it reduces the noise and makes the result more
precise. The method of making master correction frames is described in
separate chapters.
   
.. seealso:: :ref:`master-dark-frame` and :ref:`master-flat-frame`


Creating a new project
----------------------

First of all, we will create a new :ref:`project <projects>`. To begin with processing of a variable star observation, 
we create a new project. To do so, open the :guilabel:`Project` menu and activate the :guilabel:`New` item. A new dialog 
appears.

.. figure:: images/newproject.png
   :align: center
   :alt: "New project" dialog
   
   The dialog for making a new project.
   
Fill in a name that will be assigned to the project. Because the name of the file that keeps track of the data related
to the project, the project file, is derived from the name, some characters cannot appear in the project name, do not 
use: / \ ? % * : | " < and >.

The field :guilabel:`Location` shows a path to the directory where a new project will be created. Edit the path to 
change the location, you can also click the :guilabel:`Browse` button to select a directory in a separate dialog.

The dialog also displays a list of available profiles. A profiles provides an intial set of configuration parameters 
into a new project. When you confirm the dialog, you should be in the main window again now. The table of input files 
shown there is empty.

.. figure:: images/main.png
   :align: center
   :alt: Main application window
   
   The main application window with the table of input files, now empty.

.. seealso:: :ref:`new-project-dialog` and :ref:`main-window`.   
   
Input files
-----------

Now, we are going to tell the program which files we are going to work on. These files are called the input 
files. Their list is displayed in the table in the main application window. When the application is closed,
the list of files are saved to the disk and it is restored back when the program is launched again.

Supposing that the table now consists of files from your previous task, let's get rid of them. Please, use 
:menuselection:`Files --> Clear files` to start a new task instead of just removing the files from 
the table. Besides the clearing the table of input files, this function resets all internal variables, too.

Now, we need to populate the table with the CCD frames we're going to reduce. There are two methods how to 
achieve that - adding a individual files or adding all files from a folder. Which way is the best for you 
depends on organization of your observations on the disk. I'd suggest you to make a folder for each year, 
a folder for each night in it, the a subfolder for a name of object or another view field identification and 
finally a subfolder named upon the color filter (if you use more of them). In this case, the "Add frames from a folder" 
method is more convenient.

Click on :menuselection:`Files --> Add frames from folder` in the main menu. A new dialog appears. In the dialog, 
find a folder where the inputs files are stored in. Click on an entry in the :guilabel:`Places` pane to go to one 
of a preselected folders, double click in the middle pane enters the folder. The buttons in the upper part 
of the dialog shows your current position in the directory tree, you can use them to go to one of the parent
folders. Enter the folder with the input files - you should see them in the middle pane. Then, click on the 
:guilabel:`Add` button to add files to the table of input files. The program shows the number of added files in
the separate dialog. The :guilabel:`Add frames from folder` dialog is not closed automatically and allows a user 
to continue. Click on the :guilabel:`OK` button to close the dialog and return to the main window.

.. figure:: images/addfolder.png
   :align: center
   :alt: "Add folder" dialog
   
   The "Add folder" dialog with the place selection box (left), the file
   selection box (middle) and the preview panel (right).

If you want to reduce only a subset of files from a folder, click on :menuselection:`Files --> Add individual frames` 
in the main menu. A new dialog appears, similar to the previous one. In the dialog, find a folder where the inputs 
files are stored in. Click on an entry in the :guilabel:`Places` pane to go to one of a preselected folders, double
click in the middle pane enters the folder. The buttons in the upper part of the dialog shows your current position 
in the directory tree, you can use them to go to one of the parent folders. In the middle pane, select the files 
using the :kbd:`Ctrl` modifier to include and exclude a single file and the :kbd:`Shift` modifier to include a range 
of files. Then, click on the :guilabel:`Add` button to add selected files to the table of input files. The program 
shows the number of added files in the separate dialog. The :guilabel:`Add individual frames` dialog is not closed 
automatically and  allows a user to continue. Click on the :guilabel:`OK` button to close the dialog and return to 
the main window.

.. figure:: images/addfiles.png
   :align: center
   :alt: "Add files" dialog
   
   The "Add files" dialog with the place selection box (left), the file
   selection box (middle) and the preview panel (right).

.. seealso:: :ref:`add-frames-from-folder-dialog` and :ref:`add-individual-frames-dialog`.


Frame reduction
---------------

Reduction of CCD frames is a process that takes source CCD frames, performs their conversion and calibration,
detects stars on each frame and mearures their intensity and finally finds correlation (match) between objects 
that were found in the data set. The process of reduction prepares the data that are necessary for making a light 
curve or a variable star.

The reduction consists of several steps - conversion, calibration, photometry and matching. They can be invoked
step-by-step manually. The preferred way is to use the :guilabel:`Express reduction` dialog that allows to perform 
these steps in a batch. Using the menu, activate the :menuselection:`Reduce --> Express reduction` item.
A new dialog appears. The dialog has several options aligned to the left, Each of them relates to an optional 
step in the reduction process. 

.. figure:: images/express.png
   :alt: "Express reduction" dialog
   :align: center
   
   The dialog for setting parameters of the reduction process

.. rubric:: Fetch/convert files
      
Check the :guilabel:`Fetch/convert files`. In this step, the program makes copy of the source CCD frames. 
This is necessary, because the following calibration steps will modify them and we don't want the program 
to change our precious source data.

.. figure:: images/express_convert.png
   :alt: "Express reduction" dialog
   :align: center

.. note: DSLR camera - color photometry
   Be careful when you are using color components from the RAW images - see :ref:`processing_raw_images`.

.. rubric:: Dark-frame correction

A raw CCD frame consists of several components. By the calibration process, we get rid of those 
which affect the result of the photometry. In some literature, the calibration is depicted as the 
peeling of an onion. There are three major components which a raw frame consists of - the current 
made by incident light, current made thermal drift of electrons (so-called dark current) and 
constant bias level. In standard calibration scheme, which we will demonstrate here, the dark-frame 
correction subtracts the dark current and the also the bias. Because of the nature of the dark current, 
it is necessary to use a correction frame of the same exposure duration as source files and it must 
be carried out on the same CCD temperature, too. Thus, the properly working temperature regulation 
on your CCD camera is vital.

.. figure:: images/express_dark.png
   :alt: "Express reduction" dialog
   :align: center

.. rubric:: Flat-frame correction

Then, we have to compensate the spatial non-uniformity of a detector and whole optical
system. These non-uniformities are due to the fabrication process of a CCD chip and they are
also natural properties of all real optical components, lenses in particular. The flat-frame
correction uses a flat-frame to smooth them away. The flat-frame is a frame carried out 
while the telescope is pointed to uniformly luminous area. In practice, this condition is
very difficult to achieve, the clear sky before dusk is usually used instead.

.. figure:: images/express_flat.png
   :alt: "Express reduction" dialog
   :align: center

   
.. rubric:: Photometry

The photometry is a process that detects stars on a CCD frame and measures their brightness. Unlike the previous 
steps, the result is saved to a special file, so-called the photometry file. There are a lot of parameters which 
affect the star detection and also the brightness computation. In this example, the default values work fine, 
but I would suggest you to become familiar with at least two of them - FWHM and Threshold - before
you start a real work. Check the :guilabel:`Photometry` option.

.. figure:: images/express_photometry.png
   :alt: "Express reduction" dialog
   :align: center


FWHM
	The FWHM parameter specify the expected width of stars on a frame.
	The value is the Full Width at Half Maximum in pixels. The parameter
	controls the behavior of the low-pass digital filter, which is used
	in the star detection algorithm.

Threshold
	The Threshold parameter specify the lowest brightness of detected
	stars. Fainter objects are considered to be background artifacts and thus
	sorted out. The value is dimensionless coefficient.

Once you tune up the parameter for your environment, usually it is not
necessary to adjust them for every task, unless the quality of your images
varies considerably. In the first iteration, you can use the default values
(FWHM = 3.0 and Threshold = 4.0) and do the photometry. Click on the
:menuselection:`Reduce --> Photometry` item
in the main menu and confirm the new dialog by the :guilabel:`OK`.
Then, by double click on a frame in the main window open the preview window
and check the results. If there are stars which have been detected as a close
binary although it is not true, you should increase the *FWHM*
value. If the stars you are interested in are not detected, try decrease the
*Threshold* value. If it doesn't help, decrease the
*FWHM*. If there is a lot of background artifacts detected
as a real stars, increase the *Threshold*. By several
iterations, adjust the parameters, so all the stars you are interested in are
detected and there are no false binaries.


.. rubric:: Matching
   
The previous command treated all source files independently. As a result of this, a star #1 
in one file is not necessarily the same as a star #1 in another file. The matching is a process 
which finds corresponding stars on source frames and assigns an unique identifier. Check the
:guilabel:`Matching` option.

It is necessary to select one frame from the set of source frames that all other frames are matched 
to, this frame is called a reference frame. In my experience, the frame with the greatest number of 
stars works the best. Back to our example, let's pick up the first one.

.. figure:: images/express_matching.png
   :alt: "Express reduction" dialog
   :align: center

   
.. rubric:: Invoking the reduction process

In previous steps, we have configured parameters of the reduction process and we are ready to start
it. Click the :guilabel:`OK` button. During the execution a new window appears displaying the state 
of the process; all the information is also presented there. This window will be automatically 
closed after finishing the process. Wait for the process to finish. 

.. figure:: images/progress.png
   :alt: Progress dialog
   :align: center
   
   The dialog displayed during time demanding operations.

After finishing, the icon in the file table changes; the information about the time of observation, 
the length of the exposition and the used filter is filled in. In case some of the frames could not be 
processed successfully, the entry is be marked with a special icon and in the :guilabel:`Status` column 
the error message is indicated.

.. figure:: images/inputfiles3.png
   :alt: Main application window
   :align: center
   
   The main application window after the recution.
   
.. seealso:: :ref:`express-reduction-dialog`.

   
Making a light curve
--------------------

Reduce the source CCD frames (see the chapter "Reduction of CCD frames"). Then click on the :menuselection:`Plot --> Light curve`
item in the main menu. A new dialog appears. The dialog allows an user to set up the options for the light curve. 
If you want only a subset of frames from the project to be included in the curve, check the selected files only 
option in the box. The program can also include several corrections and coefficients to the output file, these 
features are discussed later in the text.

Confirm the dialog by the :guilabel:`OK` button.

.. figure:: images/makelightcurve.png
   :align: center
   :alt: Make light curve
   
   The dialog for making a light curve.

.. seealso:: :ref:`make-light-curve-dialog`
   
Selecting the stars
-------------------

The next dialog shows the reference frame and allows an user
to select variable, comparison and check stars. The detected stars are
highlighted. Click on the variable star using left mouse button. A context
menu opens. Select the :guilabel:`Variable`.
The star is now drawn in red color now and the label "var" is placed
near to it. Using the same procedure, select one comparison star and one or
more check stars. I would recommend you to use two check stars. Confirm the
selection by the :guilabel:`OK` button.

.. figure:: images/stars.png
   :align: center
   :alt: Choose stars
   
   The dialog for selection of a variable star (red), a comparison star (green)
   and check stars (blue).

.. seealso:: :ref:`choose-stars-dialog`
   
   
Choosing an aperture
--------------------   

Now, we have to choose the aperture. You can image the aperture as a virtual
circular pinhole, placed on each star on a frame to measure its brightness. All
pixels that are inside the pinhole are included in computation leaving out the
background pixels. The best aperture should be big enough to include most of the
star's light, on the other hand, the bigger aperture is used the more background
is included and the more noisy the result is. Because of this, the photometry
process computes the brightness of each star in a set of predefined apertures of
radius in the range of 2 and 30 pixels.

To select the best aperture, we can take advantage of a comparison and
check stars - providing that they are constant, we can compute the differential
magnitudes between each couple of them on each other and then compute the variance
or standard deviation from the mean level. For the best aperture, the deviations
are minimal.

In the next dialog, the graph shows the standard deviation for each aperture.
Find the aperture with the minimal deviation and
click on it using the left mouse button. A context menu appears. Select the
:guilabel:`Select aperture` item. The point is drawn in red color
now and the label is placed near to it. Confirm the selection by the :guilabel:`OK`
button.

.. figure:: images/aperture.png
   :align: center
   :alt: Choose aperture
   
   The dialog for selection of an aperture.

.. seealso:: :ref:`choose-aperture-dialog`
   
   
Plotting a light curve
----------------------

Now, the program has got enough information to make a light curve.
It is presented in a new window which appears automatically.

.. figure:: images/lightcurve.png
   :align: center
   :alt: Light curve graph
   
   The dialog with a light curve graph.

.. seealso:: :ref:`light-curve-dialog`


Saving a light curve to a file
------------------------------

In the window with a light curve, click on the :menuselection:`File` --> Save` item in the local menu. A new dialog appears.
Locate the folder where you
want to save the results and fill in the name of the output file. Confirm the
dialog by the :guilabel:`Save`.

The light curve is saved to a text-based file. On its first line, the
names of the columns are stored. The second line shows the aperture and color
filter used. The data are stored on the following lines. Each line corresponds
to a single source frame. In the first column, there is a time of observation
(center of exposure) expressed as a Julian date. The second column consists
of differential instrumental brightness of the variable stars with respect
to the comparison star in magnitudes (V-C). The error estimation is stored
in the next column. Following columns consist of differential magnitudes
of comparison star and check stars and their error estimations.

It is also possible to save a light curve in other formats.

.. seealso:: :ref:`aavso-format`


.. index::
   pair: heliocentric; correction

Heliocentric correction
-----------------------

While the Earth orbits the Sun, the distance between the observed object
and the observer changes during a year. Although the amplitude of these changes
is negligible with respect to the mean distance, it becomes significant when
you are interested in a time interval between two events, like times of minimum
of a eclipsing variable star. In this case, the finite speed of light is not
unimportant. The value of the heliocentric correction is the time that the
light needs to travel from the Earth's actual position to the center of the
Earth's orbit. The value may be either positive, when the Earth is nearer
to the object than the Sun, or negative in the opposite case.

It is possible to make a light curve that includes the value of the heliocentric
correction for each measurement in a separate column. Optionally, it is also possible
to save the output table which has heliocentric Julian date in its first column.

To compute the heliocentric corrections for an existing light curve,
click on the :menuselection:`Edit --> Properties` in the local menu in the light curve window. A new dialog opens.
Check the option :guilabel:`Compute heliocentric correction`. When you
are making a new light curve, it is also possible to turn this feature on (see above). Confirm the dialog
by the :guilabel:`OK` button.

When the heliocentric correction is turned on, you have to fill the object's
celestial coordinates to the appropriate edit boxes in the dialog, the fields may
be filled automatically if the input CCD frames or a catalogue file unsed in the
matching step contain such information.

.. figure:: images/lightcurve_helcor.png
   :align: center
   :alt: Light curve properties - heliocentric correction
   
   Light curve properties dialog (heliocentric correction)

Only one value of Julian date per each row can be exported to the file.
You can choose between the geocentric and heliocentric J.D. in the "Save light curve"
dialog - there is an appropriate option in the section "Export options". However,
in both cases, the file will contain the column :guilabel:`HELCOR` with
values of the heliocentric correction for each observation.

There are other options how to obtain the object coordinates. For more options,
click on the :guilabel:`More` button. A new dialog appears. In this dialog
you can make your table of favorite objects. For known variable stars, the program
provides a tools for searching the common variable star catalogs (GCVS, NSV and NSVS).


.. index::
   pair: air mass; coefficient

Air mass coefficients
---------------------

The air mass coefficient characterizes the attenuation of the star's signal
after it has traveled through the Earth's atmosphere. While this effect is strongly
dependent on the light's wavelength (color), it must be taken account in computation
of the color index (temperature) of an observer object. The attenuation also becomes
significant when the color index of the variable and the comparison star are not close.
The air mass coefficient is always positive value, it defined as 1.0 when the object
is in the zenith and greater than one when the object is somewhere between the horizon
and the zenith. The program also presents the value of altitude of the object above
the horizon in degrees. Please note, that the value is positive to indicate altitude
above the horizon while a negative value of altitude means that the object is below
the horizon - it indicates that either times of observation, object coordinates or
observer coordinates are wrong.

Although the C-Munipack program doesn't perform any corrections to the measured
intensity. It can compute the value for coefficient for each measurement and put those
values into the output file to a separate column.

To compute the air mass coefficients for an existing light curve,
click on the :menuselection:`Edit --> Properties` in the
local menu in the light curve window. A new dialog opens. Check the option
:guilabel:`Compute air mass coefficients`. When you are making a new light
curve, it is also possible to turn this feature on (see above). Confirm the dialog
by the :guilabel:`OK` button.

When the heliocentric correction is turned on, you have to fill the object's
celestial coordinates and observer's geographic coordinates to the appropriate edit
boxes in the dialog, the fields may be filled automatically if the input CCD frames
or a catalogue file unsed in the matching step contain such information.

.. figure:: images/lightcurve_amass.png
   :align: center
   :alt: Light curve properties - air mass coefficients
   
   Light curve properties dialog (air mass coefficients)

When you save the light curve to a file, it will contain the column :guilabel:`AIRMASS`
with values of the air mass coefficient and the column :guilabel:`ALTITUDE` with
values of altitude of the object above the horizon in degrees.

There are other options how to obtain the object and observer coordinates. For more options,
click on the :guilabel:`More` button. A new dialog appears. In this dialog
you can make your table of favorite objects and locations. For known variable stars, the program
provides a tools for searching the common variable star catalogs (GCVS, NSV and NSVS).


.. index::
   triple: raw; instrumental; magnitudes
   single: flux
   single: intensity
   single: brightness

Intensities (fluxes)
--------------------

In some cases when further processing of the photometric data is involved, ensemble
photometry for example, it is neccesary to get intensity (brightness) values of 
individual stars instead of the standard differential magnitudes. Some programs call 
such value the flux. The intensity is defined as integral of the light in an aperture 
minus contribution of a local background. The intensity can be transformed into magnitudes 
using Pogson's law and a reference intensity. In the C-Munipack software, this value is 
called the raw instrumental magnitude.

The Muniwin software provides a special output mode called 'Raw instrumental magnitudes'.
If this mode is enabled, the light curve will contain a pair of values for each selected
star and frame. The first value in pair is the intensity of a star expressed in magnitudes
and the second value is its error estimation. The reference intensity was set to 10^10. 
Please look into the separate document 'Theory of operation' section 'Aperture photometry' 
on the conversion between the raw instrumental magnitude and the intensity.

To show the raw instrumental magnitudes for an existing light curve, click on the 
:menuselection:`Edit --> Properties` in the local menu in the light curve window. A new 
dialog opens. Check the option :guilabel:`Show raw instrumental magnitudes`. Confirm the 
dialog by the :guilabel:`OK` button. When you are making a new light curve, it is also 
possible to turn this feature on (see above) in the :guilabel:`Make light curve` dialog.


.. index::
   pair: outliers; trimming

Trimming outliers from the curve
--------------------------------

Is is possible to manually trim outlying observations from the curve.
You can select an individual point by a right click, you can also
select more than one point by pressing a Shift key and left mouse button
and drawing a rectangle in the graph. Then, click the right mouse button
on a point in the selection to open the context menu. There are two options:
When you select the option :guilabel:`delete from data set`,
the selected measurements are removed from the current light curve, the
data will be shown when you make another light curve or :guilabel:`Rebuild`
the actual curve. The other option :guilabel:`remove from project`
means that the frames corresponding to the selected measurements are removed
permanently from the list of input files (see: Main application window) and
such measurements won't be included in any other output. It is not allowed
to remove a reference frame.
