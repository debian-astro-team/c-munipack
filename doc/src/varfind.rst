.. C-Munipack - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: varfind.rst,v 1.2 2015/07/12 07:44:57 dmotl Exp $

.. _varfind: 
   
.. index::
   triple: finding; variable; star

Finding variables
=================

The *Find variables* tool is based on the dependency of noise level to mean
brightness. The fainter an objects is, the smaller is the signal-to-noise ratio
and the more noisy is the measurement of the object. It can be shown
that two objects of similar brightness which do not vary in brightness 
during the observation also have similar noise levels, which is computed as
the sample standard deviation. If one of the objects changes its brightness
due to reasons other than measurement noise, for example it is an eclipsing binary,
its sample standard error is greater than a constant object of the same mean 
brightness. Using this idea, we can make a graph of sample standard deviation
vs. mean brightness of all objects detected on a frame set. Supposing the majority of objects are constant,
the graph will reveal a curve, which grows towards lower brightness. Any outlying
point located above the curve denotes an object that has brightness variations
other than measurement noise and thus it might be a variable star.

The *Find variables* tool does not find variable stars automatically. It presents
the graph described above and provides a means of selecting any object on the graph 
and showing its light curve. The decision whether an object is a variable or not is 
left to the user.

.. index::
   pair: mag-dev; curve

.. rubric:: Making the mag-dev graph

A list of source frames is analyzed and raw instrumental magnitudes stored in memory. 
Each detected object must be uniquely identified across all the frames. One
star is chosen as the comparison star and we assume that this star was constant during the
observation.

However, an object might not be measured correctly on all frames in a frame set. 
If the comparison star has an invalid measurement on one or more frames, we cannot derive a differential 
magnitude for any object on those frames, so we have to reject these frames as whole. For the rest 
of the frames, it may happen that an object does not have a valid measurement on one or more frames. 
In this case we cannot derive a differential magnitude of that particular object on those frames.

Let us denote *N* as the number of source frames and *N_C* as the number of frames where 
a comparison star has valid measurements. For each object *i* we can derive a 
number of frames *N_i* where both the object and the comparison star has valid 
measurements. If *N_i* is less than a minimum number of frames :math:`N_{min}`, we will
rule out the object from the output. The minimum number of frames :math:`N_{min}` is calculated 
from *N_C* and configurable threshold *t* in units of percent.

.. math::
   :label: n_min

	N_{min} = \lfloor{\frac{t}{100}\,N_C \rfloor}

For each object *i* that has a valid measurement on frame *j*, we compute the differential
magnitude :math:`m_{i,j}`:

.. math::
   :label: m_ij

	m_{i,j} = mag_{i,j} - mag_{C,j}

where :math:`mag_{i,j}` is instrumental magnitude of the object *i* and :math:`mag_{C,j}` is instrumental 
magnitude of the comparison star *C* on frame *j*. Then, the mean value :math:`\overline{m_i}` 
is computed by means of the robust mean algorithm and the sample standard variance :math:`s_i^2`
is also determined. Please note, that although the robust mean algorithm is used to compute mean 
brightness of an object, all valid measurements should be taken into account in the 
computation of the sample variance. Otherwise for a variable star, some of its valid measurements 
would be unjustly rejected as outliers, resulting in lower variance.   
The term :math:`\frac{N_i}{N_i-1}` is known as Bessel's correction.
                                                                   
.. math::
   :label: s_i_2
                      
	s_i^2 = \frac{N_i}{N_i-1}\,\frac{1}{N_i}\,\sum_{j=1}^{N_i}(m_{i,j} - \overline{m_i})^2
	= \frac{1}{N_i-1}\,\sum_{j=1}^{N_i}(m_{i,j} - \overline{m_i})^2  

Figure :ref:`1 <fig1>` shows a scatter graph of stars that were measured
in the field of eclipsing binary CR Cassiopeiae showing the relationship between
the stars' mean magnitude *x_i* versus their sample standard deviations *s_i*.

.. _fig1:

.. figure:: images/varfind_graph.png
   :align: center
   :alt: Mag-dev graph
   
   Mag-dev graph of stars in the field of eclipsing binary CR Cas. The majority
   of stars are constant forming a typical "background" curve. The variable star CR Cas, 
   labeled *var*, has greater standard deviation than stars of similar mean magnitude.
  
.. rubric:: Choosing a comparison star

If the user does not pick a comparison star, the program uses the following algorithm
to choose one and uses it to make a mag-dev curve. There are several criteria that
the program can use to determine the suitability of an object to be a comparison star.
A good comparison star should be 1) on the greatest number of frames to avoid more 
data rejection than necessary, 2) it should not vary in brightness and 3) the noise 
of its measurements should be low to reduce a noise of differential magnitudes and 
the resulting data.

Let us take all objects that were found on a reference frame. We assign a number of
frames *F_i* which each object $i$ has been successfully identified on. The maximum 
of values *F* is determined and all objects that have the maximum value of *F* are 
kept on the list, the others are removed.

For each :math:`\frac{N(N-1)}{2}` pairs of objects that were left on the list, a differential 
light curve is derived and a robust mean and a sample standard deviation is computed
for the light curve data. Finally, the star that has the lowest sum of standard deviations 
is picked as the comparison star. 

Please note, that although the robust mean algorithm is used to compute mean brightness, 
all valid measurements should be taken into account in the computation of the sample 
standard deviation. If there were a variable star on the list of candidates, some of 
its valid measurements would be unjustly rejected as outliers, resulting in lower deviation.   
