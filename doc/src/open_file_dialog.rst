.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: open_file_dialog.rst,v 1.1 2015/07/06 08:19:49 dmotl Exp $

.. index::
   pair: Open file; dialog
   
.. _open-file-dialog:

Open file (dialog)
==================

The "Open file" dialog is used to show content of an external file. A file is displayed
in a separate preview window.


Activating the dialog
---------------------

The dialog can be activated:

#. from the main menu: :menuselection:`Tools --> Open file`.


File browsing
-------------

.. figure:: images/openfile_couts.png
   :align: center
   :alt: Open file dialog
   
   The dialog for browsing files
   
The button "Type a file name" (1) shows and hides the "Location" text box. The keyboard shortcut
Ctrl+L key combination does the same action.

In the "Location" text box (2) you can type a path to a file. If you don't type any path, the name
of the selected file will be displayed. You can also type the first letters of the name: it will be
auto-completed and a list of file names beginning with these letters will be displayed.

The path to the current folder is displayed at the top of the dialog (3). You can navigate along
this path by clicking on an element.

\(4) Here, you can access to your main folders and to your store devices.

\(5) Here, you can add bookmarks to folders, by using the "Add" or the "Add to Bookmarks" option you
get by right-clicking a folder in the central panel, and also remove them.

The contents of the selected folder is displayed here (6). Change your current folder by double left
clicking on a folder in this panel. Select a file with a single left click. Right-clicking a folder name
opens a context menu.

If the selected file is a file recognized by the C-Munipack, the preview and short info is displayed
in the right part of the dialog (7). Double click on the preview to show a larger preview in a separate
dialog.

\(8) By clicking the Add button, you add the selected folder to bookmarks. By clicking the Remove,
you remove the selected bookmark from the list.

Click the "Open" button (9) to open a file in a separate window.


.. seealso:: :ref:`image-file-window`, :ref:`photometry-file-window`, :ref:`catalog-file-window`, :ref:`graph-window`
