.. C-Munipack - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: muniwin_reference.rst,v 1.1 2015/07/12 07:44:57 dmotl Exp $
 
.. _muniwin-reference:   
   
Muniwin reference manual
========================

.. toctree::
   :maxdepth: 1

   main_menu
   main_toolbar
   dialogs
