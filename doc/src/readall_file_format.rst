.. C-Munipack - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: readall_file_format.rst,v 1.1 2015/07/12 07:44:57 dmotl Exp $

.. index::
   pair: Readall file; format
      
.. _readall-file-format:

Readall file format
===================

This 'Read all' file is used to export the measurement data of all objects and a set of
frames to one file. Its name originates from the old 'readall' utility. In the
C-Munipack software such file can be created by the 'Find variables' tool and
this tool can also import data from such file.

The readall file is a text file. Lines are separated by ``CR+LF``, ``CR``
or ``LF`` character. The writer can use any line separator, the reader should
correctly handle any of these. Fields are separated by a space character, the reader
should ignore any additional space characters between fields, at the beginning of
a file or between the last field and following line separator.

The first line is used to identify the file. It should contain the following text:

``# JD, instrumental mags and standard deviations of all detected stars}``

The seconds line starts with a hash character and it is followed by an aperture identifier
and color filter name. Third and following lines consists of measurement data.

The first field of each line is a Julian date of observation. The following fields 
contain raw instrumental brightness of objects followed by an error estimation.
The data are organized in such a way that one object occupy always the same fields
on each line. Invalid magnitudes are represented as values '99.99999' and invalid
error estimations are represented as values '9.99999'. 

The length of a line is not limited. Because each line carries measurement data for 
a entire frame, the reader must be designed to correctly handle very long lines.
