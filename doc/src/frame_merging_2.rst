.. C-Munipack - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: frame_merging_2.rst,v 1.1 2015/07/06 08:19:48 dmotl Exp $

.. _frame-merging-2: 
   
.. index::
   pair: frame; merging

Frame merging
=============

Frame merging is a technique which uses averaging to combine multiple CCD frames 
into one frame. In a naive implementation the mean value of corresponding pixels of source
frames could be computed. However it wouldn't work well in practice since the sky is revolving around the 
celestial pole throughout the night and in most cases even perfectly adjusted motorized 
compensation does not track the stars with sufficient precision. 

Therefore, in tasks where frame merging is performed, the reduction is done in two phases. 
First, the original frames are reduced up to the matching step to get the information about their 
relative offsets. Then, the original frames are aligned to one reference frame by means of the 
shift transformation. The frames are divided into groups, and for each group one combined
frame is made. In this way we get a set of combined frames, which are processed in the second 
phase. Unlike the first phase, calibration is not applied to the combined frames since the images
were already calibrated in the first phase.
