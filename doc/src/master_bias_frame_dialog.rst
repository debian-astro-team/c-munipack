.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: master_bias_frame_dialog.rst,v 1.1 2015/07/06 08:19:49 dmotl Exp $

.. index::
   pair: Master bias frame; dialog
   
.. _master-bias-frame-dialog:

Master bias frame (dialog)
==========================

The "Master bias frame" dialog is used to enter a name and a directory for a new master bias
frame. You can find more information about bias frames in the chapter "Advanced calibration scheme".


Activating the dialog
---------------------

The dialog can be activated:

.. |masterbias_icon| image:: icons/masterbias.png

#. from the main menu: :menuselection:`Make --> Master bias frame`.

#. from the main toolbar: |masterbias_icon|


The basic dialog
----------------

.. figure:: images/masterbias_couts1.png
   :align: center
   :alt: Make master bias frame
   
   The "Make master bias frame" dialog (basic form)

In its basic form, as shown above, the dialog consists of a text box (1) to assign a name
to the file, and a drop-down list of bookmarks (2) to select a directory to save it in.

If the directory you want is not in the list of bookmarks, click on "Browse for other
folders" button (3) to expand the dialog to its full form.

\(4) It is possible to include all source files in the project or the files that are
currently selected in the table of input files. By means of this option, it is possible to
make a master frame using a subset of source files only.


Browsing the directories
------------------------

.. figure:: images/masterbias_couts2.png
   :align: center
   :alt: Make master bias frame
   
   The "Make master bias frame" dialog (with browser)

\(11) Here, you can access to your main folders and to your store devices.

\(12) The middle panel displays a list of the files in the current directory. Change your
current directory by double left-clicking on a directory in this panel. Select a file with
a single left click. You can then replace the file you have selected by clicking on the
Save button. Note that a double left click start the operation.

\(13) Above the middle panel, the path of the current directory is displayed. You can navigate
along this path by clicking on one of the buttons.

You can right click on the middle panel to access the "Show Hidden Files" command.

\(14) Enter the file name of the new image file here.

\(15) This drop-down list is only available in the basic form of the dialog. It provides
a list of bookmarks for selecting a directory in which to save your file. click on "Browse for
other folders" button (16) to shrink the dialog to its basic form.

\(17) Here, you can add bookmarks to folders, by using the "Add" or the "Add to Bookmarks"
option you get by right-clicking a folder in the central panel, and also remove them.

\(18) By clicking the Add button, you add the selected folder to bookmarks. By clicking the Remove,
you remove the selected bookmark from the list.

If you want to save the image into a folder that doesn't yet exist, you can create it
by clicking on "Create Folder" button (19) and following the instructions.

\(20) It is possible to include all source files in the project or the files that are
currently selected in the table of input files. By means of this option, it is possible to
make a master frame using a subset of source files only.

.. index::
   pair: Master bias frame; making

Making the master bias frame
----------------------------

Click on the "Save" button (5 or 21) to start the operation.

- A working copy of source frame must be made before making a master bias frame.

- Do not apply any calibration (bias, dark or flat corrections) to source frames.

- Bias correction frame are used in the "Advanced calibration scheme" only.


.. seealso:: :ref:`bias-correction-dialog`, :ref:`advanced-calibration-scheme`
