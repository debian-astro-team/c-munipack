.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: processing_raw_images.rst,v 1.2 2016/02/27 09:14:01 dmotl Exp $

.. index::
   simple: DSLR images
   simple: RAW images

.. _processing_raw_images:

Photometry using a DLSR camera
==============================

The C-Munipack software can decode RAW images produced by selected DSLR cameras. Because the DSLR
cameras have not been designed to be used for the photometry, there are several potential pitfals 
that you may stumble upon. As in any other scientific work, it is recommended to be careful and
to check if your results are correct before using or publishing them. In case of the 'raw' images,
you should know, that the available documents on the 'raw' format are imcomplete, chaotic and 
often inconsistent.


Color components
----------------

The most common mistake is that the color components are not in correct order. Almost all commercial
DSLR cameras have a single array of light sensitive elements. Before the array, there is a color filter 
mosaic, most of them is a `Bayer filter <http://en.wikipedia.org/wiki/Bayer_filter>`. Color components 
are distributed in a repeating pattern::

  R  G  R  G  R  G  ...
  G  B  G  B  G  B  ...
  R  G  R  G  R  G  ...
  ...
  
The problem is, that in the first cell of the first row, some models have red (R) component and others
starts with the blue (B) or green (G) component.

To check if the software decodes the components correctly I recommend to perform an easy test: take 
an image of any colorful object or objects that contain easily distinguished pathes of three basic
colors (red, green and blue) on a black background. Save this image in the 'raw' format and load them 
into the C-Munipack software and perform `Fetch/convert` reduction steps with different selection of 
the color channel each time. After each conversion, show the resulting frame and check if the patches
of the selected color component are bright and patches of other colors are dark.

.. seealso::

   Bayer filter
     see http://en.wikipedia.org/wiki/Bayer_filter
    
   RGB color components
     see http://en.wikipedia.org/wiki/Additive_color
     
   RAW format
     see http://en.wikipedia.org/wiki/RAW_format
