.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: main_toolbar.rst,v 1.1 2015/07/06 08:19:49 dmotl Exp $

.. index::
   pair: main; toolbar

.. _main-toolbar:

Main toolbar
============

Here is the list of all items that can be displayed in the main toolbar. Please note, that some items
may be hidden according to the actual project type. See :ref:`projects` topic for details.

.. |newproject_icon| image:: icons/newproject.png

- |newproject_icon| - start a new project; see :ref:`new-project-dialog`.

.. |openproject_icon| image:: icons/openproject.png

- |openproject_icon| - open an existing project; see :ref:`open-project-dialog`.

.. |preferences_icon| image:: icons/preferences.png

- |preferences_icon| - change project settings - name, type, processing parameters, etc.; see :ref:`project-settings-dialog`.

.. |addfiles_icon| image:: icons/addfiles.png

- |addfiles_icon| - add one or more frames from a folder; see :ref:`add-individual-frames-dialog`.

.. |addfolder_icon| image:: icons/addfolder.png

- |addfolder_icon| - add frames from a folder and its subfolders; see :ref:`add-frames-from-folder-dialog`.

.. |removefiles_icon| image:: icons/removefiles.png

- |removefiles_icon| - remove selected frames from the project.

.. |preview_icon| image:: icons/preview.png

- |preview_icon| - open selected frame in a preview window; see :ref:`frame-preview-window`.

.. |reduction_icon| image:: icons/reduction.png

- |reduction_icon| - conversion, calibration, photometry and matching done at in a batch; see :ref:`express-reduction-dialog`.

.. |newfiles_icon| image:: icons/newfiles.png

- |newfiles_icon| - process frames that were appended to the project, apply the same rules 
  as to the frames that had been already processed; watch periodically for new frames in a folder, append them to 
  the project and process them; see :ref:`process-new-frames-dialog`.

.. |convert_icon| image:: icons/convert.png
  
- |convert_icon| - make working copy of the source frames, convert them if necessary; see :ref:`fetch-convert-files-dialog`.

.. |biascorr_icon| image:: icons/biascorr.png

- |biascorr_icon| - apply bias correction; see :ref:`bias-correction-dialog`.

.. |darkcorr_icon| image:: icons/darkcorr.png

- |darkcorr_icon| - apply dark correction; see :ref:`dark-correction-dialog`.

.. |flatcorr_icon| image:: icons/flatcorr.png

- |flatcorr_icon| - apply flat correction; see :ref:`flat-correction-dialog`.

.. |photometry_icon| image:: icons/photometry.png

- |photometry_icon| - run photometry; see :ref:`photometry-dialog`.

.. |matchstars_icon| image:: icons/matchstars.png

- |matchstars_icon| - find cross-references between stars on the frames; see :ref:`match-stars-dialog`.

.. |lightcurve_icon| image:: icons/lightcurve.png

- |lightcurve_icon| - make light curve (magnitude vs. time) for a variable; see :ref:`make-light-curve-dialog`.

.. |masterbias_icon| image:: icons/masterbias.png

- |masterbias_icon| - make master bias frame; see :ref:`master-bias-frame-dialog`.

.. |masterdark_icon| image:: icons/masterdark.png

- |masterdark_icon| - make master dark frame; see :ref:`master-dark-frame-dialog`.

.. |masterflat_icon| image:: icons/masterflat.png
 
- |masterflat_icon| - make master flat frame; see :ref:`master-flat-frame-dialog`.

.. |merge_icon| image:: icons/merge.png
  
- |merge_icon| - merge frames to make combined frames; see :ref:`merge-frames-dialog`.

.. |varfind_icon| image:: icons/varfind.png

- |varfind_icon| - semi-automatic search for variable stars; see :ref:`make-find-variables-dialog`.

.. |catalogfile_icon| image:: icons/catalogfile.png

- |catalogfile_icon| - make a catalog file (a reference frame for the field); see :ref:`make-catalog-file-dialog`.

.. |iconview_icon| image:: icons/iconview.png

- |iconview_icon| - show small previews of all frames in the projects; see :ref:`thumbnails-dialog`.

.. seealso:: :ref:`main-window`, :ref:`main-menu`
