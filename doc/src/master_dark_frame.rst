.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: master_dark_frame.rst,v 1.2 2016/02/27 09:14:01 dmotl Exp $

.. index::
   single: Master dark frame

.. _master-dark-frame:

Master-dark frame
=================

The master-dark frame is an image made of several dark images. The dark
image is used in a process of CCD image calibration. To reduce a noise of a
result, it is advisory to make a number of raw dark frames and combine them
into one correction frame. The Master-dark tool in the Muniwin program provides
a simple method how to do that.

Creating a new project
----------------------

First of all, we will create a new :ref:`project <projects>`. To begin with processing of a variable star observation, 
we create a new project. To do so, open the :guilabel:`Project` menu and activate the :guilabel:`New` item. A new dialog 
appears.

.. figure:: images/newproject_mdark.png
   :align: center
   :alt: "New project" dialog
   
   The dialog for making a new project.
   
Fill in a name that will be assigned to the project (1). Because the name of the file that keeps track of the data related
to the project, the project file, is derived from the name, some characters cannot appear in the project name, do not 
use: / \ ? % * : | " < and >.

The field :guilabel:`Location` (2) shows a path to the directory where a new project will be created. Edit the path to 
change the location, you can also click the :guilabel:`Browse` button to select a directory in a separate dialog.

Select a profile (3) to specify an intial set of configuration parameters into a new project. When you confirm the dialog, 
you should be in the main window again now. The table of input files shown there is empty.

.. figure:: images/main.png
   :align: center
   :alt: Main application window
   
   The main application window with the table of input files, now empty.

.. seealso:: :ref:`new-project-dialog` and :ref:`main-window`

Input files
-----------

Now, we are going to tell the program which files we are going to work on. These files are called the input 
files. Their list is displayed in the table in the main application window. When the application is closed,
the list of files are saved to the disk and it is restored back when the program is launched again.

Supposing that the table now consists of files from your previous task, let's get rid of them. Please, use 
:menuselection:`Files --> Clear files` to start a new task instead of just removing the files from 
the table. Besides the clearing the table of input files, this function resets all internal variables, too.

Now, we need to populate the table with the CCD frames we're going to reduce. There are two methods how to 
achieve that - adding a individual files or adding all files from a folder. Which way is the best for you 
depends on organization of your observations on the disk. I'd suggest you to make a folder for each year, 
a folder for each night in it, the a subfolder for a name of object or another view field identification and 
finally a subfolder named upon the color filter (if you use more of them). In this case, the "Add frames from a folder" 
method is more convenient.

Click on :menuselection:`Files --> Add frames from folder` in the main menu. A new dialog appears. In the dialog, 
find a folder where the inputs files are stored in. Click on an entry in the :guilabel:`Places` pane to go to one 
of a preselected folders, double click in the middle pane enters the folder. The buttons in the upper part 
of the dialog shows your current position in the directory tree, you can use them to go to one of the parent
folders. Enter the folder with the input files - you should see them in the middle pane. Then, click on the 
:guilabel:`Add` button to add files to the table of input files. The program shows the number of added files in
the separate dialog. The :guilabel:`Add frames from folder` dialog is not closed automatically and allows a user 
to continue. Click on the :guilabel:`OK` button to close the dialog and return to the main window.

.. figure:: images/addfolder.png
   :align: center
   :alt: "Add folder" dialog
   
   The "Add folder" dialog with the place selection box (left), the file
   selection box (middle) and the preview panel (right).

If you want to reduce only a subset of files from a folder, click on :menuselection:`Files --> Add individual frames` 
in the main menu. A new dialog appears, similar to the previous one. In the dialog, find a folder where the inputs 
files are stored in. Click on an entry in the :guilabel:`Places` pane to go to one of a preselected folders, double
click in the middle pane enters the folder. The buttons in the upper part of the dialog shows your current position 
in the directory tree, you can use them to go to one of the parent folders. In the middle pane, select the files 
using the :kbd:`Ctrl` modifier to include and exclude a single file and the :kbd:`Shift` modifier to include a range 
of files. Then, click on the :guilabel:`Add` button to add selected files to the table of input files. The program 
shows the number of added files in the separate dialog. The :guilabel:`Add individual frames` dialog is not closed 
automatically and  allows a user to continue. Click on the :guilabel:`OK` button to close the dialog and return to 
the main window.

.. figure:: images/addfiles.png
   :align: center
   :alt: "Add files" dialog
   
   The "Add files" dialog with the place selection box (left), the file
   selection box (middle) and the preview panel (right).

.. seealso:: :ref:`add-frames-from-folder-dialog` and :ref:`add-individual-frames-dialog`
   
   
Frame reduction
---------------

The next step is called the reduction process, even though there is no real "reduction" of the data. This is to
keep the general scheme of data processing the same for all project types. In the standard calibration scheme,
it is necessary to perform just one step - conversion of the source CCD frames into a working format, no calibration
or photometry is required. 

We will use the same :guilabel:`Express reduction` dialog as in the section :ref:`light-curve`. Using the menu, 
activate the :menuselection:`Reduce --> Express reduction` item. A new dialog appears. Check the option 
:guilabel:`Fetch/convert files` and let other options unchecked.

.. figure:: images/express_convert.png
   :alt: "Express reduction" dialog
   :align: center

.. seealso:: :ref:`express-reduction-dialog`
   
   
Making master dark frame
------------------------

Open the menu :guilabel:`Make` and select the item :guilabel:`Master-dark frame`. A new dialog appears. In the dialog, 
specify whether all files shall be processed or only the selected ones. Confirm the dialog. In the following dialog 
select a directory and enter the name of output file where a new master-dark frame shall be saved to. Press the 
:guilabel:`Save` button.

When a task is finished, a new preview window appears. A new master-dark frame is displayed in a separate window.

.. seealso:: :ref:`master-dark-frame-dialog`