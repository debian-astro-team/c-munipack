.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: save_profile_dialog.rst,v 1.1 2015/07/06 08:19:49 dmotl Exp $

.. index::
   pair: Save project settings; dialog
   pair: project settings; save
   
.. _save-profile-dialog:

Save project settings (dialog)
==============================

The dialog is used to save settings from the current profile as a user-defined profile.
The profile is used to define an initial settings for a new project, see :ref:`profiles` 
for more details.


Activating the dialog
---------------------

The dialog can be opened from the :ref:`project-settings-dialog`, the root page, using 
the :guilabel:`Save as profile` button.


The dialog controls
-------------------

.. figure:: images/saveprofile_couts.png
   :align: center
   :alt: Save project settings dialog
   
   Save project settings dialog

\(1) If you want to create a new user-defined profile, enter its name to the edit field.

\(2) To update an existing profile, select it in the table.

\(3) Click the :guilabel:`Edit profiles` button to open the :ref:`edit-profiles-dialog`.

Confirm the dialog using the button (4) to continue.


.. seealso:: :ref:`project-settings-dialog`, :ref:`edit-profiles-dialog`
