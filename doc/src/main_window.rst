.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: main_window.rst,v 1.1 2015/07/06 08:19:49 dmotl Exp $

.. index::
   pair: main; window
   triple: main; application; window

.. _main-window:

Main window
===========

The "Muniwin" window is a main application window. The list of source files is presented in the table.
The :ref:`main-menu` a :ref:`main-toolbar` allow an user to access all features of the program.

Browsing the source files
-------------------------

.. figure:: images/mainwindow_couts.png
   :align: center
   :alt: Main window
   
   Main window
   
\(1) The list of source frames are presented in the table. For each frame,
the icon on the left indicates its status. Next to the icon, there is the ordinal
number assigned to a frame. Most of the fields can be turned on and off in the
"Preferences" dialog.

- Frame # - assigned ordinal number of the frame

- Date and time - Gregorian calender date and time of the center of exposure

- Julian date - Julian date of the center of exposure

- Exposure - exposure duration in seconds

- Temperature - CCD temperature in centigrade degrees

- Filter - name of the color filter

- Stars - after the photometry, the number of detected stars is displayed here. After the
  matching, two numbers are shown. The first value is a number of matched stars, the
  second value is a number of detected stars.
  
- Original file - full path to the original image file.

- Temporary file - name of the temporary image file.

- Status - the result of the last operation is displayed

Double left-click on an item to open a preview to the selected frame in a separate window.
Right-clicking a folder name opens a context menu.


.. seealso:: :ref:`add-individual-frames-dialog`, :ref:`add-frames-from-folder-dialog`, :ref:`frame-preview-window`,
   :ref:`main-menu`, :ref:`main-toolbar`
