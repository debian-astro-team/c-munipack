.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: edit_profiles_dialog.rst,v 1.3 2016/02/27 09:14:01 dmotl Exp $

.. index::
   pair: Edit profiles; dialog
   
.. _edit-profiles-dialog:

Edit profiles (dialog)
=========================

The "Project settings" dialog is used to set up the parameters of
the data processing and output. User interface options that do not affect
the data processing are places in the separate dialog "Environment options".

Among many parameters of the aperture photometry procedure a special attention
should be payed to the following: Filter width and the Detection threshold. These parameters
control mainly the detection of stars on the CCD frames. By decreasing the Filter width value
the fainter stars will be found. It is to be pointed out, that too small value may take the
artefacts on the background as regular stars. The Detection threshold value sets the distance
between the finest stars detected and the background sky noise. The value is entered in
multiplies of background standard deviations.

The description of the individual parameters can be found in :ref:`this chaper <project-settings>`.


Activating the dialog
---------------------

The dialog can be activated:

.. |preferences_icon| image:: icons/preferences.png

#. from the main menu: :menuselection:`Project --> Edit properties`.

#. from the main toolbar: |preferences_icon|


The dialog controls
-------------------

.. figure:: images/editproject_couts.png
   :align: center
   :alt: Project options dialog
   
   The "Project options" dialog
   
\(1) Select a category.

Parameters in the selected category are shown on the right pane (2).

Click "Set defaults" button (3) to set the parameters in the selected
category to the default values.

Click the button (4) to save the settings as current configuration.


Configuration profiles
----------------------

If you process the data from multiple cameras or telescopes,
you will need to adjust the project options for each setup. The program
allows to store the configuration to a file and then load it back.

To save the current settings to a file, click on the "Save to profile"
button. A standard save dialog appears. Enter a name of the file, you can
also change the target direcory. Confirm the dialog.

To restore the settings from a file, click on the "Load from profile"
button. A standard open dialog appears. Look up the file with saved project
options and confirm the dialog. Please note, that your current project
settings will be overwritten by this action.
  
.. seealso:: :ref:`projects`, :ref:`profiles`, :ref:`project-settings`, :ref:`environment-options-dialog`
