.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: express_reduction_dialog.rst,v 1.2 2016/02/27 09:14:01 dmotl Exp $

.. index::
   pair: Express reduction; dialog

.. _express-reduction-dialog:

Express reduction (dialog)
==========================

The "Express reduction" dialog provides a simple way how to go through the whole
reduction process in one step. Using the standard way, the user has to proceed the reduction
step by step manually. The asks an user for all necessary information first and then
controls the reduction automatically.


Activating the dialog
---------------------

The dialog can be activated:

.. |reduction_icon| image:: icons/reduction.png

#. from the main menu: :menuselection:`Reduce --> Express reduction`.

#. from the main toolbar: |reduction_icon|


.. index::
   pair: express; reduction

   
Setting up the reduction process
--------------------------------

.. figure:: images/express_couts1.png
   :align: center
   :alt: Express reduction
   
   The "Express reduction" dialog
   
  
\(1) Each check box on the left side of the dialog corresponds to one reduction step.
Check the option to perform the corresponding operation. For example, to start the reduction
with a fresh copy of the source frames, check the "Fetch/convert files" option.

.. note: DSLR camera - color photometry
   Be careful when you are using color components from the RAW images - see :ref:`processing_raw_images`.
   
If the time correction is turned on, the actual time shift is displayed in the text box (2).
Click on the ellipsis button (3) to open a new dialog for specifying the time shift.

For the bias, dark and flat corrections, the path and name of the correction frame
is displayed below the check box (4). You can use the ellipsis button (5) to open a new dialog
that allows you to browse the files and folders.

If the matching is turned on, you can choose as a reference either one frame from a
set of source frames (6) or a catalog frame (7). The matching step cannot be used for moving target, in this 
case use the Express reduction dialog to do the processing up to the photometry and run the matching tool
manually.

Click on the "Execute" button (8) to start the reduction.

It is possible to apply the operations on all source files in the project or the files 
that are currently selected in the table of input files. Please note, that the matching step
cannot be applied on a subset of frames - that's why this option is not available when
the operation shall be performed only to selected frames.

.. seealso:: :ref:`fetch-convert-files-dialog`, :ref:`time-correction-dialog`, :ref:`bias-correction-dialog`, 
   :ref:`dark-correction-dialog`, :ref:`flat-correction-dialog`, :ref:`photometry-dialog`, :ref:`match-stars-dialog`
