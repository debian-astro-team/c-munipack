.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: plot_object_properties_dialog.rst,v 1.1 2016/01/01 09:39:22 dmotl Exp $

.. index::
   pair: Plot object properties; dialog
   
.. _make-object-properties-dialog:

Plot object properties (dialog)
===============================

The "Plot object properties" dialog is used to set up the initial parameters
for a new plot of object properties. 


Activating the dialog
---------------------

The dialog can be activated:

#. from the main menu: :menuselection:`Plot --> Object properties`.

.. index::
   pair: object properties; plot

Making a plot of object properties
----------------------------------

.. figure:: images/plotobjectproperties_couts.png
   :align: center
   :alt: Plot object properties (dialog)
   
   Plot object properties (dialog)
   

\(1) It is possible to include all source files in the project or the files that are
currently selected in the table of input files. 

Click the button (2) to proceed.


.. seealso:: :ref:`object-properties-dialog`
