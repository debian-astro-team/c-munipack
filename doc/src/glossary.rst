.. C-Munipack - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: glossary.rst,v 1.2 2016/01/01 09:39:22 dmotl Exp $

   
Glossary
========

.. glossary::
   :sorted:
   
   Flexible Image Transport System
   FITS
		
  			The Flexible Image Transport System is a file format designed specifically 
  			to store scientific images and data. Since its first publication in 1981 in has 
  			become the most frequently used file format for storing and manipulating of images 
  			that were carried out by means of the astronomy CCD cameras.
  			
  			In the C-Munipack project, the FITS format accepts the FITS images on
  			its input and it uses the format to store the results of operation that produce
  			a CCD image as an output.

   Bias frame
		
  			A bias frame is an image obtained from the CCD camera when all 
  			light is blocked and the exposure is indefinitely short (in ideal case). 
  			It represents the constant bias level, that is preset in the camera's
  			electronics that reads out the image data. For ideal CCD camera, the
  			level should be constant for pixels and the bias could be represented
  			by a single value. In practice, there are small differences between 
  			pixels.
  			
  			In the standard calibration scheme, the bias is included in
  			the dark frame and thus it is subtracted during the dark-frame correction.
  			In the advanced calibration scheme, the bias must be subtracted
  			from a scientific frame before the dark-frame calibration is applied.
  			
  			The bias frame is used in the bias-frame correction phase of the
  			reduction process. To reduce noise, a master bias frame is often used
  			instead of a raw bias frame.
	
   Master bias frame
		
  			A master bias frame is a combined CCD frame made from a set of bias 
  			frames. By means of the robust mean algorithm, the noise of the resulting
  			image is reduced.
  			
  			The master bias frame is used in the bias-frame correction phase of the
  			reduction process.
	
   Bias-frame correction
		
  			The bias-frame correction subtracts the bias component from a CCD frame.
  			This step is a part of the reduction process of CCD frames. It is used only in
  			the advanced calibration scheme.
  			
  			To apply the correction to a set of frames, a bias frame is required.
	
   Dark frame
		
  			A dark frame is an image obtained from the CCD camera when all 
  			light is blocked. The image represents the thermal current accumulated
  			in a CCD device during an exposure. The mean value of the thermal current 
  			is proportional to the CCD temperature and exposure duration. For an ideal 
  			CCD camera, the thermal current would be the same for all pixels and 
  			it could be represented by a single value. In practice, some of the CCD 
  			elements are much more sensitive to the thermal current than the rest.
  			These pixels, which appear as a bright dots on a dark frame, are called
  			hot pixels. Depending on the calibration scheme, the dark frame may also
  			contain the bias level.
  			
  			The dark frame is used in the dark-frame correction phase of the
  			reduction process. To reduce noise, a master dark frame is often used
  			instead of a raw dark frame.
	
   Scalable dark frame
		
  			A scalable dark frame is a special case of the dark frame. It is
  			used in the advanced calibration scheme that allows to use a dark frame
  			of exposure duration different from scientific images for the dark-frame
  			correction.
  			
  			The scalable dark frame can be made from a dark frame by subtracting
  			a bias frame. Thus, the scalable dark frame contains only components that 
  			are linearly dependent on exposure duration.
  			
   Master dark frame
		
  			A master dark frame is a combined CCD frame made from a set of dark
  			frames. By means of the robust mean algorithm, the noise of the resulting
  			image is reduced.
  			
  			The master dark frame is used in the dark-frame correction phase of the
  			reduction process.
	
   Dark-frame correction
		
  			The dark-frame correction subtracts the thermal component from a CCD frame.
  			This step is a part of the reduction process of CCD frames. In the standard
  			calibration scheme, this bias components is subtracted as well, because the bias
  			is included in the dark frame.
  			To apply the correction to a set of frames, a dark frame is required.
	
   Flat frame
		
  			A flat frame is an image obtained from the CCD camera when a telescope
  			is pointed towards a luminous area of uniformly distributed intensity across the
  			field of view. The image represents the spatial distribution of the sensitivity 
  			not only of the CCD elements but whole device. It records the response of the 
  			entire optical system - the telescope, filters, camera's window, cover glass
  			and CCD chip itself - to a uniform source of light. Most significant effects that
  			affect a flat frame are vignetting and sensitivity variations of CCD elements.
  			
  			The flat frame is used in the flat-frame correction phase of the
  			reduction process. To reduce noise, a master flat frame is often used
  			instead of a raw flat frame.
	
   Master flat frame
		
  			A master flat frame is a combined CCD frame made from a set of flat
  			frames. By means of the robust mean algorithm, the noise of the resulting
  			image is reduced.
  			
  			The master flat frame is used in the flat-frame correction phase of the
  			reduction process.
	
   Flat-frame correction
		
  			The flat-frame correction equalizes sensitivity variations of the whole
  			optical system (telescope + filter + camera). This step is a part of the reduction 
  			process of CCD frames.
  			
  			To apply the correction to a set of frames, a flat frame is required.
	
   Time correction
		
  			The time correction fixes the time of observation of source frames. This step
  			of the reduction process is optional and depends on particular situation. For example,
  			you can	use the correction to fix the bias of the PC's system clock in regards to 
  			the UTC.
	
   Photometry
		
  			The photometry is a process performed during the CCD image reduction. It 
  			takes a calibrated CCD image and detects stars on it. For each star, the brightness 
  			is determined. The results are saved to a file, which is called the photometry 
  			file.
  			
  			Current version of the software uses the aperture photometry method to
  			measure brightness of an object.
	
   Aperture photometry
		
  			The aperture photometry is a method for measurement of brightness of a star
  			on a CCD frame. After image calibration, the frame consists of two components - 
  			sky background level and a signal from a star. To measure star's brightness, we 
  			have to separate these two components.
  			
  			The algorithm of aperture photometry was developed from a method used for
  			a observations made by means of a photomultiplier. Unlike this, the aperture photometry
  			for digital images carried out by means of a CCD camera uses the digital image 
  			processing algorithms. The algorithm uses two apertures - an annular shaped one to
  			measure the background level in vicinity of a star and a circular one to measure
  			the total flux from a star. The background is subtracted from a star's signal.
  			The brightness of a star is defined as a ratio of its signal to a fixed reference value.
  			Such value, expressed in magnitudes, is called instrumental absolute brightness.
	
   Matching
		
  			The matching is the last step in the standard CCD image reduction scheme. 
  			It process a set of photometry files of a same view field and finds a relation
  			between individual stars on the frames. For example, the star on position (100, 100) 
  			on frame 1 is the same one as star on position (120, 95) on frame 2 etc. Each
  			star is then assigned a unique identifier, which is the same for the same star
  			on all frame in the project.
  			
  			To perform the matching, one frame must be given as a reference frame.
  			The reference frame is usually one frame from a set of input frames, but another
  			frame may be used - see catalog files.
	
   Full Width at Half Maximum
   FWHM
		
  			Full Width at Half Maximum is used in the C-Munipack software to express
  			a diameter or width of a star-like objects. The width is measured in the middle 
  			between background level and the maximum value.
	
   Reduction of CCD frames
		
  			The reduction of CCD frames is a process which takes raw frames carried out
  			by means of a CCD camera and produces a set of photometry files, one file for
  			each input frame. The photometry file consists of a set of stars. For each star,
  			the derived brightness is stored. An unique identifier assigned to each star
  			is used to identify a particular star between frames.
  			
  			Depending on a reduction scheme, the reduction consists of the following
  			three steps: calibration (bias-frame correction, dark-frame correction, flat-frame 
  			correction and time correction), photometry and matching.
	
   Light curve
		
  			A light curve is a graph of brightness of a star, as a function of time. The 
  			light curve is one of the products of the C-Munipack software. It is used especially
  			in the observation of variable stars. The brightness of a star is expressed in 
  			magnitudes, usually as a difference between two objects - a variable star and a
  			comparison star. One or more check stars are used in addition. Absolute intensities
  			are used sometimes, another post-processing step outside the C-Munipack software
  			is required in this case.
  			
  			Depending on a type of the light curve, a user must choose a variable star,
  			a comparison star and optionally one of more check stars. Object's equatorial 
  			coordinates or observer's geographical coordinates may be required, too.
	
   Track curve
		
  			A track curve is a graph of spatial offset of CCD frames, as a function 
  			of time. Such curve is used to monitor the stability and precision of telescope 
  			mount and its clock drive.
	
   Reference file
		
  			A reference file is a photometry file which is used in the matching process.
  			It is either a photometry file for one of source frames or a catalog file of
  			the same field.
  			
  			The matching algorithm takes two photometry files at a time and find 
  			corresponding stars on them. To match more than two scientific frames, one frame from
  			the set is chosen as a reference file and all source frames are matched one by one
  			against the reference.
	
   Catalog file
		
  			A catalog file is a file that consists of a list of stars (their positions
  			and magnitudes). Unlike a photometry file, it can contain identification of variable, 
  			comparison and check stars.
  			
  			A catalog file can be made by means of the Muniwin user interface and it is
  			used as a reference file in the matching process. In this case, the software restores
  			the selection of stars from a file and an user don't need to select the stars again.
  			It is useful if you repeatedly observe the same field.
	
   Air mass coefficient
		
  			The air mass coefficient characterizes the attenuation of the light when it 
  			travels through the atmosphere. Because the atmosphere is not equally transparent
  			for all wavelengths, this effect becomes important when you want to compute the
  			absolute magnitudes for measured objects. The coefficient equals to 1.0 when the 
  			object is in the zenith by definition and it is greater than one for altitudes 
  			between the zenith and the horizon. It is not defined for objects at or below the 
  			horizon.
	
   Altitude
		
  			Altitude is one of the coordinates of the horizontal coordinate system. It is
  			used for computation of the air mass coefficient. To compute the object's altitude
  			at a time of observation, the date and time of observation, object's equatorial 
  			coordinates and observer's geographical coordinates must be known.
	
   Heliocentric correction
		
  			The heliocentric correction fixes the variations of the distance between the
  			observer (Earth) and observed object which occur because the Earth orbits around the
  			Sun. This effect is not negligible especially in observation of eclipsing variable
  			stars or exoplanet transitions, when you need to measure the time interval between
  			two events, that occurred few weeks or months apart. The heliocentric correction
  			transforms the times in such a way that they are related to a virtual fixed point 
  			placed in one focus of the Earth's orbit (close to the Sun).
  			
  			The heliocentric correction is optionally performed during construction of
  			a light curve. Object's equatorial coordinates are required.
	
   Julian date
   JD
		
  			The Julian date (JD) is the interval of time in days and fractions of a day 
  			since January 1, 4713 BC Greenwich noon. It is widely used and recommended by International 
  			Astronomical Union to express the time of an event, for example a particular observation, 
  			minimum of a variable star, etc.
  			
  			You can use the Muniwin program to convert the Gregorian calendar date to 
  			Julian date and back.
	
   Heliocentric Julian date
   HJD
   JD(hel.)
		
  			The heliocentric Julian date is a time of an event (a particular 
  			observation, time of minimum) with the heliocentric correction applied.
	
   Time of observation
		
  			In the C-Munipack software, the time of a particular CCD frame is always related 
  			to a center of the exposure.

   Project
   
			The project in the C-Munipack software is a container that retains the products of the 
			CCD frame calibration and reduction process. The project consists of project settings,
			image files and photometry files. The projects are used to keep the products when the
			user switches between various data sets.
			
   Profile
   
			The profile provides a set of configuration parameters that are used to process the data. Unlike 
			the project, it does not contain the data - image files and photometry files. The profiles
			are used to transfer the configuration parameters between projects. 

   Ensemble photometry
			The ensemble photometry is a technique of a light curve construction which utilitizes measurements
			of multiple comparison (constant) stars in order to reduce the random errors. The method implemented 
			in the C-Munipack software can be called "strict" ensemble photometry, because it requires that *all*
			stars from the comparison star set are present on every frame, if this is not satistifed, the frame
			is discarded from the light curve. Fluxes (not magnitudes) of the comparison stars are summed up to 
			get instrumental magnitude of an "artificial" comparison star which is than used to find out 
			differential brightness of a variable star.
			