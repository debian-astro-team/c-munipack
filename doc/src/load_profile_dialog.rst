.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: load_profile_dialog.rst,v 1.1 2015/07/06 08:19:49 dmotl Exp $

.. index::
   pair: Load profile; dialog
   pair: project settings; load
   
.. _load-profile-dialog:

Load profile (dialog)
=====================

The dialog is used to load settings from a profile into the current project.


Activating the dialog
---------------------

The dialog can be opened from the :ref:`project-settings-dialog`, the root page, using 
the :guilabel:`Load from profile` button.


The dialog controls
-------------------

.. figure:: images/loadprofile_couts.png
   :align: center
   :alt: Load project settings dialog
   
   Load project settings dialog

List of existing profiles is shown in the table. The profiles are divided into two categories -
user-defined profiles (1) and predefined profiles (2).

Click the :guilabel:`Edit profiles` button to open the :ref:`edit-profiles-dialog`.


.. seealso:: :ref:`project-settings-dialog`, :ref:`edit-profiles-dialog`, :ref:`save-profile-dialog`
