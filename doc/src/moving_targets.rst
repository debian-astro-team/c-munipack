.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: moving_targets.rst,v 1.2 2016/02/28 10:27:21 dmotl Exp $

.. index::
   simple: moving targets
   simple: minor bodies

.. _moving-targets:

Photometry of minor Solar System bodies
=======================================

The major challange in photometry on minor Solar System bodies is the tracking
of the moving object of interest in the view field. The matching procedure, as
it is used for a variable star, would not work without modifications, because
the object changes its position between the frames with respect to surrounding 
stars. Also, the stars moves between the frames, as a result of mechanical
inaccuracy of a telescope mount and its clockdrive. 

This chapter explains how to make a light curve of a moving object, such as 
a minor Solar System body. For a guide on making a light curve of stationary
objects (variable stars or exoplanets) follow :ref:`this link <light-curve>`.

The procedure of making a light curve of a moving object is the same as the
process in case of a variable star except the matching step. The matching process is 
different here; a user has to specify at least three frames, called ``key frames`` 
and mark the moving object on each of them.

Before you start
----------------

Before you start the reduction of your own CCD frames, you may
need to perform several pre-processing steps. Though it isn't necessary,
combining the several correction frames into so-called "master" ones
is advisory, because it reduces the noise and makes the result more
precise. The method of making master correction frames is described in
separate chapters.
   
.. seealso:: :ref:`master-dark-frame` and :ref:`master-flat-frame`


Creating a new project
----------------------

First of all, we will create a new :ref:`project <projects>`. To begin with processing of 
a set of source frames, we create a new project. To do so, open the :guilabel:`Project` menu 
and activate the :guilabel:`New` item. A new dialog appears.

.. figure:: images/newproject_adorea.png
   :align: center
   :alt: "New project" dialog
   
   The dialog for making a new project.
   
Fill in a name that will be assigned to the project. Because the name of the file that keeps track of the data related
to the project, the project file, is derived from the name, some characters cannot appear in the project name, do not 
use: / \ ? % * : | " < and >.

The field :guilabel:`Location` shows a path to the directory where a new project will be created. Edit the path to 
change the location, you can also click the :guilabel:`Browse` button to select a directory in a separate dialog.

The dialog also displays a list of available profiles. A profiles provides an intial set of configuration parameters 
into a new project. When you confirm the dialog, you should be in the main window again now. The table of input files 
shown there is empty.

.. figure:: images/mainwindow_adorea.png
   :align: center
   :alt: Main application window
   
   The main application window with the table of input files, now empty.

.. seealso:: :ref:`new-project-dialog` and :ref:`main-window`.   
   
Input files
-----------

Now, we are going to tell the program which files we are going to work on. These files are called the input 
files. Their list is displayed in the table in the main application window. When the application is closed,
the list of files are saved to the disk and it is restored back when the program is launched again.

Supposing that the table now consists of files from your previous task, let's get rid of them. Please, use 
:menuselection:`Files --> Clear files` to start a new task instead of just removing the files from 
the table. Besides the clearing the table of input files, this function resets all internal variables, too.

Now, we need to populate the table with the CCD frames we're going to reduce. There are two methods how to 
achieve that - adding a individual files or adding all files from a folder. Which way is the best for you 
depends on organization of your observations on the disk. I'd suggest you to make a folder for each year, 
a folder for each night in it, the a subfolder for a name of object or another view field identification and 
finally a subfolder named upon the color filter (if you use more of them). In this case, the "Add frames from a folder" 
method is more convenient.

Click on :menuselection:`Files --> Add frames from folder` in the main menu. A new dialog appears. In the dialog, 
find a folder where the inputs files are stored in. Click on an entry in the :guilabel:`Places` pane to go to one 
of a preselected folders, double click in the middle pane enters the folder. The buttons in the upper part 
of the dialog shows your current position in the directory tree, you can use them to go to one of the parent
folders. Enter the folder with the input files - you should see them in the middle pane. Then, click on the 
:guilabel:`Add` button to add files to the table of input files. The program shows the number of added files in
the separate dialog. The :guilabel:`Add frames from folder` dialog is not closed automatically and allows a user 
to continue. Click on the :guilabel:`OK` button to close the dialog and return to the main window.

.. figure:: images/addfolder_adorea.png
   :align: center
   :alt: "Add folder" dialog
   
   The "Add folder" dialog with the place selection box (left), the file selection box (right)

If you want to reduce only a subset of files from a folder, click on :menuselection:`Files --> Add individual frames` 
in the main menu. A new dialog appears, similar to the previous one. In the dialog, find a folder where the inputs 
files are stored in. Click on an entry in the :guilabel:`Places` pane to go to one of a preselected folders, double
click in the middle pane enters the folder. The buttons in the upper part of the dialog shows your current position 
in the directory tree, you can use them to go to one of the parent folders. In the middle pane, select the files 
using the :kbd:`Ctrl` modifier to include and exclude a single file and the :kbd:`Shift` modifier to include a range 
of files. Then, click on the :guilabel:`Add` button to add selected files to the table of input files. The program 
shows the number of added files in the separate dialog. The :guilabel:`Add individual frames` dialog is not closed 
automatically and  allows a user to continue. Click on the :guilabel:`OK` button to close the dialog and return to 
the main window.

.. figure:: images/addfiles_adorea.png
   :align: center
   :alt: "Add files" dialog
   
   The "Add files" dialog with the place selection box (left), the file
   selection box (middle) and the preview panel (right).

.. seealso:: :ref:`add-frames-from-folder-dialog` and :ref:`add-individual-frames-dialog`.


Frame reduction
---------------

Reduction of CCD frames is a process that takes source CCD frames, performs their conversion and calibration,
detects stars on each frame and mearures their intensity and finally finds correlation (match) between objects 
that were found in the data set. The process of reduction prepares the data that are necessary for making a light 
curve.

The reduction consists of several steps - conversion, calibration, photometry and matching. They can be invoked
step-by-step manually. The preferred way is to use the :guilabel:`Express reduction` dialog that allows to perform 
these steps in a batch. Using the menu, activate the :menuselection:`Reduce --> Express reduction` item.
A new dialog appears. The dialog has several options aligned to the left, Each of them relates to an optional 
step in the reduction process. 

.. figure:: images/express.png
   :alt: "Express reduction" dialog
   :align: center
   
   The dialog for setting parameters of the reduction process

.. rubric:: Fetch/convert files
      
Check the :guilabel:`Fetch/convert files`. In this step, the program makes copy of the source CCD frames. 
This is necessary, because the following calibration steps will modify them and we don't want the program 
to change our precious source data.

.. figure:: images/express_convert.png
   :alt: "Express reduction" dialog
   :align: center

.. note: DSLR camera - color photometry
   Be careful when you are using color components from the RAW images - see :ref:`processing_raw_images`.

.. rubric:: Dark-frame correction

A raw CCD frame consists of several components. By the calibration process, we get rid of those 
which affect the result of the photometry. In some literature, the calibration is depicted as the 
peeling of an onion. There are three major components which a raw frame consists of - the current 
made by incident light, current made thermal drift of electrons (so-called dark current) and 
constant bias level. In standard calibration scheme, which we will demonstrate here, the dark-frame 
correction subtracts the dark current and the also the bias. Because of the nature of the dark current, 
it is necessary to use a correction frame of the same exposure duration as source files and it must 
be carried out on the same CCD temperature, too. Thus, the properly working temperature regulation 
on your CCD camera is vital.

.. figure:: images/express_dark.png
   :alt: "Express reduction" dialog
   :align: center

.. rubric:: Flat-frame correction

Then, we have to compensate the spatial non-uniformity of a detector and whole optical
system. These non-uniformities are due to the fabrication process of a CCD chip and they are
also natural properties of all real optical components, lenses in particular. The flat-frame
correction uses a flat-frame to smooth them away. The flat-frame is a frame carried out 
while the telescope is pointed to uniformly luminous area. In practice, this condition is
very difficult to achieve, the clear sky before dusk is usually used instead.

.. figure:: images/express_flat.png
   :alt: "Express reduction" dialog
   :align: center

   
.. rubric:: Photometry

The photometry is a process that detects stars on a CCD frame and measures their brightness. Unlike the previous 
steps, the result is saved to a special file, so-called the photometry file. There are a lot of parameters which 
affect the star detection and also the brightness computation. In this example, the default values work fine, 
but I would suggest you to become familiar with at least two of them - FWHM and Threshold - before
you start a real work. Check the :guilabel:`Photometry` option.

.. figure:: images/express_photometry.png
   :alt: "Express reduction" dialog
   :align: center


FWHM
	The FWHM parameter specify the expected width of stars on a frame.
	The value is the Full Width at Half Maximum in pixels. The parameter
	controls the behavior of the low-pass digital filter, which is used
	in the star detection algorithm.

Threshold
	The Threshold parameter specify the lowest brightness of detected
	stars. Fainter objects are considered to be background artifacts and thus
	sorted out. The value is dimensionless coefficient.

Once you tune up the parameter for your environment, usually it is not
necessary to adjust them for every task, unless the quality of your images
varies considerably. In the first iteration, you can use the default values
(FWHM = 3.0 and Threshold = 4.0) and do the photometry. Click on the
:menuselection:`Reduce --> Photometry` item
in the main menu and confirm the new dialog by the :guilabel:`OK`.
Then, by double click on a frame in the main window open the preview window
and check the results. If there are stars which have been detected as a close
binary although it is not true, you should increase the *FWHM*
value. If the stars you are interested in are not detected, try decrease the
*Threshold* value. If it doesn't help, decrease the
*FWHM*. If there is a lot of background artifacts detected
as a real stars, increase the *Threshold*. By several
iterations, adjust the parameters, so all the stars you are interested in are
detected and there are no false binaries.


.. rubric:: Matching (skipped)
   
In case of observation of moving object of intereset, do not perform the matching
now; keep the :guilabel:`Matching` option unchecked. The matching process requires
user input, but this can be done only when photometry of the source frames has been 
finished.
   

.. rubric:: Invoking the reduction process

In previous steps, we have configured parameters of the reduction process and we are ready to start
it. Click the :guilabel:`OK` button. During the execution a new window appears displaying the state 
of the process; all the information is also presented there. This window will be automatically 
closed after finishing the process. Wait for the process to finish. 

.. figure:: images/progress.png
   :alt: Progress dialog
   :align: center
   
   The dialog displayed during time demanding operations.

After finishing, the icon in the file table changes; the information about the time of observation, 
the length of the exposition and the used filter is filled in. In case some of the frames could not be 
processed successfully, the entry is be marked with a special icon and in the :guilabel:`Status` column 
the error message is indicated.

.. figure:: images/mainwindow_adorea2.png
   :alt: Main application window
   :align: center
   
   The main application window after the photometry step
   
.. seealso:: :ref:`express-reduction-dialog`.


Matching the frames and tracking the object of interest
-------------------------------------------------------

The object is expected to move along a smooth curve in time. We pick up at least three frames, called key frames, 
and we mark the object of interest on each of the key frames. It is recommended to start with one key frame 
at the beginning, one key frame in the middle and one key frame at the end of the set. The key frames define
three points and a curve is fitted between them. The curve is used to determine expected position of the object 
in observation time of any other frame and an unidentified object found on a non-key frame close to the 
expected position is matched as the object of interest. 

Click on the :menuselection:`Reduce --> Match stars` item in the main menu. A new dialog appears. 

.. figure:: images/matchstars_couts3.png
   :alt: Matching (moving target)
   :align: center
   
   The Matching dialog in the moving target mode
   
In the dialog, check the option :guilabel:`Moving target` (20). List of source frames is presented in the 
table (21). In the table, select a frame that will be used as the first key frame. The first key frame has
got the same function as a reference frame in the matching process for stellar objects; all frames are 
matched against this frame and it is also used as a background in object selection dialogs. When you select
a frame, it is shown in the preview area (22). Click the :guilabel:`Add key frame` button. A new dialog 
appears.

.. figure:: images/selecttarget_couts.png
   :alt: Select target
   :align: center
   
   The Select target dialog
   
Find the object of interest and click on it (31). Confirm the dialog (32). You get back to the previous
dialog. In the table of frames (21), the key frames gets an icon of a red key before the frame number (24).

Repeat this step until there are at least three key frames. The other key frames are marked by a white key
icon (25). The red key icon indicates the frame that is used as a reference key frame in the matching process.
Then, click on the :guilabel:`Apply` button to start the matching process.

The process now runs automatically. The selected key frames are processed first, the positions of the 
selected object of interest on the key frames are used to find the path which the object follows by fitting
a polynomial function to them. Then, the rest of the frames are processed; the fit is used to find out
an expected position of the object in an observation time and an object close to the expected position 
is marked as the object of intereset on the frame.

.. seealso:: :ref:`match-stars-dialog`

   
Making a light curve
--------------------

Click on the :menuselection:`Plot --> Light curve` item in the main menu. A new dialog appears. The dialog allows 
you to set up the options for the light curve. If you want only a subset of frames from the project to be included 
in the curve, check the selected files only option in the box. The program can also include several corrections and 
coefficients to the output file, these features are discussed later in the text.

Confirm the dialog by the :guilabel:`OK` button.

.. figure:: images/makelightcurve2.png
   :align: center
   :alt: Make light curve
   
   The dialog for making a light curve.

.. seealso:: :ref:`make-light-curve-dialog`
   

Selecting the stars
-------------------

The next dialog shows the reference frame and allows you to select the object of interest, 
the comparison and the check stars. All detected objects are highlighted. Click anywhere
on the frame with the right mouse button to open a context menu. Select the item
:guilabel:`Set moving target as a variable`. The object of interest is now drawn in red 
color and the label "var" is placed near to it (1). Pick up a star that shall be used as
a comparison star and click on it using left mouse button. Select the item :guilabel:`Comparison`,
the object is now drawn in green color and the label "comp" is placed near to it (2).
I would recommend you to pick up also two check stars. Confirm the selection by the 
:guilabel:`OK` button (3).

.. figure:: images/stars2.png
   :align: center
   :alt: Choose stars
   
   The dialog for selection of an object of interest (red), a comparison star (green)
   and check stars (blue).

.. seealso:: :ref:`choose-stars-dialog`
   
   
Choosing an aperture
--------------------   

Now, we have to choose the aperture. You can image the aperture as a virtual
circular pinhole, placed on each star on a frame to measure its brightness. All
pixels that are inside the pinhole are included in computation leaving out the
background pixels. The best aperture should be big enough to include most of the
star's light, on the other hand, the bigger aperture is used the more background
is included and the more noisy the result is. Because of this, the photometry
process computes the brightness of each star in a set of predefined apertures of
radius in the range of 2 and 30 pixels.

To select the best aperture, we can take advantage of a comparison and
check stars - providing that they are constant, we can compute the differential
magnitudes between each couple of them on each other and then compute the variance
or standard deviation from the mean level. For the best aperture, the deviations
are minimal.

In the next dialog, the graph shows the standard deviation for each aperture.
Find the aperture with the minimal deviation and
click on it using the left mouse button. A context menu appears. Select the
:guilabel:`Select aperture` item. The point is drawn in red color
now and the label is placed near to it. Confirm the selection by the :guilabel:`OK`
button.

.. figure:: images/aperture.png
   :align: center
   :alt: Choose aperture
   
   The dialog for selection of an aperture.

.. seealso:: :ref:`choose-aperture-dialog`
   
   
Plotting a light curve
----------------------

Now, the program has got enough information to make a light curve.
It is presented in a new window which appears automatically.

.. figure:: images/lightcurve.png
   :align: center
   :alt: Light curve graph
   
   The dialog with a light curve graph.

.. seealso:: :ref:`light-curve-dialog`


.. index::
   pair: outliers; trimming

Trimming outliers from the curve
--------------------------------

Is is possible to manually trim outlying observations from the curve.
You can select an individual point by a right click, you can also
select more than one point by pressing a Shift key and left mouse button
and drawing a rectangle in the graph. Then, click the right mouse button
on a point in the selection to open the context menu. There are two options:
When you select the option :guilabel:`delete from data set`,
the selected measurements are removed from the current light curve, the
data will be shown when you make another light curve or :guilabel:`Rebuild`
the actual curve. The other option :guilabel:`remove from project`
means that the frames corresponding to the selected measurements are removed
permanently from the list of input files (see: Main application window) and
such measurements won't be included in any other output. It is not allowed
to remove a reference frame.

.. note:: 
   | CCD frames of 268 Adorea:
   | BlueEye600, RiLA 0.60-m f/5 Ritchey-Chretien + CCD G4-4000BI
   | (ProjectSoft HK & Astronomical Institute of the Charles University in Prague)
