.. C-Munipack - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: bias_frame_correction.rst,v 1.1 2015/07/06 08:19:48 dmotl Exp $

.. _bias-frame-correction: 
.. index::
   pair: bias-frame; correction
   
Bias-frame correction
=====================

The bias-frame correction is a subtraction of a bias frame *B* from a source frame *X* 
to generate an output frame *Y*. Unlike the dark correction, scaling is not applied, 
because the bias is independent of exposure duration. The bias-frame correction is
applied only in the *advanced calibration scheme*. 

.. math::
   :label: bfc
   
   Y(x, y) = X(x, y) - B(x, y)

The following statements describe how bad and overexposed pixels are treated:

* If a pixel is bad in either source frame or bias frame, it must be marked as bad on the corrected frame. 
* If a pixel is overexposed in the source frame, it must stay overexposed after the correction.
* If a pixel is overexposed on the bias frame, it is marked as bad on the corrected frame.
