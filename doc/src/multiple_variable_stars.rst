.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: multiple_variable_stars.rst,v 1.2 2016/01/01 09:39:22 dmotl Exp $

.. _multiple-variable-stars:

Fields with multiple variable stars
===================================

The Muniwin software provides several features that simplifies the processing of a frame set
that includes multiple variable stars on one star field. The software allows a user to create
a set of selections of objects, each of which is consisting of choice of variable star,
comparison star(s) and check star(s). These selections can be exported to a catalog file
and reused in the next observation of the same star field.


Defining a set of object selections
-----------------------------------

* Create one project for your frame set

* Reduce your observation in a normal way, create a light curve for the first variable star and
  save it to the file.

* Start making a light curve for the next variable (:menuselection:`Plot --> Light curve`).
  In the :ref:`Choose stars <choose-stars-dialog>` dialog, the software always shows the most 
  recently used selection of objects. Click on the :guilabel:`Save as...` button (10) on the 
  toolbar. A new dialog appears. Enter a name of the first variable.
  
.. figure:: images/choosestars_couts3.png
   :align: center
   :alt: Choose stars dialog
   
   The dialog for selecting a variable star, a comparison star and check stars
  
* Change the selection of the variable stars, you can also choose another comparison or
  check stars if needed. Confirm the dialog and continue. Save the light curve for the second
  variable.
  
* Use the same steps to make light curve for third, fourth, ... variable.

To revert back to the selection that was previously saved, use the selection field (11) on
the toolbar.


Multiple variables in the catalog file
--------------------------------------

The selections of objects that were saved using the steps described above are preserved when
you make a :ref:`catalog file <catalog-files>`. The dialog :ref:`Make catalog file <make-catalog-file-dialog>`
allows you to inspect and edit the selections before you create the new catalog file.

.. figure:: images/makecatalog_couts2.png
   :align: center
   :alt: Make catalog file dialog

   Make catalog file dialog

\(1) Select the name of the selection. The selection is shown in the preview area (2).

\(3) Click the :guilabel:`Edit` button to edit the selections.


Using catalog files with multiple object selections
---------------------------------------------------

The catalog file that was created using the previous steps can be used in the next observation 
of the same star field as a reference for frame matching in the same way as any other catalog file. 
All the object selections that were saved to the catalog files are restored.

When you are about to choose the stars when making a light curve for the first variable,
click on the selection field (11) in the :ref:`Choose stars <choose-stars-dialog>` dialog and
select the named that you specified for the selection of objects for the first variable.

.. figure:: images/choosestars_couts3.png
   :align: center
   :alt: Choose stars dialog

Use the same procedure for the second, third, fourth, etc. variable star.

.. seealso:: :ref:`match-stars-dialog`, :ref:`choose-stars-dialog`, :ref:`make-catalog-file-dialog`
