.. C-Munipack - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: toolkit_commands.rst,v 1.1 2016/02/27 09:14:01 dmotl Exp $

.. _toolkit-commands:

Commands
========

The following command line programs are installed as a part of the C-Munipack software:

.. toctree::
   :maxdepth: 1

   airmass_command
   autoflat_command
   biasbat_command
   darkbat_command
   flatbat_command
   helcor_command
   kombine_command
   konve_command
   meanbias_command
   meandark_command
   munifind_command
   munilist_command
   munimatch_command
   muniphot_command
   timebat_command
