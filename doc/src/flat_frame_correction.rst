.. C-Munipack - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: flat_frame_correction.rst,v 1.2 2015/07/12 07:44:57 dmotl Exp $

.. _flat-frame-correction: 
.. index::
   pair: flat-frame; correction
   
Flat-frame correction
=====================

The flat-frame correction is a division of a source frame *X* by a flat frame *F* and multiplying it
by a constant *k*. The constant *k* is determined such that the range of pixel values and 
the gain factor (number of photons per ADU) are roughly preserved. The constant *k* is computed
as a mean value of the correction frame *F* by means of the robust mean algorithm, see the chapter
:ref:`robust-mean`.

.. math::
   :label: ffc

   Y(x, y) = X(x, y) - k\,\frac{X(x, y)}{F(x, y)} = X(x, y) - \bar{F}\,\frac{X(x, y)}{F(x, y)}
   
Bad and overexposed pixels require special treatment:

* If a pixel is bad in either source frame or dark frame, it must be marked as bad on the corrected frame. 
* If a pixel is overexposed on the source pixel, it must stay overexposed after the correction.
* If a pixel is overexposed on the flat frame, it is marked as bad on the corrected frame.
