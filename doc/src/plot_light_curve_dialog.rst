.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: plot_light_curve_dialog.rst,v 1.1 2016/01/01 09:39:22 dmotl Exp $

.. index::
   pair: Plot light curve; dialog
   
.. _make-light-curve-dialog:

Plot light curve (dialog)
=========================

The "Plot light curve" dialog is used to set up the initial parameters
for a new light curve. 


Activating the dialog
---------------------

The dialog can be activated:

.. |lightcurve_icon| image:: icons/lightcurve.png

- from the main menu: :menuselection:`Plot --> Light curve`.

- from the main toolbar: |lightcurve_icon|

.. index::
   pair: light curve; plot

Making a light curve
--------------------

.. figure:: images/makelightcurve_couts.png
   :align: center
   :alt: Plot light curve (dialog)
   
   Plot light curve (dialog)


\(1) It is possible to include all source files in the project or the files that are
currently selected in the table of input files. 

\(2) Set up the light curve options:

     *  *Compute heliocentric correction* - is option if you want to include the heliocentric Julian date and heliocentric
        correction of the observed object to the resulting data. If so, you have to fill in the 
        object's coordinates below.

     *  *Compute air mass coefficients* - check this option if you want to include the air mass coefficient and apparent altitude
        of observed object to the resulting data. If so, you have to fill in the object's celestial 
        coordinates and observer's geographic coordinates below.

     *  *Ensemble photometry* - if this option is enabled the :ref:`ensemble-photometry` method is used to reduce the random
        errors of the resulting light curve by combining measurement from multiple comparison (constant) stars.
        
     *  *Show raw instrumental magnitudes* - by default the light curve consists of differential magnitudes (differences between two
        objects). If this option is checked, the light curve will consists of raw instrumental magnitudes,
        as they were determined in the aperture photometry. Please note, that the instrumental magnitudes
        does not correspond to absolute magnitude of objects and without further processing, these values
        are meaningless. 

     *  *Select all stars on the reference frame* - check this option to create a light curves for all objects
        on the reference frame. If the differential magnitudes shall be computed, you will have to select at least
        one comparison star. This option is useful if you want to export complete photometry data for further
        processing.
        
.. note::

   If you want to save your data in AAVSO extended format, make a light curve with differential magnitudes
   for one variable star - in simple way: do *not* check the last two options.
        
\(3) Specify object's designation and object coordinates into the edit fields. Enter object's right 
ascension in hours. Use the hexagesimal format, separate the fields by a space character. Enter 
object's declination in degrees. Use the hexagesimal format, separate the fields by a space character.
Click the button (4) to retrieve object's coordinates from a table of predefined objects or a catalog 
of variable stars.

\(5) Specify observer's geographic coordinates into the edit fields. Enter observing location designation
(i.e. city name). Enter the longitude in degrees. Use the hexagesimal format, separate the fields by a 
space character. Enter 'E' character at the first position in a string to indicate that the location 
is on the eastern hemisphere or 'W' character for locations on a western hemisphere. Enter observer's 
latitude in degrees. Use the hexagesimal format, separate the fields by a space character. Enter 'N' 
character at the first position in s string to indicate that the location is on the north hemisphere 
or 'S' character for locations on a southern hemisphere. Click the button (6) to retrieve observer's 
coordinates from a table of predefined locations.

Click the button (7) to proceed.


.. seealso:: :ref:`light-curve-dialog`
