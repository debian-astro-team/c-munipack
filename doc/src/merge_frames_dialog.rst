.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: merge_frames_dialog.rst,v 1.1 2015/07/06 08:19:49 dmotl Exp $

.. index::
   pair: Merge frames; dialog
   
.. _merge-frames-dialog:

Merge frames (dialog)
=====================

The `Merge frames` dialog is used to start the process of merging CCD frames.


Activating the dialog
---------------------

The dialog can be activated:

.. |merge_icon| image:: icons/merge.png

#. from the main menu: :menuselection:`Make --> Merge frames`.

#. from the main toolbar: |merge_icon|


Setting up the parameters
-------------------------

.. figure:: images/merge_couts.png
   :align: center
   :alt: Merge frames
   
   The "Merge frames" dialog
   
   
\(1) It is possible to include all source files in the project or the files that are
currently selected in the table of input files. By means of this option, it is possible to
split frames manually by processing a subset of source files at a time.

\(2) Check this option to let the program split source frames automatically. Next parameters
in the middle pane sets the merging rules. (3) Specify maximum number of source frames merged
into a single output frame. (4) Specify maximum time interval between any two frames that can
be combined	into a single frame. (5) Specify minimum number of source frames. If the there are
less frames than this value, the output frame won't be made. This situation may happen if the
time interval is too short.

\(6) Check this option to make a single output frame.

\(7) Enter a path to the directory where output files shall be saved to. You can use the
ellipsis button to open a new dialog that allows you to browse the files and folders.

\(8) Enter a string that the new files shall begin with. The prefix is followed by a dot,
ordinal number of a frame (see below), a dot and 'fts' extension.

\(9) Enter an ordinal number of the first output frame. (10) Enter a step increment for
ordinal numbers of frames. The ordinal number is left-padded with zeroes to specified number
of digits (11).


Merging frames
--------------

Click on the "Execute" button (12) to start the operation.

- All calibration steps (bias, dark and flat correction), photometry and matching must be
  made before merging frames.
