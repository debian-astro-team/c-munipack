.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: air_mass_coefficient_dialog.rst,v 1.1 2015/07/06 08:19:48 dmotl Exp $

.. index:: 
   pair: Air mass coefficient; dialog

.. _air-mass-coefficient-dialog:

Air mass coefficient (dialog)
=============================

The "Air mass coefficient" allows an user to compute air mass coefficient for specified star,
location and date of observation. The object's altitude in degrees is also presented.


Activating the dialog
---------------------

The dialog can be activated:

#. from the main menu: :menuselection:`Tools --> Air mass coefficient`.


.. index:: 
   pair: Air mass; coefficient

Computing air mass coefficient and object's altitude
----------------------------------------------------

.. figure:: images/airmass_couts2.png
   :align: center
   :alt: Air mass coefficient
   
   Computing air mass coefficient and object's altitude
   
Enter object's right ascension to the text box (1) in hours. Use the hexagesimal format, separate
the fields by a space character.

Enter object's declination to the text box (2) in degrees. Use the hexagesimal format, separate
the fields by a space character.

Click the ellipsis button (3) to retrieve object's coordinates from a table of predefined
objects or a catalog of variable stars.

Enter observer's longitude to the text box (4) in degrees. Use the hexagesimal format, separate
the fields by a space character. Enter 'E' character at the first position in a string to indicate
that the location is on the eastern hemisphere or 'W' character for locations on a western hemisphere.

Enter observer's latitude to the text box (5) in degrees. Use the hexagesimal format, separate
the fields by a space character. Enter 'N' character at the first position in s string to indicate
that the location is on the north hemisphere or 'S' character for locations on a southern hemisphere.

Click the ellipsis button (6) to retrieve observer's coordinates from a table of predefined
locations.

Enter a Julian date into the text box (7) or date and time into the text box (8). As you type in the 
data, corresponding value of air mass coefficient and altitude is updated in the last two text boxes (9, 10).


.. seealso:: :ref:`jd-converter-dialog`, :ref:`heliocentric-correction-dialog`
