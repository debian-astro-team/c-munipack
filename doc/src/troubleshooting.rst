.. C-Munipack - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: troubleshooting.rst,v 1.1 2015/07/06 08:19:49 dmotl Exp $
   
.. _troubleshooting:
   
Troubleshooting
===============

.. rubric:: The Muniwin tells that my FITS files are not correct


Usually, this is a problem with the format of the date and time of observation.
See bellow for more information.


.. rubric:: The Muniwin tells that it cannot open/find files even if they are there.

If a path to files contains non-ASCII characters, like u with german umlaut, depnding
on your system, the program might not be able to cope with such paths. I'm aware of
this limitation, but I'm not able to fix that in short term. Meanwhile, please avoid
using non-ASCII characters in paths. 


.. rubric:: The Muniwin cannot read the date and time correctly


Unfortunately, there is not any standard how to save a date and time of 
observation to FITS file, so every program uses its own special format. Although 
current version of C-Munipack can read many different formats, it may happens, 
that your program uses the another one. Send me one frame or two as sample 
(see :ref:`bug-reports`) and I will add it to the sources promptly.


.. rubric:: The Muniwin does not run after 'make install' 

When the program is run from a command line it complains about missing shared 
library 'libcmunipack-2.0.so.xxx'.

Run 'ldconfig' after 'make install'. Although 'make install' should do the trick,
on some systems it does not.
