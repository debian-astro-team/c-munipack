.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: plot_air_mass_dialog.rst,v 1.1 2016/01/01 09:39:22 dmotl Exp $

.. index::
   pair: Plot air mass; dialog
   
.. _make-air-mass-curve-dialog:

Plot air mass (dialog)
======================

The "Plot air mass" dialog is used to create a curve of air mass coefficients and altitudes as a 
function of time. These values to useful in data post-processing to compute the extinction coefficients.


Activating the dialog
---------------------

The dialog can be activated:

#. from the main menu: :menuselection:`Plot --> Air mass`.

.. index::
   pair: air mass; plot

Making a plot of air mass
-------------------------

.. figure:: images/plotairmass_couts.png
   :align: center
   :alt: Plot air mass (dialog)
   
   Plot air mass (dialog)
   

\(1) It is possible to include all source files in the project or the files that are
currently selected in the table of input files. 

\(2) Specify object's designation and object coordinates into the edit fields. Enter object's right 
ascension in hours. Use the hexagesimal format, separate the fields by a space character. Enter 
object's declination in degrees. Use the hexagesimal format, separate the fields by a space character.
Click the button (3) to retrieve object's coordinates from a table of predefined objects or a catalog 
of variable stars.

\(4) Specify observer's geographic coordinates into the edit fields. Enter observing location designation
(i.e. city name). Enter the longitude in degrees. Use the hexagesimal format, separate the fields by a 
space character. Enter 'E' character at the first position in a string to indicate that the location 
is on the eastern hemisphere or 'W' character for locations on a western hemisphere. Enter observer's 
latitude in degrees. Use the hexagesimal format, separate the fields by a space character. Enter 'N' 
character at the first position in s string to indicate that the location is on the north hemisphere 
or 'S' character for locations on a southern hemisphere. Click the button (5) to retrieve observer's 
coordinates from a table of predefined locations.

Click the button (6) to proceed.

.. seealso:: :ref:`air-mass-curve-dialog`
