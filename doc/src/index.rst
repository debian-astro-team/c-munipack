.. C-Munipack - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: index.rst,v 1.4 2016/02/27 09:14:01 dmotl Exp $

   
C-Munipack 2.1 - User's manual
==============================

| Release: |release|
| Updated: |today|

Copyright 2015 David Motl 

Permission is granted to copy, distribute and/or modify this document under the terms of the 
GNU Free Documentation License, Version 1.2 or any later version published by the Free Software 
Foundation; with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.

.. toctree::
   :maxdepth: 1

   preface
   introduction
   installation
   getting_started
   using_muniwin
   muniwin_reference
   command_line_tools
   toolkit_reference
   library
   theory
   tips_and_tricks
   glossary
   history
   troubleshooting
   bug_reports
   references
