.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: chart_dialog.rst,v 1.1 2015/07/06 08:19:48 dmotl Exp $

.. index::
   pair: Chart; dialog
   
.. _chart-dialog:

Chart (dialog)
==============

The "Chart" dialog is used to make a chart of selected stars that
were used to make a light curve.

Activating the dialog
---------------------

The dialog can be activated:

#. From the :ref:`light-curve-dialog` dialog: :menuselection:`View --> Chart`.

Dialog controls   
---------------

.. figure:: images/chart_couts.png
   :align: center
   :alt: Chart dialog
   
   Chart dialog
   
\(1) The reference frame is displayed in the dialog. The actual variable star,
comparison star and check stars are highlighted.

Click the "Save" button (2) to save the chart to the file.


.. index::
   single: tags

Tags
----

Any object on the reference frame can have user-defined short string, so called `tag`. Tags are
used to provide additional identification to objects on an exported charts. For example, when 
you assign a tag with GCVS name to your variable star; the caption 'var' and the GCVS name will
be printed on the chart next to the star. Please note, that objects that are not selected can 
have tags, too.

Tags are stored to a :ref:`catalog file <catalog-files>` and they are loaded to the project when 
you use the catalog file as a reference frame in the :ref:`matching <match-stars-dialog>` step.

.. figure:: images/chart_couts2.png
   :align: center
   :alt: Choose stars dialog (with tags)
   
   Chart dialog with tags

To assign a tag to an object or edit existing tag, place a mouse cursor over a detected object, 
it is highlighted with a cross hair (3). Open a context menu with a single click, and select
:menuselection:`Edit tag`. A new dialog appears. Enter a text (single line) and confirm the 
dialog. The text should appear next to the selected star.

Tags can be removed from the same context menu.

.. seealso:: :ref:`light-curve-dialog`
