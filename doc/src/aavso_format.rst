.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: aavso_format.rst,v 1.2 2016/01/01 09:39:22 dmotl Exp $

.. index:: AAVSO Extended Format
   
.. _aavso-format:

Export light curve in AAVSO format
==================================

The `AAVSO Extended format <https://www.aavso.org/aavso-extended-file-format>`_ is one of the
export formats supported by the Muniwin application. The format is used to submit the
CCD or DSLR to the American Association of Variable Star Observers (`AAVSO <https://www.aavso.org/>`_). 

General overview
----------------

In order to save your observation in the format that the AAVSO accepts, take the following
steps:

#) Process your observation data and create a light curve. The light curve should consists
   of differential magnitudes; this is the default for the standard light curve generation 
   process in the Muniwin application.
   
#) Open the dialog for saving the light curve: :menuselection:`File --> Save`.

#) Select the *AAVSO Extended Format* as the file type. Choose a directory where the file
   shall be stored to and fill in the name of the file. Confirm the dialog.

#) A new dialog appears. The dialog allows you to fill in the information about the 
   observation and the observer. Follow the steps in the dialog and fill in the data. When 
   its last page is confirmed, the file is generated.
   
Detailed description
--------------------

Process your observation data and create a light curve. The light curve should consists
of differential magnitudes; this is the default for the standard light curve generation 
process in the Muniwin application. A detailed description of these steps can be found
in the chapter :ref:`light-curve`. 

The following steps are optional:

* If you check the option to compute the heliocentric correction, you will have the choice
  to save heliocentric Julian dates to the exported file. You need to fill in celestial
  coordinates of the variable star.

* You can also check the option to include the air mass coefficient values as well. If you
  do so, the values will be saved to the file. You have to fill in celestial coordinates
  of the variable star and also geographic coordinates of the observation location.
  
.. figure:: images/makelightcurve.png
   :align: center
   :alt: Make light curve
   
   Make a light curve
   
In the next dialog, select the stars. If you follow the standard photometry scheme with
one comparison star, you have to mark at least the variable star and one comparison star. 
It is recommended to pick up one check star, which helps you to choose an optimum aperture.

The enseble photometry mode is also supported. In this mode, you combine several comparison
stars together; in this case, marking at least one check star is mandatory. More about this 
mode can be find in the chapter :ref:`ensemble-photometry`.

.. figure:: images/stars.png
   :align: center
   :alt: Choose stars
   
   Mark the variable star (red), comparison star (green) and check stars (blue).
   
In the next dialog, select the aperture. If you marked at least one check star, the
dialog shows a graph of standard deviation between the comparison star and the check
star vs. the aperture size. Usually it is a V-like curve. Choose the aperture with
minimum value and select it.

.. figure:: images/aperture.png
   :align: center
   :alt: Choose aperture
   
   Select an aperture with minimum value
   
A new dialog shows a light curve:

.. figure:: images/lightcurve.png
   :align: center
   :alt: Light curve graph
   
   Light curve of the variable star. Please note, that the magnitudes are differential (V-C).
   
In the window with a light curve, click on the :menuselection:`File` --> Save` item in the 
local menu. A new dialog appears. Locate the folder where you want to save the results (1) and 
fill in the name of the output file (2). Choose the file type to be *AAVSO Extended Format* (3). 
If you checked the option to compute heliocentric correction, you have the choice to export
Julian dates helicentric or geocentric (4). Confirm the dialog by the :guilabel:`Save` button (5).

.. figure:: images/savelightcurve_aavso.png
   :align: center
   :alt: Save light curve
   
   Specify directory and file name, choose the correct file type.
   
A new dialog appears:

.. figure:: images/aavso1.png
   :align: center
   :alt: Export light curve in AAVSO format (page 1)
   
   Fill in the observation data (page 1)
   
Fill in the observation data:

\(1) Star identification - use the AAVSO Designation, the AAVSO Name, or the `AAVSO Unique 
Identifier <https://www.aavso.org/aavso-unique-identifier>`_, but NOT more than one of these. 

\(2) Observer code - the official `AAVSO Observer Code <https://www.aavso.org/where-do-observer-codes-come>`_ 
for the observer which was previously assigned by the AAVSO.
   
\(3) Observation type - specify type of the camera - CCD or DSLR.

\(4) Filter - click the :guilabel:`Browse` button to choose the color filter used.

\(5) Chart - please use the sequence ID you will find in red at the bottom of the photometry table. 
If a non-AAVSO sequence was used, please describe it as clearly as possible.

\(6) Notes - comments or notes about the observation. Leave this field blank if you don't want to
specify any notes.

Click the :guilabel:`Next` button to continue. The dialog is switched to the next page.

.. figure:: images/aavso2.png
   :align: center
   :alt: Export light curve in AAVSO format (page 2)
   
   Fill in the observation data (page 2)

The next page is designated to fill in the data about the comparison star. Provide the comparison
star identifier (usually an integer number) and brightness of the comparison star according to the filter
you used. Click the :guilabel:`Next` button to continue. 

In case of the ensemble photometry, you don't need to fill in anythink on this page, just click the
:guilabel:`Next` button to continue. 

The dialog is switched to the next page.

.. figure:: images/aavso3.png
   :align: center
   :alt: Export light curve in AAVSO format (page 3)
   
   Fill in the observation data (page 3)

The last page is shown only if you selected a check star. Please note, that in case of the ensemble
photometry, using the check star is mandatory. Provide the check star identifier (20). Click the 
:guilabel:`Save` button to finish the dialog. When the dialog is finished the file is generated.

You can review the content of the file using a text editor:

.. figure:: images/aavso4.png
   :align: center
   :alt: Generated file with exported observation
   
   Generated file with exported observation in the AAVSO Extended Format shown in the Notepad application.

   
Standardized magnitude
----------------------

Under the AAVSO Extended format, the brightness of a variable star is reported in the form of the
standardized magnitude, defined as:

.. math::

   V_{std} = (V_{ins} - C_{ins}) + C_{std}
   
where :math:`V_{ins}` and :math:`C_{ins}` are the instrumental magnitude of the variable and the comparison, respectively, and
:math:`C_{std}` is the chart magnitude for the comparison. The :math:`(V_{ins} - C_{ins})` part is the :math:`V-C` value that you 
find in the (differential) light curve created in the software. You specify :math:`C_{std}` values for your comparison
in the export settings (see above).

See `this page <https://www.aavso.org/standardized-magnitude>`_ for the original description.

Unlike the variable, which is reported as a standardized magnitude, the brightness of the comparison and the check star 
is reported in form of the instrumental magnitude - :math:`C_{ins}`. Please note, that there is a different in reporting
comparison and check star magnitude in the standard (single comparison star) and the ensemble photometry (multiple
comparison stars).


Ensemble photometry
-------------------

When multiple stars are used to stand in for a comparison, the value of :math:`C_{std}` needs be computed from chart
magnitudes of all stars which are incorporated in the ensemble. The C-Munipack software uses flux averaging, so the
instumental magnitude of the comparison :math:`C_{ins}` is replaced in the above equation by the following formula.

.. math::

   C_{ins} = -2.5 log_{10} (\frac{1}{N} \sum 10^{-0.4 C_{ins,i}})
   
where :math:`N` is a number of stars in the ensemble, and :math:`C_{ins,i}` is the instrumental magnitude of *i*-th 
star in the ensemble.
   
Similarly, we define the chart magnitude of the comparison :math:`C_{std}`:

.. math::

   C_{std} = -2.5 log_{10} (\frac{1}{N} \sum 10^{-0.4 C_{std,i}})

where :math:`C_{std,i}` is the chart magnitude of *i*-th star in the ensemble.
 
   
.. seealso::

   AAVSO Extended Format
     see https://www.aavso.org/aavso-extended-file-format
     
   AAVSO Observer Code
     see https://www.aavso.org/where-do-observer-codes-come
     
   AAVSO Unique Identifier 
     see https://www.aavso.org/aavso-unique-identifier
     
   Standardized magnitude
     see https://www.aavso.org/standardized-magnitude
