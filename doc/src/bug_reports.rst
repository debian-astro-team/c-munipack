.. C-Munipack - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: bug_reports.rst,v 1.1 2015/07/06 08:19:48 dmotl Exp $

   
.. index::
   pair: reporting; bugs
   pair: new feature; requests

.. _bug-reports:   

Bug reports, new feature requests
=================================

If you find a bug in the software or if you miss some feature, 
please don't hesitate to send me an email. Please include at least 
the following information to your report:

#. The version of the C-Munipack software you use.

#. Your operating system.

#. If the program shows an unexpected error message, an exact copy of 
   the message. A screenshot is always more helpful then just the text.
  
#. Does this bug occur always, randomly but often or seldom?

#. Can you tell the minimum steps that leads to the failure?

*Thanks!*

David Motl, dmotl@volny.cz
