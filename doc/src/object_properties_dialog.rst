.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: object_properties_dialog.rst,v 1.1 2016/01/01 09:39:22 dmotl Exp $

.. index::
   pair: Object properties; dialog
   
.. _object-properties-dialog:

Object properties (dialog)
==========================

The "Object properties" dialog is used to display a table or a plot of various object properties as a 
function of time.


Activating the dialog
---------------------

The dialog can be activated:

- from the main menu: :menuselection:`Plot --> Object properties`.

The "Plot object properties" dialog appears. Confirm the dialog.


Object properties dialog
------------------------

.. figure:: images/objectproperties_couts.png
   :align: center
   :alt: Object properties
   
   Object properties
   
   
\(1) The actual data set is shown here.

\(2) You can switch the labels on the X axis between Julian date (JD) or date and time (UTC).

\(3) The list of available data sets is displayed here.

\(4) The ID of the selected object is shown here. Click the ellipsis button (...) to check
the chart or select another object.


Data sets
---------

The following data sets are available:

*  *X* - The horizontal position of the object's center w.r.t. the frame border; in pixels

*  *Y* - The vertical position of the object's center w.r.t. the frame border; in pixels

*  *SKY* - The mean local background level computed in the "sky annulus"; in ADU

*  *FWHM* - The estimated de-focus of the object expressed as the Full Width at Half Maximum value;
   average from the two values measured in the horizontal and the vertical direction; in pixels

*  *MAG* - Absolute instrumental brightness of the object; in magnitudes


Context menu
------------

You can select an individual point by a right click, you can also
select more than one point by pressing a Shift key and left mouse button
and drawing a rectangle in the graph. Then, click the right mouse button
on a point in the selection to open the context menu. It provides following
functions:

- Show frame - it shows a preview to a selected frame. It is not allowed
  when more than one frame is selected.
  
- Show properties - it opens a new dialog with properties of selected frame.
  It is not allowed when more than one frame is selected.
  
- Delete from data set - selected measurements are removed from the
  current curve, the data will be shown again when you make a new curve
  or :guilabel:`Rebuild` the actual curve.
  
- Remove from project - source frames corresponding to the selected
  measurements are removed permanently from the list of input files and
  such measurements won't be included in any other output. It is not allowed
  to remove a reference frame.
  
  
Statistics
----------

The `Statistics` is a tool that computes and shows the minimum, maximum, sample mean
and standard deviation. 

To activate the tool:

#. From the local menu, select :menuselection:`Tools --> Statistics`.
   A new panel on the right side of the preview window appears.

If no points are selected, all points in the data set are included in the computation. 
To restrict the data for the statistics, press and hold the Shift key and draw a 
rectangle in a graph while you keep the left mouse button pressed down.

  
Measurement
-----------

The `Measurement` tool displays two cursors in the graph. The cursors can be adjusted
by dragging them using the left mouse button. The position of each cursor, their distance
and statistics for the data between cursors is presented.

To activate the tool:

#. From the local menu, select :menuselection:`Tools --> Measurement`.
   A new panel on the right side of the preview window appears.

   .. figure:: images/measurement.png
      :align: center
      :alt: Measurement tool
   
   Measurement tool

\(1) Choose the axis you want to measure.

\(2) Positions of the cursor 1 and 2 are displayed here.

\(3) Distance between cursor 1 and 2.

\(4) When cursors are defined on the independent (X) axis, number of points (frames) between 
cursor 1 and 2 are displayed and also minimum, maximum, mean value and sample deviation
are presented.

   
.. seealso:: :ref:`air-mass-curve-dialog`, :ref:`ccd-temperature-dialog`, :ref:`light-curve-dialog`, 
   :ref:`track-curve-dialog`
