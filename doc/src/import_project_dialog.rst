.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: import_project_dialog.rst,v 1.1 2015/07/06 08:19:48 dmotl Exp $

.. index::
   pair: Import project; dialog
   
.. _import-project-dialog:

Import project (dialog)
=======================

The `Import project` dialog is used to enter a location of a directory containing files 
to be imported into a new project. This tool allows one to transform a project from an old
version of C-Munipack into a new one. 


Activating the dialog
---------------------

The dialog can be activated:

#. from the main menu: :menuselection:`Tools --> Import data from C-Munipack 1.x`.


File browsing
-------------

.. figure:: images/importproject_couts.png
   :align: center
   :alt: Import project dialog
   
   The dialog for making a project from files originating from an old C-Munipack software.
   
The button (1) shows and hides the :guilabel:`Location` text box. The keyboard shortcut :kbd:`Ctrl+L` 
key combination does the same action.

In the :guilabel:`Location` text box (2) you can type a path to a file. If you don't type any path, the name
of the selected file will be displayed. You can also type the first letters of the name: it will be
auto-completed and a list of file names beginning with these letters will be displayed.

The path to the current folder is displayed at the top of the dialog (3). You can navigate along
this path by clicking on an element.

\(4) Here, you can access to your main folders and to your store devices.

\(5) Here, you can add bookmarks to folders, by using the :guilabel:`Add` or the :guilabel:`Add to Bookmarks` option 
you get by right-clicking a folder in the central panel, and also remove them.

The contents of the selected folder is displayed here (6). Change your current folder by double left
clicking on a folder in this panel. Right-clicking a folder name opens a context menu.

\(7) By clicking the Add button, you add the selected folder to bookmarks. By clicking the Remove,
you remove the selected bookmark from the list.

Click the "Apply" button (8) to continue; this dialog is followed by the :ref:`new-project-dialog`.
