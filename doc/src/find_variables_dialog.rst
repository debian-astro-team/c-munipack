.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: find_variables_dialog.rst,v 1.1 2015/07/06 08:19:48 dmotl Exp $

.. index::
   pair: Find variables; dialog

.. _find-variables-dialog:

Find variables (dialog)
=======================

The "Find variables" dialog is used to find objects that significantly change their
brightness.


Activating the dialog
---------------------

The dialog can be activated:

.. |varfind_icon| image:: icons/varfind.png

#. from the main menu: :menuselection:`Tools --> Find variables`.

#. from the main toolbar: |varfind_icon|


.. index::
   triple: finding; variable; stars

The dialog controls
-------------------

.. figure:: images/varfind_couts.png
   :align: center
   :alt: Find variables dialog
   
   The dialog for finding new variable stars
   
\(1) The mag-dev graph is presented here. Each point in the graph corresponds to an object. 
Left click on a mag-dev graph to select a star. The selected star is highlighted and marked 
as "var". Its position is shown on the chart (3) and its light curve is shown below (2).

\(3) Position of the variable star and the comparison star are marked on the
chart. You can also select an object by using the left button. The object is highlighted and
marked as "var". The corresponding point on the mag-dev curve is shown on the graph (1) and
it light curve is shown below (2).

When the dialog is opened, it works in the variable selection mode. A suitable comparison star
is determined automatically and kept fixed. When you click on the chart or mag-dev curve, you
change the object that represents the variable star. To change the comparison star, click on
the :guilabel:`Change comparison` button (5). When the button is pressed, you can change the
comparison star by selecting the object on the chart or mag-dev curve. Click the :guilabel:`Change variable`
button (4) to switch back to the variable star selection mode. 

Check the :guilabel:`Ensemble photometry` to allow selection of multiple comparison stars.
Click on the icon with the plus sign and click on an object on the chart or the mag-dev curve
to add the object to the set of comparison stars. Click on the icon with minus sign and
click on a comparison star on the chart to remove the object from the set.

\(6) You can save the current selection of comparion and variable star for further inspection.
Click the :guilabel:`Save as...` button. A new dialog appears. Enter a caption that the current
selection will be identified with. Using the selection field, you can browse the saved selections
and using the :guilabel:`Remove` button, you can remove the current item from the list.

\(7) Using this selection field, you can change the current aperture.

Click the :guilabel:`Save chart` button (8) to save the chart to a file.

\(9) The labels on the X axis can be switched between Julian date (JD) and date and time (UTC).

The magnitudes can be switched between two modes:

- Adaptive - the magnitude scale of the light curve changes when you change the selection of the variable 
  or the comparison star. The program sets the scale to correspond to the range of data points in the actual 
  light curve for the selected comparison and variable stars. If the range of data points is less than 
  0.05 mag, the scale is set to cover that range.

- Fixed - the magnitude scale of the light curve does not change when you change the selection of 
  the variable star. The program makes light curves for the chosen comparison star and all other objects
  and finds the maximum range D (difference between maximum and minimum magnitude of data points).
  If D is less than 0.05 mag, it is set to this value. When a user selects a variable
  star, the light curve is made. The vertical scale of the graph is set to cover the fixed range D
  and the light curve is shown at the center of the graph area.

Click the "Save light curve" button (10) to save the light curve to a file.

The menu bar that is attached to the dialog provides following functions:

- Menu File:
  
  - Save mag-dev curve - exports table with mag-dev curve to a text file.
  
  - Save chart - save the chart to a file.
  
  - Save light curve - save the light curve to a file.

  - Export varfile data - exports magnitudes of all stars for all source frames to a single file.
  
  - Export mag-dev curve as image - exports the graph with mag-dev curve to an image file
  
  - Export light curve as image - exports the graph with light curve to an image file
  
  - Rebuild - reloads the data from the project
  
  - Close - close this window
  
- Menu View:

  - Image only - show only CCD image (if available) and hide objects.
  
  - Chart only - show only objects and hide the CCD image.
  
  - Chart and image - show CCD image on the background and objects as an overlay.
  

.. seealso:: :ref:`finding-variables`, :ref:`choose-stars-dialog`, :ref:`light-curve-dialog`
