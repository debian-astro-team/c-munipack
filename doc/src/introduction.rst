.. C-Munipack - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: introduction.rst,v 1.5 2016/06/18 06:10:08 dmotl Exp $

.. _introduction:   
   

Introduction
============

The software package C-Munipack presents the complete system for reduction of
images carried out by a CCD or DSLR camera, oriented at observation of variable stars.
The specific programs of reduction process can be called from a command line or
via an intuitive graphical user interface.

The project is based on the previous Munipack package, the command-line programs have 
the same name, but to the contrary to Munipack, the C-Munipack software is coded in the C/C++ language 
and it contains several new functions and tools in addition. Assignment of the command-line
parameters and the configuration files is similar in the both projects, exceptions
are described in the project documentation.

The graphical user interface, which was build upon the portable GTK+ toolkit, should 
be familiar for Munidos users, but it takes full advantage of the graphical environment, and
therefore the interface is more comfortable and enables a user an improved control over
the reduction process in comparison with the original interface of Munidos.


Features and capabilities
-------------------------

The following list is a short overview of the key features that the C-Munipack software provides:

- Powerful graphical user interface that runs on the most up-to-date platforms and operating systems

- Conversion of CCD frames from several input formats (see below) into the standard FITS format

- Conversion of several RAW formats produced by DSLR cameras into the standard FITS format

- Color channel separation for color DSLR images

- Extensive set of the calibration tools: correction of observation time, bias-frame correction, 
  dark-frame correction, flat-flat correction and heliocentric correction
  
- Aperture photometry using a fully configurable set of apertures of different sizes

- Automatic finding corresponding stars on a set of frames

- Tracking of moving targets, such as minor Solar System bodies

- Easy-to-use making light curves for selected stars; either differential curves or instrumental 
  magnitudes for further post-processing

- Ensemble photometry using unlimited number of comparison stars
  
- Semiautomatic detection of new and unknown variable stars on a view field

- Computation of air mass coefficient

- Combining a set of bias, dark and flat frames into high quality master-bias, master-dark and 
  master-flat correction frames

- Exports light curve as: CSV file, ASCII file, MCV compatible format, AVE compatible format

- Exports light curve in the AAVSO Extended Format
    
- All data processing and scientific routines are placed in a public ANSI C library that
  can be used to create a tailor-made photometry software

- A complete set of user (shell) commands that can be used for automatic surveys

The following input formats are supported:

- The FITS format with many date and time formats

- The SBIG's ST-x compressed and uncompressed formats

- The OES Astro format

- Selected models of DSLR camera raw format: CRW, NEF [#]_


What can NOT the software do?
-----------------------------

- It doesn't control a camera or a telescope of any manufacturer.

- It doesn't compute celestial coordinates of objects (astrometry)


.. index::
   single: authors

Authors
-------

The project manager David Motl is also the author of the most part of the source codes. 
Some small pieces of the sources originate from the Munipack package, coded by Filip Hroch. 
The algorithms for aperture photometry originate from the Daophot software by P. B. Stetson. 
The Munifind algorithm originates from the Varfind tool coded by Lukas Kral. The code of the lossless 
JPEG decoder incorporated in the DSLR image reader originates from Dave Coffin's dcraw utility.
The matching algorithm using Phi-LogR space originates from Nick Kaiser's imcat software.

The package uses the following third party software:

- FITSIO library, written by Dr. William Pence, NASA

- EXPAT library, written by James Clark's

- GTK+ toolkit, developed by the The GTK+ Team

- Sphinx documentation generator

- WCSLIB library, writter by Mark Calabretta

- FFTPACK library, written by Paul Swarztrauber, NCAR

- The windows binaries are compiled using MSVC 2010 Express edition 

- The installer is built using Nullsoft Scriptable Install System (NSIS)


.. index::
   single: licence

License and copying
-------------------

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the Free 
Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the LICENSE file for more details.


.. index::
   pair: project; home page
   pair: version; latest

Where can I get the latest version?
-----------------------------------

The project is hosted by SourceForge. The latest version of the binaries, the source codes 
and the documentation is available on the following address:

http://c-munipack.sourceforge.net/


Supported platforms
-------------------

.. rubric:: MS Windows

The binaries provided on the website are compatible with MS Windows XP SP3 and later (32-bit or 64-bit).

The C-Munipack software is developed on MS Windows 7. The distributed binaries are compiled using
the MSVC 2010 Express edition.

.. note:: Since C-Munipack version 2.1, MS Windows 2000 are no longer supported.


.. rubric:: Other platforms

The software must be compiled from the source code; it should compile on any POSIX-compliant system,
provided that the following libraries/packages have been installed.

Minimum requirements:

- C/C++ compiler (such as gcc)

- cfitsio 3.x (http://heasarc.gsfc.nasa.gov/fitsio/)

- expat (http://expat.sourceforge.net/)

- GTK+ 2.x (http://www.gtk.org/)

Optional dependencies:

- gstreamer (http://gstreamer.freedesktop.org/)

- wcslib (http://www.atnf.csiro.au/people/mcalabre/WCS/)

The software build is tested on Debian 7.7 and 8.2. It is known to compile also on *Mac OS X*.


What's new in version 2.1
-------------------------

The main goal of this version was to provide 64-bit binaries for MS Windows platform along with the 32-bit
version. The installer package for MS Windows comes with the both versions and the installer chooses the binaries
that are appropriate for the host platform. The GKT+ library was updated to the version 2.24 and therefore 
Windows 2000 are no longer supported. 

The software can also read WCS coordinates, if they are stored in the source FITS files. I thought
that I could use them to get reliable matching for difficult cases, like glubular clusters, but 
this effort was fruitless so far. The only result from this effort is that if the source FITS files
comprises the WCS data, the frame preview window shows RA/Dec coordinates of objects.

There are also two new plot types - :ref:`CCD temperature <ccd-temperature-dialog>` plot which shows temperature variations over
time; such plot can be used to monitor performace of the camera cooling sub-system. The new :ref:`Object properties <object-properties-dialog>`
plot can be used to check for changes in de-focus (FWHM), weather conditions through a plot of instrumental 
magnitudes of a constant object or sky background level.

The version 2.1 also introduces a new matching tool that can track an object that moves with respect to the 
stars, such as minor Solar System bodies, the software is not designed to measure accurate position 
of the objects, it can be used to observe changes in the brightness in time.


.. rubric:: Footnotes

.. [#] Please read the :ref:`processing_raw_images` article before using the C-Munipack software
   to process snapshots from a DSLR camera.
