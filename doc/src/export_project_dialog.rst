.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: export_project_dialog.rst,v 1.1 2016/01/01 09:39:22 dmotl Exp $

.. index::
   pair: Export project; dialog
   
.. _export-project-dialog:

Export project (dialog)
=======================

The "Export project" dialog is used to specify a target location and name of the new project
that is created as a copy of the current project.

Since the original (source) files are not considered to a part of a project, the source 
files are not duplicated. A project references to source files by their path, the export
function does not change the paths to the source files.



Activating the dialog
---------------------

The dialog can be activated:

#. from the main menu: :menuselection:`Project --> Export`.


The basic dialog
----------------

.. figure:: images/saveproject_couts1.png
   :align: center
   :alt: Export project dialog
   
   The "Export project dialog" dialog (basic form)
   
In its basic form, as shown above, the dialog consists of a text box (1) to assign a name
to the file, and a drop-down list of bookmarks (2) to select a directory to save it in.

If the directory you want is not in the list of bookmarks, click on "Browse for other
folders" button (3) to expand the dialog to its full form.

Click on the "Save" button (4) to copy the project file and the acompanying files.


Browsing the directories
------------------------

.. figure:: images/saveproject_couts2.png
   :align: center
   :alt: Export chart dialog
   
   The "Export project dialog" dialog (with browser)
   
\(10) Here, you can access to your main folders and to your store devices.

\(11) The middle panel displays a list of the files in the current directory. Change your
current directory by double left-clicking on a directory in this panel. Select a file with
a single left click. You can then replace the file you have selected by clicking on the
Save button. Note that a double left click start the operation.

\(12) Above the middle panel, the path of the current directory is displayed. You can navigate
along this path by clicking on one of the buttons.

You can right click on the middle panel to access the "Show Hidden Files" command.

\(13) Enter the file name of the new image file here.

\(14) This drop-down list is only available in the basic form of the dialog. It provides
a list of bookmarks for selecting a directory in which to save your file. click on "Browse for
other folders" button (15) to shrink the dialog to its basic form.

\(16) Here, you can add bookmarks to folders, by using the "Add" or the "Add to Bookmarks"
option you get by right-clicking a folder in the central panel, and also remove them.

\(17) By clicking the Add button, you add the selected folder to bookmarks. By clicking the Remove,
you remove the selected bookmark from the list.

If you want to save the image into a folder that doesn't yet exist, you can create it
by clicking on "Create Folder" button (18) and following the instructions.

Click on the :guilabel:`Save` button (19) to copy the  project file and the acompanying files.


.. seealso:: :ref:`projects`, :ref:`new-project-dialog`, :ref:`open-project-dialog`
