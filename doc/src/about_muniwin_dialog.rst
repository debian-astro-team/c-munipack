.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: about_muniwin_dialog.rst,v 1.1 2015/07/06 08:19:48 dmotl Exp $

.. index:: 
   pair: About Muniwin; dialog

.. _about-muniwin-dialog:

About Muniwin (dialog)
======================

The "About Muniwin" dialog shows the version, the authors and the licence of the 
software.


Activating the dialog
---------------------

The dialog can be activated:

#. from the main menu: :menuselection:`Help --> About Muniwin`.


.. seealso:: :ref:`introduction`, :ref:`getting-started`
