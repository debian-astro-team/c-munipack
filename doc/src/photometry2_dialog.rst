.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: photometry_dialog.rst,v 1.1 2015/07/06 08:19:49 dmotl Exp $

.. index::
   pair: Photometry; dialog
   
.. _photometry2-dialog:

Photometry dialog (aligned frames)
==================================

The "Photometry" dialog is used to start the photometry process. Unlike the :ref:`Standard photometry dialog <photometry-dialog>`,
this dialog is shown when the project type is set to "Light curve (aligned frames)". In this mode, the input frames needs to
be aligned. For example, thay can be created by :ref:`merging frames <merge-frames-dialog>`. The user is expected to mark 
objects on interest manually. The software skips the object detection phase and continues with measuring brightness of
specified objects. Results are stored to separate files, which are called photometry files. The user does not need to run
the :ref:`matching process <frame-matching>`.


Activating the dialog
---------------------

You have to set the project type to "Light curve (aligned frames)" - see :ref:`here <project-settings-dialog>`.

The dialog can be activated:

.. |photometry_icon| image:: icons/photometry.png

#. from the main menu: :menuselection:`Reduce --> Photometry`.

#. from the main toolbar: |photometry_icon|


Running photometry
------------------

Usually, it is not neccessary to perform any frame calibration steps (bias, dark, flat and time correction) in this mode.
The calibration takes places before frame merging.

.. figure:: images/photometry2_couts.png
   :align: center
   :alt: Photometry dialog (aligned frames)
   
   The dialog for the photometry of aligned frames

\(1) Choose one of the source frames

\(2) Right click on an image to create an object on a place of mouse cursor. From the menu, select object type. Right click 
on an object to change its type or remove it. 

Click the "Execute" button (3) to start the operation.

- The photometry can be applied several times, the old photometry files are overwritten.

- No frame matching is needed, you can proceed to making a light curve. 


Known variable stars
--------------------

If the source frames contain World Coordinate System (WCS) data, the software can show positions
of known variables on the frame. You have to specify paths to the catalog files in the program
settings (see :ref:`environment-options-dialog`). For each catalog, the toobar contains one
check box that turns the catalog on and off. By clicking on the icon, a standard color selection
dialog appears.


.. seealso:: :ref:`light-curve-dialog`, :ref:`photometry-dialog`, :ref:`environment-options-dialog`
