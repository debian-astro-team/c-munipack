.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: plot_track_curve_dialog.rst,v 1.1 2016/01/01 09:39:22 dmotl Exp $

.. index::
   pair: Plot track curve; dialog
   
.. _make-track-curve-dialog:

Plot track curve (dialog)
=========================

The "Plot track curve" dialog is used to set up the initial parameters
for a new track curve. 


Activating the dialog
---------------------

The dialog can be activated:

#. from the main menu: :menuselection:`Plot --> Track curve`.

.. index::
   pair: track curve; plot

Making a plot of track curve
----------------------------

.. figure:: images/maketrackcurve_couts.png
   :align: center
   :alt: Plot track curve
   
   Plot track curve
   

\(1) It is possible to include all source files in the project or the files that are
currently selected in the table of input files. 

Click the button (2) to proceed.


.. seealso:: :ref:`track-curve-dialog`
