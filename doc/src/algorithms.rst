.. C-Munipack - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: algorithms.rst,v 1.2 2016/02/27 09:14:01 dmotl Exp $

.. _algorithms: 
   
Algorithms
==========

The algorithms involved in the C-Munipack software at the various stages of the data processing are described in 
detail in this section. Its purpose is to explain to a curious reader how the source frames and the photometry files
are handled, what the numbers and graphs mean and how exactly they are derived. I do not claim that I have invented 
the algorithms, whenever possible, proper references to the original materials are given.      
                                          
In the following text, reference is often made to "bad" and "overexposed" pixels. 
A pixel is regarded as a bad pixel if its value is equal to or less than the value of the first configurable
parameter "Minimum pixel value". Usually this parameter is set to zero, 
the lowest possible value that can be stored in a source image. An overexposed pixel
has a value equal to or greater than the value of the second configurable parameter "Maximum pixel value". 
This configuration parameter should be set to a value at which the analog-to-digital
converter saturates, but can be set to a lower value to exclude a higher part of 
the intensity range where response of a detector is not linear enough. In the correction 
algorithms, both bad and overexposed pixels are excluded from the computations.

.. toctree::
   :maxdepth: 1

   dark_frame_correction
   flat_frame_correction
   bias_frame_correction
   master_dark_frame_2
   master_flat_frame_2
   master_bias_frame_2
   star_detection
   photometry
   matching
   tracking_moving_target
   artificial_comparison
   varfind
   frame_merging_2
   heliocentric_correction
   air_mass_coefficient
   robust_mean
   
See also: [berry00]_ and [hampel05]_.
   