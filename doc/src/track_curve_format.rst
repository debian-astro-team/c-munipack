.. C-Munipack - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: track_curve_format.rst,v 1.2 2016/01/01 09:39:22 dmotl Exp $

.. index::
   pair: track curve; format
      
.. _track-curve-format:

Track curve files
=================

Track lists (dependency of frame shift on observation time) are usually used for
determining the value of periodic error of the telescope mount. A track curve table 
consists of observation time in Julian date form followed by shifts in the horizontal 
(X) and the vertical (Y) axes in pixels relative to the reference frame.

Output files are stored in the ASCII format; the end of the line is represented by 
``CR+LF`` in the DOS/MS Windows environment and by ``LF`` in the Unix/Linux 
environment. First line contains a list of column names separated by single space
character. Second line consists of additional information and has no special 
formatting, it must not start by number, though. 

On the following lines, the table values are stored. The values are separated by tab 
character or single space, rows are separated by the end-of-line character (see above). 
Parsers must ignore all additional white characters. Empty lines indicates, that the 
corresponding frame was not successfully processed and thus shift values could not be 
determined. See table :ref:`1 <table:trk_head>` for short description of columns.

.. _table:trk_head:

Table 1

======== ===========
Keyword  Description
======== ===========
JD       Geocentric Julian date of observation
OFFSETX  Relative shift in horizontal direction
OFFSETY  Relative shift in vertical direction
======== ===========
