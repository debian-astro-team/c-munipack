.. C-Munipack - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: getting_started.rst,v 1.3 2016/02/27 09:14:01 dmotl Exp $

.. _getting-started:   
   
Getting started
===============
    
The following text describes the basic procedure for processing an observation of 
a short-periodic eclipsing variable stars by means of the Muniwin software, the 
graphical user interface which comes with the C-Munipack software.

The package with the demonstration data used in the example is available on the project
`home page <http://c-munipack.sourceforge.net/>`_. Download and unzip the archive to 
an empty folder (e.g. :file:`~/c-munipack/sample`). The archive has the following structure:

.. object:: dark / 20s / masterdark.fts

   master-dark frame (20 seconds exposure)
   
.. object:: dark / 20s / raw / dark*.fts

   set of raw dark frames (20 seconds exposure)
   
.. object:: flat / v / masterflat.fts

   master-flat frame (optical filter V)
   
.. object:: flat / raw / flat-v*.fts

   set of raw flat frames (optical filter V)
   
.. object:: data / v / frame*.fts

   sample CCD frames (optical filter V)
   

General overview
----------------
   
The reduction of CCD observation can be described in general as a process that takes a time sequence 
of source frames of the same view field and a set of calibration frames and makes a table of brightness 
values of a selected object with respect to the time of observation, called a light curve. 

The process of reduction of a CCD observation in the Muniwin software consists of the several steps:

#) Creating a new project and populating it with input files (source CCD frames)

#) Calibration of the source frames 

#) Photometry and matching

#) Selection of stars

#) Selection of aperture


Start Muniwin
-------------

Launch the :program:`Muniwin` program - a way how to launch an application depends on your operating system 
and desktop environment. For example, on the MS Windows platform, the Muniwin program can be run from the 
menu :menuselection:`Start --> Programs --> C-Munipack 2.1` folder. On POSIX-compliant platforms, execute 
the command ``muniwin`` in a terminal window. A :ref:`main application window <main-window>` appears.

.. seealso:: :ref:`main-window`.


Create a new project
--------------------

In the Muniwin application, open the main menu, select item :menuselection:`Project --> New`. A new dialog appears.

.. figure:: images/newproject_couts.png
   :align: center
   :alt: "New project" dialog
   
   The dialog for making a new project.
   
Fill in a name that will be assigned to the project (1). Because the name of the file that keeps track of the data related
to the project, the project file, is derived from the name, some characters cannot appear in the project name, do not 
use: / \ ? % * : | " < and >. In our example, let us use the designation of the variable star ``GSC 2750 854``.

The field :guilabel:`Location` (2) shows a path to the directory where a new project will be created. Edit the path to 
change the location, you can also click the :guilabel:`Browse` button (3) to select a directory in a separate dialog.

The dialog also displays a list of available project types, called the profiles. The profile is used to specify initial 
settings for a new project. The installation package comes with a set of factory-provided profiles, which are listed under 
the section :guilabel:`Predefined profiles`. Later on, when you tune your own set of configuration parameters that you 
want to use repeatedly, you can create your own profiles, called :guilabel:`User-defined profiles`. See the section 
:ref:`Profiles` in the manual for instructions how to create and edit the user-defined profiles. 

In this introductory demonstration tour, we will make a light curve for an eclipsing binary. Please, select the 
item :guilabel:`Light curve` in the section :guilabel:`Predefined profiles` in the table (4). 

The dialog is closed and you should see the main application window again now. The table of input files shown there 
is empty.

.. figure:: images/main.png
   :align: center
   :alt: Main application window
   
   The main application window with the table of input files, now empty.

.. seealso:: :ref:`new-project-dialog`.
   
Selecting source frames
-----------------------
   
Now, we're going to populate the project with source files. Open the :guilabel:`Frames` menu and click on :guilabel:`Add frames from folder`. 
Go to the folder which contains the source CCD frames. The source CCD frames from the demonstration data are stored in the 
:file:`data/v` subfolder. You should see the list of files in the center navigation pane. This dialog allows you to add 
all frames from folders and subfolders. If you need to specify only a subset of files from a particular folder, you 
the :guilabel:`Add individual frames` choice.

.. figure:: images/addfolder.png
   :alt: "Add folder" dialog
   :align: center
   
   The dialog with the place selection box (left), the file selection box (middle)
   and the preview panel (right).
	
Push the :guilabel:`Add` button. If you want to process data taken in several nights at once, use the same procedure to add 
those files to the table. After inserting all the files you want to process, close the dialog by pushing the :guilabel:`Close` 
button.

.. figure:: images/inputfiles.png
   :alt: Main application window
   :align: center

   The main application window with the sample frames inserted.
   
.. seealso:: :ref:`add-frames-from-folder-dialog` and :ref:`add-individual-frames-dialog`.

   
Frame reduction
---------------

Reduction of CCD frames is a process that takes the source CCD frames, performs their conversion and calibration,
detects stars on each frame and measures their intensity and finally finds correlation (match) between objects 
that were found in the data set. The process of reduction prepares the data that are necessary for making a light 
curve or a variable star.

The reduction consists of the four steps - conversion, calibration, photometry and matching. Although they can be invoked
step-by-step manually, the preferred way is to use the :guilabel:`Express reduction` dialog that allows to perform 
these steps in a batch. Using the menu, activate the :menuselection:`Reduce --> Express reduction` item.
A new dialog appears. The dialog has several options aligned to the left, each of them relates to an optional 
step in the reduction process. 

.. figure:: images/express.png
   :alt: "Express reduction" dialog
   :align: center
   
   The dialog for setting parameters of the reduction process

Check the :guilabel:`Fetch/convert files`. 
In this step, the program makes copy of the source CCD frames. This is necessary, because the following 
calibration steps will modify them and we don't want the program to change our precious source data.

.. figure:: images/express_convert.png
   :alt: "Express reduction" dialog
   :align: center

A raw CCD frame consists of several components. By the calibration process, we get rid of those 
which affect the result of the photometry. In some literature, the calibration is depicted as the 
peeling of an onion. There are three major components which a raw frame consists of - the current 
made by incident light, current made thermal drift of electrons (so-called dark current) and 
constant bias level. In standard calibration scheme, which we will demonstrate here, the dark-frame 
correction subtracts the dark current and the also the bias. Because of the nature of the dark current, 
it is necessary to use a correction frame of the same exposure duration as source files and it must 
be carried out on the same CCD temperature, too. Thus, the properly working temperature regulation 
on your CCD camera is vital. Our sample data needs to perform the dark-frame correction, so check
the :guilabel:`Dark-frame correction` option. Click the :guilabel:`Browse` button and find the file 
with the dark frame. For the sample data, use the file :file:`dark / 20s / masterdark.fts`.

.. figure:: images/express_dark.png
   :alt: "Express reduction" dialog
   :align: center

Then, we have to compensate the spatial non-uniformity of a detector and whole optical
system. These non-uniformities are due to the fabrication process of a CCD chip and they are
also natural properties of all real optical components, lenses in particular. The flat-frame
correction uses a flat-frame to smooth them away. The flat-frame is a frame carried out 
while the telescope is pointed to uniformly luminous area. In practice, this condition is
very difficult to achieve, the clear sky before dusk is usually used instead. Our sample data 
needs to perform the flat-frame correction too, so check the :guilabel:`Flat-frame correction` 
option. Click the :guilabel:`Browse` button and find the file with the flat frame. For the sample data,
use the file :file:`flat / V / masterflat.fts`.

.. figure:: images/express_flat.png
   :alt: "Express reduction" dialog
   :align: center

The photometry is a process that detects stars on a CCD frame and measures their brightness. Unlike the previous 
steps, the result is saved to a special file, so-called the photometry file. There are a lot of parameters which 
affect the star detection and also the brightness computation. In this example, the default values work fine, 
but I would suggest you to become familiar with at least two of them - FWHM and Threshold - before
you start a real work. Check the :guilabel:`Photometry` option.

.. figure:: images/express_photometry.png
   :alt: "Express reduction" dialog
   :align: center

The previous command treated all source files independently. As a result of this, a star #1 
in one file is not necessarily the same as a star #1 in another file. The matching is a process 
which finds corresponding stars on source frames and assigns an unique identifier. Check the
:guilabel:`Matching` option.

It is necessary to select one frame from the set of source frames that all other frames are matched 
to, this frame is called a reference frame. In my experience, the frame with the greatest number of 
stars works the best. Back to our example, let's pick up the first one.

.. figure:: images/express_matching.png
   :alt: "Express reduction" dialog
   :align: center

In previous steps, we have configured parameters of the reduction process and we are ready to start
it. Click the :guilabel:`OK` button. During the execution a new window appears displaying the state 
of the process; all the information is also presented there. This window will be automatically 
closed after finishing the process. Wait for the process to finish. 

.. figure:: images/progress.png
   :alt: Progress dialog
   :align: center
   
   The dialog displayed during time demanding operations.

After finishing, the icon in the file table changes; the information about the time of observation, 
the length of the exposition and the used filter is filled in. In case some of the frames could not be 
processed successfully, the entry is be marked with a special icon and in the :guilabel:`Status` column 
the error message is indicated.

.. figure:: images/inputfiles3.png
   :alt: Main application window
   :align: center
   
   The main application window after the recution.
   
.. seealso:: :ref:`express-reduction-dialog`.

   
Making a light curve
--------------------
      
The sample data is the observation of an eclipsing binary star. For most of such observers,
the goal is to make a light curve. A light curve is usually represented as a table, which consists 
of at least two columns - time stamps expressed as Julian dates and differential instrumental 
brightness of a variable star in magnitudes. The magnitudes are called differential, because they
are differences between two stars - a variable star and a comparison star. A variable star is a 
star that is subject of brightness variation and a comparison star is supposed to be constant. 
To check that the comparison star is really constant, it's advisory to use one or two additional
stars, so-called check stars. They are supposed to be constant too and thus the difference between
any check star and the comparison star should be constant.

The Muniwin program is designed to guide you through the process of making a light curve. 
Open the :guilabel:`Make` menu and select the :guilabel:`Light curve` item. A new dialog appears, all 
fields in this dialog are optional, they allows you to include only subset of frames to the curve, 
apply the heliocentric correction or include the air mass coefficients to the output. Let all options 
unchecked and just confirm the dialog by the OK button.

.. figure:: images/makelightcurve.png
   :alt: Make light curve
   :align: center

   The dialog for making a light curve.
   
Another dialog appears. The next dialog allows you to pick up a variable star, a comparison star and one or more 
check stars. In this dialog, the reference frame is displayed. Find a variable star and click on 
it using the left mouse button (1). A context menu appears. Select the :guilabel:`Variable` 
item. The star is drawn in red color now and the label ``var`` is placed near to it. Using 
the same procedure, select one comparison star (2) and one or more check stars (3 and 4). I would recommend 
you to use two check stars. If you are using the sample data, use the stars according to the 
following screenshot. Confirm the selection by the :guilabel:`OK` button.

.. figure:: images/stars.png
   :alt: Choose stars
   :align: center

   The dialog for selection of a variable star (red), a comparison star (green)
   and check stars (blue).

In the next dialog, we have to choose the aperture. You can image the aperture as a virtual circular pinhole, 
placed on each star on a frame to measure its brightness. All pixels that are inside 
the pinhole are included in computation leaving out the background pixels. The best 
aperture should be big enough to include most of the star's light, on the other hand, 
the bigger aperture is used the more background is included and the more noisy the 
result is. Because of this, the photometry process computes the brightness of each 
star in a set of predefined apertures of radius in the range of 2 and 30 pixels.

The programs allows you to select an aperture in the following dialog, the program 
displays a graph of standard deviation of data for each aperture. The curve has U-like 
shape usually and the best aperture is in the minimum of the curve (5). For the sample data, 
select the aperture #2 and confirm the dialog by the :guilabel:`OK` button.

.. figure:: images/aperture.png
   :alt: Choose aperture
   :align: center

   The dialog for selection of an aperture.
   
Now the program has got enough information to make a light curve of the 
selected variable star. The light curve is presented in a new window. Please keep
in mind that the magnitudes displayed in the light curve are differential with 
respect to the selected comparison star.

.. figure:: images/lightcurve.png
   :alt: Light curve graph
   :align: center

   The dialog with a light curve graph.

.. seealso:: :ref:`light-curve`.
   
   
Exporting a light curve to a file
---------------------------------

The light curve can be exported to a file in several formats. 

.. seealso:: :ref:`aavso-format`
