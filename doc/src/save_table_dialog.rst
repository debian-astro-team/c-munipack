.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: save_table_dialog.rst,v 1.1 2015/07/06 08:19:49 dmotl Exp $

.. index::
   pair: Save table; dialog
   
.. _save-table-dialog:

Save table (dialog)
===================

The "Save table" dialog is used to enter a name and a directory for an exported table.


Activating the dialog
---------------------

The dialog can be activated from various dialogs that provides the data that can be
exported in tabular form, for example a light curve, table of objects, etc.

The basic dialog
----------------

.. figure:: images/savetable_couts1.png
   :align: center
   :alt: Save table dialog
   
   The "Save table dialog" dialog (basic form)
   
In its basic form, as shown above, the dialog consists of a text box (1) to assign a name
to the file, a drop-down list of bookmarks (2) to select a directory to save it in and a group 
of export options (3).

If the directory you want is not in the list of bookmarks, click on "Browse for other
folders" button (4) to expand the dialog to its full form.

Click on the "Save" button (5) to continue.


Browsing the directories
------------------------

.. figure:: images/savetable_couts2.png
   :align: center
   :alt: Save table dialog
   
   The "Save table dialog" dialog (with browser)
   
\(10) Here, you can access to your main folders and to your store devices.

\(11) The middle panel displays a list of the files in the current directory. Change your
current directory by double left-clicking on a directory in this panel. Select a file with
a single left click. You can then replace the file you have selected by clicking on the
Save button. Note that a double left click start the operation.

\(12) Above the middle panel, the path of the current directory is displayed. You can navigate
along this path by clicking on one of the buttons.

You can right click on the middle panel to access the "Show Hidden Files" command.

\(13) Enter the file name of the new image file here.

\(14) This drop-down list is only available in the basic form of the dialog. It provides
a list of bookmarks for selecting a directory in which to save your file. click on "Browse for
other folders" button (15) to shrink the dialog to its basic form.

\(16) Here, you can add bookmarks to folders, by using the "Add" or the "Add to Bookmarks"
option you get by right-clicking a folder in the central panel, and also remove them.

\(17) By clicking the Add button, you add the selected folder to bookmarks. By clicking the Remove,
you remove the selected bookmark from the list.

If you want to save the image into a folder that doesn't yet exist, you can create it
by clicking on "Create Folder" button (18) and following the instructions.

Click on the "Save" button (19) to continue.


Export options
--------------

The current version of the software supports the following export formats. Please note, that in some
situations not all format are available.

- C-Munipack - this is the default format for saving tables using the Muniwin software. It is also
  supported by some other astrophotometry programs, i.e. (original) Munipack, Munidos, etc. Basically,
  it is a text file with two-line header and fields separated by a single space.

- AVE compatible - a format for exporting light curves that can be processed by the AVE software. Basically,
  it is a text file without a headers and two columns - JD and differential magnitude of a variable.

- MCV compatible - a format for exporting light curves that can be processed by the MCV software. Basically,
  it is a text file without a headers and two columns - JD and instrumental absolute magnitude of a variable.

- Text - a common format for exporting tabular data. It is a text file, each row is stored on a separate line. 
  The file may include a header, which consists of a single line with column names. The optional header is immediately
  followed by table data, individual fields are separated by a single space. Such files can be processed by
  common plotting tools, i.e. gnuplot.

- CSV - a common format for exporting tabular data. It is a text file, each row is stored on a separate line. 
  The file may include a header, which consists of a single line with column names. The optional header is immediately
  followed by table data, unlike the Text format, the individual fields are separated by a single comma character.
  Such files can be easily imported into common spreadsheet processors, i.e. Apache OpenOffice Calc.

The selection of other export options that are available in a particular situation depends on the type of 
the data being exported as well as on the selected file format. The options are self-explanatory.
