.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: master_bias_frame_2.rst,v 1.2 2015/07/12 07:44:57 dmotl Exp $

.. index::
   pair: frame; master bias

.. _master-bias-frame-2:

Master bias frame
=================

A master bias frame is created from a set of bias frame. The robust mean algorithm (chapter 
:ref:`robust-mean`) is applied on a pixel-by-pixel basis, while leaving out bad and overexposed 
pixels. If there are enough source frames, the robustness of the algorithm ensures that an 
accidental artifact is ruled out and does not affect the mean value. If a pixel has an invalid value 
on all source frames, it is marked as a bad pixel on the resulting frame.
