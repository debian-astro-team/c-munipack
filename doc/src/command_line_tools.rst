.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: command_line_tools.rst,v 1.3 2016/02/27 09:14:01 dmotl Exp $

.. index::
   pair: command line; tools
   single: toolkit

.. _command-line-tools:
.. _toolkit:

Command line tools (toolkit)
============================

The C-Munipack toolkit provides a complete set of tools for reduction of images 
carried out by a CCD camera, aimed at the observation of variable stars. It provides a 
powerful set of commands called directly from a shell or a command-line interpreter. 
They are ready for use in custom-built shell scripts or batch files.

The toolkit has been developed as a part of the C-Munipack project. See the project's
home page for more information about the project and other interfaces.

.. toctree::
   :maxdepth: 1

   using_command_line_tools
   program_arguments
   using_configuration_files
   batch_processing
