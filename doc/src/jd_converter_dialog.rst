.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: jd_converter_dialog.rst,v 1.1 2015/07/06 08:19:48 dmotl Exp $

.. index::
   pair: JD converter; dialog
   
.. _jd-converter-dialog:

JD converter (dialog)
=====================

The "JD converter" dialog allows an user to convert a Julian date into
a Gregorian calendar date (common date and time) and vice versa.


Activating the dialog
---------------------

The dialog can be activated:

#. from the main menu: :menuselection:`Tools --> JD converter`.


.. index::
   pair: Gregorian calender; date

Converting Gregorian calendar date to Julian date
-------------------------------------------------

.. figure:: images/jdconv_couts1.png
   :align: center
   :alt: JD converter dialog
   
   Gregorian calendar date to Julian date
   
\(1) Select the direction of the conversion

Enter the Gregorian calendar date to the text box (2) in the following form: year-month-day hour:minute:second.
The time part is optional. Seconds can contain a fractional part. As you type in the data, corresponding
Julian date is updated in the third text box (3).

.. index::
   pair: Julian; date

Converting Julian date to Gregorian calendar date
-------------------------------------------------

.. figure:: images/jdconv_couts2.png
   :align: center
   :alt: JD converter dialog
   
   Julian date to Gregorian calender date
   
\(10) Select the direction of the conversion

Enter the Julian date to the text box (11). As you type in the value, corresponding
Gregorian calender date is updated in the third text box (12).


.. seealso:: :ref:`heliocentric-correction-dialog`, :ref:`air-mass-coefficient-dialog`
