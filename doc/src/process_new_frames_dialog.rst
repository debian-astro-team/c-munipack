.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: process_new_frames_dialog.rst,v 1.1 2015/07/06 08:19:49 dmotl Exp $

.. index::
   pair: Process new frames; window
   
.. _process-new-frames-dialog:

Process new frames (dialog)
===========================

The "Process new frames" dialog is used in the incremental processing. This feature allows
an user to reduce a small set of frames and when this set is enlarged by adding a new files into
the project, the same rules can be applied to the new files without much user interaction.


Activating the dialog
---------------------

The dialog can be activated:

.. |newfiles_icon| image:: icons/newfiles.png

#. from the main menu: :menuselection:`Reduce --> Process new files`.

#. from the main toolbar: |newfiles_icon|


The dialog controls
-------------------

.. figure:: images/newfiles_couts.png
   :align: center
   :alt: Process new files
   
   The "Process new files" dialog
   
Check the option (1) if the new files has been already added to the project.
If the second option is checked (2), the program will search for new frames in
a directory and add them to the project.

\(3) Enter path to the directory where the program shall search for new files.
You can use the ellipsis button to open a new dialog that allows you to browse the
files and folders.

\(4) Check this option to include all subdirectories when searching for new files.

You can also specify the constraints, which files must meet:

\(5) Check this option to include files whose names start with a certain string.
Enter the file name prefix to the text box. Files that begin with the string are processed.
The replacement characters "*" and "?" are not supported here.

\(6) Check this option to include frames which has been carried out with specified
color filter. Enter the filter name to the text box, search is not case sensitive.

\(7) Check this option to include frames with 'object name' field equal to specified
string. Enter the object name to the text box, search is not case sensitive.

\(8) Check this option to start a background process that periodically checks
the specified path and processes new files that turn up in the path.

Click the "OK" button (9) to start the operation.


.. seealso:: :ref:`express-reduction-dialog`, :ref:`bias-correction-dialog`, :ref:`dark-correction-dialog`, 
   :ref:`flat-correction-dialog`, :ref:`photometry-dialog`, :ref:`match-stars-dialog`
