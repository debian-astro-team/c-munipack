.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: master_dark_frame_2.rst,v 1.2 2015/07/12 07:44:57 dmotl Exp $

.. index::
   pair: frame; master dark

.. _master-dark-frame-2:

Master dark frame
=================

A master dark frame is created from a set of raw dark frames. The robust mean algorithm (see :ref:`robust-mean`) 
is applied on a pixel-by-pixel basis, while leaving out bad and overexposed 
pixels. If there are enough source frames, the robustness of the algorithm ensures that an accidental 
artifact (e.g. cosmic ray particle) is ruled out and does not affect the mean value. If a pixel has an invalid 
value on all source frames, it is marked as a bad pixel on the resulting frame.

The source dark frames must have the same exposure duration, otherwise the resulting frame
is not correct. 

A scalable dark frame is computed in the same way as a master dark frame. The program checks
that the bias-frame correction was applied to all source frames and sets the *SCALABLE* flag
in the file's header. The status of this flag is checked in the dark correction, see chapter 
:ref:`dark-frame-correction`.
