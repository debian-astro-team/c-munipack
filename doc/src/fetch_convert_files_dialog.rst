.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: fetch_convert_files_dialog.rst,v 1.1 2015/07/06 08:19:48 dmotl Exp $

.. index::
   pair: Fetch/convert files; dialog

.. _fetch-convert-files-dialog:

Fetch/convert files (dialog)
============================

The "Fetch/convert files" dialog is used to make a copy of the source files. At first look, this step might
seem to be redundant, but it allows an user to start again with a fresh copy whenever one makes a mistake during
the calibration of the source frames.


Activating the dialog
---------------------

The dialog can be activated:

.. |convert_icon| image:: icons/convert.png

#. from the main menu: :menuselection:`Reduce --> Fetch/convert files`.

#. from the main toolbar: |convert_icon|


.. index::
   pair: converting; files

Copying the source files
------------------------

.. figure:: images/convert_couts.png
   :align: center
   :alt: Fetch/convert files dialog
   
   The dialog for making a working copy of source files.

\(1) You can either make a fresh copy of all source files in the project or
make a copy of the files that are currently selected in the table of input files.

\(2) When you process DSLR images, select a transformation for RAW images
to gray scale. This option is ignored if the source frames are already in gray scale.

.. note: DSLR camera - color photometry
   Be careful when you are using color components from the RAW images - see :ref:`processing_raw_images`.

- Grayscale - the program sums all four pixels in Bayer mask. The resulting
  image have half the resolution of the RAW image and its range is four times higher
  than the range of the original picture.
  
- Red / blue - the program takes only red and blue pixels respectively.
  The resulting image have half the resolution of the RAW image and its range is
  the same as the range of the original picture.
  
- Green - the program takes only two green pixels on diagonal, sums them
  and the result divides by two. The resulting image have half the resolution
  of the RAW image and its range is the same as the range of the original picture.
  
To start the action, click the "Execute" button (3).


.. seealso:: :ref:`main-window`, :ref:`express-reduction-dialog`
