.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: choose_stars_dialog.rst,v 1.1 2015/07/06 08:19:48 dmotl Exp $

.. index::
   pair: Choose stars; dialog
   
.. _choose-stars-dialog:

Choose stars (dialog)
=====================

The "Choose stars" dialog is used to select variable, comparison and check stars. This information
is required to make a light curve. One can also assign tags to objects, e.g. GSC numbers, these tags
are printed on displayed and exported charts.

.. index::
   pair: Selecting; stars

Selecting the stars
-------------------

.. figure:: images/choosestars_couts.png
   :align: center
   :alt: Choose stars dialog
   
   The dialog for selecting a variable star, a comparison star and check stars

The image shows a reference frame or a catalog file (1), it depends what kind of file
you have chosen in the Match stars dialog.

\(2) It is possible to switch the display mode between an image, a chart or both of them.

When you place a mouse cursor over a detected object, it is highlighted with a cross hair (3).
Open a context menu with a single click, select an option from a menu to select or deselect it.

\(3) Click the :guilabel:`New` button to clear the current selection. 

Click the :guilabel:`OK` button to confirm the selection.


Saving the selection of objects
-------------------------------

If you want to save the current selection of objects for further reference, click the :guilabel:`Save as...`
button (10) on the toolbar. A new dialog appears. Enter a name of the selection, e.g. name of the variable
star. This feature is useful in a case there are :ref:`multiple variable stars <multiple-variable-stars>` 
on one star field.

.. figure:: images/choosestars_couts3.png
   :align: center
   :alt: Choose stars dialog
   
   The dialog for selecting a variable star, a comparison star and check stars

To restore the selection that was saved, use the selection control (11) on the toolbar. You can 
remove the selection using the :guilabel:`Remove` button (12).


.. index::
   single: tags
   
.. _tags:

Tags
----

Any object on the reference frame can have user-defined short string, so called `tag`. Tags are
used to provide additional identification to objects on an exported charts. For example, when 
you assign a tag with GCVS name to your variable star; the caption 'var' and the GCVS name will
be printed on the chart next to the star. Please note, that objects that are not selected can 
have tags, too.

Tags are stored to a :ref:`catalog file <catalog-files>` and they are loaded to the project when 
you use the catalog file as a reference frame in the :ref:`matching <match-stars-dialog>` step.

.. figure:: images/choosestars_couts2.png
   :align: center
   :alt: Choose stars dialog (with tags)
   
   The dialog for selecting stars, with tags

To assign a tag to an object or edit existing tag, place a mouse cursor over a detected object, 
it is highlighted with a cross hair (3). Open a context menu with a single click, and select
:menuselection:`Edit tag`. A new dialog appears. Enter a text (single line) and confirm the 
dialog. The text should appear next to the selected star.

Tags can be removed from the same context menu.


Known variable stars
--------------------

If the source frames contain World Coordinate System (WCS) data, the software can show positions
of known variables on the frame. You have to specify paths to the catalog files in the program
settings (see :ref:`environment-options-dialog`). For each catalog, the toobar contains one
check box that turns the catalog on and off. By clicking on the icon, a standard color selection
dialog appears.

.. seealso:: :ref:`match-stars-dialog`, :ref:`choose-aperture-dialog`, :ref:`light-curve-dialog`, :ref:`environment-options-dialog`.
