.. C-Munipack - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: air_mass_coefficient.rst,v 1.3 2016/01/01 09:39:22 dmotl Exp $

.. _air-mass-coefficient: 

.. index::
   pair: air mass; coefficient
   
Air mass coefficient
====================

Air mass is the length of an optical path that light from an observed object
travels through the Earth's atmosphere. Along this path, the light is attenuated
by scattering and absorption. The greater is the air mass, the greater is the
attenuation. This is illustrated in Figure 1, objects near the horizon appear 
less bright than when they are at the zenith.

.. figure:: images/airmass.png
   :align: center
   :alt: Air mass

   Air mass. $z$ -- zenith angle of the object. By definition, the air mass 
   coefficient is equal to 1 at zenith ($z=0$).

The air mass coefficient *X* is the air mass relative to that at the zenith. So, its 
value is 1 when the object is at zenith, and increases as the zenith angle *z* grows, 
reaching approximately 38 at the horizon.  

.. rubric:: Versions 1.2.21 and older
                                        
In versions 1.2.21 and older, the air mass coefficient *X* was computed using 
the approximation published by Hardie in 1962 (see [hardie62]_). This gives usable
results for zenith angles up to about 85 degrees. For greater zenith angles, the
program returned a negative value, to indicate that air mass cannot be computed.

.. rubric:: Versions 1.2.22 and newer

Since version 1.2.22, the air mass coefficient *X* is computed using the approximation 
published by Pickering in 2002 (see [pickering02]_). The formula :eq:`airmass` works 
well even for zenith angles up to 90 degrees. The program stores a negative value to indicate 
a situation when an object is below a horizon. 

.. math::
   :label: airmass
   
	X = \frac{1} { \sin (h + {244}/(165+47 h^{1.1}) ) }

where *h* is apparent altitude :math:`(90^\circ - z)` in degrees.
