.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: catalog_file_window.rst,v 1.1 2015/07/06 08:19:48 dmotl Exp $

.. index::
   pair: Catalog file; window

.. _catalog-file-window:

Catalog file (window)
=====================

This preview window is used to display the content of a catalog file.


Activating the dialog
---------------------

The dialog can be activated:

#. from the main menu: :menuselection:`Tools --> Open file`.

When a file is opened, the program checks its content and decides which kind of
preview window will be activated. Each file is presented in a separate window.


Dialog controls
---------------

.. figure:: images/catfile_couts1.png
   :align: center
   :alt: Catalog file preview
   
   Preview window for catalog files
   
\(1) The chart is displayed in the preview area. It is possible to switch between
two rendering modes - displying file as a chart or as a table of objects.

\(2) When you place a cursor over an object, the position and brightness (if available)
is displayed in the status bar.

\(3) You can zoom the preview in and out using the icons on the toolbar.

\(4) If the catalog file was saved with multiple object selections, use this control to 
to review the other selections.

The local menu bar provides following functions:

- Menu File:

  - Open - open another file, this is an equivalent to selection :menuselection:`Tools --> Open file`
    from the main window.
  
  - Save as - save the catalogue file to a file.
  
  - Export - depending on the current display mode:
  
    - If a chart is shown, the function export the chart or image to a file in the PNG or JPEG format. 
      The save dialog provides several options, that allows adjusting the size of the resulting image.
    
    - If a table is shown, the function export the table to a file in the CSV or TEXT format.
      The save dialog provides several options, that allows adjusting the content of the file.
  
  - Show properties - display the further details about the file. Full header preview
    is available in separate window.
    
  - Close - close this window
  
- Menu Edit:

  - Edit properties - edit descriptive information stored in the catalog file (observer, location, user remarks, ...)
  
  - Change selected stars - enter editing mode for objects (variable, comparison and check stars)
  
- Menu View:

  - Chart - show the objects as a chart
  
  - Table - show the table of objects

  - Rulers - turn on/off the rules that are shown on the top and left side of the preview area
 
- Menu Tools:

  - Object inspector - if this tool is activated, left-click an object to display
	its properties. The information is presented in the right part of the dialog.
	
.. index::
   pair: Object inspector; tool

   
Object inspector tool
---------------------

The Object inspector tool is a tool which displays that the program registers about
an object on a frame. The main purpose of this tool is for testing and debugging. Unlike the
Quick Photometry, the Object Inspector shows results, that has been obtained during the "full"
photometry. Because of this, calibration and photometry must be performed first. The results
are more relevant.

To activate the tool:

#. From the local menu, select :menuselection:`Tools --> Object inspector`.
   A new panel on the right side of the preview window appears.
   
.. figure:: images/phtfile_couts2.png
   :align: center
   :alt: Object Inspector tool
   
   Object Inspector tool
   
Using the left mouse button, click on the image (1), to select an object. The object is
highlighted and its properties are displayed in the right panel (2).

The following object parameters are presented:

- Object # - object's ordinal number on the current frame.

- Reference ID - object's ordinal number on a reference frame.

- Center - coordinates of the object's centroid

- Brightness - instrumental brightness of the object in magnitudes.

- Error - error estimation of brightness in magnitudes.

Several dimensions are presented in the image area (3):

- Two blue circles show the annulus which is used to estimate the background properties.

- A green circle shows the size of the aperture.

\(4) Here, you can change the current aperture.


Editing object selections
-------------------------

How to edit object selections (variable, comparison and check stars) in an existing
catalog file.

In the preview window use the local menu :menuselection:`Edit --> Change selected stars`.
A stripe which indicates that you the editing mode has been activated appears between 
the preview area and the toolbar \(1). 

Now, you can change the roles of objects in the same way as in the dialog :ref:`Choose stars <choose-stars-dialog>`. 
Click on an object \(2) in the preview area. A context menu appears. Select an item to change the role of the object.

\(3) Using the toolbar controls, you can switch between existing selections and create, 
update and remove them.

.. figure:: images/catfile_couts2.png
   :align: center
   :alt: Catalog file preview
   
   Preview window for catalog files in edit mode

When you have finished the changes, click the :guilabel:`Apply` button on the 
stripe \(4). If you want to discard changes, click the :guilabel:`Discard` button \(5).
By this action you leave the editing mode.

.. seealso:: :ref:`open-file-dialog`
