/**************************************************************

ctxhelp.h (C-Munipack project)
Context help identifiers
Copyright (C) 2008 David Motl, dmotl@volny.cz

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

**************************************************************/

#ifndef CMPACK_CTXHELP_H
#define CMPACK_CTXHELP_H

/* Context help identifiers */
#define IDH_ADD_FILES			1000	/* add-individual-frames-dialog */
#define IDH_ADD_FOLDER			1001	/* add-frames-from-folder-dialog */
#define IDH_CONVERT_FILES		1002	/* fetch-convert-files-dialog */
#define IDH_BIAS_CORRECTION		1003	/* bias-correction-dialog */
#define IDH_DARK_CORRECTION		1004	/* dark-correction-dialog */
#define IDH_FLAT_CORRECTION		1005	/* flat-correction-dialog */
#define IDH_TIME_CORRECTION		1006	/* time-correction-dialog */
#define IDH_ABOUT				1007	/* about-muniwin-dialog */
#define IDH_AIR_MASS			1008	/* air-mass-coefficient-dialog */
#define IDH_AIR_MASS_CURVE		1009	/* air-mass-curve-dialog */
#define IDH_CHOOSE_APERTURE		1010	/* choose-aperture-dialog */
#define IDH_IMAGE_FILE_WINDOW	1011	/* image-file-window */
#define IDH_CATALOG_FILE_WINDOW	1012	/* catalog-file-window */
#define IDH_CHART				1013	/* chart-dialog */
#define IDH_SAVE_CHART			1014	/* save-chart-dialog */
#define IDH_PROJECT				1015	/* project-settings-dialog */
#define IDH_EXPRESS_REDUCTION	1016	/* express-reduction-dialog */
#define IDH_FRAME_PREVIEW		1017	/* frame-preview-window */
#define IDH_HELIOC_CORRECTION	1018	/* heliocentric-correction-dialog */
#define IDH_JD_CONVERSION		1019	/* jd-converter-dialog */
#define IDH_LIGHT_CURVE			1020	/* light-curve-dialog */
#define IDH_MAIN_WINDOW			1021	/* main-window */
#define IDH_MAKE_CATALOG_FILE	1022	/* make-catalog-file-dialog */
#define IDH_MAKE_MASTER_BIAS	1023	/* master-bias-frame-dialog */
#define IDH_MAKE_MASTER_DARK	1024	/* master-dark-frame-dialog */
#define IDH_MAKE_MASTER_FLAT	1025	/* master-flat-frame-dialog */
#define IDH_MATCH_STARS			1026	/* match-stars-dialog */
#define IDH_MERGE_FRAMES		1027	/* merge-frames-dialog */
#define IDH_MESSAGE_LOG			1028	/* message-log-dialog */
#define IDH_PROCESS_NEW_FRAMES	1029	/* process-new-frames-dialog */
#define IDH_OBJECT_COORDINATES	1030	/* object-coordinates-dialog */
#define IDH_PHT_FILE_WINDOW		1031	/* photometry-file-window */
#define IDH_GRAPH_WINDOW		1032	/* graph-window */
#define IDH_OBSERVER_COORDS		1033	/* observer-coordinates-dialog */
#define IDH_OPEN_FILE			1034	/* open-file-dialog */
#define IDH_PHOTOMETRY			1035	/* photometry-dialog */
#define IDH_CHOOSE_STARS		1036	/* choose-stars-dialog */
#define IDH_THUMBNAILS			1037	/* thumbnails-dialog */
#define IDH_TRACK_CURVE			1038	/* track-curve-dialog */
#define IDH_FIND_VARIABLES		1039	/* find-variables-dialog */
#define IDH_ENVIRONMENT			1040	/* environment-options-dialog */
#define IDH_NEW_PROJECT			1041	/* new-project-dialog */
#define IDH_LOAD_PROFILE		1042	/* load-profile-dialog */
#define IDH_IMPORT_PROJECT		1043	/* import-project-dialog */
#define IDH_MAKE_AIR_MASS_CURVE	1044	/* make-air-mass-curve-dialog */
#define IDH_MAKE_LIGHT_CURVE	1045	/* make-light-curve-dialog */
#define IDH_MAKE_TRACK_CURVE	1046	/* make-track-curve-dialog */
#define IDH_MAKE_FIND_VARIABLES	1047	/* make-find-variables-dialog */
#define IDH_EXPORT_PROJECT		1048	/* export-project-dialog */
#define IDH_OPEN_PROJECT		1049	/* open-project-dialog */
#define IDH_SAVE_GRAPH			1050	/* save-graph-dialog */
#define IDH_SAVE_TABLE			1051	/* save-table-dialog */
#define IDH_PROFILES			1052	/* edit-profiles-dialog */
#define IDH_IMPORT_PROFILE		1053	/* import-profile-dialog */
#define IDH_SAVE_PROFILE		1054	/* save-profile-dialog */
#define IDH_AAVSO_EXPORT		1055	/* aavso-format */
#define IDH_MAKE_TEMP_CURVE		1056	/* make-ccd-temperature-dialog */
#define IDH_PLOT_TEMP_CURVE		1057	/* ccd-temperature-dialog */
#define IDH_MAKE_OBJ_PROPS		1058	/* make-object-properties-dialog */
#define IDH_PLOT_OBJ_PROPS		1059	/* object-properties-dialog */
#define IDH_BAAVSS_EXPORT		1060	/* baavss-format */
#define IDH_PHOTOMETRY_2		1061	/* photometry2-dialog */

#endif
