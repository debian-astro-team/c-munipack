.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: master_flat_frame_2.rst,v 1.2 2015/07/12 07:44:57 dmotl Exp $

.. index::
   pair: frame; master flat

.. _master-flat-frame-2:

Master flat frame
=================

A master flat frame is created from a set of raw flat frames. Unlike the dark frame, we normalize
the source frames first to ensure equal mean value for all of them. This is correct, because the mean level
of a flat frame depends on the intensity of the light source that was used to illuminate the camera.
What we are seeking here is a ratio of sensitivity of a detector pixel to the mean sensitivity of 
all pixels. It would be natural to normalize the flat frames to a mean value of 1.0, but if
we attempted to store such a normalized frame to a file storing each pixel value as an integer
number, the quantization noise would destroy all our efforts. Therefore, we have to choose a value 
which is high enough the reduce the quantization noise. On the other hand, the allowed range of 
values that can be stored to a file is limited and depends on the number of bits reserved for each 
pixel. If we put the mean intensity too high, some of the pixels would be out of range. The value
which the flat frames is normalized to is a configurable parameter, its default value of 10,000 
provides a trade-off between quantization noise and the limited range, and it is appropriate
in the vast majority of cases.

The steps are as follows: First, take each of the source frames, apply the robust mean algorithm
(see chapter :ref:`robust-mean`) to compute its mean intensity, then normalize the frame by dividing 
each pixel value by the mean value and multiplying by the required mean value of 10,000. Next, apply 
the robust mean algorithm to normalized frames on pixel-by-pixel basis, while leaving out bad and 
overexposed pixels. If a pixel has an invalid value on all source frames, it is marked as a bad pixel 
on the resulting frame.
