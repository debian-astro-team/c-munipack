.. C-Munipack - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: catalog_files_format.rst,v 1.2 2015/07/12 07:44:57 dmotl Exp $

.. index::
   pair: catalog; files
      
.. _catalog-files-format:

Catalog files
=============

Using catalog files can speed up your work, in case you often observe
particular star field, for example long-term monitoring of a variable star. 
The structure of the data stored in a file is very similar to a photometry file, 
but it contains the selection of stars (variable star, comparison star, etc.). 
When a catalog file is used instead of a reference file in matching phase of 
reduction, the selection is automatically restored from that file.

Catalog files are saved as XML documents. The EXPAT library has been used 
for reading the files in XML based formats, see the :ref:`References` section 
for details.

Description of elements
-----------------------

cat_file
^^^^^^^^

The root element of XML document 

.. rubric:: Attributes

None 
 
.. rubric:: Model

(info?, selection*, taglist?, stars?)

.. rubric:: Description

The *cat_file* element is a root element of the document.
The document consists of meta-data (file header), arbitrary number of object selections, 
a table of object tags and a table of objects.

info
^^^^

The file header 


.. rubric:: Attributes

None 
.. rubric:: Model

(object | ra2000 | dec2000 | observer | observatory | telescope | camera | filter | fov | orientation | comment)*

.. rubric:: Description

The *info* element is a container for catalog file meta-data.
The parameters are given as child elements, parameter values are stored in 
child elements' content. The order of child elements is not significant. The description 
of parameters is given in :ref:`Table 1 <table:cat_head>`.

.. _table:cat_head:

Table 1

========================= ======== ========================================
Element name              Presence Description
========================= ======== ========================================
object			  optional Object's designation
ra2000			  optional Object's right ascension
dec2000			  optional Object's declination
observer		  optional Observer's name 
observatory		  optional Observer's location designation
longitude		  optional Observer's longitude
latitude		  optional Observer's latitude
telescope		  optional Designation of the telescope
camera			  optional Camera manufacturer and model
filter			  optional Optical filter
fov			  optional View field size
rotation		  optional View field orientation
comment			  optional User comments
========================= ======== ========================================
        
selection
^^^^^^^^^

The table of selected objects

.. rubric:: Attributes

name

.. rubric:: Model

(select*)

.. rubric:: Description

The *selection* element is a container for elements
that specify an object role (variable, comparison and check stars) to make a light curve. 
Since the version 2.0.11, the catalog file is capable of including an arbitrary number
of object selections - the main purpose of this is to support multiple variable stars
on a single star field. 

Each *select* element define one object and its role. If there are more roles for a single object, the
result is undefined. The optional attribute *name* specify a caption that is
used to provide a list of available object selections. If it is not present, it is
recommended to use the file name of the catalog file (without extension) instead.


select
^^^^^^

A selected star and its role 

.. rubric:: Attributes

id, label

.. rubric:: Model

Empty

.. rubric:: Description

The *select* element assigns a role and caption
to one star. The mandatory attribute *id* specify object's identifier, it must
be one of object identifiers that appears in the table of star. The attribute 
*label* is a description of the object. It starts with a prefix, which must 
be either 'var' for a variable star, 'comp' for a comparison star and 'chk'
for a check star. When there are more stars of the same role, the ordinal number
is appended to the prefix to give them unique labels.

selection
^^^^^^^^^

A list of tabs

.. rubric:: Attributes

None

.. rubric:: Model

(tag*)

.. rubric:: Description

The *taglist* element is a container for elements
that specify user-defined strings (tags) for objects. Each *tag* element 
define a tag for a single object. If there are more tags for a single object, the
result is undefined.

tag
^^^

A star's tag

.. rubric:: Attributes

id, value

.. rubric:: Model

Empty

.. rubric:: Description

The *tag* element assigns a tag to one star. 
The mandatory attribute *id* specify object's identifier, it must
be one of object identifiers that appears in the table of star. The attribute 
*value* is a string assigned to it.


stars
^^^^^

A table of all stars on the frame

.. rubric:: Attributes

width, height 

.. rubric:: Model

(s*)

.. rubric:: Description

The *stars* element is a container for stars, each
child element *s* specify a single star. The mandatory attributes
*width* and *height* define a frame size in pixels. If there are multiple
definitions with the same identifier, the result is undefined.


s
^

The position and brightness of a star 

.. rubric:: Attributes

id, x, y, m, e 

.. rubric:: Model

Empty

.. rubric:: Description

The *s* element defines an object. The mandatory attribute 
*id* is the object's identification, it must be unique within the file. The mandatory 
attributes *x* and *y* stores the position of the object within the frame, the 
coordinates are specified in pixels, the origin is the left top corner of the frame. The 
attributes *m* and *e* specifies raw instrumental brightness in magnitudes and 
its error estimation.
 