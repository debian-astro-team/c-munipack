.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: baavss_format.rst,v 1.1 2016/06/18 06:10:08 dmotl Exp $

.. index:: BAAVSS Extended Format
   
.. _baavss-format:

Export light curve in BAAVSO format
===================================

TBD.