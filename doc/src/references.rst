.. C-Munipack - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: references.rst,v 1.1 2015/07/12 07:44:57 dmotl Exp $

.. _references:   
   
References
==========

.. [berry00] Berry, Richard and Burnell, James. The Handbook of Astronomical Image Processing. Willmann-Bell, 2000. 
.. [hampel86] Hampel, F. R., Ronchetti, E. M., Rousseeuw, P. J. and Stahel, W. A. Robust Statistics. The Approach Based on Influence Functions. John Wiley and Sons, New York, 1986. 
.. [hampel05] Hampel, F. R., Ronchetti E. M., Rousseeuw P. J. and Stahel, W.A. Robust Statistics: The Approach Based on Influence Functions (Wiley Series in Probability and Statistics). John Wiley & Sons, Inc., New York, revised edition, 2005. {doi:10.1002/9781118186435}. 
.. [hardie62] Hardie, R. H. Photoelectric reductions. Astronomical Techniques, 1962. 
.. [huber81] Huber P. J. Robust Statistics. Wiley, 1981. 
.. [andronov04] Andronov I.L. and Baklanov A.V. Algorithm of the artificial comparison star for the ccd photometry. Astronomical School's Report, 5(1-2):264-272, 2004. 
.. [pickering02] Pickering, K. A. The southern limits of the ancient star catalog. The International Journal of Scientific History, 12:3-27, 9 2002. 
.. [barbera11] Barbera, R. and Iparraguirre, J. AVE. 12 2011. http://www.astrogea.org/soft/ave/aveint.htm 
.. [stetson89] Stetson, P. B. V Advanced School of Astrophysics, 1989. 
.. [stetson11] Stetson, P. B. MUD/9 - DAOPHOT II User's Manual. 09 2011. http://www.star.bris.ac.uk/~mbt/daophot/ 
.. [groth86] Groth, E. J. A pattern-matching algorithm for two-dimensional coordinate lists. \aj, 91:1244-1248, May 1986. (doi: 10.1086/114099). 
.. [valdes95] Valdes, F. G., Campusano, L. E., Velasquez J. D., Stetson P.B. Focas automatic catalog matching algorithms. Publications of the Astronomical Society of the Pacific, 107(717):1119-1128, November 1995. http://www.jstor.org/discover/10.2307/40680657?uid=3737784&uid=2&uid=4&sid=21102685587487, (doi: 10.1086/133667). 
.. [flandern79] van Flandern, T. C. and Pulkkinen, K. F. Low-precision formulae for planetary positions. \apjs, 41:391-411, November 1979. (doi: 10.1086/190623). 
  