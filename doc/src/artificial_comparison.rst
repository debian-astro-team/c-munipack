.. C-Munipack - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: artificial_comparison.rst,v 1.1 2015/07/06 08:19:48 dmotl Exp $

.. _artificial-comparison: 

.. index:: 
   triple: artificial; comparison; star
   
Artificial comparison star
==========================

The artificial comparison star is used to enhance a quality of a light curve
by utilizing multiple stars that are used to compute differential magnitudes
of a variable star. 

A new virtual comparison star is created from a set of stars and its brightness
in magnitudes and error estimation are derived for each frame. Then, a normal 
course of light curve construction is followed. The brightness of an artificial
star is computed by averaging the intensities (not magnitudes) of stars from
a set. The same set of stars must be incorporated in the average on each frame,
otherwise the results were incorrect. Therefore, if there is missing measurement 
for one of stars, it is not possible to compute the brightness of a comparison 
star for this frame.

.. rubric:: Deriving brightness of the artificial comparison star

First, we use inverse Pogson's law to convert brightness in magnitudes *m_i* into
intensity *I_i* for each star *i* from the set. 

.. math::
   :label: Pogsons_law
   
	\frac{I_i}{I_0} = 10^{-0.4\,m_i}

Next, intensities are combined by means of arithmetic mean. Please note, that
the factor *1/N* is used to keep the brightness in the same range as the source data.

.. math::
   :label: intensity
   
	\frac{I}{I_0} = \frac{1}{N}\,\sum{\frac{I_i}{I_0}}

Then, the intensity is converted back to magnitudes:

.. math::
   :label: magnitude
   
	m = -2.5 \log_{10}\left(\frac{I}{I_0}\right)
  
.. rubric:: Estimating the measurement error

The inverse equation :eq:`Pogson_law_sigma` can be used to transform
the error estimation from magnitudes to intensity unit. Supposing that noise
sources are independent, we can compute the variance of *I* as:

.. math::
   :label: sigma_i

	\sigma_{I}^2 = (\frac{1}{N})^2\,\sum{\sigma_{I_i}^2}

The equation :eq:`Pogson_law_sigma` is used to convert the resulting 
variance back to magnitudes. Putting all three formulas together we get
the following formula for the error estimation of the resulting brightness of
a artificial comparison star in magnitudes:

.. math::
   :label: sigma_m

	\sigma_{m} = \frac{\sqrt{\sum{(\frac{I_i}{I_0}\sigma_{m_i})^2}}}{\sum{\frac{I_i}{I_0}}}
