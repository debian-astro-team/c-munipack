.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: time_correction_dialog.rst,v 1.1 2015/07/06 08:19:49 dmotl Exp $

.. index::
   pair: Time correction; dialog
   
.. _time-correction-dialog:

Time correction (dialog)
========================

The "Time correction" dialog is used to fix the date and time observation of the source files
in case the computer time was misaligned to the real time during the observation.


Activating the dialog
---------------------

The dialog can be activated:

.. |timecorr_icon| image:: icons/timecorr.png

#. from the main menu: :menuselection:`Reduce --> Time correction`.

#. from the main toolbar: |timecorr_icon|


Specifying the time shift
-------------------------

.. figure:: images/timecorr_couts.png
   :align: center
   :alt: Time correction dialog
   
   The dialog for specifying the time shift

There are three possibilities how to specify the time shift: constant number of seconds, days
or difference between two dates.

To change time of observation by a constant number of second, check the first option (1)
and enter the number of seconds to the text box (2). The value should be always positive, here (3)
you can specify if the time shall be shifted either to the future or to the past.

To change date of observation by a constant number of days, check on the second option (4)
and enter the number of seconds to the text box (5). The value should be always positive, here (3)
you can specify if the date shall be shifted either to the future or to the past.

The third option (6) provides more elaborate control over the time shift. You are expected
to specify a pair of dates. The first one is the original (bad) date (7), the second one is the new
(correct) date (8). The difference between these two dates gives a time shift, that will be applied
to all frames. It is not required that the specified original date corresponds to a source frame.

\(9) It is possible to apply the operation on all source files in the project or
on the files that are currently selected in the table of input files. By means of this option,
it is possible to apply a correction to a subset of source files only.

.. index::
   pair: time correction; applying

Applying the time correction
----------------------------

Click the "Execute" button (10) to start the operation. The time correction is applied to a working
copy of source files.

- A working copy of source frame must be made before the time correction.

- The time correction can be applied several times to a frame, effect of this function is cumulative.

- The time correction must be applied before the photometry.

If you make a mistake, you can revoke the step by making a fresh copy of source frames
by means of the "Fetch/convert files" function.


.. seealso:: :ref:`main-window`, :ref:`express-reduction-dialog`
