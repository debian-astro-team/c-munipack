.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: airmass_command.rst,v 1.2 2016/01/01 09:39:22 dmotl Exp $

.. program:: airmass

.. index::
   pair: airmass; command

.. _airmass-command:

airmass (command)
=================

utility for computing air mass coefficient


Synopsis
--------

airmass [ options ] *input-files* ...
			
airmass [ options ] -j *julian-date* 


Description
-----------

The :command:`airmass` command computes value of air mass coefficient (X) for given Julian date, 
object's coordinates and observer's coordinates. It may also append the values to a set of measurements stored
in a text file.

When the  -j option is present on the command line, the value of air mass coefficient is printed 
to the standard output stream.

If one or more file names are present on the command line, each source file given is processed line by line, 
the program expects the JD value in the first column, which must be divided at least one of common used dividers 
(semicolon, comma, space, tab char, ...). The JD value can be in full (2453xxx.x) or short (53xxx.x) form. Decimal 
places must be separated by point, not comma. The value of air mass coefficient is computed and appended to the end 
of the line. If the line starts with the text ``JD``, it is considered to be a table header and the text 
``AIRMASS`` is appended to the end of the line. All other lines which do not fit to any of previous rules 
are copied to the output file without modification.


.. _airmass-input-files:

Input files
-----------

Names of input files can be specified directly on a command-line as command arguments; it is allowed to use the 
usual wild-card notation. In case the input files are placed outside the working directory, you have to specify 
the proper path relative to the current working directory.

Alternatively, you can also prepare a list of input file names in a text file, each input file on a separate line. 
It is not allowed to use the wild-card notation here. Use the -i option to instruct the program to read the file.


.. _airmass-output-files:

Output files
------------

By default, output files are stored to the current working directory. Their names
are derived from the command name followed by a sequential number starting by 1.
Command options allows a caller to modify the default naming of output files.

The -o option sets the format string; it may contain a path where 
the files shall be stored to. Special meaning has a sequence of question marks, it 
is replaced by the ordinal number of a file	indented by leading zeros to the same
number of decimal places as the number of the question marks.

By means of the -i option, you can modify the initial value of a counter.

On request, the program can write a list of output files to a text file, use the -g option 
to specify a file name.

		
Options
-------

Options are used to provide extra information to customize the execution of a command. They are specified as command 
arguments.

Each option has a full form starting with two dashes and an optional short form starting with one dash only. Options 
are case-sensitive. It is allowed to merge two or more successive short options together. Some options require a value; 
in this case a value is taken from a subsequent argument. When a full form is used, an option and its value can also 
be separated by an equal sign. When a short form is used, its value can immediately follow the option.

Whenever there is a conflict between a configuration file parameter and an option of the same meaning, the option 
always take precedence.

.. option:: -j, --julian-date <jd>

    compute and print air mass coefficient for given Julian date. Do not combine this option
    with input file names.
			
.. option:: -a, --right-ascension <hhmmss>

    right ascension of object in hours, minutes and seconds
			
.. option:: -d, --declination <ddmmss>

    declination of object in degrees, minutes and seconds
			
.. option:: -l, --longitude <dddmmss>

    longitude of observer in degrees, minutes and seconds; positive values for a location to the
    east of the Greenwich meridian, negative values for a location to the west of the Greenwich meridian.
			
.. option:: -b, --latitude  <ddmmss>

    latitude of observer in degrees, minutes and seconds; positive values for a location to the
    north of equator, negative values for a location to the south of equator.
			
.. option:: --altitude

    compute and print also the altitude of the object in degrees.
			
.. option:: -s, --set <name=value>

    set value of configuration parameter
			
.. option:: -i, --read-dirfile <filepath>

    read list of input files from specified file; see the :ref:`airmass-input-files` section for details.
			
.. option:: -g, --make-dirfile <filepath>

    save list of output files to specified file, existing content of the file will be overwritten;
    see the :ref:`airmass-output-files` section for details.
			
.. option:: -o, --output-mask <mask>

    set output file mask (default=:file:`amass????.dat`), see the :ref:`airmass-output-files` section for details.
			
.. option:: -c, --counter <value>

    set initial counter value (default=1), see the :ref:`airmass-output-files` section for details.
			
.. option:: -p, --configuration-file <filepath>

    read parameters from given configuration file. See the :ref:`airmass-configuration-file` section for 
    details.
			
.. option:: -h, --help

    print list of command-line parameters
			
.. option:: -q, --quiet

    quiet mode; suppress all messages
			
.. option:: --version

    print software version string
			
.. option:: --licence

    print software licence
			
.. option:: --verbose

    verbose mode; print debug messages
			
    
.. _airmass-configuration-file:

Configuration file
------------------

Configuration files are used to set the input parameters to the process that is going to be executed by a command. 
Use the -p option to instruct the program to read the file before other command-line options are processed.

The configuration file consists of a set of parameters stored in a text file. Each parameter is stored on a separate 
line in the following form: name = value, all other lines are silently ignored. Parameter names are case-sensitive.
		
.. confval:: right-ascension = hhmmss

    right ascension of object in hours, minutes and seconds
			
.. confval:: declination = ddmmss

    declination of object in degrees, minutes and seconds
			
.. confval:: longitude = dddmmss

    longitude of observer in degrees, minutes and seconds; positive values for a location to the
    east of the Greenwich meridian, negative values for a location to the west of the Greenwich meridian.
			
.. confval:: latitude = ddmmss

    latitude of observer in degrees, minutes and seconds; positive values for a location to the
    north of equator, negative values for a location to the south of equator.
			
Examples
--------

::

	airmass -l164000 -b491300 -a182932 -d223424 -j2452763.5670
	
The command computes and prints value of air mass coefficient to the standard output.
The object's coordinates are R.A. = 18h 29m 32s, DEC. = +22d 34m 24s, the observer's 
coordinates are LON. = 16d 40m to the east of the Greenwich meridian and LAT. = 49d 13m to 
the north of the equator. Julian date of observation is 2452763.5670.
			
::

	airmass -l164000 -b491300 -a182932 -d223424 -oamass.dat table.dat
	
The command adds values of air mass coefficient to the table stored in :file:`table.dat` 
file and the resulting table stores in :file:`amass.dat` file. The object's and observer's coordinates are 
the same as in previous example.
			

Exit status
-----------

The command returns a zero exit status if it succeeds to process all specified files. Otherwise, it will stop 
immediately when an error occurs and a nonzero error code is returned.
