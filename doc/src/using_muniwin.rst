.. C-Munipack - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: using_muniwin.rst,v 1.5 2016/06/18 06:10:08 dmotl Exp $

.. index::
   pair: Muniwin; program
   
.. _using-muniwin:   
   
Using Muniwin 
=============

.. toctree::
   :maxdepth: 1

   projects
   profiles
   light_curve
   master_dark_frame
   master_flat_frame
   finding_variables
   catalog_files
   frame_merging
   incremental_processing
   multiple_variable_stars
   ensemble_photometry
   advanced_calibration
   processing_raw_images
   moving_targets
   aavso_format
   baavss_format
   track_curve
   project_settings

