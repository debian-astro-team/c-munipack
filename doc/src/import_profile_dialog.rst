.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: import_profile_dialog.rst,v 1.1 2015/07/06 08:19:48 dmotl Exp $

.. index::
   pair: Import project settings; dialog
   pair: project settings; import
   
.. _import-profile-dialog:

Import project settings (dialog)
================================

The `Import project settings` dialog is used to copy settings from an existing project 
to the current project. When you do this on regular basis, consider using :ref:`profiles`.


Activating the dialog
---------------------

The dialog can be opened from the :ref:`project-settings-dialog`, the root page, using 
the :guilabel:`Import from project` button.


The dialog controls
-------------------

.. figure:: images/importprofile_couts.png
   :align: center
   :alt: Import project settings dialog
   
   Import project settings dialog

\(1) Specify a path to the project which the settings shall be imported from.

\(2) Click the :guilabel:`Browse` button to open a standard file selection dialog.

\(3) The table shows the most recetly used projects. When you select an item, the projest's 
path is filled in to the edit field.

Click the button (4) to confirm the dialog.
     

.. seealso:: :ref:`project-settings-dialog`
