.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: kombine_command.rst,v 1.1 2015/07/06 08:19:49 dmotl Exp $

.. program:: kombine

.. index::
   pair: kombine; command
  
.. _kombine-command:

kombine (command)
=================

utility for making combining CCD frames


Synopsis
--------

kombine [ options ] *output-file* *input-files* ...


Description
-----------

The :command:`kombine` command combines a set of CCD frames to a single CCD frame. 
The source frames should be corrected already. The source frames are shifted by the offset stored in the 
corresponding photometry file with offset values. The photometry files are products of the :command:`Munimatch` 
utility. Then, the resulting values are computed pixel by pixel by means of the mean algorithm 
(sum divided by total number of frames). Pixels in those regions, which are not covered by all frames, 
are set to zero. Pixels, which are zero on at least one of the source frames, are set always to zero. 
Pixels, which are overexposed on one the source frames, are set always to maximal value (655535 ADU).

All source frames must be in FITS format and of same dimensions. Frames of the same color filter 
and exposition duration should be used. The output file is in FITS format too.
		

.. _kombine-input-files:

Input files
-----------

Names of input files can be specified directly on a command-line as command arguments; it is allowed to use the 
usual wild-card notation. In case the input files are placed outside the working directory, you have to specify 
the proper path relative to the current working directory.

Alternatively, you can also prepare a list of input file names in a text file, each input file on a separate line. 
It is not allowed to use the wild-card notation here. Use the -i option to instruct the program to read the file.


Options
-------

Options are used to provide extra information to customize the execution of a command. They are specified as command 
arguments.

Each option has a full form starting with two dashes and an optional short form starting with one dash only. Options 
are case-sensitive. It is allowed to merge two or more successive short options together. Some options require a value; 
in this case a value is taken from a subsequent argument. When a full form is used, an option and its value can also 
be separated by an equal sign. When a short form is used, its value can immediately follow the option.

Whenever there is a conflict between a configuration file parameter and an option of the same meaning, the option 
always take precedence.


.. option:: -s, --set <name=value>

    set value of configuration parameter
			
.. option:: -i, --read-dirfile <filepath>

    read list of input files from specified file; see the :ref:`kombine-input-files` section for details.
			
.. option:: -p, --configuration-file <filepath>

    read parameters from given configuration file. See the :ref:`kombine-configuration-file` section for 
    details.
			
.. option:: -h, --help

    print list of command-line parameters
			
.. option:: -q, --quiet

    quiet mode; suppress all messages
			
.. option:: --version

    print software version string
			
.. option:: --licence

    print software licence
			
.. option:: --verbose

    verbose mode; print debug messages
			

.. _kombine-configuration-file:

Configuration file
------------------

Configuration files are used to set the input parameters to the process that is going to be executed by a command. 
Use the -p option to instruct the program to read the file before other command-line options are processed.

The configuration file consists of a set of parameters stored in a text file. Each parameter is stored on a separate 
line in the following form: name = value, all other lines are silently ignored. Parameter names are case-sensitive.

.. confval:: bitpix = value

    output data format (0=Auto)

    			
Examples
--------

::
    
		
	kombine ouptut.fts test1.fts test2.fts test3.fts
	
The command computes the combined frame from the files :file:`test1.fts`, 
:file:`test2.fts` a :file:`test3.fts`; the resulting frame is stored 
to :file:`output.fts`.
			

Exit status
-----------

The command returns a zero exit status if it succeeds to process all specified files. Otherwise, it will stop 
immediately when an error occurs and a nonzero error code is returned.
