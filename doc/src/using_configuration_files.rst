.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: using_configuration_files.rst,v 1.1 2015/07/06 08:19:49 dmotl Exp $

.. index::
   pair: configuration; file
   
.. _using-configuration-files:

Using configuration files
-------------------------
        
The configuration files provide extra information to customize the execution 
of a command. Unlike the command-line options, they can be prepared once and
reused repeatedly. By means of them, the number of command-line arguments can be  
significantly reduced.

The configuration file consists of a set of parameters stored in a text 
file. Each parameter is stored on a separate line in the following form: 
<replaceable>name</replaceable> = <replaceable>value</replaceable>, all other 
lines are silently ignored. Parameter names are always case-sensitive. A hash 
character starts a comment string, all following content is ignored up to the end
of the present line.

The :samp:`-p` option must be used to instruct the program to read 
and process the configuration file. Whenever there is a conflict between a configuration 
file parameter and an option of the same meaning, the option always take precedence.

Example of configuration file::

    # Sample configuration file
    right-ascension = `23.0253`        # GSC 2750 854
    declination     = `30.7408`
    longitude       = `16.6667`        # Brno
    latitude        = `49.2167`
