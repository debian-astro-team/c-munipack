.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: heliocentric_correction_dialog.rst,v 1.1 2015/07/06 08:19:48 dmotl Exp $

.. index::
   pair: Heliocentric correction; dialog
   
.. _heliocentric-correction-dialog:

Heliocentric correction (dialog)
================================

The "Heliocentric correction" allows an user to compute heliocentric
correction for specified star and date of observation. It can also convert the geocentric date into
heliocentric one and vice versa.


Activating the dialog
---------------------

The dialog can be activated:

#. from the main menu: :menuselection:`Tools --> Heliocentric correction`.


.. index::
   pair: Heliocentric;correction

Computing heliocentric correction
---------------------------------

.. figure:: images/helcorr_couts1.png
   :align: center
   :alt: Heliocentric correction
   
   Computing heliocentric correction
   
\(1) Select the desired output value.

Enter object's right ascension to the text box (2) in hours. Use the hexagesimal format, separate
the fields by a space character.

Enter object's declination to the text box (3) in degrees. Use the hexagesimal format, separate
the fields by a space character.

Click the ellipsis button (4) to retrieve object's coordinates from a table of predefined
objects or a catalog of variable stars.

Enter a Julian date into the text box (5) or date and time into the text box (6). As you type in the data, 
corresponding value of heliocentric correction in days is updated in the last text box (7).

.. index::
   pair: Julian date;heliocentric

Computing heliocentric Julian date
----------------------------------

.. figure:: images/helcorr_couts2.png
   :align: center
   :alt: Heliocentric correction
   
   Computing heliocentric Julian date
   
\(1) Select the desired output value.

Enter object's right ascension to the text box (2) in hours. Use the hexagesimal format, separate
the fields by a space character.

Enter object's declination to the text box (3) in degrees. Use the hexagesimal format, separate
the fields by a space character.

Click the ellipsis button (4) to retrieve object's coordinates from a table of predefined
objects or a catalog of variable stars.

Enter a geocentric (observed) Julian date into the text box (5) or the date and time into the text box (6). 
As you type in the data, corresponding value of heliocentric Julian date is updated in the last text box (7).


.. index::
   pair: Julian date;geocentric

Computing geocentric Julian date
--------------------------------

.. figure:: images/helcorr_couts3.png
   :align: center
   :alt: Heliocentric correction
   
   Computing geocentric Julian date
   
\(1) Select the desired output value.

Enter object's right ascension to the text box (2) in hours. Use the hexagesimal format, separate
the fields by a space character.

Enter object's declination to the text box (3) in degrees. Use the hexagesimal format, separate
the fields by a space character.

Click the ellipsis button (4) to retrieve object's coordinates from a table of predefined
objects or a catalog of variable stars.

Enter a heliocentric Julian date into the text box (5) or date and time into the text box (6). As you type 
in the data, corresponding value of geocentric (observed) Julian date is updated in the last text box (7).


.. seealso:: :ref:`jd-converter-dialog`, :ref:`air-mass-coefficient-dialog`
