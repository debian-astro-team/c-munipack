.. C-Munipack - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: history.rst,v 1.13 2016/07/06 19:08:25 dmotl Exp $

.. index::
   single: History
   single: Change log
   
.. _history:   
   
History
=======

Change log
----------

.. rubric:: Version 2.1.37

January 11, 2024`

- Added an option to select if positive longitude is east or west of Greenwich meridian.

.. rubric:: Versions 2.1.27 - 2.1.36

- Bug fixes


.. rubric:: Version 2.1.26

June 18, 2023`

- Added an option to disable alignment on local maxima in the Quick photometry tool.


.. rubric:: Version 2.1.35

`March 29, 2023`

- Added support for date & time format using UT field, the value is specified in seconds.


.. rubric:: Version 2.1.32

`December 11, 2021`

- Added support for CR3 raw files


.. rubric:: Version 2.1.27

`May 21, 2020`

- Added new date & time format (prefixed with date name)

- Added new date & time format (QHY8L camera)

- Added an option to show and hide tags for known variables

- Fixed random crash when frames in the main window are sorted by property other that date and time


.. rubric:: Version 2.1.23

`December 18, 2017`

- Added new file format - Nikon D5500


.. rubric:: Version 2.1.22

`December 07, 2017`

- New feature - copy WCS coordinates of selected object to clipboard (context menu on right click)

- New feature - sorting of frames in the main window

- Minor fix - sign of SNR values


.. rubric:: Version 2.1.21

`July 30, 2017`

- Fix for frames with zero exposure time

- Fix chart preview in the Find variables tool - the image + chart mode did not work.

- Fix highlighting a variable star in the Find variables tool


.. rubric:: Version 2.1.20

`June 3, 2017`

- Minor change - Find variables - highlight all variable stars from user-defined selections (known variables)

- Minor change - default file name for light curve and chart is set to a name of a current user-defined selection

- Minor change - if a label on a chart is located beyond the right / bottom edge, draw it on the left / above the object.


.. rubric:: Version 2.1.19

`March 29, 2017`

- Fixed error - Minimum pixel value could not be set below zero.

- Fixed crash that occurred when creating a light curve when project's path contains characters with diacritics


.. rubric:: Version 2.1.18

`March 11, 2017`

- New date & time format (DATE-OBS with dots as separators)

- Fixed build issue with 32/64-bit long integer numbers.

- Fixed bug in AAVSO export - column names must start with a hash character

- Show wcslib's version identifier in the About dialog 


.. rubric:: Version 2.1.17

`March 4, 2017`

- New date & time format (CTRLTIME field)

- Increased buffer sizes to avoid crashes due to buffer overrun.

- Removed include of "malloc.h" header, error when building for Mac.


.. rubric:: Version 2.1.16

`February 8, 2017`

- Fixed error when saving a profile - 'No such file or directory' error message is shown when the 'Profiles' directory does not exist.

- New date & time format in FITS files - hours without a leading zero.


.. rubric:: Version 2.1.15

`January 2, 2017`

- Fixed error in BAA VS export - columns VarAbsMag and VarMag were the wrong way round.


.. rubric:: Version 2.1.14

`October 11, 2016`

- Added new date & time format (PI VersArray)


.. rubric:: Version 2.1.13

`August 7, 2016`

- Ensemble (multiple comparison stars) photometry in the Find variables tool


.. rubric:: Version 2.1.12

`July 6, 2016`

- Added new date & time format - 'UTC' field


.. rubric:: Version 2.1.11

`June 18, 2016`

- New matching algorithm for very dense fields

- Fixed random crash in the 'Find variables' tool


.. rubric:: Version 2.1.10

`May 21, 2016`

- Munilist command can make a table of objects from a photometry file or a catalogue file


.. rubric:: Version 2.1.9

`May 15, 2016`

- Fixed crash in making light curve

- Fixed crash in Find variables tool

- Fixed crash in the 'munilist' command line tool

- Do not clear parameters stored in a project on 'Remove all frames' command


.. rubric:: Version 2.1.8

`May 8, 2016`

- Message log reverted to its original state (more messages)

- Frame preview dialog - when switching between frames, keep a selected object on its place

- Frame preview dialog - optionally show a track of a moving target

- DLSR processing - fixed error in reading/writing of "frames summed" parameter to FITS files


.. rubric:: Version 2.1.7

`March 6, 2016`

- Fixed minor GUI issues related to moving targets

- Fixed minor bug in frame matching for moving targets

- The tool "Process new frames" did not work with moving targets


.. rubric:: Version 2.1.6

`February 28, 2016`

- Updated user manual


.. rubric:: Version 2.1.5

`February 22, 2016`

- Matching for moving targets

- Fixed issues with 'Max. stars' parameter

- Improved error reporting capabilities


.. rubric:: Version 2.1.4

`January 9, 2016`

- Fixed program crash in the photometry


.. rubric:: Version 2.1.3

`December 30, 2015`

- The installation package for MS Windows includes 64-bit executables

- Updated GTK+ toolkit; MS Windows 2000 are no longer supported

- Added new plot - CCD temperatures

- Added new plot - object properties (position, de-focus (FWHM), instumental magnitude, ...)

- 'Save project' renamed to 'Export project'

- The option 'Ensemble photometry' added to the 'Make light curve' dialog

- Support for World Coordinate System (WCS) data; show RA/Dec coordinates of objects

- Renamed columns "Vnnn" in the light curve if the option "Select all stars on the reference frame" is checked to "IDnnn"

- Added new date & time format - 'DATE' field


.. rubric:: Version 2.0.21


`November 16, 2015`

- Fixed bug in creating a light curve with heliocentric correction

- Fixed bug in AAVSO export for the ensemble photometry.

- Added manual topic about creating an AAVSO report.


.. rubric:: Version 2.0.20


`November 11, 2015`

- Export a light curve in the AAVSO Extended format

- Fixed program crash in the Find variables tool


.. rubric:: Version 2.0.19


`November 3, 2015`

- Added option in making a light curve - show all objects on the reference frame

- Maximizing the main window on startup is default, but it can be turned off in the Environment settings


.. rubric:: Version 2.0.17


`March 8, 2015`

- Fixed saving of CCD files - message "Invalid dimensions of the image".


.. rubric:: Version 2.0.16


`November 17, 2014`

- Fixed program crash in Express reduction tool (selection of reference frame)

- The program can read 'Instrument' and 'Telescope' fields from the FITS frames.

- More formats when reading object celestial coordinates from FITS files.

- Fixed compatibility issues when compiling software on Debian 7.


.. rubric:: Version 2.0.15


`October 27, 2014`

- Time correction can be applied at any time (after photometry and matching)

- Find variables - uses median filtering to remove sporadic outlaying measurements

- Julian dates are printed with 7 digits after the decimal point

- Improved exporting of tables, all tables can be exported in CSV format

- Minor bug fixes


.. rubric:: Version 2.0.14


`July 19, 2014`

- Fixed bug in light curve - errors bars were always shown

- Find variables - added "custom" range of magnitudes in the light curve

- Fixed crash in find variables - program crashed when an image was shown instead of a chart


.. rubric:: Version 2.0.13


`July 13, 2014`

- Fixed crash in matching

- Find variables - fixed or adaptive range of magnitudes in the light curve


.. rubric:: Version 2.0.12


`July 04, 2014`

- Fixed saving of light curves


.. rubric:: Version 2.0.11


`June 15, 2014`

- Graphs and charts now respect the system color scheme (Set the Windows to high contrast theme to switch to a "night" mode).

- Fixed bug - the photometry did not work properly if the frame borders were used

- Python module - cmpack.PhtFile now exports FWHM of an object

- Frame preview - the 'Object inspector' tool shows reason why an object was not measured

- Frame preview can show a table of objects

- Light curve, track curve and air mass curve can show a table of measurements. The table can be
  exported to a CSV file.
  
- Changed dialog for creating new project, loading and saving profiles.

- It is possible to save multiple selections of objects (multiple variable stars on one view field)

- Option if the first line in the FITS file is displayed at the top or the bottom of the window

- Added possibility of constant time offset that is always applied to all frames in the project.

- Fixed creating 32-bit master flat frames



.. rubric:: Version 2.0.10


`July 7, 2013`

- Fixed bug in catalog files - var, comp and check stars were not restored in the 'Matching' tool.

- Fixed minor problem with range checking in the Autocontrast routine

- It is possible to edit user-defined object tags in a catalogue file

- Object inspector shows net intensity in ADU for a selected object

- Quick photometry - 'Signal' renamed to 'Net intensity'



.. rubric:: Version 2.0.9


`May 23, 2013`

- Graphs can alternatively display date and time (UTC) and Julian date (JD)

- Fixed bug - when 'Mater bias' project was open, the 'Make' menu was hidden

- When frames are removed from the project, the associated temporary files are deleted

- Fixed bug - matching algorithm for sparse fields required at least 5 stars

- When one wants to save a chart, the program suggests the same name as for the light curve

- When Ctrl key is held while mouse wheel is rotated, the zoom in X axis is preserved.

- The 'Help' menus are aligned to the left

- Exposure duration is shown with 3 decimal places



.. rubric:: Version 2.0.8


`January 14, 2013`

- Fixed bug - program crashed in the dark correction procedure


.. rubric:: Version 2.0.7


`January 13, 2013`

- Fixed bug - when multiple bias/dark/flat correction frames was applied inside a single
  set of source frame, the corrections were not applied correctly. The first correction frame was applied 
  always.
- Updated document `Theory of operation` - added detailed description of the matching algorithm
- Merge frames - correct handling of a case where the merged frames are rotated
- Choose stars dialog - variable star was sometimes not displayed
- All dialog controls have tooltip texts
- Fixed master flat tool - failed in case where source frames were stored in signed 16-bit format.


.. rubric:: Version 2.0.6


`November 17, 2012`

- User defined tags for objects on charts.
- New Python interface for C-Munipack library.
- Fixed bug - time correction did not work in the Express reduction dialog.
- Fixed doxygen configuration file.


.. rubric:: Version 2.0.5


`October 28, 2012`

- New 'Measurement' tool.
- Find variables - it is possible to manually remove individual objects from the mag-dev curve
- Do not show items in recent lists if they do not exist.
- Minor bug fixes
- Updated user's manual


.. rubric:: Version 2.0.4


`October 4, 2012`

- The 2.0 .. rubric:: Version is now released as a current stable .. rubric:: Version.
- List of recent projects and list of recent files are shown in the main menu.
- New `Statistics` tool that shows minimum, maximum, mean and standard deviation.
- Express reduction can be applied to selected frames only.
- Support for CSLR camera Cannon 1100D
- Minor bug fixes
- Updated user's manual


.. rubric:: Version 2.0.3


`September 21, 2012`

- Optimized main menu items
- Better icons
- Updated user's manual



.. rubric:: Version 2.0.2


`September 1, 2012`

- Added support for QHY cameras
- Updated icons
- Updated user's manual


.. rubric:: Version 2.0.1


`August 19, 2012`

- Fixed bug in processing entered object's and observer's coordinates.


.. rubric:: Version 2.0.0


`August 12, 2012`

- First public release of the 2\ :sup:`nd` generation of the C-Munipack software
