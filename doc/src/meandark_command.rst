.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: meandark_command.rst,v 1.1 2015/07/06 08:19:49 dmotl Exp $

.. program:: meandark

.. index::
   pair: meandark; command

.. meandark-command:

meandark (command)
==================

utility for making master-dark frames
	
	
Synopsis
--------

meandark [ options ] *output-file* *input-files* ...


Description
-----------

The :command:`meandark` command compose a set of dark frames and makes one 
dark frame called 'master-dark'. Applying this function, you can achieve high quality correction frame 
and	thus reducing the noise of a result.

All source frames must be in the FITS format and of same dimensions. Frames of the same exposition 
duration should be used. The output file is written in the FITS format too.
		

.. _meandark-input-files:

Input files
-----------

Names of input files can be specified directly on a command-line as command arguments; it is allowed to use the 
usual wild-card notation. In case the input files are placed outside the working directory, you have to specify 
the proper path relative to the current working directory.

Alternatively, you can also prepare a list of input file names in a text file, each input file on a separate line. 
It is not allowed to use the wild-card notation here. Use the -i option to instruct the program to read the file.


Options
-------

Options are used to provide extra information to customize the execution of a command. They are specified as command 
arguments.

Each option has a full form starting with two dashes and an optional short form starting with one dash only. Options 
are case-sensitive. It is allowed to merge two or more successive short options together. Some options require a value; 
in this case a value is taken from a subsequent argument. When a full form is used, an option and its value can also 
be separated by an equal sign. When a short form is used, its value can immediately follow the option.

Whenever there is a conflict between a configuration file parameter and an option of the same meaning, the option 
always take precedence.

.. option:: -s, --set <name=value>

    set value of configuration parameter
						
.. option:: -i, --read-dirfile <filepath>

    read list of input files from specified file; see the :ref:`meandark-input-files` section for details.
			
.. option:: -p, --configuration-file <filepath>

    read parameters from given configuration file. See the :ref:`meandark-configuration-file` section for 
    details.
			
.. option:: -h, --help

    print list of command-line parameters
			
.. option:: -q, --quiet

    quiet mode; suppress all messages
			
.. option:: --version

    print software version string
			
.. option:: --licence

    print software licence
			
.. option:: --verbose

    verbose mode; print debug messages
			
    
.. _meandark-configuration-file:

Configuration file
------------------

Configuration files are used to set the input parameters to the process that is going to be executed by a command. 
Use the -p option to instruct the program to read the file before other command-line options are processed.

The configuration file consists of a set of parameters stored in a text file. Each parameter is stored on a separate 
line in the following form: name = value, all other lines are silently ignored. Parameter names are case-sensitive.
		
.. confval:: bitpix = value

    output data format (0=Auto)
			
.. confval:: scalable = value

    make scalable dark-frame (0=No, 1=Yes)
			
    
Examples
--------

::

    meandark out.fts in1.fts in2.fts in3.fts

The command computes the master-dark frame from the input files :file:`in1.fts`, 
:file:`in2.fts` a :file:`in3.fts`; the resulting frame is stored to 
output file :file:`out.fts`.
			
	
Exit status
-----------

The command returns a zero exit status if it succeeds to process all specified files. Otherwise, it will stop 
immediately when an error occurs and a nonzero error code is returned.
