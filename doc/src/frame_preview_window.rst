.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: frame_preview_window.rst,v 1.2 2016/05/08 09:13:59 dmotl Exp $

.. index::
   pair: Frame preview; window

.. _frame-preview-window:

Frame preview window
====================

This window is used to display a source frame and all information related to this frame.


Activating the dialog
---------------------

The dialog can be activated:

.. |preview_icon| image:: icons/preview.png


#. double left click on an item in the table of source files.

#. right click on an item in the table of source files, from the context menu
   select :guilabel:`Open preview`.
   
#. from the main menu: :menuselection:`Project --> Show selected frame`.

#. from the main toolbar: |preview_icon|
   
   
Each file is presented in a separate window.


Dialog controls
---------------

.. figure:: images/preview_couts1.png
   :align: center
   :alt: Source frame preview
   
   Preview window for source frames

\(1) The image and chart are displayed in the preview area. It is possible to switch between
several rendering modes.

\(2) You can easily jump to the previous and next frame in the table by means of the
left/right arrow icons on the toolbar. The ordinal number of displayed frame and total number
of frames in the text box as a fraction.

\(3) You can zoom the preview in and out by means of the zoom icons on the toolbar.

\(4) It is possible to change the current aperture for which the object's brightness
is displayed in the chart and the information box.

\(5) The local menu bar provides following functions:

- Menu Frame:

  - Show properties - display the further details about the frame. Full header preview
    is available in separate window.
    
  - Remove from project - remove this frame from the project. It has the same effect as
    removing the frame from the main application window.
    
  - Close - close this window
  
- Menu View:

  - Image only - show only a CCD image and hide objects
  
  - Chart only - show only objects and hide a CCD image
  
  - Chart and image - show a CCD image on the background and objects as an overlay
  
  - Table - show a table of detected objects
  
  - Calibrated image - show the current calibrated frame 
  
  - Original image - show original uncalibrated image
    
  - Pseudo-color image - switch between rendering of pixel intensities in pseudo-color scale and a gray scale.
  
  - Rulers - show rules with pixels 
  
  - Moving target trace - show a trace of a moving target. The curve spans from the point where the moving object
    is supposed to be for a minimum observation date to a point for a maximum observation date. The minimum and 
    maximum date is determined from all frames in the dataset. A mark (short line perpendicular to the trace) highlights
    a point where the moving target is supposed to be at the observation date of the current frame.
  
- Menu Tools:

  - Gray scale - show a gray scale or a pseudo-color scale on the right side of the dialog.
  
  - Object inspector - show the Object inspector tool - explained below.
  
  - Quick photometry - show the Quick photometry tool - explained below.
  

Browsing the image
------------------

In the local menu, select :menuselection:`View --> Image only` to hide the objects.

When you place a cursor over the image area, the coordinates and the pixel value
are displayed in the status bar.


.. index::
   pair: Quick photometry; tool

Quick Photometry tool
---------------------

The Quick Photometry tool is a tool which computes basic photometric properties
of an object on a frame. The main goal of this tool is to provide an estimation of
the FWHM value (the important input parameter of the photometry process).

Because this tool does simplified version of aperture photometry, the results
are informative only and shouldn't be used elsewhere. Also, it doesn't require a proper
calibration to be performed first.

To activate the tool, select :menuselection:`Tools --> Quick photometry`
from the local menu. A new panel on the right side of the preview window appears.


.. figure:: images/preview_couts3.png
   :align: center
   :alt: Quick photometry tool
   
   Quick photometry tool
  
Using the left mouse button, click on the image (1), close to an object. The program
automatically finds the nearest local maximum, so you don't need to be accurate. The computation
stars automatically and the results are displayed the the right panel (2).

The following object parameters are presented:

- Center - coordinates of the local maximum.

- Max - pixel value in local maximum

- Background - mean background level, computed as a robust mean of pixels in the sky annulus.

- Noise - background noise level, computed as a standard deviation of pixels in the sky annulus.

- FWHM - object's width, computed full width at half maximum.

- Aperture - aperture radius used for computing the object's signal.

- Signal - object's signal, sum of pixels in the aperture, mean background level is subtracted.

- S/N ratio - signal to background noise ratio, displayed in decibels: SNR = 10.log(S/N).

- \(3) Several dimensions are presented in the image area.

- Two blue circles show the annulus which is used to estimate the background properties.

- A red circle shows the object's average FWHM.

- A green circle shows the size of the aperture.

It is also possible to change some parameters:

- \(4) Aperture radius in pixels

- \(5) Inner radius of the sky annulus in pixels

- \(6) Outer radius of the sky annulus in pixels


.. index::
   pair: Profile; tool

Profile tool
------------

The Profile tool is used to check the shape of objects. It shows
a curve of pixels values along a line.

To activate the tool, select :menuselection:`Tools --> Quick photometry`
from the local menu. A new panel on the bottom of the preview window appears.

.. figure:: images/preview_couts2.png
   :align: center
   :alt: Profile tool
   
   Profile tool
   
Using the left mouse button, draw a line on the image (1), crossing the center of an object.
The profile is drawn in the graph (2). You can adjust the graph style (3). The following parameters
are displayed in the information box (4):

- Min - minimum pixel value.

- Max - maximum pixel value

- Mean - mean pixel value of pixels in the profile.

- St. dev. - standard deviation of pixels in the profile.


.. index::
   pair: Histogram; tool

Histogram tool
--------------

The Histogram tool shows a histogram of pixel values and some related statistics.

To activate the tool, select :menuselection:`Tools --> Histogram` from the local 
menu. A new panel on the bottom of the preview window appears.

.. figure:: images/preview_couts4.png
   :align: center
   :alt: Histogram tool
   
   Histogram tool

The histogram is shown in the left part (1) of the tool. The following parameters
are displayed in the text area on its right side (2):

- Min - minimum pixel value

- Max - maximum pixel value

- Mean - mean pixel value of all pixels

- St. dev. - standard deviation of all pixels


Browsing the objects
--------------------

In the local menu, select :menuselection:`View --> Chart only` or :guilabel:`Image and chart` 
to show the objects.

When you place a cursor over an object in the image area, its coordinates and
other properties are displayed in the status bar.

Objects that were not identified on the reference frame (see Matching) are rendered
in a gray color.

Objects that were not measured successfully (see Photometry) are drawn in red
color and not filled.

Objects that were not measured and not matched are draw in gray color and not filled.

The brightness for each object is rendered using the selected aperture.


.. index::
   pair: Object inspector; tools

Object inspector tool
---------------------

The Object inspector tool is a tool which displays that the program registers about
an object on a frame. The main purpose of this tool is for testing and debugging. Unlike the
Quick Photometry, the Object Inspector shows results, that has been obtained during the "full"
photometry. Because of this, calibration and photometry must be performed first. The results
are more relevant.

To activate the tool:

#. From the local menu, select :menuselection:`View --> Chart only`
   or :guilabel:`Image and chart` to show the objects.

#. From the local menu, select :menuselection:`Tools --> Object inspector`.
   A new panel on the right side of the preview window appears.
   
.. figure:: images/phtfile_couts2.png
   :align: center
   :alt: Object Inspector tool
   
   Object Inspector tool
   
Using the left mouse button, click on the image (1), to select an object. The object is
highlighted and its properties are displayed in the right panel (2).

The following object parameters are presented:

- Object # - object's ordinal number on the current frame.

- Reference ID - object's ordinal number on a reference frame.

- Center - coordinates of the object's centroid

- Brightness - instrumental brightness of the object in magnitudes.

- Error - error estimation of brightness in magnitudes.

Several dimensions are presented in the image area (3):

- Two blue circles show the annulus which is used to estimate the background properties.

- A green circle shows the size of the aperture.

\(4) Here, you can change the current aperture.


.. seealso:: :ref:`main-window`
