.. C-Munipack - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: dark_frame_correction.rst,v 1.2 2015/07/12 07:44:57 dmotl Exp $

.. index::
   pair: dark-frame; correction

.. _dark-frame-correction: 

Dark-frame correction
=====================

There is a choice of two calibration schemes for the dark-frame correction: the standard calibration scheme 
and the advanced calibration scheme.

.. index::
   single: standard calibration scheme

.. rubric:: Standand calibration scheme

In the "standard calibration scheme", 
the dark-frame correction is a subtraction of a dark frame *D* from a source frame *X* to generate an 
output frame Y. The following formula is applied to each pixel at coordinates *(x, y)* independently:

.. math:: 
   :label: dfc
   
   Y(x, y) = X(x, y) - D(x, y)

Bad and overexposed pixels require special treatment:

* If a pixel is bad at either source frame or dark frame, it must be marked as bad on a corrected frame. 
* If a pixel is overexposed on the source pixel, it must stay overexposed after the correction.
* If a pixel is overexposed on the dark frame, it is marked as bad on the corrected frame.

.. index::
   single: advanced calibration scheme

.. rubric:: Advanced calibration scheme
   
In the "advanced calibration scheme", a scalable dark frame, that was acquired with
exposure duration :math:`t_SD` is scaled to match the exposure duration :math:`t_X` of a source frame *X*.
Then, the dark frame is subtracted from a source frame to generate an output frame *Y*.     

.. math::
   :label: adv_dfc
   
   Y(x, y) = X(x, y) - \frac{t_X}{t_SD}\,SD(x, y)

The program makes sure that the dark frame is a scalable dark frame by checking the status of the
*SCALABLE* flag, which is stored in the file's header. 
