.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: message_log_dialog.rst,v 1.2 2016/01/01 09:39:22 dmotl Exp $

.. index::
   pair: Message log; dialog
   
.. _message-log-dialog:

Message log (dialog)
====================

The "Message log" shows all messages from the last process. You can check the result of
the last process (photometry, matching, ...) here.


Activating the dialog
---------------------

The dialog can be activated:

#. from the main menu: :menuselection:`Tools --> Show message log`.
