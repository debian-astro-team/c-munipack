.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: new_project_dialog.rst,v 1.1 2015/07/06 08:19:49 dmotl Exp $

.. index::
   pair: New project; dialog

.. _new-project-dialog:

New project (dialog)
=======================

The `New project` dialog is used to enter name, location and select a type of a new
project.


Activating the dialog
---------------------

The dialog can be activated:

.. |newproject_icon| image:: icons/newproject.png

#. from the main menu: :menuselection:`Project --> New`.

#. from the main toolbar: |newproject_icon|


The dialog controls
-------------------

.. figure:: images/newproject_couts.png
   :align: center
   :alt: New project dialog
   
   The "New project" dialog
   
\(1) Enter a project name. Because the name of the file that keeps track of the data related to the 
project, the project file, is derived from the name, some characters cannot appear in the project name, 
do not use: / \ ? % * : | " < and >.

The entry (2) shows a path to the directory where a new project will be created. You can edit the path 
directly in the entry field or you can also click the :guilabel:`Browse` button (3) to select a directory 
in a separate dialog.

\(4) List of existing profiles is shown in the table. The profiles are divided into two categories -
user-defined profiles and predefined profiles.

Click the button (5) to proceed.


.. seealso:: :ref:`projects`, :ref:`profiles`, :ref:`load-profile-dialog`.
