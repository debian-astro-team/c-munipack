.. C-Munipack - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: preface.rst,v 1.3 2016/02/28 10:27:21 dmotl Exp $
   
Preface
=======

At the beginning of the 21st century, astronomical photometry has changed a
lot. Instead of visual and photoelectric observation, the observers began to
use CCD cameras. The CCD cameras became cheaper and therefore more affordable
even for amateur astronomers. This gave rise to the demand for a software tool
for processing the CCD-data, which would be affordable for an amateur astronomer, 
provide a simple and user-friendly graphical interface and provide robust algorithms.

In April 2003 the C-Munipack project was started; its goal was to supply the
astronomers with a powerful but also user-friendly tool for processing their
CCD-data. At the time, the two similar tools existed: MuniDOS, a text-mode
program, which was somewhat limited by the user interface. Additionally, according
to the authors, the program is not using modern algorithms, and no future
development is planned. The other available was Munipack, written partly
in Fortran and partly in C. Munipack is a modern and still maintained tool, but there
were several disadvantages: the users of Windows operating system would
have severe problems with its compilation from the source code (no binaries were provided), 
and the graphical interface, that program offers, is available only for unix-based 
operating systems. Of course, there are some commercial products available on the 
market, some of them are really good, but these products are not affordable for 
amateur astronomers, since they have to pay for everything by themselves.


.. rubric:: About this document

This manual is an official user manual and a reference of the C-Munipack software. The document consists of
several parts. The first, introductory, part gives a reader a basic information about the
software, its authors, features and internet resources that are related to the project. :ref:`The second 
part <using-muniwin>` describes in details its graphical user interface, the Muniwin application, which is the way 
that the most users uses for their daily work all the time. :ref:`The third part <toolkit>` contains detailed description 
of a set of user commands, the C-Munipack toolkit, these commands can be used to build
automated shell scripts. The detailed description of the algorithms and file formats is found
in :ref:`the fourth part <theory>` of this document.

If you are a novice user, please read the chapter :ref:`getting-started` first. This chapter
is a simple tutorial that guides your through a basic CCD frame processing work-flow
using a set of sample frames that are available on the project's home page. The basic concepts 
of the software are also explained there.


.. rubric:: Acknowledgement

I would like to thank to all my colleagues, who contributed with their ideas,
advice, and suggestions, to this program. Namely to Filip Hroch for sharing
the Munipack source code, and for his help with porting it from Fortran to C,
to Lukas Kral for sharing his Varfind source code, to Miloslav Zejda, 
Ondrej Pejcha, Petr Svoboda, Volkan Bakis for their help with testing and valuable 
suggestions to the user interface and also to Jitka Kudrnacova and Petr Lutcha for 
their help with the user manual.

The user manual contains CCD frames of GSC 2750 854 (courtesy of Vyskov observatory)
and CCD frames of 268 Adorea (courtesy of ProjectSoft HK & Astronomical Institute of the 
Charles University in Prague)