.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: timebat_command.rst,v 1.1 2015/07/06 08:19:49 dmotl Exp $

.. program:: timebat

.. index::
   pair: timebat; command

.. _timebat-command:
   
timebat (command)
=================

utility for time correction


Synopsis
--------

timebat [ options ] *time-corr* *input-files* ...


Description
-----------

The :command:`timebat` command applies time correction to a set of source frames. It means, 
that it adds given amount of time to the times of observation. The program does not touch neither
the image data nor other parameters in header.
The source frames must be in the FITS format. The output file is in the FITS format too.

.. _timebat-input-files:

Input files
-----------

Names of input files can be specified directly on a command-line as command arguments; it is allowed to use the 
usual wild-card notation. In case the input files are placed outside the working directory, you have to specify 
the proper path relative to the current working directory.

Alternatively, you can also prepare a list of input file names in a text file, each input file on a separate line. 
It is not allowed to use the wild-card notation here. Use the -i option to instruct the program to read the file.


.. _timebat-output-files:

Output files
------------

By default, output files are stored to the current working directory. Their names
are derived from the command name followed by a sequential number starting by 1.
Command options allows a caller to modify the default naming of output files.

The -o option sets the format string; it may contain a path where 
the files shall be stored to. Special meaning has a sequence of question marks, it 
is replaced by the ordinal number of a file	indented by leading zeros to the same
number of decimal places as the number of the question marks.

By means of the -i option, you can modify the initial value of a counter.

On request, the program can write a list of output files to a text file, use the -g option 
to specify a file name.

		
Options
-------

Options are used to provide extra information to customize the execution of a command. They are specified as command 
arguments.

Each option has a full form starting with two dashes and an optional short form starting with one dash only. Options 
are case-sensitive. It is allowed to merge two or more successive short options together. Some options require a value; 
in this case a value is taken from a subsequent argument. When a full form is used, an option and its value can also 
be separated by an equal sign. When a short form is used, its value can immediately follow the option.

Whenever there is a conflict between a configuration file parameter and an option of the same meaning, the option 
always take precedence.

.. option:: -i, --read-dirfile <filepath>

    read list of input files from specified file; see the :ref:`timebat-input-files` section for details.
			
.. option:: -g, --make-dirfile <filepath>

    save list of output files to specified file, existing content of the file will be overwritten;
    see the :ref:`timebat-output-files` section for details.
			
.. option:: -o, --output-mask <mask>

    set output file mask (default=:file:`tout????.fts`), see the :ref:`timebat-output-files` section for details.
			
.. option:: -c, --counter <value>

    set initial counter value (default=1), see the :ref:`timebat-output-files` section for details.
			
.. option:: -h, --help

    print list of command-line parameters
			
.. option:: -q, --quiet

    quiet mode; suppress all messages
			
.. option:: --version

    print software version string
			
.. option:: --licence

    print software licence
			
.. option:: --verbose

    verbose mode; print debug messages
		
    
Examples
--------

::

	timebat -oout.fts 43.512 in.fts
	
The command shifts time of observation of CCD frame stored in the file :file:`in.fts` 
by 43 seconds and 512 miliseconds to the future. The output is written to file :file:`out.fts`.
			

Exit status
-----------

The command returns a zero exit status if it succeeds to process all specified files. Otherwise, it will stop 
immediately when an error occurs and a nonzero error code is returned.
