.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: match_stars_dialog.rst,v 1.2 2016/02/28 10:27:21 dmotl Exp $

.. index::
   pair: Match stars; dialog
   
.. _match-stars-dialog:

Match stars (dialog)
====================

The "Match stars" dialog is used to select a frame that shall be used during the matching process
as a reference. The matching process finds corresponding stars on each source frame with respect to
the reference frame.


Activating the dialog
---------------------

The dialog can be activated:

.. |matchstars_icon| image:: icons/matchstars.png

#. from the main menu: :menuselection:`Reduce --> Match stars`.

#. from the main toolbar: |matchstars_icon|


Stationary vs. moving targets
-----------------------------   
   
The first choice is about the character of the object of interest:

#. Stationary target - choose this option for objects that do not move during the
   observation with respect to the surrounding stars - variable star, exoplanet, etc.
   The software will ask you to pick up one of the frames as a reference frame; 
   you can also use a catalog file that you prepared while processing a previous observation
   of the same view field.
   
#. Moving target - choose this option for objects that changes significantly its position 
   during the observation with respect to the surrounding stars - minor Solar System body,
   for example. The software will ask you to select at least three frames, called key frames,
   and you will also need to mark the object on each of the key frames. 
   

Reference frame vs. catalog file
--------------------------------

If you process observation of a stationary object, you have also option what to use
as a reference for the matching process:

#. To pick one frame out of the source frame and use it as a reference. This frame is
   then referred to as a reference frame. There is no simple rule which frame you shall choose,
   selecting a frame with the greatest number of measured objects is usually a good starting
   point, though. The important idea is that the program can deal with a situation when a measured
   object is not present on all frames, but it must be present on a reference frame. Thus,
   make sure that at least the variable star and the comparison star are present of a reference
   frame.
   
#. To use a catalog file that has been made during processing of previous run of the
   same field. When you observe a particular variable star on regular basis, it's tedious
   to manually select stars and enter the position for every run. Utilizing the catalog
   files makes it simpler, because the application restores the information from a persistent
   file.

      
.. index::
   pair: Reference; frame
      
Stationary object - selecting a reference frame
-----------------------------------------------

.. figure:: images/matchstars_couts1.png
   :align: center
   :alt: Match stars dialog
   
   The dialog for selecting a reference frame

Check the option (1). The list of valid source frames is displayed here (2).

\(3) When you select a frame, the preview and short info is displayed here. Double
click on the preview to show a larger preview in a separate dialog.

Select a frame.

Click the "Execute" button (4) to start the matching.

.. index::
   pair: Catalog; file

Stationary object - browsing the catalog files
----------------------------------------------

.. figure:: images/matchstars_couts2.png
   :align: center
   :alt: Match stars dialog
   
   The dialog for selecting a catalog file

Check the option (10). The list of predefined catalog files is displayed here (11).

\(12) You can change the folder in which the files are looked for.

\(13) When you select a frame, the preview and short info is displayed here. Double
click on the preview to show a larger preview in a separate dialog.

Select a file. It is recommended to use a file that was carried out with the same color
filter as your source files, this is not required, though.

Click the "Execute" button (14) to start the matching.


.. index::
   single: moving target

Moving target - defining key frames
-----------------------------------

.. figure:: images/matchstars_couts3.png
   :alt: Matching (moving target)
   :align: center
   
   The Matching dialog in the moving target mode
   
In the dialog, check the option :guilabel:`Moving target` (20). 

List of source frames is presented in the table (21). 

When you select a frame, the preview is displayed in the preview area (22).

Click the :guilabel:`Add key frame` button (23) to add the selected frame to the list of key frames.

Use the :guilabel:`Set as a reference frame` button to define which key frame is used as 
a reference frame. The reference frame is always marked with a red key icon (24), while
other key frames are marked with a white key icon (25).

Click the :guilabel:`Remove key frame` to remove the selected frame from the list of key frames.

If you need to change the marked object on one of the key frames, click the :guilabel:`Change target object`.

Click the :guilabel:`Clear key frames` to clear the list of key frames and start again.

Click the "Execute" button (26) to start the matching.
   

Executing the matching
----------------------

Click the "Execute" button to start the matching.

- The photometry must be applied before the matching.

- The matching can be applied several times, the old cross-reference data are overwritten.


.. seealso:: :ref:`make-catalog-file-dialog`, :ref:`photometry-dialog`
