.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: finding_variables.rst,v 1.1 2015/07/06 08:19:48 dmotl Exp $

.. index::
   pair: Find;variables
   triple: finding; variable; stars

.. _finding-variables:   
   
Finding variables
=================

The CCD observation allows to measure brightness of all stars occurring
in the field of the telescope. It may happen, that on a series of frames that
apart from observed star there occurs also another variable star. By the
following steps, you can perhaps discover a new variable star.

The `Find variables` dialog is the useful tool that provides
semi-automatic scanning of variable stars in the series of source files. It
is based on the relation between the standard deviations of the brightness
of the stars and their mean brightness. The program reads all photometry files
and compute the mean brightness and standard deviations of brightness of all
stars. The algorithm automatically removes stars that are missing on majority
of source frames. For the star of lower magnitude the deviation of brightness
exhibits higher value than deviation for stars with a higher brightness.

First follow all steps of observed frames reduction through matching of
the photometry files. Click on the :menuselection:`Tools --> Find variables`
item in the main menu. The program show a simple dialog which allows you to
specify a source of data. It is possible either to use all frames that are
listed in the project or to use only frames that are currently selected in the
main window. The last option allows you to process the data stored in an external
file. Confirm the dialog by "OK" button.

.. figure:: images/varfind_make.png
   :align: center
   :alt: Find variables (data source)
   
   The dialog for selecting the data source for Find variables tool.

The program computes all stars brightness data at all frames and selects one
comparison star. The dialog window opens. At top left area a graph of standard
deviation of brightness vs. mean brightness or an object is shown. On the right part the identification chart with
variable star marked up is presented. Also the selected star light curve graph is
shown.

.. figure:: images/varfind.png
   :align: center
   :alt: Find variables
   
   The dialog for semi-automatic finding of variable stars.

In the top left area of the dialog, there is the graph of standard
deviation of brightness vs. mean brightness of an object. According to the law
of statistics, if all stars were constant, all objects would be located close
to the parabola-like shape that goes from the left bottom corner of the graph
to the right and turns up to its right top corner. If object's brightness changes,
the standard deviation is higher than the deviation of object of the same mean
brightness - these objects are usually located above the curve.

.. figure:: images/varfind_ok.png
   :align: center
   :alt: Mag-dev curve
   
   The detail of the mag-dev curve with a variable star highlighted.

Click at any point of the graph left. The program marks the point with
red circle with the :guilabel:`var` label. The same way, the corresponding
star is marked up on the identification chart. In the bottom part of the dialog
window, the light curve graph of the star is plotted. Sequentially indicate all
stars, you suspect to be a variable star.

It may happen, that the comparison star automatically chosen is not
good - it might not be constant. In this case the mag-dev curve has
different character:

.. figure:: images/varfind_badcomp.png
   :align: center
   :alt: Mag-dev curve (bad comparison)
   
   The detail of the mag-dev curve when the comparison star is not constant.

The comparison star is marked on the chart with green circle with the :guilabel:`comp`
label. To change the comparison star manually, click on :guilabel:`Comp`
button on the control panel and select the star on the identification chart.
When done, click on the :guilabel:`Var` button to switch back to variable
selection mode.

By the help of the buttons on the panel, the graph, chart or the light curve
can be saved as data or image file.


Exporting data
--------------

The program also provides means of exporting the brightness data of all
stars for all frames to a single text file. It is possible to import the data
to the Find variables tool later or process the file in an external program.

To export the data to a file, click on the :guilabel:`Save data`
button on the right side of the dialog. In the following dialog select a directory
and enter the name of output file where the data shall be saved to. Press the
:guilabel:`Save button`.


Processing data from an external file
-------------------------------------

The "Find variables" tool is capable of reading the data from an external
file. Such file can be created by the same tool from a set of reduced CCD frames
(see section Exporting data) or by an external program.

To import the data from a file, click on the :menuselection :`Tools --> Find variables` 
item in the main menu. It the following dialog, check the appropriate option and enter 
the path to the data file into the following edit box. Confirm the dialog by :guilabel:`OK`
button.

.. figure:: images/varfind_import.png
   :align: center
   :alt: Find variables (external file)
   
   Find variables - importing data from an external file.

   
Known variable stars
--------------------

If the source frames contain World Coordinate System (WCS) data, the software can show positions
of known variables on the frame. You have to specify paths to the catalog files in the program
settings (see :ref:`environment-options-dialog`). For each catalog, the toobar on the right side of the window
contains one check box that turns the catalog on and off. By clicking on the icon, a standard color selection
dialog appears.
   
.. seealso:: :ref:`find-variables-dialog`, :ref:`environment-options-dialog`.
