.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: image_file_window.rst,v 1.1 2015/07/06 08:19:48 dmotl Exp $

.. index::
   pair: Image file; window
   
.. _image-file-window:

Image file (window)
===================

This preview window is used to display the content of an external image file.


Activating the dialog
---------------------

The dialog can be activated:

#. from the main menu: :menuselection:`Tools --> Open file`.

When a file is opened, the program checks its content and decides which kind of
preview window will be activated. Each file is presented in a separate window.


Dialog controls
---------------

.. figure:: images/ccdfile_couts1.png
   :align: center
   :alt: CCD image file preview
   
   Preview window for CCD image files

\(1) The CCD image is displayed in the preview area. It is possible to switch between
positive and negative style in the "Preferences" dialog.

\(2) When you place a cursor over an image, the coordinates and the pixel value
is displayed in the status bar.

\(3) You can zoom the preview in and out by means of the zoom icons on the toolbar.
The actual magnification is shown here.

\(4) The local menu bar provides following functions:

- Menu File:

  - Open - open another file, this is an equivalent to selection :menuselection:`Tools --> Open file`
    from the main window.
    
  - Save as - save the image to a file in FITS format. To export the image in one of common raster
    image formats, use the 'Export'.
    
  - Export - export the image to a file in one of common raster image formats, currently the PNG and JPEG
    formats are supported. It is possible to adjust the size of the resulting image.
    
  - Show properties - display the further details about the file. Full header preview
    is available in separate window.

  - Close - close this window
  
  
- Menu View:

  - Rulers - turn on/off the rules that are shown on the top and left side of the preview area

  - Pseudo-color image - switch between rendering of pixel intensities in pseudo-color scale and a gray scale.
 
  
- Menu Image:

  - Horizontal flip - flip image from left to right
  
  - Vertical flip - flip image from top to bottom
  
  
- Menu Tools:

  - Gray scale - show a gray scale or a pseudo-color scale on the right side of the dialog.
  
  - Quick photometry - show the Quick photometry tool - explained below.
  
  - Profile - show the Profile tool - explained below.
  
  - Histogram - show the histogram and statistics.

    
.. index::
   pair: Quick photometry; tool

Quick Photometry tool
---------------------

The Quick Photometry tool is a tool which computes basic photometric properties
of an object on a frame. The main goal of this tool is to provide an estimation of
the FWHM value (the important input parameter of the photometry process).

Because this tool does simplified version of aperture photometry, the results
are informative only and shouldn't be used elsewhere. Also, it doesn't require a proper
calibration to be performed first.

To activate the tool, select :menuselection:`Tools --> Quick photometry`
from the local menu. A new panel on the right side of the preview window appears.


.. figure:: images/preview_couts3.png
   :align: center
   :alt: Quick photometry tool
   
   Quick photometry tool
  
Using the left mouse button, click on the image (1), close to an object. The program
automatically finds the nearest local maximum, so you don't need to be accurate. The computation
stars automatically and the results are displayed the the right panel (2).

The following object parameters are presented:

- Center - coordinates of the local maximum.

- Max - pixel value in local maximum

- Background - mean background level, computed as a robust mean of pixels in the sky annulus.

- Noise - background noise level, computed as a standard deviation of pixels in the sky annulus.

- FWHM - object's width, computed full width at half maximum.

- Aperture - aperture radius used for computing the object's signal.

- Signal - object's signal, sum of pixels in the aperture, mean background level is subtracted.

- S/N ratio - signal to background noise ratio, displayed in decibels: SNR = 10.log(S/N).

- \(3) Several dimensions are presented in the image area.

- Two blue circles show the annulus which is used to estimate the background properties.

- A red circle shows the object's average FWHM.

- A green circle shows the size of the aperture.

It is also possible to change some parameters:

- \(4) Aperture radius in pixels

- \(5) Inner radius of the sky annulus in pixels

- \(6) Outer radius of the sky annulus in pixels


.. index::
   pair: Profile; tools

Profile tool
------------

The Profile tool is used to check the shape of objects. It shows
a curve of pixels values along a line.

To activate the tool, select :menuselection:`Tools --> Quick photometry`
from the local menu. A new panel on the bottom of the preview window appears.

.. figure:: images/preview_couts2.png
   :align: center
   :alt: Profile tool
   
   Profile tool
   
Using the left mouse button, draw a line on the image (1), crossing the center of an object.
The profile is drawn in the graph (2). You can adjust the graph style (3). The following parameters
are displayed in the information box (4):

- Min - minimum pixel value.

- Max - maximum pixel value

- Mean - mean pixel value of pixels in the profile.

- St. dev. - standard deviation of pixels in the profile.


.. index::
   pair: Histogram; tool

Histogram tool
--------------

The Histogram tool shows a histogram of pixel values and some related
statistics.

To activate the tool, select :menuselection:`Tools --> Histogram`
from the local menu. A new panel on the bottom of the preview window appears.

.. figure:: images/preview_couts4.png
   :align: center
   :alt: Histogram tool
   
   Histogram tool

The histogram is shown in the left part (1) of the tool. The following parameters
are displayed in the text area on its right side (2):

- Min - minimum pixel value

- Max - maximum pixel value

- Mean - mean pixel value of all pixels

- St. dev. - standard deviation of all pixels


.. seealso:: :ref:`open-file-dialog`
