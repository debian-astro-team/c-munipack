.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: thumbnails_dialog.rst,v 1.1 2015/07/06 08:19:49 dmotl Exp $

.. index::
   pair: Thumbnails; dialog
   
.. _thumbnails-dialog:

Thumbnails (dialog)
===================

The "Thumbnails" dialog is used to conveniently review all frames that
are included in the project. The dialog allows an user to manually check the source
frames and remove bogus ones from the further processing.


Activating the dialog
---------------------

The dialog can be activated:

.. |iconview_icon| image:: icons/iconview.png

#. from the main menu: :menuselection:`Tools --> Show thumbnails`.

#. from the main toolbar: |iconview_icon|


Browsing the frames
-------------------

.. figure:: images/thumbnails_couts.png
   :align: center
   :alt: Thumbnails dialog
   
   The "Thumbnails dialog
   
\(1) The source frames are displayed here. The caption corresponds to the ordinal number of
a frame. Select a file with a single left click. You can use Shift and Ctrl
modifiers to select multiple files. Right-clicking a folder name opens a context menu.

Click on the "Remove from Project" button (3) to remove selected frame from the table
of source frames.

Click on the "Show Properties" button (2) to show further details about a selected frame.


.. seealso:: :ref:`main-window`
