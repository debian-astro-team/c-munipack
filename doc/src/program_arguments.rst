.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: program_arguments.rst,v 1.1 2015/07/06 08:19:49 dmotl Exp $

.. index::
   pair: program; arguments
   triple: command; line; arguments
   
.. program-arguments:

Program arguments
=================
        
The user command accept a list of command-line arguments, that provides requisite 
information for its execution. They are used to specify names of input files, values of
configuration parameters etc. Some arguments are obligatory, others are optional only.

The syntax of command-line arguments follows in general the GNU/POSIX conventions. 
The arguments are separated by a single space character, in case an argument itself contain 
a space, it must be enclosed in double-quotes. Refer to the documentation of your shell for 
more details.

There are two types of command-line arguments - options and positional parameters.
Options are optional, while positional parameters are always obligatory.

The options are used to provide extra information to customize the execution 
of a command. Each option has a full form starting with two dashes and an optional short 
form starting with one dash only. Options are case-sensitive. It is allowed to 
merge two or more successive short options together. Some options require a value; 
in this case a value is taken from a subsequent argument. When a full form is 
used, an option and its value can also be separated by an equal sign. When a 
short form is used, its value can immediately follow the option. Whenever there is a 
conflict between a configuration file parameter and an option of the same meaning, 
the option always take precedence.

All arguments that are not options nor option values are regarded as positional 
parameters. First such argument specify value of the first positional parameter and so on.
Some parameters take variable number of arguments, usually list of input files. This case 
is indicated in the synopsis by an ellipsis (...) following the parameter name. It means 
that it takes all following positional parameters.

All following user commands are equivalent::

   prog --option-a --option-b xyz --option-c=123 file1 file2
   prog -a -b xyz -c123 file1 file2
   prog -abxyz -c123 file1 file2

