.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: object_coordinates_dialog.rst,v 1.1 2015/07/06 08:19:49 dmotl Exp $

.. index::
   pair: Object coordinates; dialog

.. _object-coordinates-dialog:

Object coordinates (dialog)
===========================

The "Object coordinates" dialog is used to specify the celestial position of the observed
object. This information is necessary to compute the heliocentric correction and the air mass
coefficient.


Sources of coordinates
----------------------

There are several possibilities how to enter the object celestial position:

- To enter equatorial coordinates manually to edit fields in the dialog.

- To pick out an item from a table predefined positions. A user can add records to the table and edit them.

- To search out the position in a catalog of variable stars. The GCVS, NSV, NSVS and BRKA
  catalogs are supported. They are not included in the installation and must be installed and
  configured separately.
  
- To get the position from the reference frame, i. e. a source frame or a catalog file that
  has been used for the matching.

  
Specifying the position manually
--------------------------------

.. figure:: images/objcoords_couts1.png
   :align: center
   :alt: Object position (manual entry)
   
   Specifying the object position manually
   
Select the source of coordinates (1).

\(2) Enter object designation here. Although this field is not required, it is recommended
to fill in a meaningful object identification.

Enter object right ascension to the text box (3) in hours. Use the hexagesimal format, separate
the fields by a space character.

Enter object declination to the text box (4) in degrees. Use the hexagesimal format, separate
the fields by a space character.

\(5) Source and Remarks are optional fields. You can enter any information that you find useful.
There are no formatting rules for these fields.

Click on the "Add" button (6) to add an object into the table of predefined objects (7).

Click the "OK" button (8) to save the coordinates.


Using the table of predefined position
--------------------------------------

.. figure:: images/objcoords_couts2.png
   :align: center
   :alt: Table of predefined objects' positions
   
   Getting the object coordinates from the table of predefined positions

Select the source of coordinates (10).

The table of predefined positions is displayed in the table (11).
Right-clicking a table opens a context menu.

Select an item in the table with a single left click to recall the data.
The data are shown in the text boxes above the table (12).

Click the "Add" button (13) to make a new item to the table with data
filled in the text boxes. Click the "Save" (14) button to update selected record.
Click the "Remove" button (15) to remove the selected record from the table.

Click the "OK" button (16) to confirm the dialog.


Searching in a catalog of variable stars
----------------------------------------

The catalog files are not included in the program's installation package.
You have to download and install them separately.

.. figure:: images/objcoords_couts3.png
   :align: center
   :alt: Catalogs of variable stars
   
   Searching the variable star catalogs

Select the source of coordinates (20).

Enter the variable star designation into the text box (21). The search is
not case sensitive.

Click the "Search" button (22) to start the search. The records that match
are displayed in the table below (23).

Select an item in the table with a single left click. Right-clicking a table
opens a context menu.

Click the "OK" button (24) to save the coordinates.


Using position from the reference frame
---------------------------------------

This option is available only when the reference frame includes the information
about the position of the object.

.. figure:: images/objcoords_couts4.png
   :align: center
   :alt: Using the position from the reference frame
   
   Object coordinates from the reference frame

Select the source of coordinates (30).

\(31) The equatorial coordinates of the object is displayed in the text boxes.

Click the "OK" button (32) to save the coordinates.


.. seealso:: :ref:`match-stars-dialog`, :ref:`make-catalog-file-dialog`
