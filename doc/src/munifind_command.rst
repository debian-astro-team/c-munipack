.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: munifind_command.rst,v 1.1 2015/07/06 08:19:49 dmotl Exp $

.. program:: munifind

.. index::
   pair: munifind; command

.. _munifind-command:

munifind (command)
==================

utility for finding unknown variable stars


Synopsis
--------
			
munifind [ options ] *output-file* *input-files* ...


Description
-----------

The :command:`munifind` command reads matched photometry files and creates the table 
of standard deviations of magnitudes in the dependence on a mean magnitude. Such table is used to detect
unknown variable stars on a set of CCD frames. The table is written to a output file in text format.
		

.. _munifind-input-files:

Input files
-----------

Names of input files can be specified directly on a command-line as command arguments; it is allowed to use the 
usual wild-card notation. In case the input files are placed outside the working directory, you have to specify 
the proper path relative to the current working directory.

Alternatively, you can also prepare a list of input file names in a text file, each input file on a separate line. 
It is not allowed to use the wild-card notation here. Use the -i option to instruct the program to read the file.


Options
-------

Options are used to provide extra information to customize the execution of a command. They are specified as command 
arguments.

Each option has a full form starting with two dashes and an optional short form starting with one dash only. Options 
are case-sensitive. It is allowed to merge two or more successive short options together. Some options require a value; 
in this case a value is taken from a subsequent argument. When a full form is used, an option and its value can also 
be separated by an equal sign. When a short form is used, its value can immediately follow the option.

Whenever there is a conflict between a configuration file parameter and an option of the same meaning, the option 
always take precedence.

.. option:: -s, --set <name=value>

    set value of configuration parameter
			
.. option:: -a, --aperture <value>

    Aperture identifier (default=1)
			
.. option:: -c, --comparison-star <star>

    identifier of the comparison star or negative value for automatic detection (default=-1)
			
.. option:: -i, --read-dirfile <filepath>

    read list of input files from specified file; see the :ref:`munifind-input-files` section for details.
			
.. option:: -p, --configuration-file <filepath>

    read parameters from given configuration file. See the :ref:`munifind-configuration-file` section for 
    details.
			
.. option:: -h, --help

    print list of command-line parameters
			
.. option:: -q, --quiet

    quiet mode; suppress all messages
			
.. option:: --version

    print software version string
			
.. option:: --licence

    print software licence
			
.. option:: --verbose

    verbose mode; print debug messages
			
    
.. _munifind-configuration-file:

Configuration file
------------------

Configuration files are used to set the input parameters to the process that is going to be executed by a command. 
Use the -p option to instruct the program to read the file before other command-line options are processed.

The configuration file consists of a set of parameters stored in a text file. Each parameter is stored on a separate 
line in the following form: name = value, all other lines are silently ignored. Parameter names are case-sensitive.

.. confval:: aperture =  value

    Aperture identifier (default=1)
			
.. confval:: comp =  star

    identifier of the comparison star or negative value for automatic detection (default=-1)
			
.. confval:: threshold =  value

    cfraction of good measurements required, in percents (default=60)
			

Examples
--------

::
	munifind output.dat test1.mat test2.mat test3.mat
	
The command makes table of brightness of the star #2 (stored on second position
in photometry files) relative to the star #3 for photometry files :file:`test1.mat`, 
:file:`test2.mat` a :file:`test3.mat`; the resulting frame is stored 
to :file:`output.dat`.
	
		
Exit status
-----------

The command returns a zero exit status if it succeeds to process all specified files. Otherwise, it will stop 
immediately when an error occurs and a nonzero error code is returned.
