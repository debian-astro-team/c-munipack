.. C-Munipack - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: installation.rst,v 1.4 2016/02/27 09:14:01 dmotl Exp $

.. index::
   pair: Installation; instructions

.. _installation:
   
Installation and uninstallation
===============================

The latest version of the binaries and the source code can be obtained from the C-Munipack 
project home page: http://c-munipack.sourceforge.net/. Choose the binary or source package 
according to your platform and download it.


Using the installer on MS Windows
---------------------------------
		
.. note:: Since C-Munipack version 2.1, MS Windows 2000 are no longer supported.

Download the installer (.exe file) from the project `home page <http://c-munipack.sourceforge.net/>`_ and execute it. Follow
the instructions in the installation wizard. By default, the files are installed to the
Program Files folder. The software itself doesn't require the write access to the installation 
path and it doesn't read or modify the windows registry. All configuration files and other 
temporary files are stored in user's Application Data folder.

The installer package comprises of the both 32-bit and 64-bit executables. The installer automatically
chooses the executables which are appropriate to the host platform.

.. rubric:: Uninstallation

To uninstall the software open the Control Panel, then open the Add/Remove programs
tool. Select the C-Munipack 2.1 item and click on the "Remove" button.


.. index::
   pair: Source code; compiling

Building from the source code on POSIX-compliant systems
--------------------------------------------------------

Download the `source code <http://c-munipack.sourceforge.net/>`_ archive to your computer and 
unpack it using the tar command. The software comes with the GNU automake configuration scripts.


.. rubric:: Run the 'configure' script

First, run the configure script that is stored in the root directory. The script takes a while 
to finish. It automatically checks the dependencies and makes a set of Makefiles that in turn 
are called to build and install the binaries and the data files. Here is the list of the most 
frequently used command-line options that the configure script accepts.
		
.. program:: configure

.. option:: --help

   Print the complete set of command-line options.
   
.. option:: --prefix=path

   Install the software in subdirectories below prefix. The default value of prefix is /usr/local.
		
The following options extends the default configure's set:

.. option:: --disable-gui

   Do not build the graphical user interface (Muniwin).
   
.. option:: --disable-toolkit

   Do not build the set of user commands (toolkit).

.. option:: --disable-sound

   Do not build the software with the support for sounds that are played when a process is finished. 
   Use this option if you do not have the gstreamer library.
   
.. option:: --disable-wcs

   Do not build the software with the Woorld Coordinate System (WCS) support. 
   Use this option if you do not have the wcslib library.
   
   
.. rubric:: Build and install   
   
Run 'make'. Once you have successfully compiled the software, run 'make install'. 

.. note:: On some systems, it is necessary to run 'ldconfig' after 'make install'.


.. rubric:: Files and directories

The 'make install' command installs a the binaries, the data and the user manual to the 
destination directory, the files is installed into following directories. In the list 
below, the prefix stands for the installation path, the default prefix is /usr/local.

.. object:: prefix/bin
   
   The binaries
   
.. object:: prefix/lib

   The libraries that are used to build applications using the C-Munipack API
   
.. object:: prefix/include

   The header files that are used to compile applications using the C-Munipack API
   
.. object:: prefix/man

   The Unix-style man pages
   
.. object:: prefix/share/c-munipack-2.0

   The icons, sounds and documentation

The configuration files are stored per user within the directory that is reserved
for the user-specific application configuration information. The path is determined 
using the mechanisms described in the XDG Base Directory Specification. 

.. object:: $HOME/.config/C-Munipack-2.0

   The configuration files
   
.. object:: $HOME/.local/share/C-Munipack-2.0

   The default directory for catalog files, projects, etc.
