.. C-Munipack - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: heliocentric_correction.rst,v 1.2 2015/07/12 07:44:57 dmotl Exp $

.. _heliocentric-correction: 

.. index::
   pair: heliocentric; correction
   triple: Julian; date; heliocentric
   
Heliocentric correction
=======================

The heliocentric correction compensates for the differences in the observer's distance
to the observed object due to movement of the Earth around the Sun. When we measure
the time that elapsed between two events and the time span is in the order of days
or more, we may need to compensate for the timing due to the finite speed of light. A common 
approach is to transform the observation time as it would be observed from the center 
of the Sun, which is considered to be fixed with respect to the object. The Julian 
date JD is transformed into Heliocentric Julian date HJD.
The difference between those two dates is called the heliocentric correction HC 
and usually it is expressed in days.

.. math::
   :label: HJD
   
	HJD = JD + HC

Figure 1 illustrates the situation. The heliocentric correction HC is the 
time that light needs to cover the distance :math:`\|\vec{r}\|`. The vector :math:`\vec{r}` can 
be thought of as a projection of the Earth-Sun vector :math:`\vec{s}` into the direction 
of Earth-object unit vector :math:`\vec{n}`. 

.. figure:: images/helcorr.png
   :align: center
   :alt: Heliocentric correction
   
   Vectors used to derive heliocentric correction. *s*: Earth-Sun vector;
   *n*: Earth-object unit vector; *r*: projection of *s* into direction of *n*

The length of the vector :math:`\vec{r}` can be computed as:

.. math::
   :label: r
   
	\|\vec{r}\| = \|\vec{s}\| \cos\theta

The cosine of angle :math:`\theta` can be derived from the expression for dot product
of two vectors:

.. math::
   :label: a_b
   
	\vec{a}\cdot\vec{b} = \|\vec{a}\|\,\|\vec{b}\|\,\cos\theta

When we put the two equations together, we get 

.. math::
   :label: r2
   
	\|\vec{r}\| = \|\vec{s}\| \frac{\vec{s}\cdot\vec{n}}{\|\vec{s}\|\,\|\vec{n}\|}

the length of vector :math:`\vec{s}` cancels out and because the vector :math:`\vec{n}` is
a unit vector, its length is equal to 1. Then we can simplify the previous equation to

.. math::
   :label: r3
   
	\|\vec{r}\| = \vec{s}\cdot\vec{n}

Vector :math:`\vec{s}` is computed using Flandern's formulas for Earth-Sun distance *R_S*
and Sun's ecliptic longitude :math:`\lambda_S` (see [flandern79]_).

The object's position is specified by two equatorial coordinates - right ascension
:math:`\alpha` and declination :math:`\delta`. These coordinates must be transformed into the
ecliptic coordinate system to get object's ecliptic longitude :math:`\lambda` and latitude
:math:`\beta`.

The easiest way to compute the dot product of two vectors in polar coordinates
is to transform them into the 3-D Cartesian system first, using a trigonometric identity
for cosine of the sum of two vectors we get

.. math::
   :label: r_s
   
	\|\vec{r}\| = R_S\,\cos\beta\,\cos(\lambda-\lambda_S)

Using a speed of light :math:`c = 299,792,458` meters per second we can derive the formula
for the Heliocentric correction. Please note, that it is necessary to convert the
units to get the value in days. 

.. math::
   :label: hc
   
	HC = \frac{R_S}{c}\,\cos\beta\,\cos(\lambda-\lambda_S) = -0.00577552\,R_S\,\cos(\beta)\,\cos(\lambda-\lambda_S)
 