.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: light_curve_dialog.rst,v 1.3 2016/01/02 09:53:13 dmotl Exp $

.. index::
   pair: Light curve; dialog
   
.. _light-curve-dialog:

Light curve (dialog)
====================

The "Light curve" dialog is used to make a light curve of an object. The light curve is
common output of the reduction of CCD frame.


Activating the dialog
---------------------

The dialog can be activated:

.. |lightcurve_icon| image:: icons/lightcurve.png

- from the main menu: :menuselection:`Plot --> Light curve`.

- from the main toolbar: |lightcurve_icon|

The "Plot light curve" dialog appears. Check the options. Fill in the object coordinates and the
observer coordinates, if necessary. Confirm the dialog.



Light curve dialog
------------------

.. figure:: images/lightcurve_couts.png
   :align: center
   :alt: Light curve
   
   Light curve

\(1) The actual data set is shown here.

\(2) You can switch the labels on the X axis between Julian date (JD) or date and time (UTC).

\(3) It is also possible to switch between apertures.
   
\(4) The list of available data sets is displayed here.
   

Data sets
---------

Depending on the type of the light curve and the status of the options, the following
data sets are available:

.. rubric:: Differential magnitudes

*  *V-C* - Differential magnitude of the variable star w.r.t. the comparison star

*  *V-Kn* - Differential magnitude of the variable star w.r.t. the n-th check star

*  *C-Kn* - Differential magnitude of the comparison star w.r.t. the n-th check star

.. rubric:: Ensemble photometry

*  *V-C* - Differential magnitude of the variable star w.r.t. the artificial comparison star

*  *V-Kn* - Differential magnitude of the variable star w.r.t. the n-th check star

*  *Cm-Kn* - Differential magnitude of the m-th comparison star w.r.t. the n-th check star

.. rubric:: Instrumental magnitudes

*  *V* - Instrumental magnitude of the variable star

*  *C* - Instrumental magnitude of the comparison star

*  *Kn* - Instrumental magnitude of the n-th check star

.. rubric:: All stars (differential)

*  *IDnnn-C* - Differential magnitude of the object with ID = *nnn* w.r.t. the comparison star

.. rubric:: All stars (instrumental)

*  *IDnnn* - Instrumental magnitude of the object with ID = *nnn*

.. rubric:: Auxilliary data sets

The following data sets are optional:

*  *HELCOR* - The heliocentric correction; in days

*  *AIRMASS* - The air mass coefficient

*  *ALTITUDE* - The apparent altitude in degrees; positive values above the horizon, negative values below the horizon


Context menu
------------

You can select an individual point by a right click, you can also
select more than one point by pressing a Shift key and left mouse button
and drawing a rectangle in the graph. Then, click the right mouse button
on a point in the selection to open the context menu. It provides following
functions:

- Show frame - it shows a preview to a selected frame. It is not allowed
  when more than one frame is selected.
  
- Show properties - it opens a new dialog with properties of selected frame.
  It is not allowed when more than one frame is selected.
  
- Delete from data set - selected measurements are removed from the
  current curve, the data will be shown again when you make a new curve
  or :guilabel:`Rebuild` the actual curve.
  
- Remove from project - source frames corresponding to the selected
  measurements are removed permanently from the list of input files and
  such measurements won't be included in any other output. It is not allowed
  to remove a reference frame.
  
  
Statistics
----------

The Statistics is a tool that computes and shows the minimum, maximum, sample mean
and standard deviation. 

To activate the tool:

#. From the local menu, select :menuselection:`Tools --> Statistics`.
   A new panel on the right side of the preview window appears.

If no points are selected, all points in the data set are included in the computation. 
To restrict the data for the statistics, press and hold the Shift key and draw a 
rectangle in a graph while you keep the left mouse button pressed down.


Measurement
-----------

The `Measurement` tool displays two cursors in the graph. The cursors can be adjusted
by dragging them using the left mouse button. The position of each cursor, their distance
and statistics for the data between cursors is presented.

To activate the tool:

#. From the local menu, select :menuselection:`Tools --> Measurement`.
   A new panel on the right side of the preview window appears.

   .. figure:: images/measurement.png
      :align: center
      :alt: Measurement tool
   
   Measurement tool

\(1) Choose the axis you want to measure.

\(2) Positions of the cursor 1 and 2 are displayed here.

\(3) Distance between cursor 1 and 2.

\(4) When cursors are defined on the independent (X) axis, number of points (frames) between 
cursor 1 and 2 are displayed and also minimum, maximum, mean value and sample deviation
are presented.
 
   
.. seealso:: :ref:`chart-dialog`, :ref:`aavso-format`
