.. C-Munipack - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: profiles.rst,v 1.2 2016/02/27 09:14:01 dmotl Exp $

.. index::
   single: profile
      
.. _profiles:

Profiles
========

The profile provides a set of configuration parameters that are used to process the data. Unlike the 
:ref:`project <projects>`, it does not contain the data - image files and photometry files. The profiles
are used to transfer the configuration parameters between projects. Once you invent the best configuration
parameters for your observations, save them as a user-defined profile. Next time, when you are going
to do the same task with another frame set, choose the profile in the :ref:`New project <new-project-dialog>`
dialog; the project is created with settings restored from the profile. When you process the data from
multiple instruments, you can create multiple user-defined profiles, each of which fits the particular
instrument.

The description of the individual parameters can be found in :ref:`this chaper <project-settings>`.


.. index::
   pair: profile; create
   
Creating user-defined profiles
------------------------------

A new profile can be created using the following steps:

\(1) Start with a project - create a new project or open an existing one. Set up the project settings.

\(2) From the main menu select :menuselection:`Project --> Edit properties`.

\(3) Go to the root page (called `Project ...`).

.. figure:: images/profiles_couts1.png 
   :align: center
   :alt: Project settings dialog
   
   Project settings dialog

\(4) Click the `Save as profile` button (1). A new dialog appears. Enter the name of the profile (2).

.. figure:: images/save_profile_couts.png 
   :align: center
   :alt: Save as profile dialog
   
   Save as profile dialog

\(5) Confirm the dialog.


Using user-defined profiles to create a new project
---------------------------------------------------

The projects settings for a new project can be restored from the user-defined profile:

From the main menu select : :menuselection:`Project --> New`. A new dialog appears.

\(1) Enter a project name. 

\(2) Adjust location of the new project if necessary. 

\(3) List of existing profiles is shown in the table. The profiles are divided into two sections -
     user-defined profiles and predefined profiles. Choose the profile and confirm the dialog.

.. figure:: images/newproject_couts2.png
   :align: center
   :alt: New project dialog
   
   New project dialog


.. index::
   pair: profile; edit
   
Editing user-defined profiles
-----------------------------

The user-defined profiles can be edited and managed by the :ref:`Edit profiles <edit-profiles-dialog>` dialog.

\(1) From the main menu select :menuselection:`Tools --> Edit profiles`. A new dialog appears.

.. figure:: images/profiles_couts3.png
   :align: center
   :alt: Edit profiles dialog
   
   Edit profiles dialog

The dialog is divided into three sections. The table on the left presents a list of existing profiles. 
They are divided into two parts - used-defined profiles (1) and predefined profiles (2). Unlike
the user-defined profiles, the predefined profiles are created at the program installation and they 
cannot be modified.

When a profile is selected, the rest of the dialog shows the configuration parameters of the selected profile.
The central sections of the dialog (3) shows a structure of pages which the parameters are divided to. Select
a page to show the configuration parameters (4).

Each page has got the :guilabel:`Set defaults` button on it. Click the button to reset the parameters
on the current page to the default values.
   

.. index::
   pair: profile; load

Loading settins from a profile to an existing project
-----------------------------------------------------

To load a the settings from a profile to an existing project:

\(1) From the main menu select :menuselection:`Project --> Edit properties`. A new dialog appears.

\(2) Go to the root page (called `Project ...`).

.. figure:: images/profiles_couts4.png
   :align: center
   :alt: Project settings dialog
   
   Project settings dialog

Click the `Load from profile` button (10). A new dialog appears. Choose the profile that shall 
be loaded into the current project. Confirm the dialog.

.. figure:: images/profiles_couts2.png
   :align: center
   :alt: Load profile dialog
   
   Load profile dialog

Files and directories
---------------------

The user's folder for application data is supplied just as a default location for the user-defined
profiles (actual folder depends on the operating system). Each profiles is stored as a separate file.

Example of the default location on Windows:

:file:`C:\\Users\\your name\\Application data\\C-Munipack-2.0\\Projects`

You can change the path using the :ref:`Environment options <environment-options-dialog>` dialog, 
the section :guilabel:`Files and directories`:

.. figure:: images/profiles_path.png
   :align: center
   :alt: Environment options dialog
   
   Environment options dialog

Check the option (1) to tell the software to use user-supplied path to the directory with user-defined profiles.
Specify the path to the edit box below (2). If the option is not checked, the program uses the default location
(see above).  
   
.. seealso:: :ref:`projects`, :ref:`project-settings-dialog`, :ref:`new-project-dialog`, :ref:`load-profile-dialog`,
   :ref:`save-profile-dialog`, :ref:`edit-profiles-dialog`, :ref:`import-profile-dialog`
