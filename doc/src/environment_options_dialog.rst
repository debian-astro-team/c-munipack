.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: environment_options_dialog.rst,v 1.1 2015/07/06 08:19:48 dmotl Exp $

.. index::
   pair: Environment options; dialog

.. _environment-options-dialog:
   
Environment options (dialog)
============================

The "Environment options" dialog is used to customize the user interface. These
options do not affect the data processing. The data processing parameters are placed
in the separate dialog "Project options".


Activating the dialog
---------------------

The dialog can be activated:

#. from the main menu: :menuselection:`Tools --> Environment options`.


.. index::
   pair: environment; options

Customizing the user interface
------------------------------

.. figure:: images/environ_couts1.png
   :align: center
   :alt: Environment options dialog
   
   The "Environment options" dialog
   
\(1) Select a category.

Options in the selected category are shown on the right pane (2).

Click "Set defaults" button (3) to set the options in the selected
category to the default values.

Click the button (4) to save the settings.


.. seealso:: :ref:`project-settings-dialog`
