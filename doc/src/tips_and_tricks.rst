.. C-Munipack - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: tips_and_tricks.rst,v 1.2 2016/02/27 09:14:01 dmotl Exp $

Tips and tricks
===============

I need to move my projects to another drive
-------------------------------------------

In case the current folder where the projects are stored in does not suit you, do the following steps:

#. Close the program

#. Move the content of the current folder with the existing projects into another location.

#. Next time you create a new project using the :ref:`new-project-dialog`, change the location for the 
   new project.

The is no special configuration option that needs to be adjusted to change the location of your 
project files.


How to back up a project
------------------------

No special action is necessary to make a copy of a project; copy a project file, a directory 
with the "-files" suffix and all files that resides in it.

Restoring is also easy; just copy the files and directories to a folder and make sure that
you have the 'read-write' access to the project file, the directory with "-files" suffix
and also to all files that resides in it. When you open a project from a read-only location,
e.g. DVD, the program detects the situation and gives a warning; you can open a project in 
'read-only' mode; in this mode, it is allowed to make outputs, e.g. light curve, but 
operations that would change the data are not allowed.
