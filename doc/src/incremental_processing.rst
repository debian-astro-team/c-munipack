.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: incremental_processing.rst,v 1.2 2016/02/27 09:14:01 dmotl Exp $

.. index::
   pair: Incremental; processing
   pair: process; new frames

.. _incremental-processing:

Incremental processing
======================

The incremental processing is a feature that allows an user to merge
new files into an existing project. The new frames are automatically reduced
using the same settings as the frames that are already in the project. It
is particularly useful in an observation of short-period variables. Using
this tool, it is possible to monitor the variable behavior during the observation
and eventually decide, by checking a light curve, whether to resume or stop
it.

During the first run, process the CCD frames by the standard procedure
up to the selection of stars and produce a light curve. After acquiring
a set of new frames, click on the :menuselection:`Reduce --> Process new files` 
item in the main menu. A new dialog window appears.
Select the option :guilabel:`process new files in the directory`
and in the :guilabel:`Directory` field, select the directory which
the new files are stored in. You can also setup file filtering conditions
using the fields in the bottom part of the dialog. Only files that meet those
conditions will be merged into the project.

If you check the option :guilabel:`Periodically check...`, the program
will monitor the specified path on background and whenever new files turn up,
the program automatically appends them to the list and processes them.

.. seealso:: :ref:`process-new-frames-dialog`