.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: observer_coordinates_dialog.rst,v 1.1 2015/07/06 08:19:49 dmotl Exp $

.. index::
   pair: Observer coordinates; dialog
   
.. _observer-coordinates-dialog:

Observer coordinates (dialog)
=============================

The "Observer coordinates" dialog is used to specify the geographic coordinates
of the observed object. This information is necessary to compute the air mass coefficient.


Sources of coordinates
----------------------

There are several possibilities how to enter the observer geographic location:

- To enter geographic coordinates manually to the edit fields in the dialog.

- To pick out an item from a table predefined locations. A user can add records to the table and edit them.

- To use the default location. The default location can be changed in the "Preferences" dialog.

- To get the location from the reference frame, i. e. a source frame or a catalog file that
  has been used for the matching.
  

Specifying the location manually
--------------------------------

.. figure:: images/obscoords_couts1.png
   :align: center
   :alt: Observer coordinates (manual entry)
   
   Specifying the observer location manually
   
Select the source of coordinates (1).

\(2) Enter location designation here. Although this field is not required, it is recommended
to fill in a meaningful object identification.

Enter observer longitude to the text box (3) in degrees. Use the hexagesimal format, separate
the fields by a space character. Enter 'E' character at the first position in a string to indicate
that the location is on the eastern hemisphere or 'W' character for locations on a western hemisphere.

Enter observer latitude to the text box (4) in degrees. Use the hexagesimal format, separate
the fields by a space character. Enter 'N' character at the first position in s string to indicate
that the location is on the north hemisphere or 'S' character for locations on a southern hemisphere.

\(5) Remarks is an optional field. You can enter any information that you find useful.
There are no formatting rules for this field.

Click on the "Add" button (6) to add an object into the table of predefined objects (7).

Click the "OK" button (8) to save the coordinates.


Using the table of predefined locations
---------------------------------------

.. figure:: images/obscoords_couts2.png
   :align: center
   :alt: Observer coordinates (predefined locations)
   
   Getting the observer coordinates from the table of predefined locations
   
Select the source of coordinates (10).

The table of predefined locations is displayed in the table (11).
Right-clicking a table opens a context menu.

Select an item in the table with a single left click to recall the data.
The data are shown in the text boxes above the table (12).

Click the "Add" button (13) to make a new item to the table with data
filled in the text boxes. Click the "Save" (14) button to update selected record.
Click the "Remove" button (15) to remove the selected record from the table.

Click the "OK" button (16) to confirm the dialog.


Using the default location
--------------------------

The default geographic coordinates can be adjusted in the "Preferences" dialog.

.. figure:: images/obscoords_couts3.png
   :align: center
   :alt: Observer coordinates (default location)
   
   Using the default geographic location

Select the source of coordinates (20).

\(21) The default geographic coordinates are displayed in the text boxes.

Click the "OK" button (22) to save the coordinates.


Using location from the reference frame
---------------------------------------

This option is available only when the reference frame includes the information
about the location of the observer.

.. figure:: images/obscoords_couts4.png
   :align: center
   :alt: Observer coordinates (from reference frame)
   
   Observer coordinates from the reference frame

Select the source of coordinates (30).

\(31) The geographic coordinates of the observer is displayed in the text boxes.

Click the "OK" button (32) to save the coordinates.


.. seealso:: :ref:`match-stars-dialog`, :ref:`make-catalog-file-dialog`
