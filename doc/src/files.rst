.. C-Munipack - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: files.rst,v 1.2 2015/07/12 07:44:57 dmotl Exp $

.. _files: 

File format specification
=========================

In this chapter, you will find the description of file formats that are created
and used by the C-Munipack software. 

All keywords and invariant phrases, which are included directly in the files 
are printed in *cursive font*. Names of files and directories are printed in 
``teletype face``. Default values of parameters are places in the [square brackets].

.. toctree::
   :maxdepth: 1

   photometry_files_format
   catalog_files_format
   light_curve_format
   track_curve_format
   air_mass_curve_format
   varfind_curve_format
   readall_file_format