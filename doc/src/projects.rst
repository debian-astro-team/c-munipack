.. C-Munipack - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: projects.rst,v 1.3 2016/02/27 09:14:01 dmotl Exp $

.. index::
   single: project
   
.. _projects:   
   
Projects 
========

The concept of projects was introduced to retain the products of the CCD frame calibration
and reduction even when another data sets were processed; it allows one to revert back to 
the data that have been already processed without necessity of processing them again.

The project consists of:

* Project settings - a set of configuration parameters

* Image files - one image file per frame. These are created as copies of the source frames,
  they are modified during the reduction process by applying various transformations and 
  calibration steps.
  
* Photometry files - one photometry file per frame. The photometry files contain the list
  of detected objects and their properites - coordinates, brightness, etc.

  
Project settings
----------------

All configuration parameters that affect the data processing are retained in the project,
allowing one to work with multiple configurations efficiently. When the projects settings
are changed, the program updates the project. There is one exception to this rule - if
the project is opened in the 'read-only' mode, the new settings are kept in the memory,
but the project file is not changed.

When a new project is created, the initial project settings is loaded from a template, called 
the :ref:`profile <profiles>`. The installation package comes with a set of predefined profiles,
but the user can save his own ones. See the :ref:`profiles` section for more information.

The description of the individual parameters can be found in :ref:`this chaper <project-settings>`.

  
Project types
-------------

The project type was introduced to optimize the user interface when a specific task is performed - 
for example, when one is making a master flat frame, some of the tools, like the light curve dialog 
or the find variables dialog, can be kept from the menus and the toolbar, because they are not used 
in this context. The :ref:`Project settings dialog <project-settings-dialog>` is affected by the 
choice of the project type, too. The dialog presents only the parameters that are used in the 
selected task.


Managing projects
-----------------

The Muniwin program allows one:

* To create a new project - :ref:`new-project-dialog`
* To open an existing project - :ref:`open-project-dialog`
* To rename the opened project using the :ref:`Project settings dialog <project-settings-dialog>`
* To make a copy of a project - :ref:`export-project-dialog`
* To import data from old C-Munipack into a new project - :ref:`import-project-dialog`


Files and directories
---------------------

Regarding the data storage, the program is not limited to any specific folder. The user's 
folder for application data is supplied just as a default location (actual folder depends on the 
operating system). A user can choose any folder that suits him. There is no special configuration 
option that needs to be adjusted, the :ref:`new-project-dialog` allows one to choose any 
location for the new project and the :ref:`open-project-dialog` also allows one to choose 
a project in any folder. The data can be stored on an external drive or shared using a network 
storage. In case of a network storage, the program takes uses a locking mechanism (see below) 
to ensure that two users does not make changes to a project at the same time.

Example of the default location on Windows:

:file:`C:\\Users\\your name\\Application data\\C-Munipack-2.0\\Projects`

A project consists of two components: a project file - it can be recognized by the extension 
'cmpack', e.g. 'GSC 2750 854.cmpack' and a related folder that consists of the calibrated images, photometry 
files, etc. This folder has the same name as the project file plus the extension '-files', 
e.g. 'GSC 2750 854.cmpack-files'. These two (file + folder) must always go together. All references 
inside a project are relative, therefore you can move or copy a project or a bunch of projects 
just by moving or copying it's folder to any location. 


Project sharing
---------------

When a project is opened more than once, the program detects this situation and notifies
a user. It allows one to open the project in the 'read-only' mode; in this mode, it is
allowed to make outputs, e.g. light curve, but operations that would change the data
files are disallowed. The file-based locking mechanism used should work over the network
in most situation, although this is not guaranteed in all configurations.


.. seealso:: :ref:`profiles`, :ref:`project-settings-dialog`, :ref:`new-project-dialog`, :ref:`load-profile-dialog`
