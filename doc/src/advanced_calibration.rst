.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: advanced_calibration.rst,v 1.1 2015/07/06 08:19:48 dmotl Exp $

.. index:: 
   pair: Advanced calibration; scheme

.. _advanced-calibration-scheme:
 
Advanced calibration scheme
===========================

The standard calibration of CCD frames uses single dark frame and single flat frame.
To obtain reasonable results, it is necessary to use dark frame of the same exposure duration
as the scientific images. If you observe several objects during a night and those objects have
different magnitude, different exposure durations are often used. But in this case you will
need to carry out as many sets of dark frames as the number of exposures you have chosen.
This task may be quite time consuming work. This disadvantage can be solved by advanced
calibration, which involves scalable dark frames. The reduction process in this case is a
bit more complicated than standard one, so it is recommended to experienced users only.


.. index:: 
   pair: calibration; procedure

Calibration procedure
---------------------

#. Acquire a set of bias frames. They are carried out by the similar way as the
   dark frames, the exposure duration should be minimal (zero in ideal case). I use 100 frames. 
   The bias frames can be reused till some change to the camera hardware was made, so you need 
   to acquire the set only once.

#. Acquire a set of flat frames. An easy way is to acquire frames of short exposure 
   duration during dusk with a telescope pointed towards east, not very low altitude.
   Move the telescope during exposure rapidly. The pixels on a flat frame shouldn't have 
   overexposed pixels and you should not see stars (bright lines if you move the telescope
   during exposure) on them when you check them in the preview window. I take 20 frames, one second each.
      
#. Each night, acquire the set of dark frames using long exposure duration. It
   is recommended to carry out dark frames on the end of observation, when the camera
   is enough uniformly cooled. I take ten frames, 120 seconds each.
   
#. Create a new project of :guilabel:`Master bias frame` type. Add your bias frames, convert them 
   and make a master bias frame. Save it to a file.

#. Create a new project of :guilabel:`Master dark frame` type. Open menu :menuselection:`Project --> Edit properties`. 
   Select the :guilabel:`Calibration` page and check the option :guilabel:`Advanced calibration scheme`. 
   Add your dark frames, convert them, apply master bias frame and make a master dark frame. 
   Save it to a file.
   
#. Create a new project of :guilabel:`Master flat frame` type. Open menu :menuselection:`Project --> Edit properties`. 
   Select the :guilabel:`Calibration` page and check the option :guilabel:`Advanced calibration scheme`. 
   Add your flat frames, convert them, apply bias and dark corrections and make a master flat frame. 
   Save it to a file.

#. Create a new project of :guilabel:`Light curve` type. Open menu :menuselection:`Project --> Edit properties`. 
   Select :guilabel:`Calibration` page and check the option :guilabel:`Advanced calibration scheme`. 
   Add your sky frames, apply bias, dark and flat corrections.

Continue in reduction of source images by standard way.


Remarks
-------

- The :index:`scalable dark frame <triple: scalable; dark; frame>` is automatically detected using 
  the information stored in the file header. For all other frames which are not considered to be scalable ones,
  the standard calibration is used.

 
.. seealso:: :ref:`bias-correction-dialog`, :ref:`master-bias-frame-dialog`, :ref:`dark-correction-dialog`, 
   :ref:`master-dark-frame-dialog`, :ref:`project-settings-dialog`
