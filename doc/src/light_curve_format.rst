.. C-Munipack - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: light_curve_format.rst,v 1.4 2016/01/02 09:53:13 dmotl Exp $

.. index::
   pair: light curve; format
      
.. _light-curve-format:

Light curve files
=================

Light curves (dependency of brightness on observation time) are usually final
product of the reduction process. A light curve table consists of observation time 
in Julian date form followed by magnitudes and their errors of all selected stars.
Two basic output working modes are provided: in differential mode, magnitudes stored
in the table are differences between each pair of the stars. On the other hand,
instrumental mode allows you to print out raw values computed by photometry. Note,
that instrumental magnitudes cannot be compared to absolute ones, which is found
in the photometry catalogs, without further post-processing.

.. rubric:: Format C-Munipack (default)

Output files are stored in the ASCII format; the end of the line is represented by 
``CR+LF`` in the DOS/MS Windows environment and by ``LF`` in the Unix/Linux 
environment. First line contains always a list of column names separated by single 
space character. Second line consists of additional information (aperture, filter, etc.)
and has no special formatting, it must not start by number, though. 

On the following lines, the table values are stored. The values are separated by tab 
character or single space, rows are separated by the end-of-line character (see above). 
Parsers must ignore all additional white characters. Empty lines indicates, that the 
corresponding frame was not successfully processed and thus a brightness of a variable 
or a comparison star could not be determined. See table :ref:`1 <table:dat_head>` for short 
description of columns.

.. _table:dat_head:

Table 1

======== ===========
Keyword  Description
======== ===========
JD       Geocentric Julian date of observation
JDHEL    Heliocentric Julian date of observation
-------- -----------
V-C      Difference of variable and comparison star
s1       Error of V-C value
V-K1     Difference of comparison and check star #1 
s2       Error of C-C1 value
...      ...
-------- -----------
V        Brightness of variable star (abs. instr. magnitude)
s1       Error of V value
C        Brightness of comparison star
s2       Error of C value
K1       Brightness of check star #1
s3       Error of K1 value
...      ...
-------- -----------
IDnnn-C  Difference of object with ID nnn and the comparison star
...      ...
-------- -----------
IDnnn    Brightness of object with ID nnn (abs. instr. magnitude)
...      ...
-------- -----------
HELCOR   Heliocentric correction in days
AIRMASS  Air mass coefficient
ALTITUDE Altitude (elevation) in degrees above horizon
======== ===========


.. rubric:: Format AVE compatible

The "AVE compatible" file format allows to save
a light curve with differential magnitudes to the AVE software [barbera11]_.
The output file is an ASCII text file. Each frame is stored on a separate line,
lines begin with a Julian date of observation, followed by a space and followed by a brightness
of a variable star in magnitudes. Frames where a variable was not measured are
omitted. The file does not contain any header.

.. rubric:: Format MCV compatible

The "MCV compatible" file format allows to save instrumental magnitudes of
selected stars in a file that can be processed in the MCV software [andronov04]_.
The output file is an ASCII text file. Each frame is stored on a separate line,
lines begin with a Julian date of observation, followed by instrumental magnitude
for each selected star. Fields are separated by a space character. A zero value
stored instead of a valid magnitude indicates missing or invalid data. The file 
does not have any header.

.. seealso:: :ref:`light-curve`, :ref:`light-curve-dialog`, :ref:`aavso-format`