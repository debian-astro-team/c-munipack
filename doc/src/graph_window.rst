.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: graph_window.rst,v 1.1 2015/07/06 08:19:48 dmotl Exp $

.. index::
   pair: Graph; window

.. _graph-window:

Graph (window)
==============

This preview window is used to display the content of an external file.


Activating the dialog
---------------------

The dialog can be activated:

#. from the main menu: :menuselection:`Tools --> Open file`.

When a file is opened, the program checks its content and decides which kind of
preview window will be activated. Each file is presented in a separate window.


Dialog controls
---------------

.. figure:: images/table_couts1.png
   :align: center
   :alt: Graph preview
   
   Preview window for light curves, track-lists, etc.
   
\(1) The data are displayed in the preview area. It is possible to switch between
two rendering modes - displying file as a graph (plot) or as a table.

\(2) When you place a cursor over a data point, the values are displayed in the status bar.

\(3) You can zoom the preview in and out by means of the zoom icons on the toolbar.

If the file contains multiple columns, you can use the controls (4) and (5) to switch 
the displayed data.

The local menu bar provides following functions:

- Menu File:

  - Open - open another file, this is an equivalent to selection :menuselection:`Tools --> Open file`
    from the main window.

  - Export - depending on the current display mode:
  
    - If a graph is shown, the function export the graph to a file in the PNG format. 
    
    - If a table is shown, the function export the table to a file in the CSV or TEXT format.
      The save dialog provides several options, that allows adjusting the content of the file.

  - Show properties - display the further details about the file. Full header preview
    is available in separate window.
    
  - Close - close this window

- Menu View:

  - Graph - show the data as a graph (plot)
  
  - Table - show the data as a table

  - Show errors - turns on and off the error bars (if available).

  - Grid - turn on and off the horizontal and vertical grid
  
- Menu Tools:

  - Statistics - turns on and off the 'Statistics' tool - see below.  
    
  - Measurement - the tool allows measuring distances using two cursors.
  
  
Statistics
----------

The Statistics is a tool that computes and shows the minimum, maximum, sample mean
and standard deviation. 

To activate the tool:

#. From the local menu, select :menuselection:`Tools --> Statistics`.
   A new panel on the right side of the preview window appears.

If no points are selected, all points in the data set are included in the computation. 
To restrict the data for the statistics, press and hold the Shift key and draw a 
rectangle in a graph while you keep the left mouse button pressed down.


Measurement
-----------

The `Measurement` tool displays two cursors in the graph. The cursors can be adjusted
by dragging them using the left mouse button. The position of each cursor, their distance
and statistics for the data between cursors is presented.

To activate the tool:

#. From the local menu, select :menuselection:`Tools --> Measurement`.
   A new panel on the right side of the preview window appears.

   .. figure:: images/measurement.png
      :align: center
      :alt: Measurement tool
   
   Measurement tool

\(1) Choose the axis you want to measure.

\(2) Positions of the cursor 1 and 2 are displayed here.

\(3) Distance between cursor 1 and 2.

\(4) When cursors are defined on the independent (X) axis, number of points (frames) between 
cursor 1 and 2 are displayed and also minimum, maximum, mean value and sample deviation
are presented.


.. seealso:: :ref:`open-file-dialog`
