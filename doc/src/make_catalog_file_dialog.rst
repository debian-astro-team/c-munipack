.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: make_catalog_file_dialog.rst,v 1.1 2015/07/06 08:19:49 dmotl Exp $

.. index::
   pair: Make catalog file; dialog

.. _make-catalog-file-dialog:

Make catalog file (dialog)
==========================

The "Make catalog file" dialog allows an user to fill in the information that he wants
to save to a new catalog file.


Activating the dialog
---------------------

The dialog can be activated:

.. |catalogfile_icon| image:: icons/catalogfile.png

#. from the main menu: :menuselection:`Tools --> Make catalog file`.

#. from the main toolbar: |catalogfile_icon|


Filling up the form
-------------------

.. figure:: images/makecatalog_couts.png
   :align: center
   :alt: Make catalog file
   
   The "Make catalog file" dialog
   
\(1) Enter a name of the catalogue file. The string can't include the characters that are not 
allowed in the file name, i.e. colon, slash, backslash, asterisk, question mark, etc.

\(2) This field shows a path to the folder where the new catalog file will be saved to.
Click on the "Browse" button to change the folder.

\(3) If you make light curves with heliocentric JD or air mass coefficients, you can
fill in the object's position. The coordinates will be restored when you use the catalog file
as a reference file in the matching. Click on the button (4) to retrieve the coordinates
from a list of user-defined objects or a catalog of variable stars.

\(5) If you make light curves air mass coefficients, you can fill in the observer's
location. The coordinates will be restored when you use the catalog file as a reference
file in the matching. Click on the button (6) to retrieve the coordinates from a list
of user-defined locations.

\(7) These fields are optional. There are no formatting rules for these fields.

\(8) If you want to provide more information, you can write it here.

\(9) The preview area shows the default selection of objects. The variable star is 
marked with red color, the comparison star is green and check stars are rendered in blue color.
Use the selection field (10) to review the defined selections. All the selections that
are presented here, will be saved to the catalog file. Click the :guilabel:`Edit` button (11) 
to edit the selections.

Click the button (12) to proceed.


.. seealso:: :ref:`match-stars-dialog`
