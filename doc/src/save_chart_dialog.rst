.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: save_chart_dialog.rst,v 1.1 2015/07/06 08:19:49 dmotl Exp $

.. index::
   pair: Save chart; dialog
   
.. _save-chart-dialog:

Save chart (dialog)
===================

The "Save chart" dialog is used to enter a name and a directory for a new image file.


Activating the dialog
---------------------

The dialog can be activated:

#. from the "Chart" dialog: button "Save".


The basic dialog
----------------

.. figure:: images/savechart_couts1.png
   :align: center
   :alt: Save chart dialog
   
   The "Save chart dialog" dialog (basic form)
   
In its basic form, as shown above, the dialog consists of a text box (1) to assign a name
to the file, and a drop-down list of bookmarks (2) to select a directory to save it in.

If the directory you want is not in the list of bookmarks, click on "Browse for other
folders" button (3) to expand the dialog to its full form.

(4) You can specify the size of the new image. You can either specify the size as a
percentage of the original CCD image (5) or you can specify the width (6) and height (7) of
the new image in pixels.

Click on the "Save" button (8) to make an image file.


Browsing the directories
------------------------

.. figure:: images/savechart_couts2.png
   :align: center
   :alt: Save chart dialog
   
   The "Save chart dialog" dialog (with browser)
   
\(10) Here, you can access to your main folders and to your store devices.

\(11) The middle panel displays a list of the files in the current directory. Change your
current directory by double left-clicking on a directory in this panel. Select a file with
a single left click. You can then replace the file you have selected by clicking on the
Save button. Note that a double left click start the operation.

\(12) Above the middle panel, the path of the current directory is displayed. You can navigate
along this path by clicking on one of the buttons.

You can right click on the middle panel to access the "Show Hidden Files" command.

\(13) Enter the file name of the new image file here.

\(14) This drop-down list is only available in the basic form of the dialog. It provides
a list of bookmarks for selecting a directory in which to save your file. click on "Browse for
other folders" button (15) to shrink the dialog to its basic form.

\(16) Here, you can add bookmarks to folders, by using the "Add" or the "Add to Bookmarks"
option you get by right-clicking a folder in the central panel, and also remove them.

\(17) By clicking the Add button, you add the selected folder to bookmarks. By clicking the Remove,
you remove the selected bookmark from the list.

If you want to save the image into a folder that doesn't yet exist, you can create it
by clicking on "Create Folder" button (18) and following the instructions.

\(20) You can specify the size of the new image. You can either specify the size as a
percentage of the original CCD image (21) or you can specify the width (22) and height (23) of
the new image in pixels.

Click on the "Save" button (24) to make an image file.
