.. C-Munipack - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: air_mass_curve_format.rst,v 1.2 2016/01/01 09:39:22 dmotl Exp $

.. index::
   pair: Air mass curve; format
      
.. _air-mass-curve-format:

Air mass curve files
====================

The table of air mass coefficients is stored in the ASCII format; lines are separated 
``CR+LF``, ``LF`` or ``CR`` character. The writer can choose any separator,
it is recommended to use a separator native to the writer's environment. The reader
must correctly handle any separator. The first line contains a list of column names 
separated by a single space character. Second line is left empty. 

On the following lines, the table values are stored. The values are separated by a 
space character, rows are separated by an end-of-line character (see above). 
Parsers must ignore all additional white characters between fields, at the beginning
and at the end of a line. See table :ref:`1 <table:amass_table>` for the description of 
the columns.

.. _table:amass_table:

Table 1

======== ===========
Keyword  Description
======== ===========
JD       Geocentric Julian date of observation 
AIRMASS  Air mass coefficient
ALTITUDE Apparent altitude in degrees above horizon
======== ===========
