.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: catalog_files.rst,v 1.2 2016/02/27 09:14:01 dmotl Exp $

.. index::
   pair: Catalog; files

.. _catalog-files:
   
Catalog files
=============

Catalog files speed up your work in case you often observe particular
star field, for example in long-term monitoring of a variable star. In a standard
work flow, you would have to mark the stars again and again. The catalog
file's structure is similar to that of the photometry file, except it includes
the selection of stars. When you use the catalog file as a reference frame
in the matching phase, the program will	match the photometry files against
it and the selection of stars is restored as well. Then, when you make a light
curve, the variable, comparison and check stars are already marked.

The first observation you process by standard procedure, using one frame from
a set as a reference file. When you have finished, you can make a catalog
file. Click on the :menuselection:`Tools --> Make catalog file` item in the main 
menu. A new dialog appears that allows you to select a variable star, a comparison 
star and optionally check one or more check stars. If you have made a light curve 
before, the stars are automatically selected.

Confirm the selection dialog by the :guilabel:`OK` button to proceed
to the next step. Another dialog appears. In its left part, fill in the edit fields,
you have to fill in the name of the catalogue file (first entry), all other fields
are optional and you can left them blank. Confirm the dialog by the
:guilabel:`Save` button.

.. figure:: images/makecatalog.png
   :align: center
   :alt: Sample catalog files
   
   The dialog for making a catalog file.

.. seealso:: :ref:`make-catalog-file-dialog` and :ref:`match-stars-dialog`
