.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: using_command_line_tools.rst,v 1.1 2015/07/06 08:19:49 dmotl Exp $

.. index::
   pair: program; using
   triple; command; line; using
   
.. using-command-line-tools:

Using command-line tools
========================

The following text describes the basic procedure for processing an observation of a 
short-periodic variable star by means of the C-Munipack toolkit.

The package with demo data used here is available on the project's web pages. Unzip
the archive to an empty folder at your hard disk (e.g. ~/c-munipack/sample), open a shell
or command-line window and make the folder your current working directory. The archive 
has got the following structure:

+--------------------------------------+----------------------------------------------+
| :file:`dark / 20s / masterdark.fts`  | master-dark frame (20 seconds exposure)      |
+--------------------------------------+----------------------------------------------+
| :file:`dark / 20s / raw / dark*.fts` | set of raw dark frames (20 seconds exposure) |
+--------------------------------------+----------------------------------------------+
| :file:`flat / v / masterflat.fts`    | master-flat frame (optical filter V)         |
+--------------------------------------+----------------------------------------------+
| :file:`flat / raw / flat-v*.fts`     | set of raw flat frames (optical filter V)    |
+--------------------------------------+----------------------------------------------+
| :file:`data / v / frame*.fts`        | sample CCD frames (optical filter V)         |
+--------------------------------------+----------------------------------------------+
   
The reduction of CCD data has the following general scheme:

#. Conversion of input files to a working format

#. Calibration

#. Photometry and matching

#. Making output

Most of widely used CCD camera controlling software save the data in FITS format,
which has virtually became the standard in this field. This is also a working format
for all tools from the C-Munipack project - it means, that they expect the FITS files
on their input and saves the output in this format, too. There is one exception to this
rule, the :command:`konve` command, which reads several different formats and
convert them to FITS. See the documentation for the :command:`konve` command
for the list of supported formats. If your files are not in FITS format, use this 
command to convert your source files to the FITS format. ::

   mkdir tmp
   konve -o tmp/frame???.fts your-files/\*.st7 
		
Even if your input files are saved in the FITS format, I would suggest you to
make a local copy, because it's always advisory to play safe and make sure you don't 
tamper your original data with a misprint in a command. You can use the :command:`konve` 
command or just simple :command:`cp` command for this job, but I will use the former 
to demonstrate its use. ::

   mkdir tmp
   konve -o tmp/frame???.fts data/v/\*.fts
   
First command makes a new folder, that we will use for storing all temporary files hereafter.
The second line makes a copy of all sample frames to it.

A raw CCD frame consists of several components. By the calibration process, we get rid of those 
which affect the result of the photometry. In some literature, the calibration is depicted as the 
peeling of an onion. There are three major components which a raw frame consists of - the current 
made by incident light, current made thermal drift of electrons (so-called dark current) and 
constant bias level. In standard calibration scheme, which we will demonstrate here, the dark-frame 
correction subtracts the dark current and the also the bias. Because of the nature of the dark current, 
it is necessary to use a correction frame of the same exposure duration as source files and it must 
be carried out on the same CCD temperature, too. Thus, the properly working temperature regulation 
on your CCD camera is vital. The dark-frame calibration is made by means of the :command:`darkbat`
command. ::

   darkbat -o tmp/dark???.fts dark/20s/masterdark.fts tmp/frame*.fts

Then, we have to compensate the spatial non-uniformity of a detector and whole optical
system. These non-uniformities are due to the fabrication process of a CCD chip and they are
also natural properties of all real optical components, lenses in particular. The flat-frame
correction uses a flat-frame to smooth them away. The flat-frame is a frame carried out 
while the telescope is pointed to uniformly luminous area. In practice, this condition is
very difficult to achieve, the clear sky before dusk is usually used instead. The :command:`flatbat`
has very similar syntax to the previous one. ::

   flatbat -o tmp/flat???.fts flat/v/masterflat.fts tmp/dark*.fts
		
The source files are calibrated now. We are ready to detect a stars on each frames and measure 
their brightness. This process is called the photometry and unlike the previous commands, the result 
is saved to a XML based file, so-called photometry file. There are a lot of parameters which affect
the star detection and also the brightness computation. In this example, the default values work fine, 
but I would suggest you to become familiar with at least two of them - FWHM and Threshold - before
you start a real work. ::

    muniphot -o tmp/frame???.pht tmp/flat*.fts
		
The previous command treated all source files independently. As a result of this, a star #1 
in one file is not necessarily the same as a star #1 in another file. The process called matching
finds correspondences between them and a unique identifier is assigned to each star. Before we do
so, we have to choose one frame, which shall be used as a reference frame. In my experience, the
frame with the greatest number of stars works the best. For our example, let's pick up the third
frame - :file:`frame003.pht`. ::

    munimatch -o tmp/match???.pht tmp/frame003.pht tmp/frame*.pht
		
The sample data is the observation of an eclipsing binary star. For most of such observers,
the goal is to make a light curve. A light curve is a table, which consists of at least two
columns - time stamp expressed as a Julian date and difference between two stars - a variable star
and a comparison star. A variable star is a star that is subject of brightness variation and
a comparison star is supposed to be constant. Before we go on, we have to find out the identifiers 
of at the variable star and the comparison star. This task is very difficult to perform on a 
command-line, so I will skip it for the present. If you followed the previous step, the variable
star is #1 and the comparison is #2.

Now, we have got all information we need to make a light curve. This is done by means of
the :command:`munilist` command. ::

   munilist -v 1 -c 2 output.txt tmp/match*.pht
		
Let's have a look on the output file. The first several line should look like this::
		
   JD V-C s1
   ApertureIndex = 1
   ApertureRadius = 2.0000
   Filter =  V

   2453614.46632 -0.1058 0.0110
   2453614.47054 -0.0952 0.0108 	
   ...

The first line is a table header. It consists of names of table columns separated by a space. 
The following lines up to the single empty line form the file header. They provide the
descriptive information about the data stored in the file. Each line consists of a keyword,
the equal sign an the value. After an empty line, the table data follows. Each row is written
on a separate line and columns are divided by a space.
