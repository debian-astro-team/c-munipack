.. C-Munipack - User's manual

   Copyright 2012 David Motl
   
   Permission is granted to copy, distribute and/or modify this document under the 
   terms of the GNU Free Documentation License, Version 1.2 or any later version published 
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and 
   no Back-Cover Texts.

   $Id: add_individual_frames_dialog.rst,v 1.1 2015/07/06 08:19:48 dmotl Exp $

.. index:: 
   pair: Add individual frames; dialog

.. _add-individual-frames-dialog:
   
Add individual frames (dialog)
==============================

The "Add individual frames" dialog is used to add source frames into the current project.
Unlike the closely related dialog "Add frames from folder", it allows an user to select individual
files.


Activating the dialog
---------------------

The dialog can be activated:

.. |addfiles_icon| image:: icons/addfiles.png

#. from the main menu: :menuselection:`Frames --> Add individual frames`.

#. from the main toolbar: |addfiles_icon|


File browsing
-------------

.. figure:: images/addfiles_couts.png
   :align: center
   :alt: Add files dialog
   
   The dialog for adding individual files to the project.

The button "Type a file name" (1) shows and hides the "Location" text box. The keyboard shortcut
Ctrl+L key combination does the same action.

In the "Location" text box (2) you can type a path to a file. If you don't type any path, the name
of the selected file will be displayed. You can also type the first letters of the name: it will be
auto-completed and a list of file names beginning with these letters will be displayed.

The path to the current folder is displayed at the top of the dialog (3). You can navigate along
this path by clicking on an element.

\(4) Here, you can access to your main folders and to your store devices.

\(5) Here, you can add bookmarks to folders, by using the "Add" or the "Add to Bookmarks" option you
get by right-clicking a folder in the central panel, and also remove them.

The contents of the selected folder is displayed here (6). Change your current folder by double left
clicking on a folder in this panel. Select a file with a single left click. You can use Shift and Ctrl
modifiers to select multiple files. Right-clicking a folder name opens a context menu.

If the selected file is a file recognized by the C-Munipack, the preview and short info is displayed
in the right part of the dialog (7). Double click on the preview to show a larger preview in a separate
dialog.

\(8) By clicking the Add button, you add the selected folder to bookmarks. By clicking the Remove,
you remove the selected bookmark from the list.


Including files the project
---------------------------

To add the selected files to the project, click the "Add" button (9). The program checks the selected
files. Several situations may occur:

- If the file is not a valid CCD frame, it is ignored.

- If the file is already included in the project, it is ignored.

- If there are multiple files left and they have different color filter name, the program
  shows a new dialog and asks an user to select which filters shall be added to the project.
  You can use the "All files" option or select one or more items in the box, other files will be ignored.
  
- If there are multiple files left and they have different exposure duration, the program
  shows another dialog and asks an user to select which exposures shall be added to the project.
  You can use the "All files" option or select one or more items in the box, other files will be ignored.
  
- If there are multiple files left and they have different object designation, the program
  shows another dialog and asks an user to select which objects shall be added to the project.
  You can use the "All files" option or select one or more items in the box, other files will be ignored.
  
When the action is completed, the dialog is not closed; you can go on to add files from another
folder or close the dialog by the "Close" button (10).


.. seealso:: :ref:`add-frames-from-folder-dialog`, :ref:`main-window`
