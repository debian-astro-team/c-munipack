.. C-Munipack - User's manual

   Copyright 2012 David Motl

   Permission is granted to copy, distribute and/or modify this document under the
   terms of the GNU Free Documentation License, Version 1.2 or any later version published
   by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and
   no Back-Cover Texts.

   $Id: main_menu.rst,v 1.3 2016/02/27 09:14:01 dmotl Exp $

.. index::
   pair: main; menu

.. _main-menu:

Main menu
=========

Here is the list of all items that can be displayed in the main menu. Please note, that some items
may be hidden according to the actual project type. See :ref:`projects` topic for details.


- Menu `Project`
  
  - :menuselection:`New` - start a new project; see :ref:`new-project-dialog`.

  - :menuselection:`Open` - open an existing project; see :ref:`open-project-dialog`.

  - :menuselection:`Recent projects` - open one of the most recently used projects.
  
  - :menuselection:`Export` - make copy of the current project to another location; see :ref:`export-project-dialog`.

  - :menuselection:`Close` - close current project.

  - :menuselection:`Edit project settings` - change project settings - name, type, processing parameters, etc.; see :ref:`project-settings-dialog`.

  - :menuselection:`Exit` - close the project and the main window.


- Menu `Frames`

  - :menuselection:`Add individual frames` - add one or more frames from a folder; see :ref:`add-individual-frames-dialog`.

  - :menuselection:`Add frames from folder` - add frames from a folder and its subfolders; see :ref:`add-frames-from-folder-dialog`.

  - :menuselection:`Remove selected frames` - remove selected frames from the project.

  - :menuselection:`Remove all frames` - remove all frames from the project.

  - :menuselection:`Show selected frame` - open selected frame in a preview window; see :ref:`frame-preview-window`.

  - :menuselection:`Show properties` - show properties of a selected frame.

  
- Menu `Reduce`

  - :menuselection:`Express reduction` - conversion, calibration, photometry and matching done at in a batch; see :ref:`express-reduction-dialog`.
  
  - :menuselection:`Process new frames` - process frames that were appended to the project, apply the same rules 
    as to the frames that had been already processed; watch periodically for new frames in a folder, append them to 
    the project and process them; see :ref:`process-new-frames-dialog`.
  
  - :menuselection:`Fetch/covert files` - make working copy of the source frames, convert them if necessary; see :ref:`fetch-convert-files-dialog`.
    
  - :menuselection:`Bias correction` - apply bias correction; see :ref:`bias-correction-dialog`.
  
  - :menuselection:`Dark correction` - apply dark correction; see :ref:`dark-correction-dialog`.
  
  - :menuselection:`Flat correction` - apply flat correction; see :ref:`flat-correction-dialog`.
  
  - :menuselection:`Photometry` - run photometry; see :ref:`photometry-dialog`.

  - :menuselection:`Match stars` - find cross-references between stars on the frames; see :ref:`match-stars-dialog`.

  
- Menu `Plot`

  - :menuselection:`Light curve` - make light curve (magnitude vs. time) for a variable; see :ref:`make-light-curve-dialog`.

  - :menuselection:`Track curve` - make track curve (frame offset vs. time); see :ref:`make-track-curve-dialog`.

  - :menuselection:`Air mass` - make air mass curve (air mass vs. time); see :ref:`make-air-mass-curve-dialog`.
  
  - :menuselection:`CCD temperature` - make CCD temperature curve (temperature vs. time); see :ref:`make-ccd-temperature-dialog`.
  
  - :menuselection:`Object properties` - plot object properites (position, de-focus (FWHM), instrumental magnitude, ...); see :ref:`make-object-properties-dialog`.

  
- Menu `Make`

  - :menuselection:`Master bias frame` - make master bias frame; see :ref:`master-bias-frame-dialog`.

  - :menuselection:`Master dark frame` - make master dark frame; see :ref:`master-dark-frame-dialog`.
  
  - :menuselection:`Master flat frame` - make master flat frame; see :ref:`master-flat-frame-dialog`.
  
  - :menuselection:`Merge frames` - merge frames to make combined frames; see :ref:`merge-frames-dialog`.
  
  
- Menu `Tools`

  - :menuselection:`Find variables` - semi-automatic search for variable stars; see :ref:`make-find-variables-dialog`.

  - :menuselection:`Make catalog file` - make a catalog file (a reference frame for the field); see :ref:`make-catalog-file-dialog`.

  - :menuselection:`Show thumbnails` - show small previews of all frames in the projects; see :ref:`thumbnails-dialog`.

  - :menuselection:`Show message log` - show detailed report from the last process; see :ref:`message-log-dialog`.

  - :menuselection:`Open file` - open CCD frame, photometry file or curve in a preview window; see :ref:`image-file-window`, 
    :ref:`photometry-file-window`, :ref:`catalog-file-window` and :ref:`graph-window`.
    
  - :menuselection:`Recent files` - open one of the most recently used files.
  
  - :menuselection:`Import data from C-Munipack 1.x` - import data from a folder that contains C-Munipack 1.x files; see :ref:`import-project-dialog`.    
  
  - :menuselection:`JD converter` - convert a Julian date into a Gregorian calendar date; see :ref:`jd-converter-dialog`.    

  - :menuselection:`Heliocentric correction` - compute heliocentric correction for given Julian date and object corrdinates; 
    see :ref:`heliocentric-correction-dialog`.    
  
  - :menuselection:`Air mass coefficient` - compute air mass coefficient for given Julian date, object corrdinates and
    observer coordinates; see :ref:`air-mass-coefficient-dialog`.    

  - :menuselection:`Edit profiles` - open dialog for editing user-defined profiles; see :ref:`edit-profiles-dialog`.    
    
  - :menuselection:`Environment options` - change user interface settings; see :ref:`environment-options-dialog`.    

  
- Menu `Help`
    
  - :menuselection:`Show help` - show help for the main application window.

  - :menuselection:`User's manual` - open the user manual at the top-level table of contents.
                               
  - :menuselection:`About Muniwin` - information about version of the software and libraries, license and authors.
  
.. seealso:: :ref:`main-window`, :ref:`main-toolbar`
